
#include "dataset.h"
#include <stdio.h>
#include <string.h>

int
dsReadDataset(
			char	*pcFileName,
			DATASET &ds
			)
{
	ds.iFeaturesPerVector = 0;
	ds.iVectorCount = 0;
	ds.pfVectors = 0;

	FILE *infile = fopen( pcFileName, "rt" );
	if( !infile )
	{
		return 0;
	}

	ds.iFeaturesPerVector = 1;

	char buffer[1000];
	while( !strchr(buffer, '.') )
	{
		fgets( buffer, sizeof(buffer), infile );
	}
	char *pcDataPtr = buffer;
	while( (pcDataPtr = strchr( pcDataPtr, ' ' )) != 0 )
	{
		pcDataPtr++;
		ds.iFeaturesPerVector++;
	}

	// Get the number of feature vectors

	ds.iVectorCount = 1;

	while( fgets( buffer, sizeof(buffer), infile ) != 0 )
	{
		if( strchr( buffer, '.' ) )
		{
			ds.iVectorCount++;
		}
	}

	ds.pfVectors = new double[ds.iFeaturesPerVector*ds.iVectorCount];

	if( !ds.pfVectors )
	{
		ds.iFeaturesPerVector = 0;
		ds.iVectorCount = 0;
		ds.pfVectors = 0;
		return 0;
	}

	fseek( infile, 0, SEEK_SET );

	int iFeatureCount = 0;

	while( fgets( buffer, sizeof(buffer), infile ) )
	{
		if( !strchr( buffer, '.' ) )
		{
			continue;
		}
		pcDataPtr = buffer;
		float fData;
		while( sscanf( pcDataPtr, "%e", &fData ) )
		{
			ds.pfVectors[iFeatureCount] = (double)fData;
			iFeatureCount++;
			if( ( pcDataPtr = strchr( pcDataPtr, ' ' ) ) == 0 )
			{
				break;
			}
			pcDataPtr++;
		}
	}

	fclose( infile );

	return 1;
}

int
dsWriteDataset(
			char	*pcFileName,
			DATASET &ds
			)
{
	FILE *outfile = fopen( pcFileName, "wt" );
	if( !outfile )
	{
		return 0;
	}

	for( int i = 0; i < ds.iVectorCount; i++ )
	{
		fprintf( outfile, "%.4f", ds.pfVectors[i*ds.iFeaturesPerVector] );
		for( int j = 1; j < ds.iFeaturesPerVector; j++ )
		{
			fprintf( outfile, " %.4f", ds.pfVectors[i*ds.iFeaturesPerVector + j] );
		}

		fprintf( outfile, "\n" );
	}

	fclose( outfile );

	return 1;
}

int
dsDeleteDataset(
			DATASET &ds
			)
{
	if( ds.pfVectors != 0 )
	{
		delete [] ds.pfVectors;
	}

	ds.iFeaturesPerVector = 0;
	ds.iVectorCount = 0;
	ds.pfVectors = 0;

	return 1;
}


