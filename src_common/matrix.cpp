
#include "matrix.h"
#include <assert.h>
#include <memory.h>
#include <math.h>
#include <stdio.h>

int
matrix::delete_data(
)
{
	if( m_pdData )
	{
		// Data exists
		assert( m_pdDataRows );
		assert( m_iRows > 0 );
		assert( m_iCols > 0 );
		if( !m_pdDataRows || m_iRows <= 0 || m_iCols <= 0 )
		{
			return 0;
		}
		delete [] m_pdData;
		delete [] m_pdDataRows;

		m_iRows = 0;
		m_iCols = 0;
		m_pdData = 0;
		m_pdDataRows = 0;
	}
	else
	{
		// Data does not exist
		assert( m_pdDataRows == 0 );
		assert( m_iRows == 0 );
		assert( m_iCols == 0 );
		if( m_pdDataRows || m_iRows > 0 || m_iCols > 0 )
		{
			return 0;
		}
	}

	return 1;
}

int
matrix::new_data(
	const int &iRows,
	const int &iCols
)
{
	if( !delete_data() )
	{
		return 0;
	}
	
	m_iRows = iRows;
	m_iCols = iCols;
	m_pdData = new double[iRows*iCols];
	m_pdDataRows = new double*[iRows];
	assert( m_pdData && m_pdDataRows );

	if( !m_pdData || !m_pdDataRows )
	{
		return 0;
	}
	for( int r = 0; r < iRows; r++ )
	{
		m_pdDataRows[r] = m_pdData + r*m_iCols;
	}

	for( int i = 0; i < iRows*iCols; i++ )
	{
		m_pdData[i] = 0.0;
	}

	return 1;
}

matrix::matrix(
			   )
{
	m_pdData = 0;
	m_pdDataRows = 0;
	m_iRows = 0;
	m_iCols = 0;
}

matrix::matrix(
			   const int &iRows,
			   const int &iCols
			   )
{
	m_pdData = 0;
	m_pdDataRows = 0;
	m_iRows = 0;
	m_iCols = 0;
	new_data( iRows, iCols );
}

matrix::~matrix(
				)
{
	delete_data();
}

int
matrix::Rows(
			 ) const
{
	return m_iRows;
}
int
matrix::Cols(
			 ) const
{
	return m_iCols;
}

double **
matrix::RowPointers(
					) const
{
	return m_pdDataRows;
}

double *
matrix::RowPointer(
				   const int &iRow
				   ) const
{
	assert( iRow < m_iRows && iRow >= 0 );
	return m_pdDataRows[ iRow ];
}

double &
matrix::operator()(
				   const int &iRow,
				   const int &iCol
				   )
{
	assert( iRow < m_iRows && iRow >= 0 && iCol < m_iCols && iCol >= 0 );
	return m_pdData[ iRow*m_iCols + iCol ];
}

double
matrix::operator()(
				   const int &iRow,
				   const int &iCol
				   ) const
{
	assert( iRow < m_iRows && iRow >= 0 && iCol < m_iCols && iCol >= 0 );
	return m_pdData[ iRow*m_iCols + iCol ];
}

matrix &
matrix::operator=(
		const matrix &mat
				   )
{
	delete_data();
	new_data( mat.Rows(), mat.Cols() );
	memcpy( m_pdData, mat.RowPointer( 0 ), mat.Rows()*mat.Cols()*sizeof(double) );
	return *this;
}

int
matrix::multiply_scalar(
						const double &dFactor
						)
{

	for( int r = 0; r < Rows(); r++ )
	{
		for( int c = 0; c < Cols(); c++ )
		{
			(*this)( r, c ) *= dFactor;
		}
	}
	return 1;
}

int
matrix::add_matrix(
				   const matrix &mat
				   )
{
	if( mat.Rows() != m_iRows || mat.Cols() != m_iCols )
	{
		assert( 0 );
		return 0;
	}

	for( int r = 0; r < Rows(); r++ )
	{
		for( int c = 0; c < Cols(); c++ )
		{
			(*this)( r, c ) += mat(r,c);
		}
	}

	return 1;
}

	
double
matrix::length(
			) const
{
	double dSumOfSquares = 0.0;
	for( int r = 0; r < Rows(); r++ )
	{
		for( int c = 0; c < Cols(); c++ )
		{
			double dValue = (*this)( r, c );
			dSumOfSquares += dValue*dValue;
		}
	}
	return sqrt( dSumOfSquares );
}

int
matrix::writeFile(
			const char *pchFileName
			) const
{
	if( !pchFileName )
	{
		return 0;
	}

	FILE *outfile = fopen( pchFileName, "wt" );
	if( !outfile )
	{
		return 0;
	}

	for( int r = 0; r < Rows(); r++ )
	{
		for( int c = 0; c < Cols(); c++ )
		{
			fprintf( outfile, "%f\t", (*this)( r, c ) );
		}
		fputs( "\n", outfile );
	}

	fclose( outfile );

	return 1;
}
