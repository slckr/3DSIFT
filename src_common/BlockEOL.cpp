


#include "BlockEOL.h"
#include "convolution.h"

#include <stdio.h>
#include <math.h>
#include <assert.h>

//
// calculate_eol_sums_ncc()
//
// Calculates EOL for the normalized covariance similarity measure.
//
// Probability of the form:
//
//	p(Block2=Block1) = exp( -[ 1 - NCC(Block1,Block) ] * fNCCVarrDiv )
//
float
calculate_eol_sums_ncc(
					   const int &iRowStart,		// Domain start
					   const int &iColStart,
					   const int &iRows,			// Domain extents
					   const int &iCols,
					   
					   const PpImage &ppTmp1,		// Source Template (fio1)
					   const PpImage &ppImg2,		// Domain Image (fio2)
					   
					   const float		&fStdDev1,	// Stddev fir ppTmp1
					   const FEATUREIO	&fio2Varr,	// Varriance for all locations in ppImg2
					   
					   const float &fNCCVarrDiv		// Varriance term for NCC energy
					   )
{
	float fEntropySum = 0.0f;
	float fLikelihoodSum = 0.0f;
	float fEntropy = 0.0;
	
	int iRowFinish = iRowStart + iRows;
	int iColFinish = iColStart + iCols;
	
	//FILE *outfile = fopen( "prob.txt", "wt" );
	
	for( int y = iRowStart; y < iRowFinish; y++ )
	{
		for( int x = iColStart; x < iColFinish; x++ )
		{
			float fResult;

			if( fio2Varr.pfVectors[y*fio2Varr.x + x] == 0 )
			{
				// Ill-defined NCC
				fResult = 9e20f;
			}
			else
			{
				covariance_reflection_float( ppImg2, ppTmp1, y, x, fResult );
				fResult /= fStdDev1*sqrt( fio2Varr.pfVectors[y*fio2Varr.x + x] );
				fResult = (1.0f - fResult)*fNCCVarrDiv;
			}
			
			float fExp = exp( -fResult );
			
			//fprintf( outfile, "%100.100f\t", fExp );
			
			fLikelihoodSum += fExp;
			fEntropySum    += fExp*fResult;
		}
		//fprintf( outfile, "\n" );
	}
	//fclose( outfile );
	fEntropy = log( fLikelihoodSum ) + fEntropySum / fLikelihoodSum;
	return fEntropy;
}

float
calculate_eol_sums_ncc_robust(
					   const int &iRowStart,		// Domain start
					   const int &iColStart,
					   const int &iRows,			// Domain extents
					   const int &iCols,
					   
					   const PpImage &ppTmp1,		// Source Template (fio1)
					   const PpImage &ppImg2,		// Domain Image (fio2)
					   
					   const float		&fStdDev1,	// Stddev fir ppTmp1
					   const FEATUREIO	&fio2Varr,	// Varriance for all locations in ppImg2
					   
					   const float &fRobustVarr		// Varriance term for NCC energy
					   )
{
	float fEntropySum = 0.0f;
	float fLikelihoodSum = 0.0f;
	float fEntropy = 0.0;

	int iRowFinish = iRowStart + iRows;
	int iColFinish = iColStart + iCols;

	//FILE *outfile = fopen( "prob.txt", "wt" );

	for( int y = iRowStart; y < iRowFinish; y++ )
	{
		for( int x = iColStart; x < iColFinish; x++ )
		{
			float fResult;

			if( fio2Varr.pfVectors[y*fio2Varr.x + x] == 0 )
			{
				// Ill-defined NCC
				fResult = 9e20f;
			}
			else
			{
				covariance_reflection_float( ppImg2, ppTmp1, y, x, fResult );
				fResult /= fStdDev1*sqrt( fio2Varr.pfVectors[y*fio2Varr.x + x] );
				fResult = (1.0f - fResult);
			}
			
			float fExp = fRobustVarr / (fRobustVarr + fResult);
			
			//fprintf( outfile, "%100.100f\t", fExp );
			
			fLikelihoodSum += fExp;
			fEntropySum    += fExp*fResult;
		}
		//fprintf( outfile, "\n" );
	}
	//fclose( outfile );
	fEntropy = log( fLikelihoodSum ) + fEntropySum / fLikelihoodSum;
	return fEntropy;
}

static float
calculate_eol_ncc_standard(
					   const int &iRowStart,		// Domain start
					   const int &iColStart,
					   const int &iRows,			// Domain extents
					   const int &iCols,
					   
					   const PpImage &ppTmp1,		// Source Template (fio1)
					   const PpImage &ppImg2,		// Domain Image (fio2)
					   
					   const float		&fStdDev1,	// Stddev fir ppTmp1
					   const FEATUREIO	&fio2Varr,	// Varriance for all locations in ppImg2
					   
					   const float &fNCCVarrDiv		// Varriance term for NCC energy
					   )
{
	double dEntropySum = 0;
	
	int iRowFinish = iRowStart + iRows;
	int iColFinish = iColStart + iCols;
	
	//FILE *outfile = fopen( "prob.txt", "wt" );

	// Calculate normalization constant

	double dProbSum = 0.0;

	for( int y = iRowStart; y < iRowFinish; y++ )
	{
		for( int x = iColStart; x < iColFinish; x++ )
		{
			float fResult;

			if( fio2Varr.pfVectors[y*fio2Varr.x + x] == 0 )
			{
				// Ill-defined NCC
				fResult = 9e30f;
			}
			else
			{
				covariance_reflection_float( ppImg2, ppTmp1, y, x, fResult );
				fResult /= fStdDev1*sqrt( fio2Varr.pfVectors[y*fio2Varr.x + x] );
				fResult = (1.0f - fResult)*fNCCVarrDiv;
			}
			
			dProbSum += exp( -fResult );
		}
	}

	for( int y = iRowStart; y < iRowFinish; y++ )
	{
		for( int x = iColStart; x < iColFinish; x++ )
		{
			float fResult;

			if( fio2Varr.pfVectors[y*fio2Varr.x + x] == 0 )
			{
				// Ill-defined NCC
				fResult = 9e30f;
			}
			else
			{
				covariance_reflection_float( ppImg2, ppTmp1, y, x, fResult );
				fResult /= fStdDev1*sqrt( fio2Varr.pfVectors[y*fio2Varr.x + x] );
				fResult = (1.0f - fResult)*fNCCVarrDiv;
			}
			
			double dProb = exp( -fResult ) / dProbSum;

			dEntropySum += dProb*log( dProb );
		}
		//fprintf( outfile, "\n" );
	}
	//fclose( outfile );
	return (float)dEntropySum;
}

//
// calculate_eol_image_ncc()
//
// Calculates EOL for the normalized covariance similarity measure.
//
// Probability of the form:
//
//	p(Block2=Block1) = exp( [ 1 - NCC(Block1,Block) ] * fNCCVarrDiv )
//
int
calculate_eol_image_ncc(
						const int &iRowOffset,	// Offset of a pixel in fio1 to fio2
						const int &iColOffset,
						const int &iRows,		// Domain size in fio2
						const int &iCols,		//
						FEATUREIO &fio1,
						FEATUREIO &fio2,
						FEATUREIO &fioEOL,
						
						FEATUREIO &fio1Varr,	// Stuff related to normalized covariance
						FEATUREIO &fio2Varr, // Precalculated variances
						const int &iRowSize, // Size of window
						const int &iColSize, 
						const float &fNCCVarrDiv		// Varriance term for NCC energy
						)
{
	PpImage ppImg, ppTmp;
	
	// Initialized image references
	
	ppImg.InitializeSubImage( fio2.y, fio2.x, fio2.x*sizeof(float), sizeof(float)*8, (unsigned char*)fio2.pfVectors );
	
	float fEntropyDivisor = 1.0f / log((float)iRows*iCols);
	
	int iPercentDone = (fio1.x*fio1.y)/100;
	
	int iRowsDiv2=iRows/2, iColsDiv2=iCols/2;
	
	fioSet( fioEOL, 1.0f );
	
	for( int y = iRowSize/2; y < fio1.y - iRowSize/2; y++ )
	{
		int iRow2Start = y + iRowOffset - iRowsDiv2;
		if( iRow2Start < 0 )
		{
			iRow2Start = 0;
		}
		else if( iRow2Start + iRows >= fio2.y - 1 )
		{
			iRow2Start = fio2.y - iRows - 1;
		}
		printf( "row: %2.2d\n", y );
		
		for( int x = iColSize/2; x < fio1.x - iColSize/2; x++ )
		{			
			int iCol2Start = x + iColOffset - iColsDiv2;
			if( iCol2Start < 0 )
			{
				iCol2Start = 0;
			}
			else if( iCol2Start + iCols >= fio2.x - 1 )
			{
				iCol2Start = fio2.x - iCols - 1;
			}
			
			if( fio1Varr.pfVectors[y*fio1.x + x] == 0 )
			{
				// Ill-defined NCC for variance 0, set to bad entropy value
				fioEOL.pfVectors[y*fioEOL.x + x] = 1.0f;
			}
			else
			{			
				ppTmp.InitializeSubImage( iRowSize, iColSize, fio1.x*sizeof(float), sizeof(float)*8,
					(unsigned char*)(fio1.pfVectors + (y - iRowSize/2)*fio1.x + (x - iColSize/2)) );
				
				float fEntropy =
					//calc_eol_pixel(
					calculate_eol_sums_ncc(
					iRow2Start, iCol2Start, 
					iRows, iCols,
					ppTmp, ppImg,
					sqrt( fio1Varr.pfVectors[y*fio1Varr.x + x] ),
					fio2Varr, fNCCVarrDiv );

				//
				//float fEntropyCheck = calculate_eol_ncc_standard(
				//	iRow2Start, iCol2Start, 
				//	iRows, iCols,
				//	ppTmp, ppImg,
				//	sqrt( fio1Varr.pfVectors[y*fio1Varr.x + x] ),
				//	fio2Varr, fNCCVarrDiv );
				//

				// Normalize
				fEntropy *= fEntropyDivisor;
				
				//if( fEntropy < 0.0 || fEntropy > 1.0 ) { assert( 0 ); }
				fioEOL.pfVectors[y*fioEOL.x + x] = fEntropy;
			}
		}
	}
	
	return 1;
}


//
// calculate_eol_sums_ssd()
//
// Calculates EOL for the normalized covariance similarity measure.
//
// Probability of the form:
//
//	p(Block2=Block1) = exp( -SSD(Block1,Block) * fSSDVarr )
//
float
calculate_eol_sums_ssd(
					   const int &iRowStart,		// Domain start
					   const int &iColStart,
					   const int &iRows,			// Domain extents
					   const int &iCols,
					   
					   const PpImage &ppTmp1,		// Source Template (fio1)
					   const PpImage &ppImg2,		// Domain Image (fio2)
					   
					   const float &fSSDVarrDiv		// Varriance term for NCC energy
					   )
{
	float fEntropySum = 0.0f;
	float fLikelihoodSum = 0.0f;
	float fEntropy = 0.0;
	
	int iRowFinish = iRowStart + iRows;
	int iColFinish = iColStart + iCols;
	
	//FILE *outfile = fopen( "prob.txt", "wt" );
	
	for( int y = iRowStart; y < iRowFinish; y++ )
	{
		for( int x = iColStart; x < iColFinish; x++ )
		{
			float fResult;
			
			//ssd_reflection_float_remove_mean( ppImg2, ppTmp1, y, x, fResult );
			ssd_reflection_float( ppImg2, ppTmp1, y, x, fResult );
			fResult = fResult*fSSDVarrDiv;
			
			float fExp = exp( -fResult );
			
			//fprintf( outfile, "%100.100f\t", fExp );
			
			fLikelihoodSum += fExp;
			fEntropySum    += fExp*fResult;
		}
		//fprintf( outfile, "\n" );
	}
	//fclose( outfile );
	fEntropy = log( fLikelihoodSum ) + fEntropySum / fLikelihoodSum;
	return fEntropy;
}


//
// calculate_eol_sums_ssd()
//
// Calculates EOL for the normalized covariance similarity measure.
//
// Probability of the form:
//
//	p(Block2=Block1) = exp( -SSD(Block1,Block) * fSSDVarr )
//
float
calculate_eol_sums_ssd_robust(
					   const int &iRowStart,		// Domain start
					   const int &iColStart,
					   const int &iRows,			// Domain extents
					   const int &iCols,
					   
					   const PpImage &ppTmp1,		// Source Template (fio1)
					   const PpImage &ppImg2,		// Domain Image (fio2)
					   
					   const float &fSSDVarr		// Varriance term for NCC energy
					   )
{
	float fEntropySum = 0.0f;
	float fLikelihoodSum = 0.0f;
	float fEntropy = 0.0;
	
	int iRowFinish = iRowStart + iRows;
	int iColFinish = iColStart + iCols;
	
	//FILE *outfile = fopen( "prob.txt", "wt" );
	
	for( int y = iRowStart; y < iRowFinish; y++ )
	{
		for( int x = iColStart; x < iColFinish; x++ )
		{
			float fResult;
			
			//ssd_reflection_float_remove_mean( ppImg2, ppTmp1, y, x, fResult );
			ssd_reflection_float( ppImg2, ppTmp1, y, x, fResult );
			fResult = fResult*fSSDVarr;
			
			float fExp = fSSDVarr / (fSSDVarr + fResult);
			
			//fprintf( outfile, "%100.100f\t", fExp );
			
			fLikelihoodSum += fExp;
			fEntropySum    += fExp*fResult;
		}
		//fprintf( outfile, "\n" );
	}
	//fclose( outfile );
	fEntropy = log( fLikelihoodSum ) + fEntropySum / fLikelihoodSum;
	return fEntropy;
}

int
calculate_eol_image_ssd(
						const int &iRowOffset,	// Offset of a pixel in fio1 to fio2
						const int &iColOffset,
						
						const int &iRows,		// Domain size in fio2
						const int &iCols,		//
						
						FEATUREIO &fio1,
						FEATUREIO &fio2,
						FEATUREIO &fioEOL,
						
						const int &iRowSize, // Size of window
						const int &iColSize, 
						
						const float &fSSDVarrDiv		// Varriance term for NCC energy
						)
{
	PpImage ppImg, ppTmp;
	
	// Initialized image references
	
	ppImg.InitializeSubImage( fio2.y, fio2.x, fio2.x*sizeof(float), sizeof(float)*8, (unsigned char*)fio2.pfVectors );
	
	float fEntropyDivisor = 1.0f / log((float)iRows*iCols);
	
	int iPercentDone = (fio1.x*fio1.y)/100;
	
	int iRowsDiv2=iRows/2, iColsDiv2=iCols/2;
	
	fioSet( fioEOL, 1.0f );

	for( int y = iRowSize/2; y < fio1.y - iRowSize/2; y++ )
	{
		int iRow2Start = y + iRowOffset - iRowsDiv2;
		if( iRow2Start < 0 )
		{
			iRow2Start = 0;
		}
		else if( iRow2Start + iRows >= fio2.y - 1 )
		{
			iRow2Start = fio2.y - iRows - 1;
		}
		printf( "row: %2.2d\n", y );
		
		for( int x = iColSize/2; x < fio1.x - iColSize/2; x++ )
		{			
			int iCol2Start = x + iColOffset - iColsDiv2;
			if( iCol2Start < 0 )
			{
				iCol2Start = 0;
			}
			else if( iCol2Start + iCols >= fio2.x - 1 )
			{
				iCol2Start = fio2.x - iCols - 1;
			}
			
			ppTmp.InitializeSubImage( iRowSize, iColSize, fio1.x*sizeof(float), sizeof(float)*8,
				(unsigned char*)(fio1.pfVectors + (y - iRowSize/2)*fio1.x + (x - iColSize/2)) );
			
			float fEntropy =
				calculate_eol_sums_ssd(
				iRow2Start, iCol2Start, 
				iRows, iCols,
				ppTmp, ppImg,
				fSSDVarrDiv );
			
			// Normalize
			fEntropy *= fEntropyDivisor;
			
			//if( fEntropy < 0.0 || fEntropy > 1.0 ) { assert( 0 ); }
			fioEOL.pfVectors[y*fioEOL.x + x] = fEntropy;
		}
	}
	
	return 1;
}

