
#ifndef __OUTPUT_FUNCTIONS_H__
#define __OUTPUT_FUNCTIONS_H__

#include "PpImage.h"
#include "Transform.h"
#include "Posterior.h"
#include "BilinearInterpolation.h"
#include "FeatureIO.h"

//
// output_transform_neighbours()
//
// Outputs an image of the transform neighbourhood relationships
//
int
output_transform_neighbours(
							const PpImage &ppImgTr,		// Transform
							const PpImage &ppImgDomain,	// Image over domain
							const int &iMult,				// Tells how big to stretch the output domain
							char *pcFileName,				// File name
							int bOverlayOnImage			// If true, overlays arrows on ppImgDomain
							);

int
output_transform_neighbours(
							const LOCATION_VALUE_ARRAY &lvaLocations,
							const int iLocations,
							const PpImage &ppImgNeighbours,
							const PpImage &ppImgDomain,	 // Image over domain
							const int &iMultOutput, // Tells how big to stretch output
							const int &iMult, // Tells how big to stretch the locations
							char *pcFileName				// File name
							);
int
output_transform_bubbles(
							const LOCATION_VALUE_ARRAY &lvaLocations,
							const int iLocations,
							const PpImage &ppImgNeighbours,
							const PpImage &ppImgDomain,	 // Image over domain
							const int &iMultOutput, // Tells how big to stretch output
							const int &iMult, // Tells how big to stretch the locations
							char *pcFileName,				// File name
							unsigned char *pucRGB = 0
							);
int
output_transform_bubbles(
							const LOCATION_VALUE_XYZ_ARRAY &lvaLocations,
							const int iLocations,
							const PpImage &ppImgNeighbours,
							const PpImage &ppImgDomain,	 // Image over domain
							const int &iMultOutput, // Tells how big to stretch output
							const int &iMult, // Tells how big to stretch the locations
							char *pcFileName,				// File name
							unsigned char *pucRGB = 0
							);

//
// output_transform_arrows()
//
// Outputs an image of the transform arrows from their position
// in image 1 to image 2
//
int
output_transform_arrows(
						const PpImage &ppImgTr,		// Transform
						const PpImage &ppImgDomain,	// Tells how big the output domain is
						const int &iMult,				// Tells how big to stretch the output domain
						char *pcFileName,				// File name
						int bOverlayOnImage			// If true, overlays arrows on ppImgDomain
						);

int
output_list_float(
				  const PpImage &ppImg,
				  const LOCATION_VALUE *plp,
				  const int &iLocationProbs,
				  char *pcFileName
				  );

int
output_neighbourhood(
					 const PpImage &ppImgTr,
					 const int &iRow,
					 const int &iCol,
					 char *pcFileName
					 );


int
of_paint_grey_circle(
					  int iRow,
					  int iCol,
					  int iRadius,
					  PpImage &ppImgOut
			 );
int
of_paint_white_circle(
					  int iRow,
					  int iCol,
					  int iRadius,
					  PpImage &ppImgOut
			 );

int
of_paint_black_circle(
					  int iRow,
					  int iCol,
					  int iRadius,
					  PpImage &ppImgOut
			 );

int
of_paint_circle(
					  int iRow,
					  int iCol,
					  int iRadius,
					  PpImage &ppImgOut,
					  unsigned char *pucRGB = 0,	// color
					  float fAlphaValue = 1.0f,	// alpha blending value
					  PpImage *ppImgSrc = 0 // source image for blending
			 );

int
of_paint_keypoint(
				  float fRow,
				  float fCol,
				  float fRadius,
				  float fOrientation,
				  PpImage &ppImgOut,
					  unsigned char *pucRGB = 0,	// color
					  float fAlphaValue = 1.0f,	// alpha blending value
					  PpImage *ppImgSrc = 0 // source image for blending
				  );

int
of_paint_line(
					  int iRow1,
					  int iCol1,
					  int iRow2,
					  int iCol2,
					  PpImage &ppImgOut,
					  unsigned char *pucRGB = 0,	// color
					  float fAlphaValue = 1.0f,	// alpha blending value
					  PpImage *ppImgSrc = 0 // source image for blending
			 );


int
of_paint_line_length_orientation(
			  float fRow,
			  float fCol,
			  float fLength,
			  float fOrientation,
			  PpImage &ppImgOut,
					  unsigned char *pucRGB = 0,	// color
					  float fAlphaValue = 1.0f,	// alpha blending value
					  PpImage *ppImgSrc = 0 // source image for blending
			 );

int
output_lvarray_slices(
				  FEATUREIO &fio,
				  LOCATION_VALUE_XYZ_ARRAY &lva,
				  int iFeature,				// Index of feature to ouput
				  char *pcFileNameBase,
				  int iPoints = -1
			   );

void
output_lvcollection_slices(
				  FEATUREIO &fio1,
				  FEATUREIO &fio2,
				  LOCATION_VALUE_XYZ_COLLECTION &lvcMarginals,	// Array of likelihoods in fio2
				  char *pcFileNameBase
				  );

// Outputs all points in lva onto a 2D image. Note: fio.z must be 1 (2D images only)
int
output_lvarray_points_2d(
				  FEATUREIO &fio,
				  LOCATION_VALUE_XYZ_ARRAY &lva,
				  int iFeature,				// Index of feature to ouput
				  char *pcFileNameBase
			   );

// Outputs all points in lva onto a 3D image (3 slices)
int
output_lvarray_points(
				  FEATUREIO &fio,
				  LOCATION_VALUE_XYZ_ARRAY &lva,
				  int iFeature,				// Index of feature to ouput
				  char *pcFileNameBase
			   );

// Output keypoint images
int
output_keypoint_image(
		   PpImage &ppImg,
		   unsigned char *pucDesc
		   );

#endif

