
#include <math.h>
#include <assert.h>

#include "BilinearInterpolation.h"

//
// bilinear_interpolation_float()
//
// Performs bilinear interpolation on an image of float.
//
int
bilinear_interpolation_float(
							 const PpImage &ppImgFloat,
							 const float &fRow, 
							 const float &fCol,
							 float *pfVector
							 )
{
	float fRowCont;
	float fColCont;
	
	int iRowCoord;
	int iColCoord;
	
	if( fRow < 0.5f )
	{
		iRowCoord = 0;
		fRowCont = 1.0f;
	}
	else if( fRow >= ((float)(ppImgFloat.Rows() - 0.5f)) )
	{
		iRowCoord = ppImgFloat.Rows() - 2;
		fRowCont = 0.0f;
	}
	else
	{
		float fMinusHalf = fRow - 0.5f;
		iRowCoord = (int)floor( fMinusHalf );
		fRowCont = 1.0f - (fMinusHalf - ((float)iRowCoord));
	}
	
	if( fCol < 0.5f )
	{
		iColCoord = 0;
		fColCont = 1.0f;
	}
	else if( fCol >= ((float)(ppImgFloat.Cols() - 0.5f)) )
	{
		iColCoord = ppImgFloat.Cols() - 2;
		fColCont = 0.0f;
	}
	else
	{
		float fMinusHalf = fCol - 0.5f;
		iColCoord = (int)floor( fMinusHalf );
		fColCont = 1.0f - (fMinusHalf - ((float)iColCoord));
	}

	int iFeaturesPerVector = (ppImgFloat.BitsPerPixel()/8)/sizeof(float);
	for( int i = 0; i < iFeaturesPerVector; i++ )
	{
		float *pfRow0 = (float*)ppImgFloat.ImageRow(iRowCoord);
		float *pfRow1 = (float*)ppImgFloat.ImageRow(iRowCoord+1);

		float f00 = pfRow0[(iColCoord)  *iFeaturesPerVector+i];
		float f01 = pfRow0[(iColCoord+1)*iFeaturesPerVector+i];
		float f10 = pfRow1[(iColCoord)  *iFeaturesPerVector+i];
		float f11 = pfRow1[(iColCoord+1)*iFeaturesPerVector+i];
		
		pfVector[i] = fRowCont*(f00*fColCont + f01*(1.0f - fColCont))
			+ (1.0f - fRowCont)*(f10*fColCont + f11*(1.0f - fColCont));
	}

	return 1;
}

int
bilinear_interpolation_RGB(
							const GenericImage &ppImgRGB,
							const float &fRow, 
							const float &fCol,
							unsigned char *pucRGB
)
{
	assert( ppImgRGB.BitsPerPixel() == 24 );

	float fRowCont;
	float fColCont;
	
	int iRowCoord;
	int iColCoord;
	
	if( fRow < 0.5f )
	{
		iRowCoord = 0;
		fRowCont = 1.0f;
	}
	else if( fRow >= ((float)(ppImgRGB.Rows() - 0.5f)) )
	{
		iRowCoord = ppImgRGB.Rows() - 2;
		fRowCont = 0.0f;
	}
	else
	{
		float fMinusHalf = fRow - 0.5f;
		iRowCoord = (int)floor( fMinusHalf );
		fRowCont = 1.0f - (fMinusHalf - ((float)iRowCoord));
	}
	
	if( fCol < 0.5f )
	{
		iColCoord = 0;
		fColCont = 1.0f;
	}
	else if( fCol >= ((float)(ppImgRGB.Cols() - 0.5f)) )
	{
		iColCoord = ppImgRGB.Cols() - 2;
		fColCont = 0.0f;
	}
	else
	{
		float fMinusHalf = fCol - 0.5f;
		iColCoord = (int)floor( fMinusHalf );
		fColCont = 1.0f - (fMinusHalf - ((float)iColCoord));
	}

	int iFeaturesPerVector = 3;
	for( int i = 0; i < iFeaturesPerVector; i++ )
	{
		unsigned char *pucRow0 = ppImgRGB.ImageRow(iRowCoord);
		unsigned char *pucRow1 = ppImgRGB.ImageRow(iRowCoord+1);

		float f00 = pucRow0[(iColCoord)  *iFeaturesPerVector+i];
		float f01 = pucRow0[(iColCoord+1)*iFeaturesPerVector+i];
		float f10 = pucRow1[(iColCoord)  *iFeaturesPerVector+i];
		float f11 = pucRow1[(iColCoord+1)*iFeaturesPerVector+i];
		
		pucRGB[i] = fRowCont*(f00*fColCont + f01*(1.0f - fColCont))
			+ (1.0f - fRowCont)*(f10*fColCont + f11*(1.0f - fColCont));
	}

	return 1;
}

//
// bilinear_interpolation_char()
//
// Performs bilinear interpolation on an image of char.
//
float
bilinear_interpolation_char(
							const PpImage &ppImgChar,
							const float &fRow, 
							const float &fCol
							)
{
	float fRowCont;
	float fColCont;
	
	int iRowCoord;
	int iColCoord;
	
	if( fRow < 0.5f )
	{
		iRowCoord = 0;
		fRowCont = 1.0f;
	}
	else if( fRow >= ((float)(ppImgChar.Rows() - 0.5f)) )
	{
		iRowCoord = ppImgChar.Rows() - 2;
		fRowCont = 0.0f;
	}
	else
	{
		float fMinusHalf = fRow - 0.5f;
		iRowCoord = (int)floor( fMinusHalf );
		fRowCont = 1.0f - (fMinusHalf - ((float)iRowCoord));
	}
	
	if( fCol < 0.5f )
	{
		iColCoord = 0;
		fColCont = 1.0f;
	}
	else if( fCol >= ((float)(ppImgChar.Cols() - 0.5f)) )
	{
		iColCoord = ppImgChar.Cols() - 2;
		fColCont = 0.0f;
	}
	else
	{
		float fMinusHalf = fCol - 0.5f;
		iColCoord = (int)floor( fMinusHalf );
		fColCont = 1.0f - (fMinusHalf - ((float)iColCoord));
	}
	
	float f00 = ((unsigned char*)(ppImgChar.ImageRow(iRowCoord)))[iColCoord];
	float f01 = ((unsigned char*)(ppImgChar.ImageRow(iRowCoord)))[iColCoord+1];
	float f10 = ((unsigned char*)(ppImgChar.ImageRow(iRowCoord+1)))[iColCoord];
	float f11 = ((unsigned char*)(ppImgChar.ImageRow(iRowCoord+1)))[iColCoord+1];
	
	return fRowCont*(f00*fColCont + f01*(1.0f - fColCont))
		+ (1.0f - fRowCont)*(f10*fColCont + f11*(1.0f - fColCont));
}

//
// bilinear_interpolation_paint_float()
//
// Paints a pixel on an image using bilinear interpolation.
//
int
bilinear_interpolation_paint_float(
								   PpImage &ppImgFloat,
								   const float &fValue,
								   const float &fRow,
								   const float &fCol
								   )
{
	float fRowCont;
	float fColCont;
	
	int iRowCoord;
	int iColCoord;
	
	if( fRow < 0.5f )
	{
		iRowCoord = 0;
		fRowCont = 1.0f;
	}
	else if( fRow >= ((float)(ppImgFloat.Rows() - 0.5f)) )
	{
		iRowCoord = ppImgFloat.Rows() - 2;
		fRowCont = 0.0f;
	}
	else
	{
		float fMinusHalf = fRow - 0.5f;
		iRowCoord = (int)floor( fMinusHalf );
		fRowCont = 1.0f - (fMinusHalf - ((float)iRowCoord));
	}
	
	if( fCol < 0.5f )
	{
		iColCoord = 0;
		fColCont = 1.0f;
	}
	else if( fCol >= ((float)(ppImgFloat.Cols() - 0.5f)) )
	{
		iColCoord = ppImgFloat.Cols() - 2;
		fColCont = 0.0f;
	}
	else
	{
		float fMinusHalf = fCol - 0.5f;
		iColCoord = (int)floor( fMinusHalf );
		fColCont = 1.0f - (fMinusHalf - ((float)iColCoord));
	}
	
	if( fRowCont*fColCont > 0.0f )
	{
		((float*)(ppImgFloat.ImageRow(iRowCoord)))[iColCoord] +=
			fRowCont*fColCont*fValue;
	}
	if( fRowCont*(1.0f - fColCont) > 0.0f )
	{
		((float*)(ppImgFloat.ImageRow(iRowCoord)))[iColCoord+1] +=
			fRowCont*(1.0f - fColCont)*fValue;
	}
	if( (1.0f - fRowCont)*fColCont > 0.0f )
	{
		((float*)(ppImgFloat.ImageRow(iRowCoord+1)))[iColCoord] +=
			(1.0f - fRowCont)*fColCont*fValue;
	}
	if( (1.0f - fRowCont)*(1.0f - fColCont) > 0.0f )
	{
		((float*)(ppImgFloat.ImageRow(iRowCoord+1)))[iColCoord+1] +=
			(1.0f - fRowCont)*(1.0f - fColCont)*fValue;
	}
	
	return 1;
}
