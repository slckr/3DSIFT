
#ifndef __HOMOGRAPHY_H__
#define __HOMOGRAPHY_H__

class Point
{
public:

	double x,y,z;

	Point()
	{
		x=y=z=0.0;
	}
	Point(double xx,double yy, double zz)
	{
		x=xx;
		y=yy;
		z=zz;
	}

	//
	// Cross product of two vectors. 
	// Returns a vector perpendicular to the plane spanned by the two vectors
	//
	inline void
	CrossProduct(
		Point v1,
		Point v2
	) 
	{ 
		x=v1.y*v2.z-v1.z*v2.y;
		y=v1.z*v2.x-v1.x*v2.z;
		z=v1.x*v2.y-v1.y*v2.x;
	}

	inline void
	ScalarMultiply(
		Point v,
		double l
	)
	{
		x=l*v.x;
		y=l*v.y;
		z=l*v.z;
	}
};

class Matrix
{
public: 
	double array[3][3];
};

inline double
DotProduct(
		   Point v1,
		   Point v2
		   )
{
	double res;

	res=v1.x*v2.x+v1.y*v2.y+v1.z*v2.z;
	return res;
}

inline double
DistSqrEuclidean(
		   Point v1,
		   Point v2
		   )
{
	double dx = v1.x-v2.x;
	double dy = v1.y-v2.y;
	double dz = v1.z-v2.z;
	double res = dx*dx+dy*dy+dx*dz;
	return res;
}

void
MatrixMultiply(
			   Matrix m1,
			   Matrix m2,
			   Matrix &result
			   );

//
// Compute the homography by decomposing to the unit square
// We can compute the inverse of a matrix as InvM=AdjunctM/detM. This implies mostly
// multiplications. Also since it is a projective transformation we can discard the 
// computation of det M
//
int
ComputeHomography4Points(
						 Point i1[4],
						 Point i2[4],
						 Matrix &H
						 );

void
MatrixPoint(
			Matrix &H,
			double x1,
			double y1,
			double &x2,
			double &y2
			);


int
ComputeFundamentalMatrix8Points(
						 Point *i1,
						 Point *i2,
						 Matrix &F,
						 int iPoints
						 );


#endif
