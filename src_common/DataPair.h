
//
// Description: many applications require sorted lists of index/value pairs.
// That's what this module does oes oes.
//

#ifndef __DATAPAIR_H__
#define __DATAPAIR_H__

typedef struct _DATA_PAIR
{
	int iIndex;
	int iValue;
} DATA_PAIR;

//
// dpSortAscending()
//
// Sort an array of DATA_PAIR in ascending order of value.
//
int
dpSortAscending(
				DATA_PAIR *pdp,
				int iCount
			);

//
// dpSortAscending()
//
// Sort an array of DATA_PAIR in decending order of value.
//
int
dpSortDescending(
				DATA_PAIR *pdp,
				int iCount
			);

#endif
