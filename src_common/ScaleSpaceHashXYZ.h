

#ifndef __ScaleSpaceXYZHASH_H__
#define __ScaleSpaceXYZHASH_H__

//
// Description:
//    This is a scale-space hash table for efficiently enumerating
//  clusters in (x,y,scale) space. For each division in two by 
//  scale, the x,y dimension reduces by half.
//
//

#include "XYZArray.h"

#pragma warning(disable:4786)

#include <list>

using namespace std;


// Each bin in the hash table is a list
// of indices.
typedef list<int> ScaleSpaceXYZBin;

class ScaleSpaceXYZIterator
{
public:
	virtual int
	Process(
		ScaleSpaceXYZBin *pssbCenter,
		ScaleSpaceXYZBin *pssbNeighbour
		) = 0;
};

class ScaleSpaceXYZHash
{
public:

	ScaleSpaceXYZHash();

	~ScaleSpaceXYZHash();

	//
	// Read() and Write(): File IO.
	//
	int
	Read(
		char *pcFileName
		);
	int
	Write(
		char *pcFileName
		);

	int
	Copy(
		ScaleSpaceXYZHash &pssh
	);

	//
	// Init()
	// Initialize the hash table.
	//
	int
	Init(
		int iDim,
		float *pfMin, // Min/Max/increment values for each dimension
		float *pfMax,
		float *pfIncPerScale, // Increment value per scale (e.g. 0.5)

		float fScaleMin, // Min/Max scale - pixels, this is unused, max scale is calculated from fMinS and fScaleBinMultiple
		float fScaleMax, 
		float fScaleInc
	);

	//
	// InitFromExample()
	//
	// Initialize hash table dimensions from an example.
	//
	int
	InitOther(
		ScaleSpaceXYZHash &sshOther
	);

	//
	// CreateFrequencyImage()
	//
	// Create an image with bin frequencies, for the ArrayXYZ at scale fScale.
	//
	int
	CreateFrequencyImage(
		float fScale,
		float *pfFreqImg
		);

	// Return scale space bin for coordinates x,y,z
	ScaleSpaceXYZBin *
	GetBin(
		float *pfXYZ,
		float fScale
		);

	ScaleSpaceXYZBin *
	GetNeighborBin(
			float *pfXYZ,
			float fScale,
			int iScaleInc, // Scale neighbor
			int	*piNeighborInc // Neighbor bin
			);

	//
	// Insert()
	//
	// Insert a value into the hash table.
	//
	int
	Insert(
		float *pfXYZ,
		float fScale,
		int iValue
		);

	//
	// EmptyAllBins()
	//
	// Empties all bins
	//
	int
	EmptyAllBins(
	);

	
	//
	// GetScaleBinCount()
	//
	// Returns the number of scale bins. For interating
	// through the hash table.
	//
	int
	GetScaleBinCount(
		int &iScaleBinCount
	);

	//
	// GetXYZBinCounts()
	//
	// Gets the number of XYZ bins at scale iScaleBin. For interating
	// through the hash table.
	//
	int
	GetXYZBinCounts(
		const int iScaleBin,
		int *piXYZBinCounts
	);


	//
	// GetBinFromIndex()
	//
	// Returns a bin based on its index.
	//
	ScaleSpaceXYZBin *
	GetBinFromIndex(
			int iScaleBin,
			int *piXYZBins
			);

	ScaleSpaceXYZBin *
	GetNeighborBinFromIndex(
		int iScaleBin, int *piXYZBins,
		int iScaleInc, int *piNeighborInc,
		int &iScaleBinNew, int *piXYZBinsNew 
		);

	float
	GetBinScaleFromIndex(
		int iScaleBin
		);

private:

	// Hash table size information

	XYZArray	*m_paArrays;

	float	m_fScaleMin;
	float	m_fScaleMax;
	int		m_iScaleLen;
	float	m_fScaleMult; // Multiplicative increment

	float	m_pfIncPerScale[10]; // distance increment per unit of scale, one for each dimension (no more than 10 dimensions?)

	float	m_fScaleMinLog; // Logarithmic sum increment
	float	m_fScaleMaxLog; // Logarithmic sum increment
	float	m_fScaleIncLog; // Logarithmic sum increment
};

#endif
