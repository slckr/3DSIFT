
#include <math.h>
#include <assert.h>
#include "FishersExactTest.h"

int
FishersExactTest::Init( 
		int iMaxCount
		)
{
	// Precompute the log of factorials

	m_vfLogIntFactorial.resize( iMaxCount + 1 );

	float fLogFact = 0;
	for( int i = 0; i <= iMaxCount; i++ )
	{
		if( i > 0 )
		{
			fLogFact += log( (float)i );
		}
		m_vfLogIntFactorial[i] = fLogFact;
	}
	return 1;
}

float
FishersExactTest::OneSidedPValue(
		int a11,
		int a12,
		int a21,
		int a22
		)
{
	assert( a11 >= 0 );
	assert( a12 >= 0 );
	assert( a21 >= 0 );
	assert( a22 >= 0 );
	int a1x = a11+a12;
	int a2x = a21+a22;
	int ax1 = a11+a21;
	int ax2 = a12+a22;
	int axx = a11+a12+a21+a22;

	int b11;
	int b12;
	int b21;
	int b22;

	// Sum out tail to complete independence
	float fTailDiag = 0;
	b11 = a11;
	b12 = a12;
	b21 = a21;
	b22 = a22;
	while( b12 >= 0 && b21 >= 0 )
	{
		float fNum = m_vfLogIntFactorial[a1x] + m_vfLogIntFactorial[a2x] + m_vfLogIntFactorial[ax1] + m_vfLogIntFactorial[ax2];
		float fDen = m_vfLogIntFactorial[b11] + m_vfLogIntFactorial[b12] + m_vfLogIntFactorial[b21] + m_vfLogIntFactorial[b22]
				+ m_vfLogIntFactorial[axx];
		fTailDiag += exp( fNum - fDen );

		// Subtract 1 from cross diagonal, add 1 to diagonal
		b11++;	b12--;
		b21--;	b22++;
	}

	float fTailCrossDiag = 0;
	b11 = a11;
	b12 = a12;
	b21 = a21;
	b22 = a22;
	while( b11 >= 0 && b22 >= 0 )
	{
		float fNum = m_vfLogIntFactorial[a1x] + m_vfLogIntFactorial[a2x] + m_vfLogIntFactorial[ax1] + m_vfLogIntFactorial[ax2];
		float fDen = m_vfLogIntFactorial[b11] + m_vfLogIntFactorial[b12] + m_vfLogIntFactorial[b21] + m_vfLogIntFactorial[b22]
				+ m_vfLogIntFactorial[axx];
		fTailCrossDiag += exp( fNum - fDen );

		// Subtract 1 from diagonal, add 1 to cross diagonal
		b11--;	b12++;
		b21++;	b22--;
	}

	// Return min
	float fMin = fTailDiag < fTailCrossDiag ? fTailDiag : fTailCrossDiag;

	return fMin;
}


float
FishersExactTest::ExactValue(
		int a11,
		int a12,
		int a21,
		int a22
		)
{
	assert( a11 >= 0 );
	assert( a12 >= 0 );
	assert( a21 >= 0 );
	assert( a22 >= 0 );
	int a1x = a11+a12;
	int a2x = a21+a22;
	int ax1 = a11+a21;
	int ax2 = a12+a22;
	int axx = a11+a12+a21+a22;

	int b11;
	int b12;
	int b21;
	int b22;

	// Sum out tail to complete independence
	float fTailDiag = 0;
	b11 = a11;
	b12 = a12;
	b21 = a21;
	b22 = a22;

	float fNum = m_vfLogIntFactorial[a1x] + m_vfLogIntFactorial[a2x] + m_vfLogIntFactorial[ax1] + m_vfLogIntFactorial[ax2];
	float fDen = m_vfLogIntFactorial[b11] + m_vfLogIntFactorial[b12] + m_vfLogIntFactorial[b21] + m_vfLogIntFactorial[b22]
	+ m_vfLogIntFactorial[axx];

	float fMin = exp( fNum - fDen );

	return fMin;
}

