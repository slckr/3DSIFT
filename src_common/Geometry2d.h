
#ifndef __GEOMETRY2D_H__
#define __GEOMETRY2D_H__

// A point is represented by two floats
typedef float g2dPoint[2];

// A line is represented by two points, to avoid infinite slope problems
typedef g2dPoint g2dLine[2];

// A circle is represented by a center point plus a radius
typedef float g2dCircle[3];

//
// Interesting: note that a circle inherets from a point in a c++ sense:
//
// class point{ float x; float y };
// class circle : point { float radius; };
//

int
g2dMakeLine(
			float *pfDxDy, // float array dx dy
			float *pfXY, // float array x y 
			g2dLine g2dLine // line
			);

// Find the intersection of a line and a circle
int
g2dInterceptLineCircle(
					   g2dLine line,
					   g2dCircle circle,
					   g2dPoint p1,
					   g2dPoint p2
			);

// Find the squared distance between two points
int
g2dDistance(
					   g2dPoint p1,
					   g2dPoint p2,
					   float *pfDist
			);

// Find the intersection of two lines (could be infinity...)
int
g2dInterceptLineLine(
					   g2dLine line1,
					   g2dLine line2,
					   g2dPoint p1
			);

// Find the two roots of the quadratic ax^2 + bx^1 + cx^0 = 0
int
g2dSolveQuad(
			 float a,
			 float b,
			 float c,
			 float *px1,	// root 1
			 float *px2		// root 2
			 );

// Find the area of overlap between two circles
// I don't believe this works...
int
g2dCircleOverlapArea(
					 g2dCircle *pc1,
					 g2dCircle *pc2,
					 float *pArea
					 );

float
g2dCircleOverlapArea(
			  float x1, float y1, float r1,
			  float x2, float y2, float r2
			  );

#endif

