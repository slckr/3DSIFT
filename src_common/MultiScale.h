
#ifndef __MULTISCALE_H__
#define __MULTISCALE_H__

#pragma warning(disable:4786)

#include <vector>

using namespace std;

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include "FeatureIO.h"

//
// m_uiInfo field
// Provide info regarding the features
// 0xFFFFFFFF
//

// Headers for feature files: there is a version number
//#define FEAT3DHEADER "FEAT3D000"
//#define FEAT2DHEADER "FEAT2D000"

// Version 001: 48 principal components added to info file
#define FEAT3DHEADER "FEAT3D001"
#define FEAT2DHEADER "FEAT2D001"

//
// Feature3D
// A 3D scale-invariant feature.
//

//#define FEATURE_3D_DIM 11

class Feature3DChar;
class Feature3DShort;
class Feature3D;

// Defines wether feature corresponds to a minima (0) or maxima (1)
#define INFO_FLAG_MIN0MAX1 0x00000010
// Defines wether feature appearance has been reoriented, yes (1) or no (0)
#define INFO_FLAG_REORIENT 0x00000020
// Defines wether feature is a line feature (1) or not (0)
// In the case of a line feature, the ori field contains the coordinates
// of the 2nd point
#define INFO_FLAG_LINE     0x00000100

// 12:34 PM 4/27/2011
// Make array big enough to store gradient orientation histograms spatial locations=(2x2x2), orientations=(8)
#define PC_ARRAY_SIZE 64

typedef float PC_ARRAY[PC_ARRAY_SIZE];

class Feature3DInfo
{

public:

	Feature3DInfo();

	// 
	// Visualization functions
	//
	int
		OutputVisualFeature(
			char *pcFileNameBase,
			int bOneImage = 1 // Output 3 planes in 1 image
		) {
		return 1;
	}

	int
		OutputVisualFeatureInVolume(
			FEATUREIO &fioImg,
			char *pcFileNameBase,
			int bShowFeats = 0,
			int bOneImage = 1
		);

	int
		PaintVisualFeatureInVolume(
			FEATUREIO &fioImg,
			int bWhite
		);

	void
		InitFromFeature3D(
			Feature3D &feat3D
		);

	float
		DistSqrPCs(
			const Feature3DInfo &feat3D,
			int iPCs = PC_ARRAY_SIZE
		) const;

	float
		DistSqrXYZ(
			const Feature3DInfo &feat3D
		) const;

	// Rank order only PC components
	void
		NormalizeDataRankedPCs(
		);

	void
		NormalizeDataPositivePCs(
		);

	void
		ZeroData(
		);

	void
		SimilarityTransform(
			float *pfCenter0,
			float *pfCenter1,
			float *rot01,
			float fScaleDiff
		);

	// Apply similarity transform in matrix form
	void
		SimilarityTransform(
			float *pfMat4x4
		);

	//
	// MirrorFeatureY()
	//
	// Mirror feature across Y axis;
	//
	void
		MirrorFeatureY(
		);

	//
	//
	//
	//
	//
	int
		DeepCopy(
			Feature3DInfo &feat3D
		)
	{
		m_uiInfo = feat3D.m_uiInfo;
		x = feat3D.x;
		y = feat3D.y;
		z = feat3D.z;
		scale = feat3D.scale;
		memcpy(&(m_pfPC[0]), &(feat3D.m_pfPC[0]), sizeof(m_pfPC));
		memcpy(&(ori[0][0]), &(feat3D.ori[0][0]), sizeof(ori));
		memcpy(&(eigs[0]), &(feat3D.eigs[0]), sizeof(eigs));
		return 0;
	}

	// Information regarding this feature
	unsigned int	m_uiInfo;

	// Location
	float x;
	float y;
	float z;
	// Scale
	float scale;
	// Orientation axes (major to minor)
	float ori[3][3];
	// Eigenvalues (major to minor)
	float eigs[3];

	// Insert principal components into info .. it is very small compared to 
	// size of data_zyx ...
	static const int FEATURE_3D_PCS = PC_ARRAY_SIZE;
	// Data fields: added to support different data descriptors
	float m_pfPC[FEATURE_3D_PCS];
};

//
//
//
//
class Feature3DStringInfo : public Feature3DInfo
{

public:

	Feature3DStringInfo() {};

	// Scale
	float scaleMin;
	float scaleMax;
	int iScaleCount;

};

//
// compatible_features()
//
// Return 1 if f1 and f2 are geometrically similar, 0 otherwise.
//
//
#define LOG_1_5 0.4054651
//
int
compatible_features(
	const Feature3DInfo &f1,
	const Feature3DInfo &f2,
	//float fScaleDiffThreshold = (float)log(1.5f), // works the best, MICCAI 2009 results
	float fScaleDiffThreshold = LOG_1_5,
	float fShiftThreshold = 0.5, // works the best, MICCAI 2009 results
	float fCosineAngleThreshold = -1.0f // Allow all all angles by default
);

class Feature3DData
{

public:
	// Image sample dimension
	static const int FEATURE_3D_DIM = 11;

	//unsigned int	m_uiInfo; // Information regarding this feature

	//// Location
	//float x;
	//float y;
	//float z;
	//// Scale
	//float scale;
	//// Orientation axes (major to minor)
	//float ori[3][3];
	//// Eigenvalues (major to minor)
	//float eigs[3];
	// Image sample

	int
		OutputVisualFeature(
			char *pcFileNameBase
		);

	float data_zyx[FEATURE_3D_DIM][FEATURE_3D_DIM][FEATURE_3D_DIM];

	//
	// OrientationInvariantDescriptor()
	//
	// Determine orientation invariant descriptor.
	//
	void
		OrientationInvariantDescriptor(
			int &iCode
		);

	void
		DeterminePartialOrientationCube(
			int &iCode, // Returns a code, unique number describing the orientation
			float *ori = 0// 3x3 rotation matrix
		);
	void
		DetermineOrientationCube(
			int &iCode, // Returns a code, unique number describing the orientation
			float *ori = 0// 3x3 rotation matrix
		);
	void
		DetermineOrientationCubeIntensity(
			int &iCode, // Returns a code, unique number describing the orientation
			float *ori = 0// 3x3 rotation matrix
		);

	void
		DetermineOrientation3DNoGood(

			int iCode = 0
		);

	//
	// Only keep 3 orthogonal planes
	//
	void
		PlanesDescriptor(
			int &iCode
		);

	//
	// Keep small data cube ... 
	//
	void
		CubeDescriptor(
			int &iCode
		);



	//
	// DetermineOrientationInvDesc()
	//
	// Investigate descriptors.
	//
	void
		DetermineOrientationInvDesc(
			int iCode
		);

	//
	// Tried to do stuff with azimuth/elevation, doesn't work...
	// problems with normalizing tilt & elevation...
	//
	//void
	//DetermineOrientationAzEl(
	//								int iCode
	//);

	void
		DetermineOrientationXYProjection(
			int iCode
		);

	//
	// DetermineOrientationGenerateAzimuthElevationTable()
	//
	// Output a lookup table for normalization.
	//
	void
		DetermineOrientationGenerateAzimuthElevationTable(
			int iCode
		);


};

class Feature3D : public Feature3DInfo, public Feature3DData
{
public:

	Feature3D() {};
	~Feature3D() {};

	void
		InitFromFeature3D(
			Feature3D &feat3D
		);

	int
		InitFromInfoData(
			Feature3DInfo &f3dInfo,
			Feature3DData &f3dData
		);

	int
		InitFromFeature3DInfo(
			Feature3DInfo &feat3DInfo
		);

	int
		InitFromFeature3DShort(
			Feature3DShort &feat3DShort
		);

	int
		ToFileBin(
			FILE *outfile,	// Initalized
			int bAppearance = 1
		);
	int
		FromFileBin(
			FILE *infile,	// Initalized "rb"
			int iFeatureSize // Size of feature to read
		);

	//
	// FromFileBinOld()
	//
	// Backwards compatible read function.
	//
	int
		FromFileBinOld(
			FILE *infile	// Initalized "rb"
		);

	//
	// Statistical functions
	//
	int
		ZeroData(
		);

	int
		AccumulateData(
			Feature3D &feat
		);
	int
		AccumulateDataSqr(
			Feature3D &feat,
			Feature3D &featMean
		);
	int
		CalculateMeanVarr(
			Feature3D &featAccumData,
			Feature3D &featAccumDataSqr,
			int iSamples
		);

	// 
	// Visualization functions
	//
	int
		OutputVisualFeature(
			char *pcFileNameBase,
			int bOneImage = 1 // Output 3 planes in 1 image
		);

	int
		OutputVisualFeatureInVolume(
			FEATUREIO &fioImg,
			char *pcFileNameBase,
			int bShowFeats = 0,
			int bOneImage = 1
		);

	//
	// DistSqr()
	//
	// Compute the Euclidean distance between this feature and feat3D.
	//
	float
		DistSqr(
			const Feature3D &feat3D
		) const;
	// Compute the Mahalanobis distance given multiplicative (inverted) feature-wise variance
	float
		DistSqr(
			const Feature3D	&feat3D,
			const Feature3D	&featVarrInv
		) const;

	// Consider only a fixed feature count
	float
		DistSqr(
			const Feature3D &feat3D,
			int iFeatCount
		) const;

	//
	// DotProduct()
	//
	// Compute dot product between this feature & others
	//
	float
		DotProduct(
			const Feature3D &feat3D
		) const;

	//
	// GaussianWeight()
	//
	// Apply Gaussian weighting to focus most importance on 
	// central voxels.
	//
	void
		GaussianWeight(
			float fSigma = 8.0f
		);

	//
	// RandomAppearance()
	//
	// Set random appearance vector xyz_data .
	//
	void
		RandomAppearance(
		);

	//
	// NormalizeData()
	//
	// Normalize data vector to unit Euclidean length.
	// Subtract mean.
	//
	// Optionally, return mean & varr of original data.
	//
	void
		NormalizeData(
			float *pfMean = 0,
			float *pfVarr = 0,
			float *pfMin = 0,
			float *pfMax = 0
		);

	//
	// Take 1st derivative of PC vector (useful when intensity profiles?)
	//
	void
		DiffPCs(
			int iPCs
		);

	//
	// ProjectToPCs()
	//
	// Project feature data to principal components in vecPCs.
	// Feature data in vecPCs should be zero mean and unit length.
	// If pfPCArray is null, projection coefficients are stored in this feature.
	//
	void
		ProjectToPCs(
			vector<Feature3D>	&vecPCs,
			int iPCs = FEATURE_3D_PCS,
			float *pfPCArray = 0
		);

	//
	// NormalizeData()
	//
	// Normalize data vector to unit Euclidean length.
	// Do not subtract mean.
	//
	void
		NormalizeDataPositive(
		);

	//
	// NormalizeDataRanked()
	//
	// Normalize data by rank-ordering.
	//
	void
		NormalizeDataRanked(
		);

	int
		RotateData(
			float *ori
		);

	//// Image sample dimension
	//static const int FEATURE_3D_DIM = 11;

	////unsigned int	m_uiInfo; // Information regarding this feature

	////// Location
	////float x;
	////float y;
	////float z;
	////// Scale
	////float scale;
	////// Orientation axes (major to minor)
	////float ori[3][3];
	////// Eigenvalues (major to minor)
	////float eigs[3];
	//// Image sample
	//float data_zyx[FEATURE_3D_DIM][FEATURE_3D_DIM][FEATURE_3D_DIM];
};



class Feature3DShort : public Feature3DInfo
{
public:

	Feature3DShort() {};
	~Feature3DShort() {};

	//
	// InitFromFeature3D()
	//
	// Convert data to signed character format.
	//
	// Input Feature3D must be normalized to unit length.
	//
	int
		InitFromFeature3D(
			Feature3D &feat3D
		)
	{
		x = feat3D.x;
		y = feat3D.y;
		z = feat3D.z;

		scale = feat3D.scale;

		ori[0][0] = feat3D.ori[0][0];
		ori[0][1] = feat3D.ori[0][1];
		ori[0][2] = feat3D.ori[0][2];
		ori[1][0] = feat3D.ori[1][0];
		ori[1][1] = feat3D.ori[1][1];
		ori[1][2] = feat3D.ori[1][2];
		ori[2][0] = feat3D.ori[2][0];
		ori[2][1] = feat3D.ori[2][1];
		ori[2][2] = feat3D.ori[2][2];

		eigs[0] = feat3D.eigs[0];
		eigs[1] = feat3D.eigs[1];
		eigs[2] = feat3D.eigs[2];

		//float fMax = -1, fMin = 1;
		//short cMax = -127, cMin = 127;
		float fNorm = 0;
		float fMean = 0;
		for (int x = 0; x < FEATURE_3D_DIM; x++)
		{
			for (int y = 0; y < FEATURE_3D_DIM; y++)
			{
				for (int z = 0; z < FEATURE_3D_DIM; z++)
				{
					data_zyx[x][y][z] = (signed short)(32767.0*feat3D.data_zyx[x][y][z]);
					//if( data_zyx[x][y][z] > cMax ) cMax = data_zyx[x][y][z];
					//if( data_zyx[x][y][z] < cMin ) cMin = data_zyx[x][y][z];
					fNorm += data_zyx[x][y][z] * data_zyx[x][y][z];
					fMean += data_zyx[x][y][z];
				}
			}
		}
		return 0;
	}

	int
		ToFileBin(
			FILE *outfile,	// Initalized "wb"
			int bAppearance = 1
		);
	int
		FromFileBin(
			FILE *infile	// Initalized "rb"
		);

	int
		OutputVisualFeature(
			char *pcFileNameBase
		);

	int
		OutputVisualFeatureInVolume(
			FEATUREIO &fioImg,
			char *pcFileNameBase,
			int bShowFeats = 0,
			int bOneImage = 1
		);

	//
	// DistSqr()
	//
	// Compute the Euclidean distance between this feature and feat3D.
	//
	float
		DistSqr(
			const Feature3DShort &feat3D
		) const;

	//
	// DistSqrFlipX()
	//
	// Compute the Euclidean distance between this feature and feat3D with the x
	// dimension mirrored at 0=x.
	//
	float
		DistSqrFlipX(
			const Feature3DShort &feat3D
		) const;

	// Compute the Mahalanobis distance given multiplicative (inverted) feature-wise variance
	float
		DistSqr(
			const Feature3DShort &feat3D,
			const Feature3D		  &featVarrInv
		) const;

	// Image sample dimension
	static const int FEATURE_3D_DIM = 11;

	//// Location
	//float x;
	//float y;
	//float z;
	//// Scale
	//float scale;
	//// Orientation axes (major to minor)
	//float ori[3][3];
	//// Eigenvalues (major to minor)
	//float eigs[3];
	// Image sample
	signed short data_zyx[FEATURE_3D_DIM][FEATURE_3D_DIM][FEATURE_3D_DIM];
};

class Feature3DChar : public Feature3DInfo
{
public:

	Feature3DChar() {};
	~Feature3DChar() {};

	//
	// InitFromFeature3D()
	//
	// Convert data to signed character format.
	//
	// Input Feature3D must be normalized to unit length.
	//
	int
		InitFromFeature3D(
			Feature3D &feat3D
		)
	{
		x = feat3D.x;
		y = feat3D.y;
		z = feat3D.z;

		scale = feat3D.scale;

		ori[0][0] = feat3D.ori[0][0];
		ori[0][1] = feat3D.ori[0][1];
		ori[0][2] = feat3D.ori[0][2];
		ori[1][0] = feat3D.ori[1][0];
		ori[1][1] = feat3D.ori[1][1];
		ori[1][2] = feat3D.ori[1][2];
		ori[2][0] = feat3D.ori[2][0];
		ori[2][1] = feat3D.ori[2][1];
		ori[2][2] = feat3D.ori[2][2];

		eigs[0] = feat3D.eigs[0];
		eigs[1] = feat3D.eigs[1];
		eigs[2] = feat3D.eigs[2];

		float fMax = -1, fMin = 1;
		char cMax = -127, cMin = 127;
		for (int x = 0; x < FEATURE_3D_DIM; x++)
		{
			for (int y = 0; y < FEATURE_3D_DIM; y++)
			{
				for (int z = 0; z < FEATURE_3D_DIM; z++)
				{
					data_zyx[x][y][z] = (signed char)(127.0*feat3D.data_zyx[x][y][z]);
					if (data_zyx[x][y][z] > cMax) cMax = data_zyx[x][y][z];
					if (data_zyx[x][y][z] < cMin) cMin = data_zyx[x][y][z];
				}
			}
		}
		return 0;
	}

	int
		ToFileBin(
			FILE *outfile,	// Initalized "wt"
			int bAppearance = 1
		);
	int
		FromFileBin(
			FILE *infile	// Initalized "rb"
		);

	int
		OutputVisualFeature(
			char *pcFileNameBase
		);

	int
		OutputVisualFeatureInVolume(
			FEATUREIO &fioImg,
			char *pcFileNameBase,
			int bShowFeats = 0,
			int bOneImage = 1
		);

	//
	// DistSqr()
	//
	// Compute the Euclidean distance between this feature and feat3D.
	//
	float
		DistSqr(
			const Feature3DChar &feat3D
		) const;

	// Image sample dimension
	static const int FEATURE_3D_DIM = 11;

	//// Location
	//float x;
	//float y;
	//float z;
	//// Scale
	//float scale;
	//// Orientation axes (major to minor)
	//float ori[3][3];
	//// Eigenvalues (major to minor)
	//float eigs[3];
	// Image sample
	signed char data_zyx[FEATURE_3D_DIM][FEATURE_3D_DIM][FEATURE_3D_DIM];
};


class Feature2D
{
public:

	Feature2D() {};
	~Feature2D() {};

	int
		ToFileBin(
			FILE *outfile	// Initalized "wb"
		);
	int
		FromFileBin(
			FILE *infile	// Initalized "rb"
		);

	//
	// DistSqr()
	//
	// Compute the Euclidean distance between this feature and feat2D.
	//
	float
		DistSqr(
			Feature2D &feat2D
		);

	//
	// MeanData()
	// MinData()
	// MaxData()
	//
	// Compute data value: min, max, mean.
	//
	float
		MeanData(
		);
	float
		MinData(
		);
	float
		MaxData(
		);

	//
	// NormalizeData()
	//
	// Normalize data vector to a specified Euclidean length,
	// use specified mean and length fMean and fLength.
	//
	// If bAbs is flagged, the absolute value of data elements
	// after subtracting the mean is considered.
	//
	void
		NormalizeData(
			float fMean = 0.0f,
			float fLength = 1.0f,
			int bAbs = 0
		);

	//
	// AbsoluteValueData()
	//
	// Convert data to absolute value - for derivative features.
	//
	void
		AbsoluteValueData(
		);

	int
		OutputVisualFeature(
			char *pcFileNameBase
		);

	//
	// OutputVisualFeatureInImage()
	//
	// Output feature on image fioImg and write to file pcFileNameBase.
	// If pcFileNameBase is null, just output to image.
	//
	int
		OutputVisualFeatureInImage(
			FEATUREIO &fioImg,
			char *pcFileNameBase = 0
		);

	//
	// ZeroData()
	//
	// Set data vector to all zero.
	//
	int
		ZeroData(
		);

	unsigned int	m_uiInfo; // Information regarding this feature

							  // Image sample dimension - enough to hold 127 values
	static const int FEATURE_2D_DIM = 12;

	// Location
	float x;
	float y;
	// Scale
	float scale;
	// Orientation
	float theta;
	// Orientation axes (major to minor)
	float ori[2][2];
	// Eigenvalues (major to minor)
	float eigs[2];
	// Image sample
	float data_yx[FEATURE_2D_DIM][FEATURE_2D_DIM];
};

//
// msResampleFeaturesGradientOrientationHistogram()
//
// Resample feature appearance as gradient orientation histograms. data_xyz must
// contain sampled data.
//
int
msResampleFeaturesGradientOrientationHistogram(
	Feature3D &feat3D
);
// Second attempt with different features
int
msResampleFeaturesGradientOrientationHistogram2(
	Feature3D &feat3D
);

//
// msResampleFeaturesRotationInvariantHistogram()
//
// Resample as a rotation invariant gradient orientation histogram.
//
int
msResampleFeaturesRotationInvariantHistogram(
	Feature3D &feat3D
);

//
// msGeneratePyramidDOG()
//
// Generates a DOG (difference-of-Gaussian) pyramid.
//

int
msGeneratePyramidDOG(
	FEATUREIO	&fioImg
);

int
msGeneratePyramidDOG2D(
	FEATUREIO	&fioImg,
	vector<Feature2D> &vecFeats,
	float fInitialImageScale = 1.0f,
	int bDense = 0,
	int bOutput = 0
);
int
msGeneratePyramidEntropy2D(
	FEATUREIO	&fioImg,
	vector<Feature2D> &vecFeats,
	float fInitialImageScale = 1.0f,
	int bDense = 0,
	int bOutput = 0
);
int
msGeneratePyramidMutualInfo2D(
	FEATUREIO	&fioImg,
	vector<Feature2D> &vecFeats,
	float fInitialImageScale = 1.0f,
	int bDense = 0,
	int bOutput = 0
);
int
msGeneratePyramidMutualInfoSpatialClasses2D(
	FEATUREIO	&fioImg,
	vector<Feature2D> &vecFeats,
	float fInitialImageScale = 1.0f,
	int bDense = 0,
	int bOutput = 0
);



int
msGeneratePyramidGauss2DTriangle(
	FEATUREIO	&fioImg,
	vector<Feature2D> &vecFeats,
	float fInitialImageScale,
	int bDense = 0,
	int bOutput = 0
);


int
msGeneratePyramidDOG3D(
	FEATUREIO	&fioImg,
	vector<Feature3D> &vecFeats,
	float fInitialImageScale = 1.0f,
	int bDense = 0,
	int bOutput = 0,
	float fEigThres = -1 // Eigenvalue threshold, ignore if < 0
);

//
// msGeneratePyramidDOG3D_efficient()
//
// Attempt to be more efficient memorywise
//
int
msGeneratePyramidDOG3D_efficient(
	FEATUREIO	&fioImg,
	vector<Feature3D> &vecFeats,
	float fInitialImageScale = 1.0f,
	int bDense = 0,
	int bOutput = 0,
	float fEigThres = -1 // Eigenvalue threshold, ignore if < 0
);

int
msGeneratePyramidEntropy3D(
	FEATUREIO	&fioImg,
	vector<Feature3D> &vecFeats,
	float fInitialImageScale = 1.0f,
	int bDense = 0,
	int bOutput = 0
);
int
msGeneratePyramidMutualInfo3D(
	FEATUREIO	&fioImg,
	vector<Feature3D> &vecFeats,
	float fInitialImageScale = 1.0f,
	int bDense = 0,
	int bOutput = 0
);

int
msGeneratePyramidHarris3D(
	FEATUREIO	&fioImg,
	vector<Feature3D> &vecFeats,
	float fInitialImageScale = 1.0f,
	int bDense = 0,
	int bOutput = 0
);

//int
//msGeneratePyramidMutualInfoSpatialClasses3D(
//					 FEATUREIO	&fioImg,
//					 vector<Feature3D> &vecFeats
//					 );


//
// msFeature3DVectorOutput()
//
// Output a vector of Feature3D.
//
int
msFeature3DVectorOutput(
	vector<Feature3D> &vecFeats3D,
	char *pcFileName,
	int bAppearance = 1
);

//
// msFeature3DVectorOutputText()
//
// Simple text output. Option to output principal components.
//
//int
//msFeature3DVectorOutputText(
//				  vector<Feature3D> &vecFeats3D,
//				  char *pcFileName,
//				  float fEigThres = -1
//);
//int
//msFeature3DVectorOutputText(
//				  vector<Feature3DInfo> &vecFeats3D,
//				  char *pcFileName,
//				  float fEigThres = -1
//);
//
//int
//msFeature3DVectorInputText(
//				  vector<Feature3DInfo> &vecFeats3D,
//				  char *pcFileName,
//				  float fEigThres = -1
//);
//int
//msFeature3DVectorInputText(
//				  vector<Feature3D> &vecFeats3D,
//				  char *pcFileName,
//				  float fEigThres = -1
//);


//
// msCompatibleFeatures()
//
// Count the number of geometrically compatible/dupicate features in arrays.
//
int
msCompatibleFeatures(
	vector<Feature3D> &vecFeats1,
	vector<Feature3D> &vecFeats2
);

//
// msFeature3DVectorInput()
//
// Read in a vector of Feature3D.
//
int
msFeature3DVectorInput(
	vector<Feature3D> &vecFeats3D,
	char *pcFileName,
	float fEigThres = -1.0f // Threshold on trace/product of eigenvalues, if negative then accept all features
);

int
msFeature3DVectorOutputSIFTFormat(
	vector<Feature3D> &vecFeats3D,
	char *pcFileName,
	int bOutputAppearance
);

int
msFeature2DVectorOutput(
	vector<Feature2D> &vecFeats2D,
	char *pcFileName
);
int
msFeature2DVectorOutputSIFTFormat(
	vector<Feature2D> &vecFeats2D,
	char *pcFileName,
	int bOutputAppearance = 1 // Flag to output appearance vector
);

int
msFeature2DVectorInput(
	vector<Feature2D> &vecFeats2D,
	char *pcFileName,
	float fEigThres = 10.0f // Threshold on trace/product of eigenvalues
);

//
// msGeneratePyramidTwoImgMutualInfo3D()
//
// Two-image MI-based feature detection. fioImg1 and fioImg2 are spatially aligned
// images from different modalities.
//
// Two different versions based on different independence assumptions:
//
// 1) msGeneratePyramidTwoImgMutualInfo3DCondIndepIJ:
//		Assumes intensites are conditionally independent given location x
//
// 2) msGeneratePyramidTwoImgMutualInfo3DCondIndepIJ:
//		Assumes intensites are independent given location x
//
int
msGeneratePyramidTwoImgMutualInfo3DCondIndepIJ(
	FEATUREIO	&fioImg1, // Interleaved features?
	FEATUREIO	&fioImg2,
	vector<Feature3D> &vecFeats,
	float fInitialImageScale,
	int bDense,
	int bOutput
);


int
msGeneratePyramidTwoImgMutualInfo3DIndepIJ(
	FEATUREIO	&fioImg1, // Interleaved features?
	FEATUREIO	&fioImg2,
	vector<Feature3D> &vecFeats,
	float fInitialImageScale,
	int bOutput
);



//
// msBilateralFilter2D256()
//
// Investigate bilateral filter.
// Accepts only 2D images
//
//
int
msBilateralFilter2D256(
	FEATUREIO	&fioImg,
	FEATUREIO	&fioImgOut,
	float fSigmaX, // spatial blur parameter
	float fSigmaI // intensity blur parameter
);

//
// msGenerateScaleSpaceCube()
//
// Generate a scale-space cube just to see...
//
int
msGenerateScaleSpaceCube(
	FEATUREIO	&fioImg,
	FEATUREIO	&fioImgOut,
	float fInitialImageScale,
	float fSamplingRate,
	int bOutput
);

//
// msGenerateScaleSpaceCubeMI()
//
// Generate cube for visualization based on MI
//
int
msGenerateScaleSpaceCubeMI(
	FEATUREIO	&fioImg,
	FEATUREIO	&fioImgOut,
	float fInitialImageScale,
	float fSamplingRate,
	int bOutput
);
//
// msComputeNearestNeighbors()
//
// Matches two sets of 3D features, puts the result in vecMatchIndex12/vecMatchDist12
//
// If iPCs > 0, nearest neighbours are calculated using iPCs principal components
//
int
msComputeNearestNeighbors(
	vector<Feature3D> &vecFeats1,
	vector<Feature3D> &vecFeats2,
	vector<int>		&vecMatchIndex12,
	vector<float>		&vecMatchDist12,
	int iPCs = 0
);

int
msComputeNearestNeighborDistanceRatio(
	vector<Feature3D> &vecFeats1,
	vector<Feature3D> &vecFeats2,
	vector<int>		&vecMatchIndex12,
	vector<float>		&vecMatchDist12,
	int iPCs
);

int
msComputeNearestNeighbors(
	char *pcFeats1,
	char *pcFeats2,
	char *pcPCs = 0
);

//
// msCalculatePC_SVD()
//
// Compute principal components of a of set feature vectors.
//
void
msCalculatePC_SVD(
	vector<Feature3D> &vecFeats,
	vector<Feature3D> &vecFeatPCs
);

//
// determine_rotation_3point()
//
// Determines a rotation matrix which can be used to rotate pf0 in image 1 to 
// pf1 in image 2:
//   mult_3x3<float,float>( rot, pf0, pf1 );
// 
int
determine_rotation_3point(
	float *pfP01, float *pfP02, float *pfP03, // points in image 1 (3d)
	float *pfP11, float *pfP12, float *pfP13, // points in image 2 (3d)
	float *rot // rotation matrix  (3x3)
);

void
vec3D_euler_rotation(
	float *pfx0, // 1x3
	float *pfy0, // 1x3
	float *pfz0, // 1x3

	float *pfx1, // 1x3
	float *pfy1, // 1x3
	float *pfz1, // 1x3

	float *ori // 3x3
);

int
determine_rotation_3point(
	float *pfP01, float *pfP02, float *pfP03, // points in image 1 (3d)
	float *rot // rotation matrix  (3x3)
);


//
// similarity_transform_3point()
//
// Transform a point pfP0 from image 1 to a point pfP1 in image 2
// via similarity transform.
//
// This function was experimental and is now not used.
// The function below is used, with only 1 rotation matrix.
//
//int
//similarity_transform_3point(
//							float *pfP0,
//							float *pfP1,
//
//							float *pfCenter0,
//							float *pfCenter1,
//						float *rot0,
//						float *rot1,
//					   float fScaleDiff
//					   )

//
// similarity_transform_3point()
//
// Transform a point pfP0 from image 1 to a point pfP1 in image 2
// via similarity transform.
//
// Output:
//		pfP1
//
int
similarity_transform_3point(
	float *pfP0,
	float *pfP1,

	float *pfCenter0,
	float *pfCenter1,
	float *rot01,
	float fScaleDiff
);


//
// similarity_transform_invert()
//
// Invert a similarity transform.
//
void
similarity_transform_invert(
	float *pfCenter0,
	float *pfCenter1,
	float *rot01,
	float &fScaleDiff
);

//
// similarity_transform_image()
//
// Resample fio1 into space of fio0.
// Transform parameters encode transform from fio0 to fio1.
//
int
similarity_transform_image(
	FEATUREIO &fio0,
	FEATUREIO &fio1,
	float *pfCenter0,
	float *pfCenter1,
	float *rot,
	float fScaleDiff,
	float fDefaultPixelValue = 0
);

//
// similarity_transform_image()
//
// Transform features in fio1 (pf1, pfs1) into space of fio0.
// Transform parameters encode transform from fio0 to fio1.
//
int
similarity_transform_feature_list(
	float *pfCenter0,
	float *pfCenter1,
	float *rot,
	float fScaleDiff,
	float *pf0, // Points in image 1
	float *pf1, // Points in image 2
	float *pfs0, // Scale for points in image 1
	float *pfs1, // Scale for points in image 2
	int iCount
);

//
// refine_feature_matches()
//
// Refine feature matches by block-wise correlation, from fio1 to fio0.
// Update feature locations/scales in fio0 (pf0,ps0).
//
int
refine_feature_matches(
	FEATUREIO &fio0,
	FEATUREIO &fio1,
	float *pf0, // Points in image 1
	float *pf1, // Points in image 2
	float *pfs0, // Scale for points in image 1
	float *pfs1, // Scale for points in image 2
	int iCount,
	int iSearchRadius = 11,
	float *pfResultNCC = 0
);

int
generateFeature3D(
	Feature3D &feat3D, // Feature geometrical information
	FEATUREIO &fioSample, // Sub-image sample
	FEATUREIO &fioImg, // Original image
	FEATUREIO &fioDx, // Derivative images
	FEATUREIO &fioDy,
	FEATUREIO &fioDz,
	vector<Feature3D> &vecFeats,
	float fEigThres = -1,// Threshold on eigenvalues, discard if
	int bReorientedFeatures = 1
);

int
determine_similarity_transform_3point(
	float *pfP01, float *pfP02, float *pfP03,
	float *pfP11, float *pfP12, float *pfP13,

	// Output
	// Rotation about point pfP01/pfP11
	float *rot0,
	float *rot1,

	// Scale change (magnification) from image 1 to image 2
	float &fScaleDiff
);

int
determine_similarity_transform_ransac(
	float *pf0, // Points in image 1
	float *pf1, // Points in image 2
	float *pfs0, // Scale for points in image 1
	float *pfs1, // Scale for points in image 2
	float *pfProb, // Probability associated with point match

	int iPoints, // number of points
	int iIterations, // number of iterations

	float *pfC0, // Reference center point in image 1

				 // Output

				 // Reference center in image 2
	float *pfC1,

	// Rotation about point pfP01/pfP11
	float *rot,

	// Scale change (magnification) from image 1 to image 2
	float &fScaleDiff,

	// Flag inliers
	int *piInliers,

	float fScaleDiffThreshold = LOG_1_5,
	float fShiftThreshold = 0.5, // works the best, MICCAI 2009 results
	float fCosineAngleThreshold = -1.0f // Allow all all angles by default
);
//
// refine_similarity_transform_ransac()
//
// Assume all features are inliers, minimize residual error of location.
// This is intended simply to provide a better (more accurate) similarity transform estimate.
//
int
refine_similarity_transform_ransac(
	float *pf0, // Points in image 1
	float *pf1, // Points in image 2
	float *pfs0, // Scale for points in image 1
	float *pfs1, // Scale for points in image 2
	float *pfProb, // Probability associated with point match

	int iPoints, // number of points
	int iIterations, // number of iterations

	float *pfC0, // Reference center point in image 1

				 // Output

				 // Reference center in image 2
	float *pfC1,

	// Rotation about point pfP01/pfP11
	float *rot,

	// Scale change (magnification) from image 1 to image 2
	float &fScaleDiff,

	// Flag inliers
	int *piInliers,

	float fScaleDiffThreshold = LOG_1_5,
	float fShiftThreshold = 0.5, // works the best, MICCAI 2009 results
	float fCosineAngleThreshold = -1.0f // Allow all all angles by default
);


//
// determine_similarity_transform_hough()
//
// Hough transform
//
int
determine_similarity_transform_hough(
	float *pf0, // Points in image 1
	float *pf1, // Points in image 2
	float *pfs0, // Scale for points in image 1
	float *pfs1, // Scale for points in image 2
	float *pfo0, // 3x3 Orientation for points in image 1
	float *pfo1, // 3x3 Orientation for points in image 2
	float *pfProb, // Probability associated with match

	int iPoints, // number of points
	int iIterations, // number of iterations

	float *pfC0, // Reference center point in image 1

				 // Output

				 // Reference center in image 2
	float *pfC1,

	// Rotation about point pfP01/pfP11
	float *rot,

	// Scale change (magnification) from image 1 to image 2
	float &fScaleDiff,

	// Flag inliers
	int *piInliers
);

int
determine_similarity_transform_hough_expanded(
	float *pf0, // Points in image 1
	float *pf1, // Points in image 2
	float *pfs0, // Scale for points in image 1
	float *pfs1, // Scale for points in image 2
	float *pfo0, // 3x3 Orientation for points in image 1
	float *pfo1, // 3x3 Orientation for points in image 2
	float *pfProb, // Probability associated with match

	int iPoints, // number of points
	int iIterations, // number of iterations

	float *pfC0, // Reference center point in image 1

				 // Output

				 // Reference center in image 2
	float *pfC1,

	// Rotation about point pfP01/pfP11
	float *rot,

	// Scale change (magnification) from image 1 to image 2
	float &fScaleDiff,

	// Flag inliers
	int *piInliers
);

template <class T, class DIV>
void
mult_3x3(
	T mat[3][3],
	T vec_in[3],
	T vec_out[3]
)
{
	for (int i = 0; i < 3; i++)
	{
		vec_out[i] = 0;
		for (int j = 0; j < 3; j++)
		{
			vec_out[i] += mat[i][j] * vec_in[j];
		}
	}
}

template <class T, class DIV>
void
mult_3x3_matrix(
	T mat1[3][3],
	T mat2[3][3],
	T mat_out[3][3]
)
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			mat_out[i][j] = 0;
			for (int ii = 0; ii < 3; ii++)
			{
				mat_out[i][j] += mat1[i][ii] * mat2[ii][j];
			}
		}
	}
}

// matrix multiplication for the case where mat2 is transposed.
// multiplication along rows of mat2_trans
template <class T, class DIV>
void
mult_3x3_trasposed_matrix(
	T mat1[3][3],
	T mat2_trans[3][3],
	T mat_out[3][3]
)
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			mat_out[i][j] = 0;
			for (int ii = 0; ii < 3; ii++)
			{
				mat_out[i][j] += mat1[i][ii] * mat2_trans[j][ii];
			}
		}
	}
}

template <class T, class DIV>
void
transpose_3x3(
	T mat_in[3][3],
	T mat_trans[3][3]
)
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			mat_trans[i][j] = mat_in[j][i];
		}
	}
}

//
// inverse_3x3()
//
// Invert a 3x3 matrix.
//
template <class T, class DIV>
void
invert_3x3(
	T mat_in[3][3],
	T mat_out[3][3]
)
{
	T &a11 = mat_in[0][0];
	T &a21 = mat_in[1][0];
	T &a31 = mat_in[2][0];
	T &a12 = mat_in[0][1];
	T &a22 = mat_in[1][1];
	T &a32 = mat_in[2][1];
	T &a13 = mat_in[0][2];
	T &a23 = mat_in[1][2];
	T &a33 = mat_in[2][2];

	T det = a11*(a33*a22 - a32*a23) - a21*(a33*a12 - a32*a13) + a31*(a23*a12 - a22*a13);

	DIV div = 1 / (DIV)det;

	mat_out[0][0] = (a33*a22 - a32*a23)*div;
	mat_out[1][0] = -(a33*a21 - a31*a23)*div;
	mat_out[2][0] = (a32*a21 - a31*a22)*div;
	mat_out[0][1] = -(a33*a12 - a32*a13)*div;
	mat_out[1][1] = (a33*a11 - a31*a13)*div;
	mat_out[2][1] = -(a32*a11 - a31*a12)*div;
	mat_out[0][2] = (a23*a12 - a22*a13)*div;
	mat_out[1][2] = -(a23*a11 - a21*a13)*div;
	mat_out[2][2] = (a22*a11 - a21*a12)*div;
}

template <class T, class DIV>
void
scale_3x3(
	T vec[3],
	T length
)
{
	DIV dSumSqr = 0;
	for (int i = 0; i < 3; i++)
	{
		dSumSqr += vec[i] * vec[i];
	}
	assert(dSumSqr > 0);
	dSumSqr = sqrt((length*length) / dSumSqr);
	for (int i = 0; i < 3; i++)
	{
		vec[i] *= dSumSqr;
	}
}




float
vec3D_dot_3d(
	const float *pv1,
	const float *pv2
);



template< class FEATURE_TYPE >
int
msFeature3DVectorOutputBin(
	vector<FEATURE_TYPE> &vecFeats3D,
	char *pcFileName,
	float fEigThres = -1
)
{
	FILE *outfile = fopen(pcFileName, "wb");
	if (!outfile)
	{
		return -1;
	}
	int iFeatCount = 0; //vecFeats3D.size();

	for (int i = 0; i < vecFeats3D.size(); i++)
	{
		FEATURE_TYPE &feat3D = vecFeats3D[i];

		// Sphere, apply threshold
		float fEigSum = feat3D.eigs[0] + feat3D.eigs[1] + feat3D.eigs[2];
		float fEigPrd = feat3D.eigs[0] * feat3D.eigs[1] * feat3D.eigs[2];
		float fEigSumProd = fEigSum*fEigSum*fEigSum;
		if (fEigSumProd < fEigThres*fEigPrd || fEigThres < 0)
		{
			iFeatCount++;
		}
	}

	fprintf(outfile, "# featExtract %s\n", "1.1");
	fprintf(outfile, "Features: %d\n", iFeatCount);
	//fprintf( outfile, "Scale-space location[x y z scale] orientation[o11 o12 o13 o21 o22 o23 o31 o32 o32] 2nd moment eigenvalues[e1 e2 e3] info flag[i1] descriptor[d1 .. d64]\n" );

	for (int i = 0; i < vecFeats3D.size(); i++)
	{
		FEATURE_TYPE &feat3D = vecFeats3D[i];

		// Sphere, apply threshold
		float fEigSum = feat3D.eigs[0] + feat3D.eigs[1] + feat3D.eigs[2];
		float fEigPrd = feat3D.eigs[0] * feat3D.eigs[1] * feat3D.eigs[2];
		float fEigSumProd = fEigSum*fEigSum*fEigSum;
		if (fEigSumProd < fEigThres*fEigPrd || fEigThres < 0)
		{
		}
		else
		{
			continue;
		}

		// Location and scale
		fwrite(&(vecFeats3D[i].x), sizeof(float), 1, outfile);
		fwrite(&(vecFeats3D[i].y), sizeof(float), 1, outfile);
		fwrite(&(vecFeats3D[i].z), sizeof(float), 1, outfile);
		fwrite(&(vecFeats3D[i].scale), sizeof(float), 1, outfile);
		//fprintf( outfile, "%f\t%f\t%f\t%f\t", vecFeats3D[i].x, vecFeats3D[i].y, vecFeats3D[i].z, vecFeats3D[i].scale );

		// Orientation (could save a vector here)
		fwrite(&(vecFeats3D[i].ori[0][0]), sizeof(float), 9, outfile);
		for (int j = 0; j < 3; j++)
		{
			for (int k = 0; k < 3; k++)
			{
				//fprintf( outfile, "%f\t", vecFeats3D[i].ori[j][k] );
			}
		}

		// Eigenvalues of 2nd moment matrix
		fwrite(&(vecFeats3D[i].eigs[0]), sizeof(float), 3, outfile);
		for (int j = 0; j < 3; j++)
		{
			//fprintf( outfile, "%f\t", vecFeats3D[i].eigs[j] );
		}

		// Info flag
		fwrite(&(vecFeats3D[i].m_uiInfo), sizeof(unsigned int), 1, outfile);
		//fprintf( outfile, "%d\t", vecFeats3D[i].m_uiInfo );

		// Output principal components, if set
		//fwrite( &(vecFeats3D[i].m_pfPC[0]), sizeof(unsigned char), Feature3DInfo::FEATURE_3D_PCS, outfile );
		unsigned char pucVec[Feature3DInfo::FEATURE_3D_PCS];
		for (int j = 0; j < Feature3DInfo::FEATURE_3D_PCS; j++)
		{
			pucVec[j] = (unsigned char)(vecFeats3D[i].m_pfPC[j]);
			//fprintf( outfile, "%i\t", (unsigned char)(vecFeats3D[i].m_pfPC[j]) );
		}
		fwrite(&(pucVec[0]), sizeof(unsigned char), Feature3DInfo::FEATURE_3D_PCS, outfile);
		//fprintf( outfile, "\n" );
	}

	fclose(outfile);
	return 0;
}

template< class FEATURE_TYPE >
int
msFeature3DVectorInputBin(
	vector<FEATURE_TYPE> &vecFeats3D,
	char *pcFileName,
	float fEigThres = -1
)
{
	FILE *infile = fopen(pcFileName, "rb");
	if (!infile)
	{
		return -1;
	}
	int iFeatCount = vecFeats3D.size();

	char buff[400];
	char versi[400];

	int bGotVersion = 0;
	fgets(buff, sizeof(buff), infile);
	while (buff[0] == '#')
	{
		if (sscanf(buff, "# featExtract %s\n", versi) == 1)
		{
			//int b1 = strcmp( versi, "1.7b" );
			//int b2 = strcmp( versi, "1.1" );
			if (strcmp(versi, "1.7") != 0 && strcmp(versi, "1.1") != 0)
			{
				fclose(infile);
				return -1;
			}
			else
			{
				bGotVersion = 1;
			}
		}
		fgets(buff, sizeof(buff), infile);
	}
	if (!bGotVersion)
	{
		fclose(infile);
		// File exists, no version name. Perhaps it is a text file
		return msFeature3DVectorInputText(vecFeats3D, pcFileName, fEigThres);
	}
	sscanf(buff, "Features: %d\n", &iFeatCount);
	if (iFeatCount <= 0)
	{
		fclose(infile);
		return -1;
	}

	//fprintf( infile, "Scale-space location[x y z scale] orientation[o11 o12 o13 o21 o22 o23 o31 o32 o32] 2nd moment eigenvalues[e1 e2 e3] info flag[i1] descriptor[d1 .. d64]\n" );

	vecFeats3D.resize(iFeatCount);

	for (int i = 0; i < iFeatCount; i++)
	{
		fread(&(vecFeats3D[i].x), sizeof(float), 1, infile);
		fread(&(vecFeats3D[i].y), sizeof(float), 1, infile);
		fread(&(vecFeats3D[i].z), sizeof(float), 1, infile);
		fread(&(vecFeats3D[i].scale), sizeof(float), 1, infile);
		fread(&(vecFeats3D[i].ori[0][0]), sizeof(float), 9, infile);
		fread(&(vecFeats3D[i].eigs[0]), sizeof(float), 3, infile);
		fread(&(vecFeats3D[i].m_uiInfo), sizeof(unsigned int), 1, infile);

		unsigned char pucVec[Feature3DInfo::FEATURE_3D_PCS];
		fread(&(pucVec[0]), sizeof(unsigned char), Feature3DInfo::FEATURE_3D_PCS, infile);
		for (int j = 0; j < Feature3DInfo::FEATURE_3D_PCS; j++)
		{
			vecFeats3D[i].m_pfPC[j] = pucVec[j];
		}

		FEATURE_TYPE &feat3D = vecFeats3D[i];
		float fEigSum = feat3D.eigs[0] + feat3D.eigs[1] + feat3D.eigs[2];
		float fEigPrd = feat3D.eigs[0] * feat3D.eigs[1] * feat3D.eigs[2];
		float fEigSumProd = fEigSum*fEigSum*fEigSum;
		if (fEigSumProd < fEigThres*fEigPrd || fEigThres < 0)
		{
		}
		else
		{
			i--;
			iFeatCount--;
		}
	}

	fclose(infile);

	vecFeats3D.resize(iFeatCount);

	return vecFeats3D.size();
}

template< class FEATURE_TYPE >
int
msFeature3DVectorInputText(
	vector<FEATURE_TYPE> &vecFeats3D,
	char *pcFileName,
	float fEigThres = -1
)
{
	FILE *infile = fopen(pcFileName, "rt");
	if (!infile)
	{
		return -1;
	}
	int iFeatCount = vecFeats3D.size();

	char buff[400];
	buff[0] = '#';

	// Read past comments
	while (buff[0] == '#')
		//while( buff[0] == '#' || strstr(buff, "Number of descriptors:") == NULL)
	{
		fgets(buff, sizeof(buff), infile);
	}

	if (sscanf(buff, "Features: %d\n", &iFeatCount) <= 0 || iFeatCount <= 0)
		//if( sscanf( buff, "Number of descriptors: %d\n", &iFeatCount ) <= 0 || iFeatCount <= 0 )
	{
		fclose(infile);
		return -1;
	}
	//fgets( buff, sizeof(buff), infile );

	fgets(buff, sizeof(buff), infile);
	//if( strcmp( buff, "Scale-space location[x y z scale] orientation[o11 o12 o13 o21 o22 o23 o31 o32 o32] 2nd moment eigenvalues[e1 e2 e3] info flag[i1] descriptor[d1 .. d64]\n" ) != 0 )
	if (!strstr(buff, "Scale-space location[x y z scale]"))
		//if ( !strstr( buff, "Descriptors:" ) )
	{
		fclose(infile);
		return -1;
	}

	vecFeats3D.resize(iFeatCount);

	for (int i = 0; i < iFeatCount; i++)
	{
		int iReturn =
			fscanf(infile, "%f\t%f\t%f\t%f\t", &(vecFeats3D[i].x), &(vecFeats3D[i].y), &(vecFeats3D[i].z), &(vecFeats3D[i].scale));
		assert(iReturn == 4);

		for (int j = 0; j < 3; j++)
		{
			for (int k = 0; k < 3; k++)
			{
				iReturn = fscanf(infile, "%f\t", &(vecFeats3D[i].ori[j][k]));
				assert(iReturn == 1);
			}
		}

		// Eigenvalues of 2nd moment matrix
		for (int j = 0; j < 3; j++)
		{
			iReturn = fscanf(infile, "%f\t", &(vecFeats3D[i].eigs[j]));
			assert(iReturn == 1);
		}

		// Info flag
		iReturn = fscanf(infile, "%d\t", &(vecFeats3D[i].m_uiInfo));
		assert(iReturn == 1);

		for (int j = 0; j < Feature3DInfo::FEATURE_3D_PCS; j++)
		{
			iReturn = fscanf(infile, "%f\t", &(vecFeats3D[i].m_pfPC[j]));
			assert(iReturn == 1);
		}

		FEATURE_TYPE &feat3D = vecFeats3D[i];
		/*
		float fEigSum = feat3D.eigs[0] + feat3D.eigs[1] + feat3D.eigs[2];
		float fEigPrd = feat3D.eigs[0]*feat3D.eigs[1]*feat3D.eigs[2];
		float fEigSumProd = fEigSum*fEigSum*fEigSum;
		if( fEigSumProd < fEigThres*fEigPrd || fEigThres < 0 )
		{
		}
		else
		{
		i--;
		iFeatCount--;
		}
		*/
	}

	fclose(infile);

	vecFeats3D.resize(iFeatCount);

	return 0;
}

template< class FEATURE_TYPE >
int
msFeature3DVectorOutputText(
	vector<FEATURE_TYPE> &vecFeats3D,
	char *pcFileName,
	float fEigThres = -1,
	int iCommentLines = 0,
	char **ppcCommentLines = 0
)
{
	FILE *outfile = fopen(pcFileName, "wt");
	if (!outfile)
	{
		return -1;
	}
	int iFeatCount = 0; //vecFeats3D.size();

	for (int i = 0; i < vecFeats3D.size(); i++)
	{
		FEATURE_TYPE &feat3D = vecFeats3D[i];

		// Sphere, apply threshold
		float fEigSum = feat3D.eigs[0] + feat3D.eigs[1] + feat3D.eigs[2];
		float fEigPrd = feat3D.eigs[0] * feat3D.eigs[1] * feat3D.eigs[2];
		float fEigSumProd = fEigSum*fEigSum*fEigSum;
		if (fEigSumProd < fEigThres*fEigPrd || fEigThres < 0)
		{
			iFeatCount++;
		}
	}

	fprintf(outfile, "# featExtract %s\n", "1.1");
	for (int i = 0; i < iCommentLines; i++)
	{
		// Print out comments
		fprintf(outfile, "# %s\n", ppcCommentLines[i]);
	}
	fprintf(outfile, "Features: %d\n", iFeatCount);
	fprintf(outfile, "Scale-space location[x y z scale] orientation[o11 o12 o13 o21 o22 o23 o31 o32 o32] 2nd moment eigenvalues[e1 e2 e3] info flag[i1] descriptor[d1 .. d64]\n");

	for (int i = 0; i < vecFeats3D.size(); i++)
	{
		FEATURE_TYPE &feat3D = vecFeats3D[i];

		// Sphere, apply threshold
		float fEigSum = feat3D.eigs[0] + feat3D.eigs[1] + feat3D.eigs[2];
		float fEigPrd = feat3D.eigs[0] * feat3D.eigs[1] * feat3D.eigs[2];
		float fEigSumProd = fEigSum*fEigSum*fEigSum;
		if (fEigSumProd < fEigThres*fEigPrd || fEigThres < 0)
		{
		}
		else
		{
			continue;
		}

		// Location and scale
		fprintf(outfile, "%f\t%f\t%f\t%f\t", vecFeats3D[i].x, vecFeats3D[i].y, vecFeats3D[i].z, vecFeats3D[i].scale);

		// Orientation (could save a vector here)
		for (int j = 0; j < 3; j++)
		{
			for (int k = 0; k < 3; k++)
			{
				fprintf(outfile, "%f\t", vecFeats3D[i].ori[j][k]);
			}
		}

		// Eigenvalues of 2nd moment matrix
		for (int j = 0; j < 3; j++)
		{
			fprintf(outfile, "%f\t", vecFeats3D[i].eigs[j]);
		}

		// Info flag
		fprintf(outfile, "%d\t", vecFeats3D[i].m_uiInfo);

		// Output principal components, if set
		for (int j = 0; j < Feature3DInfo::FEATURE_3D_PCS; j++)
		{
			fprintf(outfile, "%i\t", (unsigned char)(vecFeats3D[i].m_pfPC[j]));
		}
		fprintf(outfile, "\n");
	}

	fclose(outfile);
	return 0;
}


//
// get_max_2D()
// get_max_3D()
//
// Find the maximum/maximum voxel in an image in a range of +/- 2 voxels around lvMid.
//
//
int
get_max_3D(
	float fxIn, float fyIn, float fzIn,
	FEATUREIO &fio,
	float &fx,
	float &fy,
	float &fz,
	int bPeak // boolean - peak = 1, valley = 0
);
int
get_max_2D(
	float fxIn, float fyIn,
	FEATUREIO &fio,
	float &fx,
	float &fy,
	int bPeak // boolean - peak = 1, valley = 0
);



//
// Mirror Y axis in feature descriptor.
// Should also reflect y coordinate of feautre.
//
int
msMirrorYFeaturesGradientOrientationHistogram(
	Feature3DInfo &feat3D
);



//
// msInvertedIntensity()
//
// Convert feature such that it can be matched to an image with
// an inverted intensity profile.
//
int
msInvertedIntensity(
	Feature3DInfo &feat
);

//
// msExpandMultiModal()
//
// Turn a feature vector into a multimodal feature vector.
// Append to itself, invert.
//
int
msExpandMultiModal(
	vector<Feature3DInfo> &vecFeats
);





// Some useful vector / matrix functions

void
vec3D_norm_3d(
	float *pf1
);


float
vec3D_mag(
	float *pf1
);
//void
//mult_3x3_matrix(
//			float *mat1,
//			float *mat2,
//			float *mat_out
//			);

void
mult_3x3_scalar(
	float *mat1,
	float multiple,
	float *mat_out
);

void
mult_3x3_vector(
	float *mat1,
	float *mat2,
	float *mat_out // Here, 1 vector
);

void
mult_4x4_vector(
	float *mat,
	float *vec_in,
	float *vec_out
);

#endif
