
#ifndef __FLOODNEIGHBOURS_H__
#define __FLOODNEIGHBOURS_H__

// Function overriden by user

typedef int FNCallback( int iLocation, void *pArg );

void
fnFloodNeighbours(
		  int			*pStack,					// Stack big enough for all data
		  int			iStartLocation,				// Start Row
		  int			*piNeighourDisplacements,	// Neighbour displacements
		  int			iNeighbours,				// Neighbour displacement count
		  FNCallback	*pProcess,					// Function to process pixel
		  FNCallback	*pPushOnStack,				// Function to put pixel on stack
		  void			*pArg						// Argument passed to FNCallback
);

#endif
