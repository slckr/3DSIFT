
#include <assert.h>
#include <search.h>
#include <stdio.h>
#include <memory.h>
#include <math.h>

#include "DataPair.h"
#include "MinSpanningTree.h"

float
floatVectorDistanceEuclidean(
							 const int iVectorLengthBytes,
							 const void *pVector1,
							 const void *pVector2,
							 const void *pArg
							 )
{
	int iFeats = iVectorLengthBytes/sizeof(float); // 4 bytes per float
	float *pfVec1 = (float*)pVector1;
	float *pfVec2 = (float*)pVector2;
	float fDistSum = 0;
	for( int i = 0; i < iFeats; i++ )
	{
		float fDist = pfVec1[i]-pfVec2[i];
		fDistSum += fDist*fDist;
	}
	return (float)sqrt( fDistSum );
}

float
ucharVectorDistanceEuclidean(
							 const int iVectorLengthBytes,
							 const void *pVector1,
							 const void *pVector2,
							 const void *pArg
							 )
{
	int iFeats = iVectorLengthBytes/sizeof(unsigned char); // 1 byte per char
	unsigned char *pucVec1 = (unsigned char*)pVector1;
	unsigned char *pucVec2 = (unsigned char*)pVector2;
	float fDistSum = 0;
	for( int i = 0; i < iFeats; i++ )
	{
		float fDist = pucVec1[i]-pucVec2[i];
		fDistSum += fDist*fDist;
	}
	return (float)sqrt( fDistSum );
}

//
// initForest()
//
// Initialized a FOREST structure, allocates memory.
//
int
initForest(
		   FOREST	&fF1,
		   int		iMaxTrees
		   )
{
	assert( iMaxTrees >= 0 );
	if( iMaxTrees < 0 )
	{
		return 0;
	}

	memset( &fF1, 0, sizeof(FOREST) );

	if( iMaxTrees == 0 )
	{
		return 1;
	}

	fF1.piTreeIndicies = new int[ iMaxTrees ];
	assert( fF1.piTreeIndicies );
	if( !fF1.piTreeIndicies )
	{
		fF1.iTreeIndexCountUsed = 0;
		fF1.iTreeIndexCountMax = 0;
		return 0;
	}

	fF1.iTreeIndexCountMax = iMaxTrees;
	fF1.iTreeIndexCountUsed = 0;

	return 1;
}

//
// copyForest()
//
// Deep copy of fF2 to fF1
//
int
copyForest(
		   FOREST		&fF1,
		   const FOREST	&fF2
		   )
{
	if( fF1.iTreeIndexCountMax < fF2.iTreeIndexCountMax )
	{
		int *pNewTreeIndicies = new int[ fF2.iTreeIndexCountMax ];
		assert( pNewTreeIndicies );
		if( !pNewTreeIndicies )
		{
			return 0;
		}

		fF1.piTreeIndicies = pNewTreeIndicies;
		fF1.iTreeIndexCountMax = fF2.iTreeIndexCountMax;
	}

	memcpy( fF1.piTreeIndicies, fF2.piTreeIndicies, sizeof(int)*fF2.iTreeIndexCountMax );
	fF1.iTreeIndexCountUsed = fF2.iTreeIndexCountUsed;

	return 1;
}

int
deleteForest(
		   FOREST		&fF1
		   )
{
	if( fF1.piTreeIndicies )
	{
		assert( fF1.iTreeIndexCountMax > 0 );
		delete [] fF1.piTreeIndicies;
		return ( fF1.iTreeIndexCountMax > 0 );
	}
	else
	{
		assert( fF1.iTreeIndexCountMax == 0 );
		return (fF1.iTreeIndexCountMax == 0 );
	}
}

//
// minTreeDistance()
//
// Calculates the minimum distance between two trees. Minimum distance
// is the minimum distance between any two points, one from tree1 and 
// one from tree2.
// Indicies of the actual points resulting in the min distance are returned
// in ipBestPair.
//
static float
minTreeDistance(
			const unsigned char	*pucVectors,
			const int			&iVectorLengthBytes,
			const int			*piPointIndicies,
			const TREE			&tree1,
			const TREE			&tree2,
			FLOAT_VECTOR_DISTANCE_FUNCTION floatDist,
			INDEXPAIR			&ipBestPair
			)
{
	assert( tree1.iPointsCount > 0 && tree2.iPointsCount > 0 );
	if( !(tree1.iPointsCount > 0 && tree2.iPointsCount > 0 ) )
	{
		return 0.0;
	}

	//
	// N*N time - this could be slow! Better algorithms exist...
	//
	// Maybe we only have to check the convex hulls of each tree?
	//

	const int *piPoints1 = piPointIndicies + tree1.iPointsIndex;
	const int *piPoints2 = piPointIndicies + tree2.iPointsIndex;

	const unsigned char *pucVector1 = pucVectors + piPoints1[0]*iVectorLengthBytes;
	const unsigned char *pucVector2 = pucVectors + piPoints2[0]*iVectorLengthBytes;

	float fMinDist = floatDist( iVectorLengthBytes, pucVector1, pucVector2, 0 );
	int iMinI1 = 0;
	int iMinI2 = 0;
	ipBestPair[0] = piPoints1[0];
	ipBestPair[1] = piPoints2[0];

	for( int i1 = 0; i1 < tree1.iPointsCount; i1++ )
	{
		pucVector1 = pucVectors + piPoints1[i1]*iVectorLengthBytes;

		for( int i2 = 0; i2 < tree2.iPointsCount; i2++ )
		{
			pucVector2 = pucVectors + piPoints2[i2]*iVectorLengthBytes;

			float fDist = floatDist( iVectorLengthBytes, pucVector1, pucVector2, 0 );
			if( fDist < fMinDist )
			{
				fMinDist = fDist;
				iMinI1 = i1;
				iMinI2 = i2;
				ipBestPair[0] = piPoints1[i1];
				ipBestPair[1] = piPoints2[i2];
			}
		}
	}

	return fMinDist;
}

int
computeMinSpanningTree(
					   void			*pPointVectors,
					   int			iPointVectorLengthBytes,
					   int			iPointVectorCount,
					   FLOAT_VECTOR_DISTANCE_FUNCTION floatDist,
					   INDEXPAIR	*pipEdges
					   )
{
	// Allocate memory

	float *pfDistances = 0;				// Best distances
	int *piTreeIndicies = 0;			// Maps an old tree to a new merged tree

	INDEXPAIR *pipIndexPairTrees = 0;	// Saves matched trees
	INDEXPAIR *pipIndexPairPoints = 0;	// Saves edges between matched trees

	int *piTreePointIndicies1 = 0;		// Tree point data 1
	int *piTreePointIndicies2 = 0;		// Tree point data 2
	INDEXPAIR *pipTreePointEdges1 = 0;	// Tree edge data 1
	INDEXPAIR *pipTreePointEdges2 = 0;	// Tree edge data 2
	TREE	*pTrees1 = 0;				// Tree data 1
	TREE	*pTrees2 = 0;				// Tree data 2
	FOREST fForest1;					// Forest 1
	FOREST fForest2;					// Forest 2
	memset( &fForest1, 0, sizeof(FOREST) );
	memset( &fForest2, 0, sizeof(FOREST) );

	pfDistances = new float[iPointVectorCount];
	assert( pfDistances );
	if( !pfDistances )
	{
		return 0;
	}

	piTreeIndicies = new int[iPointVectorCount];
	assert( piTreeIndicies );
	if( !piTreeIndicies )
	{
		return 0;
	}

	piTreePointIndicies1 = new int[iPointVectorCount];
	assert( piTreePointIndicies1 );
	if( !piTreePointIndicies1 )
	{
		return 0;
	}

	piTreePointIndicies2 = new int[iPointVectorCount];
	assert( piTreePointIndicies2 );
	if( !piTreePointIndicies2 )
	{
		return 0;
	}

	pipTreePointEdges1 = new INDEXPAIR[iPointVectorCount];
	pipTreePointEdges2 = new INDEXPAIR[iPointVectorCount];

	pipIndexPairTrees = new INDEXPAIR[iPointVectorCount];
	assert( pipIndexPairTrees );
	if( !pipIndexPairTrees )
	{
		return 0;
	}

	pipIndexPairPoints = new INDEXPAIR[iPointVectorCount];
	assert( pipIndexPairPoints );
	if( !pipIndexPairPoints )
	{
		return 0;
	}
	
	pTrees1 = new TREE[iPointVectorCount];
	assert( pTrees1 );
	if( !pTrees1 )
	{
		return 0;
	}
	
	pTrees2 = new TREE[iPointVectorCount];
	assert( pTrees2 );
	if( !pTrees2 )
	{
		return 0;
	}

	if( !initForest( fForest1, iPointVectorCount ) )
	{
		return 0;
	}

	if( !initForest( fForest2, iPointVectorCount ) )
	{
		return 0;
	}

	// Init tree points

	for( int i = 0; i < iPointVectorCount; i++ )
	{
		piTreePointIndicies1[i] = i;
	}

	// Init trees - 1 point per tree, no edges

	for( int i = 0; i < iPointVectorCount; i++ )
	{
		pTrees1[i].iEdgesCount = pTrees2[i].iEdgesCount = 0;
		pTrees1[i].iEdgesIndex = pTrees2[i].iEdgesIndex = -1;
		pTrees1[i].iPointsCount = pTrees2[i].iPointsCount = 1;
		pTrees1[i].iPointsIndex = pTrees2[i].iPointsIndex = i;
	}

	// Init forests

	fForest1.iTreeIndexCountUsed = iPointVectorCount;
	fForest2.iTreeIndexCountUsed = iPointVectorCount;

	for( int i = 0; i < iPointVectorCount; i++ )
	{
		fForest1.piTreeIndicies[i] = fForest2.piTreeIndicies[i] = i;
	}

	// Repeat while there are more than 1 trees remaining

	while( fForest1.iTreeIndexCountUsed > 1 )
	{
		// Set flags

		for( int i = 0; i < iPointVectorCount; i++ )
		{
			pipIndexPairTrees[i][0] = -1;
		}

		// Generate min distances between all trees

		for( int iT1 = 0; iT1 < fForest1.iTreeIndexCountUsed; iT1++ )
		{
			for( int iT2 = iT1 + 1; iT2 < fForest1.iTreeIndexCountUsed; iT2++ )
			{
				INDEXPAIR ipBestPair;
				float fDist =
					minTreeDistance(
						(unsigned char*)pPointVectors, iPointVectorLengthBytes,
						piTreePointIndicies1,
						pTrees1[iT1], pTrees1[iT2],
						floatDist,
						ipBestPair
					);

				if( fDist < pfDistances[iT1] || pipIndexPairTrees[iT1][0] == -1 )
				{
					pfDistances[iT1] = fDist;
					pipIndexPairTrees[iT1][0] = iT1;
					pipIndexPairTrees[iT1][1] = iT2;
					pipIndexPairPoints[iT1][0] = ipBestPair[0];
					pipIndexPairPoints[iT1][1] = ipBestPair[1];
				}

				if( fDist < pfDistances[iT2] || pipIndexPairTrees[iT2][0] == -1 )
				{
					pfDistances[iT2] = fDist;
					pipIndexPairTrees[iT2][0] = iT2;
					pipIndexPairTrees[iT2][1] = iT1;
					pipIndexPairPoints[iT2][0] = ipBestPair[0];
					pipIndexPairPoints[iT2][1] = ipBestPair[1];
				}
			}
		}

//		FILE *outfile = fopen( "indexpairs_trees.txt", "wt" );
//		for( int i = 0; i < fForest1.iTreeIndexCountUsed; i++ )
//		{
//			fprintf( outfile, "%d, %d\n", pipIndexPairTrees[i][0], pipIndexPairTrees[i][1] );
//		}
//		fclose( outfile );
//
//		outfile = fopen( "indexpairs_points.txt", "wt" );
//		for( int i = 0; i < fForest1.iTreeIndexCountUsed; i++ )
//		{
//			fprintf( outfile, "%d, %d\n", pipIndexPairPoints[i][0], pipIndexPairPoints[i][1] );
//		}
//		fclose( outfile );

		// Set flags - all new tree indicies are invalid (-1)
		for( int i = 0; i < fForest1.iTreeIndexCountUsed; i++ )
		{
			piTreeIndicies[i] = -1;
		}

		// Create indicies of new trees to be created after merging

		int iNextNewTree = 0;

		for( int i = 0; i < fForest1.iTreeIndexCountUsed; i++ )
		{
//			outfile = fopen( "next_trees_inc.txt", "wt" );
//			for( int tree = 0; tree < fForest1.iTreeIndexCountUsed; tree++ )
//			{
//				fprintf( outfile, "%d\n", piTreeIndicies[ tree ] );
//			}
//			fclose( outfile );

			int iTree0 = pipIndexPairTrees[i][0];
			int iTree1 = pipIndexPairTrees[i][1];

			assert( iTree0 != iTree1 );
			assert( iTree0 == i );

			if( iTree1 < iTree0 )
			{
				// Skip if this tree pair has already been used
				assert( pipIndexPairTrees[iTree1][0] == iTree1 );
				
				if( pipIndexPairTrees[iTree1][1] == iTree0 )
				{
					assert( piTreeIndicies[ iTree0 ] != -1 && piTreeIndicies[ iTree1 ] != -1 );
					assert( piTreeIndicies[ iTree0 ] == piTreeIndicies[ iTree1 ] );
					continue;
				}
			}

			if( piTreeIndicies[ iTree0 ] == -1 || piTreeIndicies[ iTree1 ] == -1 )
			{
				if( piTreeIndicies[ iTree0 ] == -1 && piTreeIndicies[ iTree1 ] == -1 )
				{
					// Neither tree is merged into a new tree, create a new merge tree
					piTreeIndicies[ iTree0 ] = iNextNewTree;
					piTreeIndicies[ iTree1 ] = iNextNewTree;
					iNextNewTree++;
				}
				else if( piTreeIndicies[ iTree0 ] == -1 )
				{
					//  iTree1 is alread merged into a new tree, add iTree0
					piTreeIndicies[ iTree0 ] = piTreeIndicies[ iTree1 ];
				}
				else if( piTreeIndicies[ iTree1 ] == -1 )
				{
					//  iTree0 is alread merged into a new tree, add iTree1
					piTreeIndicies[ iTree1 ] = piTreeIndicies[ iTree0 ];
				}
			}
			else if( piTreeIndicies[ iTree0 ] != piTreeIndicies[ iTree1 ] )
			{
				// iTree0 and iTree1 merged into different trees but are connected.
				int iMinNextNewTree =
					( piTreeIndicies[ iTree0 ] < piTreeIndicies[ iTree1 ] ? piTreeIndicies[ iTree0 ] : piTreeIndicies[ iTree1 ] );
				int iMaxNextNewTree =
					( piTreeIndicies[ iTree0 ] > piTreeIndicies[ iTree1 ] ? piTreeIndicies[ iTree0 ] : piTreeIndicies[ iTree1 ] );

				assert( iMinNextNewTree < iMaxNextNewTree );

				// Set all trees marked with max to min,
				// decrement all indicies higher than max

				for( int j = 0; j < fForest1.iTreeIndexCountUsed; j++ )
				{
					if( piTreeIndicies[ j ] > iMaxNextNewTree )
					{
						piTreeIndicies[ j ]--;
					}
					else if( piTreeIndicies[ j ] == iMaxNextNewTree )
					{
						piTreeIndicies[ j ] = iMinNextNewTree;
					}
				}

				// Set trees themselves

				piTreeIndicies[ iTree0 ] = iMinNextNewTree;
				piTreeIndicies[ iTree1 ] = iMinNextNewTree;
			
				assert( piTreeIndicies[ iTree0 ] != -1 && piTreeIndicies[ iTree1 ] != -1 );
				assert( piTreeIndicies[ iTree0 ] == piTreeIndicies[ iTree1 ] );

				iNextNewTree--;
			}
		}

//		outfile = fopen( "next_tree_indicies.txt", "wt" );
//		for( int i = 0; i < fForest1.iTreeIndexCountUsed; i++ )
//		{
//			fprintf( outfile, "%d\t(%d, %d)\n", piTreeIndicies[ i ], pipIndexPairTrees[i][0], pipIndexPairTrees[i][1] );
//		}
//		fclose( outfile );

		// Prepare new trees - indicies & counts
		
		fForest2.iTreeIndexCountUsed = iNextNewTree;

		pTrees2[0].iEdgesIndex = 0;
		pTrees2[0].iPointsIndex = 0;

		for( int i = 0; i < fForest2.iTreeIndexCountUsed; i++ )
		{
			pTrees2[i].iPointsCount = 0;
			pTrees2[i].iEdgesCount = 0;

			// Get point/edge count

			int iCount = 0;
			for( int j = 0; j < fForest1.iTreeIndexCountUsed; j++ )
			{
				if( piTreeIndicies[ j ] == i )
				{
					iCount += pTrees1[j].iPointsCount;
				}
			}

			assert( iCount >= 2 );

			pTrees2[i].iPointsCount = iCount;
			pTrees2[i].iEdgesCount = iCount - 1;

			// Set next start index based on count

			if( i < fForest2.iTreeIndexCountUsed - 1  )
			{
				pTrees2[i+1].iPointsIndex =
					pTrees2[i].iPointsIndex + pTrees2[i].iPointsCount;
			}
			if( i < fForest2.iTreeIndexCountUsed - 1  )
			{
				pTrees2[i+1].iEdgesIndex =
					pTrees2[i].iEdgesIndex + pTrees2[i].iEdgesCount;
			}
		}

		// Merge trees with closest distances

		int iNewPointCount = 0;
		int iNewEdgeCount = 0;

		for( int i = 0; i < fForest2.iTreeIndexCountUsed; i++ )
		{
			// Merge all points in trees merging to this new tree

			TREE &treeNew = pTrees2[i];

			assert( treeNew.iPointsIndex == iNewPointCount );
			assert( treeNew.iEdgesIndex == iNewEdgeCount );

			for( int j = 0; j < fForest1.iTreeIndexCountUsed; j++ )
			{
				if( piTreeIndicies[j] == i )
				{
					// Merge old tree j to new tree i

					// Add points to point set

					TREE &treeOld = pTrees1[j];
					memcpy(
						piTreePointIndicies2 + iNewPointCount,
						piTreePointIndicies1 + treeOld.iPointsIndex,
						treeOld.iPointsCount*sizeof(int)
						);
					iNewPointCount += treeOld.iPointsCount;

					// Add old edges to point set

					if( treeOld.iEdgesCount > 0 )
					{
						memcpy(
							pipTreePointEdges2 + iNewEdgeCount,
							pipTreePointEdges1 + treeOld.iEdgesIndex,
							treeOld.iEdgesCount*sizeof(INDEXPAIR)
							);
						iNewEdgeCount += treeOld.iEdgesCount;
					}

					// Add new edge if it has not already been added

					int iTree0 = pipIndexPairTrees[j][0];
					int iTree1 = pipIndexPairTrees[j][1];
					if( iTree1 < iTree0 )
					{
						// Skip if this tree pair has already been used
						assert( pipIndexPairTrees[iTree1][0] == iTree1 );
						
						if( pipIndexPairTrees[iTree1][1] == iTree0 )
						{
							assert( piTreeIndicies[ iTree0 ] != -1 && piTreeIndicies[ iTree1 ] != -1 );
							assert( piTreeIndicies[ iTree0 ] == piTreeIndicies[ iTree1 ] );
							continue;
						}
					}

					pipTreePointEdges2[ iNewEdgeCount ][0] = pipIndexPairPoints[j][0];
					pipTreePointEdges2[ iNewEdgeCount ][1] = pipIndexPairPoints[j][1];
					iNewEdgeCount++;
				}
			}

//			char pcOutfile[200];
//			sprintf( pcOutfile, "tree%d_edges.txt", i );
//			outfile = fopen( pcOutfile, "wt" );
//			for( int edge = 0; edge < treeNew.iEdgesCount; edge++ )
//			{
//				fprintf( outfile, "%d\t%d\n", pipTreePointEdges2[treeNew.iEdgesIndex + edge][0], pipTreePointEdges2[treeNew.iEdgesIndex + edge][1] );
//			}
//			fclose( outfile );
//			
//			sprintf( pcOutfile, "tree%d_points.txt", i );
//			outfile = fopen( pcOutfile, "wt" );
//			for( int point = 0; point < treeNew.iPointsCount; point++ )
//			{
//				fprintf( outfile, "%d\n", piTreePointIndicies2[treeNew.iPointsIndex + point] );
//			}
//			fclose( outfile );
		}

		// Swap old tree data with new

		if( fForest2.iTreeIndexCountUsed == 1 )
		{
			assert( iNewEdgeCount == iPointVectorCount - 1 );
			assert( iNewPointCount == iPointVectorCount );
		}

		copyForest( fForest1, fForest2 );
		memcpy( pTrees1, pTrees2, sizeof(TREE)*fForest2.iTreeIndexCountUsed );
		memcpy( pipTreePointEdges1, pipTreePointEdges2, sizeof(INDEXPAIR)*iNewEdgeCount );
		memcpy( piTreePointIndicies1, piTreePointIndicies2, sizeof(int)*iNewPointCount );
	}

	// Copy edges to user array

	memcpy( pipEdges, pipTreePointEdges1, sizeof(INDEXPAIR)*(iPointVectorCount - 1) );

	delete [] pTrees1;
	delete [] pTrees2;
	delete [] piTreePointIndicies1;
	delete [] piTreePointIndicies2;
	delete [] pipTreePointEdges1;
	delete [] pipTreePointEdges2;
	delete [] piTreeIndicies;
	delete [] pfDistances;
	delete [] pipIndexPairTrees;
	delete [] pipIndexPairPoints;
	deleteForest( fForest2 );
	deleteForest( fForest1 );

	return 1;
}



//
// computeMinSpanningTree()
//
// Computes the minumum spanning tree of a set of points.
//
// Returns the result in array pipEdges - an array of INDEXPAIR
// of size (iPointVectorCount - 1) containing index pairs of points
// in pPointVectors.
//
int
computeMinSpanningTree(
					   void			*pPointVectors,
					   int			iPointVectorLengthBytes,
					   int			iPointVectorCount,
					   FLOAT_MIN_TREE_DISTANCE_FUNCTION floatMinTreeDist,
					   void			*pArg,
					   INDEXPAIR	*pipEdges
					   )
{
	// Allocate memory

	float *pfDistances = 0;				// Best distances
	int *piTreeIndicies = 0;			// Maps an old tree to a new merged tree

	INDEXPAIR *pipIndexPairTrees = 0;	// Saves matched trees
	INDEXPAIR *pipIndexPairPoints = 0;	// Saves edges between matched trees

	int *piTreePointIndicies1 = 0;		// Tree point data 1
	int *piTreePointIndicies2 = 0;		// Tree point data 2
	INDEXPAIR *pipTreePointEdges1 = 0;	// Tree edge data 1
	INDEXPAIR *pipTreePointEdges2 = 0;	// Tree edge data 2
	TREE	*pTrees1 = 0;				// Tree data 1
	TREE	*pTrees2 = 0;				// Tree data 2
	FOREST fForest1;					// Forest 1
	FOREST fForest2;					// Forest 2
	memset( &fForest1, 0, sizeof(FOREST) );
	memset( &fForest2, 0, sizeof(FOREST) );

	pfDistances = new float[iPointVectorCount];
	assert( pfDistances );
	if( !pfDistances )
	{
		return 0;
	}

	piTreeIndicies = new int[iPointVectorCount];
	assert( piTreeIndicies );
	if( !piTreeIndicies )
	{
		return 0;
	}

	piTreePointIndicies1 = new int[iPointVectorCount];
	assert( piTreePointIndicies1 );
	if( !piTreePointIndicies1 )
	{
		return 0;
	}

	piTreePointIndicies2 = new int[iPointVectorCount];
	assert( piTreePointIndicies2 );
	if( !piTreePointIndicies2 )
	{
		return 0;
	}

	pipTreePointEdges1 = new INDEXPAIR[iPointVectorCount];
	pipTreePointEdges2 = new INDEXPAIR[iPointVectorCount];

	pipIndexPairTrees = new INDEXPAIR[iPointVectorCount];
	assert( pipIndexPairTrees );
	if( !pipIndexPairTrees )
	{
		return 0;
	}

	pipIndexPairPoints = new INDEXPAIR[iPointVectorCount];
	assert( pipIndexPairPoints );
	if( !pipIndexPairPoints )
	{
		return 0;
	}
	
	pTrees1 = new TREE[iPointVectorCount];
	assert( pTrees1 );
	if( !pTrees1 )
	{
		return 0;
	}
	
	pTrees2 = new TREE[iPointVectorCount];
	assert( pTrees2 );
	if( !pTrees2 )
	{
		return 0;
	}

	if( !initForest( fForest1, iPointVectorCount ) )
	{
		return 0;
	}

	if( !initForest( fForest2, iPointVectorCount ) )
	{
		return 0;
	}

	// Init tree points

	for( int i = 0; i < iPointVectorCount; i++ )
	{
		piTreePointIndicies1[i] = i;
	}

	// Init trees - 1 point per tree, no edges

	for( int i = 0; i < iPointVectorCount; i++ )
	{
		pTrees1[i].iEdgesCount = pTrees2[i].iEdgesCount = 0;
		pTrees1[i].iEdgesIndex = pTrees2[i].iEdgesIndex = -1;
		pTrees1[i].iPointsCount = pTrees2[i].iPointsCount = 1;
		pTrees1[i].iPointsIndex = pTrees2[i].iPointsIndex = i;
	}

	// Init forests

	fForest1.iTreeIndexCountUsed = iPointVectorCount;
	fForest2.iTreeIndexCountUsed = iPointVectorCount;

	for( int i = 0; i < iPointVectorCount; i++ )
	{
		fForest1.piTreeIndicies[i] = fForest2.piTreeIndicies[i] = i;
	}

	// Repeat while there are more than 1 trees remaining

	while( fForest1.iTreeIndexCountUsed > 1 )
	{
		// Set flags

		for( int i = 0; i < iPointVectorCount; i++ )
		{
			pipIndexPairTrees[i][0] = -1;
		}

		// Generate min distances between all trees

		for( int iT1 = 0; iT1 < fForest1.iTreeIndexCountUsed; iT1++ )
		{
			for( int iT2 = iT1 + 1; iT2 < fForest1.iTreeIndexCountUsed; iT2++ )
			{
				INDEXPAIR ipBestPair;
				float fDist =
					floatMinTreeDist(
						pPointVectors, iPointVectorLengthBytes,
						piTreePointIndicies1,
						pTrees1[iT1], pTrees1[iT2],
						pArg, ipBestPair
					);

				if( fDist < pfDistances[iT1] || pipIndexPairTrees[iT1][0] == -1 )
				{
					pfDistances[iT1] = fDist;
					pipIndexPairTrees[iT1][0] = iT1;
					pipIndexPairTrees[iT1][1] = iT2;
					pipIndexPairPoints[iT1][0] = ipBestPair[0];
					pipIndexPairPoints[iT1][1] = ipBestPair[1];
				}

				if( fDist < pfDistances[iT2] || pipIndexPairTrees[iT2][0] == -1 )
				{
					pfDistances[iT2] = fDist;
					pipIndexPairTrees[iT2][0] = iT2;
					pipIndexPairTrees[iT2][1] = iT1;
					pipIndexPairPoints[iT2][0] = ipBestPair[0];
					pipIndexPairPoints[iT2][1] = ipBestPair[1];
				}
			}
		}

//		FILE *outfile = fopen( "indexpairs_trees.txt", "wt" );
//		for( int i = 0; i < fForest1.iTreeIndexCountUsed; i++ )
//		{
//			fprintf( outfile, "%d, %d\n", pipIndexPairTrees[i][0], pipIndexPairTrees[i][1] );
//		}
//		fclose( outfile );
//
//		outfile = fopen( "indexpairs_points.txt", "wt" );
//		for( int i = 0; i < fForest1.iTreeIndexCountUsed; i++ )
//		{
//			fprintf( outfile, "%d, %d\n", pipIndexPairPoints[i][0], pipIndexPairPoints[i][1] );
//		}
//		fclose( outfile );

		// Set flags - all new tree indicies are invalid (-1)
		for( int i = 0; i < fForest1.iTreeIndexCountUsed; i++ )
		{
			piTreeIndicies[i] = -1;
		}

		// Create indicies of new trees to be created after merging

		int iNextNewTree = 0;

		for( int i = 0; i < fForest1.iTreeIndexCountUsed; i++ )
		{
//			outfile = fopen( "next_trees_inc.txt", "wt" );
//			for( int tree = 0; tree < fForest1.iTreeIndexCountUsed; tree++ )
//			{
//				fprintf( outfile, "%d\n", piTreeIndicies[ tree ] );
//			}
//			fclose( outfile );

			int iTree0 = pipIndexPairTrees[i][0];
			int iTree1 = pipIndexPairTrees[i][1];

			assert( iTree0 != iTree1 );
			assert( iTree0 == i );

			if( iTree1 < iTree0 )
			{
				// Skip if this tree pair has already been used
				assert( pipIndexPairTrees[iTree1][0] == iTree1 );
				
				if( pipIndexPairTrees[iTree1][1] == iTree0 )
				{
					assert( piTreeIndicies[ iTree0 ] != -1 && piTreeIndicies[ iTree1 ] != -1 );
					assert( piTreeIndicies[ iTree0 ] == piTreeIndicies[ iTree1 ] );
					continue;
				}
			}

			if( piTreeIndicies[ iTree0 ] == -1 || piTreeIndicies[ iTree1 ] == -1 )
			{
				if( piTreeIndicies[ iTree0 ] == -1 && piTreeIndicies[ iTree1 ] == -1 )
				{
					// Neither tree is merged into a new tree, create a new merge tree
					piTreeIndicies[ iTree0 ] = iNextNewTree;
					piTreeIndicies[ iTree1 ] = iNextNewTree;
					iNextNewTree++;
				}
				else if( piTreeIndicies[ iTree0 ] == -1 )
				{
					//  iTree1 is alread merged into a new tree, add iTree0
					piTreeIndicies[ iTree0 ] = piTreeIndicies[ iTree1 ];
				}
				else if( piTreeIndicies[ iTree1 ] == -1 )
				{
					//  iTree0 is alread merged into a new tree, add iTree1
					piTreeIndicies[ iTree1 ] = piTreeIndicies[ iTree0 ];
				}
			}
			else if( piTreeIndicies[ iTree0 ] != piTreeIndicies[ iTree1 ] )
			{
				// iTree0 and iTree1 merged into different trees but are connected.
				int iMinNextNewTree =
					( piTreeIndicies[ iTree0 ] < piTreeIndicies[ iTree1 ] ? piTreeIndicies[ iTree0 ] : piTreeIndicies[ iTree1 ] );
				int iMaxNextNewTree =
					( piTreeIndicies[ iTree0 ] > piTreeIndicies[ iTree1 ] ? piTreeIndicies[ iTree0 ] : piTreeIndicies[ iTree1 ] );

				assert( iMinNextNewTree < iMaxNextNewTree );

				// Set all trees marked with max to min,
				// decrement all indicies higher than max

				for( int j = 0; j < fForest1.iTreeIndexCountUsed; j++ )
				{
					if( piTreeIndicies[ j ] > iMaxNextNewTree )
					{
						piTreeIndicies[ j ]--;
					}
					else if( piTreeIndicies[ j ] == iMaxNextNewTree )
					{
						piTreeIndicies[ j ] = iMinNextNewTree;
					}
				}

				// Set trees themselves

				piTreeIndicies[ iTree0 ] = iMinNextNewTree;
				piTreeIndicies[ iTree1 ] = iMinNextNewTree;
			
				assert( piTreeIndicies[ iTree0 ] != -1 && piTreeIndicies[ iTree1 ] != -1 );
				assert( piTreeIndicies[ iTree0 ] == piTreeIndicies[ iTree1 ] );

				iNextNewTree--;
			}
		}

//		outfile = fopen( "next_tree_indicies.txt", "wt" );
//		for( int i = 0; i < fForest1.iTreeIndexCountUsed; i++ )
//		{
//			fprintf( outfile, "%d\t(%d, %d)\n", piTreeIndicies[ i ], pipIndexPairTrees[i][0], pipIndexPairTrees[i][1] );
//		}
//		fclose( outfile );

		// Prepare new trees - indicies & counts
		
		fForest2.iTreeIndexCountUsed = iNextNewTree;

		pTrees2[0].iEdgesIndex = 0;
		pTrees2[0].iPointsIndex = 0;

		for( int i = 0; i < fForest2.iTreeIndexCountUsed; i++ )
		{
			pTrees2[i].iPointsCount = 0;
			pTrees2[i].iEdgesCount = 0;

			// Get point/edge count

			int iCount = 0;
			for( int j = 0; j < fForest1.iTreeIndexCountUsed; j++ )
			{
				if( piTreeIndicies[ j ] == i )
				{
					iCount += pTrees1[j].iPointsCount;
				}
			}

			assert( iCount >= 2 );

			pTrees2[i].iPointsCount = iCount;
			pTrees2[i].iEdgesCount = iCount - 1;

			// Set next start index based on count

			if( i < fForest2.iTreeIndexCountUsed - 1  )
			{
				pTrees2[i+1].iPointsIndex =
					pTrees2[i].iPointsIndex + pTrees2[i].iPointsCount;
			}
			if( i < fForest2.iTreeIndexCountUsed - 1  )
			{
				pTrees2[i+1].iEdgesIndex =
					pTrees2[i].iEdgesIndex + pTrees2[i].iEdgesCount;
			}
		}

		// Merge trees with closest distances

		int iNewPointCount = 0;
		int iNewEdgeCount = 0;

		for( int i = 0; i < fForest2.iTreeIndexCountUsed; i++ )
		{
			// Merge all points in trees merging to this new tree

			TREE &treeNew = pTrees2[i];

			assert( treeNew.iPointsIndex == iNewPointCount );
			assert( treeNew.iEdgesIndex == iNewEdgeCount );

			for( int j = 0; j < fForest1.iTreeIndexCountUsed; j++ )
			{
				if( piTreeIndicies[j] == i )
				{
					// Merge old tree j to new tree i

					// Add points to point set

					TREE &treeOld = pTrees1[j];
					memcpy(
						piTreePointIndicies2 + iNewPointCount,
						piTreePointIndicies1 + treeOld.iPointsIndex,
						treeOld.iPointsCount*sizeof(int)
						);
					iNewPointCount += treeOld.iPointsCount;

					// Add old edges to point set

					if( treeOld.iEdgesCount > 0 )
					{
						memcpy(
							pipTreePointEdges2 + iNewEdgeCount,
							pipTreePointEdges1 + treeOld.iEdgesIndex,
							treeOld.iEdgesCount*sizeof(INDEXPAIR)
							);
						iNewEdgeCount += treeOld.iEdgesCount;
					}

					// Add new edge if it has not already been added

					int iTree0 = pipIndexPairTrees[j][0];
					int iTree1 = pipIndexPairTrees[j][1];
					if( iTree1 < iTree0 )
					{
						// Skip if this tree pair has already been used
						assert( pipIndexPairTrees[iTree1][0] == iTree1 );
						
						if( pipIndexPairTrees[iTree1][1] == iTree0 )
						{
							assert( piTreeIndicies[ iTree0 ] != -1 && piTreeIndicies[ iTree1 ] != -1 );
							assert( piTreeIndicies[ iTree0 ] == piTreeIndicies[ iTree1 ] );
							continue;
						}
					}

					pipTreePointEdges2[ iNewEdgeCount ][0] = pipIndexPairPoints[j][0];
					pipTreePointEdges2[ iNewEdgeCount ][1] = pipIndexPairPoints[j][1];
					iNewEdgeCount++;
				}
			}

//			char pcOutfile[200];
//			sprintf( pcOutfile, "tree%d_edges.txt", i );
//			outfile = fopen( pcOutfile, "wt" );
//			for( int edge = 0; edge < treeNew.iEdgesCount; edge++ )
//			{
//				fprintf( outfile, "%d\t%d\n", pipTreePointEdges2[treeNew.iEdgesIndex + edge][0], pipTreePointEdges2[treeNew.iEdgesIndex + edge][1] );
//			}
//			fclose( outfile );
//			
//			sprintf( pcOutfile, "tree%d_points.txt", i );
//			outfile = fopen( pcOutfile, "wt" );
//			for( int point = 0; point < treeNew.iPointsCount; point++ )
//			{
//				fprintf( outfile, "%d\n", piTreePointIndicies2[treeNew.iPointsIndex + point] );
//			}
//			fclose( outfile );
		}

		// Swap old tree data with new

		if( fForest2.iTreeIndexCountUsed == 1 )
		{
			assert( iNewEdgeCount == iPointVectorCount - 1 );
			assert( iNewPointCount == iPointVectorCount );
		}

		copyForest( fForest1, fForest2 );
		memcpy( pTrees1, pTrees2, sizeof(TREE)*fForest2.iTreeIndexCountUsed );
		memcpy( pipTreePointEdges1, pipTreePointEdges2, sizeof(INDEXPAIR)*iNewEdgeCount );
		memcpy( piTreePointIndicies1, piTreePointIndicies2, sizeof(int)*iNewPointCount );
	}

	// Copy edges to user array

	memcpy( pipEdges, pipTreePointEdges1, sizeof(INDEXPAIR)*(iPointVectorCount - 1) );

	delete [] pTrees1;
	delete [] pTrees2;
	delete [] piTreePointIndicies1;
	delete [] piTreePointIndicies2;
	delete [] pipTreePointEdges1;
	delete [] pipTreePointEdges2;
	delete [] piTreeIndicies;
	delete [] pfDistances;
	delete [] pipIndexPairTrees;
	delete [] pipIndexPairPoints;
	deleteForest( fForest2 );
	deleteForest( fForest1 );

	return 1;
}

int
mstCutLinks(
			void			*pPointVectors,
			int			iPointVectorLengthBytes,
			int			iPointVectorCount,
			FLOAT_VECTOR_DISTANCE_FUNCTION floatDist,
			INDEXPAIR	*pipEdges,
			int *piPointToTreeMap,
			MST_CUT_LINK_FUNCTION *pfuncCutLink,
			void *pArg
			)
{
	//
	// Horribly inefficient code, passes through the entire
	// graph N times after each cut to propagate labels. This
	// could be made much more efficient using a stack.
	//
	DATA_PAIR *pdpPairs = new DATA_PAIR[iPointVectorCount-1];
	assert( pdpPairs );

	unsigned char *pucVectors = (unsigned char*)pPointVectors;
	int iEdge;
	for( iEdge = 0; iEdge < iPointVectorCount-1; iEdge++ )
	{
		unsigned char *pucVector1 = pucVectors + pipEdges[iEdge][0]*iPointVectorLengthBytes;
		unsigned char *pucVector2 = pucVectors + pipEdges[iEdge][1]*iPointVectorLengthBytes;

		float fDist = floatDist( iPointVectorLengthBytes, pucVector1, pucVector2, 0 );
		pdpPairs[iEdge].iIndex = iEdge;
		pdpPairs[iEdge].iValue = 100*fDist;
	}

	// Sort
	dpSortDescending( pdpPairs, iPointVectorCount-1 );

	// Create node-to-tree mapping
	int *piNodeToTree = piPointToTreeMap;
	assert( piNodeToTree );
	// Set all nodes to tree 0
	memset( piNodeToTree, 0, sizeof(int)*iPointVectorCount );

	// Cut edges from longest to shortest
	for( iEdge = 0; iEdge < iPointVectorCount-1; iEdge++ )
	{
		// Determine edge to cut: cut makes a new tree
		int iEdgeCut = pdpPairs[iEdge].iIndex;
		int iSeedNode0 = pipEdges[iEdgeCut][0];
		int iSeedNode1 = pipEdges[iEdgeCut][1];

		// Nodes must be from the same tree
		int iTreeCut = piNodeToTree[iSeedNode0];
		assert( iTreeCut == piNodeToTree[iSeedNode0] );
		assert( iTreeCut == piNodeToTree[iSeedNode1] );

		// Invalidate all nodes from tree to cut
		for( int iNode = 0; iNode < iPointVectorCount; iNode++ )
		{
			if( piNodeToTree[iNode] == iTreeCut )
			{
				piNodeToTree[iNode] = -1;
			}
		}

		// Initialize seed nodes
		piNodeToTree[iSeedNode0] = iTreeCut; // Seed node 0 maintains tree index
		piNodeToTree[iSeedNode1] = iEdge+1;  // Seed node 1 gets next node index

		// Propagate new tree labels through all nodes
		int bTreeChanging = 1;
		while( bTreeChanging )
		{
			bTreeChanging = 0;
			// Don't consider edges already cut
			for( int iEdgeFill = iEdge+1; iEdgeFill < iPointVectorCount-1; iEdgeFill++ )
			{
				int iNode0 = pipEdges[ pdpPairs[iEdgeFill].iIndex ][0];
				int iNode1 = pipEdges[ pdpPairs[iEdgeFill].iIndex ][1];

				if( piNodeToTree[iNode0] == piNodeToTree[iSeedNode0] )
				{
					// Tree for neighbour must be invalid or the same as seed node 0
					assert( piNodeToTree[iNode1] == -1
						|| piNodeToTree[iNode1] == piNodeToTree[iSeedNode0] );
					if( piNodeToTree[iNode1] == -1 )
					{
						// Node tree is invalid, set to seed node 0 tree
						bTreeChanging = 1;
						piNodeToTree[iNode1] = piNodeToTree[iSeedNode0];
					}
				}
				else if( piNodeToTree[iNode0] == piNodeToTree[iSeedNode1] )
				{
					// Tree for neighbour must be invalid or the same as seed node 1
					assert( piNodeToTree[iNode1] == -1
						|| piNodeToTree[iNode1] == piNodeToTree[iSeedNode1] );
					if( piNodeToTree[iNode1] == -1 )
					{
						// Node tree is invalid, set to seed node 0 tree
						bTreeChanging = 1;
						piNodeToTree[iNode1] = piNodeToTree[iSeedNode1];
					}
				}

				if( piNodeToTree[iNode1] == piNodeToTree[iSeedNode0] )
				{
					// Tree for neighbour must be invalid or the same as seed node 0
					assert( piNodeToTree[iNode0] == -1
						|| piNodeToTree[iNode0] == piNodeToTree[iSeedNode0] );
					if( piNodeToTree[iNode0] == -1 )
					{
						// Node tree is invalid, set to seed node 0 tree
						bTreeChanging = 1;
						piNodeToTree[iNode0] = piNodeToTree[iSeedNode0];
					}
				}
				else if( piNodeToTree[iNode1] == piNodeToTree[iSeedNode1] )
				{
					// Tree for neighbour must be invalid or the same as seed node 1
					assert( piNodeToTree[iNode0] == -1
						|| piNodeToTree[iNode0] == piNodeToTree[iSeedNode1] );
					if( piNodeToTree[iNode0] == -1 )
					{
						// Node tree is invalid, set to seed node 0 tree
						bTreeChanging = 1;
						piNodeToTree[iNode0] = piNodeToTree[iSeedNode1];
					}
				}
			}
		}

		if( pfuncCutLink )
		{
			pfuncCutLink( piPointToTreeMap, iPointVectorCount, pArg );
		}
	}

	delete [] pdpPairs;

	return 0;
}

int
mstForestStats(
			   void			*pPointVectors,		// Point data
			int			iPointVectorLengthBytes, // Point length in bytes
			int			iPointVectorCount,	// Number of points
			FLOAT_VECTOR_DISTANCE_FUNCTION floatDist, // Point distance function
			INDEXPAIR	*pipEdges,  // Array of iPointVectorCount edges
			int *piPointToTreeMap	// Array of iPointVectorCount indices
							// mapping pPointVectors to a tree
			   )
{
	return 0;
}
//
//
// Dirichlet Process Clustering Algorithm
// To Be Developed!!!! 2012
//
//int
//mstCluster(
//			void			*pPointVectors,
//			int			iPointVectorLengthBytes,
//			int			iPointVectorCount,
//			FLOAT_VECTOR_DISTANCE_FUNCTION floatDist,
//			INDEXPAIR	*pipEdges,
//			int *piPointToTreeMap,
//			MST_CUT_LINK_FUNCTION *pfuncCutLink,
//			void *pArg
//			)
//{
//	//
//	// Horribly inefficient code, passes through the entire
//	// graph N times after each cut to propagate labels. This
//	// could be made much more efficient using a stack.
//	//
//	DATA_PAIR *pdpPairs = new DATA_PAIR[iPointVectorCount-1];
//	assert( pdpPairs );
//
//	unsigned char *pucVectors = (unsigned char*)pPointVectors;
//	int iEdge;
//	for( iEdge = 0; iEdge < iPointVectorCount-1; iEdge++ )
//	{
//		unsigned char *pucVector1 = pucVectors + pipEdges[iEdge][0]*iPointVectorLengthBytes;
//		unsigned char *pucVector2 = pucVectors + pipEdges[iEdge][1]*iPointVectorLengthBytes;
//
//		float fDist = floatDist( iPointVectorLengthBytes, pucVector1, pucVector2, 0 );
//		pdpPairs[iEdge].iIndex = iEdge;
//		pdpPairs[iEdge].iValue = 100*fDist;
//	}
//
//	// Sort ascending
//	dpSortAscending( pdpPairs, iPointVectorCount-1 );
//
//	// Create node-to-tree mapping
//	int *piPointToTree = piPointToTreeMap;
//	assert( piPointToTree );
//	// Set all nodes to tree unique values
//	for( iEdge = 0; iEdge < iPointVectorCount; iEdge++ )
//	{
//		piPointToTree[iEdge] = iEdge;
//	}
//
//	float *pfMeans = new float[2*iPointVectorCount];
//	float *pfVars = new float[iPointVectorCount];
//	float *pfProbs = new float[iPointVectorCount];
//	for( iEdge = 0; iEdge < iPointVectorCount; iEdge++ )
//	{
//		unsigned char *pucVector1 = pucVectors + iEdge*iPointVectorLengthBytes;
//		float *pfVector1 = (float*)pucVector1;
//		pfMeans[iEdge*2 + 0] = pfVector1[0];
//		pfMeans[iEdge*2 + 1] = pfVector1[1];
//		pfVars[iEdge] = 0;
//		pfProbs[iEdge] = 1.0f/(float)iPointVectorCount;
//	}
//
//	// Add edges from longest to shortest
//	for( iEdge = 0; iEdge < iPointVectorCount-1; iEdge++ )
//	{
//		// Determine edge to cut: cut makes a new tree
//		int iEdgeAdd = pdpPairs[iEdge].iIndex;
//		int iSeedNode0 = pipEdges[iEdgeAdd][0];
//		int iSeedNode1 = pipEdges[iEdgeAdd][1];
//		int iClusterLabelNew = piPointToTree[iSeedNode0];
//		int iClusterLabelOld = piPointToTree[iSeedNode1];
//		if( piPointToTree[iSeedNode1] < iClusterLabelNew )
//		{
//			iClusterLabelNew = piPointToTree[iSeedNode1];
//			iClusterLabelOld = piPointToTree[iSeedNode0];
//		}
//
//		// Convert all old labels to new
//		float pfMeanNew[2] = {0,0};
//		int iCount = 0;
//		for( int iNode = 0; iNode < iPointVectorCount; iNode++ )
//		{
//			if( piPointToTree[iNode] == iClusterLabelOld )
//			{
//				piPointToTree[iNode] = iClusterLabelNew;
//			}
//
//			// Compute stats/mean
//			if( piPointToTree[iNode] == iClusterLabelNew )
//			{
//				unsigned char *pucVector1 = pucVectors + iEdge*iPointVectorLengthBytes;
//				float *pfVector1 = (float*)pucVector1;
//				pfMeanNew[2*iNode+0] += pfVector1[0];
//				pfMeanNew[2*iNode+1] += pfVector1[1];
//				iCount++;
//			}
//		}
//
//		assert( iCount >= 1 );
//		pfMeanNew[0] /= (float)iCount;
//		pfMeanNew[1] /= (float)iCount;
//
//		// Re-estimate parameters
//		float fDistSqr = 0;
//		for( int iNode = 0; iNode < iPointVectorCount; iNode++ )
//		{
//			// Compute stats/mean
//			if( piPointToTree[iNode] == iClusterLabelNew )
//			{
//				unsigned char *pucVector1 = pucVectors + iEdge*iPointVectorLengthBytes;
//				float *pfVector1 = (float*)pucVector1;
//				float fdx = pfMeanNew[2*iNode+0] - pfVector1[0];
//				float fdy = pfMeanNew[2*iNode+1] - pfVector1[1];
//				fDistSqr += fdx*fdx + fdy*fdy;
//			}
//		}
//
//		pfMeans[iClusterLabelNew*2 + 0] = pfMeanNew[0];
//		pfMeans[iClusterLabelNew*2 + 1] = pfMeanNew[1];
//		fDistSqr[iEdge] = fDistSqr/(float)iCount;
//		pfProbs[iEdge] = (float)iCount/(float)iPointVectorCount;
//
//		// Compute data likelihood
//		int iCount = 0;
//		for( int iNode = 0; iNode < iPointVectorCount; iNode++ )
//		{
//			if( piPointToTree[iNode] == iClusterLabelOld )
//			{
//				piPointToTree[iNode] = iClusterLabelNew;
//			}
//		}
//
//
//	}
//
//	delete [] pdpPairs;
//
//	return 0;
//}
