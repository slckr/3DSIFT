
#include <assert.h>
#include <search.h>
#include <stdlib.h>
#include <stdio.h>
#include <memory.h>

#include "MinSpanningTreeFeatureIO.h"

typedef struct _EDGE_DIST
{
	INDEXPAIR ip;
	float fDist;
} EDGE_DIST;

static int
sortEdgeDist(
				 const void *elem1,
				 const void *elem2
				 )
{
	EDGE_DIST *pED1 = (EDGE_DIST*)elem1;
	EDGE_DIST *pED2 = (EDGE_DIST*)elem2;

	if( pED1->fDist > pED2->fDist )
	{
		return -1;
	}
	else
	{
		return (pED1->fDist < pED2->fDist );
	}
}

typedef struct _TREE_NN_DIST
{
	int		iTree;		// The tree associated with this pixel
	int		iNNSrc;		// Pixel in this tree linking to nearest neighbour tree
	int		iNNDst;		// Pixel in nearest neighbour tree linking to this tree
	float	fDist;		// Distance between pixels iNNSrc and iNNDst
} TREE_NN_DIST;


static inline int
abs_diff_int( const int &i1, const int &i2 )
{
	return (i1 > i2 ? i1 - i2 : i2 - i1);
}

//
// initNeighbourOffsets()
//
// Initializes an array of offsets to neighbouring pixels
// in an FEATUREIO structure.
//

int
computeNearestNeighbourGraph(
							  FEATUREIO &fio,
							  FLOAT_VECTOR_DISTANCE_FUNCTION floatDist,
							  void			*pArg,
							  INDEXPAIR	*pipEdges	// Branches in Trees
							  )
{

	// Allocate memory

	int iPixels = fio.x*fio.y*fio.z;
	int iTrees = fio.x*fio.y*fio.z;
	TREE_NN_DIST *ptnd = new TREE_NN_DIST[iTrees];
	if( !ptnd )
	{
		return 0;
	}

	int i;

	for( int i = 0; i < iPixels; i++ )
	{
		ptnd[i].iTree = i;
		ptnd[i].iNNSrc = i;
		ptnd[i].iNNDst = -1;
		ptnd[i].fDist = -1;
	}

	int iNeighbourIndexCount;
	int piNeighbourIndices[27];

	int iEdges = 0;

	fioInitNeighbourOffsets( fio, iNeighbourIndexCount, piNeighbourIndices, 0, 1 );

	// In this loop, go through each pixel, updating the nearest neighbour
	// of the pixel and it's corresponding tree.
	
	int iPix1;
	
	for( iPix1 = 0; iPix1 < iPixels; iPix1++ )
	{
		int x1, y1, z1;
		int x2, y2, z2;
		
		assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
		assert( ptnd[iPix1].iTree <= iPix1 );
		
		fioIndexToCoord( fio, iPix1, x1, y1, z1 );
		
		for( int iN = 0; iN < iNeighbourIndexCount; iN++ )
		{
			// Test to see if neighbour is within image, i.e. a valid
			// coordinate. Deviation cannot be more than 1 in x, y, or z
			// directions
			
			int iPix2 = iPix1 + piNeighbourIndices[iN];
			fioIndexToCoord( fio, iPix2, x2, y2, z2 );
			
			assert( x2 >= 0 && y2 >= 0 && z2 >= 0 );
			
			if(
				//x2 >= 0 && y2 >= 0 && z2 >= 0 &&
				abs_diff_int(x1,x2) <= 1 && abs_diff_int(y1,y2) <= 1 && abs_diff_int(z1,z2) <= 1
				//x2 < fio.x && y2 < fio.y && z2 < fio.z
				)
			{
				// Only calculate distances for unmerged trees,
				// where the index of tree1 is less than tree2 (symetry)
				
				// Removing this condition should not change the functioning of the code,
				// just result in more distance calculations
				
				assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
				assert( iPix2 >= 0 && iPix2 < fio.x*fio.y*fio.z );
				
				if( ptnd[iPix1].iTree != ptnd[iPix2].iTree )
				{
					float fDist = floatDist( fio.iFeaturesPerVector*sizeof(float),
						fio.pfVectors + iPix1*fio.iFeaturesPerVector,
						fio.pfVectors + iPix2*fio.iFeaturesPerVector, 0
						);
					
					// Update min distances and neighbours for trees
					
					assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
					assert( iPix2 >= 0 && iPix2 < fio.x*fio.y*fio.z );
					assert( ptnd[iPix1].iTree >= 0 && ptnd[iPix1].iTree < fio.x*fio.y*fio.z );
					assert( ptnd[iPix2].iTree >= 0 && ptnd[iPix2].iTree < fio.x*fio.y*fio.z );
					
					if( ptnd[ptnd[iPix1].iTree].iNNDst == -1 || fDist < ptnd[ptnd[iPix1].iTree].fDist )
					{
						//ptnd[iPix1].iNNSrc = iPix1;
						//ptnd[iPix1].iNNDst = iPix2; //ptnd[iPix2].iTree;
						ptnd[ptnd[iPix1].iTree].iNNSrc = iPix1;
						ptnd[ptnd[iPix1].iTree].iNNDst = iPix2; //ptnd[iPix2].iTree;
						ptnd[ptnd[iPix1].iTree].fDist = fDist;
					}
					if( ptnd[ptnd[iPix2].iTree].iNNDst == -1 || fDist < ptnd[ptnd[iPix2].iTree].fDist )
					{
						//ptnd[iPix2].iNNSrc = iPix2;
						//ptnd[iPix2].iNNDst = iPix1; //ptnd[iPix1].iTree;
						ptnd[ptnd[iPix2].iTree].iNNSrc = iPix2;
						ptnd[ptnd[iPix2].iTree].iNNDst = iPix1; //ptnd[iPix1].iTree;
						ptnd[ptnd[iPix2].iTree].fDist = fDist;
					}
				}
			}
		}
	}
	
	// Merge each pixel/tree into it's nearest neighbour tree
	// Algorithm:
	//	1) traverse pixels from 1 to N
	//	2) after processing pixel k, all iTree values for
	//		pixels 1..k are assigned their true, unique tree.
	for( iPix1 = 0; iPix1 < iPixels; iPix1++ )
	{
		if( ptnd[iPix1].iNNDst != -1 )
		{
			int iTree1 = ptnd[iPix1].iNNSrc;
			while( ptnd[iTree1].iTree != iTree1 )
			{
				iTree1 = ptnd[iTree1].iTree;
			}
			
			int iTree2 = ptnd[iPix1].iNNDst;
			while( ptnd[iTree2].iTree != iTree2 )
			{
				iTree2 = ptnd[iTree2].iTree;
			}
			
			if( iTree1 != iTree2 )
			{
				//
				// Trees haven't already been merged
				// The iTree field of iPix1 may have been modified
				// Check just that the edge is not bidirectional
				//
				
				pipEdges[iEdges][0] = ptnd[iPix1].iNNSrc;
				pipEdges[iEdges][1] = ptnd[iPix1].iNNDst;
				iEdges++;
				//iTrees--; // Each edge removes a tree
				
				if( iTree1 < iTree2 )
				{
					ptnd[iTree2].iTree = iTree1;
				}
				else
				{
					ptnd[iTree1].iTree = iTree2;
				}
			}
		}
		else
		{
			// iPix1 is part of a tree but not a tree index
			// tree index may have changed, reset
			//assert( ptnd[iPix1].iTree < iPix1 );
			//assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
			//assert( ptnd[iPix1].iTree >= 0 && ptnd[iPix1].iTree < fio.x*fio.y*fio.z );
			//ptnd[iPix1].iTree = ptnd[ptnd[iPix1].iTree].iTree;
		}

		assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
		assert( ptnd[iPix1].iTree <= iPix1 );

		// Flag nearest neighbour/distance as invalid

		ptnd[iPix1].iNNDst = -1;
		//ptnd[iPix1].fDist = -1;
		
	}
	
	// Check to see that things were merged properly
	for( iPix1 = 0; iPix1 < iPixels; iPix1++ )
	{
		ptnd[iPix1].iTree = ptnd[ptnd[iPix1].iTree].iTree;
		assert( ptnd[iPix1].iTree <= iPix1 );
		assert( ptnd[ptnd[iPix1].iTree].iTree == ptnd[iPix1].iTree );
	}

	delete [] ptnd;

	return iEdges;
}


/*
//
// minTreeDistance()
//
// Calculates the minimum distance between two trees. Minimum distance
// is the minimum distance between any two points, one from tree1 and 
// one from tree2.
// Indicies of the actual points resulting in the min distance are returned
// in ipBestPair.
//
static float
minTreeDistance(
				FEATUREIO &fio,
				const int			*piPointIndicies,
				const TREE			&tree1,
				const TREE			&tree2,
				FLOAT_VECTOR_DISTANCE_FUNCTION floatDist,
				INDEXPAIR			&ipBestPair
			)
{
	assert( tree1.iPointsCount > 0 && tree2.iPointsCount > 0 );
	if( !(tree1.iPointsCount > 0 && tree2.iPointsCount > 0 ) )
	{
		return 0.0;
	}

	const int *piPoints1 = piPointIndicies + tree1.iPointsIndex;
	const int *piPoints2 = piPointIndicies + tree2.iPointsIndex;

	float *pfVector1 = fio.pfVectors + piPoints1[0]*fio.iFeaturesPerVector;
	float *pfVector2 = fio.pfVectors + piPoints2[0]*fio.iFeaturesPerVector;

	float fMinDist = floatDist( iVectorLengthBytes, pucVector1, pucVector2 );
	int iMinI1 = 0;
	int iMinI2 = 0;
	ipBestPair[0] = piPoints1[0];
	ipBestPair[1] = piPoints2[0];

	// Only test neighbours

	for( int i1 = 0; i1 < tree1.iPointsCount; i1++ )
	{
		pucVector1 = pucVectors + piPoints1[i1]*iVectorLengthBytes;

		for( int i2 = 0; i2 < tree2.iPointsCount; i2++ )
		{
			pucVector2 = pucVectors + piPoints2[i2]*iVectorLengthBytes;

			float fDist = floatDist( iVectorLengthBytes, pucVector1, pucVector2 );
			if( fDist < fMinDist )
			{
				fMinDist = fDist;
				iMinI1 = i1;
				iMinI2 = i2;
				ipBestPair[0] = piPoints1[i1];
				ipBestPair[1] = piPoints2[i2];
			}
		}
	}

	return fMinDist;
}

*/

int
computeMinSpanningTreeFeatureIO(
								FEATUREIO &fio,
								FLOAT_VECTOR_DISTANCE_FUNCTION floatDist,
								void			*pArg,
								INDEXPAIR	*pipEdges
								)
{
	// Allocate memory

	int iPixels = fio.x*fio.y*fio.z;
	int iTrees = fio.x*fio.y*fio.z;
	TREE_NN_DIST *ptnd = new TREE_NN_DIST[iTrees];
	if( !ptnd )
	{
		return 0;
	}

	int i;

	for( int i = 0; i < iPixels; i++ )
	{
		ptnd[i].iTree = i;
		ptnd[i].iNNSrc = i;
		ptnd[i].iNNDst = -1;
		ptnd[i].fDist = -1;
	}

	int iNeighbourIndexCount;
	int piNeighbourIndices[27];

	int iEdges = 0;

	fioInitNeighbourOffsets( fio, iNeighbourIndexCount, piNeighbourIndices, 0, 1 );

	// In this loop, we find nearest neighbour trees and merge them until
	// only 1 tree is left.
	while( iTrees > 1 )
	{
		// In this loop, go through each pixel, updating the nearest neighbour
		// of the pixel and it's corresponding tree.

		int iPix1;

		for( iPix1 = 0; iPix1 < iPixels; iPix1++ )
		{
			int x1, y1, z1;
			int x2, y2, z2;

			assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
			assert( ptnd[iPix1].iTree <= iPix1 );

			fioIndexToCoord( fio, iPix1, x1, y1, z1 );

			for( int iN = 0; iN < iNeighbourIndexCount; iN++ )
			{
				// Test to see if neighbour is within image, i.e. a valid
				// coordinate. Deviation cannot be more than 1 i ccc vv,n x, y, or z
				// directions

				int iPix2 = iPix1 + piNeighbourIndices[iN];
				fioIndexToCoord( fio, iPix2, x2, y2, z2 );

				assert( x2 >= 0 && y2 >= 0 && z2 >= 0 );

				if(
					//x2 >= 0 && y2 >= 0 && z2 >= 0 &&
					abs_diff_int(x1,x2) <= 1 && abs_diff_int(y1,y2) <= 1 && abs_diff_int(z1,z2) <= 1
					//x2 < fio.x && y2 < fio.y && z2 < fio.z
					)
				{
					// Only calculate distances for unmerged trees,
					// where the index of tree1 is less than tree2 (symetry)

					// Removing this condition should not change the functioning of the code,
					// just result in more distance calculations

					assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
					assert( iPix2 >= 0 && iPix2 < fio.x*fio.y*fio.z );

					if( ptnd[iPix1].iTree != ptnd[iPix2].iTree )
					{
						float fDist = floatDist( fio.iFeaturesPerVector*sizeof(float),
							fio.pfVectors + iPix1*fio.iFeaturesPerVector,
							fio.pfVectors + iPix2*fio.iFeaturesPerVector, 0
							);

						// Update min distances and neighbours for trees
							
						assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
						assert( iPix2 >= 0 && iPix2 < fio.x*fio.y*fio.z );
						assert( ptnd[iPix1].iTree >= 0 && ptnd[iPix1].iTree < fio.x*fio.y*fio.z );
						assert( ptnd[iPix2].iTree >= 0 && ptnd[iPix2].iTree < fio.x*fio.y*fio.z );

						if( ptnd[ptnd[iPix1].iTree].iNNDst == -1 || fDist < ptnd[ptnd[iPix1].iTree].fDist )
						{
							//ptnd[iPix1].iNNSrc = iPix1;
							//ptnd[iPix1].iNNDst = iPix2; //ptnd[iPix2].iTree;
							ptnd[ptnd[iPix1].iTree].iNNSrc = iPix1;
							ptnd[ptnd[iPix1].iTree].iNNDst = iPix2; //ptnd[iPix2].iTree;
							ptnd[ptnd[iPix1].iTree].fDist = fDist;
						}
						if( ptnd[ptnd[iPix2].iTree].iNNDst == -1 || fDist < ptnd[ptnd[iPix2].iTree].fDist )
						{
							//ptnd[iPix2].iNNSrc = iPix2;
							//ptnd[iPix2].iNNDst = iPix1; //ptnd[iPix1].iTree;
							ptnd[ptnd[iPix2].iTree].iNNSrc = iPix2;
							ptnd[ptnd[iPix2].iTree].iNNDst = iPix1; //ptnd[iPix1].iTree;
							ptnd[ptnd[iPix2].iTree].fDist = fDist;
						}
					}
				}
			}
		}

		// Merge each pixel/tree into it's nearest neighbour tree
		// Algorithm:
		//	1) traverse pixels from 1 to N
		//	2) after processing pixel k, all iTree values for
		//		pixels 1..k are assigned their true, unique tree.
		for( iPix1 = 0; iPix1 < iPixels; iPix1++ )
		{
			if( ptnd[iPix1].iNNDst != -1 )
			{
				int iTree1 = ptnd[iPix1].iNNSrc;
				while( ptnd[iTree1].iTree != iTree1 )
				{
					iTree1 = ptnd[iTree1].iTree;
				}

				int iTree2 = ptnd[iPix1].iNNDst;
				while( ptnd[iTree2].iTree != iTree2 )
				{
					iTree2 = ptnd[iTree2].iTree;
				}

				if( iTree1 != iTree2 )
				{
					//
					// Trees haven't already been merged
					// The iTree field of iPix1 may have been modified
					// Check just that the edge is not bidirectional
					//

					pipEdges[iEdges][0] = ptnd[iPix1].iNNSrc;
					pipEdges[iEdges][1] = ptnd[iPix1].iNNDst;
					iEdges++;
					//iTrees--; // Each edge removes a tree

					if( iTree1 < iTree2 )
					{
						ptnd[iTree2].iTree = iTree1;
					}
					else
					{
						ptnd[iTree1].iTree = iTree2;
					}
				}
			}
			else
			{
				// iPix1 is part of a tree but not a tree index
				// tree index may have changed, reset
				//assert( ptnd[iPix1].iTree < iPix1 );
				//assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
				//assert( ptnd[iPix1].iTree >= 0 && ptnd[iPix1].iTree < fio.x*fio.y*fio.z );
				//ptnd[iPix1].iTree = ptnd[ptnd[iPix1].iTree].iTree;
			}

			assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
			assert( ptnd[iPix1].iTree <= iPix1 );
			
			// Flag nearest neighbour/distance as invalid

			ptnd[iPix1].iNNDst = -1;
			//ptnd[iPix1].fDist = -1;

		}
		
		// Check to see that things were merged properly
		for( iPix1 = 0; iPix1 < iPixels; iPix1++ )
		{
			ptnd[iPix1].iTree = ptnd[ptnd[iPix1].iTree].iTree;
			assert( ptnd[iPix1].iTree <= iPix1 );
			assert( ptnd[ptnd[iPix1].iTree].iTree == ptnd[iPix1].iTree );
		}

		// Count unique trees - this can probably be done more efficiently
		iTrees = 0;
		for( iPix1 = 0; iPix1 < iPixels; iPix1++ )
		{
			assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
			if( ptnd[iPix1].iTree == iPix1 )
			{
				iTrees++;
			}
		}
	}

	delete [] ptnd;

	return iEdges;
}

int
computeBlobsFeatureIO(
					  FEATUREIO &fio,
					  FLOAT_VECTOR_DISTANCE_FUNCTION floatDist,
					  float			fMaxAcceptableDist,
					  void			*pArg,
					  INDEXPAIR	*pipEdges
					  )
{
	// Allocate memory

	int iPixels = fio.x*fio.y*fio.z;
	int iTrees = fio.x*fio.y*fio.z;
	TREE_NN_DIST *ptnd = new TREE_NN_DIST[iTrees];
	if( !ptnd )
	{
		return 0;
	}

	int i;

	for( int i = 0; i < iPixels; i++ )
	{
		ptnd[i].iTree = i;
		ptnd[i].iNNSrc = i;
		ptnd[i].iNNDst = -1;
		ptnd[i].fDist = -1;
	}

	int iNeighbourIndexCount;
	int piNeighbourIndices[27];

	int iEdges = 0;

	fioInitNeighbourOffsets( fio, iNeighbourIndexCount, piNeighbourIndices, 0, 1 );

	// In this loop, we find nearest neighbour trees and merge them until
	// only 1 tree is left.

	int iPreviousTrees = -1;

	while( iPreviousTrees == -1 || iTrees != iPreviousTrees )
	{
		// In this loop, go through each pixel, updating the nearest neighbour
		// of the pixel and it's corresponding tree.

		int iPix1;

		for( iPix1 = 0; iPix1 < iPixels; iPix1++ )
		{
			int x1, y1, z1;
			int x2, y2, z2;

			assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
			assert( ptnd[iPix1].iTree <= iPix1 );

			fioIndexToCoord( fio, iPix1, x1, y1, z1 );

			for( int iN = 0; iN < iNeighbourIndexCount; iN++ )
			{
				// Test to see if neighbour is within image, i.e. a valid
				// coordinate. Deviation cannot be more than 1 in x, y, or z
				// directions

				int iPix2 = iPix1 + piNeighbourIndices[iN];
				fioIndexToCoord( fio, iPix2, x2, y2, z2 );

				assert( x2 >= 0 && y2 >= 0 && z2 >= 0 );

				if(
					//x2 >= 0 && y2 >= 0 && z2 >= 0 &&
					abs_diff_int(x1,x2) <= 1 && abs_diff_int(y1,y2) <= 1 && abs_diff_int(z1,z2) <= 1
					//x2 < fio.x && y2 < fio.y && z2 < fio.z
					)
				{
					// Only calculate distances for unmerged trees,
					// where the index of tree1 is less than tree2 (symetry)

					// Removing this condition should not change the functioning of the code,
					// just result in more distance calculations

					assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
					assert( iPix2 >= 0 && iPix2 < fio.x*fio.y*fio.z );
					
					if( ptnd[iPix1].iTree != ptnd[iPix2].iTree )
					{
						float fDist = floatDist( fio.iFeaturesPerVector*sizeof(float),
							fio.pfVectors + iPix1*fio.iFeaturesPerVector,
							fio.pfVectors + iPix2*fio.iFeaturesPerVector, 0
							);
						
						// Only continue if nearest distance is less than
						// the acceptable distance.

						if( fDist < fMaxAcceptableDist )
						{
							
							// Update min distances and neighbours for trees
							
							assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
							assert( iPix2 >= 0 && iPix2 < fio.x*fio.y*fio.z );
							assert( ptnd[iPix1].iTree >= 0 && ptnd[iPix1].iTree < fio.x*fio.y*fio.z );
							assert( ptnd[iPix2].iTree >= 0 && ptnd[iPix2].iTree < fio.x*fio.y*fio.z );
							
							if( ptnd[ptnd[iPix1].iTree].iNNDst == -1 || fDist < ptnd[ptnd[iPix1].iTree].fDist )
							{
								//ptnd[iPix1].iNNSrc = iPix1;
								//ptnd[iPix1].iNNDst = iPix2; //ptnd[iPix2].iTree;
								ptnd[ptnd[iPix1].iTree].iNNSrc = iPix1;
								ptnd[ptnd[iPix1].iTree].iNNDst = iPix2; //ptnd[iPix2].iTree;
								ptnd[ptnd[iPix1].iTree].fDist = fDist;
							}
							if( ptnd[ptnd[iPix2].iTree].iNNDst == -1 || fDist < ptnd[ptnd[iPix2].iTree].fDist )
							{
								//ptnd[iPix2].iNNSrc = iPix2;
								//ptnd[iPix2].iNNDst = iPix1; //ptnd[iPix1].iTree;
								ptnd[ptnd[iPix2].iTree].iNNSrc = iPix2;
								ptnd[ptnd[iPix2].iTree].iNNDst = iPix1; //ptnd[iPix1].iTree;
								ptnd[ptnd[iPix2].iTree].fDist = fDist;
							}
						}
					}
				}
			}
		}

		// Merge each pixel/tree into it's nearest neighbour tree
		// Algorithm:
		//	1) traverse pixels from 1 to N
		//	2) after processing pixel k, all iTree values for
		//		pixels 1..k are assigned their true, unique tree.
		for( iPix1 = 0; iPix1 < iPixels; iPix1++ )
		{
			if( ptnd[iPix1].iNNDst != -1 )
			{
				int iTree1 = ptnd[iPix1].iNNSrc;
				while( ptnd[iTree1].iTree != iTree1 )
				{
					iTree1 = ptnd[iTree1].iTree;
				}

				int iTree2 = ptnd[iPix1].iNNDst;
				while( ptnd[iTree2].iTree != iTree2 )
				{
					iTree2 = ptnd[iTree2].iTree;
				}

				if( iTree1 != iTree2 )
				{
					//
					// Trees haven't already been merged
					// The iTree field of iPix1 may have been modified
					// Check just that the edge is not bidirectional
					//

					pipEdges[iEdges][0] = ptnd[iPix1].iNNSrc;
					pipEdges[iEdges][1] = ptnd[iPix1].iNNDst;
					iEdges++;
					//iTrees--; // Each edge removes a tree

					if( iTree1 < iTree2 )
					{
						ptnd[iTree2].iTree = iTree1;
					}
					else
					{
						ptnd[iTree1].iTree = iTree2;
					}
				}
			}
			else
			{
				// iPix1 is part of a tree but not a tree index
				// tree index may have changed, reset
				//assert( ptnd[iPix1].iTree < iPix1 );
				//assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
				//assert( ptnd[iPix1].iTree >= 0 && ptnd[iPix1].iTree < fio.x*fio.y*fio.z );
				//ptnd[iPix1].iTree = ptnd[ptnd[iPix1].iTree].iTree;
			}

			assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
			assert( ptnd[iPix1].iTree <= iPix1 );
			
			// Flag nearest neighbour/distance as invalid

			ptnd[iPix1].iNNDst = -1;
			//ptnd[iPix1].fDist = -1;

		}
		
		// Check to see that things were merged properly
		for( iPix1 = 0; iPix1 < iPixels; iPix1++ )
		{
			ptnd[iPix1].iTree = ptnd[ptnd[iPix1].iTree].iTree;
			assert( ptnd[iPix1].iTree <= iPix1 );
			assert( ptnd[ptnd[iPix1].iTree].iTree == ptnd[iPix1].iTree );
		}

		// Count unique trees - this can probably be done more efficiently
		iPreviousTrees = iTrees;
		iTrees = 0;
		for( iPix1 = 0; iPix1 < iPixels; iPix1++ )
		{
			assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
			if( ptnd[iPix1].iTree == iPix1 )
			{
				iTrees++;
			}
		}
	}

	delete [] ptnd;

	return iEdges;
}

int
computeMutualNearestNeighbourGraph(
								FEATUREIO &fio,
								FLOAT_VECTOR_DISTANCE_FUNCTION floatDist,
								void			*pArg,
								INDEXPAIR	*pipEdges,
								int iSteps
								)
{
	// Allocate memory

	int iPixels = fio.x*fio.y*fio.z;
	int iTrees = fio.x*fio.y*fio.z;
	TREE_NN_DIST *ptnd = new TREE_NN_DIST[iTrees];
	if( !ptnd )
	{
		return 0;
	}

	int i;

	for( int i = 0; i < iPixels; i++ )
	{
		ptnd[i].iTree = i;
		ptnd[i].iNNSrc = i;
		ptnd[i].iNNDst = -1;
		ptnd[i].fDist = -1;
	}

	int iNeighbourIndexCount;
	int piNeighbourIndices[27];

	int iEdges = 0;

	fioInitNeighbourOffsets( fio, iNeighbourIndexCount, piNeighbourIndices, 0, 1 );

	// In this loop, we find nearest neighbour trees and merge them until
	// only 1 tree is left.

	int iStep = 0;

	while( iTrees > 1 || iStep < iSteps )
	{
		// In this loop, go through each pixel, updating the nearest neighbour
		// of the pixel and it's corresponding tree.

		int iPix1;

		for( iPix1 = 0; iPix1 < iPixels; iPix1++ )
		{
			int x1, y1, z1;
			int x2, y2, z2;

			assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
			assert( ptnd[iPix1].iTree <= iPix1 );

			fioIndexToCoord( fio, iPix1, x1, y1, z1 );

			for( int iN = 0; iN < iNeighbourIndexCount; iN++ )
			{
				// Test to see if neighbour is within image, i.e. a valid
				// coordinate. Deviation cannot be more than 1 in x, y, or z
				// directions

				int iPix2 = iPix1 + piNeighbourIndices[iN];
				fioIndexToCoord( fio, iPix2, x2, y2, z2 );

				assert( x2 >= 0 && y2 >= 0 && z2 >= 0 );

				if(
					//x2 >= 0 && y2 >= 0 && z2 >= 0 &&
					abs_diff_int(x1,x2) <= 1 && abs_diff_int(y1,y2) <= 1 && abs_diff_int(z1,z2) <= 1
					//x2 < fio.x && y2 < fio.y && z2 < fio.z
					)
				{
					// Only calculate distances for unmerged trees,
					// where the index of tree1 is less than tree2 (symetry)

					// Removing this condition should not change the functioning of the code,
					// just result in more distance calculations

					assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
					assert( iPix2 >= 0 && iPix2 < fio.x*fio.y*fio.z );

					if( ptnd[iPix1].iTree != ptnd[iPix2].iTree )
					{
						float fDist = floatDist( fio.iFeaturesPerVector*sizeof(float),
							fio.pfVectors + iPix1*fio.iFeaturesPerVector,
							fio.pfVectors + iPix2*fio.iFeaturesPerVector, 0
							);

						// Update min distances and neighbours for trees
							
						assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
						assert( iPix2 >= 0 && iPix2 < fio.x*fio.y*fio.z );
						assert( ptnd[iPix1].iTree >= 0 && ptnd[iPix1].iTree < fio.x*fio.y*fio.z );
						assert( ptnd[iPix2].iTree >= 0 && ptnd[iPix2].iTree < fio.x*fio.y*fio.z );

						if( ptnd[ptnd[iPix1].iTree].iNNDst == -1 || fDist < ptnd[ptnd[iPix1].iTree].fDist )
						{
							//ptnd[iPix1].iNNSrc = iPix1;
							//ptnd[iPix1].iNNDst = iPix2; //ptnd[iPix2].iTree;
							ptnd[ptnd[iPix1].iTree].iNNSrc = iPix1;
							ptnd[ptnd[iPix1].iTree].iNNDst = iPix2; //ptnd[iPix2].iTree;
							ptnd[ptnd[iPix1].iTree].fDist = fDist;
						}
						if( ptnd[ptnd[iPix2].iTree].iNNDst == -1 || fDist < ptnd[ptnd[iPix2].iTree].fDist )
						{
							//ptnd[iPix2].iNNSrc = iPix2;
							//ptnd[iPix2].iNNDst = iPix1; //ptnd[iPix1].iTree;
							ptnd[ptnd[iPix2].iTree].iNNSrc = iPix2;
							ptnd[ptnd[iPix2].iTree].iNNDst = iPix1; //ptnd[iPix1].iTree;
							ptnd[ptnd[iPix2].iTree].fDist = fDist;
						}
					}
				}
			}
		}

		// Merge each pixel/tree into it's nearest neighbour tree
		// Algorithm:
		//	1) traverse pixels from 1 to N
		//	2) after processing pixel k, all iTree values for
		//		pixels 1..k are assigned their true, unique tree.

		for( iPix1 = 0; iPix1 < iPixels; iPix1++ )
		{
			if( ptnd[iPix1].iNNDst != -1 )
			{
				int iTree1 = ptnd[iPix1].iNNSrc;
				while( ptnd[iTree1].iTree != iTree1 )
				{
					iTree1 = ptnd[iTree1].iTree;
				}

				int iTree2 = ptnd[iPix1].iNNDst;
				while( ptnd[iTree2].iTree != iTree2 )
				{
					iTree2 = ptnd[iTree2].iTree;
				}

				if( iTree1 != iTree2 )
				{
					//
					// Trees haven't already been merged
					// The iTree field of iPix1 may have been modified
					// Check just that the edge is not bidirectional
					//

					pipEdges[iEdges][0] = ptnd[iPix1].iNNSrc;
					pipEdges[iEdges][1] = ptnd[iPix1].iNNDst;
					iEdges++;
					//iTrees--; // Each edge removes a tree

					if( iTree1 < iTree2 )
					{
						ptnd[iTree2].iTree = iTree1;
					}
					else
					{
						ptnd[iTree1].iTree = iTree2;
					}
				}
			}
			else
			{
				// iPix1 is part of a tree but not a tree index
				// tree index may have changed, reset
				//assert( ptnd[iPix1].iTree < iPix1 );
				//assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
				//assert( ptnd[iPix1].iTree >= 0 && ptnd[iPix1].iTree < fio.x*fio.y*fio.z );
				//ptnd[iPix1].iTree = ptnd[ptnd[iPix1].iTree].iTree;
			}

			assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
			assert( ptnd[iPix1].iTree <= iPix1 );
			
			// Flag nearest neighbour/distance as invalid

			ptnd[iPix1].iNNDst = -1;
			//ptnd[iPix1].fDist = -1;

		}
		
		// Check to see that things were merged properly
		for( iPix1 = 0; iPix1 < iPixels; iPix1++ )
		{
			ptnd[iPix1].iTree = ptnd[ptnd[iPix1].iTree].iTree;
			assert( ptnd[iPix1].iTree <= iPix1 );
			assert( ptnd[ptnd[iPix1].iTree].iTree == ptnd[iPix1].iTree );
		}

		// Count unique trees - this can probably be done more efficiently
		iTrees = 0;
		for( iPix1 = 0; iPix1 < iPixels; iPix1++ )
		{
			assert( iPix1 >= 0 && iPix1 < fio.x*fio.y*fio.z );
			if( ptnd[iPix1].iTree == iPix1 )
			{
				iTrees++;
			}
		}
	}

	delete [] ptnd;

	return iEdges;
}

int
createDendriteGraph(
					FEATUREIO	&fio,
					FLOAT_VECTOR_DISTANCE_FUNCTION floatDist,
					INDEXPAIR	*pipEdges,
					int			iEdgeCount
					)
{
	EDGE_DIST *ped = new EDGE_DIST[iEdgeCount];
	if( !ped )
	{
		return 1;
	}

	int i;

	for( int i = 0; i < iEdgeCount; i++ )
	{
		ped[i].ip[0] = pipEdges[i][0];
		ped[i].ip[1] = pipEdges[i][1];
		ped[i].fDist = floatDist(
			fio.iFeaturesPerVector*sizeof(float),
			fio.pfVectors + ped[i].ip[0]*fio.iFeaturesPerVector,
			fio.pfVectors + ped[i].ip[1]*fio.iFeaturesPerVector, 0
			);
	}

	qsort( ped, iEdgeCount, sizeof(EDGE_DIST), sortEdgeDist );

	//FILE *outfile = fopen( "edges.txt", "wt" );
	for( int i = 0; i < iEdgeCount; i++ )
	{
		pipEdges[i][0] = ped[i].ip[0];
		pipEdges[i][1] = ped[i].ip[1];
		//fprintf( outfile, "%f\n", ped[i].fDist );
	}
	//fclose( outfile );

	delete [] ped;

	return 1;
}
