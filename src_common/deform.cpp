
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>

#include "deform.h"
#include "RotateRegion.h"

int
defCreateArray(
			   int iSize,
			   DEFCOORD_VALUE_ARRAY &cvaLatice
			   )
{
	assert( iSize > 0 );
	if( iSize <= 0 )
	{
		return 0;
	}
	
	cvaLatice.iCount = iSize;
	cvaLatice.pcv = new DEFCOORD_VALUE[iSize];
	
	assert( cvaLatice.pcv );
	if( cvaLatice.pcv == 0 )
	{
		return 0;
	}
	
	return 1;
}

//
// defCreateArray()
//
// Destroy a DEFCOORD_VALUE_ARRAY.
//
int
defDestroyArray(
				DEFCOORD_VALUE_ARRAY &cvaLatice
				)
{
	assert( cvaLatice.pcv );
	if( cvaLatice.pcv == 0 )
	{
		return 0;
	}
	
	delete [] cvaLatice.pcv;
	
	cvaLatice.pcv = 0;
	cvaLatice.iCount = 0;
	
	return 1;
}

//
// defCreateUniformLatice()
//
// Calculates an an array of coordinates on a
// square latice.
//
int
defCreateSquareLatice(
					  DEFCOORD &cStart,
					  DEFCOORD &cFinish,
					  int iSize,
					  DEFCOORD_VALUE_ARRAY &cvaLatice
					  )
{
	if( !defCreateArray( (iSize+1)*(iSize+1), cvaLatice ) )
	{
		return 0;
	}
	
	float fXInc = (cFinish.fX - cStart.fX) / (float)(iSize);
	float fYInc = (cFinish.fY - cStart.fY) / (float)(iSize);
	
	DEFCOORD_VALUE	*pcvCurr = cvaLatice.pcv;
	
	for( int x = 0; x <= iSize; x++ )
	{
		for( int y = 0; y <= iSize; y++ )
		{
			pcvCurr->pos.fY = y*fYInc;
			pcvCurr->pos.fX = x*fXInc;
			if( x == 0 || x == iSize || y == 0 || y == iSize )
			{
				pcvCurr->fValue = 0;
			}
			else
			{
				pcvCurr->fValue = 1;
			}
			pcvCurr++;
		}
	}
	
	return 1;
}

//
// defCreateRandomDeformation()
//
// Perform a deformation on non-boundary points of cvaLaticeIn,
// create a new array of points in cvaLaticeOut.
//

float
ranf()
{
	return (float)(((double)(rand())) / ((double)(RAND_MAX + 1.0)));
}

static void
random_Gaussian_coord(
					  float fSigma,
					  DEFCOORD &cCoord
					  )
{
	float x1, x2, w, y1, y2;
	
	do
	{
		x1 = 2.0f * ranf() - 1.0f;
		x2 = 2.0f * ranf() - 1.0f;
		w = x1 * x1 + x2 * x2;
	} while ( w >= 1.0 );
	
	w = (float)sqrt( (-2.0 * log( w ) ) / w );
	y1 = x1 * w;
	y2 = x2 * w;
	
	cCoord.fX = fSigma*y1;
	cCoord.fY = fSigma*y2;
}



int
defCreateRandomDeformation(
						   DEFCOORD_VALUE_ARRAY &cvaLaticeIn,
						   DEFCOORD_VALUE_ARRAY &cvaLaticeOut,
						   float fSigma,
						   float fMaxDeviation
						   )
{
	if( !defCreateArray( cvaLaticeIn.iCount, cvaLaticeOut ) )
	{
		return 0;
	}
	
	DEFCOORD_VALUE	*pcvCurr = cvaLaticeIn.pcv;
	
	for( int c = 0; c < cvaLaticeIn.iCount; c++ )
	{
		DEFCOORD_VALUE &cvIn  = cvaLaticeIn.pcv[c];
		DEFCOORD_VALUE &cvOut = cvaLaticeOut.pcv[c];
		cvOut = cvIn;
		if( cvIn.fValue == 1 )
		{
			float fDeviationSqr;
			do
			{
				random_Gaussian_coord( fSigma, cvOut.pos );
				fDeviationSqr = cvOut.pos.fX*cvOut.pos.fX + cvOut.pos.fY*cvOut.pos.fY;
			} while( fDeviationSqr >= fMaxDeviation*fMaxDeviation );
			
			cvOut.pos.fX += cvIn.pos.fX;
			cvOut.pos.fY += cvIn.pos.fY;
		}
	}
	
	return 1;
}

//
// defCalculateWeights()
//
// Calculates the weights of latice nodes in determining the position of cCoord.
//
int
defCalculateWeights(
					DEFCOORD_VALUE_ARRAY &cvaLatice,
					DEFCOORD &cCoord,
					float *pfWeights
					)
{
	int c;
	float fWeightSum = 0.0f;
	for( int c = 0; c < cvaLatice.iCount; c++ )
	{
		DEFCOORD_VALUE &cv  = cvaLatice.pcv[c];

		float fXDiff = cv.pos.fX - cCoord.fX;
		float fYDiff = cv.pos.fY - cCoord.fY;
		float fDistSqr = fXDiff*fXDiff + fYDiff*fYDiff;

		if( fDistSqr == 0.0f )
		{
			// 100% match
			memset( pfWeights, 0, sizeof(float)*cvaLatice.iCount );
			pfWeights[c] = 1.0f;
			return 1;
		}
		else
		{
			pfWeights[c] = 1.0f / fDistSqr;
			//pfWeights[c] = fDistSqr*log(fDistSqr);
		}
		fWeightSum += pfWeights[c];
	}

	// Normalize
	float fInvertedWeightSum = 1.0f / fWeightSum;
	assert( fInvertedWeightSum > 0.0f );
	for( int c = 0; c < cvaLatice.iCount; c++ )
	{
		pfWeights[c] *= fInvertedWeightSum;
	}

	return 1;
}

//
// defCalculateCoord()
//
// Determines the position of cCoord given the weights and latice nodes in cvaLatice.
//
int
defCalculateCoord(
					DEFCOORD_VALUE_ARRAY &cvaLatice,
					DEFCOORD &cCoord,
					float *pfWeights
					)
{
	int c;

	cCoord.fX = 0.0f;
	cCoord.fY = 0.0f;

	for( int c = 0; c < cvaLatice.iCount; c++ )
	{
		DEFCOORD_VALUE &cv  = cvaLatice.pcv[c];

		cCoord.fX += cv.pos.fX*pfWeights[c];
		cCoord.fY += cv.pos.fY*pfWeights[c];
	}

	return 1;
}

int
defCalculateCoordIn(
					DEFCOORD_VALUE_ARRAY &cvaLaticeIn,
					DEFCOORD_VALUE_ARRAY &cvaLaticeOut,
					DEFCOORD &cCoordIn,
					DEFCOORD &cCoordOut,
					float *pfWeights
					)
{
	assert( pfWeights );

	defCalculateWeights( cvaLaticeOut, cCoordOut, pfWeights );
	defCalculateCoord( cvaLaticeIn, cCoordIn, pfWeights );

	return 1;
}

//

int
defInitFieldEvidenceGaussian(
			 DEFORMATION_FIELD &dfField,
			 int iSubSampling,
			 float fSigma,
			 float fMaxDeviation
			 )
{
	int x, y;
	for( int y = 0; y < dfField.iYDim; y++ )
	{
		for( int x = 0; x < dfField.iXDim; x++ )
		{
			DEFCOORD_VALUE &cv  = dfField.pcv[y*dfField.iXDim + x];

			if( x == 0 || x == dfField.iXDim - 1 || y == 0 || y == dfField.iYDim - 1 )
			{
				cv.fValue = 1;
			}
			cv.pos.fX = x;
			cv.pos.fY = y;
		}
	}

	for( int y = iSubSampling; y <= dfField.iYDim - iSubSampling; y += iSubSampling )
	{
		for( int x = iSubSampling; x <= dfField.iXDim - iSubSampling; x += iSubSampling )
		{
			DEFCOORD_VALUE &cv  = dfField.pcv[y*dfField.iXDim + x];
			DEFCOORD cSample;

			float fDeviationSqr;
			do
			{
				random_Gaussian_coord( fSigma, cSample );
				fDeviationSqr = cSample.fX*cSample.fX + cSample.fY*cSample.fY;
			} while( fDeviationSqr >= fMaxDeviation*fMaxDeviation );

			cv.pos.fX += cSample.fX;
			cv.pos.fY += cSample.fY;
			cv.fValue = 1;
		}
	}
	return 1;
}

int
defInitFieldEvidenceUniform(
			 DEFORMATION_FIELD &dfField,
			 int iSubSampling,
			 float fMaxDeviation
			 )
{
	int x, y;
	for( int y = 0; y < dfField.iYDim; y++ )
	{
		for( int x = 0; x < dfField.iXDim; x++ )
		{
			DEFCOORD_VALUE &cv  = dfField.pcv[y*dfField.iXDim + x];

			if( x == 0 || x == dfField.iXDim - 1 || y == 0 || y == dfField.iYDim - 1 )
			{
				cv.fValue = 1;
			}
			cv.pos.fX = x;
			cv.pos.fY = y;
		}
	}

	for( int y = iSubSampling; y <= dfField.iYDim - iSubSampling; y += iSubSampling )
	{
		for( int x = iSubSampling; x <= dfField.iXDim - iSubSampling; x += iSubSampling )
		{
			DEFCOORD_VALUE &cv  = dfField.pcv[y*dfField.iXDim + x];
			DEFCOORD cSample;

			float fDeviationSqr;
			do
			{
				cSample.fX = (ranf() - 1.0f)*iSubSampling/2.0f;
				cSample.fY = (ranf() - 1.0f)*iSubSampling/2.0f;
				fDeviationSqr = cSample.fX*cSample.fX + cSample.fY*cSample.fY;
			} while( fDeviationSqr >= fMaxDeviation*fMaxDeviation );

			cv.pos.fX += cSample.fX;
			cv.pos.fY += cSample.fY;
			cv.fValue = 1;
		}
	}
	return 1;
}

int
defIterateField8x8(
				DEFORMATION_FIELD &dfField,
				float fSmoothing
				)
{
	int x, y;

	for( int y = 0; y < dfField.iYDim; y++ )
	{
		for( int x = 0; x < dfField.iXDim; x++ )
		{
			DEFCOORD_VALUE &cv  = dfField.pcv[y*dfField.iXDim + x];

			if( cv.fValue == 1 )
			{
				// Do not update this coordinate, it is evidence
				continue;
			}
			
			float fYSum = 0.0f;
			float fYAvg = 0.0f;
			
			float fXSum = 0.0f;
			float fXAvg = 0.0f;

			int   iNeighbourCount = 0;		

			if( y - 1 >= 0 )
			{
				fYSum += dfField.pcv[(y-1)*dfField.iXDim + x].pos.fY;
				fXSum += dfField.pcv[(y-1)*dfField.iXDim + x].pos.fX;
				iNeighbourCount++;
	
				if( x - 1 >= 0 )
				{
					fYSum += dfField.pcv[(y-1)*dfField.iXDim + (x-1)].pos.fY;
					fXSum += dfField.pcv[(y-1)*dfField.iXDim + (x-1)].pos.fX;
					iNeighbourCount++;
				}
				if( x + 1 < dfField.iXDim )
				{
					fYSum += dfField.pcv[(y-1)*dfField.iXDim + (x+1)].pos.fY;
					fXSum += dfField.pcv[(y-1)*dfField.iXDim + (x+1)].pos.fX;
					iNeighbourCount++;
				}
			}
			{
				if( x - 1 >= 0 )
				{
					fYSum += dfField.pcv[y*dfField.iXDim + (x-1)].pos.fY;
					fXSum += dfField.pcv[y*dfField.iXDim + (x-1)].pos.fX;
					iNeighbourCount++;
				}
				if( x + 1 < dfField.iXDim )
				{
					fYSum += dfField.pcv[y*dfField.iXDim + (x+1)].pos.fY;
					fXSum += dfField.pcv[y*dfField.iXDim + (x+1)].pos.fX;
					iNeighbourCount++;
				}
			}
			if( y + 1 < dfField.iYDim )
			{
				fYSum += dfField.pcv[(y+1)*dfField.iXDim + x].pos.fY;
				fXSum += dfField.pcv[(y+1)*dfField.iXDim + x].pos.fX;
				iNeighbourCount++;
				
				if( x - 1 >= 0 )
				{
					fYSum += dfField.pcv[(y+1)*dfField.iXDim + (x-1)].pos.fY;
					fXSum += dfField.pcv[(y+1)*dfField.iXDim + (x-1)].pos.fX;
					iNeighbourCount++;
				}
				if( x + 1 < dfField.iXDim )
				{
					fYSum += dfField.pcv[(y+1)*dfField.iXDim + (x+1)].pos.fY;
					fXSum += dfField.pcv[(y+1)*dfField.iXDim + (x+1)].pos.fX;
					iNeighbourCount++;
				}
			}

			if( iNeighbourCount > 0 )
			{
				cv.pos.fX = fSmoothing*x + (fXSum / (float)iNeighbourCount)*(1.0f - fSmoothing);
				cv.pos.fY = fSmoothing*y + (fYSum / (float)iNeighbourCount)*(1.0f - fSmoothing);
			}
		}
	}
	return 1;
}

int
defIterateField4x4(
				DEFORMATION_FIELD &dfField,
				float fSmoothing
				)
{
	int x, y;

	for( int y = 0; y < dfField.iYDim; y++ )
	{
		for( int x = 0; x < dfField.iXDim; x++ )
		{
			DEFCOORD_VALUE &cv  = dfField.pcv[y*dfField.iXDim + x];

			if( cv.fValue == 1 )
			{
				// Do not update this coordinate, it is evidence
				continue;
			}
			
			float fYSum = 0.0f;
			float fYAvg = 0.0f;
			
			float fXSum = 0.0f;
			float fXAvg = 0.0f;
			
			int   iNeighbourCount = 0;		
			
			if( y - 1 >= 0 )
			{
				fYSum += dfField.pcv[(y-1)*dfField.iXDim + x].pos.fY;
				fXSum += dfField.pcv[(y-1)*dfField.iXDim + x].pos.fX;
				iNeighbourCount++;
			}
			if( y + 1 < dfField.iYDim )
			{
				fYSum += dfField.pcv[(y+1)*dfField.iXDim + x].pos.fY;
				fXSum += dfField.pcv[(y+1)*dfField.iXDim + x].pos.fX;
				iNeighbourCount++;
			}	
			if( x - 1 >= 0 )
			{
				fYSum += dfField.pcv[y*dfField.iXDim + (x-1)].pos.fY;
				fXSum += dfField.pcv[y*dfField.iXDim + (x-1)].pos.fX;
				iNeighbourCount++;
			}
			if( x + 1 < dfField.iXDim )
			{
				fYSum += dfField.pcv[y*dfField.iXDim + (x+1)].pos.fY;
				fXSum += dfField.pcv[y*dfField.iXDim + (x+1)].pos.fX;
				iNeighbourCount++;
			}

			if( iNeighbourCount > 0 )
			{
				cv.pos.fX = fSmoothing*x + (fXSum / (float)iNeighbourCount)*(1.0f - fSmoothing);
				cv.pos.fY = fSmoothing*y + (fYSum / (float)iNeighbourCount)*(1.0f - fSmoothing);
			}
		}
	}
	return 1;
}

int
defTranslateDeformation(
				 DEFORMATION_FIELD &dfField,
				 DEFCOORD &cTrans
				)
{
	int x, y;

	for( int y = 0; y < dfField.iYDim; y++ )
	{
		for( int x = 0; x < dfField.iXDim; x++ )
		{
			DEFCOORD_VALUE &cv  = dfField.pcv[y*dfField.iXDim + x];
			cv.pos.fX += cTrans.fX;
			cv.pos.fY += cTrans.fY;
		}
	}
	return 1;
}

int
defFindDestCoord(
				 const DEFORMATION_FIELD &dfField,
				 const DEFCOORD &cSource,
				 DEFCOORD &cDest
				)
{
	int x, y;

	float fMinDistSqr = 9999e30;

	for( int y = 0; y < dfField.iYDim; y++ )
	{
		for( int x = 0; x < dfField.iXDim; x++ )
		{
			DEFCOORD_VALUE &cv  = dfField.pcv[y*dfField.iXDim + x];

			float fDistX = cSource.fX - cv.pos.fX;
			float fDistY = cSource.fY - cv.pos.fY;
			float fDistSqr = fDistX*fDistX + fDistY*fDistY;
			if( fDistSqr < fMinDistSqr )
			{
				fMinDistSqr = fDistSqr;
				cDest.fX = x;
				cDest.fY = y;
			}
		}
	}
	return 1;
}

int
defWriteDeformation(
				DEFORMATION_FIELD &dfField,
				char *pcFileName
					)
{
	FILE *outfile = fopen( pcFileName, "wb" );
	if( !outfile )
	{
		return 0;
	}

	fprintf( outfile, "DEFORMATION_FIELD\n%d\t%d", dfField.iYDim, dfField.iXDim );
	int iWrite = fwrite( dfField.pcv, sizeof(DEFCOORD_VALUE), dfField.iYDim*dfField.iXDim, outfile );
	if( iWrite != dfField.iYDim*dfField.iXDim )
	{
		fclose( outfile );
		return 0;
	}

	fclose( outfile );
	return 1;
}

int
defReadDeformation(
				DEFORMATION_FIELD &dfField,
				char *pcFileName
				   )
{
	FILE *infile = fopen( pcFileName, "rb" );
	if( !infile )
	{
		return 0;
	}

	int iRead;
	iRead = fscanf( infile, "DEFORMATION_FIELD\n" );
	iRead = fscanf( infile, "%3d\t%3d", &dfField.iYDim, &dfField.iXDim );
	//iRead = fscanf( infile, "DEFORMATION_FIELD\n%d\t%d", &dfField.iYDim, &dfField.iXDim );

	if( !defAllocateDeformation( dfField ) )
	{
		fclose( infile );
		return 0;
	}

	iRead = fread( dfField.pcv, sizeof(DEFCOORD_VALUE), dfField.iYDim*dfField.iXDim, infile );
	if( iRead != dfField.iYDim*dfField.iXDim )
	{
		defDestroyDeformation( dfField );
		fclose( infile );
		return 0;
	}

	fclose( infile );
	return 1;
}

int
defDestroyDeformation(
				DEFORMATION_FIELD &dfField
					  )
{
	if( dfField.iXDim*dfField.iYDim <= 0 || dfField.pcv == 0 )
	{
		return 0;
	}
	
	dfField.iYDim = 0;
	dfField.iXDim = 0;
	delete [] dfField.pcv;
	
	return 1;
}

int
defAllocateDeformation(
				DEFORMATION_FIELD &dfField
					  )
{
	if( dfField.iXDim*dfField.iYDim <= 0 )
	{
		return 0;
	}

	dfField.pcv = new DEFCOORD_VALUE[dfField.iXDim*dfField.iYDim];
	if( !dfField.pcv )
	{
		dfField.iYDim = 0;
		dfField.iXDim = 0;
		return 0;
	}
	
	return 1;
}

int
defInitFieldEvidenceRotMag(
			 DEFORMATION_FIELD &dfField,
			 const float &fRadians,	// Rotation angle
			 const DEFCOORD &cMag,
			 const DEFCOORD &cOutputOffset
			 )
{
	float fCosAngle = (float)cos(-fRadians);
	float fSinAngle = (float)sin(-fRadians);

	int x, y;

	for( int y = 0; y < dfField.iYDim; y++ )
	{
		float fPosY = ((float)y - cOutputOffset.fY) / cMag.fY;

		for( int x = 0; x < dfField.iXDim; x++ )
		{
			float fPosX = ((float)x - cOutputOffset.fX) / cMag.fX;

			DEFCOORD_VALUE &cv = dfField.pcv[y*dfField.iXDim + x];

			// Calcuate rotation

			cv.pos.fY = fCosAngle*fPosY - fSinAngle*fPosX;
			cv.pos.fX = fSinAngle*fPosY + fCosAngle*fPosX;
			cv.fValue = 1;
		}
	}
	return 1;
}

int
defCoordRotMag(
			 DEFCOORD &cPoint,
			 const float &fRadians,	// Rotation angle
			 const DEFCOORD &cMag
			 )
{
	float fCosAngle = (float)cos(-fRadians);
	float fSinAngle = (float)sin(-fRadians);

	float x = cPoint.fX;
	float y = cPoint.fY;

	float fPosY = ((float)y) / cMag.fY;
	float fPosX = ((float)x) / cMag.fX;

	cPoint.fY = fCosAngle*fPosY - fSinAngle*fPosX;
	cPoint.fX = fSinAngle*fPosY + fCosAngle*fPosX;

	return 1;
}

int
defCoordRotMagForward(
			 DEFCOORD &cPoint,
			 const float &fRadians,	// Rotation angle
			 const DEFCOORD &cMag
			 )
{
	float fCosAngle = (float)cos(-fRadians);
	float fSinAngle = (float)sin(-fRadians);

	float fPosY = cPoint.fY;// / cMag.fY;
	float fPosX = cPoint.fX;// / cMag.fX;

	float y = fCosAngle*fPosY - fSinAngle*fPosX;
	float x = fSinAngle*fPosY + fCosAngle*fPosX;

	cPoint.fY = y / cMag.fY;
	cPoint.fX = x / cMag.fX;

	return 1;
}

