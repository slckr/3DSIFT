
#ifndef __DEFORM_H__
#define __DEFORM_H__

typedef struct _DEFCOORD
{
	float fX;
	float fY;
} DEFCOORD;

typedef struct _DEFCOORD_VALUE
{
	DEFCOORD pos;
	float fValue;
} DEFCOORD_VALUE;

typedef struct _DEFCOORD_VALUE_ARRAY
{
	DEFCOORD_VALUE	*pcv;
	int			iCount;
} DEFCOORD_VALUE_ARRAY;

typedef struct _DEFORMATION_FIELD
{
	int iYDim;
	int iXDim;
	DEFCOORD_VALUE	*pcv;
} DEFORMATION_FIELD;

//
// defCreateArray()
//
// Create a DEFCOORD_VALUE_ARRAY.
//
int
defCreateArray(
			   int iSize,
			   DEFCOORD_VALUE_ARRAY &cvaLatice
			   );

//
// defCreateArray()
//
// Destroy a DEFCOORD_VALUE_ARRAY.
//
int
defDestroyArray(
			   DEFCOORD_VALUE_ARRAY &cvaLatice
			   );

//
// defCreateUniformLatice()
//
// Calculates an an array of coordinates on a
// square latice. All boundary points are indicated
// with by fValue = 1.0.
//
int
defCreateSquareLatice(
					   DEFCOORD &cStart,
					   DEFCOORD &cFinish,
					   int iSize,
					   DEFCOORD_VALUE_ARRAY &cvaLatice
					  );

//
// defCreateRandomDeformation()
//
// Perform a deformation on non-boundary points of cvaLaticeIn,
// create a new array of points in cvaLaticeOut. Deformed point
// is generated as a sample from a Gaussian distribution around
// the original point.
//
int
defCreateRandomDeformation(
						   DEFCOORD_VALUE_ARRAY &cvaLaticeIn,
						   DEFCOORD_VALUE_ARRAY &cvaLaticeOut,
						   float fSigma,
						   float fMaxDeviation
						   );
//
// defCalculateCoordIn()
//
// Calculates the coordinate in the 'In' latice
// from a known point in the 'Out' latice.
//
int
defCalculateCoordIn(
					DEFCOORD_VALUE_ARRAY &cvaLaticeIn,
					DEFCOORD_VALUE_ARRAY &cvaLaticeOut,
					DEFCOORD &cCoordIn,
					DEFCOORD &cCoordOut,
					float *pfWeights
					);

//
// defInitFieldEvidence()
//
// Initializes fixed evidence points in the deformation field.
// The border is fixed as unmoving evidence. Subsampled points
// in the interior of the image are perturbed by Gaussian (x,y)
// noise and fixed as evidencen.
//
int
defInitFieldEvidenceGaussian(
			 DEFORMATION_FIELD &dfField,
			 int iSubSampling,
			 float fSigma,
			 float fMaxDeviation
			 );

int
defInitFieldEvidenceUniform(
			 DEFORMATION_FIELD &dfField,
			 int iSubSampling,
			 float fMaxDeviation
			 );

int
defInitFieldEvidenceRotMag(
			 DEFORMATION_FIELD &dfField,
			 const float &fRadians,	// Rotation angle
			 const DEFCOORD &cMag,
			 const DEFCOORD &cOutputOffset
			 );

int
defCoordRotMag(
			 DEFCOORD &cPoint,
			 const float &fRadians,	// Rotation angle
			 const DEFCOORD &cMag
			 );

int
defCoordRotMagForward(
			 DEFCOORD &cPoint,
			 const float &fRadians,	// Rotation angle
			 const DEFCOORD &cMag
			 );

//
// defIterateField4x4()
//
// Gradient ascent iteration of deformation field assuming
// nearest 4 neighbours. Points with fValue = 1.0 are considered
// as evidence, and are not changed.
//
int
defIterateField4x4(
				DEFORMATION_FIELD &dfField,
				float fSmoothing = 0.0f
				);

//
// defIterateField8x8()
//
// Gradient ascent iteration of deformation field assuming
// nearest 4 neighbours. Points with fValue = 1.0 are considered
// as evidence, and are not changed.
//
int
defIterateField8x8(
				DEFORMATION_FIELD &dfField,
				float fSmoothing = 0.0f
				);

int
defTranslateDeformation(
				 DEFORMATION_FIELD &dfField,
				 DEFCOORD &cTrans
				);

//
// defFindDestCoord()
//
// Finds the closest destination coordinate for a particular
// source coordinate.
//
int
defFindDestCoord(
				 const DEFORMATION_FIELD &dfField,
				 const DEFCOORD &cSource,
				 DEFCOORD &cDest
				);

//
// defWriteDeformation()
//
// Write a deformation file.
//
int
defWriteDeformation(
				DEFORMATION_FIELD &dfField,
				char *pcFileName
					);
//
// defWriteDeformation()
//
// Read a deformation file.
//
int
defReadDeformation(
				DEFORMATION_FIELD &dfField,
				char *pcFileName
				   );

//
// defAllocateDeformation()
//
// Allocates a deformation field to the dimensions specified.
//
int
defAllocateDeformation(
				DEFORMATION_FIELD &dfField
					  );

//
// defDestroyDeformation()
//
// Frees an allocated deformation field.
//
int
defDestroyDeformation(
				DEFORMATION_FIELD &dfField
					  );


#endif
