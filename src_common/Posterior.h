
#ifndef __POSTERIOR_H__
#define __POSTERIOR_H__

#include "PpImage.h"
#include "Transform.h"
#include "LocationValue.h"

//
// Varriance constants for cliques
//
//#define CLIQUE2_VARR	100.0
//#define CLIQUE2_VARR	0.000000000000152587890625

// The larger the clique varriance, the more varriance is permitted in the relationship.
#define CLIQUE2_VARR	0.5
//#define CLIQUE2_VARR	1.0
#define CLIQUE3_VARR	0.1

#define PIXEL_VARR		1.0
//#define PIXEL_VARR    (400000000.0)
//#define PIXEL_VARR    (800000000.0) 	DOG 
#define PIXEL_VARR_DIV	((1.0)/(PIXEL_VARR))

#define CLIQUE2_VARR2	CLIQUE2_VARR
#define CLIQUE3_VARR2	CLIQUE3_VARR
#define PIXEL_VARR2		PIXEL_VARR



//
// Structure used to define a posterior distribution (actually the prior...)
//
typedef struct _POSTERIOR_STRUCT
{
	POINT2D	ppt2dCenterC2[MAX_NEIGHBOURS];
	float	pfDistSqrC2[MAX_NEIGHBOURS];
	float	pfVarrInvC2[MAX_NEIGHBOURS];
	int		iCliqueCountC2;
	
	POINT2D	ppt2dCenterC3[MAX_NEIGHBOURS];
	float	pfVarrInvC3[MAX_NEIGHBOURS];
	int		iCliqueCountC3;

	float	fFunctionMax;
	
} POSTERIOR_STRUCT;


//
// posterior_generate_first_order_doughnut()
//
// Generates the posterior distribution that will be sampled in 
// gibb_sample(). The first order doughnut posterior is a multiplication
// of doughnut-shaped probabilities around neighbours saying
//
int
posterior_generate_first_order_doughnut(
										const PpImage &ppImg1,	// Image 1 - float features
										const PpImage &ppImg2,	// Image 2 - float features
										PpImage &ppImgPost,		// Posterior distribution image - 32BBP float
										LOCATION_VALUE *plp,		// Stack of LOCATION_VALUE for fast sampling
										const PpImage &ppImgTr,	// Transform image - 32x2BBP TRANS_STRUCT
										const int &iRow,			// Current transformation row to sample
										const int &iCol,			// Current transformation row to sample
										const float &fTemperature,		// Current temperature
										POSTERIOR_STRUCT &postStruct
										);
//
// posterior_generate_second_order_doughnut()
//
// Generates the posterior distribution that will be sampled in 
// gibb_sample().
//
int
posterior_generate_second_order_doughnut(
										 const PpImage &ppImg1,	// Image 1 - float features
										 const PpImage &ppImg2,	// Image 2 - float features
										 PpImage &ppImgPost,		// Posterior distribution image - 32BBP float
										 LOCATION_VALUE *plp,		// Stack of LOCATION_VALUE for fast sampling
										 const PpImage &ppImgTr,	// Transform image - 32x2BBP TRANS_STRUCT
										 const int &iRow,			// Current transformation row to sample
										 const int &iCol,			// Current transformation row to sample
										 const float &fTemperature,		// Current temperature
										 POSTERIOR_STRUCT &postStruct
										 );

//
// posterior_calculate_probability_doughnut()
//
// Calculates the probability of a given transform to a 
// constant multiple, the function G(Tr):
//
//	P(Tr) = exp( -E(Tr|I1,I2) ) / Z
//
//  G(Tr) = Z * P(Tr)
//
float
posterior_calculate_probability_doughnut(
											const PpImage &ppImg1,	// Image 1 - float features
											const PpImage &ppImg2,	// Image 2 - float features
											const PpImage &ppImgTr	// Transform image - 32x2BBP TRANS_STRUCT
											);
//
// posterior_generate_image()
//
// Generates an imagf of the posterior distribution. Can be used for sampling.
//
int
posterior_generate_image(
						 const PpImage &ppImg1,	// Image 1 - float features
						 const PpImage &ppImg2,	// Image 2 - float features
						 PpImage &ppImgPost,		// Posterior distribution image - 32BBP float
						 LOCATION_VALUE *plp,		// Stack of LOCATION_VALUE for fast sampling
						 const PpImage &ppImgTr,	// Transform image - 32x2BBP TRANS_STRUCT
						 const TRANS_STRUCT &tsPtCurr,
						 POSTERIOR_STRUCT &postStruct
					   );

//
// evaluate_posterior_structure()
//
// Evaluates the posterior distribution defined by two feature images and a
// POSTERIOR_STRUCT at a specific location.
//
float
posterior_evaluate_structure(
						 const PpImage &ppImg1,	// Image 1 - float features
						 const PpImage &ppImg2,	// Image 2 - float features
						 const PpImage &ppImgTr,	// Transform image - 32x2BBP TRANS_STRUCT
						 const TRANS_STRUCT &tsPtCurr,
						 const POSTERIOR_STRUCT &postStruct,
						 const POINT2D	&ptLocation
							 );

//
// posterior_evaluate_likelihood_A()
//
// Evaluates the likelihood of a transform A:
//
//		P(I2|A,I1)
//
float
posterior_evaluate_likelihood_A(
								const PpImage &ppImg1,	// Image 1 - float features
								const PpImage &ppImg2,	// Image 2 - float features
								const TRANS_STRUCT &tsPtA
							  );

//
// posterior_evaluate_likelihood_A_given_B()
//
// Evaluates the likelyhood of a transform A given a transform B
//
//		P(I2|A,B,I1)
//
float
posterior_evaluate_likelihood_A_given_B(
								const PpImage &ppImg1,	// Image 1 - float features
								const PpImage &ppImg2,	// Image 2 - float features
								const TRANS_STRUCT &tsPtA
							  );

//
// sample_posterior_structure()
//
// Samples the posterior distribution defined by two feature images and a
// POSTERIOR_STRUCT.
//
int
posterior_sample_structure(
						 const PpImage &ppImg1,	// Image 1 - float features
						 const PpImage &ppImg2,	// Image 2 - float features
						 const PpImage &ppImgTr,	// Transform image - 32x2BBP TRANS_STRUCT
						 const TRANS_STRUCT &tsPtCurr,
						 const POSTERIOR_STRUCT &postStruct,
						 POINT2D &ptSample,		// Sample found
						 float	 &fProb			// Probability (constant multiple of...)
				 );



#endif
