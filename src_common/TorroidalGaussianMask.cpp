
#include "TorroidalGaussianMask.h"
#include "GaussianMask.h"

#include <math.h>
#include <assert.h>

int
calculate_torroidal_gaussian_filter_size(
										 const float &fSigmaQuad,
										 const float &fExpectedDistance,
										 const float &fMinValue
										 )
{
	float fPower = 0.0f;
	float fValue = (float)exp( fPower ); // = 1.0f
	float fMaxValue = fValue;
	int i;

	for( i = 1; fValue >= fMaxValue*fMinValue; i++ )
	{
		// Note: for a center point on torroid
		// the value is approximated to be (distance^4 / sigma^4)
		fPower = ((float)(i*i*i*i)) / (-fSigmaQuad);
		fValue = (float)exp( fPower );
	}

	int iSize = 2*i + (int)( 2.0f*fExpectedDistance );

	// Keep size odd

	return ((iSize/2)*2) + 1;
}

int
generate_torroidal_gaussian_filter2d(
									 PpImage	&ppTmp,
									 const float &fSigmaQuad,
									 const float &fExpectedDistance,
									 const float &fMeanRow,
									 const float &fMeanCol
									 )
{
	assert( fMeanRow > 0.0 && fMeanRow < ppTmp.Rows() );
	assert( fMeanCol > 0.0 && fMeanCol < ppTmp.Cols() );

	float fScale = 1.0f; //(float)(1.0 / (fSigmaSqrRow*fSigmaSqrCol*2.0*PI));

	float fExpDistSqr = fExpectedDistance*fExpectedDistance;

	for(int i = 0; i < ppTmp.Rows(); i++)
	{
		float fRowPos = ((float)i + 0.5f - fMeanRow);
		float fRowPosSqr = fRowPos*fRowPos;

		float *pfImgRow = (float*)ppTmp.ImageRow( i );

		for(int j = 0; j < ppTmp.Cols(); j++)
		{
			float fColPos = ((float)j + 0.5f - fMeanCol);
			float fColPosSqr = fColPos*fColPos;

			float fDistSqr = fRowPosSqr + fColPosSqr;

			float fDistSqrDiff = fDistSqr - fExpDistSqr;

			float fDistSqrDiffSqr = fDistSqrDiff*fDistSqrDiff;

			float fPower = -fDistSqrDiffSqr / fSigmaQuad;

			pfImgRow[ j ] *= (float)(fScale*exp( fPower ));
		}
	}

	return 1;
}

int
generate_torroidal_gaussian_filter2d_exponent(
									 PpImage	&ppTmp,
									 const float &fSigmaQuad,
									 const float &fExpectedDistance,
									 const float &fMeanRow,
									 const float &fMeanCol
									 )
{
	assert( fMeanRow > 0.0 && fMeanRow < ppTmp.Rows() );
	assert( fMeanCol > 0.0 && fMeanCol < ppTmp.Cols() );

	float fScale = 1.0f; //(float)(1.0 / (fSigmaSqrRow*fSigmaSqrCol*2.0*PI));

	float fExpDistSqr = fExpectedDistance*fExpectedDistance;

	for(int i = 0; i < ppTmp.Rows(); i++)
	{
		float fRowPos = ((float)i + 0.5f - fMeanRow);
		float fRowPosSqr = fRowPos*fRowPos;

		float *pfImgRow = (float*)ppTmp.ImageRow( i );

		for(int j = 0; j < ppTmp.Cols(); j++)
		{
			float fColPos = ((float)j + 0.5f - fMeanCol);
			float fColPosSqr = fColPos*fColPos;

			float fDistSqr = fRowPosSqr + fColPosSqr;

			float fDistSqrDiff = fDistSqr - fExpDistSqr;

			float fDistSqrDiffSqr = fDistSqrDiff*fDistSqrDiff;

			float fPower = -fDistSqrDiffSqr / fSigmaQuad;

			pfImgRow[ j ] += fPower;
		}
	}

	return 1;
}

