
#ifndef __BINOMIAL_H__
#define __BINOMIAL_H__

//
// Does not work for numbers greater than 150 - use a Sterling approximation
//

double
factorial_150(
		  int iN
		  );

double
combination_150(
			int iN,
			int iK
			);

//
// Calculates the p-value of data sample (iD0, iD1), where the null hypothesis is
// of independent data samples following a binomial distribution
// with parameters (fP0, fP1).
//
double
cumm_binomial_p_value(
				 double dP0,	// Prior probability of 0
				 double dP1,  // Prior probability of 1
				 int iD0,    // Data samples of 0
				 int iD1		// Data samples of 1
				 );

#endif
