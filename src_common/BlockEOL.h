
#ifndef __BLOCKEOL_H__
#define __BLOCKEOL_H__

#include "PpImage.h"
#include "FeatureIO.h"

//
// EOL for block-based matching schemes.
//

//
// calculate_eol_image_ncc()
//
// Calculates EOL for the normalized covariance similarity measure of
// one template.
//
// Probability of the form:
//
//	p(Block2=Block1) = exp( -[ 1 - NCC(Block1,Block2) ] * fNCCVarrDiv )
//
float
calculate_eol_sums_ncc(
			   const int &iRowStart,		// Domain starting row
			   const int &iColStart,		// Domain starting col
			   const int &iRows,			// Domain row size
			   const int &iCols,			// Domain col size

			   const PpImage &ppTmp1,		// Source Template (fio1)
			   const PpImage &ppImg2,		// Domain Image (fio2)

			   const float		&fStdDev1,	// Stddev for ppTmp1
			   const FEATUREIO	&fio2Varr,	// Varriance for all locations in ppImg2

			   const float &fNCCVarrDiv		// Varriance term for NCC energy
			   );


//
// calculate_eol_image_ncc()
//
// Calculates EOL for the normalized covariance similarity measure for
// an entire image.
//
// Probability of the form:
//
//	p(Block2=Block1) = exp( -[ 1 - NCC(Block1,Block2) ] * fNCCVarrDiv )
//
int
calculate_eol_image_ncc(
										  const int &iRowOffset, // Row offset of matching domain from fio1 to fio2
										  const int &iColOffset, // Col ffset of matching domain from fio1 to fio2

										  const int &iRows,		// Matching domain row size in fio2
										  const int &iCols,		// Matching domain col size in fio2

										  FEATUREIO &fio1,		// Image 1, floating point
										  FEATUREIO &fio2,		// Image 2, floating point

										  FEATUREIO &fioEOL,	// EOL image, floating point
										  
										  FEATUREIO &fio1Varr,	// Precalculated window varriances for image 1
										  FEATUREIO &fio2Varr,	// Precalculated window varriances for image 2

										  const int &iRowSize,	// Row size of correlation window
										  const int &iColSize,	// Col size of correlation window

										  const float &fNCCVarr		// Varriance term for NCC energy: exp( - NCC / fNccVar );
										  );

//
// calculate_eol_sums_ncc()
//
// Calculates EOL for the sum-of-squared-differences similarity measure of
// one template.
//
// Probability of the form:
//
//	p(Block2=Block1) = exp( -SSD(Block1,Block2) * fSSDVarrDiv )
//
float
calculate_eol_sums_ssd(
			   const int &iRowStart,		// Domain starting row
			   const int &iColStart,		// Domain starting col
			   const int &iRows,			// Domain row size
			   const int &iCols,			// Domain col size

			   const PpImage &ppTmp1,		// Source Template (fio1)
			   const PpImage &ppImg2,		// Domain Image (fio2)

			   const float &fSSDVarrDiv		// Varriance term for NCC energy
			   );
float
calculate_eol_sums_ssd_robust(
			   const int &iRowStart,		// Domain starting row
			   const int &iColStart,		// Domain starting col
			   const int &iRows,			// Domain row size
			   const int &iCols,			// Domain col size

			   const PpImage &ppTmp1,		// Source Template (fio1)
			   const PpImage &ppImg2,		// Domain Image (fio2)

			   const float &fSSDVarr		// Varriance term for NCC energy
			   );


//
// calculate_eol_image_ssd()
//
// Calculates EOL for the sum-of-squared-differences similarity measure for
// an entire image.
//
// Probability of the form:
//
//	p(Block2=Block1) = exp( -SSD(Block1,Block2) * fSSDVarrDiv )
//
int
calculate_eol_image_ssd(
										  const int &iRowOffset, // Row offset of matching domain from fio1 to fio2
										  const int &iColOffset, // Col ffset of matching domain from fio1 to fio2

										  const int &iRows,		// Matching domain row size in fio2
										  const int &iCols,		// Matching domain col size in fio2

										  FEATUREIO &fio1,		// Image 1, floating point
										  FEATUREIO &fio2,		// Image 2, floating point

										  FEATUREIO &fioEOL,	// EOL image, floating point

										  const int &iRowSize,	// Row size of correlation window
										  const int &iColSize,	// Col size of correlation window

										  const float &fSSDVarrDiv		// Varriance term for NCC energy: exp( - NCC / fNccVar );
										  );

#endif

