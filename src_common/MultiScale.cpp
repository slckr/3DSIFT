
#include "MultiScale.h"
#include "GaussBlur3D.h"
#include "GaussianMask.h"
#include "LocationValue.h"
#include "PpImageFloatOutput.h"
#include "output_functions.h"
#include "featureio_image.h"
#include "PpImage.h"
#include "RotateRegion.h"
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "SVD.h"
#include "RotationInvariantDescriptor.h"
#include "HistogramFeatures.h"
#include "svdcmp.h"

#include <map>
#include <list>
#include <algorithm>

#define PRINT_ONCE( stuff_to_print ) \
	static int _here_1 = 0; \
	if( _here_1 == 0 ) \
	{	printf( stuff_to_print ); \
	_here_1++; }


// Most experiments use 1.0f;
// 0.5 - many false matches
// 1.5 - not bad, interestingly there are different correct matches
// 1.2 - not great
float fBlurGradOriHist = 0.5f;

// Most experiments use 0.5
float fHist2ndPeakThreshold = 0.5f;

#define BLUR_PRECISION 0.01f

//
// get_max_2D()
// get_max_3D()
//
// Find the maximum/maximum voxel in an image in a range of +/- 2 voxels around lvMid.
//
//
int
get_max_3D(
	LOCATION_VALUE_XYZ &lvMid,
	FEATUREIO &fio,
	float &fx,
	float &fy,
	float &fz,
	int bPeak // boolean - peak = 1, valley = 0
);
int
get_max_2D(
	LOCATION_VALUE_XYZ &lvMid,
	FEATUREIO &fio,
	float &fx,
	float &fy,
	int bPeak // boolean - peak = 1, valley = 0
);

void
interpolate_discrete_3D_point(
	FEATUREIO &fioC,	// Center image: for interpolating feature geometry
	int ix, int iy, int iz,
	float &fx, float &fy, float &fz
);

static int
outputIntVector(
	vector<int> &vec,
	char *pcFileName
)
{
	FILE *outfile = fopen(pcFileName, "wt");
	for (int i = 0; i < vec.size(); i++)
	{
		fprintf(outfile, "%d\t%d\n", i, vec[i]);
	}
	fclose(outfile);
	return 1;
}


int
compatible_features(
	const Feature3DInfo &f1,
	const Feature3DInfo &f2,
	float fScaleDiffThreshold,
	float fShiftThreshold,
	float fCosineAngleThreshold
)
{
	if ((f1.m_uiInfo & INFO_FLAG_LINE) != (f2.m_uiInfo & INFO_FLAG_LINE))
	{
		// Not the same geometry
		return 0;
	}
	else  if (
		(f1.m_uiInfo & INFO_FLAG_LINE) == INFO_FLAG_LINE
		)
	{
		// Lines

		// Euclidean distance of features
		float fdx = f1.x - f2.x;
		float fdy = f1.y - f2.y;
		float fdz = f1.z - f2.z;
		float fDist12_1 = sqrt(fdx*fdx + fdy*fdy + fdz*fdz);

		fdx = f1.ori[0][0] - f2.ori[0][0];
		fdy = f1.ori[0][1] - f2.ori[0][1];
		fdz = f1.ori[0][2] - f2.ori[0][2];
		float fDist12_2 = sqrt(fdx*fdx + fdy*fdy + fdz*fdz);

		fdx = f1.ori[0][0] - f1.x;
		fdy = f1.ori[0][1] - f1.y;
		fdz = f1.ori[0][2] - f1.z;
		float fLength1 = sqrt(fdx*fdx + fdy*fdy + fdz*fdz);

		fdx = f2.ori[0][0] - f2.x;
		fdy = f2.ori[0][1] - f2.y;
		fdz = f2.ori[0][2] - f2.z;
		float fLength2 = sqrt(fdx*fdx + fdy*fdy + fdz*fdz);

		float fMeasure = (fDist12_1 + fDist12_2) / (fLength1 + fLength2);

		if (fMeasure < fShiftThreshold)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else if (
		(f1.m_uiInfo & INFO_FLAG_LINE) == 0
		)
	{
		// Spheres

		// Euclidean distance of features
		float fdx = f1.x - f2.x;
		float fdy = f1.y - f2.y;
		float fdz = f1.z - f2.z;
		float fDist = sqrt(fdx*fdx + fdy*fdy + fdz*fdz);

		//float fScaleDiffThreshold = (float)log(1.5f);
		//float fShiftThreshold = 0.5; // works the best, MICCAI 2009 results
		//////float fShiftThreshold = 0.75;
		//////float fShiftThreshold = 0.4;
		//////float fShiftThreshold = 0.6;
		//////float fShiftThreshold = 1.0;

		//printf( "Shift threshold: %f\n", fShiftThreshold );

		float fScaleDiff = (float)fabs(log(f1.scale / f2.scale));

		float fMinCosineAngle = vec3D_dot_3d(&f1.ori[0][0], &f2.ori[0][0]);
		if (vec3D_dot_3d(&f1.ori[1][0], &f2.ori[1][0]) < fMinCosineAngle) fMinCosineAngle = vec3D_dot_3d(&f1.ori[1][0], &f2.ori[1][0]);
		if (vec3D_dot_3d(&f1.ori[2][0], &f2.ori[2][0]) < fMinCosineAngle) fMinCosineAngle = vec3D_dot_3d(&f1.ori[2][0], &f2.ori[2][0]);

		if (fScaleDiff < fScaleDiffThreshold
			&&
			fDist < fShiftThreshold*f1.scale
			&&
			fCosineAngleThreshold < fMinCosineAngle
			)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		// Shapes incompatible
		return 0;
	}
}


typedef struct _EXTREMA_STRUCT
{
	FEATUREIO *pfioH;
	FEATUREIO *pfioL;
	LOCATION_VALUE_XYZ lva;
	int iCurrIndex;
	int *piNeighbourIndices;
	int iNeighbourCount;
	int bDense;
} EXTREMA_STRUCT;

typedef int EXTREMA_FUNCTION(EXTREMA_STRUCT *);

//
// peakFunction()
//
// Checks that a particular point is a peak in scales
// above and below the current scale.
//
int
peakFunction4D(
	EXTREMA_STRUCT *pES
)
{
	int bPeak = 1;

	float fNeighValue;
	int iNeighbourIndex;

	if (pES->pfioH)
	{
		fNeighValue =
			pES->pfioH->pfVectors[pES->iCurrIndex*pES->pfioH->iFeaturesPerVector];
		bPeak &= (fNeighValue < pES->lva.fValue);
	}

	if (pES->pfioL)
	{
		fNeighValue =
			pES->pfioL->pfVectors[pES->iCurrIndex*pES->pfioL->iFeaturesPerVector];
		bPeak &= (fNeighValue < pES->lva.fValue);
	}

	if (pES->bDense)
	{
		return bPeak;
	}

	if (pES->pfioH)
	{
		// Check 26 neighbours in scale above
		for (int n = 0; n < pES->iNeighbourCount && bPeak; n++)
		{
			iNeighbourIndex = pES->iCurrIndex + pES->piNeighbourIndices[n];
			fNeighValue =
				pES->pfioH->pfVectors[iNeighbourIndex*pES->pfioH->iFeaturesPerVector];
			bPeak &= (fNeighValue < pES->lva.fValue);
		}
	}

	if (pES->pfioL)
	{
		// Check 26 neighbours in scale below
		for (int n = 0; n < pES->iNeighbourCount && bPeak; n++)
		{
			iNeighbourIndex = pES->iCurrIndex + pES->piNeighbourIndices[n];
			fNeighValue =
				pES->pfioL->pfVectors[iNeighbourIndex*pES->pfioL->iFeaturesPerVector];
			bPeak &= (fNeighValue < pES->lva.fValue);
		}
	}

	return bPeak;
}

//
// peakFunction()
//
// Checks that a particular point is a peak in scales
// above and below the current scale.
//
static int
peakFunction3D(
	EXTREMA_STRUCT *pES
)
{
	int bPeak = 1;

	float fNeighValue;
	int iNeighbourIndex;
	//return bPeak;

	// Check center values in scales above/below
	fNeighValue =
		pES->pfioH->pfVectors[pES->iCurrIndex*pES->pfioH->iFeaturesPerVector];
	bPeak &= (fNeighValue < pES->lva.fValue);
	fNeighValue =
		pES->pfioL->pfVectors[pES->iCurrIndex*pES->pfioL->iFeaturesPerVector];
	bPeak &= (fNeighValue < pES->lva.fValue);
	//return bPeak;

	// Check 8 neighbours in scale above
	for (int n = 0; n < pES->iNeighbourCount && bPeak; n++)
	{
		iNeighbourIndex = pES->iCurrIndex + pES->piNeighbourIndices[n];
		fNeighValue =
			pES->pfioH->pfVectors[iNeighbourIndex*pES->pfioH->iFeaturesPerVector];
		bPeak &= (fNeighValue < pES->lva.fValue);
	}

	// Check 8 neighbours in scale below
	for (int n = 0; n < pES->iNeighbourCount && bPeak; n++)
	{
		iNeighbourIndex = pES->iCurrIndex + pES->piNeighbourIndices[n];
		fNeighValue =
			pES->pfioL->pfVectors[iNeighbourIndex*pES->pfioL->iFeaturesPerVector];
		bPeak &= (fNeighValue < pES->lva.fValue);
	}

	return bPeak;
}

int
peakFunction2D(
	EXTREMA_STRUCT *pES
)
{
	int bPeak = 1;

	float fNeighValue;
	int iNeighbourIndex;
	//return bPeak;

	return bPeak;
}

//
// valleyFunction()
//
// Checks that a particular point is a valley in scales
// above and below the current scale.
//
int
valleyFunction4D(
	EXTREMA_STRUCT *pES
)
{
	int bValley = 1;

	float fNeighValue;
	int iNeighbourIndex;

	// Check center values above/below
	if (pES->pfioH)
	{
		fNeighValue =
			pES->pfioH->pfVectors[pES->iCurrIndex*pES->pfioH->iFeaturesPerVector];
		bValley &= (fNeighValue > pES->lva.fValue);
	}

	if (pES->pfioL)
	{
		fNeighValue =
			pES->pfioL->pfVectors[pES->iCurrIndex*pES->pfioL->iFeaturesPerVector];
		bValley &= (fNeighValue > pES->lva.fValue);
	}

	if (pES->bDense)
	{
		return bValley;
	}

	if (pES->pfioH)
	{
		// Check 26 neighbours (including center) in scale above
		for (int n = 0; n < pES->iNeighbourCount && bValley; n++)
		{
			iNeighbourIndex = pES->iCurrIndex + pES->piNeighbourIndices[n];
			fNeighValue =
				pES->pfioH->pfVectors[iNeighbourIndex*pES->pfioH->iFeaturesPerVector];
			bValley &= (fNeighValue > pES->lva.fValue);
		}
	}

	if (pES->pfioL)
	{
		// Check 26 neighbours (including center) in scale below
		for (int n = 0; n < pES->iNeighbourCount && bValley; n++)
		{
			iNeighbourIndex = pES->iCurrIndex + pES->piNeighbourIndices[n];
			fNeighValue =
				pES->pfioL->pfVectors[iNeighbourIndex*pES->pfioL->iFeaturesPerVector];
			bValley &= (fNeighValue > pES->lva.fValue);
		}
	}

	return bValley;
}

static int
valleyFunction3D(
	EXTREMA_STRUCT *pES
)
{
	int bValley = 1;

	float fNeighValue;
	int iNeighbourIndex;
	//return bValley;

	// Check center values above & below
	fNeighValue =
		pES->pfioH->pfVectors[pES->iCurrIndex*pES->pfioH->iFeaturesPerVector];
	bValley &= (fNeighValue > pES->lva.fValue);
	fNeighValue =
		pES->pfioL->pfVectors[pES->iCurrIndex*pES->pfioL->iFeaturesPerVector];
	bValley &= (fNeighValue > pES->lva.fValue);

	//return bValley;

	// Check 8 neighbours (including center) in scale above
	for (int n = 0; n < pES->iNeighbourCount && bValley; n++)
	{
		iNeighbourIndex = pES->iCurrIndex + pES->piNeighbourIndices[n];
		fNeighValue =
			pES->pfioH->pfVectors[iNeighbourIndex*pES->pfioH->iFeaturesPerVector];
		bValley &= (fNeighValue > pES->lva.fValue);
	}

	// Check 8 neighbours (including center) in scale below
	for (int n = 0; n < pES->iNeighbourCount && bValley; n++)
	{
		iNeighbourIndex = pES->iCurrIndex + pES->piNeighbourIndices[n];
		fNeighValue =
			pES->pfioL->pfVectors[iNeighbourIndex*pES->pfioL->iFeaturesPerVector];
		bValley &= (fNeighValue > pES->lva.fValue);
	}

	return bValley;
}

static int
regFindFEATUREIOPeaks(
	LOCATION_VALUE_XYZ_ARRAY &lvaPeaks,
	FEATUREIO &fio,
	EXTREMA_FUNCTION pCallback = 0,
	EXTREMA_STRUCT *pES = 0
)
{
	// Set up neighbourhood

	int iNeighbourIndexCount;
	int piNeighbourIndices[27];

	int iZStart, iZCount;

	if (fio.z > 1)
	{
		// 3D
		iNeighbourIndexCount = 26;
		iZStart = 1;
		iZCount = fio.z - 1;

		piNeighbourIndices[0] = -fio.x*fio.y - fio.x - 1;
		piNeighbourIndices[1] = -fio.x*fio.y - fio.x;
		piNeighbourIndices[2] = -fio.x*fio.y - fio.x + 1;
		piNeighbourIndices[3] = -fio.x*fio.y - 1;
		piNeighbourIndices[4] = -fio.x*fio.y;
		piNeighbourIndices[5] = -fio.x*fio.y + 1;
		piNeighbourIndices[6] = -fio.x*fio.y + fio.x - 1;
		piNeighbourIndices[7] = -fio.x*fio.y + fio.x;
		piNeighbourIndices[8] = -fio.x*fio.y + fio.x + 1;

		piNeighbourIndices[9] = -fio.x - 1;
		piNeighbourIndices[10] = -fio.x;
		piNeighbourIndices[11] = -fio.x + 1;
		piNeighbourIndices[12] = -1;
		piNeighbourIndices[13] = 1;
		piNeighbourIndices[14] = fio.x - 1;
		piNeighbourIndices[15] = fio.x;
		piNeighbourIndices[16] = fio.x + 1;

		piNeighbourIndices[17] = fio.x*fio.y - fio.x - 1;
		piNeighbourIndices[18] = fio.x*fio.y - fio.x;
		piNeighbourIndices[19] = fio.x*fio.y - fio.x + 1;
		piNeighbourIndices[20] = fio.x*fio.y - 1;
		piNeighbourIndices[21] = fio.x*fio.y;
		piNeighbourIndices[22] = fio.x*fio.y + 1;
		piNeighbourIndices[23] = fio.x*fio.y + fio.x - 1;
		piNeighbourIndices[24] = fio.x*fio.y + fio.x;
		piNeighbourIndices[25] = fio.x*fio.y + fio.x + 1;
	}
	else
	{
		// 2D
		iNeighbourIndexCount = 8;
		iZStart = 0;
		iZCount = 1;

		piNeighbourIndices[0] = -fio.x - 1;
		piNeighbourIndices[1] = -fio.x;
		piNeighbourIndices[2] = -fio.x + 1;
		piNeighbourIndices[3] = -1;
		piNeighbourIndices[4] = 1;
		piNeighbourIndices[5] = fio.x - 1;
		piNeighbourIndices[6] = fio.x;
		piNeighbourIndices[7] = fio.x + 1;
	}

	lvaPeaks.iCount = 0;

	for (int z = iZStart; z < iZCount; z++)
	{
		int iZIndex = z*fio.x*fio.y;
		for (int y = 1; y < fio.y - 1; y++)
		{
			int iYIndex = iZIndex + y*fio.x;
			for (int x = 1; x < fio.x - 1; x++)
			{
				int iIndex = iYIndex + x;

				int bPeak = 1;
				float fCenterValue = fio.pfVectors[iIndex*fio.iFeaturesPerVector];

				for (int n = 0; n < iNeighbourIndexCount && bPeak; n++)
				{
					int iNeighbourIndex = iIndex + piNeighbourIndices[n];
					float fNeighValue = fio.pfVectors[iNeighbourIndex*fio.iFeaturesPerVector];

					// A peak means all neighbours are lower than the center value
					bPeak &= (fNeighValue < fCenterValue);
				}

				if (bPeak)
				{
					if (pCallback)
					{
						pES->iCurrIndex = iIndex;
						pES->iNeighbourCount = iNeighbourIndexCount;
						pES->lva.x = x;
						pES->lva.y = y;
						pES->lva.z = z;
						pES->lva.fValue = fCenterValue;
						pES->piNeighbourIndices = piNeighbourIndices;

						if (pCallback(pES))
						{
							if (lvaPeaks.plvz)
							{
								lvaPeaks.plvz[lvaPeaks.iCount].x = x;
								lvaPeaks.plvz[lvaPeaks.iCount].y = y;
								lvaPeaks.plvz[lvaPeaks.iCount].z = z;
								lvaPeaks.plvz[lvaPeaks.iCount].fValue = fCenterValue;
							}
							lvaPeaks.iCount++;
						}
					}
					else
					{
						// No callback, just add as a peak
						if (lvaPeaks.plvz)
						{
							lvaPeaks.plvz[lvaPeaks.iCount].x = x;
							lvaPeaks.plvz[lvaPeaks.iCount].y = y;
							lvaPeaks.plvz[lvaPeaks.iCount].z = z;
							lvaPeaks.plvz[lvaPeaks.iCount].fValue = fCenterValue;
						}
						lvaPeaks.iCount++;
					}
				}
			}
		}
	}

	return 1;
}

static int
regFindFEATUREIOValleys(
	LOCATION_VALUE_XYZ_ARRAY &lvaPeaks,
	FEATUREIO &fio,
	EXTREMA_FUNCTION pCallback = 0,
	EXTREMA_STRUCT *pES = 0
)
{
	// Set up neighbourhood

	int iNeighbourIndexCount;
	int piNeighbourIndices[27];

	int iZStart, iZCount;

	if (fio.z > 1)
	{
		// 3D
		iNeighbourIndexCount = 26;
		iZStart = 1;
		iZCount = fio.z - 1;

		piNeighbourIndices[0] = -fio.x*fio.y - fio.x - 1;
		piNeighbourIndices[1] = -fio.x*fio.y - fio.x;
		piNeighbourIndices[2] = -fio.x*fio.y - fio.x + 1;
		piNeighbourIndices[3] = -fio.x*fio.y - 1;
		piNeighbourIndices[4] = -fio.x*fio.y;
		piNeighbourIndices[5] = -fio.x*fio.y + 1;
		piNeighbourIndices[6] = -fio.x*fio.y + fio.x - 1;
		piNeighbourIndices[7] = -fio.x*fio.y + fio.x;
		piNeighbourIndices[8] = -fio.x*fio.y + fio.x + 1;

		piNeighbourIndices[9] = -fio.x - 1;
		piNeighbourIndices[10] = -fio.x;
		piNeighbourIndices[11] = -fio.x + 1;
		piNeighbourIndices[12] = -1;
		piNeighbourIndices[13] = 1;
		piNeighbourIndices[14] = fio.x - 1;
		piNeighbourIndices[15] = fio.x;
		piNeighbourIndices[16] = fio.x + 1;

		piNeighbourIndices[17] = fio.x*fio.y - fio.x - 1;
		piNeighbourIndices[18] = fio.x*fio.y - fio.x;
		piNeighbourIndices[19] = fio.x*fio.y - fio.x + 1;
		piNeighbourIndices[20] = fio.x*fio.y - 1;
		piNeighbourIndices[21] = fio.x*fio.y;
		piNeighbourIndices[22] = fio.x*fio.y + 1;
		piNeighbourIndices[23] = fio.x*fio.y + fio.x - 1;
		piNeighbourIndices[24] = fio.x*fio.y + fio.x;
		piNeighbourIndices[25] = fio.x*fio.y + fio.x + 1;
	}
	else
	{
		// 2D
		iNeighbourIndexCount = 8;
		iZStart = 0;
		iZCount = 1;

		piNeighbourIndices[0] = -fio.x - 1;
		piNeighbourIndices[1] = -fio.x;
		piNeighbourIndices[2] = -fio.x + 1;
		piNeighbourIndices[3] = -1;
		piNeighbourIndices[4] = 1;
		piNeighbourIndices[5] = fio.x - 1;
		piNeighbourIndices[6] = fio.x;
		piNeighbourIndices[7] = fio.x + 1;
	}

	lvaPeaks.iCount = 0;

	for (int z = iZStart; z < iZCount; z++)
	{
		int iZIndex = z*fio.x*fio.y;
		for (int y = 1; y < fio.y - 1; y++)
		{
			int iYIndex = iZIndex + y*fio.x;
			for (int x = 1; x < fio.x - 1; x++)
			{
				int iIndex = iYIndex + x;

				int bPeak = 1;
				float fCenterValue = fio.pfVectors[iIndex*fio.iFeaturesPerVector];

				for (int n = 0; n < iNeighbourIndexCount && bPeak; n++)
				{
					int iNeighbourIndex = iIndex + piNeighbourIndices[n];
					float fNeighValue = fio.pfVectors[iNeighbourIndex*fio.iFeaturesPerVector];

					// A valley means all neighbours are higher than the center value
					bPeak &= (fNeighValue > fCenterValue);
				}

				if (bPeak)
				{
					if (pCallback)
					{
						pES->iCurrIndex = iIndex;
						pES->iNeighbourCount = iNeighbourIndexCount;
						pES->lva.x = x;
						pES->lva.y = y;
						pES->lva.z = z;
						pES->lva.fValue = fCenterValue;
						pES->piNeighbourIndices = piNeighbourIndices;

						if (pCallback(pES))
						{
							if (lvaPeaks.plvz)
							{
								lvaPeaks.plvz[lvaPeaks.iCount].x = x;
								lvaPeaks.plvz[lvaPeaks.iCount].y = y;
								lvaPeaks.plvz[lvaPeaks.iCount].z = z;
								lvaPeaks.plvz[lvaPeaks.iCount].fValue = fCenterValue;
							}
							lvaPeaks.iCount++;
						}
					}
				}
			}
		}
	}

	return 1;
}

int
detectExtrema4D_test(
	FEATUREIO *pfioH,	// Hi res image
	FEATUREIO *pfioC,	// Center image
	FEATUREIO *pfioL,	// Lo res image
	LOCATION_VALUE_XYZ_ARRAY	&lvaMinima,
	LOCATION_VALUE_XYZ_ARRAY	&lvaMaxima,
	int bDense = 0
)
{
	// Perform only 3D 
	EXTREMA_STRUCT es;
	es.pfioH = pfioH;
	es.pfioL = pfioL;
	es.bDense = bDense;

	regFindFEATUREIOValleys(lvaMinima, *pfioC, valleyFunction4D, &es);
	regFindFEATUREIOPeaks(lvaMaxima, *pfioC, peakFunction4D, &es);
	//float fx0;
	//float fx1;
	//float fx2;

	//for( int i = 0; i < lvaMinima.iCount; i++ )
	//{
	//	fx0 = fioGetPixel( fioH, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, lvaMinima.plvz[i].z );
	//	fx1 = fioGetPixel( fioC, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, lvaMinima.plvz[i].z );
	//	fx2 = fioGetPixel( fioL, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, lvaMinima.plvz[i].z );
	//	if( fx1 > fx0 || fx1 > fx2 )
	//	{
	//		// Problem
	//		fx1 = 0;
	//	}
	//}

	//for( int i = 0; i < lvaMaxima.iCount; i++ )
	//{
	//	fx0 = fioGetPixel( fioH, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].z );
	//	fx1 = fioGetPixel( fioC, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].z );
	//	fx2 = fioGetPixel( fioL, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].z );
	//	if( fx1 < fx0 || fx1 < fx2 )
	//	{
	//		// Problem
	//		fx1 = 0;
	//	}
	//}

	return 1;
}


int
detectExtrema4D(
	FEATUREIO &fioH,	// Hi res image
	FEATUREIO &fioC,	// Center image
	FEATUREIO &fioL,	// Lo res image
	LOCATION_VALUE_XYZ_ARRAY	&lvaMinima,
	LOCATION_VALUE_XYZ_ARRAY	&lvaMaxima,
	int bDense = 0
)
{
	assert(fioDimensionsEqual(fioH, fioC) && fioDimensionsEqual(fioL, fioC));

	if (!fioDimensionsEqual(fioH, fioC) || !fioDimensionsEqual(fioL, fioC))
	{
		return 0;
	}

	EXTREMA_STRUCT es;
	es.pfioH = &fioH;
	es.pfioL = &fioL;
	es.bDense = bDense;

	regFindFEATUREIOValleys(lvaMinima, fioC, valleyFunction4D, &es);
	regFindFEATUREIOPeaks(lvaMaxima, fioC, peakFunction4D, &es);
	float fx0;
	float fx1;
	float fx2;

	for (int i = 0; i < lvaMinima.iCount; i++)
	{
		fx0 = fioGetPixel(fioH, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, lvaMinima.plvz[i].z);
		fx1 = fioGetPixel(fioC, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, lvaMinima.plvz[i].z);
		fx2 = fioGetPixel(fioL, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, lvaMinima.plvz[i].z);
		if (fx1 > fx0 || fx1 > fx2)
		{
			// Problem
			fx1 = 0;
		}
	}

	for (int i = 0; i < lvaMaxima.iCount; i++)
	{
		fx0 = fioGetPixel(fioH, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].z);
		fx1 = fioGetPixel(fioC, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].z);
		fx2 = fioGetPixel(fioL, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].z);
		if (fx1 < fx0 || fx1 < fx2)
		{
			// Problem
			fx1 = 0;
		}
	}

	return 1;
}

int
detectExtrema3D(
	FEATUREIO &fioH,	// Hi res image
	FEATUREIO &fioC,	// Center image
	FEATUREIO &fioL,	// Lo res image
	LOCATION_VALUE_XYZ_ARRAY	&lvaMinima,
	LOCATION_VALUE_XYZ_ARRAY	&lvaMaxima,
	int bDense = 0
)
{
	assert(fioDimensionsEqual(fioH, fioC) && fioDimensionsEqual(fioL, fioC));

	if (!fioDimensionsEqual(fioH, fioC) || !fioDimensionsEqual(fioL, fioC))
	{
		return 0;
	}

	EXTREMA_STRUCT es;
	es.pfioH = &fioH;
	es.pfioL = &fioL;
	es.bDense = bDense;

	regFindFEATUREIOValleys(lvaMinima, fioC, valleyFunction3D, &es);
	regFindFEATUREIOPeaks(lvaMaxima, fioC, peakFunction3D, &es);

	float fx0;
	float fx1;
	float fx2;

	for (int i = 0; i < lvaMinima.iCount; i++)
	{
		fx0 = fioGetPixel(fioH, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, 0);
		fx1 = fioGetPixel(fioC, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, 0);
		fx2 = fioGetPixel(fioL, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, 0);
		if (fx1 > fx0 || fx1 > fx2)
		{
			// Problem
			fx1 = 0;
		}
	}

	for (int i = 0; i < lvaMaxima.iCount; i++)
	{
		fx0 = fioGetPixel(fioH, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, 0);
		fx1 = fioGetPixel(fioC, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, 0);
		fx2 = fioGetPixel(fioL, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, 0);
		if (fx1 < fx0 || fx1 < fx2)
		{
			// Problem
			fx1 = 0;
		}
	}

	return 1;
}

int
detectExtrema2D(
	FEATUREIO &fioC,	// Center image
	LOCATION_VALUE_XYZ_ARRAY	&lvaMinima,
	LOCATION_VALUE_XYZ_ARRAY	&lvaMaxima
)
{
	EXTREMA_STRUCT es;

	regFindFEATUREIOValleys(lvaMinima, fioC, peakFunction2D, &es);
	regFindFEATUREIOPeaks(lvaMaxima, fioC, peakFunction2D, &es);

	return 1;
}





int
Feature3D::InitFromInfoData(
	Feature3DInfo &f3dInfo,
	Feature3DData &f3dData
)
{
	x = f3dInfo.x;
	y = f3dInfo.y;
	z = f3dInfo.z;
	scale = f3dInfo.scale;
	ori[0][0] = f3dInfo.ori[0][0];
	ori[0][1] = f3dInfo.ori[0][1];
	ori[0][2] = f3dInfo.ori[0][2];
	ori[1][0] = f3dInfo.ori[1][0];
	ori[1][1] = f3dInfo.ori[1][1];
	ori[1][2] = f3dInfo.ori[1][2];
	ori[2][0] = f3dInfo.ori[2][0];
	ori[2][1] = f3dInfo.ori[2][1];
	ori[2][2] = f3dInfo.ori[2][2];

	eigs[0] = f3dInfo.eigs[0];
	eigs[1] = f3dInfo.eigs[1];
	eigs[2] = f3dInfo.eigs[2];

	float fSumSqr = 0;


	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				float fData = f3dData.data_zyx[zz][yy][xx];
				data_zyx[zz][yy][xx] = fData;
				fSumSqr += data_zyx[zz][yy][xx] * data_zyx[zz][yy][xx];
				//if( data_zyx[zz][yy][xx] > cMax ) cMax = data_zyx[zz][yy][xx];
				//if( data_zyx[zz][yy][xx] < cMin ) cMin = data_zyx[zz][yy][xx];
				//fNorm += data_zyx[zz][yy][xx]*data_zyx[zz][yy][xx];
			}
		}
	}
	return 0;
}

int
Feature3D::InitFromFeature3DShort(
	Feature3DShort &feat3DShort
)
{
	x = feat3DShort.x;
	y = feat3DShort.y;
	z = feat3DShort.z;

	scale = feat3DShort.scale;

	ori[0][0] = feat3DShort.ori[0][0];
	ori[0][1] = feat3DShort.ori[0][1];
	ori[0][2] = feat3DShort.ori[0][2];
	ori[1][0] = feat3DShort.ori[1][0];
	ori[1][1] = feat3DShort.ori[1][1];
	ori[1][2] = feat3DShort.ori[1][2];
	ori[2][0] = feat3DShort.ori[2][0];
	ori[2][1] = feat3DShort.ori[2][1];
	ori[2][2] = feat3DShort.ori[2][2];

	eigs[0] = feat3DShort.eigs[0];
	eigs[1] = feat3DShort.eigs[1];
	eigs[2] = feat3DShort.eigs[2];

	float fSumSqr = 0;

	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				float fData = feat3DShort.data_zyx[zz][yy][xx];
				data_zyx[zz][yy][xx] = fData;
				fSumSqr += data_zyx[zz][yy][xx] * data_zyx[zz][yy][xx];
				//if( data_zyx[zz][yy][xx] > cMax ) cMax = data_zyx[zz][yy][xx];
				//if( data_zyx[zz][yy][xx] < cMin ) cMin = data_zyx[zz][yy][xx];
				//fNorm += data_zyx[zz][yy][xx]*data_zyx[zz][yy][xx];
			}
		}
	}
	return 0;
}



#define PI 3.1415926536

//
// One octave is a doubling in scale, i.e. blur sigma.
// 3 blurs per octave is optimal, and an additional 3 are needed
// to perform DOG selection.
//
#define BLURS_PER_OCTAVE	3
#define BLURS_EXTRA			3
#define BLURS_TOTAL			(BLURS_PER_OCTAVE+BLURS_EXTRA)


//
// output_feature_in_volume()
//
// Output parallel images corresponding point (x,y,z)
// in volume fioImg.
//
//
void
output_feature_in_volume(
	FEATUREIO &fioImg,
	int iScale,
	int x,
	int y,
	int z,
	char *pcFileNameBase,
	int bShowFeats = 0
)
{
	char pcFileName[400];
	PpImage ppImgOut;

	ppImgOut.Initialize(fioImg.z, fioImg.y, fioImg.y * sizeof(float), sizeof(float) * 8);
	fioFeatureSliceZY(fioImg, x, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_zy.pgm", pcFileNameBase, x, y, z);
	if (bShowFeats)
		of_paint_white_circle(z, y, 2 * iScale, ppImgOut);
	output_float(ppImgOut, pcFileName);

	ppImgOut.Initialize(fioImg.y, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
	fioFeatureSliceXY(fioImg, z, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_xy.pgm", pcFileNameBase, x, y, z);
	if (bShowFeats)
		of_paint_white_circle(y, x, 2 * iScale, ppImgOut);
	output_float(ppImgOut, pcFileName);

	ppImgOut.Initialize(fioImg.z, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
	fioFeatureSliceZX(fioImg, y, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_zx.pgm", pcFileNameBase, x, y, z);
	if (bShowFeats)
		of_paint_white_circle(z, x, 2 * iScale, ppImgOut);
	output_float(ppImgOut, pcFileName);
}

int
Feature3D::ZeroData(
)
{
	Feature3DInfo::ZeroData();

	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				data_zyx[zz][yy][xx] = 0;
			}
		}
	}


	return 0;
}

int
Feature3D::AccumulateData(
	Feature3D &feat
)
{
	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				data_zyx[zz][yy][xx] += feat.data_zyx[zz][yy][xx];
			}
		}
	}
	x += feat.x;
	y += feat.y;
	z += feat.z;
	scale += log(feat.scale);
	return 0;
}

int
Feature3D::AccumulateDataSqr(
	Feature3D &feat,
	Feature3D &featMean
)
{
	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				float fDiff = feat.data_zyx[zz][yy][xx] - featMean.data_zyx[zz][yy][xx];
				data_zyx[zz][yy][xx] += fDiff*fDiff;
			}
		}
	}
	x += (feat.x - featMean.x)*(feat.x - featMean.x);
	y += (feat.y - featMean.y)*(feat.y - featMean.y);
	z += (feat.z - featMean.z)*(feat.z - featMean.z);

	float fDiff = log(feat.scale) - featMean.scale;
	scale += fDiff;
	return 0;
}

int
Feature3D::CalculateMeanVarr(
	Feature3D &featAccumData,
	Feature3D &featAccumDataSqr,
	int iSamples
)
{
	if (iSamples <= 1)
	{
		// Cannot calculate variance from 1 sample or less

		return -1;
	}

	float fSamplesDiv = 1.0f / (float)iSamples;
	float fSamplesMinusOneDiv = 1.0f / (float)(iSamples - 1);


	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				float fMean = fSamplesDiv*featAccumData.data_zyx[zz][yy][xx];
				float fVarr = fSamplesMinusOneDiv*((featAccumDataSqr.data_zyx[zz][yy][xx] - fMean*fMean*iSamples) + 80000/*prior*/);
				float fVarr2 = fSamplesMinusOneDiv*((featAccumDataSqr.data_zyx[zz][yy][xx] - fMean*fMean*iSamples) + 0/*prior*/);
				featAccumData.data_zyx[zz][yy][xx] = fMean;
				if (fVarr2 == 0)
				{
					fVarr2 = 1.0f;
				}
				featAccumDataSqr.data_zyx[zz][yy][xx] = 1.0f / fVarr;
			}
		}
	}
	return 0;
}

int
Feature3D::OutputVisualFeature(
	char *pcFileNameBase,
	int bOneImage
)
{
	char pcFileName[400];
	PpImage ppImgOut;

	FEATUREIO fioImg;
	fioImg.t = 1;
	fioImg.x = Feature3D::FEATURE_3D_DIM;
	fioImg.y = Feature3D::FEATURE_3D_DIM;
	fioImg.z = Feature3D::FEATURE_3D_DIM;
	fioImg.iFeaturesPerVector = 1;
	fioImg.pfVectors = (float*)&(data_zyx[0][0][0]);

	if (bOneImage)
	{
		PpImage ppImgOutSub;
		PpImage ppBigOut;
		FEATUREIO fioTmp;


		ppBigOut.Initialize(fioImg.z + fioImg.x, fioImg.y + fioImg.x, (fioImg.y + fioImg.x) * sizeof(float), sizeof(float) * 8);
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		for (int yy = 0; yy < ppBigOut.Rows(); yy++)
		{
			for (int xx = 0; xx < ppBigOut.Cols(); xx++)
			{
				((float*)ppBigOut.ImageRow(yy))[xx] = 0;
			}
		}

		ppImgOutSub.InitializeSubImage(
			fioImg.z, fioImg.y,
			ppBigOut.RowInc(), ppBigOut.BitsPerPixel(),
			(unsigned char*)ppBigOut.ImageRow(0)
		);
		ppImgOut.Initialize(fioImg.z, fioImg.y, fioImg.y * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceZY(fioImg, FEATURE_3D_DIM / 2, 0, (float*)ppImgOut.ImageRow(0));
		//output_float( ppImgOut, "imgzy.pgm" );
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		for (int yy = 0; yy < fioImg.z; yy++)
		{
			for (int xx = 0; xx < fioImg.y; xx++)
			{
				((float*)ppImgOutSub.ImageRow(yy))[xx] =
					((float*)ppImgOut.ImageRow(yy))[xx];
			}
		}
		//output_float( ppBigOut, pcFileName );

		ppImgOutSub.InitializeSubImage(
			fioImg.y, fioImg.x,
			ppBigOut.RowInc(), ppBigOut.BitsPerPixel(),
			(unsigned char*)ppBigOut.ImageRow(fioImg.z)
		);
		ppImgOut.Initialize(fioImg.y, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceXY(fioImg, FEATURE_3D_DIM / 2, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				((float*)ppImgOutSub.ImageRow(xx))[yy] =
					((float*)ppImgOut.ImageRow(yy))[fioImg.x - 1 - xx];
			}
		}
		//output_float( ppBigOut, pcFileName );

		ppImgOutSub.InitializeSubImage(
			fioImg.z, fioImg.x,
			ppBigOut.RowInc(), ppBigOut.BitsPerPixel(),
			(unsigned char*)((float*)ppBigOut.ImageRow(0) + fioImg.y)
		);
		ppImgOut.Initialize(fioImg.z, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceZX(fioImg, FEATURE_3D_DIM / 2, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		for (int yy = 0; yy < fioImg.z; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				((float*)ppImgOutSub.ImageRow(yy))[xx] =
					((float*)ppImgOut.ImageRow(yy))[xx];
			}
		}
		output_float(ppBigOut, pcFileName);
	}
	else
	{

		ppImgOut.Initialize(fioImg.z, fioImg.y, fioImg.y * sizeof(float), sizeof(float) * 8);
		memset(ppImgOut.ImageRow(0), 0, ppImgOut.Rows()*ppImgOut.RowInc());
		fioFeatureSliceZY(fioImg, Feature3D::FEATURE_3D_DIM / 2, 0, (float*)ppImgOut.ImageRow(0));
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_zy.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		output_float(ppImgOut, pcFileName);

		ppImgOut.Initialize(fioImg.y, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceXY(fioImg, Feature3D::FEATURE_3D_DIM / 2, 0, (float*)ppImgOut.ImageRow(0));
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_xy.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		output_float(ppImgOut, pcFileName);

		ppImgOut.Initialize(fioImg.z, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceZX(fioImg, Feature3D::FEATURE_3D_DIM / 2, 0, (float*)ppImgOut.ImageRow(0));
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_zx.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		output_float(ppImgOut, pcFileName);
	}

	return 1;
}

int
Feature3DData::OutputVisualFeature(
	char *pcFileNameBase
)
{
	char pcFileName[400];
	PpImage ppImgOut;

	FEATUREIO fioImg;
	fioImg.t = 1;
	fioImg.x = Feature3D::FEATURE_3D_DIM;
	fioImg.y = Feature3D::FEATURE_3D_DIM;
	fioImg.z = Feature3D::FEATURE_3D_DIM;
	fioImg.iFeaturesPerVector = 1;
	fioImg.pfVectors = (float*)&(data_zyx[0][0][0]);

	ppImgOut.Initialize(fioImg.z, fioImg.y, fioImg.y * sizeof(float), sizeof(float) * 8);
	memset(ppImgOut.ImageRow(0), 0, ppImgOut.Rows()*ppImgOut.RowInc());
	fioFeatureSliceZY(fioImg, Feature3D::FEATURE_3D_DIM / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "%s_zy.pgm", pcFileNameBase);
	output_float(ppImgOut, pcFileName);

	ppImgOut.Initialize(fioImg.y, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
	fioFeatureSliceXY(fioImg, Feature3D::FEATURE_3D_DIM / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "%s_xy.pgm", pcFileNameBase);
	output_float(ppImgOut, pcFileName);

	ppImgOut.Initialize(fioImg.z, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
	fioFeatureSliceZX(fioImg, Feature3D::FEATURE_3D_DIM / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "%s_zx.pgm", pcFileNameBase);
	output_float(ppImgOut, pcFileName);

	return 1;
}

int
Feature3DInfo::PaintVisualFeatureInVolume(
	FEATUREIO &fioImg,
	int bWhite
)
{
	for (int zz = z - scale; zz < z + scale; zz++)
	{
		for (int yy = y - scale; yy < y + scale; yy++)
		{
			for (int xx = x - scale; xx < x + scale; xx++)
			{
				int dx = xx - x;
				int dy = yy - y;
				int dz = zz - z;
				int iRadSqr = dx*dx + dy*dy + dz*dz;
				if (iRadSqr <= scale*scale)
				{
					if (xx >= 0 && xx < fioImg.x &&
						yy >= 0 && yy < fioImg.y &&
						zz >= 0 && zz < fioImg.z)
					{
						float *pfVec = fioGetVector(fioImg, xx, yy, zz);
						pfVec[0] = 255;
					}
				}
			}
		}
	}
	return 1;
}

int
Feature3D::OutputVisualFeatureInVolume(
	FEATUREIO &fioImg,
	char *pcFileNameBase,
	int bShowFeats,
	int bOneImage
)
{
	char pcFileName[400];
	PpImage ppImgOut;
	FEATUREIO fioTmp;

	if (bOneImage)
	{
		PpImage ppImgOutSub;
		PpImage ppBigOut;

		ppBigOut.Initialize(fioImg.z + fioImg.x, fioImg.y + fioImg.x, (fioImg.y + fioImg.x) * sizeof(float), sizeof(float) * 8);
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		for (int yy = 0; yy < ppBigOut.Rows(); yy++)
		{
			for (int xx = 0; xx < ppBigOut.Cols(); xx++)
			{
				((float*)ppBigOut.ImageRow(yy))[xx] = 0;
			}
		}

		ppImgOutSub.InitializeSubImage(
			fioImg.z, fioImg.y,
			ppBigOut.RowInc(), ppBigOut.BitsPerPixel(),
			(unsigned char*)ppBigOut.ImageRow(0)
		);
		ppImgOut.Initialize(fioImg.z, fioImg.y, fioImg.y * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceZY(fioImg, x, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		if (bShowFeats)
		{
			if (m_uiInfo & INFO_FLAG_LINE)
			{
				of_paint_line(z, y, ori[0][2], ori[0][1], ppImgOut);
			}
			else
			{
				of_paint_white_circle(z, y, 2 * scale, ppImgOut);
			}
		}
		for (int yy = 0; yy < fioImg.z; yy++)
		{
			for (int xx = 0; xx < fioImg.y; xx++)
			{
				((float*)ppImgOutSub.ImageRow(yy))[xx] =
					((float*)ppImgOut.ImageRow(yy))[xx];
			}
		}
		//output_float( ppBigOut, pcFileName );

		ppImgOutSub.InitializeSubImage(
			fioImg.y, fioImg.x,
			ppBigOut.RowInc(), ppBigOut.BitsPerPixel(),
			(unsigned char*)ppBigOut.ImageRow(fioImg.z)
		);
		ppImgOut.Initialize(fioImg.y, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceXY(fioImg, z, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		if (bShowFeats)
		{
			if (m_uiInfo & INFO_FLAG_LINE)
			{
				of_paint_line(y, x, ori[0][1], ori[0][0], ppImgOut);
			}
			else
			{
				of_paint_white_circle(y, x, 2 * scale, ppImgOut);
			}
		}
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				((float*)ppImgOutSub.ImageRow(xx))[yy] =
					((float*)ppImgOut.ImageRow(yy))[fioImg.x - 1 - xx];
			}
		}
		//output_float( ppBigOut, pcFileName );

		ppImgOutSub.InitializeSubImage(
			fioImg.z, fioImg.x,
			ppBigOut.RowInc(), ppBigOut.BitsPerPixel(),
			(unsigned char*)((float*)ppBigOut.ImageRow(0) + fioImg.y)
		);
		ppImgOut.Initialize(fioImg.z, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceZX(fioImg, y, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		if (bShowFeats)
		{
			if (m_uiInfo & INFO_FLAG_LINE)
			{
				of_paint_line(z, x, ori[0][2], ori[0][0], ppImgOut);
			}
			else
			{
				of_paint_white_circle(z, x, 2 * scale, ppImgOut);
			}
		}
		for (int yy = 0; yy < fioImg.z; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				((float*)ppImgOutSub.ImageRow(yy))[xx] =
					((float*)ppImgOut.ImageRow(yy))[xx];
			}
		}
		output_float(ppBigOut, pcFileName);
	}
	else
	{

		ppImgOut.Initialize(fioImg.z, fioImg.y, fioImg.y * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceZY(fioImg, x, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_zy.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		if (bShowFeats)
			of_paint_white_circle(z, y, 2 * scale, ppImgOut);
		output_float(ppImgOut, pcFileName);

		ppImgOut.Initialize(fioImg.y, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceXY(fioImg, z, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_xy.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		if (bShowFeats)
			of_paint_white_circle(y, x, 2 * scale, ppImgOut);
		output_float(ppImgOut, pcFileName);

		ppImgOut.Initialize(fioImg.z, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceZX(fioImg, y, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_zx.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		if (bShowFeats)
			of_paint_white_circle(z, x, 2 * scale, ppImgOut);
		output_float(ppImgOut, pcFileName);
	}

	return 1;
}

int
Feature3D::ToFileBin(
	FILE *outfile,	// Initalized "wt"
	int bAppearance
)
{
	int iBytes = 0;
	iBytes += fwrite(&m_uiInfo, sizeof(m_uiInfo), 1, outfile);
	iBytes += fwrite(&x, sizeof(x), 1, outfile);
	iBytes += fwrite(&y, sizeof(y), 1, outfile);
	iBytes += fwrite(&z, sizeof(z), 1, outfile);
	iBytes += fwrite(&scale, sizeof(scale), 1, outfile);
	iBytes += fwrite(&ori, sizeof(ori), 1, outfile);
	iBytes += fwrite(&eigs, sizeof(eigs), 1, outfile);
	iBytes += fwrite(&m_pfPC, sizeof(m_pfPC), 1, outfile);
	if (bAppearance)
	{
		iBytes += fwrite(&data_zyx, sizeof(data_zyx), 1, outfile);
	}

	return iBytes;
}

int
Feature3D::FromFileBin(
	FILE *infile,	// Initalized "rt"
	int iFeatureSize
)
{
	int iBytes = 0;

	int iSizeF3D__000 = 5392;// (info+data), no longer supported
	int iSizeF3DI_000 = 68;// 	(info), ok, small size
	int iSizeF3DI_001 = 260;//	(info+PC48) add pcs
	int iSizeF3D__001 = 5584;//	(info+PC48+data)
	int iSizeF3DI_001_PC64 = 324;//	(info+GOH/PC64)
	int iSizeF3D__001_PC64 = 5648;//	(info+GOH/PC64+data)

	iBytes += fread(&m_uiInfo, sizeof(m_uiInfo), 1, infile);
	iBytes += fread(&x, sizeof(x), 1, infile);
	iBytes += fread(&y, sizeof(y), 1, infile);
	iBytes += fread(&z, sizeof(z), 1, infile);
	iBytes += fread(&scale, sizeof(scale), 1, infile);
	iBytes += fread(&ori, sizeof(ori), 1, infile);
	iBytes += fread(&eigs, sizeof(eigs), 1, infile);

	if (iFeatureSize == iSizeF3D__001 || iFeatureSize == iSizeF3DI_001 || iFeatureSize == iSizeF3DI_001_PC64)
	{
		// Read PCs
		memset(&m_pfPC, 0, sizeof(m_pfPC));
		if (iFeatureSize == iSizeF3D__001 || iFeatureSize == iSizeF3DI_001)
		{
			iBytes += fread(&m_pfPC, 48 * sizeof(float), 1, infile);
		}
		else if (iFeatureSize == iSizeF3DI_001_PC64)
		{
			iBytes += fread(&m_pfPC, 64 * sizeof(float), 1, infile);
		}
	}

	if (iFeatureSize == iSizeF3D__000 || iFeatureSize == iSizeF3D__001 || iFeatureSize == iSizeF3D__001_PC64)
	{
		// Read data
		iBytes += fread(&data_zyx, sizeof(data_zyx), 1, infile);
	}

	return iBytes;
}

int
Feature3D::FromFileBinOld(
	FILE *infile	// Initalized "rb"
)
{
	int iBytes = 0;

	iBytes += fread(&x, sizeof(x), 1, infile);
	iBytes += fread(&y, sizeof(y), 1, infile);
	iBytes += fread(&z, sizeof(z), 1, infile);
	iBytes += fread(&scale, sizeof(scale), 1, infile);
	iBytes += fread(&ori, sizeof(ori), 1, infile);
	iBytes += fread(&eigs, sizeof(eigs), 1, infile);
	iBytes += fread(&data_zyx, sizeof(data_zyx), 1, infile);

	return iBytes;
}

void
Feature3D::GaussianWeight(
	float fSigma
)
{
	float fSigmaSqrDiv = 1.0f / (fSigma*fSigma);
	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		float fDistZ = zz - Feature3D::FEATURE_3D_DIM / 2;
		fDistZ *= fDistZ;
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			float fDistY = yy - Feature3D::FEATURE_3D_DIM / 2;
			fDistY *= fDistY;
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				float fDistX = xx - Feature3D::FEATURE_3D_DIM / 2;
				fDistX *= fDistX;

				float fDistSqr = fDistZ + fDistY + fDistX;
				data_zyx[zz][yy][xx] *= exp(-fDistSqr*fSigmaSqrDiv);
			}
		}
	}
}

void
Feature3D::RandomAppearance(
)
{
	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				data_zyx[zz][yy][xx] = rand();
			}
		}
	}
}

void
Feature3D::NormalizeDataPositive(
)
{
	float fMin = 100000;

	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				if (data_zyx[zz][yy][xx] < fMin)
				{
					fMin = data_zyx[zz][yy][xx];
				}
			}
		}
	}
	float fSumSqr = 0;

	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				data_zyx[zz][yy][xx] -= fMin;
				fSumSqr += data_zyx[zz][yy][xx] * data_zyx[zz][yy][xx];
			}
		}
	}
	float fDiv = 1.0f / (float)sqrt(fSumSqr);
	fSumSqr = 0;

	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				data_zyx[zz][yy][xx] *= fDiv;
				fSumSqr += data_zyx[zz][yy][xx] * data_zyx[zz][yy][xx];
			}
		}
	}
}

void
msNormalizeDataPositive(
	float *pfVec,
	int iLength
)
{
	float fMin = 100000;

	for (int zz = 0; zz < iLength; zz++)
	{
		if (pfVec[zz] < fMin)
		{
			fMin = pfVec[zz];
		}

	}
	float fSumSqr = 0;

	for (int zz = 0; zz < iLength; zz++)
	{
		pfVec[zz] -= fMin;
		fSumSqr += pfVec[zz] * pfVec[zz];
	}
	float fDiv = 1.0f / (float)sqrt(fSumSqr);
	fSumSqr = 0;

	for (int zz = 0; zz < iLength; zz++)
	{
		pfVec[zz] *= fDiv;
		fSumSqr += pfVec[zz] * pfVec[zz];
	}
}

typedef struct _COMP_MATCH_VALUE
{
	int iIndex1;
	int iIndex2;
	float fValue;
} COMP_MATCH_VALUE;

//
// _sortAscendingMVNature()
//
// Break sorting ties according to natural order of SIFT descriptor values.
//
int
_sortAscendingMVNature(
	const void *pf1,
	const void *pf2
)
{
	COMP_MATCH_VALUE &f1 = *((COMP_MATCH_VALUE*)pf1);
	COMP_MATCH_VALUE &f2 = *((COMP_MATCH_VALUE*)pf2);
	if (f1.fValue < f2.fValue)
	{
		return -1;
	}
	else if (f1.fValue > f2.fValue)
	{
		return 1;
	}
	else
	{
		// Split ties in terms of index
		if (f1.iIndex1 < f2.iIndex1)
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}
}

void
Feature3D::NormalizeDataRanked(
)
{
	COMP_MATCH_VALUE mvScan[Feature3D::FEATURE_3D_DIM*Feature3D::FEATURE_3D_DIM*Feature3D::FEATURE_3D_DIM];

	// Load up vector
	int k = 0;
	float fMin = 100000;

	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				mvScan[k].iIndex1 = z * 1000000 + y * 1000 + x;
				mvScan[k].iIndex2 = 0;
				mvScan[k].fValue = data_zyx[zz][yy][xx];
				k++;
			}
		}
	}

	// Sort descending
	qsort((void*)(mvScan), Feature3D::FEATURE_3D_DIM*Feature3D::FEATURE_3D_DIM*Feature3D::FEATURE_3D_DIM, sizeof(COMP_MATCH_VALUE), _sortAscendingMVNature);

	// Asign indices
	for (k = 0; k < Feature3D::FEATURE_3D_DIM*Feature3D::FEATURE_3D_DIM*Feature3D::FEATURE_3D_DIM; k++)
	{
		int xx = (mvScan[k].iIndex1) % 1000;
		int yy = (mvScan[k].iIndex1 / 1000) % 1000;
		int zz = (mvScan[k].iIndex1 / 1000000) % 1000;
		data_zyx[zz][yy][xx] = k;
		//data_zyx[zz][yy][xx] = k < (Feature3D::FEATURE_3D_DIM*Feature3D::FEATURE_3D_DIM*Feature3D::FEATURE_3D_DIM)/2 ?  0 : 1;
	}
}

void
Feature3DInfo::NormalizeDataRankedPCs(
)
{
	COMP_MATCH_VALUE mvScan[PC_ARRAY_SIZE];

	// Load up vector
	int k = 0;
	float fMin = 100000;

	for (int zz = 0; zz < PC_ARRAY_SIZE; zz++)
	{
		mvScan[k].iIndex1 = zz;
		mvScan[k].iIndex2 = 0;
		mvScan[k].fValue = m_pfPC[zz];
		k++;
	}

	// Sort descending
	qsort((void*)(mvScan), PC_ARRAY_SIZE, sizeof(COMP_MATCH_VALUE), _sortAscendingMVNature);

	// Asign indices
	for (k = 0; k < PC_ARRAY_SIZE; k++)
	{
		m_pfPC[mvScan[k].iIndex1] = k;
	}
}

void
Feature3DInfo::NormalizeDataPositivePCs(
)
{
	float fMin = 100000;

	for (int zz = 0; zz < PC_ARRAY_SIZE; zz++)
	{
		if (m_pfPC[zz] < fMin)
		{
			fMin = m_pfPC[zz];
		}

	}
	fMin = 0;
	float fSumSqr = 0;

	for (int zz = 0; zz < PC_ARRAY_SIZE; zz++)
	{
		m_pfPC[zz] -= fMin;
		fSumSqr += m_pfPC[zz] * m_pfPC[zz];
	}
	float fDiv = 1.0f / (float)sqrt(fSumSqr);
	fSumSqr = 0;

	for (int zz = 0; zz < PC_ARRAY_SIZE; zz++)
	{
		m_pfPC[zz] *= fDiv;
		fSumSqr += m_pfPC[zz] * m_pfPC[zz];
	}

	// Stretch to 0-256
	for (int zz = 0; zz < PC_ARRAY_SIZE; zz++)
	{
		float fData = m_pfPC[zz];
		int iRank = fData * 256;
		if (iRank >= 64)
			iRank = 63;
		m_pfPC[zz] = iRank;
	}
}


int
Feature3D::RotateData(
	float *ori
)
{
	FEATUREIO fioImg;
	fioImg.x = fioImg.z = fioImg.y = FEATURE_3D_DIM;
	fioImg.pfVectors = &(data_zyx[0][0][0]);
	fioImg.pfMeans = 0;
	fioImg.pfVarrs = 0;
	fioImg.iFeaturesPerVector = 1;
	fioImg.t = 1;

	int iRadius = FEATURE_3D_DIM / 2;

	float ori_here_[3][3];
	memcpy(&(ori_here_[0][0]), ori, sizeof(float) * 3 * 3);

	// Compute inverse rotation matrix (same as transpose)
	// to resample original image in rotation normalized space
	float ori_here[3][3];
	transpose_3x3<float, float>(ori_here_, ori_here);

	Feature3D fT0;

	// Rotate template
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				float pf1[3] = { (float)(xx - iRadius),(float)(yy - iRadius),(float)(zz - iRadius) };
				float pf2[3];

				mult_3x3<float, float>(ori_here, pf1, pf2);

				// Add center offset
				pf2[0] += iRadius;
				pf2[1] += iRadius;
				pf2[2] += iRadius;

				float fPix = fioGetPixel(fioImg, pf2[0], pf2[1], pf2[2]);
				fT0.data_zyx[zz][yy][xx] = fPix;
			}
		}
	}

	// Copy back into this feature
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				data_zyx[zz][yy][xx] = fT0.data_zyx[zz][yy][xx];
			}
		}
	}

	return 1;
}

#define ORIBINS 32


void
mult_4x4_vector(
	float *mat,
	float *vec_in,
	float *vec_out
)
{
	for (int i = 0; i < 4; i++)
	{
		vec_out[i] = 0;
		for (int j = 0; j < 4; j++)
		{
			vec_out[i] += mat[i * 4 + j] * vec_in[j];
		}
	}
}


void
vec3D_norm_3d(
	float *pf1
)
{
	float fSumSqr = pf1[0] * pf1[0] + pf1[1] * pf1[1] + pf1[2] * pf1[2];
	if (fSumSqr > 0)
	{
		float fDiv = 1.0 / sqrt(fSumSqr);
		pf1[0] *= fDiv;
		pf1[1] *= fDiv;
		pf1[2] *= fDiv;
	}
	else
	{
		pf1[0] = 1;
		pf1[1] = 0;
		pf1[2] = 0;
	}
}

float
vec3D_mag(
	float *pf1
)
{
	float fSumSqr = pf1[0] * pf1[0] + pf1[1] * pf1[1] + pf1[2] * pf1[2];
	if (fSumSqr > 0)
	{
		return sqrt(fSumSqr);
	}
	else
	{
		return 0;
	}
}

void
vec3D_cross_3d(
	float *pv1,
	float *pv2,
	float *pvCross
)
{
	pvCross[0] = pv1[1] * pv2[2] - pv1[2] * pv2[1];
	pvCross[1] = -pv1[0] * pv2[2] + pv1[2] * pv2[0];
	pvCross[2] = pv1[0] * pv2[1] - pv1[1] * pv2[0];
}

void
vec3D_diff_3d(
	float *pv1,
	float *pv2,
	float *pv12
)
{
	pv12[0] = pv2[0] - pv1[0];
	pv12[1] = pv2[1] - pv1[1];
	pv12[2] = pv2[2] - pv1[2];
}

void
vec3D_summ_3d(
	float *pv1,
	float *pv2,
	float *pv12
)
{
	pv12[0] = pv2[0] + pv1[0];
	pv12[1] = pv2[1] + pv1[1];
	pv12[2] = pv2[2] + pv1[2];
}


float
vec3D_dist_3d(
	float *pv1,
	float *pv2
)
{

	float dx = pv2[0] - pv1[0];
	float dy = pv2[1] - pv1[1];
	float dz = pv2[2] - pv1[2];
	return sqrt(dx*dx + dy*dy + dz*dz);
}
float
vec3D_dot_3d(
	const float *pv1,
	const float *pv2
)
{
	return pv1[0] * pv2[0] + pv1[1] * pv2[1] + pv1[2] * pv2[2];
}


//
// vec3D_euler_rotation()
//
// Calculate euler rotation matrix from standard reference space
// (100), (010), (001)
// to an arbitrary.
//
void
vec3D_euler_rotation(
	float *pfx, // 1x3
	float *pfy, // 1x3
	float *pfz, // 1x3
	float *ori // 3x3
)
{
	// Better check for degeneracies, gimbal lock

	float fZXYMagSqr = pfz[0] * pfz[0] + pfz[1] * pfz[1];

	float fAlph = atan2(-pfz[1], pfz[0]);
	float fBeta = atan2(pfz[2], sqrt(fZXYMagSqr));
	float fGamm = atan2(pfy[2], pfx[2]);

	float ca = cos(fAlph);
	float sa = sin(fAlph);

	float cb = cos(fBeta);
	float sb = sin(fBeta);

	float cg = cos(fGamm);
	float sg = sin(fGamm);

	ori[3 * 0 + 0] = ca*cg - cb*sa*sg;
	ori[3 * 0 + 1] = cg*sa - ca*cb*sg;
	ori[3 * 0 + 2] = sb*sg;

	ori[3 * 1 + 0] = -cb*cg*sa - ca*sg;
	ori[3 * 1 + 1] = ca*cb*cg - sa*sg;
	ori[3 * 1 + 2] = cg*sb;

	ori[3 * 2 + 0] = sa*sb;
	ori[3 * 2 + 1] = -ca*sb;
	ori[3 * 2 + 2] = cb;
}

//
// vec3D_euler_rotation()
//
// Calculate euler rotation matrix from an arbitrary basis
//
void
vec3D_euler_rotation(
	float *pfx0, // 1x3
	float *pfy0, // 1x3
	float *pfz0, // 1x3

	float *pfx1, // 1x3
	float *pfy1, // 1x3
	float *pfz1, // 1x3

	float *ori // 3x3
)
{
	// Better check for degeneracies, gimbal lock

	float fz0z1Dot = pfz0[0] * pfz1[0] + pfz0[1] * pfz1[1] + pfz0[2] * pfz1[2];
	float pfz0z1Cross[3];
	if (fz0z1Dot == 1.0 || fz0z1Dot == -1.0)
	{
		// Invalid
		return;
	}
	vec3D_cross_3d(pfz0, pfz1, pfz0z1Cross);
	float fz0z1CrossMag = vec3D_mag(pfz0z1Cross);
	if (fz0z1CrossMag == 0)
	{
		// Invalid
		return;
	}

	float fx0CrossDot = vec3D_dot_3d(pfx0, pfz0z1Cross);
	float fy0CrossDot = vec3D_dot_3d(pfy0, pfz0z1Cross);

	float fx1CrossDot = vec3D_dot_3d(pfx1, pfz0z1Cross);
	float fy1CrossDot = vec3D_dot_3d(pfy1, pfz0z1Cross);

	float fAlph = atan2(fx0CrossDot, fy0CrossDot);
	float fBeta = atan2(fz0z1Dot, fz0z1CrossMag);
	float fGamm = -atan2(fx1CrossDot, fy1CrossDot);

	float ca = cos(fAlph);
	float sa = sin(fAlph);

	float cb = cos(fBeta);
	float sb = sin(fBeta);

	float cg = cos(fGamm);
	float sg = sin(fGamm);

	ori[3 * 0 + 0] = ca*cg - cb*sa*sg;
	//ori[3*0+1]= cg*sa-ca*cb*sg;
	ori[3 * 0 + 1] = cg*sa + ca*cb*sg;
	ori[3 * 0 + 2] = sb*sg;

	ori[3 * 1 + 0] = -cb*cg*sa - ca*sg;
	ori[3 * 1 + 1] = ca*cb*cg - sa*sg;
	ori[3 * 1 + 2] = cg*sb;

	ori[3 * 2 + 0] = sa*sb;
	ori[3 * 2 + 1] = -ca*sb;
	ori[3 * 2 + 2] = cb;

	// Another definition

	ori[3 * 0 + 0] = ca*cg - cb*sa*sg;
	//ori[3*0+1]= cg*sa-ca*cb*sg;
	ori[3 * 0 + 1] = cg*sa + ca*cb*sg;
	ori[3 * 0 + 2] = sb*sg;

	ori[3 * 1 + 0] = -cb*cg*sa - ca*sg;
	ori[3 * 1 + 1] = ca*cb*cg - sa*sg;
	ori[3 * 1 + 2] = cg*sb;

	ori[3 * 2 + 0] = sa*sb;
	ori[3 * 2 + 1] = -ca*sb;
	ori[3 * 2 + 2] = cb;
}

void
Feature3DData::PlanesDescriptor(
	int &iCode
)
{
	FEATUREIO fioImg;
	fioImg.x = fioImg.z = fioImg.y = FEATURE_3D_DIM;
	fioImg.pfVectors = &(data_zyx[0][0][0]);
	fioImg.pfMeans = 0;
	fioImg.pfVarrs = 0;
	fioImg.iFeaturesPerVector = 1;
	fioImg.t = 1;


	// Derivative iamges
	FEATUREIO fioDx = fioImg;
	FEATUREIO fioDy = fioImg;
	FEATUREIO fioDz = fioImg;
	Feature3D fdx;
	Feature3D fdy;
	Feature3D fdz;
	fioDx.pfVectors = &(fdx.data_zyx[0][0][0]);
	fioDy.pfVectors = &(fdy.data_zyx[0][0][0]);
	fioDz.pfVectors = &(fdz.data_zyx[0][0][0]);

	// Angles and blurred versions
	FEATUREIO fioT0 = fioImg;
	FEATUREIO fioT1 = fioImg;
	FEATUREIO fioT2 = fioImg;
	Feature3D fT0;
	Feature3D fT1;
	Feature3D fT2;
	fioT0.pfVectors = &(fT0.data_zyx[0][0][0]);
	fioT1.pfVectors = &(fT1.data_zyx[0][0][0]);
	fioT2.pfVectors = &(fT2.data_zyx[0][0][0]);
	fioSet(fioT0, 0);

	float fRadius = fioImg.x / 2;
	float fRadiusSqr = (fioImg.x / 2)*(fioImg.x / 2);

	int iSampleCount = 0;



	char pcFileName[400];
	PpImage ppImgOut;

	ppImgOut.InitializeSubImage(fioImg.z, fioImg.y, fioImg.y * sizeof(float), sizeof(float) * 8, (unsigned char*)fioGetVector(fioT0, 0, 0, 0));
	fioFeatureSliceZY(fioImg, Feature3D::FEATURE_3D_DIM / 2, 0, (float*)ppImgOut.ImageRow(0));


	ppImgOut.InitializeSubImage(fioImg.y, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8, (unsigned char*)fioGetVector(fioT0, 0, 0, 1));
	fioFeatureSliceXY(fioImg, Feature3D::FEATURE_3D_DIM / 2, 0, (float*)ppImgOut.ImageRow(0));

	ppImgOut.InitializeSubImage(fioImg.z, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8, (unsigned char*)fioGetVector(fioT0, 0, 0, 2));
	fioFeatureSliceZX(fioImg, Feature3D::FEATURE_3D_DIM / 2, 0, (float*)ppImgOut.ImageRow(0));

	// Normalize
	float fSum = 0;
	float fMax = fT0.data_zyx[0][0][0], fMin = fT0.data_zyx[0][0][0];
	for (int zz = 0; zz < 3; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				fSum += fT0.data_zyx[zz][yy][xx];
			}
		}
	}
	float fMean = fSum / (3 * Feature3D::FEATURE_3D_DIM*Feature3D::FEATURE_3D_DIM);
	float fSumSqr = 0;
	fSum = 0;

	for (int zz = 0; zz < 3; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				fT0.data_zyx[zz][yy][xx] -= fMean;
				fSum += fT0.data_zyx[zz][yy][xx];
				fSumSqr += fT0.data_zyx[zz][yy][xx] * fT0.data_zyx[zz][yy][xx];
			}
		}
	}

	float fDiv = 1.0f / (float)sqrt(fSumSqr);
	fSum = 0;
	fSumSqr = 0;

	for (int zz = 0; zz < 3; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				fT0.data_zyx[zz][yy][xx] *= fDiv;
				fSum += fT0.data_zyx[zz][yy][xx];
				fSumSqr += fT0.data_zyx[zz][yy][xx] * fT0.data_zyx[zz][yy][xx];
			}
		}
	}

	// Copy back into data
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				data_zyx[zz][yy][xx] = fT0.data_zyx[zz][yy][xx];
			}
		}
	}
}

void
Feature3DData::CubeDescriptor(
	int &iCode
)
{
	FEATUREIO fioImg;
	fioImg.x = fioImg.z = fioImg.y = FEATURE_3D_DIM;
	fioImg.pfVectors = &(data_zyx[0][0][0]);
	fioImg.pfMeans = 0;
	fioImg.pfVarrs = 0;
	fioImg.iFeaturesPerVector = 1;
	fioImg.t = 1;

	// Angles and blurred versions
	FEATUREIO fioT0 = fioImg;
	FEATUREIO fioT1 = fioImg;
	FEATUREIO fioT2 = fioImg;
	Feature3D fT0;
	Feature3D fT1;
	Feature3D fT2;
	fioT0.pfVectors = &(fT0.data_zyx[0][0][0]);
	fioT1.pfVectors = &(fT1.data_zyx[0][0][0]);
	fioT2.pfVectors = &(fT2.data_zyx[0][0][0]);
	fioSet(fioT0, 0);

	fioT0.x /= 2;
	fioT0.y /= 2;
	fioT0.z /= 2;
	fioSubSample(fioImg, fioT0);


	// Normalize
	float fSum = 0;
	float fMax = fT0.data_zyx[0][0][0], fMin = fT0.data_zyx[0][0][0];
	for (int zz = 0; zz < fioT0.z; zz++)
	{
		for (int yy = 0; yy < fioT0.y; yy++)
		{
			for (int xx = 0; xx < fioT0.x; xx++)
			{
				fSum += fT0.data_zyx[zz][yy][xx];
			}
		}
	}
	float fMean = fSum / (3 * Feature3D::FEATURE_3D_DIM*Feature3D::FEATURE_3D_DIM);
	float fSumSqr = 0;
	fSum = 0;

	for (int zz = 0; zz < fioT0.z; zz++)
	{
		for (int yy = 0; yy < fioT0.y; yy++)
		{
			for (int xx = 0; xx < fioT0.x; xx++)
			{
				fT0.data_zyx[zz][yy][xx] -= fMean;
				fSum += fT0.data_zyx[zz][yy][xx];
				fSumSqr += fT0.data_zyx[zz][yy][xx] * fT0.data_zyx[zz][yy][xx];
			}
		}
	}

	float fDiv = 1.0f / (float)sqrt(fSumSqr);
	fSum = 0;
	fSumSqr = 0;
	for (int zz = 0; zz < fioT0.z; zz++)
	{
		for (int yy = 0; yy < fioT0.y; yy++)
		{
			for (int xx = 0; xx < fioT0.x; xx++)
			{
				fT0.data_zyx[zz][yy][xx] *= fDiv;
				fSum += fT0.data_zyx[zz][yy][xx];
				fSumSqr += fT0.data_zyx[zz][yy][xx] * fT0.data_zyx[zz][yy][xx];
			}
		}
	}

	// Copy back into data
	for (int zz = 0; zz < fioT0.z; zz++)
	{
		for (int yy = 0; yy < fioT0.y; yy++)
		{
			for (int xx = 0; xx < fioT0.x; xx++)
			{
				data_zyx[zz][yy][xx] = fT0.data_zyx[zz][yy][xx];
			}
		}
	}
}


void
Feature3DData::OrientationInvariantDescriptor(
	int &iCode
)
{
	FEATUREIO fioImg;
	fioImg.x = fioImg.z = fioImg.y = FEATURE_3D_DIM;
	fioImg.pfVectors = &(data_zyx[0][0][0]);
	fioImg.pfMeans = 0;
	fioImg.pfVarrs = 0;
	fioImg.iFeaturesPerVector = 1;
	fioImg.t = 1;


	// Derivative iamges
	FEATUREIO fioDx = fioImg;
	FEATUREIO fioDy = fioImg;
	FEATUREIO fioDz = fioImg;
	Feature3D fdx;
	Feature3D fdy;
	Feature3D fdz;
	fioDx.pfVectors = &(fdx.data_zyx[0][0][0]);
	fioDy.pfVectors = &(fdy.data_zyx[0][0][0]);
	fioDz.pfVectors = &(fdz.data_zyx[0][0][0]);

	// Angles and blurred versions
	FEATUREIO fioT0 = fioImg;
	FEATUREIO fioT1 = fioImg;
	FEATUREIO fioT2 = fioImg;
	Feature3D fT0;
	Feature3D fT1;
	Feature3D fT2;
	fioT0.pfVectors = &(fT0.data_zyx[0][0][0]);
	fioT1.pfVectors = &(fT1.data_zyx[0][0][0]);
	fioT2.pfVectors = &(fT2.data_zyx[0][0][0]);
	fioSet(fioT0, 0);

	// Flip x/z axes: test to see if peaks found are correctly flipped, yes works.
	//for( int zz = 0; zz < fioImg.z; zz++ )
	//{
	//	for( int yy = 0; yy < fioImg.y; yy++ )
	//	{
	//		for( int xx = 0; xx < fioImg.x; xx++ )
	//		{
	//			fT0.data_zyx[zz][yy][xx] = data_zyx[zz][yy][xx];
	//		}
	//	}
	//}
	//for( int zz = 0; zz < fioImg.z; zz++ )
	//{
	//	for( int yy = 0; yy < fioImg.y; yy++ )
	//	{
	//		for( int xx = 0; xx < fioImg.x; xx++ )
	//		{
	//			data_zyx[zz][yy][xx] = fT0.data_zyx[xx][yy][zz];
	//		}
	//	}
	//}
	//fioSet( fioT0, 0 );

	fioGenerateEdgeImages3D(fioImg, fioDx, fioDy, fioDz);

	float fRadius = fioImg.x / 2;
	float fRadiusSqr = (fioImg.x / 2)*(fioImg.x / 2);

	int iSampleCount = 0;

	// Determine dominant orientation of feature: azimuth/elevation
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				float dx = xx - fioImg.x / 2;
				float dy = yy - fioImg.y / 2;
				float dz = zz - fioImg.z / 2;
				float fLocRadiusSqr = dz*dz + dy*dy + dx*dx;
				if (fLocRadiusSqr < fRadiusSqr && fLocRadiusSqr > 0)
				{
					// Keep this sample
					float pfEdge[3];
					float pfEdgeUnit[3];
					pfEdge[0] = fioGetPixel(fioDx, xx, yy, zz);
					pfEdge[1] = fioGetPixel(fioDy, xx, yy, zz);
					pfEdge[2] = fioGetPixel(fioDz, xx, yy, zz);
					float fEdgeMag = vec3D_mag(pfEdge);
					if (fEdgeMag == 0)
					{
						continue;
					}
					memcpy(pfEdgeUnit, pfEdge, sizeof(pfEdgeUnit));
					vec3D_norm_3d(pfEdgeUnit);

					iSampleCount++;

					// Location relative to center
					float pfLoc[3];
					float pfLocUnit[3];
					pfLoc[0] = dx;
					pfLoc[1] = dy;
					pfLoc[2] = dz;
					float fLocMag = vec3D_mag(pfLoc);
					memcpy(pfLocUnit, pfLoc, sizeof(pfLocUnit));
					vec3D_norm_3d(pfLocUnit);

					// Dot product cosine, between -1 & 1
					float fCosEdgeLoc = vec3D_dot_3d(pfEdgeUnit, pfLocUnit);

					// Edge magnitudes: in direction parallel to sample, perpendicular to sample
					float fEdgeMagPar = vec3D_dot_3d(pfEdge, pfLocUnit);
					float fEdgeMagPrpSqr = fEdgeMag*fEdgeMag - fEdgeMagPar*fEdgeMagPar;
					assert(fEdgeMagPrpSqr >= 0);
					if (fEdgeMagPrpSqr <= 0)
					{
						continue;
					}
					float fEdgeMagPrp = sqrt(fEdgeMagPrpSqr);

					int iRad = fLocMag;
					int iBinsPerRad = 2 * iRad*iRad;
					int iBin = ((fCosEdgeLoc + 1.0f) / 2.0f)*iBinsPerRad;
					if (iBin >= iBinsPerRad)
					{
						iBin = iBinsPerRad - 1;
					}

					float *pfPix;

					pfPix = fioGetVector(fioT0, 0, iBin, iRad);
					*pfPix += fEdgeMag;


					//pfPix = fioGetVector( fioT0, 0, iBin, iRad );
					//*pfPix += fEdgeMagPar;
					//pfPix = fioGetVector( fioT0, 1, iBin, iRad );
					//*pfPix += fEdgeMagPrp;
				}
			}
		}
	}

	//
	fT0.NormalizeDataPositive();

	fioSet(fioImg, 0);
	// Copy back into data
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				float dz = zz - fioImg.z / 2;
				float dy = yy - fioImg.y / 2;
				float dx = xx - fioImg.x / 2;
				{
					data_zyx[zz][yy][xx] = fT0.data_zyx[zz][yy][xx];
				}
			}
		}
	}
}

void
Feature3DData::DeterminePartialOrientationCube(
	int &iCode,
	float *ori
)
{
	FEATUREIO fioImg;
	fioImg.x = fioImg.z = fioImg.y = FEATURE_3D_DIM;
	fioImg.pfVectors = &(data_zyx[0][0][0]);
	fioImg.pfMeans = 0;
	fioImg.pfVarrs = 0;
	fioImg.iFeaturesPerVector = 1;
	fioImg.t = 1;


	// Derivative iamges
	FEATUREIO fioDx = fioImg;
	FEATUREIO fioDy = fioImg;
	FEATUREIO fioDz = fioImg;
	Feature3D fdx;
	Feature3D fdy;
	Feature3D fdz;
	fioDx.pfVectors = &(fdx.data_zyx[0][0][0]);
	fioDy.pfVectors = &(fdy.data_zyx[0][0][0]);
	fioDz.pfVectors = &(fdz.data_zyx[0][0][0]);

	// Angles and blurred versions
	FEATUREIO fioT0 = fioImg;
	FEATUREIO fioT1 = fioImg;
	FEATUREIO fioT2 = fioImg;
	Feature3D fT0;
	Feature3D fT1;
	Feature3D fT2;
	fioT0.pfVectors = &(fT0.data_zyx[0][0][0]);
	fioT1.pfVectors = &(fT1.data_zyx[0][0][0]);
	fioT2.pfVectors = &(fT2.data_zyx[0][0][0]);
	fioSet(fioT0, 0);

	// Flip x/z axes: test to see if peaks found are correctly flipped, yes works.
	//for( int zz = 0; zz < fioImg.z; zz++ )
	//{
	//	for( int yy = 0; yy < fioImg.y; yy++ )
	//	{
	//		for( int xx = 0; xx < fioImg.x; xx++ )
	//		{
	//			fT0.data_zyx[zz][yy][xx] = data_zyx[zz][yy][xx];
	//		}
	//	}
	//}
	//for( int zz = 0; zz < fioImg.z; zz++ )
	//{
	//	for( int yy = 0; yy < fioImg.y; yy++ )
	//	{
	//		for( int xx = 0; xx < fioImg.x; xx++ )
	//		{
	//			data_zyx[zz][yy][xx] = fT0.data_zyx[xx][yy][zz];
	//		}
	//	}
	//}
	//fioSet( fioT0, 0 );





	// Major peaks
	LOCATION_VALUE_XYZ_ARRAY lvaPeaks;
	LOCATION_VALUE_XYZ lvData[ORIBINS*ORIBINS / 8];
	lvaPeaks.plvz = &(lvData[0]);

	fioGenerateEdgeImages3D(fioImg, fioDx, fioDy, fioDz);


	float fRadius = fioImg.x / 2;
	float fRadiusSqr = (fioImg.x / 2)*(fioImg.x / 2);

	int iSampleCount = 0;

	// Determine dominant orientation of feature: azimuth/elevation
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				float dz = zz - fioImg.z / 2;
				float dy = yy - fioImg.y / 2;
				float dx = xx - fioImg.x / 2;
				if (dz*dz + dy*dy + dx*dx < fRadiusSqr)
				{
					// Keep this sample
					float pfEdge[3];
					pfEdge[0] = fioGetPixel(fioDx, xx, yy, zz);
					pfEdge[1] = fioGetPixel(fioDy, xx, yy, zz);
					pfEdge[2] = fioGetPixel(fioDz, xx, yy, zz);
					float fEdgeMagSqr = pfEdge[0] * pfEdge[0] + pfEdge[1] * pfEdge[1] + pfEdge[2] * pfEdge[2];
					if (fEdgeMagSqr == 0)
					{
						continue;
					}
					float fEdgeMag = sqrt(fEdgeMagSqr);

					iSampleCount++;

					// Vector length fRadius in direction of edge
					float pfEdgeUnit[3];
					for (int i = 0; i < 3; i++)
					{
						pfEdgeUnit[i] = pfEdge[i] * fRadius / fEdgeMag;
					}
					for (int i = 0; i < 3; i++)
					{
						pfEdgeUnit[i] += fRadius;
					}


					// Bilinear interpolation

					//int iZCoord;
					//float fZCont;
					//PpImage ppImgRef;
					//if( pfEdgeUnit[2] < 0.5f )
					//{
					//	iZCoord = 0;
					//	fZCont = 1.0f;
					//	ppImgRef.InitializeSubImage(
					//		fioT0.y, fioT0.x, fioT0.x*sizeof(float), sizeof(float)*8,
					//		(unsigned char*)fioGetVector( fioT0, 0, 0, iZCoord ) 
					//		);
					//	bilinear_interpolation_paint_float( ppImgRef, fEdgeMag,  pfEdgeUnit[1], pfEdgeUnit[0] );
					//}
					//else if( pfEdgeUnit[2] >= ((float)(fioT0.z - 0.5f)) )
					//{
					//	iZCoord = fioT0.z - 1;
					//	fZCont = 0.0f;
					//	ppImgRef.InitializeSubImage(
					//		fioT0.y, fioT0.x, fioT0.x*sizeof(float), sizeof(float)*8,
					//		(unsigned char*)fioGetVector( fioT0, 0, 0, iZCoord ) 
					//		);
					//	bilinear_interpolation_paint_float( ppImgRef, fEdgeMag, pfEdgeUnit[1], pfEdgeUnit[0] );
					//}
					//else
					//{
					//	float fMinusHalf = pfEdgeUnit[2] - 0.5f;
					//	iZCoord = (int)floor( fMinusHalf );
					//	fZCont = 1.0f - (fMinusHalf - ((float)iZCoord));

					//	ppImgRef.InitializeSubImage(
					//		fioT0.y, fioT0.x, fioT0.x*sizeof(float), sizeof(float)*8,
					//		(unsigned char*)fioGetVector( fioT0, 0, 0, iZCoord ) 
					//		);
					//	bilinear_interpolation_paint_float( ppImgRef, fZCont*fEdgeMag, pfEdgeUnit[1], pfEdgeUnit[0] );

					//	ppImgRef.InitializeSubImage(
					//		fioT0.y, fioT0.x, fioT0.x*sizeof(float), sizeof(float)*8,
					//		(unsigned char*)fioGetVector( fioT0, 0, 0, iZCoord+1 ) 
					//		);
					//	bilinear_interpolation_paint_float( ppImgRef, (1.0f-fZCont)*fEdgeMag, pfEdgeUnit[1], pfEdgeUnit[0] );
					//}

					//bilinear_interpolation_paint_float
					float *pfPix = fioGetVector(fioT0, pfEdgeUnit[0] + 0.5, pfEdgeUnit[1] + 0.5, pfEdgeUnit[2] + 0.5);

					// Add pixel
					*pfPix += fEdgeMag;
				}
			}
		}
	}

	PpImage ppImgRef;
	char pcFileName[400];

	sprintf(pcFileName, "bin%5.5d_dx", iCode);
	//fdx.OutputVisualFeature( pcFileName );
	sprintf(pcFileName, "bin%5.5d_dy", iCode);
	//fdy.OutputVisualFeature( pcFileName );
	sprintf(pcFileName, "bin%5.5d_dz", iCode);
	//fdz.OutputVisualFeature( pcFileName );

	gb3d_blur3d(fioT0, fioT1, fioT2, 1.0f, 0.01);

	regFindFEATUREIOPeaks(lvaPeaks, fioT2);
	lvSortHighLow(lvaPeaks);
	sprintf(pcFileName, "bin%5.5d_orig", iCode);
	//fioWrite( fioT0, pcFileName );
	//fT0.OutputVisualFeature( pcFileName );

	sprintf(pcFileName, "bin%5.5d_blur", iCode);
	//fioWrite( fioT2, pcFileName );
	//fT2.OutputVisualFeature( pcFileName );

	float pfP1[3]; //z
	float pfP2[3]; //y
	float pfP3[3]; //x

				   // Testing: output major orientations into descriptor, for in-code tesing

				   // Set my descriptor to 0
	fioSet(fioImg, 0);
	LOCATION_VALUE_XYZ_ARRAY lvaPeaks2;
	LOCATION_VALUE_XYZ lvData2[ORIBINS*ORIBINS / 8];
	lvaPeaks2.plvz = &(lvData2[0]);
	for (int i = 0; i < lvaPeaks.iCount && i < fioImg.z; i++)
	{
		// Primary orientation unit vector
		// Should interpolate here for correctnewss
		pfP1[0] = lvaPeaks.plvz[i].x - fRadius;
		pfP1[1] = lvaPeaks.plvz[i].y - fRadius;
		pfP1[2] = lvaPeaks.plvz[i].z - fRadius;
		vec3D_norm_3d(pfP1);

		this->data_zyx[0][i][0] = pfP1[0];
		this->data_zyx[0][i][1] = pfP1[1];
		this->data_zyx[0][i][2] = pfP1[2];

		// Compute secondary direction unit vector
		// perpendicular to primary

		fioSet(fioT0, 0);
		for (int zz = 0; zz < fioImg.z; zz++)
		{
			for (int yy = 0; yy < fioImg.y; yy++)
			{
				for (int xx = 0; xx < fioImg.x; xx++)
				{
					float dx = xx - fioImg.x / 2;
					float dy = yy - fioImg.y / 2;
					float dz = zz - fioImg.z / 2;
					float fLocRadiusSqr = dz*dz + dy*dy + dx*dx;
					if (fLocRadiusSqr < fRadiusSqr)
					{
						// Vector: intensity edge
						float pfEdge[3];
						float pfEdgeUnit[3];
						pfEdge[0] = fioGetPixel(fioDx, xx, yy, zz);
						pfEdge[1] = fioGetPixel(fioDy, xx, yy, zz);
						pfEdge[2] = fioGetPixel(fioDz, xx, yy, zz);
						float fEdgeMag = vec3D_mag(pfEdge);
						if (fEdgeMag == 0)
						{
							continue;
						}
						memcpy(pfEdgeUnit, pfEdge, sizeof(pfEdgeUnit));
						vec3D_norm_3d(pfEdgeUnit);

						// Remove component parallel to primary orientatoin
						float pVecPerp[3];
						float fParallelMag = vec3D_dot_3d(pfP1, pfEdgeUnit);
						pVecPerp[0] = pfEdgeUnit[0] - fParallelMag*pfP1[0];
						pVecPerp[1] = pfEdgeUnit[1] - fParallelMag*pfP1[1];
						pVecPerp[2] = pfEdgeUnit[2] - fParallelMag*pfP1[2];

						// Normalize
						vec3D_norm_3d(pVecPerp);

						// Vector length fRadius in direction of edge, centered in image
						for (int i = 0; i < 3; i++)
						{
							pVecPerp[i] *= fRadius;
							pVecPerp[i] += fRadius;
						}

						//bilinear_interpolation_paint_float
						float *pfPix = fioGetVector(fioT0, pVecPerp[0] + 0.5, pVecPerp[1] + 0.5, pVecPerp[2] + 0.5);

						// Add pixel
						*pfPix += fEdgeMag;

					}
				}
			}
		}

		// Blur, find peaks
		gb3d_blur3d(fioT0, fioT1, fioT2, 1.0f, 0.01);
		regFindFEATUREIOPeaks(lvaPeaks2, fioT2);
		lvSortHighLow(lvaPeaks2);
		sprintf(pcFileName, "bin%5.5d_orig", iCode);


		float pfP2[3];
		if (lvaPeaks2.iCount > 0)
		{
			pfP2[0] = lvaPeaks2.plvz[0].x - fRadius;
			pfP2[1] = lvaPeaks2.plvz[0].y - fRadius;
			pfP2[2] = lvaPeaks2.plvz[0].z - fRadius;
			vec3D_norm_3d(pfP2);
		}
		else
		{
			pfP2[0] = 0;
			pfP2[1] = 0;
			pfP2[2] = 0;
		}

		// Save major peak
		this->data_zyx[0][i][3] = pfP2[0];
		this->data_zyx[0][i][4] = pfP2[1];
		this->data_zyx[0][i][5] = pfP2[2];
	}
	// return - this is just for testing
	return;

	// Major direction unit vector
	// Should interpolate here for correctnewss
	pfP1[0] = lvaPeaks.plvz[0].x - fRadius;
	pfP1[1] = lvaPeaks.plvz[0].y - fRadius;
	pfP1[2] = lvaPeaks.plvz[0].z - fRadius;
	vec3D_norm_3d(pfP1);

	// Calculate offset start bin for radius
	int piRadOffset[10];
	for (int iRad = 0; iRad < 10; iRad++)
	{
		int iBinsPerRad = iRad*iRad;
		if (iRad == 0)
		{
			piRadOffset[iRad] = 0;
		}
		else
		{
			piRadOffset[iRad] = piRadOffset[iRad - 1] + iBinsPerRad;
		}
	}

	// Compute descriptors
	fioSet(fioT0, 0);
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				float dx = xx - fioImg.x / 2;
				float dy = yy - fioImg.y / 2;
				float dz = zz - fioImg.z / 2;
				float fLocRadiusSqr = dz*dz + dy*dy + dx*dx;
				if (fLocRadiusSqr < fRadiusSqr)
				{
					// Vector: intensity edge
					//float pfEdge[3];
					//float pfEdgeUnit[3];
					//pfEdge[0] = fioGetPixel( fioDx, xx, yy, zz );
					//pfEdge[1] = fioGetPixel( fioDy, xx, yy, zz );
					//pfEdge[2] = fioGetPixel( fioDz, xx, yy, zz );
					//float fEdgeMag = vec3D_mag( pfEdge );
					//if( fEdgeMag == 0 )
					//{
					//	continue;
					//}
					//memcpy( pfEdgeUnit, pfEdge, sizeof(pfEdgeUnit) );
					//vec3D_norm_3d( pfEdgeUnit );

					//iSampleCount++;

					// Vector: location relative to center
					float pfLoc[3];
					float pfLocUnit[3];
					pfLoc[0] = dx;
					pfLoc[1] = dy;
					pfLoc[2] = dz;
					float fLocMag = vec3D_mag(pfLoc);
					memcpy(pfLocUnit, pfLoc, sizeof(pfLocUnit));
					vec3D_norm_3d(pfLocUnit);

					// Z coordinate is the magnitude of the location vector projected onto the unit principal direction
					float fZMag = vec3D_dot_3d(pfP1, pfLoc);
					int iZ = (int)(fZMag + fRadius);
					if (iZ >= fioImg.z)
					{
						iZ = fioImg.z - 1;
					}

					// Vector: perpendicular to principal direction, towards location
					float pVecPerp[3];
					float pVecPerpUnit[3];
					pVecPerp[0] = pfLoc[0] - fZMag*pfP1[0];
					pVecPerp[1] = pfLoc[1] - fZMag*pfP1[1];
					pVecPerp[2] = pfLoc[2] - fZMag*pfP1[2];
					memcpy(pVecPerpUnit, pVecPerp, sizeof(pVecPerpUnit));
					vec3D_norm_3d(pVecPerpUnit);

					float fRadSqr = fLocMag*fLocMag - fZMag*fZMag;
					float fRad = 0;
					if (fRadSqr > 0)
					{
						fRad = sqrt(fRadSqr);
					}

					// Dot product cosine, between -1 & 1
					//float fCosEdgeLoc = vec3D_dot_3d( pfEdgeUnit, pVecPerpUnit );

					// Edge magnitudes: in direction parallel to sample, perpendicular to sample
					//float fEdgeMagPar = vec3D_dot_3d( pfEdge, pfLocUnit );
					//float fEdgeMagPrpSqr = fEdgeMag*fEdgeMag - fEdgeMagPar*fEdgeMagPar;
					//assert( fEdgeMagPrpSqr >= 0 );
					//if( fEdgeMagPrpSqr <= 0 )
					//{
					//	continue;
					//}
					//float fEdgeMagPrp = sqrt( fEdgeMagPrpSqr );

					//int iRad = fLocMag;
					//int iBinsPerRad = iRad*iRad;
					//int iBin = ((fCosEdgeLoc+1.0f)/2.0f)*iBinsPerRad;
					//if( iBin >= iBinsPerRad )
					//{
					//	iBin = iBinsPerRad-1;
					//}

					float *pfPix;

					float fValue = data_zyx[xx][yy][zz];

					// Update sum
					//pfPix = fioGetVector( fioT0, piRadOffset[iRad]+iBin, 0, iZ  );
					//*pfPix += fEdgeMag;

					pfPix = fioGetVector(fioT0, (int)fRad, iZ, 0);
					*pfPix += fValue;

					// Update count
					pfPix = fioGetVector(fioT0, (int)fRad, iZ, 1);
					*pfPix += 1;

					//pfPix = fioGetVector( fioT0, 0, iBin, iRad );
					//*pfPix += fEdgeMagPar;
					//pfPix = fioGetVector( fioT0, 1, iBin, iRad );
					//*pfPix += fEdgeMagPrp;
				}
			}
		}
	}


	PpImage ppImgOut;
	//ppImgOut.InitializeSubImage( 11, 11, 11*sizeof(float), sizeof(float)*8, (unsigned char*)&(fT0.data_zyx[0][0][0]) );
	//output_float( ppImgOut, "descriptor.pgm" );

	// Normalize all bins
	for (int yy = 0; yy < fioImg.y; yy++)
	{
		for (int xx = 0; xx < fioImg.x; xx++)
		{
			if (fT0.data_zyx[0][yy][xx] > 0)
			{
				fT0.data_zyx[0][yy][xx] /= fT0.data_zyx[1][yy][xx];
			}
		}
	}

	sprintf(pcFileName, "bin%5.5d_desc_norm.pgm", iCode);
	ppImgOut.InitializeSubImage(11, 11, 11 * sizeof(float), sizeof(float) * 8, (unsigned char*)&(fT0.data_zyx[0][0][0]));
	//output_float( ppImgOut, pcFileName );

	//ppImgOut.InitializeSubImage( 11, 11, 11*sizeof(float), sizeof(float)*8, (unsigned char*)&(fT0.data_zyx[1][0][0]) );
	//output_float( ppImgOut, "descriptor_counts.pgm" );

	//fT0.NormalizeDataPositive();

	fioSet(fioImg, 0);
	// Copy back into data
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				if (zz > 0)
				{
					data_zyx[zz][yy][xx] = 0;
				}
				else
				{
					data_zyx[zz][yy][xx] = fT0.data_zyx[zz][yy][xx];
				}
			}
		}
	}

	int iCode0 = (lvaPeaks.plvz[0].z*(fioImg.x*fioImg.x))
		+ (lvaPeaks.plvz[0].y*(fioImg.x))
		+ lvaPeaks.plvz[0].x;

	int iCode1 = (lvaPeaks.plvz[1].z*(fioImg.x*fioImg.x))
		+ (lvaPeaks.plvz[1].y*(fioImg.x))
		+ lvaPeaks.plvz[1].x;

	iCode = iCode0*iCode1;
}




void
Feature3DData::DetermineOrientationCubeIntensity(
	int &iCode,
	float *ori
)
{
	FEATUREIO fioImg;
	fioImg.x = fioImg.z = fioImg.y = FEATURE_3D_DIM;
	fioImg.pfVectors = &(data_zyx[0][0][0]);
	fioImg.pfMeans = 0;
	fioImg.pfVarrs = 0;
	fioImg.iFeaturesPerVector = 1;
	fioImg.t = 1;


	// Derivative iamges
	FEATUREIO fioDx = fioImg;
	FEATUREIO fioDy = fioImg;
	FEATUREIO fioDz = fioImg;
	Feature3D fdx;
	Feature3D fdy;
	Feature3D fdz;
	fioDx.pfVectors = &(fdx.data_zyx[0][0][0]);
	fioDy.pfVectors = &(fdy.data_zyx[0][0][0]);
	fioDz.pfVectors = &(fdz.data_zyx[0][0][0]);

	// Angles and blurred versions
	FEATUREIO fioT0 = fioImg;
	FEATUREIO fioT1 = fioImg;
	FEATUREIO fioT2 = fioImg;
	Feature3D fT0;
	Feature3D fT1;
	Feature3D fT2;
	fioT0.pfVectors = &(fT0.data_zyx[0][0][0]);
	fioT1.pfVectors = &(fT1.data_zyx[0][0][0]);
	fioT2.pfVectors = &(fT2.data_zyx[0][0][0]);
	fioSet(fioT0, 0);

	// Flip x/z axes: test to see if peaks found are correctly flipped, yes works.
	//for( int zz = 0; zz < fioImg.z; zz++ )
	//{
	//	for( int yy = 0; yy < fioImg.y; yy++ )
	//	{
	//		for( int xx = 0; xx < fioImg.x; xx++ )
	//		{
	//			fT0.data_zyx[zz][yy][xx] = data_zyx[zz][yy][xx];
	//		}
	//	}
	//}
	//for( int zz = 0; zz < fioImg.z; zz++ )
	//{
	//	for( int yy = 0; yy < fioImg.y; yy++ )
	//	{
	//		for( int xx = 0; xx < fioImg.x; xx++ )
	//		{
	//			data_zyx[zz][yy][xx] = fT0.data_zyx[xx][yy][zz];
	//		}
	//	}
	//}
	//fioSet( fioT0, 0 );





	// Major peaks
	LOCATION_VALUE_XYZ_ARRAY lvaPeaks;
	LOCATION_VALUE_XYZ lvData[ORIBINS*ORIBINS / 8];
	lvaPeaks.plvz = &(lvData[0]);

	//fioGenerateEdgeImages3D( fioImg, fioDx, fioDy, fioDz );

	float fRadius = fioImg.x / 2;
	float fRadiusSqr = (fioImg.x / 2)*(fioImg.x / 2);

	int iSampleCount = 0;

	float fPixelCenter = fioGetPixel(fioImg, fioImg.x / 2, fioImg.y / 2, fioImg.z / 2);

	// Determine dominant orientation of feature: azimuth/elevation
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				float dz = zz - fioImg.z / 2;
				float dy = yy - fioImg.y / 2;
				float dx = xx - fioImg.x / 2;
				if (dz*dz + dy*dy + dx*dx < fRadiusSqr)
				{
					// Keep this sample
					float pfEdge[3];
					pfEdge[0] = dx;
					pfEdge[1] = dy;
					pfEdge[2] = dz;
					float fEdgeMagSqr = pfEdge[0] * pfEdge[0] + pfEdge[1] * pfEdge[1] + pfEdge[2] * pfEdge[2];
					if (fEdgeMagSqr == 0)
					{
						continue;
					}
					float fEdgeMag = sqrt(fEdgeMagSqr);

					float fPixel = fioGetPixel(fioImg, xx, yy, zz);
					float fPixelDiff = fPixelCenter - fPixel;
					float fPixelDiffSqr = fPixelDiff*fPixelDiff;
					float fPixelExp = exp(-fPixelDiffSqr / 0.00500f);
					//fPixelExp = 1.0f-fabs(fPixelDiff);

					iSampleCount++;

					// Vector length fRadius in direction of edge
					float pfEdgeUnit[3];
					for (int i = 0; i < 3; i++)
					{
						pfEdgeUnit[i] = pfEdge[i] * fRadius / fEdgeMag;
					}
					for (int i = 0; i < 3; i++)
					{
						pfEdgeUnit[i] += fRadius;
					}


					// Bilinear interpolation

					//int iZCoord;
					//float fZCont;
					//PpImage ppImgRef;
					//if( pfEdgeUnit[2] < 0.5f )
					//{
					//	iZCoord = 0;
					//	fZCont = 1.0f;
					//	ppImgRef.InitializeSubImage(
					//		fioT0.y, fioT0.x, fioT0.x*sizeof(float), sizeof(float)*8,
					//		(unsigned char*)fioGetVector( fioT0, 0, 0, iZCoord ) 
					//		);
					//	bilinear_interpolation_paint_float( ppImgRef, fEdgeMag,  pfEdgeUnit[1], pfEdgeUnit[0] );
					//}
					//else if( pfEdgeUnit[2] >= ((float)(fioT0.z - 0.5f)) )
					//{
					//	iZCoord = fioT0.z - 1;
					//	fZCont = 0.0f;
					//	ppImgRef.InitializeSubImage(
					//		fioT0.y, fioT0.x, fioT0.x*sizeof(float), sizeof(float)*8,
					//		(unsigned char*)fioGetVector( fioT0, 0, 0, iZCoord ) 
					//		);
					//	bilinear_interpolation_paint_float( ppImgRef, fEdgeMag, pfEdgeUnit[1], pfEdgeUnit[0] );
					//}
					//else
					//{
					//	float fMinusHalf = pfEdgeUnit[2] - 0.5f;
					//	iZCoord = (int)floor( fMinusHalf );
					//	fZCont = 1.0f - (fMinusHalf - ((float)iZCoord));

					//	ppImgRef.InitializeSubImage(
					//		fioT0.y, fioT0.x, fioT0.x*sizeof(float), sizeof(float)*8,
					//		(unsigned char*)fioGetVector( fioT0, 0, 0, iZCoord ) 
					//		);
					//	bilinear_interpolation_paint_float( ppImgRef, fZCont*fEdgeMag, pfEdgeUnit[1], pfEdgeUnit[0] );

					//	ppImgRef.InitializeSubImage(
					//		fioT0.y, fioT0.x, fioT0.x*sizeof(float), sizeof(float)*8,
					//		(unsigned char*)fioGetVector( fioT0, 0, 0, iZCoord+1 ) 
					//		);
					//	bilinear_interpolation_paint_float( ppImgRef, (1.0f-fZCont)*fEdgeMag, pfEdgeUnit[1], pfEdgeUnit[0] );
					//}

					//bilinear_interpolation_paint_float
					float *pfPix = fioGetVector(fioT0, pfEdgeUnit[0] + 0.5, pfEdgeUnit[1] + 0.5, pfEdgeUnit[2] + 0.5);

					// Add pixel
					*pfPix += fPixelExp;
				}
			}
		}
	}

	PpImage ppImgRef;
	char pcFileName[400];
	gb3d_blur3d(fioT0, fioT1, fioT2, 1.0f, 0.01);

	regFindFEATUREIOPeaks(lvaPeaks, fioT0);
	lvSortHighLow(lvaPeaks);
	sprintf(pcFileName, "bin%5.5d_orig", iCode);
	//fioWrite( fioT0, pcFileName );
	//fT0.OutputVisualFeature( pcFileName );

	sprintf(pcFileName, "bin%5.5d_blur", iCode);
	//fioWrite( fioT2, pcFileName );
	//fT2.OutputVisualFeature( pcFileName );

	float pfP1[3]; //z
	float pfP2[3]; //y
	float pfP3[3]; //x
	for (int iP1 = 0; iP1 < 1/*lvaPeaks.iCount*/; iP1++)
	{
		// Major direction unit vector
		// Should interpolate here for correctnewss
		pfP1[0] = lvaPeaks.plvz[iP1].x - fRadius;
		pfP1[1] = lvaPeaks.plvz[iP1].y - fRadius;
		pfP1[2] = lvaPeaks.plvz[iP1].z - fRadius;
		vec3D_norm_3d(pfP1);
		for (int iP2 = iP1 + 1; iP2 < 2/*lvaPeaks.iCount*/; iP2++)
		{
			// Secondary direction unit vector
			pfP2[0] = lvaPeaks.plvz[iP2].x - fRadius;
			pfP2[1] = lvaPeaks.plvz[iP2].y - fRadius;
			pfP2[2] = lvaPeaks.plvz[iP2].z - fRadius;
			vec3D_norm_3d(pfP2);

			// Cross product between major axis & 2ndary 
			vec3D_cross_3d(pfP1, pfP2, pfP3);
			vec3D_norm_3d(pfP3);

			// Re-cross to get P2 orthogonal to P1
			vec3D_cross_3d(pfP3, pfP1, pfP2);
			vec3D_norm_3d(pfP2);

			// Generate euler rotation

			float pfVx[3] = { 1, 0, 0 };
			float pfVy[3] = { 0, 1, 0 };
			float pfVz[3] = { 0, 0, 1 };

			//vec3D_euler_rotation( pfVx, pfVy, pfVz, pfP3, pfP2, pfP1, ori );
			determine_rotation_3point(pfP3, pfP2, pfP1, ori);
			//vec3D_euler_rotation( pfP3, pfP2, pfP1, ori );
		}
	}

	float ori_here_[3][3];
	memcpy(&(ori_here_[0][0]), ori, sizeof(float) * 3 * 3);

	// Compute inverse rotation matrix (same as transpose)
	// to resample original image in rotation normalized space
	float ori_here[3][3];
	transpose_3x3<float, float>(ori_here_, ori_here);

	// Rotate template
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				float pf1[3] = { xx - fRadius,yy - fRadius,zz - fRadius };
				float pf2[3];

				mult_3x3<float, float>(ori_here, pf1, pf2);

				// Add center offset
				pf2[0] += fRadius;
				pf2[1] += fRadius;
				pf2[2] += fRadius;

				float fPix = fioGetPixelTrilinearInterp(fioImg, pf2[0], pf2[1], pf2[2]);
				fT0.data_zyx[zz][yy][xx] = fPix;
			}
		}
	}

	fioSet(fioImg, 0);
	// Copy back into data
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				float dz = zz - fioImg.z / 2;
				float dy = yy - fioImg.y / 2;
				float dx = xx - fioImg.x / 2;
				//if( dz*dz + dy*dy + dx*dx <= fRadiusSqr )
				{
					data_zyx[zz][yy][xx] = fT0.data_zyx[zz][yy][xx];
				}
			}
		}
	}

	int iCode0 = (lvaPeaks.plvz[0].z*(fioImg.x*fioImg.x))
		+ (lvaPeaks.plvz[0].y*(fioImg.x))
		+ lvaPeaks.plvz[0].x;

	int iCode1 = (lvaPeaks.plvz[1].z*(fioImg.x*fioImg.x))
		+ (lvaPeaks.plvz[1].y*(fioImg.x))
		+ lvaPeaks.plvz[1].x;

	iCode = iCode0*iCode1;
}


void
Feature3DData::DetermineOrientationCube(
	int &iCode,
	float *ori
)
{
	FEATUREIO fioImg;
	fioImg.x = fioImg.z = fioImg.y = FEATURE_3D_DIM;
	fioImg.pfVectors = &(data_zyx[0][0][0]);
	fioImg.pfMeans = 0;
	fioImg.pfVarrs = 0;
	fioImg.iFeaturesPerVector = 1;
	fioImg.t = 1;


	// Derivative iamges
	FEATUREIO fioDx = fioImg;
	FEATUREIO fioDy = fioImg;
	FEATUREIO fioDz = fioImg;
	Feature3D fdx;
	Feature3D fdy;
	Feature3D fdz;
	fioDx.pfVectors = &(fdx.data_zyx[0][0][0]);
	fioDy.pfVectors = &(fdy.data_zyx[0][0][0]);
	fioDz.pfVectors = &(fdz.data_zyx[0][0][0]);

	// Angles and blurred versions
	FEATUREIO fioT0 = fioImg;
	FEATUREIO fioT1 = fioImg;
	FEATUREIO fioT2 = fioImg;
	Feature3D fT0;
	Feature3D fT1;
	Feature3D fT2;
	fioT0.pfVectors = &(fT0.data_zyx[0][0][0]);
	fioT1.pfVectors = &(fT1.data_zyx[0][0][0]);
	fioT2.pfVectors = &(fT2.data_zyx[0][0][0]);
	fioSet(fioT0, 0);

	// Flip x/z axes: test to see if peaks found are correctly flipped, yes works.
	//for( int zz = 0; zz < fioImg.z; zz++ )
	//{
	//	for( int yy = 0; yy < fioImg.y; yy++ )
	//	{
	//		for( int xx = 0; xx < fioImg.x; xx++ )
	//		{
	//			fT0.data_zyx[zz][yy][xx] = data_zyx[zz][yy][xx];
	//		}
	//	}
	//}
	//for( int zz = 0; zz < fioImg.z; zz++ )
	//{
	//	for( int yy = 0; yy < fioImg.y; yy++ )
	//	{
	//		for( int xx = 0; xx < fioImg.x; xx++ )
	//		{
	//			data_zyx[zz][yy][xx] = fT0.data_zyx[xx][yy][zz];
	//		}
	//	}
	//}
	//fioSet( fioT0, 0 );





	// Major peaks
	LOCATION_VALUE_XYZ_ARRAY lvaPeaks;
	LOCATION_VALUE_XYZ lvData[ORIBINS*ORIBINS / 8];
	lvaPeaks.plvz = &(lvData[0]);

	fioGenerateEdgeImages3D(fioImg, fioDx, fioDy, fioDz);

	float fRadius = fioImg.x / 2;
	float fRadiusSqr = (fioImg.x / 2)*(fioImg.x / 2);

	int iSampleCount = 0;

	// Determine dominant orientation of feature: azimuth/elevation
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				float dz = zz - fioImg.z / 2;
				float dy = yy - fioImg.y / 2;
				float dx = xx - fioImg.x / 2;
				if (dz*dz + dy*dy + dx*dx < fRadiusSqr)
				{
					// Keep this sample
					float pfEdge[3];
					pfEdge[0] = fioGetPixel(fioDx, xx, yy, zz);
					pfEdge[1] = fioGetPixel(fioDy, xx, yy, zz);
					pfEdge[2] = fioGetPixel(fioDz, xx, yy, zz);
					float fEdgeMagSqr = pfEdge[0] * pfEdge[0] + pfEdge[1] * pfEdge[1] + pfEdge[2] * pfEdge[2];
					if (fEdgeMagSqr == 0)
					{
						continue;
					}
					float fEdgeMag = sqrt(fEdgeMagSqr);

					iSampleCount++;

					// Vector length fRadius in direction of edge
					float pfEdgeUnit[3];
					for (int i = 0; i < 3; i++)
					{
						pfEdgeUnit[i] = pfEdge[i] * fRadius / fEdgeMag;
					}
					for (int i = 0; i < 3; i++)
					{
						pfEdgeUnit[i] += fRadius;
					}


					// Bilinear interpolation

					//int iZCoord;
					//float fZCont;
					//PpImage ppImgRef;
					//if( pfEdgeUnit[2] < 0.5f )
					//{
					//	iZCoord = 0;
					//	fZCont = 1.0f;
					//	ppImgRef.InitializeSubImage(
					//		fioT0.y, fioT0.x, fioT0.x*sizeof(float), sizeof(float)*8,
					//		(unsigned char*)fioGetVector( fioT0, 0, 0, iZCoord ) 
					//		);
					//	bilinear_interpolation_paint_float( ppImgRef, fEdgeMag,  pfEdgeUnit[1], pfEdgeUnit[0] );
					//}
					//else if( pfEdgeUnit[2] >= ((float)(fioT0.z - 0.5f)) )
					//{
					//	iZCoord = fioT0.z - 1;
					//	fZCont = 0.0f;
					//	ppImgRef.InitializeSubImage(
					//		fioT0.y, fioT0.x, fioT0.x*sizeof(float), sizeof(float)*8,
					//		(unsigned char*)fioGetVector( fioT0, 0, 0, iZCoord ) 
					//		);
					//	bilinear_interpolation_paint_float( ppImgRef, fEdgeMag, pfEdgeUnit[1], pfEdgeUnit[0] );
					//}
					//else
					//{
					//	float fMinusHalf = pfEdgeUnit[2] - 0.5f;
					//	iZCoord = (int)floor( fMinusHalf );
					//	fZCont = 1.0f - (fMinusHalf - ((float)iZCoord));

					//	ppImgRef.InitializeSubImage(
					//		fioT0.y, fioT0.x, fioT0.x*sizeof(float), sizeof(float)*8,
					//		(unsigned char*)fioGetVector( fioT0, 0, 0, iZCoord ) 
					//		);
					//	bilinear_interpolation_paint_float( ppImgRef, fZCont*fEdgeMag, pfEdgeUnit[1], pfEdgeUnit[0] );

					//	ppImgRef.InitializeSubImage(
					//		fioT0.y, fioT0.x, fioT0.x*sizeof(float), sizeof(float)*8,
					//		(unsigned char*)fioGetVector( fioT0, 0, 0, iZCoord+1 ) 
					//		);
					//	bilinear_interpolation_paint_float( ppImgRef, (1.0f-fZCont)*fEdgeMag, pfEdgeUnit[1], pfEdgeUnit[0] );
					//}

					//bilinear_interpolation_paint_float
					float *pfPix = fioGetVector(fioT0, pfEdgeUnit[0] + 0.5, pfEdgeUnit[1] + 0.5, pfEdgeUnit[2] + 0.5);

					// Add pixel
					*pfPix += fEdgeMag;
				}
			}
		}
	}

	PpImage ppImgRef;
	char pcFileName[400];
	gb3d_blur3d(fioT0, fioT1, fioT2, 1.0f, 0.01);

	regFindFEATUREIOPeaks(lvaPeaks, fioT0);
	lvSortHighLow(lvaPeaks);
	sprintf(pcFileName, "bin%5.5d_orig", iCode);
	//fioWrite( fioT0, pcFileName );
	//fT0.OutputVisualFeature( pcFileName );

	sprintf(pcFileName, "bin%5.5d_blur", iCode);
	//fioWrite( fioT2, pcFileName );
	//fT2.OutputVisualFeature( pcFileName );

	float pfP1[3]; //z
	float pfP2[3]; //y
	float pfP3[3]; //x
	for (int iP1 = 0; iP1 < 1/*lvaPeaks.iCount*/; iP1++)
	{
		// Major direction unit vector
		// Should interpolate here for correctnewss
		pfP1[0] = lvaPeaks.plvz[iP1].x - fRadius;
		pfP1[1] = lvaPeaks.plvz[iP1].y - fRadius;
		pfP1[2] = lvaPeaks.plvz[iP1].z - fRadius;
		vec3D_norm_3d(pfP1);
		for (int iP2 = iP1 + 1; iP2 < 2/*lvaPeaks.iCount*/; iP2++)
		{
			// Secondary direction unit vector
			pfP2[0] = lvaPeaks.plvz[iP2].x - fRadius;
			pfP2[1] = lvaPeaks.plvz[iP2].y - fRadius;
			pfP2[2] = lvaPeaks.plvz[iP2].z - fRadius;
			vec3D_norm_3d(pfP2);

			// Cross product between major axis & 2ndary 
			vec3D_cross_3d(pfP1, pfP2, pfP3);
			vec3D_norm_3d(pfP3);

			// Re-cross to get P2 orthogonal to P1
			vec3D_cross_3d(pfP3, pfP1, pfP2);
			vec3D_norm_3d(pfP2);

			// Generate euler rotation

			float pfVx[3] = { 1, 0, 0 };
			float pfVy[3] = { 0, 1, 0 };
			float pfVz[3] = { 0, 0, 1 };

			//vec3D_euler_rotation( pfVx, pfVy, pfVz, pfP3, pfP2, pfP1, ori );
			determine_rotation_3point(pfP3, pfP2, pfP1, ori);
			//vec3D_euler_rotation( pfP3, pfP2, pfP1, ori );
		}
	}

	float ori_here_[3][3];
	memcpy(&(ori_here_[0][0]), ori, sizeof(float) * 3 * 3);

	// Compute inverse rotation matrix (same as transpose)
	// to resample original image in rotation normalized space
	float ori_here[3][3];
	transpose_3x3<float, float>(ori_here_, ori_here);

	// Rotate template
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				float pf1[3] = { xx - fRadius,yy - fRadius,zz - fRadius };
				float pf2[3];

				mult_3x3<float, float>(ori_here, pf1, pf2);

				// Add center offset
				pf2[0] += fRadius;
				pf2[1] += fRadius;
				pf2[2] += fRadius;

				float fPix = fioGetPixelTrilinearInterp(fioImg, pf2[0], pf2[1], pf2[2]);
				fT0.data_zyx[zz][yy][xx] = fPix;
			}
		}
	}

	fioSet(fioImg, 0);
	// Copy back into data
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				float dz = zz - fioImg.z / 2;
				float dy = yy - fioImg.y / 2;
				float dx = xx - fioImg.x / 2;
				//if( dz*dz + dy*dy + dx*dx <= fRadiusSqr )
				{
					data_zyx[zz][yy][xx] = fT0.data_zyx[zz][yy][xx];
				}
			}
		}
	}

	int iCode0 = (lvaPeaks.plvz[0].z*(fioImg.x*fioImg.x))
		+ (lvaPeaks.plvz[0].y*(fioImg.x))
		+ lvaPeaks.plvz[0].x;

	int iCode1 = (lvaPeaks.plvz[1].z*(fioImg.x*fioImg.x))
		+ (lvaPeaks.plvz[1].y*(fioImg.x))
		+ lvaPeaks.plvz[1].x;

	iCode = iCode0*iCode1;
}



void
Feature3DData::DetermineOrientationGenerateAzimuthElevationTable(
	int iCode
)
{
	FEATUREIO fioImg;
	fioImg.x = fioImg.z = fioImg.y = FEATURE_3D_DIM;
	fioImg.pfVectors = &(data_zyx[0][0][0]);
	fioImg.pfMeans = 0;
	fioImg.pfVarrs = 0;
	fioImg.iFeaturesPerVector = 1;
	fioImg.t = 1;

	FEATUREIO fioDx = fioImg;
	FEATUREIO fioDy = fioImg;
	FEATUREIO fioDz = fioImg;

	Feature3D f11;
	Feature3D fdx;
	Feature3D fdy;
	Feature3D fdz;

	//fioAllocate( fioDx );
	//fioAllocate( fioDy );
	//fioAllocate( fioDz );

	fioDx.pfVectors = &(fdx.data_zyx[0][0][0]);
	fioDy.pfVectors = &(fdy.data_zyx[0][0][0]);
	fioDz.pfVectors = &(fdz.data_zyx[0][0][0]);


	fioGenerateEdgeImages3D(fioImg, fioDx, fioDy, fioDz);

	//fdx.OutputVisualFeature( "feat_dx" );
	//fdy.OutputVisualFeature( "feat_dy" );
	//fdz.OutputVisualFeature( "feat_dz" );
	//fioWrite( fioDx, "dx" );
	//fioWrite( fioDy, "dy" );
	//fioWrite( fioDz, "dz" );

	float pfHistAzEl[ORIBINS][ORIBINS];
	float pfHistAzElEdge[ORIBINS + 2][ORIBINS + 2];
	float pfHistAzElBlur[3 * ORIBINS][3 * ORIBINS];
	float pfHistAzElBlur1[3 * ORIBINS][3 * ORIBINS];
	float pfHistAzElBlur2[3 * ORIBINS][3 * ORIBINS];
	memset(pfHistAzEl, 0, sizeof(pfHistAzEl));
	memset(pfHistAzElBlur, 0, sizeof(pfHistAzElBlur));

	FEATUREIO fioHist;
	fioHist.iFeaturesPerVector = 1;
	fioHist.pfMeans = fioHist.pfVarrs = 0;
	fioHist.pfVectors = &(pfHistAzEl[0][0]);
	fioHist.t = fioHist.z = 1;
	fioHist.x = fioHist.y = ORIBINS;

	FEATUREIO fioHistEdge;
	fioHistEdge.iFeaturesPerVector = 1;
	fioHistEdge.pfMeans = fioHistEdge.pfVarrs = 0;
	fioHistEdge.pfVectors = &(pfHistAzElEdge[0][0]);
	fioHistEdge.t = fioHistEdge.z = 1;
	fioHistEdge.x = fioHistEdge.y = ORIBINS + 2;

	FEATUREIO fioHistBlur;
	fioHistBlur.iFeaturesPerVector = 1;
	fioHistBlur.pfMeans = fioHistBlur.pfVarrs = 0;
	fioHistBlur.pfVectors = &(pfHistAzElBlur[0][0]);
	fioHistBlur.t = fioHistBlur.z = 1;
	fioHistBlur.x = fioHistBlur.y = 3 * ORIBINS;

	FEATUREIO fioHistBlur1 = fioHistBlur;
	FEATUREIO fioHistBlur2 = fioHistBlur;
	fioHistBlur1.pfVectors = &(pfHistAzElBlur1[0][0]);
	fioHistBlur2.pfVectors = &(pfHistAzElBlur2[0][0]);

	LOCATION_VALUE_XYZ_ARRAY lvaPeaks;
	LOCATION_VALUE_XYZ lvData[ORIBINS*ORIBINS / 8];
	lvaPeaks.plvz = &(lvData[0]);

	float fRadiusSqr = (fioImg.x / 2)*(fioImg.x / 2);

	// Determine dominant orientation of feature: azimuth/elevation
	for (int zz = 0; zz < 10000000; zz++)
	{
		// Uniformly sample...
		float pfEdge[3];
		pfEdge[0] = (rand() / (RAND_MAX + 1.0) - 0.5);
		pfEdge[1] = (rand() / (RAND_MAX + 1.0) - 0.5);
		pfEdge[2] = (rand() / (RAND_MAX + 1.0) - 0.5);
		if (pfEdge[0] == 0 || pfEdge[1] == 0 || pfEdge[2] == 0)
		{
			continue;
		}

		// Determine Azimuth
		float fThetaAz = atan2(pfEdge[1], pfEdge[0]);
		int iBinAz = ((fThetaAz + PI) / (2.0*PI))*ORIBINS;
		if (iBinAz == ORIBINS)
		{
			iBinAz = ORIBINS - 1;
		}
		if (iBinAz < 0)
		{
			iBinAz = 0;
		}

		// Determine Elevation, ranges from -PI/2 to PI/2
		float fMagXY = sqrt(pfEdge[0] * pfEdge[0] + pfEdge[1] * pfEdge[1]);
		float fThetaEl = atan2(pfEdge[2], fMagXY);
		int iBinEl = ((fThetaEl + (PI / 2.0)) / ((float)PI))*ORIBINS;
		if (iBinEl == ORIBINS)
		{
			iBinEl = ORIBINS - 1;
		}
		if (iBinEl < 0)
		{
			iBinEl = 0;
		}

		// Ad vote
		float fMag = sqrt(pfEdge[0] * pfEdge[0] + pfEdge[1] * pfEdge[1] + pfEdge[2] * pfEdge[2]);
		pfHistAzEl[iBinEl][iBinAz] += fMag;

		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				pfHistAzElBlur[ORIBINS*i + iBinEl][ORIBINS*j + iBinAz] += fMag;///fAngleArea;
			}
		}

	}

	FILE *outfile = fopen("table_azimuth_elevation.txt", "wt");
	for (int i = 0; i < ORIBINS; i++)
	{
		for (int j = 0; j < ORIBINS; j++)
		{
			fprintf(outfile, "%f,\t", pfHistAzEl[i][j]);
		}
		fprintf(outfile, "\n");
	}
	fclose(outfile);

	PpImage ppImgRef;
	char pcFileName[400];
	gb3d_blur3d(fioHistBlur, fioHistBlur1, fioHistBlur2, 2.0f, 0.01);

	regFindFEATUREIOPeaks(lvaPeaks, fioHist);
	lvSortHighLow(lvaPeaks);

	// Find max
	int iMaxBinEl, iMaxBinAz, iZ;
	fioFindMax(fioHist, iMaxBinAz, iMaxBinEl, iZ);

	fioimgInitReference(ppImgRef, fioHist);
	sprintf(pcFileName, "hist_%6.6d.pgm", iCode);
	//output_float( ppImgRef, pcFileName );

	fioimgInitReference(ppImgRef, fioHistBlur2);
	sprintf(pcFileName, "hist_blur_%6.6d.pgm", iCode);
	//output_float( ppImgRef, pcFileName );
	fioimgInitReference(ppImgRef, fioHistBlur);
	sprintf(pcFileName, "hist_blur_%6.6d.pgm", iCode);
	//output_float( ppImgRef, pcFileName );

	for (int i = 0; i < ORIBINS; i++)
	{
		for (int j = 0; j < ORIBINS; j++)
		{
			pfHistAzEl[i][j] =
				pfHistAzElBlur2[ORIBINS + i][ORIBINS + j];
		}
	}
	fioimgInitReference(ppImgRef, fioHist);
	//sprintf( pcFileName, "hist_%6.6d_blur.pgm", iCode );
	//output_float( ppImgRef, pcFileName );

	for (int i = 0; i < ORIBINS + 2; i++)
	{
		for (int j = 0; j < ORIBINS + 2; j++)
		{
			pfHistAzElEdge[i][j] =
				pfHistAzElBlur2[ORIBINS + i - 1][ORIBINS + j - 1];
		}
	}
	fioimgInitReference(ppImgRef, fioHistEdge);
	sprintf(pcFileName, "hist_%6.6d_blurshift.pgm", iCode);
	//output_float( ppImgRef, pcFileName );

	regFindFEATUREIOPeaks(lvaPeaks, fioHistEdge);
	lvSortHighLow(lvaPeaks);
	for (int i = 0; i < lvaPeaks.iCount; i++)
	{
		// Remove shift
		lvaPeaks.plvz[i].x -= 1;
		lvaPeaks.plvz[i].y -= 1;
	}

	// Now find secondary tilt angles
	for (int i = 0; i < lvaPeaks.iCount; i++)
	{

	}

	//fioDelete( fioDx );
	//fioDelete( fioDy );
	//fioDelete( fioDz );
}

void
Feature3DData::DetermineOrientationInvDesc(
	int iCode
)
{
	FEATUREIO fioImg;
	fioImg.x = fioImg.z = fioImg.y = FEATURE_3D_DIM;
	fioImg.pfVectors = &(data_zyx[0][0][0]);
	fioImg.pfMeans = 0;
	fioImg.pfVarrs = 0;
	fioImg.iFeaturesPerVector = 1;
	fioImg.t = 1;

	FEATUREIO fioDx = fioImg;
	FEATUREIO fioDy = fioImg;
	FEATUREIO fioDz = fioImg;

	Feature3D f11;

	Feature3D fdx;
	Feature3D fdy;
	Feature3D fdz;

	//fioAllocate( fioDx );
	//fioAllocate( fioDy );
	//fioAllocate( fioDz );

	fioDx.pfVectors = &(fdx.data_zyx[0][0][0]);
	fioDy.pfVectors = &(fdy.data_zyx[0][0][0]);
	fioDz.pfVectors = &(fdz.data_zyx[0][0][0]);


	fioGenerateEdgeImages3D(fioImg, fioDx, fioDy, fioDz);

	//fdx.OutputVisualFeature( "feat_dx" );
	//fdy.OutputVisualFeature( "feat_dy" );
	//fdz.OutputVisualFeature( "feat_dz" );
	//fioWrite( fioDx, "dx" );
	//fioWrite( fioDy, "dy" );
	//fioWrite( fioDz, "dz" );

	float pfHistAzEl[ORIBINS][ORIBINS];
	float pfHistAzElEdge[ORIBINS + 2][ORIBINS + 2];
	float pfHistAzElBlur[3 * ORIBINS][3 * ORIBINS];
	float pfHistAzElBlur1[3 * ORIBINS][3 * ORIBINS];
	float pfHistAzElBlur2[3 * ORIBINS][3 * ORIBINS];
	memset(pfHistAzEl, 0, sizeof(pfHistAzEl));
	memset(pfHistAzElBlur, 0, sizeof(pfHistAzElBlur));

	FEATUREIO fioHist;
	fioHist.iFeaturesPerVector = 1;
	fioHist.pfMeans = fioHist.pfVarrs = 0;
	fioHist.pfVectors = &(pfHistAzEl[0][0]);
	fioHist.t = fioHist.z = 1;
	fioHist.x = fioHist.y = ORIBINS;

	FEATUREIO fioHistEdge;
	fioHistEdge.iFeaturesPerVector = 1;
	fioHistEdge.pfMeans = fioHistEdge.pfVarrs = 0;
	fioHistEdge.pfVectors = &(pfHistAzElEdge[0][0]);
	fioHistEdge.t = fioHistEdge.z = 1;
	fioHistEdge.x = fioHistEdge.y = ORIBINS + 2;

	FEATUREIO fioHistBlur;
	fioHistBlur.iFeaturesPerVector = 1;
	fioHistBlur.pfMeans = fioHistBlur.pfVarrs = 0;
	fioHistBlur.pfVectors = &(pfHistAzElBlur[0][0]);
	fioHistBlur.t = fioHistBlur.z = 1;
	fioHistBlur.x = fioHistBlur.y = 3 * ORIBINS;

	FEATUREIO fioHistBlur1 = fioHistBlur;
	FEATUREIO fioHistBlur2 = fioHistBlur;
	fioHistBlur1.pfVectors = &(pfHistAzElBlur1[0][0]);
	fioHistBlur2.pfVectors = &(pfHistAzElBlur2[0][0]);

	LOCATION_VALUE_XYZ_ARRAY lvaPeaks;
	LOCATION_VALUE_XYZ lvData[ORIBINS*ORIBINS / 8];
	lvaPeaks.plvz = &(lvData[0]);

	float fRadiusSqr = (fioImg.x / 2)*(fioImg.x / 2);

	int iSampleCount = 0;

	// Determine dominant orientation of feature: azimuth/elevation
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				float dz = zz - fioImg.z / 2;
				float dy = yy - fioImg.y / 2;
				float dx = xx - fioImg.x / 2;
				if (dz*dz + dy*dy + dx*dx < fRadiusSqr)
				{
					// Keep this sample
					float pfEdge[3];
					pfEdge[0] = fioGetPixel(fioDx, xx, yy, zz);
					pfEdge[1] = fioGetPixel(fioDy, xx, yy, zz);
					pfEdge[2] = fioGetPixel(fioDz, xx, yy, zz);
					float fEdgeMagSqr = pfEdge[0] * pfEdge[0] + pfEdge[1] * pfEdge[1] + pfEdge[2] * pfEdge[2];
					if (fEdgeMagSqr == 0)
					{
						continue;
					}
					float fEdgeMag = sqrt(fEdgeMagSqr);

					float pfRadial[3];
					pfRadial[0] = dx;
					pfRadial[1] = dy;
					pfRadial[2] = dz;
					float fRadialMagSqr = dz*dz + dy*dy + dx*dx;
					if (fRadialMagSqr == 0)
					{
						continue;
					}
					float fRadialMag = sqrt(fRadialMagSqr);
					iSampleCount++;

					float pfRadialUnit[3];
					for (int i = 0; i < 3; i++)
					{
						pfRadialUnit[i] = pfRadial[i] / fRadialMag;
					}

					// Projection along radial line
					float fProjectEdgeRadialUnit = 0;
					for (int i = 0; i < 3; i++)
					{
						fProjectEdgeRadialUnit += pfRadialUnit[i] * pfEdge[i];
					}

					float fThetaAz = acos(fProjectEdgeRadialUnit / fEdgeMag);
					int iBinAz = (fThetaAz / ((float)PI))*ORIBINS;
					if (iBinAz == ORIBINS)
					{
						iBinAz = ORIBINS - 1;
					}
					if (iBinAz < 0)
					{
						iBinAz = 0;
					}

					float pfProjEdgeMagSqr = 0;
					float pfProjEdge[3];
					for (int i = 0; i < 3; i++)
					{
						pfProjEdge[i] = fProjectEdgeRadialUnit*pfRadialUnit[i];
						pfProjEdgeMagSqr += pfProjEdge[i] * pfProjEdge[i];
					}
					float pfProjEdgeMag = sqrt(pfProjEdgeMagSqr);

					float pfPerpEdgeMagSqr = 0;
					float pfPerpEdge[3];
					for (int i = 0; i < 3; i++)
					{
						pfPerpEdge[i] = pfEdge[i] - fProjectEdgeRadialUnit*pfRadialUnit[i];
						pfPerpEdgeMagSqr += pfPerpEdge[i] * pfPerpEdge[i];
					}
					float pfPerpEdgeMag = sqrt(pfPerpEdgeMagSqr);


					// Ad vote
					//pfHistAzEl[0][iBinAz] += fEdgeMag;
					pfHistAzEl[0][iBinAz] += pfProjEdgeMag;
					pfHistAzEl[1][iBinAz] += pfPerpEdgeMag;

					for (int i = 0; i < 3; i++)
					{
						for (int j = 0; j < 3; j++)
						{
							pfHistAzElBlur[ORIBINS*i + 0][ORIBINS*j + iBinAz] += fEdgeMag;
							pfHistAzElBlur[ORIBINS*i + 1][ORIBINS*j + iBinAz] += pfPerpEdgeMag;
						}
					}

				}
			}
		}
	}

	PpImage ppImgRef;
	char pcFileName[400];
	gb3d_blur3d(fioHistBlur, fioHistBlur1, fioHistBlur2, 2.0f, 0.01);

	regFindFEATUREIOPeaks(lvaPeaks, fioHist);
	lvSortHighLow(lvaPeaks);

	// Find max
	int iMaxBinEl, iMaxBinAz, iZ;
	fioFindMax(fioHist, iMaxBinAz, iMaxBinEl, iZ);

	fioimgInitReference(ppImgRef, fioHist);
	sprintf(pcFileName, "hist_%6.6d_nowrap.pgm", iCode);
	output_float(ppImgRef, pcFileName);

	fioimgInitReference(ppImgRef, fioHistBlur2);
	sprintf(pcFileName, "hist_%6.6d_blur.pgm", iCode);
	//output_float( ppImgRef, pcFileName );
	fioimgInitReference(ppImgRef, fioHistBlur);
	//sprintf( pcFileName, "hist_%6.6d_original.pgm", iCode );
	//output_float( ppImgRef, pcFileName );

	for (int i = 0; i < ORIBINS; i++)
	{
		for (int j = 0; j < ORIBINS; j++)
		{
			pfHistAzEl[i][j] =
				pfHistAzElBlur2[ORIBINS + i][ORIBINS + j];
		}
	}
	fioimgInitReference(ppImgRef, fioHist);
	sprintf(pcFileName, "hist_%6.6d_blur.pgm", iCode);
	//output_float( ppImgRef, pcFileName );

	for (int i = 0; i < ORIBINS + 2; i++)
	{
		for (int j = 0; j < ORIBINS + 2; j++)
		{
			pfHistAzElEdge[i][j] =
				pfHistAzElBlur2[ORIBINS + i - 1][ORIBINS + j - 1];
		}
	}
	fioimgInitReference(ppImgRef, fioHistEdge);
	sprintf(pcFileName, "hist_%6.6d_blurshift.pgm", iCode);
	output_float(ppImgRef, pcFileName);

	regFindFEATUREIOPeaks(lvaPeaks, fioHistEdge);
	lvSortHighLow(lvaPeaks);
	for (int i = 0; i < lvaPeaks.iCount; i++)
	{
		// Remove shift
		lvaPeaks.plvz[i].x -= 1;
		lvaPeaks.plvz[i].y -= 1;
	}

	// Now find secondary tilt angles
	for (int i = 0; i < lvaPeaks.iCount; i++)
	{

	}

	//fioDelete( fioDx );
	//fioDelete( fioDy );
	//fioDelete( fioDz );
}


//
// determineCanonicalOrientation3DAzimuthElevation()
//
// Assign orientation according to method of Scovanner, Allaire
//
int
determineCanonicalOrientation3DAzimuthElevation(
	Feature3D &feat3D,
	float *pfOri, // Array to store iMaxOri 3D rotation arrays. Each rotation array is
				  //			encoded as three 3D unit vectors
	int iMaxOri // Number of 6-float values available in pfOri
)
{
	FEATUREIO fioImg;
	fioImg.x = fioImg.z = fioImg.y = Feature3D::FEATURE_3D_DIM;
	fioImg.pfVectors = &(feat3D.data_zyx[0][0][0]);
	fioImg.pfMeans = 0;
	fioImg.pfVarrs = 0;
	fioImg.iFeaturesPerVector = 1;
	fioImg.t = 1;

	FEATUREIO fioDx = fioImg;
	FEATUREIO fioDy = fioImg;
	FEATUREIO fioDz = fioImg;

	Feature3D f11;


	Feature3D fdx;
	Feature3D fdy;
	Feature3D fdz;

	//fioAllocate( fioDx );
	//fioAllocate( fioDy );
	//fioAllocate( fioDz );

	fioDx.pfVectors = &(fdx.data_zyx[0][0][0]);
	fioDy.pfVectors = &(fdy.data_zyx[0][0][0]);
	fioDz.pfVectors = &(fdz.data_zyx[0][0][0]);

	//fdx.OutputVisualFeature( "feat_dx" );
	//fdy.OutputVisualFeature( "feat_dy" );
	//fdz.OutputVisualFeature( "feat_dz" );
	//fioWrite( fioDx, "dx" );
	//fioWrite( fioDy, "dy" );
	//fioWrite( fioDz, "dz" );

	float pfHistAzEl[ORIBINS][ORIBINS];
	float pfHistAzElEdge[ORIBINS + 2][ORIBINS + 2];
	float pfHistAzElBlur[3 * ORIBINS][3 * ORIBINS];
	float pfHistAzElBlur1[3 * ORIBINS][3 * ORIBINS];
	float pfHistAzElBlur2[3 * ORIBINS][3 * ORIBINS];
	memset(pfHistAzEl, 0, sizeof(pfHistAzEl));
	memset(pfHistAzElBlur, 0, sizeof(pfHistAzElBlur));

	FEATUREIO fioHist;
	fioHist.iFeaturesPerVector = 1;
	fioHist.pfMeans = fioHist.pfVarrs = 0;
	fioHist.pfVectors = &(pfHistAzEl[0][0]);
	fioHist.t = fioHist.z = 1;
	fioHist.x = fioHist.y = ORIBINS;

	FEATUREIO fioHistEdge;
	fioHistEdge.iFeaturesPerVector = 1;
	fioHistEdge.pfMeans = fioHistEdge.pfVarrs = 0;
	fioHistEdge.pfVectors = &(pfHistAzElEdge[0][0]);
	fioHistEdge.t = fioHistEdge.z = 1;
	fioHistEdge.x = fioHistEdge.y = ORIBINS + 2;

	FEATUREIO fioHistBlur;
	fioHistBlur.iFeaturesPerVector = 1;
	fioHistBlur.pfMeans = fioHistBlur.pfVarrs = 0;
	fioHistBlur.pfVectors = &(pfHistAzElBlur[0][0]);
	fioHistBlur.t = fioHistBlur.z = 1;
	fioHistBlur.x = fioHistBlur.y = 3 * ORIBINS;

	FEATUREIO fioHistBlur1 = fioHistBlur;
	FEATUREIO fioHistBlur2 = fioHistBlur;
	fioHistBlur1.pfVectors = &(pfHistAzElBlur1[0][0]);
	fioHistBlur2.pfVectors = &(pfHistAzElBlur2[0][0]);

	// Angles and blurred versions
	FEATUREIO fioT0 = fioImg;
	FEATUREIO fioT1 = fioImg;
	FEATUREIO fioT2 = fioImg;
	Feature3D fT0;
	Feature3D fT1;
	Feature3D fT2;
	fioT0.pfVectors = &(fT0.data_zyx[0][0][0]);
	fioT1.pfVectors = &(fT1.data_zyx[0][0][0]);
	fioT2.pfVectors = &(fT2.data_zyx[0][0][0]);
	fioSet(fioT0, 0);

	// Major peaks
	LOCATION_VALUE_XYZ_ARRAY lvaPeaks;
	LOCATION_VALUE_XYZ lvData[ORIBINS*ORIBINS / 8];
	float pfOriData[ORIBINS*ORIBINS / 8];
	lvaPeaks.plvz = &(lvData[0]);

	fioGenerateEdgeImages3D(fioImg, fioDx, fioDy, fioDz);

	float fRadius = fioImg.x / 2;
	float fRadiusSqr = (fioImg.x / 2)*(fioImg.x / 2);

	// Determine dominant orientation of feature: azimuth/elevation
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				float dz = zz - fioImg.z / 2;
				float dy = yy - fioImg.y / 2;
				float dx = xx - fioImg.x / 2;
				if (dz*dz + dy*dy + dx*dx < fRadiusSqr)
				{
					// Keep this sample
					float pfEdge[3];
					pfEdge[0] = fioGetPixel(fioDx, xx, yy, zz);
					pfEdge[1] = fioGetPixel(fioDy, xx, yy, zz);
					pfEdge[2] = fioGetPixel(fioDz, xx, yy, zz);
					float fMagSqr = pfEdge[0] * pfEdge[0] + pfEdge[1] * pfEdge[1] + pfEdge[2] * pfEdge[2];
					if (fMagSqr == 0)
					{
						continue;
					}

					float fMag = sqrt(pfEdge[0] * pfEdge[0] + pfEdge[1] * pfEdge[1] + pfEdge[2] * pfEdge[2]);

					// Determine Azimuth
					float fThetaAz = atan2(pfEdge[1], pfEdge[0]);
					int iBinAz = ((fThetaAz + PI) / (2.0*PI))*ORIBINS;
					if (iBinAz == ORIBINS)
					{
						iBinAz = ORIBINS - 1;
					}
					if (iBinAz < 0)
					{
						iBinAz = 0;
					}

					// Determine Elevation, ranges from -PI/2 to PI/2
					float fMagXY = sqrt(pfEdge[0] * pfEdge[0] + pfEdge[1] * pfEdge[1]);
					float fThetaEl = atan2(pfEdge[2], fMagXY);
					int iBinEl = ((fThetaEl + (PI / 2.0)) / ((float)PI))*ORIBINS;
					if (iBinEl == ORIBINS)
					{
						iBinEl = ORIBINS - 1;
					}
					if (iBinEl < 0)
					{
						iBinEl = 0;
					}


					// Normalize vote for tilt angle
					//float fUnitX = pfEdge[0]/fMag;
					//float fUnitY = pfEdge[1]/fMag;
					//float fUnitXYMag = sqrt(fUnitX*fUnitX+fUnitY*fUnitY);
					float fIncAz = ((2.0*((float)PI)) / ((float)ORIBINS));
					float fIncEl = (((float)PI) / ((float)ORIBINS));
					float fAngleArea = 0;
					float fElBinStart = (iBinEl - (ORIBINS / 2))*fIncEl;
					fAngleArea = fIncAz*(cos(fElBinStart) + cos(fElBinStart + fIncEl)) / 2.0f;
					if (fAngleArea < 0)
						fAngleArea *= -1;
					assert(fElBinStart < fThetaEl && fThetaEl < fElBinStart + fIncEl);
					assert(fAngleArea > 0);

					// Ad vote
					pfHistAzEl[iBinEl][iBinAz] += fMag / fAngleArea;///g_pfAzElNorm[iBinEl][iBinAz];


					for (int i = 0; i < 3; i++)
					{
						for (int j = 0; j < 3; j++)
						{
							pfHistAzElBlur[ORIBINS*i + iBinEl][ORIBINS*j + iBinAz] += fMag / fAngleArea;///g_pfAzElNorm[iBinEl][iBinAz];///fAngleArea;
						}
					}

				}
			}
		}
	}

	PpImage ppImgRef;
	char pcFileName[400];
	gb3d_blur3d(fioHistBlur, fioHistBlur1, fioHistBlur2, 2.0f, 0.01);

	regFindFEATUREIOPeaks(lvaPeaks, fioHist);
	lvSortHighLow(lvaPeaks);

	// Find max
	int iMaxBinEl, iMaxBinAz, iZ;
	fioFindMax(fioHist, iMaxBinAz, iMaxBinEl, iZ);

	int iCode = 0;

	fioimgInitReference(ppImgRef, fioHist);
	sprintf(pcFileName, "hist_%6.6d_nowrap.pgm", iCode);
	output_float(ppImgRef, pcFileName);

	fioimgInitReference(ppImgRef, fioHistBlur2);
	sprintf(pcFileName, "hist_%6.6d_blur.pgm", iCode);
	//output_float( ppImgRef, pcFileName );
	fioimgInitReference(ppImgRef, fioHistBlur);
	//sprintf( pcFileName, "hist_%6.6d_original.pgm", iCode );
	output_float(ppImgRef, pcFileName);

	for (int i = 0; i < ORIBINS; i++)
	{
		for (int j = 0; j < ORIBINS; j++)
		{
			pfHistAzEl[i][j] =
				pfHistAzElBlur2[ORIBINS + i][ORIBINS + j];
		}
	}
	fioimgInitReference(ppImgRef, fioHist);
	sprintf(pcFileName, "hist_%6.6d_blur.pgm", iCode);
	output_float(ppImgRef, pcFileName);

	for (int i = 0; i < ORIBINS + 2; i++)
	{
		for (int j = 0; j < ORIBINS + 2; j++)
		{
			pfHistAzElEdge[i][j] =
				pfHistAzElBlur2[ORIBINS + i - 1][ORIBINS + j - 1];
		}
	}
	fioimgInitReference(ppImgRef, fioHistEdge);
	sprintf(pcFileName, "hist_%6.6d_blurshift.pgm", iCode);
	output_float(ppImgRef, pcFileName);

	regFindFEATUREIOPeaks(lvaPeaks, fioHistEdge);
	lvSortHighLow(lvaPeaks);





	//
	// The code here is copied from function below:
	//
	// determineCanonicalOrientation3D(
	//

	float pfP1[3]; //z
	float pfP2[3]; //y
	float pfP3[3]; //x

				   // determine sub-pixel orientation vectors
	for (int i = 0; i < lvaPeaks.iCount && i < fioImg.z && i < iMaxOri; i++)
	{
		float *pfOriCurr = &pfOriData[i * 3];

		// Interpolate
		interpolate_discrete_3D_point(fioT2,
			lvaPeaks.plvz[i].x, lvaPeaks.plvz[i].y, lvaPeaks.plvz[i].z,
			pfOriCurr[0], pfOriCurr[1], pfOriCurr[2]);

		// Subtract radius/image center of fioT2
		pfOriCurr[0] -= fRadius;
		pfOriCurr[1] -= fRadius;
		pfOriCurr[2] -= fRadius;

		// Normalize to unit length
		vec3D_norm_3d(pfOriCurr);
	}

	int iOrientationsReturned = 0;

	// Set my descriptor to 0
	fioSet(fioImg, 0);
	LOCATION_VALUE_XYZ_ARRAY lvaPeaks2;
	LOCATION_VALUE_XYZ lvData2[ORIBINS*ORIBINS / 8];
	lvaPeaks2.plvz = &(lvData2[0]);
	for (int i = 0; i < lvaPeaks.iCount && i < fioImg.z && iOrientationsReturned < iMaxOri; i++)
	{
		//if( lvaPeaks.plvz[i].fValue < fHist2ndPeakThreshold*lvaPeaks.plvz[0].fValue )
		if (lvaPeaks.plvz[i].fValue < 0.8*lvaPeaks.plvz[0].fValue)
			//if( lvaPeaks.plvz[i].fValue < 0.4*lvaPeaks.plvz[0].fValue )
		{
			// Must be above threshold
			// 0.5 used in preliminary matching experiments
			// 0.8 works well to, 0.2 is terrible...
			break;
		}

		// Primary orientation unit vector
		// Used interpolated vectors here for correctnewss

		float *pfOriCurr = &pfOriData[i * 3];
		//pfP1[0] = lvaPeaks.plvz[i].x-fRadius;
		//pfP1[1] = lvaPeaks.plvz[i].y-fRadius;
		//pfP1[2] = lvaPeaks.plvz[i].z-fRadius;
		//vec3D_norm_3d( pfP1 );
		pfP1[0] = pfOriCurr[0];
		pfP1[1] = pfOriCurr[1];
		pfP1[2] = pfOriCurr[2];

		// This was just for testing
		//feat3D.data_zyx[0][i][0] = pfP1[0];
		//feat3D.data_zyx[0][i][1] = pfP1[1];
		//feat3D.data_zyx[0][i][2] = pfP1[2];

		// Compute secondary direction unit vector
		// perpendicular to primary

		fioSet(fioT0, 0);
		for (int zz = 0; zz < fioImg.z; zz++)
		{
			for (int yy = 0; yy < fioImg.y; yy++)
			{
				for (int xx = 0; xx < fioImg.x; xx++)
				{
					float dx = xx - fioImg.x / 2;
					float dy = yy - fioImg.y / 2;
					float dz = zz - fioImg.z / 2;
					float fLocRadiusSqr = dz*dz + dy*dy + dx*dx;
					if (fLocRadiusSqr < fRadiusSqr)
					{
						// Vector: intensity edge
						float pfEdge[3];
						float pfEdgeUnit[3];
						pfEdge[0] = fioGetPixel(fioDx, xx, yy, zz);
						pfEdge[1] = fioGetPixel(fioDy, xx, yy, zz);
						pfEdge[2] = fioGetPixel(fioDz, xx, yy, zz);
						float fEdgeMag = vec3D_mag(pfEdge);
						if (fEdgeMag == 0)
						{
							continue;
						}
						memcpy(pfEdgeUnit, pfEdge, sizeof(pfEdgeUnit));
						vec3D_norm_3d(pfEdgeUnit);

						// Remove component parallel to primary orientatoin
						float pVecPerp[3];
						float fParallelMag = vec3D_dot_3d(pfP1, pfEdgeUnit);
						pVecPerp[0] = pfEdgeUnit[0] - fParallelMag*pfP1[0];
						pVecPerp[1] = pfEdgeUnit[1] - fParallelMag*pfP1[1];
						pVecPerp[2] = pfEdgeUnit[2] - fParallelMag*pfP1[2];

						// Normalize
						vec3D_norm_3d(pVecPerp);

						// Vector length fRadius in direction of edge, centered in image
						for (int i = 0; i < 3; i++)
						{
							pVecPerp[i] *= fRadius;
							pVecPerp[i] += fRadius;
						}

						//bilinear_interpolation_paint_float
						// Add pixel
						//float *pfPix = fioGetVector( fioT0, pVecPerp[0]+0.5, pVecPerp[1]+0.5, pVecPerp[2]+0.5 );
						//*pfPix += fEdgeMag;
						fioIncPixelTrilinearInterp(fioT0, pVecPerp[0] + 0.5, pVecPerp[1] + 0.5, pVecPerp[2] + 0.5, 0, fEdgeMag);

					}
				}
			}
		}

		// Blur, find peaks
		gb3d_blur3d(fioT0, fioT1, fioT2, fBlurGradOriHist, 0.01);
		regFindFEATUREIOPeaks(lvaPeaks2, fioT2);
		lvSortHighLow(lvaPeaks2);
		sprintf(pcFileName, "bin%5.5d_orig", iCode);

		//
		//
		//

		// determine sub-pixel orientation vectors
		for (int j = 0; j < lvaPeaks2.iCount && iOrientationsReturned < fioImg.z && iOrientationsReturned < iMaxOri; j++)
		{
			if (lvaPeaks2.plvz[j].fValue < fHist2ndPeakThreshold*lvaPeaks2.plvz[0].fValue)
			{
				// fHist2ndPeakThreshold = 0.5 works well
				// Must be above threshold
				break;
			}

			//		if( lvaPeaks2.iCount > 0 )
			//		{
			// 2nd dominant orientation found, keep only first
			pfP2[0] = lvaPeaks2.plvz[j].x - fRadius;
			pfP2[1] = lvaPeaks2.plvz[j].y - fRadius;
			pfP2[2] = lvaPeaks2.plvz[j].z - fRadius;
			vec3D_norm_3d(pfP2);

			// Interpolate
			interpolate_discrete_3D_point(fioT2,
				lvaPeaks2.plvz[j].x, lvaPeaks2.plvz[j].y, lvaPeaks2.plvz[j].z,
				pfP2[0], pfP2[1], pfP2[2]);

			// Subtract radius/image center of fioT2
			pfP2[0] -= fRadius;
			pfP2[1] -= fRadius;
			pfP2[2] -= fRadius;

			// Normalize to unit length
			vec3D_norm_3d(pfP2);

			// Enforce to be perpendicular to pfP1
			float pVecPerp[3];
			float fParallelMag = vec3D_dot_3d(pfP1, pfP2);
			assert(fabs(fParallelMag) < 0.5f);
			pfP2[0] = pfP2[0] - fParallelMag*pfP1[0];
			pfP2[1] = pfP2[1] - fParallelMag*pfP1[1];
			pfP2[2] = pfP2[2] - fParallelMag*pfP1[2];
			// Re-normalize to unit length
			vec3D_norm_3d(pfP2);
			assert(vec3D_dot_3d(pfP1, pfP2) <= fabs(fParallelMag));



			// Ensure 

			// 3rd dominant orientation is cross product
			vec3D_cross_3d(pfP1, pfP2, pfP3);

			// Save vectors 
			float *pfOriMatrix = pfOri + 9 * iOrientationsReturned;
			for (int ivec = 0; ivec < 3; ivec++)
			{
				pfOriMatrix[0 * 3 + ivec] = pfP1[ivec];
				pfOriMatrix[1 * 3 + ivec] = pfP2[ivec];
				pfOriMatrix[2 * 3 + ivec] = pfP3[ivec];
			}

			iOrientationsReturned++;
		}

		//else
		//{
		//	pfP2[0] = 0;
		//	pfP2[1] = 0; 
		//	pfP2[2] = 0;
		//}

		// Save major peak
		//feat3D.data_zyx[0][i][3] = pfP2[0];
		//feat3D.data_zyx[0][i][4] = pfP2[1];
		//feat3D.data_zyx[0][i][5] = pfP2[2];
	}
	// return - this is just for testing
	return iOrientationsReturned;
}

void
Feature3DData::DetermineOrientationXYProjection(
	int iCode
)
{
	FEATUREIO fioImg;
	fioImg.x = fioImg.z = fioImg.y = FEATURE_3D_DIM;
	fioImg.pfVectors = &(data_zyx[0][0][0]);
	fioImg.pfMeans = 0;
	fioImg.pfVarrs = 0;
	fioImg.iFeaturesPerVector = 1;
	fioImg.t = 1;

	FEATUREIO fioDx = fioImg;
	FEATUREIO fioDy = fioImg;
	FEATUREIO fioDz = fioImg;

	Feature3D f11;
	Feature3D fdx;
	Feature3D fdy;
	Feature3D fdz;

	//fioAllocate( fioDx );
	//fioAllocate( fioDy );
	//fioAllocate( fioDz );

	fioDx.pfVectors = &(fdx.data_zyx[0][0][0]);
	fioDy.pfVectors = &(fdy.data_zyx[0][0][0]);
	fioDz.pfVectors = &(fdz.data_zyx[0][0][0]);


	fioGenerateEdgeImages3D(fioImg, fioDx, fioDy, fioDz);

	//fdx.OutputVisualFeature( "feat_dx" );
	//fdy.OutputVisualFeature( "feat_dy" );
	//fdz.OutputVisualFeature( "feat_dz" );
	//fioWrite( fioDx, "dx" );
	//fioWrite( fioDy, "dy" );
	//fioWrite( fioDz, "dz" );

	float pfHistAzEl[ORIBINS][ORIBINS];
	float pfHistAzElEdge[ORIBINS + 2][ORIBINS + 2];
	float pfHistAzElBlur[ORIBINS][ORIBINS];
	float pfHistAzElBlur1[ORIBINS][ORIBINS];
	float pfHistAzElBlur2[ORIBINS][ORIBINS];
	memset(pfHistAzEl, 0, sizeof(pfHistAzEl));
	memset(pfHistAzElBlur, 0, sizeof(pfHistAzElBlur));

	FEATUREIO fioHist;
	fioHist.iFeaturesPerVector = 1;
	fioHist.pfMeans = fioHist.pfVarrs = 0;
	fioHist.pfVectors = &(pfHistAzEl[0][0]);
	fioHist.t = fioHist.z = 1;
	fioHist.x = fioHist.y = ORIBINS;

	FEATUREIO fioHistEdge;
	fioHistEdge.iFeaturesPerVector = 1;
	fioHistEdge.pfMeans = fioHistEdge.pfVarrs = 0;
	fioHistEdge.pfVectors = &(pfHistAzElEdge[0][0]);
	fioHistEdge.t = fioHistEdge.z = 1;
	fioHistEdge.x = fioHistEdge.y = ORIBINS + 2;

	FEATUREIO fioHistBlur;
	fioHistBlur.iFeaturesPerVector = 1;
	fioHistBlur.pfMeans = fioHistBlur.pfVarrs = 0;
	fioHistBlur.pfVectors = &(pfHistAzElBlur[0][0]);
	fioHistBlur.t = fioHistBlur.z = 1;
	fioHistBlur.x = fioHistBlur.y = ORIBINS;

	FEATUREIO fioHistBlur1 = fioHistBlur;
	FEATUREIO fioHistBlur2 = fioHistBlur;
	fioHistBlur1.pfVectors = &(pfHistAzElBlur1[0][0]);
	fioHistBlur2.pfVectors = &(pfHistAzElBlur2[0][0]);

	LOCATION_VALUE_XYZ_ARRAY lvaPeaks;
	LOCATION_VALUE_XYZ lvData[ORIBINS*ORIBINS / 8];
	lvaPeaks.plvz = &(lvData[0]);

	float fRadiusSqr = (fioImg.x / 2)*(fioImg.x / 2);

	if (iCode == -1)
	{
		// Determine dominant orientation of feature: azimuth/elevation
		for (int zz = 0; zz < 10000000; zz++)
		{
			// Uniformly sample...
			float pfEdge[3];
			pfEdge[0] = (rand() / (RAND_MAX + 1.0) - 0.5);
			pfEdge[1] = (rand() / (RAND_MAX + 1.0) - 0.5);
			pfEdge[2] = (rand() / (RAND_MAX + 1.0) - 0.5);
			float fMag = sqrt(pfEdge[0] * pfEdge[0] + pfEdge[1] * pfEdge[1] + pfEdge[2] * pfEdge[2]);
			for (int i = 0; i < 3; i++)
			{
				// Normalize to unit length
				pfEdge[i] /= fMag;
			}

			// Determine X angle
			float fThetaAz = pfEdge[0];
			int iBinAz = ((fThetaAz + 1.0f) / 2.0f)*ORIBINS;
			if (iBinAz == ORIBINS)
			{
				iBinAz = ORIBINS - 1;
			}
			if (iBinAz < 0)
			{
				iBinAz = 0;
			}

			// Determine Y angle
			float fThetaEl = pfEdge[1];
			int iBinEl = ((fThetaEl + 1.0f) / 2.0f)*ORIBINS;
			if (iBinEl == ORIBINS)
			{
				iBinEl = ORIBINS - 1;
			}
			if (iBinEl < 0)
			{
				iBinEl = 0;
			}

			// Ad vote
			pfHistAzEl[iBinEl][iBinAz] += fMag;///g_pfAzElNorm[iBinEl][iBinAz];
			pfHistAzElBlur[iBinEl][iBinAz] += fMag;///g_pfAzElNorm[iBinEl][iBinAz];

		}
		FILE *outfile = fopen("table_azimuth_elevation.txt", "wt");
		for (int i = 0; i < ORIBINS; i++)
		{
			for (int j = 0; j < ORIBINS; j++)
			{
				fprintf(outfile, "%f,\t", pfHistAzEl[i][j]);
			}
			fprintf(outfile, "\n");
		}
		fclose(outfile);
	}
	else
	{

		// Determine dominant orientation of feature: azimuth/elevation
		for (int zz = 0; zz < fioImg.z; zz++)
		{
			for (int yy = 0; yy < fioImg.y; yy++)
			{
				for (int xx = 0; xx < fioImg.x; xx++)
				{
					float dz = zz - fioImg.z / 2;
					float dy = yy - fioImg.y / 2;
					float dx = xx - fioImg.x / 2;
					if (dz*dz + dy*dy + dx*dx < fRadiusSqr)
					{
						// Keep this sample
						float pfEdge[3];
						pfEdge[0] = fioGetPixel(fioDx, xx, yy, zz);
						pfEdge[1] = fioGetPixel(fioDy, xx, yy, zz);
						pfEdge[2] = fioGetPixel(fioDz, xx, yy, zz);
						float fMag = sqrt(pfEdge[0] * pfEdge[0] + pfEdge[1] * pfEdge[1] + pfEdge[2] * pfEdge[2]);
						for (int i = 0; i < 3; i++)
						{
							// Normalize to unit length
							pfEdge[i] /= fMag;
						}

						// Determine X angle
						float fThetaAz = pfEdge[0];
						int iBinAz = ((fThetaAz + 1.0f) / 2.0f)*ORIBINS;
						if (iBinAz == ORIBINS)
						{
							iBinAz = ORIBINS - 1;
						}
						if (iBinAz < 0)
						{
							iBinAz = 0;
						}

						// Determine Y angle
						float fThetaEl = pfEdge[1];
						int iBinEl = ((fThetaEl + 1.0f) / 2.0f)*ORIBINS;
						if (iBinEl == ORIBINS)
						{
							iBinEl = ORIBINS - 1;
						}
						if (iBinEl < 0)
						{
							iBinEl = 0;
						}

						// Ad vote
						pfHistAzEl[iBinEl][iBinAz] += fMag;///g_pfAzElNorm[iBinEl][iBinAz];
						pfHistAzElBlur[iBinEl][iBinAz] += fMag;///g_pfAzElNorm[iBinEl][iBinAz];

															   // Normalize vote for tilt angle
															   //float fUnitX = pfEdge[0]/fMag;
															   //float fUnitY = pfEdge[1]/fMag;
															   //float fUnitXYMag = sqrt(fUnitX*fUnitX+fUnitY*fUnitY);
															   //float fIncAz = ((2.0*PI)/((float)ORIBINS));
															   //float fIncEl = (((float)PI)/((float)ORIBINS));
															   //float fAngleArea = 0;
															   //float fElBinStart = (iBinEl-(ORIBINS/2))*fIncEl;
															   //fAngleArea = fIncAz*(sin(fElBinStart)-sin(fElBinStart+fIncEl))/2.0f;
															   //if( fAngleArea < 0 )
															   //	fAngleArea *= -1;
															   //assert( fElBinStart < fThetaEl && fThetaEl < fElBinStart+fIncEl );
															   //assert( fAngleArea > 0 );

					}
				}
			}
		}
	}

	PpImage ppImgRef;
	char pcFileName[400];
	gb3d_blur3d(fioHistBlur, fioHistBlur1, fioHistBlur2, 2.0f, 0.01);

	regFindFEATUREIOPeaks(lvaPeaks, fioHist);
	lvSortHighLow(lvaPeaks);

	// Find max
	int iMaxBinEl, iMaxBinAz, iZ;
	fioFindMax(fioHist, iMaxBinAz, iMaxBinEl, iZ);

	fioimgInitReference(ppImgRef, fioHist);
	sprintf(pcFileName, "hist_%6.6d.pgm", iCode);
	output_float(ppImgRef, pcFileName);

	fioimgInitReference(ppImgRef, fioHistBlur2);
	sprintf(pcFileName, "hist_blur_%6.6d.pgm", iCode);
	output_float(ppImgRef, pcFileName);
	fioimgInitReference(ppImgRef, fioHistBlur);
	sprintf(pcFileName, "hist_blur_%6.6d.pgm", iCode);
	//output_float( ppImgRef, pcFileName );

}

void
Feature3DData::DetermineOrientation3DNoGood(
	int iCode
)
{
	FEATUREIO fioImg;
	fioImg.x = fioImg.z = fioImg.y = FEATURE_3D_DIM;
	fioImg.pfVectors = &(data_zyx[0][0][0]);
	fioImg.pfMeans = 0;
	fioImg.pfVarrs = 0;
	fioImg.iFeaturesPerVector = 1;
	fioImg.t = 1;

	FEATUREIO fioDx = fioImg;
	FEATUREIO fioDy = fioImg;
	FEATUREIO fioDz = fioImg;
	fioAllocate(fioDx);
	fioAllocate(fioDy);
	fioAllocate(fioDz);
	fioGenerateEdgeImages3D(fioImg, fioDx, fioDy, fioDz);

	//#define ORIBINS 16
	float pfHistAzElEdge[ORIBINS + 2][ORIBINS + 2][ORIBINS + 2];
	float pfHistAzElBlur[3 * ORIBINS][3 * ORIBINS][3 * ORIBINS];
	float pfHistAzElBlur1[3 * ORIBINS][3 * ORIBINS][3 * ORIBINS];
	float pfHistAzElBlur2[3 * ORIBINS][3 * ORIBINS][3 * ORIBINS];

	memset(pfHistAzElBlur, 0, sizeof(pfHistAzElBlur));

	FEATUREIO fioHistEdge;
	fioHistEdge.iFeaturesPerVector = 1;
	fioHistEdge.pfMeans = fioHistEdge.pfVarrs = 0;
	fioHistEdge.pfVectors = &(pfHistAzElEdge[0][0][0]);
	fioHistEdge.t = 1;
	fioHistEdge.x = fioHistEdge.y = fioHistEdge.z = ORIBINS + 2;

	FEATUREIO fioHistBlur;
	fioHistBlur.iFeaturesPerVector = 1;
	fioHistBlur.pfMeans = fioHistBlur.pfVarrs = 0;
	fioHistBlur.pfVectors = &(pfHistAzElBlur[0][0][0]);
	fioHistBlur.t = 1;
	fioHistBlur.x = fioHistBlur.y = fioHistBlur.z = 3 * ORIBINS;

	FEATUREIO fioHistBlur1 = fioHistBlur;
	FEATUREIO fioHistBlur2 = fioHistBlur;
	fioHistBlur1.pfVectors = &(pfHistAzElBlur1[0][0][0]);
	fioHistBlur2.pfVectors = &(pfHistAzElBlur2[0][0][0]);

	LOCATION_VALUE_XYZ_ARRAY lvaPeaks;
	LOCATION_VALUE_XYZ lvData[ORIBINS*ORIBINS*ORIBINS / 27];
	lvaPeaks.plvz = &(lvData[0]);

	float fRadiusSqr = (fioImg.x / 2)*(fioImg.x / 2);

	// Determine dominant orientation of feature: azimuth/elevation
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				float dz = zz - fioImg.z / 2;
				float dy = yy - fioImg.y / 2;
				float dx = xx - fioImg.x / 2;
				if (dz*dz + dy*dy + dx*dx < fRadiusSqr)
				{
					// Keep this sample
					float pfEdge[3];
					pfEdge[0] = fioGetPixel(fioDx, xx, yy, zz);
					pfEdge[1] = fioGetPixel(fioDy, xx, yy, zz);
					pfEdge[2] = fioGetPixel(fioDz, xx, yy, zz);
					if (pfEdge[0] == 0 || pfEdge[1] == 0 || pfEdge[2] == 0)
					{
						continue;
					}

					float fMag = sqrt(pfEdge[0] * pfEdge[0] + pfEdge[1] * pfEdge[1] + pfEdge[2] * pfEdge[2]);
					if (fMag == 0.0f)
					{
						continue;
					}

					// Calculate angles in planes YX, ZX, ZY
					float pfTheta[3];
					pfTheta[0] = atan2(pfEdge[1] / fMag, pfEdge[0] / fMag); // Angle wrt x axis
					pfTheta[1] = atan2(pfEdge[2] / fMag, pfEdge[1] / fMag); // Angle wrt y axis
					pfTheta[2] = atan2(pfEdge[0] / fMag, pfEdge[2] / fMag); // Angle wrt z axis

																			// Convert to histogram bins
					int piBin[3];
					for (int iAngle = 0; iAngle < 3; iAngle++)
					{
						int iBin = ((pfTheta[iAngle] + PI) / (2.0*PI))*ORIBINS;
						if (iBin == ORIBINS)
						{
							iBin = ORIBINS - 1;
						}
						if (iBin < 0)
						{
							iBin = 0;
						}
						piBin[iAngle] = iBin;
					}

					// Set into histogram
					for (int i = 0; i < 3; i++)
					{
						for (int j = 0; j < 3; j++)
						{
							for (int k = 0; k < 3; k++)
							{
								pfHistAzElBlur[ORIBINS*i + piBin[0]][ORIBINS*j + piBin[1]][ORIBINS*j + piBin[2]] += fMag;
							}
						}
					}
				}
			}
		}
	}

	PpImage ppImgRef;
	char pcFileName[400];

	// Blur histogram
	gb3d_blur3d(fioHistBlur, fioHistBlur1, fioHistBlur2, 2.0f, 0.01);

	// Copy values into search volume
	for (int i = 0; i < ORIBINS + 2; i++)
	{
		for (int j = 0; j < ORIBINS + 2; j++)
		{
			for (int k = 0; k < ORIBINS + 2; k++)
			{
				pfHistAzElEdge[i][j][k] =
					pfHistAzElBlur2[ORIBINS + i - 1][ORIBINS + j - 1][ORIBINS + k - 1];
			}
		}
	}

	// Find peaks
	regFindFEATUREIOPeaks(lvaPeaks, fioHistEdge);
	lvSortHighLow(lvaPeaks);
	for (int i = 0; i < lvaPeaks.iCount; i++)
	{
		// Remove shift
		lvaPeaks.plvz[i].x -= 1;
		lvaPeaks.plvz[i].y -= 1;
		lvaPeaks.plvz[i].z -= 1;
	}

	// Now find secondary tilt angles

	//memset( pfHistBlur, 0, sizeof(pfHistAzElBlur) );
	for (int iPeak = 0; iPeak < lvaPeaks.iCount; iPeak++)
	{
		// Determine dominant orientation of feature
		// perpendicular

		float pfVector[3];
		for (int i = 0; i < 3; i++)
		{
			// Cosine of angle gives the projection along the axis
			float fAngle = (lvaPeaks.plvz[iPeak].x / ((float)ORIBINS))*(2.0*PI);
			pfVector[i] = cos(fAngle);
		}

		for (int zz = 0; zz < fioImg.z; zz++)
		{
			for (int yy = 0; yy < fioImg.y; yy++)
			{
				for (int xx = 0; xx < fioImg.x; xx++)
				{
					float dz = zz - fioImg.z / 2;
					float dy = yy - fioImg.y / 2;
					float dx = xx - fioImg.x / 2;
					if (dz*dz + dy*dy + dx*dx < fRadiusSqr)
					{
						// Keep this sample
						float pfEdge[3];
						pfEdge[0] = fioGetPixel(fioDx, xx, yy, zz);
						pfEdge[1] = fioGetPixel(fioDy, xx, yy, zz);
						pfEdge[2] = fioGetPixel(fioDz, xx, yy, zz);
						if (pfEdge[0] == 0 || pfEdge[1] == 0 || pfEdge[2] == 0)
						{
							continue;
						}

						float pfCrossVector[3];

						float fMag = sqrt(pfEdge[0] * pfEdge[0] + pfEdge[1] * pfEdge[1] + pfEdge[2] * pfEdge[2]);
						if (fMag == 0.0f)
						{
							continue;
						}

						// Calculate angles in planes YX, ZX, ZY
						float pfTheta[3];
						pfTheta[0] = atan2(pfEdge[1] / fMag, pfEdge[0] / fMag);
						pfTheta[1] = atan2(pfEdge[2] / fMag, pfEdge[0] / fMag);
						pfTheta[2] = atan2(pfEdge[2] / fMag, pfEdge[1] / fMag);

						// Convert to histogram bins
						int piBin[3];
						for (int iAngle = 0; iAngle < 3; iAngle++)
						{
							int iBin = ((pfTheta[iAngle] + PI) / (2.0*PI))*ORIBINS;
							if (iBin == ORIBINS)
							{
								iBin = ORIBINS - 1;
							}
							if (iBin < 0)
							{
								iBin = 0;
							}
							piBin[iAngle] = iBin;
						}

						// Set into histogram
						for (int i = 0; i < 3; i++)
						{
							for (int j = 0; j < 3; j++)
							{
								for (int k = 0; k < 3; k++)
								{
									pfHistAzElBlur[ORIBINS*i + piBin[0]][ORIBINS*j + piBin[1]][ORIBINS*j + piBin[2]] += fMag;
								}
							}
						}
					}
				}
			}
		}
	}

	fioDelete(fioDx);
	fioDelete(fioDy);
	fioDelete(fioDz);
}

void
Feature3D::NormalizeData(
	float *pfMean,
	float *pfVarr,
	float *pfMin,
	float *pfMax
)
{
	float fSum = 0;
	float fMax = data_zyx[0][0][0], fMin = data_zyx[0][0][0];

	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				fSum += data_zyx[zz][yy][xx];
				if (data_zyx[zz][yy][xx] < fMin)
				{
					fMin = data_zyx[zz][yy][xx];
				}
				if (data_zyx[zz][yy][xx] > fMax)
				{
					fMax = data_zyx[zz][yy][xx];
				}
			}
		}
	}
	float fMean = fSum / (Feature3D::FEATURE_3D_DIM*Feature3D::FEATURE_3D_DIM*Feature3D::FEATURE_3D_DIM);
	float fSumSqr = 0;

	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				data_zyx[zz][yy][xx] -= fMean;
				fSumSqr += data_zyx[zz][yy][xx] * data_zyx[zz][yy][xx];
			}
		}
	}

	if (pfMean)
	{
		*pfMean = fMean;
	}
	if (pfVarr)
	{
		*pfVarr = fSumSqr;
	}

	if (pfMin)
	{
		*pfMin = fMin;
	}
	if (pfMax)
	{
		*pfMax = fMax;
	}

	float fDiv = 1.0f / (float)sqrt(fSumSqr);
	fSum = 0;
	fSumSqr = 0;

	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				data_zyx[zz][yy][xx] *= fDiv;
				fSum += data_zyx[zz][yy][xx];
				fSumSqr += data_zyx[zz][yy][xx] * data_zyx[zz][yy][xx];
			}
		}
	}
}

void
Feature3D::DiffPCs(
	int iPCs
)
{
	float fMean = 0;
	for (int i = 0; i < FEATURE_3D_PCS; i++)
	{
		if (i < iPCs - 1)
		{
			m_pfPC[i] = m_pfPC[i + 1] - m_pfPC[i];
		}
		else
		{
			m_pfPC[i] = 0;
		}
		fMean += m_pfPC[i];
	}
	fMean /= (float)(iPCs - 1);

	float fSumSqr = 0;
	for (int i = 0; i < iPCs - 1; i++)
	{
		m_pfPC[i] -= fMean;
		fSumSqr += m_pfPC[i] * m_pfPC[i];
	}

	float fDiv = 1 / sqrt(fSumSqr);

	fMean = 0;
	fSumSqr = 0;
	for (int i = 0; i < iPCs - 1; i++)
	{
		m_pfPC[i] *= fDiv;
		fMean += m_pfPC[i];
		fSumSqr += m_pfPC[i] * m_pfPC[i];
	}


	// Normalize

}

void
Feature3D::ProjectToPCs(
	vector<Feature3D>	&vecPCs,
	int iPCs,
	float *pfPCArray
)
{
	float *pfCoeffs = &(m_pfPC[0]);
	if (pfPCArray)
	{
		pfCoeffs = pfPCArray;
	}

	for (int i = 0; i < FEATURE_3D_PCS; i++)
	{
		pfCoeffs[i] = 0;
		if (i < vecPCs.size())
		{
			pfCoeffs[i] = DotProduct(vecPCs[i]);
		}
	}

	//// Zero everything
	//for( int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++ )
	//{
	//	for( int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++ )
	//	{
	//		for( int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++ )
	//		{
	//			data_zyx[zz][yy][xx] = 0;
	//		}
	//	}
	//}

	//// Set PC coeffs
	//for( int i = 0; i < vecPCs.size() && i < iPCs; i++ )
	//{
	//	data_zyx[0][0][i] = pfCoeffs[i];
	//}

}

//void
//Feature3D::ProjecasdfasdtToPCs(
//						vector<Feature3D>	&vecPCs,
//						int iPCs
//	)
//{
//	for( int i = 0; i < FEATURE_3D_PCS; i++ )
//	{
//		m_pfPC[i] = 0;
//		if( i < vecPCs.size() )
//		{
//			m_pfPC[i] = DotProduct( vecPCs[i] );
//		}
//	}
//}

float
Feature3D::DotProduct(
	const Feature3D &feat3D
) const
{
	float fSumDot = 0;

	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				float fDot = (data_zyx[zz][yy][xx] * feat3D.data_zyx[zz][yy][xx]);
				fSumDot += fDot;
			}
		}
	}
	return fSumDot;
}

float
Feature3DInfo::DistSqrXYZ(
	const Feature3DInfo &feat3D
) const
{
	float dx = x - feat3D.x;
	float dy = y - feat3D.y;
	float dz = z - feat3D.z;
	return dx*dx + dy*dy + dz*dx;
}

float
Feature3D::DistSqr(
	const Feature3D &feat3D
) const
{
	float fSumSqr = 0;

	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				float fDiff = (data_zyx[zz][yy][xx] - feat3D.data_zyx[zz][yy][xx]);
				fSumSqr += fDiff*fDiff;
			}
		}
	}
	return fSumSqr;
}

float
Feature3D::DistSqr(
	const Feature3D &feat3D,
	int iFeatCount
) const
{
	float fSumSqr = 0;

	for (int zz = 0; zz < iFeatCount; zz++)
	{
		float fDiff = (data_zyx[0][0][zz] - feat3D.data_zyx[0][0][zz]);
		fSumSqr += fDiff*fDiff;
	}
	return fSumSqr;
}

float
Feature3D::DistSqr(
	const Feature3D &feat3D,
	const Feature3D &featVarrInv
) const
{
	float fSumSqr = 0;

	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				float fDiff = (data_zyx[zz][yy][xx] - feat3D.data_zyx[zz][yy][xx]);
				fSumSqr += fDiff*fDiff*featVarrInv.data_zyx[zz][yy][xx];
			}
		}
	}
	return fSumSqr;
}


int
Feature3DChar::OutputVisualFeatureInVolume(
	FEATUREIO &fioImg,
	char *pcFileNameBase,
	int bShowFeats,
	int bOneImage
)
{
	char pcFileName[400];
	PpImage ppImgOut;
	FEATUREIO fioTmp;

	if (bOneImage)
	{
		PpImage ppImgOutSub;
		PpImage ppBigOut;

		ppBigOut.Initialize(fioImg.z + fioImg.x, fioImg.y + fioImg.x, (fioImg.y + fioImg.x) * sizeof(float), sizeof(float) * 8);
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		for (int yy = 0; yy < ppBigOut.Rows(); yy++)
		{
			for (int xx = 0; xx < ppBigOut.Cols(); xx++)
			{
				((float*)ppBigOut.ImageRow(yy))[xx] = 0;
			}
		}

		ppImgOutSub.InitializeSubImage(
			fioImg.z, fioImg.y,
			ppBigOut.RowInc(), ppBigOut.BitsPerPixel(),
			(unsigned char*)ppBigOut.ImageRow(0)
		);
		ppImgOut.Initialize(fioImg.z, fioImg.y, fioImg.y * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceZY(fioImg, x, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		if (bShowFeats)
			of_paint_white_circle(z, y, 2 * scale, ppImgOut);
		for (int yy = 0; yy < fioImg.z; yy++)
		{
			for (int xx = 0; xx < fioImg.y; xx++)
			{
				((float*)ppImgOutSub.ImageRow(yy))[xx] =
					((float*)ppImgOut.ImageRow(yy))[xx];
			}
		}
		output_float(ppBigOut, pcFileName);

		ppImgOutSub.InitializeSubImage(
			fioImg.y, fioImg.x,
			ppBigOut.RowInc(), ppBigOut.BitsPerPixel(),
			(unsigned char*)ppBigOut.ImageRow(fioImg.z)
		);
		ppImgOut.Initialize(fioImg.y, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceXY(fioImg, z, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		if (bShowFeats)
			of_paint_white_circle(y, x, 2 * scale, ppImgOut);
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				((float*)ppImgOutSub.ImageRow(xx))[yy] =
					((float*)ppImgOut.ImageRow(yy))[fioImg.x - 1 - xx];
			}
		}
		output_float(ppBigOut, pcFileName);

		ppImgOutSub.InitializeSubImage(
			fioImg.z, fioImg.x,
			ppBigOut.RowInc(), ppBigOut.BitsPerPixel(),
			(unsigned char*)((float*)ppBigOut.ImageRow(0) + fioImg.y)
		);
		ppImgOut.Initialize(fioImg.z, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceZX(fioImg, y, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		if (bShowFeats)
			of_paint_white_circle(z, x, 2 * scale, ppImgOut);
		for (int yy = 0; yy < fioImg.z; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				((float*)ppImgOutSub.ImageRow(yy))[xx] =
					((float*)ppImgOut.ImageRow(yy))[xx];
			}
		}
		output_float(ppBigOut, pcFileName);
	}
	else
	{

		ppImgOut.Initialize(fioImg.z, fioImg.y, fioImg.y * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceZY(fioImg, x, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_zy.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		if (bShowFeats)
			of_paint_white_circle(z, y, 2 * scale, ppImgOut);
		output_float(ppImgOut, pcFileName);

		ppImgOut.Initialize(fioImg.y, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceXY(fioImg, z, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_xy.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		if (bShowFeats)
			of_paint_white_circle(y, x, 2 * scale, ppImgOut);
		output_float(ppImgOut, pcFileName);

		ppImgOut.Initialize(fioImg.z, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceZX(fioImg, y, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_zx.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		if (bShowFeats)
			of_paint_white_circle(z, x, 2 * scale, ppImgOut);
		output_float(ppImgOut, pcFileName);
	}

	return 1;
}

float
Feature3DChar::DistSqr(
	const Feature3DChar &feat3D
) const
{
	float fSumSqr = 0;

	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				float fDiff = (data_zyx[zz][yy][xx] - feat3D.data_zyx[zz][yy][xx]);
				fSumSqr += fDiff*fDiff;
			}
		}
	}
	return fSumSqr;
}

int
Feature3DShort::OutputVisualFeatureInVolume(
	FEATUREIO &fioImg,
	char *pcFileNameBase,
	int bShowFeats,
	int bOneImage
)
{
	char pcFileName[400];
	PpImage ppImgOut;
	FEATUREIO fioTmp;

	if (bOneImage)
	{
		PpImage ppImgOutSub;
		PpImage ppBigOut;

		ppBigOut.Initialize(fioImg.z + fioImg.x, fioImg.y + fioImg.x, (fioImg.y + fioImg.x) * sizeof(float), sizeof(float) * 8);
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		for (int yy = 0; yy < ppBigOut.Rows(); yy++)
		{
			for (int xx = 0; xx < ppBigOut.Cols(); xx++)
			{
				((float*)ppBigOut.ImageRow(yy))[xx] = 0;
			}
		}

		ppImgOutSub.InitializeSubImage(
			fioImg.z, fioImg.y,
			ppBigOut.RowInc(), ppBigOut.BitsPerPixel(),
			(unsigned char*)ppBigOut.ImageRow(0)
		);
		ppImgOut.Initialize(fioImg.z, fioImg.y, fioImg.y * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceZY(fioImg, x, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		if (bShowFeats)
			of_paint_white_circle(z, y, 2 * scale, ppImgOut);
		for (int yy = 0; yy < fioImg.z; yy++)
		{
			for (int xx = 0; xx < fioImg.y; xx++)
			{
				((float*)ppImgOutSub.ImageRow(yy))[xx] =
					((float*)ppImgOut.ImageRow(yy))[xx];
			}
		}
		output_float(ppBigOut, pcFileName);

		ppImgOutSub.InitializeSubImage(
			fioImg.y, fioImg.x,
			ppBigOut.RowInc(), ppBigOut.BitsPerPixel(),
			(unsigned char*)ppBigOut.ImageRow(fioImg.z)
		);
		ppImgOut.Initialize(fioImg.y, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceXY(fioImg, z, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		if (bShowFeats)
			of_paint_white_circle(y, x, 2 * scale, ppImgOut);
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				((float*)ppImgOutSub.ImageRow(xx))[yy] =
					((float*)ppImgOut.ImageRow(yy))[fioImg.x - 1 - xx];
			}
		}
		output_float(ppBigOut, pcFileName);

		ppImgOutSub.InitializeSubImage(
			fioImg.z, fioImg.x,
			ppBigOut.RowInc(), ppBigOut.BitsPerPixel(),
			(unsigned char*)((float*)ppBigOut.ImageRow(0) + fioImg.y)
		);
		ppImgOut.Initialize(fioImg.z, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceZX(fioImg, y, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		if (bShowFeats)
			of_paint_white_circle(z, x, 2 * scale, ppImgOut);
		for (int yy = 0; yy < fioImg.z; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				((float*)ppImgOutSub.ImageRow(yy))[xx] =
					((float*)ppImgOut.ImageRow(yy))[xx];
			}
		}
		output_float(ppBigOut, pcFileName);
	}
	else
	{

		ppImgOut.Initialize(fioImg.z, fioImg.y, fioImg.y * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceZY(fioImg, x, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_zy.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		if (bShowFeats)
			of_paint_white_circle(z, y, 2 * scale, ppImgOut);
		output_float(ppImgOut, pcFileName);

		ppImgOut.Initialize(fioImg.y, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceXY(fioImg, z, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_xy.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		if (bShowFeats)
			of_paint_white_circle(y, x, 2 * scale, ppImgOut);
		output_float(ppImgOut, pcFileName);

		ppImgOut.Initialize(fioImg.z, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceZX(fioImg, y, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_zx.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		if (bShowFeats)
			of_paint_white_circle(z, x, 2 * scale, ppImgOut);
		output_float(ppImgOut, pcFileName);
	}

	return 1;
}

float
Feature3DShort::DistSqr(
	const Feature3DShort &feat3D
) const
{
	float fSumSqr = 0;

	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				float fDiff = (data_zyx[zz][yy][xx] - feat3D.data_zyx[zz][yy][xx]);
				fSumSqr += fDiff*fDiff;
			}
		}
	}
	return fSumSqr;
}

float
Feature3DShort::DistSqrFlipX(
	const Feature3DShort &feat3D
) const
{
	float fSumSqr = 0;

	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				float fDiff = (data_zyx[zz][yy][xx] - feat3D.data_zyx[zz][yy][Feature3D::FEATURE_3D_DIM - 1 - xx]);
				fSumSqr += fDiff*fDiff;
			}
		}
	}
	return fSumSqr;
}

float
Feature3DShort::DistSqr(
	const Feature3DShort &feat3D,
	const Feature3D &featVarrInv
) const
{
	float fSumSqr = 0;

	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				float fDiff = (data_zyx[zz][yy][xx] - feat3D.data_zyx[zz][yy][xx]);
				fSumSqr += fDiff*fDiff*featVarrInv.data_zyx[zz][yy][xx];
			}
		}
	}
	return fSumSqr;
}

int
Feature2D::OutputVisualFeature(
	char *pcFileNameBase
)
{
	char pcFileName[400];
	PpImage ppImgOut;

	ppImgOut.InitializeSubImage(
		Feature2D::FEATURE_2D_DIM, Feature2D::FEATURE_2D_DIM,
		Feature2D::FEATURE_2D_DIM * sizeof(float), sizeof(float) * 8,
		(unsigned char*)(&(data_yx[0][0]))
	);

	assert(pcFileNameBase);

	sprintf(pcFileName, "%s_x%3.3d_y%3.3d.pgm", pcFileNameBase, (int)x, (int)y);
	output_float(ppImgOut, pcFileName);

	return 1;
}


int
Feature2D::OutputVisualFeatureInImage(
	FEATUREIO &fioImg,
	char *pcFileNameBase
)
{
	char pcFileName[400];
	PpImage ppImgOut;

	fioimgInitReference(ppImgOut, fioImg);
	fioNormalize(fioImg, 255.0f);
	of_paint_keypoint(y, x, scale, theta, ppImgOut);
	if (pcFileNameBase)
	{
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d.pgm", pcFileNameBase, (int)x, (int)y);
		output_float(ppImgOut, pcFileName);
	}
	return 1;
}

int
Feature2D::ZeroData(
)
{
	memset(&(data_yx[0][0]), 0, sizeof(data_yx));
	return 0;
}

int
Feature2D::ToFileBin(
	FILE *outfile	// Initalized "wb"
)
{
	int iBytes = 0;
	iBytes += fwrite(&x, sizeof(x), 1, outfile);
	iBytes += fwrite(&y, sizeof(y), 1, outfile);
	iBytes += fwrite(&scale, sizeof(scale), 1, outfile);
	iBytes += fwrite(&theta, sizeof(theta), 1, outfile);
	iBytes += fwrite(&ori, sizeof(ori), 1, outfile);
	iBytes += fwrite(&eigs, sizeof(eigs), 1, outfile);
	iBytes += fwrite(&data_yx, sizeof(data_yx), 1, outfile);
	return iBytes;
}

int
Feature2D::FromFileBin(
	FILE *infile	// Initalized "rb"
)
{
	int iBytes = 0;
	iBytes += fread(&x, sizeof(x), 1, infile);
	iBytes += fread(&y, sizeof(y), 1, infile);
	iBytes += fread(&scale, sizeof(scale), 1, infile);
	iBytes += fread(&theta, sizeof(theta), 1, infile);
	iBytes += fread(&ori, sizeof(ori), 1, infile);
	iBytes += fread(&eigs, sizeof(eigs), 1, infile);
	iBytes += fread(&data_yx, sizeof(data_yx), 1, infile);
	return iBytes;
}

float
Feature2D::DistSqr(
	Feature2D &feat2D
)
{
	float fSumSqr = 0;

	for (int yy = 0; yy < Feature2D::FEATURE_2D_DIM; yy++)
	{
		for (int xx = 0; xx < Feature2D::FEATURE_2D_DIM; xx++)
		{
			float fDiff = (data_yx[yy][xx] - feat2D.data_yx[yy][xx]);
			fSumSqr += fDiff*fDiff;
		}
	}
	return fSumSqr;
}

float
Feature2D::MeanData(
)
{
	float fSum = 0;

	for (int yy = 0; yy < Feature2D::FEATURE_2D_DIM; yy++)
	{
		for (int xx = 0; xx < Feature2D::FEATURE_2D_DIM; xx++)
		{
			fSum += data_yx[yy][xx];
		}
	}
	return fSum / (float)(Feature2D::FEATURE_2D_DIM*Feature2D::FEATURE_2D_DIM);
}

void
Feature2D::NormalizeData(
	float fMean,
	float fLength,
	int bAbs
)
{
	float fSumSqr = 0;
	if (bAbs)
	{

		for (int yy = 0; yy < Feature2D::FEATURE_2D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature2D::FEATURE_2D_DIM; xx++)
			{
				data_yx[yy][xx] -= fMean;
				data_yx[yy][xx] = fabs(data_yx[yy][xx]);
				fSumSqr += data_yx[yy][xx] * data_yx[yy][xx];
			}
		}
	}
	else
	{

		for (int yy = 0; yy < Feature2D::FEATURE_2D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature2D::FEATURE_2D_DIM; xx++)
			{
				data_yx[yy][xx] -= fMean;
				fSumSqr += data_yx[yy][xx] * data_yx[yy][xx];
			}
		}
	}
	float fDiv = (float)sqrt(fLength / fSumSqr);
	fSumSqr = 0;

	for (int yy = 0; yy < Feature2D::FEATURE_2D_DIM; yy++)
	{
		for (int xx = 0; xx < Feature2D::FEATURE_2D_DIM; xx++)
		{
			data_yx[yy][xx] *= fDiv;
			fSumSqr += data_yx[yy][xx] * data_yx[yy][xx];
		}
	}
}

void
Feature2D::AbsoluteValueData(
)
{

	for (int yy = 0; yy < Feature2D::FEATURE_2D_DIM; yy++)
	{
		for (int xx = 0; xx < Feature2D::FEATURE_2D_DIM; xx++)
		{
			data_yx[yy][xx] = fabs(data_yx[yy][xx]);
		}
	}
}

//void
//Feature2D::NormalizeUnitLength(
//							   )
//{
//	// Normalize to length 512...
//	float fNorm = 0;
//
//	float fMin = 1000;
//	for( int y = 0; y < Feature2D::FEATURE_2D_DIM; y++ )
//	{
//		for( int x = 0; x < Feature2D::FEATURE_2D_DIM; x++ )
//		{
//			if( data_yx[yy][xx] < fMin )
//			{
//				fMin = data_yx[yy][xx];
//			}
//		}
//	}
//
//	for( int y = 0; y < Feature2D::FEATURE_2D_DIM; y++ )
//	{
//		for( int x = 0; x < Feature2D::FEATURE_2D_DIM; x++ )
//		{
//			float fValue = data_yx[yy][xx]-fMin;
//			fNorm += fValue*fValue;
//		}
//	}
//	assert( fNorm > 0 );
//	fNorm = 1.0/(float)sqrt( fNorm );
//	for( int y = 0; y < Feature2D::FEATURE_2D_DIM; y++ )
//	{
//		for( int x = 0; x < Feature2D::FEATURE_2D_DIM; x++ )
//		{
//			float fValue = 512.0*(data_yx[yy][xx]-fMin)*fNorm;
//			data_yx[yy][xx] = (unsigned char)fValue;
//		}
//	}
//}


int
generateFeatures(
	FEATUREIO					&fio,
	LOCATION_VALUE_XYZ_ARRAY	&lvaMinima,
	LOCATION_VALUE_XYZ_ARRAY	&lvaMaxima,
	float fScale
)
{
	PpImage ppImgXY;
	ppImgXY.Initialize(fio.y, fio.x, fio.x * sizeof(float), sizeof(float) * 8);
	FEATUREIO fioOut;
	fioOut.x = fio.x;
	fioOut.y = fio.y;
	fioOut.z = fio.z;
	fioOut.iFeaturesPerVector = fio.iFeaturesPerVector;
	fioOut.pfVectors = (float*)ppImgXY.ImageRow(0);

	char pcFileName[300];

	fioFeatureSliceXY(fio, 0, 0, (float*)ppImgXY.ImageRow(0));
	fioNormalize(fioOut, 255);
	for (int i = 0; i < lvaMinima.iCount; i++)
	{
		LOCATION_VALUE_XYZ &lv = lvaMinima.plvz[i];

		//fioFeatureSliceXY( fio, lv.z, 0, (float*)ppImgXY.ImageRow(0) );
		//fioNormalize( fio, 255 );
		*fioGetVector(fioOut, lv.x, lv.y, lv.z) = 255;
		//of_paint_white_circle( lv.y, lv.x, fScale + 1, ppImgXY );

		//sprintf( pcFileName, "point_%3.3d_%3.3d_%3.3d.pgm", lv.z, lv.x, lv.y );
		//output_float( ppImgXY, pcFileName );
	}
	sprintf(pcFileName, "minima_%2.2d.pgm", (int)fScale);
	output_float(ppImgXY, pcFileName);

	fioFeatureSliceXY(fio, 0, 0, (float*)ppImgXY.ImageRow(0));
	fioNormalize(fioOut, 255);
	for (int i = 0; i < lvaMaxima.iCount; i++)
	{
		LOCATION_VALUE_XYZ &lv = lvaMaxima.plvz[i];

		//fioFeatureSliceXY( fio, lv.z, 0, (float*)ppImgXY.ImageRow(0) );
		//fioNormalize( fio, 255 );
		*fioGetVector(fioOut, lv.x, lv.y, lv.z) = 255;
		//of_paint_white_circle( lv.y, lv.x, fScale + 1, ppImgXY );

		//sprintf( pcFileName, "point_%3.3d_%3.3d_%3.3d.pgm", lv.z, lv.x, lv.y );
		//output_float( ppImgXY, pcFileName );
	}
	sprintf(pcFileName, "maxima_%2.2d.pgm", (int)fScale);
	output_float(ppImgXY, pcFileName);

	return 1;
}

void
output_z(
	FEATUREIO &fio,
	int x, int y,
	char *pcFileName
)
{
	FILE *outfile = fopen(pcFileName, "wt");
	for (int i = 0; i < fio.z; i++)
	{
		fprintf(outfile, "%f\t%f\t%f\n", fioGetPixel(fio, x - 1, y, i), fioGetPixel(fio, x, y, i), fioGetPixel(fio, x + 1, y, i));
	}
	fclose(outfile);
}


//
// initFIOStack()
//
// Return non-zero on error, zero on success.
//
//
int
initFIOStack(
	FEATUREIO &fioExample, // Example of a stack image (for dimensions)
	FEATUREIO &fioStack, // Stack of images, allocated here
	FEATUREIO *pfioStackRefs, // Array of FEATUREIO, size iStackSize
	int iStackSize // Number of images in stack & stack references
)
{
	fioStack = fioExample;
	fioStack.t = iStackSize;
	fioStack.pfVectors = 0;
	int iAlloc = fioAllocate(fioStack);
	if (!iAlloc)
	{
		// Error
		return -1;
	}

	for (int i = 0; i < iStackSize; i++)
	{
		pfioStackRefs[i] = fioExample;
		int iElementsPerImage = fioExample.z*fioExample.y*fioExample.x;
		pfioStackRefs[i].pfVectors = fioStack.pfVectors + i*iElementsPerImage;
	}

	return 0;
}

//
// sampleImage3D()
//
// Need to know the scale difference between 
//
int
sampleImage3D(
	Feature3D &feat3D,
	FEATUREIO &fioSample, // 5x5x5 = 125 feats?,
	FEATUREIO &fioImg, // Original image
	FEATUREIO &fioDx, // Derivative images
	FEATUREIO &fioDy,
	FEATUREIO &fioDz
)
{
	float fOriInv[3][3];

	// We want to sample the image in a manner proportional to 
	// feat3D.scale

	// Image region is 2x the feature scale
	float fImageRad = 2.0f*feat3D.scale; //MICCAI 09, NeuroImage 09
	int iRadMax = fImageRad + 2;

	if (
		feat3D.x - iRadMax < 0 ||
		feat3D.y - iRadMax < 0 ||
		feat3D.z - iRadMax < 0 ||
		feat3D.x + iRadMax >= fioImg.x ||
		feat3D.y + iRadMax >= fioImg.y ||
		feat3D.z + iRadMax >= fioImg.z)
	{
		// Feature out of image bounds
		return -1;
	}

	//fImageRad = 3.0f*feat3D.scale; // Rescale, to get cover a bigger image area.

	// Invert orientation marix
	invert_3x3<float, double>(feat3D.ori, fOriInv);

	//
	// Sample pixels: trilinear interpolation
	// Here, (x,y,z) are in the sampled image coordinate system
	//
	int iSampleRad = Feature3D::FEATURE_3D_DIM / 2;
	for (int z = -iSampleRad; z <= iSampleRad; z++)
	{
		int iZ = feat3D.z + z;
		for (int y = -iSampleRad; y <= iSampleRad; y++)
		{
			int iY = feat3D.y + y;
			for (int x = -iSampleRad; x <= iSampleRad; x++)
			{
				int iX = feat3D.x + x;
				//if( z*z + y*y + x*x <= iRadPlusOneSqr )
				{
					// 1) Rotate feature coordinate to image coordinate

					float xyz_feat[3];  // Feat coords
					float xyz_img[3]; // Image coords
					xyz_feat[0] = x;
					xyz_feat[1] = y;
					xyz_feat[2] = z;

					// Skip orientation for now ... 
					//xyz_img[0] = xyz_feat[0]; xyz_img[1] = xyz_feat[1]; xyz_img[2] = xyz_feat[2];
					// Include rotation - if no rotation is desired, set
					// rotation matrix to identity.
					mult_3x3<float, double>(fOriInv, xyz_feat, xyz_img);

					// 2) Scale feature magnitude
					//scale_3x3<float,double>( xyz_img, feat3D.scale/(float)(iRad) );
					float fScale = fImageRad / (float)(iSampleRad);
					xyz_img[0] *= fScale;
					xyz_img[1] *= fScale;
					xyz_img[2] *= fScale;

					// 3) Translated to current feature center

					xyz_img[0] += feat3D.x;
					xyz_img[1] += feat3D.y;
					xyz_img[2] += feat3D.z;

					// 4) Interpolate pixel

					float fPixel;

					if (xyz_img[0] < 0 || xyz_img[0] >= fioImg.x ||
						xyz_img[0] < 0 || xyz_img[0] >= fioImg.x ||
						xyz_img[0] < 0 || xyz_img[0] >= fioImg.x)
					{
						// Out of image gets black pixel (MR imagery)
						fPixel = 0;
					}
					else
					{
						// Aha - I believe we are introducing a 0.5 pixel shift backwards here
						// In GenerateFeatures3D() is corrected tho, by adding 0.5
						fPixel = fioGetPixelTrilinearInterp(
							fioImg, xyz_img[0], xyz_img[1], xyz_img[2]);
					}

					// 5) Set pixel into sample volume - could just as well set directly
					// into feature.
					float *pfVec = fioGetVector(fioSample,
						x + iSampleRad,
						y + iSampleRad,
						z + iSampleRad);
					*pfVec = fPixel;
				}
			}
		}
	}
	return 0;
}

//
// determineOrientation3D()
//
// Determine orientation component of feat3D.
//
int
determineOrientation3D(
	Feature3D &feat3D,
	FEATUREIO &fioDx,
	FEATUREIO &fioDy,
	FEATUREIO &fioDz
)
{
	float fMat[3][3] = { { 0,0,0 },{ 0,0,0 },{ 0,0,0 } };
	float fRad = feat3D.scale;
	int iRad = (int)(fRad + 0.5);
	int iRadPlusOneSqr = (iRad + 1)*iRad;

	if (
		feat3D.x - iRad < 0 ||
		feat3D.y - iRad < 0 ||
		feat3D.z - iRad < 0 ||
		feat3D.x + iRad >= fioDx.x ||
		feat3D.y + iRad >= fioDy.y ||
		feat3D.z + iRad >= fioDz.z)
	{
		// Feature out of image bounds
		return -1;
	}

	// Generate Hessian of edge values: assume values are distributed
	// about zero mean (dx,dy,dz=0,0,0)
	for (int z = -iRad; z <= iRad; z++)
	{
		int iZ = feat3D.z + z;
		for (int y = -iRad; y <= iRad; y++)
		{
			int iY = feat3D.y + y;
			for (int x = -iRad; x <= iRad; x++)
			{
				int iX = feat3D.x + x;
				if (z*z + y*y + x*x <= iRadPlusOneSqr)
				{
					// Keep this sample
					float pfEdge[3];
					pfEdge[0] = fioGetPixel(fioDx, iX, iY, iZ);
					pfEdge[1] = fioGetPixel(fioDy, iX, iY, iZ);
					pfEdge[2] = fioGetPixel(fioDz, iX, iY, iZ);

					for (int i = 0; i < 3; i++)
					{
						for (int j = 0; j < 3; j++)
						{
							fMat[i][j] += pfEdge[i] * pfEdge[j];
						}
					}
				}
			}
		}
	}

	SingularValueDecomp<float, 3, 3>(fMat, feat3D.eigs, feat3D.ori);
	SortEigenDecomp<float, 3>(feat3D.eigs, feat3D.ori);

	return 0;
}

//
// determineOrientation3D()
//
// Determine orientation
//
int
determineOrientation3D(
	Feature3D &feat3D
)
{
	FEATUREIO fioImg;
	fioImg.x = fioImg.z = fioImg.y = Feature3D::FEATURE_3D_DIM;
	fioImg.pfVectors = &(feat3D.data_zyx[0][0][0]);
	fioImg.pfMeans = 0;
	fioImg.pfVarrs = 0;
	fioImg.iFeaturesPerVector = 1;
	fioImg.t = 1;

	// 1st derivative images - these could be passed in from the outside.
	FEATUREIO fioDx = fioImg;
	FEATUREIO fioDy = fioImg;
	FEATUREIO fioDz = fioImg;
	Feature3D fdx;
	Feature3D fdy;
	Feature3D fdz;
	fioDx.pfVectors = &(fdx.data_zyx[0][0][0]);
	fioDy.pfVectors = &(fdy.data_zyx[0][0][0]);
	fioDz.pfVectors = &(fdz.data_zyx[0][0][0]);

	fioGenerateEdgeImages3D(fioImg, fioDx, fioDy, fioDz);

	float fMat[3][3] = { { 0,0,0 },{ 0,0,0 },{ 0,0,0 } };

	float fRadius = fioImg.x / 2;
	float fRadiusSqr = (fioImg.x / 2)*(fioImg.x / 2);

	int iSampleCount = 0;

	// Determine dominant orientation of feature: azimuth/elevation
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				float dz = zz - fioImg.z / 2;
				float dy = yy - fioImg.y / 2;
				float dx = xx - fioImg.x / 2;
				if (dz*dz + dy*dy + dx*dx < fRadiusSqr)
				{
					// Keep this sample
					float pfEdge[3];
					pfEdge[0] = fioGetPixel(fioDx, xx, yy, zz);
					pfEdge[1] = fioGetPixel(fioDy, xx, yy, zz);
					pfEdge[2] = fioGetPixel(fioDz, xx, yy, zz);

					for (int i = 0; i < 3; i++)
					{
						for (int j = 0; j < 3; j++)
						{
							fMat[i][j] += pfEdge[i] * pfEdge[j];
						}
					}
				}
			}
		}
	}

	//float fTrace = fMat[0][0]+fMat[1][1]+fMat[2][2];
	//float fDet =
	//	fMat[0][0]*(fMat[1][1]*fMat[2][2]-fMat[1][2]*fMat[1][2])
	//	-
	//	fMat[0][1]*(fMat[1][0]*fMat[2][2]-fMat[1][2]*fMat[2][0])
	//	+
	//	fMat[0][2]*(fMat[1][0]*fMat[2][1]-fMat[1][1]*fMat[2][0]);

	SingularValueDecomp<float, 3, 3>(fMat, feat3D.eigs, feat3D.ori);
	SortEigenDecomp<float, 3>(feat3D.eigs, feat3D.ori);

	//float fTrace2 = feat3D.eigs[0] + feat3D.eigs[1] + feat3D.eigs[2];
	//float fDet2 = feat3D.eigs[0] * feat3D.eigs[1] * feat3D.eigs[2];

	return 0;
}

//
// msResampleFeaturesGradientOrientationHistogram()
//
// Resample as gradient orientation histograms.
//
int
msResampleFeaturesGradientOrientationHistogram(
	Feature3D &feat3D
)
{
	FEATUREIO fioImg;
	fioImg.x = fioImg.z = fioImg.y = Feature3D::FEATURE_3D_DIM;
	fioImg.pfVectors = &(feat3D.data_zyx[0][0][0]);
	fioImg.pfMeans = 0;
	fioImg.pfVarrs = 0;
	fioImg.iFeaturesPerVector = 1;
	fioImg.t = 1;

	// 1st derivative images - these could be passed in from the outside.
	FEATUREIO fioDx = fioImg;
	FEATUREIO fioDy = fioImg;
	FEATUREIO fioDz = fioImg;
	Feature3D fdx;
	Feature3D fdy;
	Feature3D fdz;
	fioDx.pfVectors = &(fdx.data_zyx[0][0][0]);
	fioDy.pfVectors = &(fdy.data_zyx[0][0][0]);
	fioDz.pfVectors = &(fdz.data_zyx[0][0][0]);

	fioGenerateEdgeImages3D(fioImg, fioDx, fioDy, fioDz);

	float fRadius = fioImg.x / 2;
	float fRadiusSqr = (fioImg.x / 2)*(fioImg.x / 2);

	int iSampleCount = 0;

	// Consider 8 orientation angles
#define GRAD_ORI_ORIBINS 8
	float pfOriAngles[GRAD_ORI_ORIBINS][3] =
	{
		{ 1,	 1,	 1 },
		{ 1,	 1,	-1 },
		{ 1,	-1,	 1 },
		{ 1,	-1,	-1 },
		{ -1,	 1,	 1 },
		{ -1,	 1,	-1 },
		{ -1,	-1,	 1 },
		{ -1,	-1,	-1 },
	};

	// Proportions for mixing
	//float fMixProportions[Feature3D::FEATURE_3D_DIM] = {1,	1,	1,	1,	1,	0.5,	1,	1,	1,	1,	1};

	// Determine dominant orientation of feature: azimuth/elevation
	//fioSet( fioImg, 0 );

	// Reference to grad orientation info
#define GRAD_ORI_SPACEBINS 2
	float fBinSize = Feature3D::FEATURE_3D_DIM / (float)GRAD_ORI_SPACEBINS;
	FEATUREIO fioImgGradOri;
	fioImgGradOri.x = fioImgGradOri.z = fioImgGradOri.y = 2;
	//fioImgGradOri.pfVectors = &(feat3D.data_zyx[0][0][0]);
	assert(Feature3DInfo::FEATURE_3D_PCS >= 64);
	fioImgGradOri.pfVectors = &(feat3D.m_pfPC[0]);
	fioImgGradOri.pfMeans = 0;
	fioImgGradOri.pfVarrs = 0;
	fioImgGradOri.iFeaturesPerVector = GRAD_ORI_ORIBINS;
	fioImgGradOri.t = 1;
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		float fZCoord = (int)(zz / fBinSize) + 0.5f;
		if ((int)((zz + 0) / fBinSize) != (int)((zz + 1) / fBinSize))
		{
			//float fIntPart;
			//int iNext = (int)((zz+1)/fBinSize);
			//float fFraction = fmodf( (float)(iNext*fBinSize), fIntPart);
			//fZCoord = iNext-1+fFraction+0.5;
			float fP0 = ((zz + 0) / fBinSize);
			float fP1 = ((zz + 1) / fBinSize);
			fZCoord = (fP0 + fP1) / 2.0f;
		}

		for (int yy = 0; yy < fioImg.y; yy++)
		{
			float fYCoord = (int)(yy / fBinSize) + 0.5f;
			if ((int)((yy + 0) / fBinSize) != (int)((yy + 1) / fBinSize))
			{
				//float fIntPart;
				//int iNext = (int)((yy+1)/fBinSize);
				//float fFraction = fmodf((float)(iNext*fBinSize), fIntPart);
				//fYCoord = iNext-1+fFraction+0.5;
				float fP0 = ((yy + 0) / fBinSize);
				float fP1 = ((yy + 1) / fBinSize);
				fYCoord = (fP0 + fP1) / 2.0f;
			}

			for (int xx = 0; xx < fioImg.x; xx++)
			{

				float fXCoord = (int)(xx / fBinSize) + 0.5f;
				if ((int)((xx + 0) / fBinSize) != (int)((xx + 1) / fBinSize))
				{
					float fIntPart;
					int iNext = (int)((xx + 1) / fBinSize);


					//float fFraction = fmodf((float)(iNext*fBinSize), fIntPart);
					float fP0 = ((xx + 0) / fBinSize);
					float fP1 = ((xx + 1) / fBinSize);
					fXCoord = (fP0 + fP1) / 2.0f; //iNext-1+fFraction+0.5;
				}

				float dz = zz - fioImg.z / 2;
				float dy = yy - fioImg.y / 2;
				float dx = xx - fioImg.x / 2;
				//if( dz*dz + dy*dy + dx*dx < fRadiusSqr ) {
				// Keep this sample

				float pfEdge[3];
				pfEdge[0] = fioGetPixel(fioDx, xx, yy, zz);
				pfEdge[1] = fioGetPixel(fioDy, xx, yy, zz);
				pfEdge[2] = fioGetPixel(fioDz, xx, yy, zz);
				float fEdgeMag = vec3D_mag(&pfEdge[0]);
				if (fEdgeMag > 0)
				{
					vec3D_norm_3d(pfEdge);

					// Find max orientation bin: this could be made more efficient ... 
					int iMaxDotIndex = 0;
					float fMaxDot = vec3D_dot_3d(&pfOriAngles[iMaxDotIndex][0], &pfEdge[0]);
					for (int k = 1; k < 8; k++)
					{
						float fDot = vec3D_dot_3d(&pfOriAngles[k][0], &pfEdge[0]);
						if (fDot > fMaxDot)
						{
							fMaxDot = fDot;
							iMaxDotIndex = k;
						}
					}

					//float fRadiusSqr = (xx-5)*(xx-5) + (yy-5)*(yy-5) + (zz-5)*(zz-5);
					//float fWeight = exp( -fRadiusSqr/80.0f );*fWeight
					fioIncPixelTrilinearInterp(fioImgGradOri, fXCoord, fYCoord, fZCoord, iMaxDotIndex, fEdgeMag);
				}

				//}
			}
		}
	}

	// Normalize the PC vector
	msNormalizeDataPositive(&(feat3D.m_pfPC[0]), Feature3DInfo::FEATURE_3D_PCS);

	// Normalize data, no subtracting mean
	//feat3D.NormalizeDataPositive();

	return 0;
}



//
// msResampleFeaturesGradientOrientationHistogram2()
//
// Resample as gradient orientation histograms.
// This function tries to use a different derivative operator to better estimate orientation.
// Try to use a Roberts Cross type operator to compute 8 direction histogram.
// Does not work - why??? For OASIS test images, 34 inliers vs. 2 ... here 4:13 PM 2/5/2013
//
int
msResampleFeaturesGradientOrientationHistogram2(
	Feature3D &feat3D
)
{
	FEATUREIO fioImg;
	fioImg.x = fioImg.z = fioImg.y = Feature3D::FEATURE_3D_DIM;
	fioImg.pfVectors = &(feat3D.data_zyx[0][0][0]);
	fioImg.pfMeans = 0;
	fioImg.pfVarrs = 0;
	fioImg.iFeaturesPerVector = 1;
	fioImg.t = 1;

	// Consider 8 orientation angles

	//float pfOriAngles[GRAD_ORI_ORIBINS][3] = 
	//{
	//	{ 1,	 1,	 1},
	//	{ 1,	 1,	-1},
	//	{ 1,	-1,	 1},
	//	{ 1,	-1,	-1},
	//	{-1,	 1,	 1},
	//	{-1,	 1,	-1},
	//	{-1,	-1,	 1},
	//	{-1,	-1,	-1},
	//};

	// Rework derivatives
	float pfOriAngles[GRAD_ORI_ORIBINS][3] =
	{
		{ 1,	 1,	 1 },
		{ 1,	 1,	 0 },
		{ 1,	 0,	 1 },
		{ 1,	 0,	 0 },
		{ 0,	 1,	 1 },
		{ 0,	 1,	 0 },
		{ 0,	 0,	 1 },
		{ 0,	 0,	 0 },
	};
	// Indices for computing derivatives,
	// Assuming pfOriAngles[] is in (z,y,x) order
	// Assming fioImg is 11x11x11 in size
	int piOriIndices[GRAD_ORI_ORIBINS] =
	{
		133	,
		132	,
		122	,
		121	,
		12	,
		11	,
		1	,
		0
	};

	// Reference to grad orientation info
#define GRAD_ORI_SPACEBINS 2
	float fBinSize = Feature3D::FEATURE_3D_DIM / (float)GRAD_ORI_SPACEBINS;
	FEATUREIO fioImgGradOri;
	fioImgGradOri.x = fioImgGradOri.z = fioImgGradOri.y = 2;
	//fioImgGradOri.pfVectors = &(feat3D.data_zyx[0][0][0]);
	assert(Feature3DInfo::FEATURE_3D_PCS >= 64);
	fioImgGradOri.pfVectors = &(feat3D.m_pfPC[0]);
	fioImgGradOri.pfMeans = 0;
	fioImgGradOri.pfVarrs = 0;
	fioImgGradOri.iFeaturesPerVector = GRAD_ORI_ORIBINS;
	fioImgGradOri.t = 1;

	// Note: derivatives are computed between neighbouring pixels,
	// so the derivative sample location is actually between the pixels.
	for (int zz = 0; zz < fioImg.z - 1; zz++)
	{
		for (int yy = 0; yy < fioImg.y - 1; yy++)
		{
			for (int xx = 0; xx < fioImg.x - 1; xx++)
			{
				float *pfOriHistVec = fioGetVector(fioImgGradOri, xx / 2, yy / 2, zz / 2);

				float fMaxEdge = -1;
				int iMaxIndex = -1;
				for (int k = 0; k < 4; k++)
				{
					// Compute edge magnitude: intensities on opposite ends of a 2x2x2 cube, centered on 0.5, 0.5, 0.5
					float fEdgeMag = fioImg.pfVectors[piOriIndices[k]] - fioImg.pfVectors[piOriIndices[7 - k]];
					//if( fEdgeMag > fMaxEdge )
					//{
					//	fMaxEdge = fEdgeMag;
					//	iMaxIndex = k;
					//}
					//else if( -fEdgeMag > fMaxEdge )
					//{
					//	fMaxEdge = -fEdgeMag;
					//	iMaxIndex = 7-k;
					//}

					// Keep all positive derivatives
					// Non-zero negative derivatives are added to other side.
					if (fEdgeMag > 0)
					{
						pfOriHistVec[k] += fEdgeMag;
					}
					else
					{
						pfOriHistVec[7 - k] -= fEdgeMag;
					}
				}
			}
		}
	}

	// Normalize the PC vector
	msNormalizeDataPositive(&(feat3D.m_pfPC[0]), Feature3DInfo::FEATURE_3D_PCS);

	// Normalize data, no subtracting mean
	//feat3D.NormalizeDataPositive();

	return 0;
}



bool
_sortFloatPairFirstAscending(
	const pair<float, pair<float, float> > &pf1,
	const pair<float, pair<float, float> > &pf2
)
{
	return pf1.first < pf2.first;
}

//
// msResampleFeaturesRotationInvariantHistogram()
//
// Resample as a rotation invariant gradient orientation histogram.
//
int
msResampleFeaturesRotationInvariantHistogram(
	Feature3D &feat3D
)
{
	FEATUREIO fioImg;
	fioImg.x = fioImg.z = fioImg.y = Feature3D::FEATURE_3D_DIM;
	fioImg.pfVectors = &(feat3D.data_zyx[0][0][0]);
	fioImg.pfMeans = 0;
	fioImg.pfVarrs = 0;
	fioImg.iFeaturesPerVector = 1;
	fioImg.t = 1;

	// 1st derivative images - these could be passed in from the outside.
	FEATUREIO fioDx = fioImg;
	FEATUREIO fioDy = fioImg;
	FEATUREIO fioDz = fioImg;
	Feature3D fdx;
	Feature3D fdy;
	Feature3D fdz;
	fioDx.pfVectors = &(fdx.data_zyx[0][0][0]);
	fioDy.pfVectors = &(fdy.data_zyx[0][0][0]);
	fioDz.pfVectors = &(fdz.data_zyx[0][0][0]);

	fioGenerateEdgeImages3D(fioImg, fioDx, fioDy, fioDz);

	float fRadius = fioImg.x / 2;
	float fRadiusSqr = (fioImg.x / 2)*(fioImg.x / 2);

	int iSampleCount = 0;

	// Create list of intensity/orientation/magnitude pairs
	vector< pair<float, pair<float, float> > > vecIntensityOrientationPairs;
	//vecIntensityOrientationPairs.resize( GRAD_ORI_ORIBINS*GRAD_ORI_ORIBINS );

	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				float dx = xx - fioImg.x / 2;
				float dy = yy - fioImg.y / 2;
				float dz = zz - fioImg.z / 2;
				float fLocRadiusSqr = dz*dz + dy*dy + dx*dx;
				if (fLocRadiusSqr > fRadiusSqr || fLocRadiusSqr <= 0)
				{
					// Only consider samples within sphere
					continue;
				}
				float pfEdge[3];
				pfEdge[0] = fioGetPixel(fioDx, xx, yy, zz);
				pfEdge[1] = fioGetPixel(fioDy, xx, yy, zz);
				pfEdge[2] = fioGetPixel(fioDz, xx, yy, zz);
				float fEdgeMag = vec3D_mag(&pfEdge[0]);
				if (fEdgeMag <= 0)
				{
					// No sample
					continue;
				}
				iSampleCount++;
			}
		}
	}
	vecIntensityOrientationPairs.resize(iSampleCount);
	iSampleCount = 0;

	assert(Feature3DInfo::FEATURE_3D_PCS >= 64);
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				float dx = xx - fioImg.x / 2;
				float dy = yy - fioImg.y / 2;
				float dz = zz - fioImg.z / 2;
				float fLocRadiusSqr = dz*dz + dy*dy + dx*dx;
				if (fLocRadiusSqr > fRadiusSqr || fLocRadiusSqr <= 0)
				{
					// Only consider samples within sphere
					continue;
				}

				float pfPos[3];
				pfPos[0] = dx;
				pfPos[1] = dy;
				pfPos[2] = dz;
				vec3D_norm_3d(pfPos);

				float pfEdge[3];
				pfEdge[0] = fioGetPixel(fioDx, xx, yy, zz);
				pfEdge[1] = fioGetPixel(fioDy, xx, yy, zz);
				pfEdge[2] = fioGetPixel(fioDz, xx, yy, zz);
				float fEdgeMag = vec3D_mag(&pfEdge[0]);
				if (fEdgeMag <= 0)
				{
					// No sample
					continue;
				}
				vec3D_norm_3d(pfEdge);

				float fDot = vec3D_dot_3d(&pfPos[0], &pfEdge[0]);
				float fAngle = acos(fDot);
				float fIntensity = fioGetPixel(fioImg, xx, yy, zz);
				vecIntensityOrientationPairs[iSampleCount] = pair<float, pair<float, float> >(fIntensity, pair<float, float>(fAngle, fEdgeMag));
				iSampleCount++;
			}
		}
	}

	// Sort
	sort(vecIntensityOrientationPairs.begin(), vecIntensityOrientationPairs.end(), _sortFloatPairFirstAscending);

	memset(&feat3D.m_pfPC[0], 0, sizeof(feat3D.m_pfPC));

	// Create histogram: 8 intensity bins, 8 orientation bins.
	for (int i = 0; i < vecIntensityOrientationPairs.size(); i++)
	{
		float fAngle = (vecIntensityOrientationPairs[i].second.first * 8) / (float)PI;
		int iAngleBin = fAngle;
		if (iAngleBin >= 8)
		{
			iAngleBin = 7;
		}

		float fIntensityBin = (i * 8) / (float)(vecIntensityOrientationPairs.size() + 1.0f);
		int iIntensityBin = fIntensityBin;

		feat3D.m_pfPC[8 * iIntensityBin + iAngleBin] += vecIntensityOrientationPairs[i].second.second;
	}

	// Normalize the PC vector
	msNormalizeDataPositive(&(feat3D.m_pfPC[0]), Feature3DInfo::FEATURE_3D_PCS);

	// Normalize data, no subtracting mean
	//feat3D.NormalizeDataPositive();

	return 0;
}



//
// determineCanonicalOrientation3D()
//
// Determine canonical orientation of a 3D feature patch. Use cube method, to avoid angular estimation problems.
// Estimate N primary rotations, then for each, estimate the strongest secondary rotation.
//
int
determineCanonicalOrientation3D(
	Feature3D &feat3D,
	float *pfOri, // Array to store iMaxOri 3D rotation arrays. Each rotation array is
				  //			encoded as three 3D unit vectors
	int iMaxOri // Number of 6-float values available in pfOri
)
{
	FEATUREIO fioImg;
	fioImg.x = fioImg.z = fioImg.y = Feature3D::FEATURE_3D_DIM;
	fioImg.pfVectors = &(feat3D.data_zyx[0][0][0]);
	fioImg.pfMeans = 0;
	fioImg.pfVarrs = 0;
	fioImg.iFeaturesPerVector = 1;
	fioImg.t = 1;

	// 1st derivative images - these could be passed in from the outside.
	FEATUREIO fioDx = fioImg;
	FEATUREIO fioDy = fioImg;
	FEATUREIO fioDz = fioImg;
	Feature3D fdx;
	Feature3D fdy;
	Feature3D fdz;
	fioDx.pfVectors = &(fdx.data_zyx[0][0][0]);
	fioDy.pfVectors = &(fdy.data_zyx[0][0][0]);
	fioDz.pfVectors = &(fdz.data_zyx[0][0][0]);

	// Angles and blurred versions
	FEATUREIO fioT0 = fioImg;
	FEATUREIO fioT1 = fioImg;
	FEATUREIO fioT2 = fioImg;
	Feature3D fT0;
	Feature3D fT1;
	Feature3D fT2;
	fioT0.pfVectors = &(fT0.data_zyx[0][0][0]);
	fioT1.pfVectors = &(fT1.data_zyx[0][0][0]);
	fioT2.pfVectors = &(fT2.data_zyx[0][0][0]);
	fioSet(fioT0, 0);

	// Major peaks
	LOCATION_VALUE_XYZ_ARRAY lvaPeaks;
	LOCATION_VALUE_XYZ lvData[ORIBINS*ORIBINS / 8];
	float pfOriData[ORIBINS*ORIBINS / 8];
	lvaPeaks.plvz = &(lvData[0]);

	fioGenerateEdgeImages3D(fioImg, fioDx, fioDy, fioDz);


	float fRadius = fioImg.x / 2;
	float fRadiusSqr = (fioImg.x / 2)*(fioImg.x / 2);

	int iSampleCount = 0;

	// Determine dominant orientation of feature: azimuth/elevation
	for (int zz = 0; zz < fioImg.z; zz++)
	{
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				float dz = zz - fioImg.z / 2;
				float dy = yy - fioImg.y / 2;
				float dx = xx - fioImg.x / 2;
				if (dz*dz + dy*dy + dx*dx < fRadiusSqr)
				{
					// Keep this sample
					float pfEdge[3];
					pfEdge[0] = fioGetPixel(fioDx, xx, yy, zz);
					pfEdge[1] = fioGetPixel(fioDy, xx, yy, zz);
					pfEdge[2] = fioGetPixel(fioDz, xx, yy, zz);
					float fEdgeMagSqr = pfEdge[0] * pfEdge[0] + pfEdge[1] * pfEdge[1] + pfEdge[2] * pfEdge[2];
					if (fEdgeMagSqr == 0)
					{
						continue;
					}
					float fEdgeMag = sqrt(fEdgeMagSqr);

					iSampleCount++;

					// Vector length fRadius in direction of edge
					float pfEdgeUnit[3];
					for (int i = 0; i < 3; i++)
					{
						pfEdgeUnit[i] = pfEdge[i] * fRadius / fEdgeMag;
					}
					for (int i = 0; i < 3; i++)
					{
						pfEdgeUnit[i] += fRadius;
					}

					// Add edge magnitude vote to pixel
					//   - this should be done via bilinear_interpolation_paint_float
					//float *pfPix = fioGetVector( fioT0, pfEdgeUnit[0]+0.5, pfEdgeUnit[1]+0.5, pfEdgeUnit[2]+0.5 );
					//*pfPix += fEdgeMag;

					fioIncPixelTrilinearInterp(fioT0, pfEdgeUnit[0] + 0.5, pfEdgeUnit[1] + 0.5, pfEdgeUnit[2] + 0.5, 0, fEdgeMag);
				}
			}
		}
	}

	iSampleCount = 0;
	for (int i = 0; i < 11 * 11 * 11; i++)
	{
		if (fioT0.pfVectors[i] > 0)
		{
			iSampleCount++;
		}
	}

	PpImage ppImgRef;
	char pcFileName[400];

	int iCode = 1;

	sprintf(pcFileName, "bin%5.5d_dx", iCode);
	//fdx.OutputVisualFeature( pcFileName );
	sprintf(pcFileName, "bin%5.5d_dy", iCode);
	//fdy.OutputVisualFeature( pcFileName );
	sprintf(pcFileName, "bin%5.5d_dz", iCode);
	//fdz.OutputVisualFeature( pcFileName );

	gb3d_blur3d(fioT0, fioT1, fioT2, fBlurGradOriHist, 0.01);

	regFindFEATUREIOPeaks(lvaPeaks, fioT2);
	lvSortHighLow(lvaPeaks);
	sprintf(pcFileName, "bin%5.5d_orig", iCode);
	//fioWrite( fioT0, pcFileName );
	//fT0.OutputVisualFeature( pcFileName );

	sprintf(pcFileName, "bin%5.5d_blur", iCode);
	//fioWrite( fioT2, pcFileName );
	//fT2.OutputVisualFeature( pcFileName );


	float pfP1[3]; //z
	float pfP2[3]; //y
	float pfP3[3]; //x

				   // determine sub-pixel orientation vectors
	for (int i = 0; i < lvaPeaks.iCount && i < fioImg.z && i < iMaxOri; i++)
	{
		float *pfOriCurr = &pfOriData[i * 3];

		// Interpolate
		interpolate_discrete_3D_point(fioT2,
			lvaPeaks.plvz[i].x, lvaPeaks.plvz[i].y, lvaPeaks.plvz[i].z,
			pfOriCurr[0], pfOriCurr[1], pfOriCurr[2]);

		// Subtract radius/image center of fioT2
		pfOriCurr[0] -= fRadius;
		pfOriCurr[1] -= fRadius;
		pfOriCurr[2] -= fRadius;

		// Normalize to unit length
		vec3D_norm_3d(pfOriCurr);
	}

	int iOrientationsReturned = 0;

	// Set my descriptor to 0
	fioSet(fioImg, 0);
	LOCATION_VALUE_XYZ_ARRAY lvaPeaks2;
	LOCATION_VALUE_XYZ lvData2[ORIBINS*ORIBINS / 8];
	lvaPeaks2.plvz = &(lvData2[0]);
	for (int i = 0; i < lvaPeaks.iCount && i < fioImg.z && iOrientationsReturned < iMaxOri; i++)
	{
		//if( lvaPeaks.plvz[i].fValue < fHist2ndPeakThreshold*lvaPeaks.plvz[0].fValue )
		if (lvaPeaks.plvz[i].fValue < 0.8*lvaPeaks.plvz[0].fValue)
			//if( lvaPeaks.plvz[i].fValue < 0.4*lvaPeaks.plvz[0].fValue )
		{
			// Must be above threshold
			// 0.5 used in preliminary matching experiments
			// 0.8 works well to, 0.2 is terrible...
			break;
		}

		// Primary orientation unit vector
		// Used interpolated vectors here for correctnewss

		float *pfOriCurr = &pfOriData[i * 3];
		//pfP1[0] = lvaPeaks.plvz[i].x-fRadius;
		//pfP1[1] = lvaPeaks.plvz[i].y-fRadius;
		//pfP1[2] = lvaPeaks.plvz[i].z-fRadius;
		//vec3D_norm_3d( pfP1 );
		pfP1[0] = pfOriCurr[0];
		pfP1[1] = pfOriCurr[1];
		pfP1[2] = pfOriCurr[2];

		// This was just for testing
		//feat3D.data_zyx[0][i][0] = pfP1[0];
		//feat3D.data_zyx[0][i][1] = pfP1[1];
		//feat3D.data_zyx[0][i][2] = pfP1[2];

		// Compute secondary direction unit vector
		// perpendicular to primary

		fioSet(fioT0, 0);
		for (int zz = 0; zz < fioImg.z; zz++)
		{
			for (int yy = 0; yy < fioImg.y; yy++)
			{
				for (int xx = 0; xx < fioImg.x; xx++)
				{
					float dx = xx - fioImg.x / 2;
					float dy = yy - fioImg.y / 2;
					float dz = zz - fioImg.z / 2;
					float fLocRadiusSqr = dz*dz + dy*dy + dx*dx;
					if (fLocRadiusSqr < fRadiusSqr)
					{
						// Vector: intensity edge
						float pfEdge[3];
						float pfEdgeUnit[3];
						pfEdge[0] = fioGetPixel(fioDx, xx, yy, zz);
						pfEdge[1] = fioGetPixel(fioDy, xx, yy, zz);
						pfEdge[2] = fioGetPixel(fioDz, xx, yy, zz);
						float fEdgeMag = vec3D_mag(pfEdge);
						if (fEdgeMag == 0)
						{
							continue;
						}
						memcpy(pfEdgeUnit, pfEdge, sizeof(pfEdgeUnit));
						vec3D_norm_3d(pfEdgeUnit);

						// Remove component parallel to primary orientatoin
						float pVecPerp[3];
						float fParallelMag = vec3D_dot_3d(pfP1, pfEdgeUnit);
						pVecPerp[0] = pfEdgeUnit[0] - fParallelMag*pfP1[0];
						pVecPerp[1] = pfEdgeUnit[1] - fParallelMag*pfP1[1];
						pVecPerp[2] = pfEdgeUnit[2] - fParallelMag*pfP1[2];

						// Normalize
						vec3D_norm_3d(pVecPerp);

						// Vector length fRadius in direction of edge, centered in image
						for (int i = 0; i < 3; i++)
						{
							pVecPerp[i] *= fRadius;
							pVecPerp[i] += fRadius;
						}

						//bilinear_interpolation_paint_float
						// Add pixel
						//float *pfPix = fioGetVector( fioT0, pVecPerp[0]+0.5, pVecPerp[1]+0.5, pVecPerp[2]+0.5 );
						//*pfPix += fEdgeMag;
						fioIncPixelTrilinearInterp(fioT0, pVecPerp[0] + 0.5, pVecPerp[1] + 0.5, pVecPerp[2] + 0.5, 0, fEdgeMag);

					}
				}
			}
		}

		// Blur, find peaks
		gb3d_blur3d(fioT0, fioT1, fioT2, fBlurGradOriHist, 0.01);
		regFindFEATUREIOPeaks(lvaPeaks2, fioT2);
		lvSortHighLow(lvaPeaks2);
		sprintf(pcFileName, "bin%5.5d_orig", iCode);

		//
		//
		//

		// determine sub-pixel orientation vectors
		for (int j = 0; j < lvaPeaks2.iCount && iOrientationsReturned < fioImg.z && iOrientationsReturned < iMaxOri; j++)
		{
			if (lvaPeaks2.plvz[j].fValue < fHist2ndPeakThreshold*lvaPeaks2.plvz[0].fValue)
			{
				// fHist2ndPeakThreshold = 0.5 works well
				// Must be above threshold
				break;
			}

			//		if( lvaPeaks2.iCount > 0 )
			//		{
			// 2nd dominant orientation found, keep only first
			pfP2[0] = lvaPeaks2.plvz[j].x - fRadius;
			pfP2[1] = lvaPeaks2.plvz[j].y - fRadius;
			pfP2[2] = lvaPeaks2.plvz[j].z - fRadius;
			vec3D_norm_3d(pfP2);

			// Interpolate
			interpolate_discrete_3D_point(fioT2,
				lvaPeaks2.plvz[j].x, lvaPeaks2.plvz[j].y, lvaPeaks2.plvz[j].z,
				pfP2[0], pfP2[1], pfP2[2]);

			// Subtract radius/image center of fioT2
			pfP2[0] -= fRadius;
			pfP2[1] -= fRadius;
			pfP2[2] -= fRadius;

			// Normalize to unit length
			vec3D_norm_3d(pfP2);

			// Enforce to be perpendicular to pfP1
			float pVecPerp[3];
			float fParallelMag = vec3D_dot_3d(pfP1, pfP2);
			assert(fabs(fParallelMag) < 0.5f);
			pfP2[0] = pfP2[0] - fParallelMag*pfP1[0];
			pfP2[1] = pfP2[1] - fParallelMag*pfP1[1];
			pfP2[2] = pfP2[2] - fParallelMag*pfP1[2];
			// Re-normalize to unit length
			vec3D_norm_3d(pfP2);

			//assert( vec3D_dot_3d( pfP1, pfP2 ) <= fabs( fParallelMag ) );



			// Ensure 

			// 3rd dominant orientation is cross product
			vec3D_cross_3d(pfP1, pfP2, pfP3);

			// Save vectors 
			// *Note* orientation vectors saved along columns, not rows :p :p :p
			float *pfOriMatrix = pfOri + 9 * iOrientationsReturned;
			for (int ivec = 0; ivec < 3; ivec++)
			{
				pfOriMatrix[0 * 3 + ivec] = pfP1[ivec];
				pfOriMatrix[1 * 3 + ivec] = pfP2[ivec];
				pfOriMatrix[2 * 3 + ivec] = pfP3[ivec];
			}

			iOrientationsReturned++;
		}

		//else
		//{
		//	pfP2[0] = 0;
		//	pfP2[1] = 0; 
		//	pfP2[2] = 0;
		//}

		// Save major peak
		//feat3D.data_zyx[0][i][3] = pfP2[0];
		//feat3D.data_zyx[0][i][4] = pfP2[1];
		//feat3D.data_zyx[0][i][5] = pfP2[2];
	}
	// return - this is just for testing
	return iOrientationsReturned;
}

//
// determineCanonicalOrientation3DUnified()
//
// Selecting 2nd dominant peak from original peaks.
// This function sucked so badly I just erased it all ... 
// it is apparently very important to consider 2ndardy orientations in the plane
// perpendicular to the 1st orientation, as above ... 
//
int
determineCanonicalOrientation3DUnified(
	Feature3D &feat3D,
	float *pfOri, // Array to store iMaxOri 3D rotation arrays. Each rotation array is
				  //			encoded as three 3D unit vectors
	int iMaxOri // Number of 6-float values available in pfOri
) {
	return -1;
}



float g_fMaxPixel;
float g_fMinPixel;
int   g_iContrastReject = 0;

//
// generateFeature3D()
//
// Determine orientation parameters for feat3D, based on ocation & scale
// of feat3D.
//
int
generateFeature3D(
	Feature3D &feat3D, // Feature geometrical information
	FEATUREIO &fioSample, // Sub-image sample
	FEATUREIO &fioImg, // Original image
	FEATUREIO &fioDx, // Derivative images
	FEATUREIO &fioDy,
	FEATUREIO &fioDz,
	vector<Feature3D> &vecFeats,
	float fEigThres,// = -1,// Threshold on eigenvalues, discard if
	int bReorientedFeatures// = 1 Wether to reorient features or not, default yes (1)
)
{
	// Determine feature orientation - to be scale-consistent,
	// derivatives should be calculated on rescaled image ... 
	//   ..and never again will I calculate derivatives on a non-scaled image ...
	//if( determineOrientation3D( feat3D, fioDx, fioDy, fioDz ) != 0 )
	//{
	//	return -1;
	//}

	// Determine feature image content - init rotation to identity
	memset(&(feat3D.ori[0][0]), 0, sizeof(feat3D.ori));
	feat3D.ori[0][0] = 1; feat3D.ori[1][1] = 1; feat3D.ori[2][2] = 1;
	if (sampleImage3D(feat3D, fioSample, fioImg, fioDx, fioDy, fioDz) != 0)
	{
		return -1;
	}

	float fMaxPixel = fioGetPixel(fioSample, 0, 0, 0);
	float fMinPixel = fioGetPixel(fioSample, 0, 0, 0);
	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				feat3D.data_zyx[zz][yy][xx] = fioGetPixel(fioSample, xx, yy, zz);
				//if( feat3D.data_zyx[zz][yy][xx] > fMaxPixel )
				//{
				//	fMaxPixel = feat3D.data_zyx[zz][yy][xx];
				//}
				//if( feat3D.data_zyx[zz][yy][xx] < fMinPixel )
				//{
				//	fMinPixel = feat3D.data_zyx[zz][yy][xx];
				//}
			}
		}
	}

	//// Insufficient contrast
	//if( 1000*(fMaxPixel-fMinPixel) < (g_fMaxPixel-g_fMinPixel) )
	//{
	//	g_iContrastReject++;
	//	//return -1;
	//}

	feat3D.NormalizeData();

	//// Determine feature eigenorientation
	if (determineOrientation3D(feat3D))
	{
		return -1;
	}

	float fEigSum = feat3D.eigs[0] + feat3D.eigs[1] + feat3D.eigs[2];
	float fEigPrd = feat3D.eigs[0] * feat3D.eigs[1] * feat3D.eigs[2];
	float fEigSumProd = fEigSum*fEigSum*fEigSum;

	static float fMaxRatio = -1;
	static float fMinRatio = 2;
	float fRatio = (27.0f*fEigPrd) / fEigSumProd;
	if (fRatio > fMaxRatio)
	{
		fMaxRatio = fRatio;
	}
	if (fRatio < fMinRatio)
	{
		fMinRatio = fRatio;
	}

	if (fEigSumProd < fEigThres*fEigPrd || fEigThres < 0)
	{
	}
	else
	{
		return -1;
	}

	// Flag as not reoriented
	feat3D.m_uiInfo &= ~INFO_FLAG_REORIENT;
	vecFeats.push_back(feat3D);

	if (!bReorientedFeatures)
	{
		return 0;
	}

	//PRINT_ONCE( "\nNo reorientation, only 1 orientation\n" );
	//return 0;

	//#define INCLUDE_EIGENORIENTATION_FEATURE
#ifdef INCLUDE_EIGENORIENTATION_FEATURE

	// Sample feature eigenorientation
	if (sampleImage3D(feat3D, fioSample, fioImg, fioDx, fioDy, fioDz) != 0)
	{
		return -1;
	}
	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				feat3D.data_zyx[zz][yy][xx] = fioGetPixel(fioSample, xx, yy, zz);
			}
		}
	}

	//msResampleFeaturesGradientOrientationHistogram( feat3D );
	vecFeats.push_back(feat3D);
	//return 0;

	// *******************************
	// // Determine feature image content - init rotation to identity
	memset(&(feat3D.ori[0][0]), 0, sizeof(feat3D.ori));
	feat3D.ori[0][0] = 1; feat3D.ori[1][1] = 1; feat3D.ori[2][2] = 1;
	if (sampleImage3D(feat3D, fioSample, fioImg, fioDx, fioDy, fioDz) != 0)
	{
		return -1;
	}
	// Copy sampled data
	for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
	{
		for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
		{
			for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
			{
				feat3D.data_zyx[zz][yy][xx] = fioGetPixel(fioSample, xx, yy, zz);
			}
		}
	}
	//vecFeats.push_back( feat3D );
	//return 0;

	//	output_feature_in_volume( fioImg, (int)feat3D.scale, feat3D.x, feat3D.y, feat3D.z, "original", 1 );
	//	output_feature_in_volume( fioSample, (int)feat3D.scale, fioSample.x/2, fioSample.y/2, fioSample.z/2, "feature" );

	// *** Original code ended here - let's test
	//vecFeats.push_back( feat3D );
	//return 0;

#endif

	// There are multiple orientations, create feature for each
	// Pass in enough room for 30 rotation matrices (each consists of three 3D unit vectors)
	float pfOri[30 * 9];
	float iMaxOri = 30;
	int iOrientationsFound = determineCanonicalOrientation3D(feat3D, pfOri, iMaxOri);
	for (int iOri = 0; iOri < iOrientationsFound; iOri++)
	{
		// Copy orientation vector
		float *pfOriStart = pfOri + 9 * iOri;
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				feat3D.ori[i][j] = pfOriStart[i * 3 + j];
			}
		}

		// Resample image with new orientation
		if (sampleImage3D(feat3D, fioSample, fioImg, fioDx, fioDy, fioDz) != 0)
		{
			continue;
		}

		// Copy sampled data to feature
		for (int zz = 0; zz < Feature3D::FEATURE_3D_DIM; zz++)
		{
			for (int yy = 0; yy < Feature3D::FEATURE_3D_DIM; yy++)
			{
				for (int xx = 0; xx < Feature3D::FEATURE_3D_DIM; xx++)
				{
					feat3D.data_zyx[zz][yy][xx] = fioGetPixel(fioSample, xx, yy, zz);
				}
			}
		}

		// Save to vector
		//msResampleFeaturesGradientOrientationHistogram( feat3D );

		// Flag as reoriented
		feat3D.m_uiInfo |= INFO_FLAG_REORIENT;
		vecFeats.push_back(feat3D);
	}

	return 0;
}



//
// interpolate_peak_quadratic()
//
// Interpolate an extremum (peak or valley) in a function f(x). f(x1) is the uninterpolated extremum.
//
double finddet(double a1, double a2, double a3, double b1, double b2, double b3, double c1, double c2, double c3)
{
	return ((a1*b2*c3) - (a1*b3*c2) - (a2*b1*c3) + (a3*b1*c2) + (a2*b3*c1) - (a3*b2*c1)); /*expansion of a 3x3 determinant*/
}
double
interpolate_extremum_quadratic(
	double x0, // Three coordinates x
	double x1,
	double x2,
	double fx0, // Three functions of coordinates f(x)
	double fx1,
	double fx2
)
{
	// fx1 must be either a minimum or a maximum
	if (!(fx1 < fx0 && fx1 < fx2) && !(fx1 > fx0 && fx1 > fx2))
	{
		printf("%f\t%f\t%f\n", fx0, fx1, fx2);
		assert(0);
		return x1;
	}

	double a1 = x0*x0, b1 = x0, c1 = 1;
	double a2 = x1*x1, b2 = x1, c2 = 1;
	double a3 = x2*x2, b3 = x2, c3 = 1;
	double d1 = fx0, d2 = fx1, d3 = fx2;
	double det, detx, dety, detz;

	det = finddet(a1, a2, a3, b1, b2, b3, c1, c2, c3);   /*Find determinants*/
	detx = finddet(d1, d2, d3, b1, b2, b3, c1, c2, c3);
	dety = finddet(a1, a2, a3, d1, d2, d3, c1, c2, c3);
	detz = finddet(a1, a2, a3, b1, b2, b3, d1, d2, d3);

	if (d1 == 0 && d2 == 0 && d3 == 0 && det == 0)
	{
		printf("\n Infinite Solutions\n ");
	}
	else if (d1 == 0 && d2 == 0 && d3 == 0 && det != 0)
	{
		printf("\n x=0\n y=0, \n z=0\n ");
	}
	else if (det != 0)
	{
		//printf("\n x=%lf\n y=%lf\n z=%lf\n", (detx/det), (dety/det), (detz/det));
		if (detx != 0)
		{
			// Valid interpolated value
			return dety / (-2.0*detx);
		}
	}
	else if (det == 0 && detx == 0 && dety == 0 && detz == 0)
	{
		printf("\n Infinite Solutions\n ");
	}
	else
	{
		printf("No Solution\n ");
	}

	// Return x1 (uninterpolated peak) by default
	return x1;
}

void
interpolate_discrete_3D_point(
	FEATUREIO &fioC,	// Center image: for interpolating feature geometry
	int ix, int iy, int iz,
	float &fx, float &fy, float &fz
)
{
	fx = interpolate_extremum_quadratic(
		ix - 1, ix, ix + 1,
		fioGetPixel(fioC, ix - 1, iy, iz),
		fioGetPixel(fioC, ix, iy, iz),
		fioGetPixel(fioC, ix + 1, iy, iz)
	);
	fy = interpolate_extremum_quadratic(
		iy - 1, iy, iy + 1,
		fioGetPixel(fioC, ix, iy - 1, iz),
		fioGetPixel(fioC, ix, iy, iz),
		fioGetPixel(fioC, ix, iy + 1, iz)
	);
	fz = interpolate_extremum_quadratic(
		iz - 1, iz, iz + 1,
		fioGetPixel(fioC, ix, iy, iz - 1),
		fioGetPixel(fioC, ix, iy, iz),
		fioGetPixel(fioC, ix, iy, iz + 1)
	);
}

//
// generateFeatures3D()
//
// Generate a vector of features for all minima/maxima. Essentially,
// set location, scale & orientation parameters, then store in vector.
//
int
generateFeatures3D(
	LOCATION_VALUE_XYZ_ARRAY	&lvaMinima,
	LOCATION_VALUE_XYZ_ARRAY	&lvaMaxima,

	FEATUREIO &fioH,	// Hi res image
	FEATUREIO &fioC,	// Center image: for interpolating feature geometry
	FEATUREIO &fioL,	// Lo res image

	float fScaleH,	// Scale factors: for interpolating feature geometry
	float fScaleC,
	float fScaleL,

	FEATUREIO &fioImg,
	FEATUREIO &fioDx,
	FEATUREIO &fioDy,
	FEATUREIO &fioDz,
	float fScale,
	vector<Feature3D> &vecFeats,
	float fEigThres = -1
)
{
	Feature3D feat3D;
	FEATUREIO featSample;
	featSample.x = featSample.y = featSample.z = Feature3D::FEATURE_3D_DIM;
	featSample.t = 1;
	featSample.iFeaturesPerVector = 1;
	fioAllocate(featSample);

	int bInterpolate = 1;

	for (int i = 0; i < lvaMinima.iCount; i++)
	{
		if (!bInterpolate)
		{
			feat3D.x = lvaMinima.plvz[i].x;
			feat3D.y = lvaMinima.plvz[i].y;
			feat3D.z = lvaMinima.plvz[i].z;
			feat3D.scale = 2 * fScale;
		}
		else
		{
			interpolate_discrete_3D_point(
				fioC,
				lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, lvaMinima.plvz[i].z,
				feat3D.x, feat3D.y, feat3D.z);
			feat3D.scale = 2 * interpolate_extremum_quadratic(
				fScaleH, fScaleC, fScaleL,
				fioGetPixel(fioH, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, lvaMinima.plvz[i].z),
				fioGetPixel(fioC, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, lvaMinima.plvz[i].z),
				fioGetPixel(fioL, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, lvaMinima.plvz[i].z)
			);
		}
		// Convert pixel locations to subpixel precision
		feat3D.x += 0.5f;
		feat3D.y += 0.5f;
		feat3D.z += 0.5f;

		feat3D.m_uiInfo &= ~INFO_FLAG_MIN0MAX1;
		generateFeature3D(feat3D, featSample, fioImg, fioDx, fioDy, fioDz, vecFeats, fEigThres);
	}
	for (int i = 0; i < lvaMaxima.iCount; i++)
	{

		if (!bInterpolate)
		{
			feat3D.x = lvaMaxima.plvz[i].x;
			feat3D.y = lvaMaxima.plvz[i].y;
			feat3D.z = lvaMaxima.plvz[i].z;
			feat3D.scale = 2 * fScale;
		}
		else
		{
			interpolate_discrete_3D_point(
				fioC,
				lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].z,
				feat3D.x, feat3D.y, feat3D.z);
			feat3D.scale = 2 * interpolate_extremum_quadratic(
				fScaleH, fScaleC, fScaleL,
				fioGetPixel(fioH, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].z),
				fioGetPixel(fioC, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].z),
				fioGetPixel(fioL, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].z)
			);
		}
		// Convert pixel locations to subpixel precision
		feat3D.x += 0.5f;
		feat3D.y += 0.5f;
		feat3D.z += 0.5f;

		feat3D.m_uiInfo |= INFO_FLAG_MIN0MAX1;
		generateFeature3D(feat3D, featSample, fioImg, fioDx, fioDy, fioDz, vecFeats, fEigThres);
	}
	fioDelete(featSample);
	return 0;
}

//
// generateFeatures3D_efficient()
//
// 
//
int
generateFeatures3D_efficient(
	LOCATION_VALUE_XYZ_ARRAY	&lvaMinima,
	LOCATION_VALUE_XYZ_ARRAY	&lvaMaxima,

	vector<float> &vecMinH,
	vector<float> &vecMinL,
	vector<float> &vecMaxH,
	vector<float> &vecMaxL,

	FEATUREIO &fioC,	// Center image: for interpolating feature geometry

	float fScaleH,	// Scale factors: for interpolating feature geometry
	float fScaleC,
	float fScaleL,

	FEATUREIO &fioImg,

	float fScale,	// Equals fScaleC
	vector<Feature3D> &vecFeats,
	float fEigThres = -1
)
{
	Feature3D feat3D;
	FEATUREIO featSample;
	featSample.x = featSample.y = featSample.z = Feature3D::FEATURE_3D_DIM;
	featSample.t = 1;
	featSample.iFeaturesPerVector = 1;
	fioAllocate(featSample);


	FEATUREIO featJunk; memset(&featJunk, 0, sizeof(featJunk));

	int bInterpolate = 1;

	for (int i = 0; i < lvaMinima.iCount; i++)
	{
		if (!bInterpolate)
		{
			feat3D.x = lvaMinima.plvz[i].x;
			feat3D.y = lvaMinima.plvz[i].y;
			feat3D.z = lvaMinima.plvz[i].z;
			feat3D.scale = 2 * fScale;
		}
		else
		{
			interpolate_discrete_3D_point(
				fioC,
				lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, lvaMinima.plvz[i].z,
				feat3D.x, feat3D.y, feat3D.z);
			feat3D.scale = 2 * interpolate_extremum_quadratic(
				fScaleH, fScaleC, fScaleL,
				vecMinH[i],//fioGetPixel( fioH, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, lvaMinima.plvz[i].z ),
				fioGetPixel(fioC, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, lvaMinima.plvz[i].z),
				vecMinL[i]//fioGetPixel( fioL, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, lvaMinima.plvz[i].z )
			);
		}
		// Convert pixel locations to subpixel precision
		feat3D.x += 0.5f;
		feat3D.y += 0.5f;
		feat3D.z += 0.5f;

		feat3D.m_uiInfo &= ~INFO_FLAG_MIN0MAX1;
		generateFeature3D(feat3D, featSample, fioImg, featJunk, featJunk, featJunk, vecFeats, fEigThres);
	}
	for (int i = 0; i < lvaMaxima.iCount; i++)
	{

		if (!bInterpolate)
		{
			feat3D.x = lvaMaxima.plvz[i].x;
			feat3D.y = lvaMaxima.plvz[i].y;
			feat3D.z = lvaMaxima.plvz[i].z;
			feat3D.scale = 2 * fScale;
		}
		else
		{
			interpolate_discrete_3D_point(
				fioC,
				lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].z,
				feat3D.x, feat3D.y, feat3D.z);
			feat3D.scale = 2 * interpolate_extremum_quadratic(
				fScaleH, fScaleC, fScaleL,
				vecMaxH[i],//fioGetPixel( fioH, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].z ),
				fioGetPixel(fioC, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].z),
				vecMaxL[i]//fioGetPixel( fioL, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].z )
			);
		}
		// Convert pixel locations to subpixel precision
		feat3D.x += 0.5f;
		feat3D.y += 0.5f;
		feat3D.z += 0.5f;

		feat3D.m_uiInfo |= INFO_FLAG_MIN0MAX1;
		generateFeature3D(feat3D, featSample, fioImg, featJunk, featJunk, featJunk, vecFeats, fEigThres);
	}
	fioDelete(featSample);
	return 0;
}

//
// generateFeatures3D()
//
// Doesn't consider image data, strictly outputs max
// This version is designed for multi-modal feature selection and registration,
// here we output prior histograms to be used in registration.
//
int
generateFeatures3D(
	LOCATION_VALUE_XYZ_ARRAY	&lvaMinima,
	LOCATION_VALUE_XYZ_ARRAY	&lvaMaxima,

	FEATUREIO &fioH,	// Hi res image
	FEATUREIO &fioC,	// Center image: for interpolating feature geometry
	FEATUREIO &fioL,	// Lo res image

	float fScaleH,	// Scale factors: for interpolating feature geometry
	float fScaleC,
	float fScaleL,
	float fScale,

	FEATUREIO *fioImages,
	int iImages,

	vector<Feature3D> &vecFeats
)
{
	Feature3D feat3D;
	FEATUREIO featSample;
	featSample.x = featSample.y = featSample.z = Feature3D::FEATURE_3D_DIM;
	featSample.t = 1;
	featSample.iFeaturesPerVector = 1;
	fioAllocate(featSample);
	for (int i = 0; i < lvaMinima.iCount; i++)
	{
		feat3D.x = lvaMinima.plvz[i].x;
		feat3D.y = lvaMinima.plvz[i].y;
		feat3D.z = lvaMinima.plvz[i].z;
		//feat3D.scale = 2*fScale;
		feat3D.scale = 2 * interpolate_extremum_quadratic(
			fScaleH, fScaleC, fScaleL,
			fioGetPixel(fioH, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, lvaMinima.plvz[i].z),
			fioGetPixel(fioC, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, lvaMinima.plvz[i].z),
			fioGetPixel(fioL, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, lvaMinima.plvz[i].z)
		);
		feat3D.eigs[0] = feat3D.eigs[1] = feat3D.eigs[2] = 1;



		//// Image region is 2x the feature scale
		//float fImageRad = 2.0f*feat3D.scale; //MICCAI 09, NeuroImage 09
		//int iRadMax = fImageRad+2;

		//if(
		//	feat3D.x-iRadMax < 0 ||
		//	feat3D.y-iRadMax < 0 ||
		//	feat3D.z-iRadMax < 0 ||
		//	feat3D.x+iRadMax >= fioH.x ||
		//	feat3D.y+iRadMax >= fioH.y ||
		//	feat3D.z+iRadMax >= fioH.z )
		//{
		//	// Feature out of image bounds
		//	continue;
		//}
		if (lvaMaxima.plvz[i].fValue < 0.0001f)
		{
			continue;
		}

		// Save joint histogram data for future prior
		for (int iFeat = 0; iFeat < iImages; iFeat++)
		{
			float *pfProbVec = fioGetVector(fioImages[iFeat], lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, lvaMinima.plvz[i].z);
			feat3D.data_zyx[0][0][iFeat] = *pfProbVec;
		}

		{
			feat3D.m_uiInfo &= 0xFFFFFFEF;
			//feat3D.ori[0][0] = -1; // Flag as minimum
			vecFeats.push_back(feat3D);
		}
	}
	for (int i = 0; i < lvaMaxima.iCount; i++)
	{
		feat3D.x = lvaMaxima.plvz[i].x;
		feat3D.y = lvaMaxima.plvz[i].y;
		feat3D.z = lvaMaxima.plvz[i].z;
		//feat3D.scale = 2*fScale;
		feat3D.scale = 2 * interpolate_extremum_quadratic(
			fScaleH, fScaleC, fScaleL,
			fioGetPixel(fioH, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].z),
			fioGetPixel(fioC, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].z),
			fioGetPixel(fioL, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].z)
		);
		feat3D.eigs[0] = feat3D.eigs[1] = feat3D.eigs[2] = 1;

		// Image region is 2x the feature scale
		//float fImageRad = 2.0f*feat3D.scale; //MICCAI 09, NeuroImage 09
		//int iRadMax = fImageRad+2;

		//if(
		//	feat3D.x-iRadMax < 0 ||
		//	feat3D.y-iRadMax < 0 ||
		//	feat3D.z-iRadMax < 0 ||
		//	feat3D.x+iRadMax >= fioH.x ||
		//	feat3D.y+iRadMax >= fioH.y ||
		//	feat3D.z+iRadMax >= fioH.z )
		//{
		//	// Feature out of image bounds
		//	continue;
		//}
		if (lvaMaxima.plvz[i].fValue < 0.0001f)
		{
			continue;
		}

		// Save joint histogram data for future prior
		for (int iFeat = 0; iFeat < iImages; iFeat++)
		{
			float *pfProbVec = fioGetVector(fioImages[iFeat], lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].z);
			feat3D.data_zyx[0][0][iFeat] = *pfProbVec;
		}

		{
			feat3D.m_uiInfo |= 0x00000010;
			//feat3D.ori[0][0] = 1; // Flag as maximum
			vecFeats.push_back(feat3D);
		}
	}
	fioDelete(featSample);
	return 0;
}

int
msGeneratePyramidDOG3D(
	FEATUREIO	&fioImg,
	vector<Feature3D> &vecFeats,
	float fInitialImageScale,
	int bDense,
	int bOutput,
	float fEigThres
)
{
	int iScaleCount = 4;
	char pcFileName[300];

	PpImage ppImgOut;
	PpImage ppImgOutMem;
	ppImgOutMem.Initialize(1, fioImg.x*fioImg.y*fioImg.z, fioImg.x*fioImg.y*fioImg.z * sizeof(float), sizeof(float) * 8);

	// Allocate temporary arrays for blurring
	FEATUREIO fioTemp;
	FEATUREIO fioImgBase;
	fioImgBase = fioTemp = fioImg;
	fioImgBase.pfVectors = fioTemp.pfVectors = 0;
	if (!fioAllocate(fioTemp)) return 0;
	if (!fioAllocate(fioImgBase)) return 0;

	LOCATION_VALUE_XYZ_ARRAY lvaMaxima, lvaMinima;
	lvaMaxima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	lvaMinima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	assert(lvaMaxima.plvz);
	assert(lvaMinima.plvz);
	if (!lvaMaxima.plvz) return 0;
	if (!lvaMinima.plvz) return 0;

	//For Testing ***
	LOCATION_VALUE_XYZ_ARRAY lvaMaxima_test, lvaMinima_test;
	lvaMaxima_test.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	lvaMinima_test.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	assert(lvaMaxima_test.plvz);
	assert(lvaMinima_test.plvz);
	if (!lvaMaxima_test.plvz) return 0;
	if (!lvaMinima_test.plvz) return 0;

	fioFindMinMax(fioImg, g_fMinPixel, g_fMaxPixel);

	int i;

	// Blur pyramid is a 3D FEATUREIO
	FEATUREIO fioStack;
	FEATUREIO fioBlurs[BLURS_TOTAL];
	if (initFIOStack(fioImg, fioStack, (FEATUREIO*)&fioBlurs, BLURS_TOTAL) != 0) return 0;

	// DOG pyramid
	FEATUREIO fioStackDOG;
	FEATUREIO fioDOGs[BLURS_TOTAL - 1];
	if (initFIOStack(fioImg, fioStackDOG, (FEATUREIO*)&fioDOGs, BLURS_TOTAL - 1) != 0) return 0;

	// Edges *** no longer used
	FEATUREIO fioEdgeStack;
	FEATUREIO fioEdgeXYZ[3];
	//initFIOStack( fioImg, fioEdgeStack, (FEATUREIO*)&fioEdgeXYZ, 3 );

	//
	// Here, the initial blur sigma is calculated along with the subsequent
	// sigma increments.
	//
	//float fSigma = 1.0f;
	float fSigmaInit = 0.5; // initial blurring: image resolution
	if (fInitialImageScale > 0) fSigmaInit /= fInitialImageScale;
	float fSigma = 1.6f; // desired initial blurring
	float fSigmaFactor = (float)pow(2.0, 1.0 / (double)BLURS_PER_OCTAVE);


	// Output initial image
	ppImgOut.InitializeSubImage(
		fioImg.y,
		fioImg.x,
		fioImg.x * sizeof(float), sizeof(float) * 8,
		(unsigned char*)ppImgOutMem.ImageRow(0)
	);
	fioFeatureSliceXY(fioImg, fioImg.z / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "image.pgm");
	if (bOutput) output_float(ppImgOut, pcFileName);

	fioFeatureSliceXY(fioTemp, fioTemp.z / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "image_temp.pgm");
	if (bOutput) output_float(ppImgOut, pcFileName);

	fioFeatureSliceXY(fioBlurs[0], fioBlurs[0].z / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "image_blur0.pgm");
	if (bOutput) output_float(ppImgOut, pcFileName);




	// Start initial blur: blur_2^2 = blur_1^2 + fSigmaExtra^2
	float fSigmaExtra = sqrt(fSigma*fSigma - fSigmaInit*fSigmaInit);
	gb3d_blur3d(fioImg, fioTemp, fioBlurs[0], fSigmaExtra, BLUR_PRECISION);

	fioFeatureSliceXY(fioImg, fioImg.z / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "image.pgm");
	if (bOutput) output_float(ppImgOut, pcFileName);

	fioFeatureSliceXY(fioTemp, fioTemp.z / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "image_temp.pgm");
	if (bOutput) output_float(ppImgOut, pcFileName);

	fioFeatureSliceXY(fioBlurs[0], fioBlurs[0].z / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "image_blur0.pgm");
	if (bOutput) output_float(ppImgOut, pcFileName);

	float fScale = 1;

	float pfBlurSigmas[BLURS_TOTAL];

	int iFeatDiff = 1;
	for (int i = 0; 1; i++)
	{
		printf("Scale %d: blur...", i);

		fSigma = 1.6f; // desired initial blurring

		pfBlurSigmas[0] = fSigma;

		if (fioBlurs[0].x <= 2 || fioBlurs[0].y <= 2 || fioBlurs[0].z <= 2)
			goto finish_up; // Quit if too small

							// Generate blurs in octave
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			// How much extra blur required to raise fSigma-blurred image
			// to next bur level
			float fSigmaExtra = fSigma*sqrt(fSigmaFactor*fSigmaFactor - 1.0f);

			int iReturn = gb3d_blur3d(fioBlurs[j - 1], fioTemp, fioBlurs[j], fSigmaExtra, BLUR_PRECISION);
			if (iReturn == 0)
				goto finish_up;

			fSigma *= fSigmaFactor;
			pfBlurSigmas[j] = fSigma;
		}

		// Output blurs to see
		for (int j = 0; j < BLURS_TOTAL; j++)
		{
			ppImgOut.InitializeSubImage(
				fioBlurs[j].y,
				fioBlurs[j].x,
				fioBlurs[j].x * sizeof(float), sizeof(float) * 8,
				(unsigned char*)ppImgOutMem.ImageRow(0)
			);
			fioFeatureSliceXY(fioBlurs[j], fioBlurs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "blur%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);
		}

		// Save 2*sigma image
		fioCopy(fioTemp, fioBlurs[BLURS_PER_OCTAVE]);

		printf("diff...");

		// Create difference of Gaussian
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			fioMultSum(fioBlurs[j - 1], fioBlurs[j], fioDOGs[j - 1], -1.0f);
		}

		// Output dogs to see
		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			ppImgOut.InitializeSubImage(fioDOGs[j].y, fioDOGs[j].x, fioDOGs[j].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
			fioFeatureSliceXY(fioDOGs[j], fioDOGs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "dog%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);
		}

		printf("peaks...");

		// Detect peaks and create features
		int iFeatsCurr = vecFeats.size();
		for (int j = 0; j < BLURS_PER_OCTAVE; j++)
		{
			// Perform peak selection
			detectExtrema4D(
				fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
				lvaMinima,
				lvaMaxima,
				bDense
			);

			//// Testing - 
			//detectExtrema4D_test(
			//	&fioDOGs[j], &fioDOGs[j+1], 0,
			//	lvaMinima_test,
			//	lvaMaxima_test
			//	);

			////fioBlurs[j-1], fioBlurs[j], fioDOGs[j-1]

			//map<float,int> mst;
			//int iSi = sizeof(mst);

			//int iSameMin = 0;
			//for( int h = 0; h < lvaMinima.iCount; h++ )
			//{
			//	for( int k = 0; k < lvaMinima_test.iCount; k++ )
			//	{
			//		if( (abs(lvaMinima.plvz[h].x - lvaMinima_test.plvz[k].x) |
			//			 abs(lvaMinima.plvz[h].y - lvaMinima_test.plvz[k].y) |
			//			 abs(lvaMinima.plvz[h].z - lvaMinima_test.plvz[k].z)) <= 0 )
			//		{
			//			iSameMin++;
			//		}
			//	}
			//}
			//int iSameMax = 0;
			//for( int h = 0; h < lvaMaxima.iCount; h++ )
			//{
			//	for( int k = 0; k < lvaMaxima_test.iCount; k++ )
			//	{
			//		if( (abs(lvaMaxima.plvz[h].x - lvaMaxima_test.plvz[k].x) |
			//			 abs(lvaMaxima.plvz[h].y - lvaMaxima_test.plvz[k].y) |
			//			 abs(lvaMaxima.plvz[h].z - lvaMaxima_test.plvz[k].z)) <= 0 )
			//		{
			//			iSameMax++;
			//		}
			//	}
			//}

			//iSameMin = 0;
			//iSameMax = 0;

			//for( int k = 0; k < lvaMaxima_test.iCount; k++ )
			//{
			//	//float fValue = fioGetPixel( fioG1, lvaMaxima_test.plvz[k].x, lvaMaxima_test.plvz[k].y, lvaMaxima_test.plvz[k].z )
			//	//	- fioGetPixel( fioG2, lvaMaxima_test.plvz[k].x, lvaMaxima_test.plvz[k].y, lvaMaxima_test.plvz[k].z );
			//	float fValue = fioGetPixel( fioDOGs[j+2], lvaMaxima_test.plvz[k].x, lvaMaxima_test.plvz[k].y, lvaMaxima_test.plvz[k].z );

			//	if( fValue < lvaMaxima_test.plvz[k].fValue )
			//	{
			//		iSameMax++;
			//	}
			//}

			//printf( "done\n" );

			// Generate edges, for orientation
			// ** Not used anymore
			//fioGenerateEdgeImages3D( fioBlurs[j+1], fioEdgeXYZ[0], fioEdgeXYZ[1], fioEdgeXYZ[2] );

			// Output edge images to see
			//int k;
			//for( k = 0; k < 3; k++ )
			//{
			//	ppImgOut.InitializeSubImage( fioEdgeXYZ[k].z, fioEdgeXYZ[k].y, fioEdgeXYZ[k].y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
			//	fioFeatureSliceZY( fioEdgeXYZ[k],  fioEdgeXYZ[k].x/2, 0, (float*)ppImgOut.ImageRow(0) );
			//	sprintf( pcFileName, "edge%2.2d.pgm", k );
			//	if( bOutput ) output_float( ppImgOut, pcFileName );
			//}

			// Generate features for points found
			int iFirstFeat = vecFeats.size();
			generateFeatures3D(
				lvaMinima, lvaMaxima,
				fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
				pfBlurSigmas[j], pfBlurSigmas[j + 1], pfBlurSigmas[j + 2],
				fioBlurs[j + 1], //MICCAI 09, NeuroImage 09
				fioEdgeXYZ[0],
				fioEdgeXYZ[1],
				fioEdgeXYZ[2],
				pfBlurSigmas[j + 1],
				vecFeats, fEigThres
			);

			// Update geometry to image space
			float fFactor = fScale;
			float fAddend = 0;//fFactor/2.0f;
			for (int iFU = iFirstFeat; iFU < vecFeats.size(); iFU++)
			{
				// Update feature scale to actual
				vecFeats[iFU].scale *= fFactor;
				// Should also update location...
				vecFeats[iFU].x = vecFeats[iFU].x*fFactor + fAddend;
				vecFeats[iFU].y = vecFeats[iFU].y*fFactor + fAddend;
				vecFeats[iFU].z = vecFeats[iFU].z*fFactor + fAddend;
			}
		}

		iFeatDiff = vecFeats.size() - iFeatsCurr;
		printf("%d...", vecFeats.size() - iFeatsCurr);

		printf("subsample...");

		fioBlurs[0].x /= 2;
		fioBlurs[0].y /= 2;
		fioBlurs[0].z /= 2;
		// Subsample sigma=2 image into first position
		//fioSubSample( fioTemp, fioBlurs[0] );
		fioSubSampleInterpolate(fioTemp, fioBlurs[0]);

		ppImgOut.InitializeSubImage(fioBlurs[0].z, fioBlurs[0].y, fioBlurs[0].y * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
		fioFeatureSliceZY(fioBlurs[0], fioBlurs[0].x / 2, 0, (float*)ppImgOut.ImageRow(0));
		sprintf(pcFileName, "blur_first%2.2d.pgm", 222);
		output_float(ppImgOut, pcFileName);

		//ppImgOut.InitializeSubImage( fioTemp.z, fioTemp.y, fioTemp.y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioTemp,  fioTemp.x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "temp_after%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		// Halve dimensions of all blur images
		// fioBlurs[0] is already done above
		fioTemp.x /= 2;
		fioTemp.y /= 2;
		fioTemp.z /= 2;
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			fioBlurs[j].x /= 2;
			fioBlurs[j].y /= 2;
			fioBlurs[j].z /= 2;
			int iElementsPerImage = fioBlurs[j].z*fioBlurs[j].y*fioBlurs[j].x;
			fioBlurs[j].pfVectors = fioStack.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			fioDOGs[j].x /= 2;
			fioDOGs[j].y /= 2;
			fioDOGs[j].z /= 2;
			int iElementsPerImage = fioDOGs[j].z*fioDOGs[j].y*fioDOGs[j].x;
			fioDOGs[j].pfVectors = fioStackDOG.pfVectors + j*iElementsPerImage;
		}

		// ** No longer used
		//for( int j = 0; j < 3; j++ )
		//{
		//	fioEdgeXYZ[j].x /= 2;
		//	fioEdgeXYZ[j].y /= 2;
		//	fioEdgeXYZ[j].z /= 2;
		//	int iElementsPerImage = fioEdgeXYZ[j].z*fioEdgeXYZ[j].y*fioEdgeXYZ[j].x;
		//	fioEdgeXYZ[j].pfVectors = fioEdgeStack.pfVectors + j*iElementsPerImage;
		//}

		fScale *= 2.0f;

		printf("done.\n");
	}

finish_up:


	fioDelete(fioStackDOG);
	// ** no longer used fioDelete( fioEdgeStack );
	fioDelete(fioStack);
	fioDelete(fioTemp);
	fioDelete(fioImgBase);

	return 1;
}

//
// validateDifferencePeak3D()
//
// Returns 1 if lvPeak is a peak compared to fio-fio2, 0 otherwise.
//
static int
validateDifferencePeak3D(
	LOCATION_VALUE_XYZ &lvPeak,
	FEATUREIO &fio,
	FEATUREIO &fio2
)
{
	// Set up neighbourhood

	int iNeighbourIndexCount;
	int piNeighbourIndices[27];

	int iZStart, iZCount;

	if (fio.z > 1)
	{
		// 3D
		iNeighbourIndexCount = 26;
		iZStart = 1;
		iZCount = fio.z - 1;

		piNeighbourIndices[0] = -fio.x*fio.y - fio.x - 1;
		piNeighbourIndices[1] = -fio.x*fio.y - fio.x;
		piNeighbourIndices[2] = -fio.x*fio.y - fio.x + 1;
		piNeighbourIndices[3] = -fio.x*fio.y - 1;
		piNeighbourIndices[4] = -fio.x*fio.y;
		piNeighbourIndices[5] = -fio.x*fio.y + 1;
		piNeighbourIndices[6] = -fio.x*fio.y + fio.x - 1;
		piNeighbourIndices[7] = -fio.x*fio.y + fio.x;
		piNeighbourIndices[8] = -fio.x*fio.y + fio.x + 1;

		piNeighbourIndices[9] = -fio.x - 1;
		piNeighbourIndices[10] = -fio.x;
		piNeighbourIndices[11] = -fio.x + 1;
		piNeighbourIndices[12] = -1;
		piNeighbourIndices[13] = 1;
		piNeighbourIndices[14] = fio.x - 1;
		piNeighbourIndices[15] = fio.x;
		piNeighbourIndices[16] = fio.x + 1;

		piNeighbourIndices[17] = fio.x*fio.y - fio.x - 1;
		piNeighbourIndices[18] = fio.x*fio.y - fio.x;
		piNeighbourIndices[19] = fio.x*fio.y - fio.x + 1;
		piNeighbourIndices[20] = fio.x*fio.y - 1;
		piNeighbourIndices[21] = fio.x*fio.y;
		piNeighbourIndices[22] = fio.x*fio.y + 1;
		piNeighbourIndices[23] = fio.x*fio.y + fio.x - 1;
		piNeighbourIndices[24] = fio.x*fio.y + fio.x;
		piNeighbourIndices[25] = fio.x*fio.y + fio.x + 1;
	}
	else
	{
		// 2D
		iNeighbourIndexCount = 8;
		iZStart = 0;
		iZCount = 1;

		piNeighbourIndices[0] = -fio.x - 1;
		piNeighbourIndices[1] = -fio.x;
		piNeighbourIndices[2] = -fio.x + 1;
		piNeighbourIndices[3] = -1;
		piNeighbourIndices[4] = 1;
		piNeighbourIndices[5] = fio.x - 1;
		piNeighbourIndices[6] = fio.x;
		piNeighbourIndices[7] = fio.x + 1;
	}

	int iZIndex = lvPeak.z*fio.x*fio.y;
	int iYIndex = iZIndex + lvPeak.y*fio.x;
	int iIndex = iYIndex + lvPeak.x;

	int bPeak = 1;
	float fCenterValue = lvPeak.fValue;

	// Compute DoG value for center
	float fNeighValue = fio.pfVectors[iIndex*fio.iFeaturesPerVector]
		- fio2.pfVectors[iIndex*fio.iFeaturesPerVector];
	bPeak &= (fNeighValue < fCenterValue);

	for (int n = 0; n < iNeighbourIndexCount && bPeak; n++)
	{
		int iNeighbourIndex = iIndex + piNeighbourIndices[n];
		fNeighValue = fio.pfVectors[iNeighbourIndex*fio.iFeaturesPerVector]
			- fio2.pfVectors[iNeighbourIndex*fio.iFeaturesPerVector];

		// A peak means all neighbours are lower than the center value
		bPeak &= (fNeighValue < fCenterValue);
	}
	return bPeak;
}

//
// validateDifferenceValley3D()
//
// Returns 1 if lvPeak is a valley compared to fio1-fio2, 0 otherwise.
//
static int
validateDifferenceValley3D(
	LOCATION_VALUE_XYZ &lvPeak,
	FEATUREIO &fio,
	FEATUREIO &fio2
)
{
	// Set up neighbourhood

	int iNeighbourIndexCount;
	int piNeighbourIndices[27];

	int iZStart, iZCount;

	if (fio.z > 1)
	{
		// 3D
		iNeighbourIndexCount = 26;
		iZStart = 1;
		iZCount = fio.z - 1;

		piNeighbourIndices[0] = -fio.x*fio.y - fio.x - 1;
		piNeighbourIndices[1] = -fio.x*fio.y - fio.x;
		piNeighbourIndices[2] = -fio.x*fio.y - fio.x + 1;
		piNeighbourIndices[3] = -fio.x*fio.y - 1;
		piNeighbourIndices[4] = -fio.x*fio.y;
		piNeighbourIndices[5] = -fio.x*fio.y + 1;
		piNeighbourIndices[6] = -fio.x*fio.y + fio.x - 1;
		piNeighbourIndices[7] = -fio.x*fio.y + fio.x;
		piNeighbourIndices[8] = -fio.x*fio.y + fio.x + 1;

		piNeighbourIndices[9] = -fio.x - 1;
		piNeighbourIndices[10] = -fio.x;
		piNeighbourIndices[11] = -fio.x + 1;
		piNeighbourIndices[12] = -1;
		piNeighbourIndices[13] = 1;
		piNeighbourIndices[14] = fio.x - 1;
		piNeighbourIndices[15] = fio.x;
		piNeighbourIndices[16] = fio.x + 1;

		piNeighbourIndices[17] = fio.x*fio.y - fio.x - 1;
		piNeighbourIndices[18] = fio.x*fio.y - fio.x;
		piNeighbourIndices[19] = fio.x*fio.y - fio.x + 1;
		piNeighbourIndices[20] = fio.x*fio.y - 1;
		piNeighbourIndices[21] = fio.x*fio.y;
		piNeighbourIndices[22] = fio.x*fio.y + 1;
		piNeighbourIndices[23] = fio.x*fio.y + fio.x - 1;
		piNeighbourIndices[24] = fio.x*fio.y + fio.x;
		piNeighbourIndices[25] = fio.x*fio.y + fio.x + 1;
	}
	else
	{
		// 2D
		iNeighbourIndexCount = 8;
		iZStart = 0;
		iZCount = 1;

		piNeighbourIndices[0] = -fio.x - 1;
		piNeighbourIndices[1] = -fio.x;
		piNeighbourIndices[2] = -fio.x + 1;
		piNeighbourIndices[3] = -1;
		piNeighbourIndices[4] = 1;
		piNeighbourIndices[5] = fio.x - 1;
		piNeighbourIndices[6] = fio.x;
		piNeighbourIndices[7] = fio.x + 1;
	}

	int iZIndex = lvPeak.z*fio.x*fio.y;
	int iYIndex = iZIndex + lvPeak.y*fio.x;
	int iIndex = iYIndex + lvPeak.x;

	int bPeak = 1;
	float fCenterValue = lvPeak.fValue;

	// Compute DoG value for center
	float fNeighValue = fio.pfVectors[iIndex*fio.iFeaturesPerVector]
		- fio2.pfVectors[iIndex*fio.iFeaturesPerVector];
	bPeak &= (fNeighValue > fCenterValue);

	for (int n = 0; n < iNeighbourIndexCount && bPeak; n++)
	{
		int iNeighbourIndex = iIndex + piNeighbourIndices[n];
		fNeighValue = fio.pfVectors[iNeighbourIndex*fio.iFeaturesPerVector]
			- fio2.pfVectors[iNeighbourIndex*fio.iFeaturesPerVector];

		// A peak means all neighbours are lower than the center value
		bPeak &= (fNeighValue > fCenterValue);
	}
	return bPeak;
}



int
msGeneratePyramidDOG3D_efficient(
	FEATUREIO	&fioImg,
	vector<Feature3D> &vecFeats,
	float fInitialImageScale,
	int bDense,
	int bOutput,
	float fEigThres
)
{
	int iScaleCount = 4;
	char pcFileName[300];

	PpImage ppImgOut;
	PpImage ppImgOutMem;
	ppImgOutMem.Initialize(1, fioImg.x*fioImg.y*fioImg.z, fioImg.x*fioImg.y*fioImg.z * sizeof(float), sizeof(float) * 8);

	// DoG extrema candidates
	vector<LOCATION_VALUE_XYZ> vecLVAMin;
	vector<LOCATION_VALUE_XYZ> vecLVAMax;
	vecLVAMin.resize(fioImg.x*fioImg.y);
	vecLVAMax.resize(fioImg.x*fioImg.y);

	int i;

	// Holders for DoG values above & below extrema 
	vector<float> vecMinH, vecMinL, vecMaxH, vecMaxL;
	vecMinH.resize(fioImg.x*fioImg.y);
	vecMinL.resize(fioImg.x*fioImg.y);
	vecMaxH.resize(fioImg.x*fioImg.y);
	vecMaxL.resize(fioImg.x*fioImg.y);

	// Should be able to do everyting with 4 extra images
	// Blur pyramid is a 3D FEATUREIO
	FEATUREIO fioBlurs[4];
	for (i = 0; i < 4; i++)
	{
		fioBlurs[i] = fioImg;
		fioAllocate(fioBlurs[i]);
	}
	// Half dimension image for saving 2X octave
	FEATUREIO fioSaveHalf = fioImg;
	fioSaveHalf.x /= 2;
	fioSaveHalf.y /= 2;
	fioSaveHalf.z /= 2;
	fioAllocate(fioSaveHalf);

	//
	// Here, the initial blur sigma is calculated along with the subsequent
	// sigma increments.
	//
	//float fSigma = 1.0f;
	float fSigmaInit = 0.5; // initial blurring: image resolution
	if (fInitialImageScale > 0) fSigmaInit /= fInitialImageScale;
	float fSigma = 1.6f; // desired initial blurring
	float fSigmaFactor = (float)pow(2.0, 1.0 / (double)BLURS_PER_OCTAVE);

	// Start initial blur: blur_2^2 = blur_1^2 + fSigmaExtra^2
	float fSigmaExtra = sqrt(fSigma*fSigma - fSigmaInit*fSigmaInit);
	gb3d_blur3d(fioImg, fioBlurs[1], fioBlurs[0], fSigmaExtra, BLUR_PRECISION);

	// Output initial image
	ppImgOut.InitializeSubImage(
		fioImg.y,
		fioImg.x,
		fioImg.x * sizeof(float), sizeof(float) * 8,
		(unsigned char*)ppImgOutMem.ImageRow(0)
	);
	fioFeatureSliceXY(fioImg, fioImg.z / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "image.pgm");
	if (bOutput) output_float(ppImgOut, pcFileName);

	float fScale = 1;

	float pfBlurSigmas[BLURS_TOTAL];

	// Temporary image is the original image - a little distructive ... 
	FEATUREIO fioTemp = fioImg;

	// Initialize only the size of these images, memory pointers will be modified later
	FEATUREIO fioG0 = fioImg;
	FEATUREIO fioG1 = fioImg;
	FEATUREIO fioG2 = fioImg;
	FEATUREIO fioD0 = fioImg;
	FEATUREIO fioD1 = fioImg;

	LOCATION_VALUE_XYZ_ARRAY lvaMaxima, lvaMinima;

	int iFeatDiff = 1;
	for (int i = 0; 1; i++)
	{
		printf("Scale %d: blur...", i);

		fSigma = 1.6f; // desired initial blurring

		pfBlurSigmas[0] = fSigma;

		// Init pointers to image memory
		fioG0.pfVectors = fioBlurs[0].pfVectors;
		fioG1.pfVectors = fioBlurs[1].pfVectors;
		fioG2.pfVectors = fioBlurs[2].pfVectors;
		fioD0.pfVectors = fioBlurs[3].pfVectors;

		//if( fioBlurs[0].x <= 2 || fioBlurs[0].y <= 2 || fioBlurs[0].z <= 2 )
		if (fioG0.x <= 2 || fioG0.y <= 2 || fioG0.z <= 2)
			goto finish_up; // Quit if too small

		int iFirstFeat = vecFeats.size();

		// Generate blurs in octave
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			// How much extra blur required to raise fSigma-blurred image
			// to next bur level
			float fSigmaExtra = fSigma*sqrt(fSigmaFactor*fSigmaFactor - 1.0f);

			//printf( "sigmaE: %f\n", fSigmaExtra );
			if (j == 1)
			{
				//int iReturn = gb3d_blur3d( fioBlurs[j-1], fioTemp, fioBlurs[j], fSigmaExtra, BLUR_PRECISION );
				int iReturn = gb3d_blur3d(fioG0, fioTemp, fioG1, fSigmaExtra, BLUR_PRECISION);
				if (iReturn == 0)
					goto finish_up;

				// Initialize DoG
				fioMultSum(fioG0, fioG1, fioD0, -1.0f);
			}
			else
			{
				//int iReturn = gb3d_blur3d( fioBlurs[j-1], fioTemp, fioBlurs[j], fSigmaExtra, BLUR_PRECISION );
				int iReturn = gb3d_blur3d(fioG1, fioTemp, fioG2, fSigmaExtra, BLUR_PRECISION);
				if (iReturn == 0)
					goto finish_up;

				if (j == BLURS_PER_OCTAVE)
				{
					// Save 2X octave image for next round
					fioSubSampleInterpolate(fioG2, fioSaveHalf);
				}

				if (j >= 3)
				{
					// A list exists, verify and compute features

					// Verify Maxima from previous level
					int iCurrTop = 0;
					for (int k = 0; k < lvaMaxima.iCount; k++)
					{
						if (validateDifferencePeak3D(vecLVAMax[k], fioG1, fioG2))
							//float fValue = fioGetPixel( fioG1, vecLVAMax[k].x, vecLVAMax[k].y, vecLVAMax[k].z ) - fioGetPixel( fioG2, vecLVAMax[k].x, vecLVAMax[k].y, vecLVAMax[k].z );
							//if( fValue < vecLVAMax[k].fValue )
						{
							// Save this value (overwrite)
							vecMaxH[iCurrTop] = vecMaxH[k];
							vecMaxL[iCurrTop] = fioGetPixel(fioG1, vecLVAMax[k].x, vecLVAMax[k].y, vecLVAMax[k].z) - fioGetPixel(fioG2, vecLVAMax[k].x, vecLVAMax[k].y, vecLVAMax[k].z);
							vecLVAMax[iCurrTop] = vecLVAMax[k];
							//lvaMaxima.plvz[iCurrTop] = vecLVAMax[k];
							iCurrTop++;
						}
					}
					lvaMaxima.iCount = iCurrTop;

					// Verify Minima from previous level
					iCurrTop = 0;
					for (int k = 0; k < lvaMinima.iCount; k++)
					{
						if (validateDifferenceValley3D(vecLVAMin[k], fioG1, fioG2))
							//float fValue = fioGetPixel( fioG1, vecLVAMin[k].x, vecLVAMin[k].y, vecLVAMin[k].z ) - fioGetPixel( fioG2, vecLVAMin[k].x, vecLVAMin[k].y, vecLVAMin[k].z );
							//if( fValue > vecLVAMin[k].fValue )
						{
							// Save this value (overwrite)
							vecMinH[iCurrTop] = vecMinH[k];
							vecMinL[iCurrTop] = fioGetPixel(fioG1, vecLVAMin[k].x, vecLVAMin[k].y, vecLVAMin[k].z) - fioGetPixel(fioG2, vecLVAMin[k].x, vecLVAMin[k].y, vecLVAMin[k].z);
							vecLVAMin[iCurrTop] = vecLVAMin[k];
							//lvaMinima.plvz[iCurrTop] = vecLVAMin[k];
							iCurrTop++;
						}
					}
					lvaMinima.iCount = iCurrTop;

					lvaMaxima.plvz = &(vecLVAMax[0]);
					lvaMinima.plvz = &(vecLVAMin[0]);

					// Generate features for points found
					// Have to send in feature values above/below for interpolation
					generateFeatures3D_efficient(
						lvaMinima, lvaMaxima,
						vecMinH, vecMinL, vecMaxH, vecMaxL,
						fioD0,
						pfBlurSigmas[j - 3], pfBlurSigmas[j - 2], pfBlurSigmas[j - 1],
						fioG0, //MICCAI 09, NeuroImage 09
						pfBlurSigmas[j + 1],
						vecFeats, fEigThres
					);
				}

				if (j < BLURS_TOTAL - 1)
				{
					// Lists and fioG0 are now free

					// Difference: fioD1 = fioG1 - fioG2
					fioD1.pfVectors = fioG0.pfVectors;
					fioMultSum(fioG1, fioG2, fioD1, -1.0f);

					// Identify peak candidates

					lvaMaxima.plvz = &(vecLVAMax[0]);
					lvaMinima.plvz = &(vecLVAMin[0]);
					detectExtrema4D_test(
						&fioD0, &fioD1, 0,
						lvaMinima,
						lvaMaxima
					);

					// Save values for scale below Maxima
					for (int k = 0; k < lvaMaxima.iCount; k++)
					{
						//vecLVAMax[k] = vecLVAMax[k];
						vecMaxH[k] = fioGetPixel(fioD0, vecLVAMax[k].x, vecLVAMax[k].y, vecLVAMax[k].z);
					}

					// Save values for scale below Minima
					for (int k = 0; k < lvaMinima.iCount; k++)
					{
						//vvLVAMin[0][k] = vecLVAMin[k];
						vecMinH[k] = fioGetPixel(fioD0, vecLVAMin[k].x, vecLVAMin[k].y, vecLVAMin[k].z);
					}

					// fioD0 is now free

					float *pfG2Temp = fioG2.pfVectors;
					fioG2.pfVectors = fioD0.pfVectors;
					fioD0.pfVectors = fioD1.pfVectors;
					fioG0.pfVectors = fioG1.pfVectors;
					fioG1.pfVectors = pfG2Temp;
				}
			}

			fSigma *= fSigmaFactor;
			pfBlurSigmas[j] = fSigma;
		}

		// Update geometry to image space
		float fFactor = fScale;
		float fAddend = 0;//fFactor/2.0f;
		for (int iFU = iFirstFeat; iFU < vecFeats.size(); iFU++)
		{
			// Update feature scale to actual
			vecFeats[iFU].scale *= fFactor;
			// Should also update location...
			vecFeats[iFU].x = vecFeats[iFU].x*fFactor + fAddend;
			vecFeats[iFU].y = vecFeats[iFU].y*fFactor + fAddend;
			vecFeats[iFU].z = vecFeats[iFU].z*fFactor + fAddend;
		}
		fScale *= 2.0f;

		// Initialize saved image
		fioBlurs[0].x /= 2;
		fioBlurs[0].y /= 2;
		fioBlurs[0].z /= 2;
		fioCopy(fioBlurs[0], fioSaveHalf);
		fioSaveHalf.x /= 2;
		fioSaveHalf.y /= 2;
		fioSaveHalf.z /= 2;

		//ppImgOut.InitializeSubImage( fioBlurs[0].z, fioBlurs[0].y, fioBlurs[0].y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioBlurs[0],  fioBlurs[0].x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "blur_first%2.2d.pgm", 444 );
		//output_float( ppImgOut, pcFileName );


		fioTemp.x = fioG0.x = fioG1.x = fioG2.x = fioD0.x = fioD1.x = fioBlurs[0].x;
		fioTemp.y = fioG0.y = fioG1.y = fioG2.y = fioD0.y = fioD1.y = fioBlurs[0].y;
		fioTemp.z = fioG0.z = fioG1.z = fioG2.z = fioD0.z = fioD1.z = fioBlurs[0].z;

		printf("done.\n");
	}

finish_up:

	for (i = 0; i < 4; i++)
	{
		fioDelete(fioBlurs[i]);
	}
	fioDelete(fioSaveHalf);

	return 1;
}



int
generateScaleInvariantHarris3D(
	FEATUREIO *fioD,
	FEATUREIO &fioHarris
)
{
	for (int zz = 0; zz < fioHarris.z; zz++)
	{
		for (int yy = 0; yy < fioHarris.y; yy++)
		{
			for (int xx = 0; xx < fioHarris.x; xx++)
			{
				float dxdx = fioGetPixel(fioD[0], xx, yy, zz);
				float dydy = fioGetPixel(fioD[1], xx, yy, zz);
				float dzdz = fioGetPixel(fioD[2], xx, yy, zz);
				float dxdy = fioGetPixel(fioD[3], xx, yy, zz);
				float dxdz = fioGetPixel(fioD[4], xx, yy, zz);
				float dydz = fioGetPixel(fioD[5], xx, yy, zz);

				float fDet =
					dxdx*(dydy*dzdz - dydz*dydz) -
					dxdy*(dxdy*dzdz - dxdz*dydz) +
					dxdz*(dxdy*dydz - dydy*dxdz);

				float fTrace = dxdx + dydy + dzdz;

				//if( fDet != 0 && fTrace != 0 )
				//{

				//	float *pfHarris = fioGetVector( fioHarris, xx, yy, zz );
				//	*pfHarris = fDet - 0.6f*fTrace*fTrace*fTrace;
				//}
				//else
				{
					float *pfHarris = fioGetVector(fioHarris, xx, yy, zz);
					*pfHarris = fDet - 0.6f*fTrace*fTrace*fTrace;
				}

			}
		}
	}
	return 1;
}

int
msGeneratePyramidHarris3D(
	FEATUREIO	&fioImg,
	vector<Feature3D> &vecFeats,
	float fInitialImageScale,
	int bDense,
	int bOutput
)
{
	int iScaleCount = 4;
	char pcFileName[300];

	PpImage ppImgOut;
	PpImage ppImgOutMem;
	ppImgOutMem.Initialize(1, fioImg.x*fioImg.y*fioImg.z, fioImg.x*fioImg.y*fioImg.z * sizeof(float), sizeof(float) * 8);

	// Allocate temporary arrays for blurring
	FEATUREIO fioTemp;
	FEATUREIO fioImgBase;
	fioImgBase = fioTemp = fioImg;
	fioImgBase.pfVectors = fioTemp.pfVectors = 0;
	fioAllocate(fioTemp);
	fioAllocate(fioImgBase);

	LOCATION_VALUE_XYZ_ARRAY lvaMaxima, lvaMinima;
	lvaMaxima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	lvaMinima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	assert(lvaMaxima.plvz);
	assert(lvaMinima.plvz);

	int i;

	// Blur pyramid is a 3D FEATUREIO
	FEATUREIO fioStack;
	FEATUREIO fioBlurs[BLURS_TOTAL];
	initFIOStack(fioImg, fioStack, (FEATUREIO*)&fioBlurs, BLURS_TOTAL);

	// DOG pyramid
	FEATUREIO fioStackDOG;
	FEATUREIO fioDOGs[BLURS_TOTAL - 1];
	initFIOStack(fioImg, fioStackDOG, (FEATUREIO*)&fioDOGs, BLURS_TOTAL - 1);

	// Edges - edge stack must be large enough to contain all 1st derivative combinations, dxdx,dydy,dzdz,dxdy,dxdz,dydz
	FEATUREIO fioEdgeStack;
	FEATUREIO fioEdgeXYZ[6];
	initFIOStack(fioImg, fioEdgeStack, (FEATUREIO*)&fioEdgeXYZ, 6);

	//
	// Here, the initial blur sigma is calculated along with the subsequent
	// sigma increments.
	//
	//float fSigma = 1.0f;
	float fSigmaInit = 0.5; // initial blurring: image resolution
	if (fInitialImageScale > 0) fSigmaInit /= fInitialImageScale;
	float fSigma = 1.6f; // desired initial blurring
	float fSigmaFactor = (float)pow(2.0, 1.0 / (double)BLURS_PER_OCTAVE);

	// Start initial blur: blur_2^2 = blur_1^2 + fSigmaExtra^2
	float fSigmaExtra = sqrt(fSigma*fSigma - fSigmaInit*fSigmaInit);
	gb3d_blur3d(fioImg, fioTemp, fioBlurs[0], fSigmaExtra, BLUR_PRECISION);

	// Output initial image
	ppImgOut.InitializeSubImage(
		fioImg.y,
		fioImg.x,
		fioImg.x * sizeof(float), sizeof(float) * 8,
		(unsigned char*)ppImgOutMem.ImageRow(0)
	);
	fioFeatureSliceXY(fioImg, fioImg.z / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "image.pgm");
	if (bOutput) output_float(ppImgOut, pcFileName);

	float fScale = fInitialImageScale;

	float pfBlurSigmas[BLURS_TOTAL];

	int iFeatDiff = 1;
	for (int i = 0; 1; i++)
	{
		printf("Scale %d: blur...", i);

		fSigma = 1.6f; // desired initial blurring

		pfBlurSigmas[0] = fSigma;

		// Generate blurs in octave
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			// How much extra blur required to raise fSigma-blurred image
			// to next bur level
			float fSigmaExtra = fSigma*sqrt(fSigmaFactor*fSigmaFactor - 1.0f);

			int iReturn = gb3d_blur3d(fioBlurs[j - 1], fioTemp, fioBlurs[j], fSigmaExtra, BLUR_PRECISION);
			if (iReturn == 0)
				goto finish_up;

			fSigma *= fSigmaFactor;
			pfBlurSigmas[j] = fSigma;
		}

		// Output blurs to see
		for (int j = 0; j < BLURS_TOTAL; j++)
		{
			ppImgOut.InitializeSubImage(
				fioBlurs[j].y,
				fioBlurs[j].x,
				fioBlurs[j].x * sizeof(float), sizeof(float) * 8,
				(unsigned char*)ppImgOutMem.ImageRow(0)
			);
			fioFeatureSliceXY(fioBlurs[j], fioBlurs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "blur%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);
		}

		// Save 2*sigma image
		fioCopy(fioTemp, fioBlurs[BLURS_PER_OCTAVE]);

		printf("diff...");

		// Create difference of Gaussian
		for (int j = 0; j < BLURS_TOTAL; j++)
		{
			// Generate first derivatives
			fioGenerateEdgeImages3D(
				fioBlurs[j],
				fioEdgeXYZ[0], fioEdgeXYZ[1], fioEdgeXYZ[2]
			);

			// Generate 2nd moment matrix, six product images (dxdx, dxdy,dxdz, dydy, dydz, dzdz) 
			// This is very slow...at least no extra memory is required	


			//fioMultiply( fioEdgeXYZ[0], fioEdgeXYZ[1], fioImgBase );
			//ppImgOut.InitializeSubImage( fioImgBase.y, fioImgBase.x, fioImgBase.x*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
			//fioFeatureSliceXY( fioImgBase,  fioImgBase.z/2, 0, (float*)ppImgOut.ImageRow(0) );
			//sprintf( pcFileName, "dxdx_0.pgm", 0 );
			//output_float( ppImgOut, pcFileName );

			//gb3d_blur3d( fioImgBase, fioTemp, fioEdgeXYZ[3], pfBlurSigmas[j]*.7f, BLUR_PRECISION ); // dxdy
			//ppImgOut.InitializeSubImage( fioEdgeXYZ[3].y, fioEdgeXYZ[3].x, fioEdgeXYZ[3].x*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
			//fioFeatureSliceXY( fioEdgeXYZ[3],  fioEdgeXYZ[3].z/2, 0, (float*)ppImgOut.ImageRow(0) );
			//sprintf( pcFileName, "dxdx_0blur.pgm", 0 );
			//output_float( ppImgOut, pcFileName );

			fioMultiply(fioEdgeXYZ[0], fioEdgeXYZ[1], fioImgBase); gb3d_blur3d(fioImgBase, fioTemp, fioEdgeXYZ[3], pfBlurSigmas[j] * .7f, BLUR_PRECISION); // dxdy
			fioMultiply(fioEdgeXYZ[0], fioEdgeXYZ[2], fioImgBase); gb3d_blur3d(fioImgBase, fioTemp, fioEdgeXYZ[4], pfBlurSigmas[j] * .7f, BLUR_PRECISION); // dxdz
			fioMultiply(fioEdgeXYZ[1], fioEdgeXYZ[2], fioImgBase); gb3d_blur3d(fioImgBase, fioTemp, fioEdgeXYZ[5], pfBlurSigmas[j] * .7f, BLUR_PRECISION); // dydz

			fioMultiply(fioEdgeXYZ[0], fioEdgeXYZ[0], fioImgBase); gb3d_blur3d(fioImgBase, fioTemp, fioEdgeXYZ[0], pfBlurSigmas[j] * .7f, BLUR_PRECISION); // dxdx
			fioMultiply(fioEdgeXYZ[1], fioEdgeXYZ[1], fioImgBase); gb3d_blur3d(fioImgBase, fioTemp, fioEdgeXYZ[1], pfBlurSigmas[j] * .7f, BLUR_PRECISION); // dydy
			fioMultiply(fioEdgeXYZ[2], fioEdgeXYZ[2], fioImgBase); gb3d_blur3d(fioImgBase, fioTemp, fioEdgeXYZ[2], pfBlurSigmas[j] * .7f, BLUR_PRECISION); // dzdz

			ppImgOut.InitializeSubImage(fioEdgeXYZ[0].y, fioEdgeXYZ[0].x, fioEdgeXYZ[0].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
			fioFeatureSliceXY(fioEdgeXYZ[0], fioEdgeXYZ[0].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "dxdx.pgm", 0);
			if (bOutput)  output_float(ppImgOut, pcFileName);
			ppImgOut.InitializeSubImage(fioEdgeXYZ[1].y, fioEdgeXYZ[1].x, fioEdgeXYZ[1].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
			fioFeatureSliceXY(fioEdgeXYZ[1], fioEdgeXYZ[1].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "dydy.pgm", 0);
			if (bOutput)  output_float(ppImgOut, pcFileName);
			ppImgOut.InitializeSubImage(fioEdgeXYZ[3].y, fioEdgeXYZ[3].x, fioEdgeXYZ[3].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
			fioFeatureSliceXY(fioEdgeXYZ[3], fioEdgeXYZ[3].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "dxdy.pgm", 0);
			if (bOutput)  output_float(ppImgOut, pcFileName);

			// Generate Harris criterion, put result in fioDOGs
			generateScaleInvariantHarris3D(fioEdgeXYZ, fioDOGs[j]);

			ppImgOut.InitializeSubImage(fioDOGs[j].y, fioDOGs[j].x, fioDOGs[j].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
			fioFeatureSliceXY(fioDOGs[j], fioDOGs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "harris.pgm", 0);
			if (bOutput)  output_float(ppImgOut, pcFileName);

			// Scale-normalize
			fioMult(fioDOGs[j], fioDOGs[j], pfBlurSigmas[j] * pfBlurSigmas[j]);
		}

		// Output dogs to see
		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			ppImgOut.InitializeSubImage(fioDOGs[j].y, fioDOGs[j].x, fioDOGs[j].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
			fioFeatureSliceXY(fioDOGs[j], fioDOGs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "dog%2.2d.pgm", j);
			if (bOutput)  output_float(ppImgOut, pcFileName);
		}

		printf("peaks...");

		// Detect peaks and create features
		int iFeatsCurr = vecFeats.size();
		for (int j = 0; j < BLURS_PER_OCTAVE; j++)
		{
			// Perform peak selection
			detectExtrema4D(
				fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
				lvaMinima,
				lvaMaxima
			);

			// Generate edges, for orientation
			fioGenerateEdgeImages3D(
				fioBlurs[j + 1],
				fioEdgeXYZ[0], fioEdgeXYZ[1], fioEdgeXYZ[2]
			);

			// Output edge images to see
			int k;
			for (k = 0; k < 3; k++)
			{
				ppImgOut.InitializeSubImage(fioEdgeXYZ[k].z, fioEdgeXYZ[k].y, fioEdgeXYZ[k].y * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
				fioFeatureSliceZY(fioEdgeXYZ[k], fioEdgeXYZ[k].x / 2, 0, (float*)ppImgOut.ImageRow(0));
				sprintf(pcFileName, "edge%2.2d.pgm", k);
				if (bOutput) output_float(ppImgOut, pcFileName);
			}

			// Generate features for points found
			int iFirstFeat = vecFeats.size();
			lvaMinima.iCount = 0;
			generateFeatures3D(
				lvaMinima, lvaMaxima,
				fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
				pfBlurSigmas[j], pfBlurSigmas[j + 1], pfBlurSigmas[j + 2],
				fioBlurs[j + 1], //MICCAI 09, NeuroImage 09
				fioEdgeXYZ[0],
				fioEdgeXYZ[1],
				fioEdgeXYZ[2],
				pfBlurSigmas[j + 1],
				vecFeats
			);

			// Update geometry to image space
			float fFactor = fScale;
			float fAddend = 0;//fFactor/2.0f;
			for (int iFU = iFirstFeat; iFU < vecFeats.size(); iFU++)
			{
				// Update feature scale to actual
				vecFeats[iFU].scale *= fFactor;
				// Should also update location...
				vecFeats[iFU].x = vecFeats[iFU].x*fFactor + fAddend;
				vecFeats[iFU].y = vecFeats[iFU].y*fFactor + fAddend;
				vecFeats[iFU].z = vecFeats[iFU].z*fFactor + fAddend;
			}
		}

		iFeatDiff = vecFeats.size() - iFeatsCurr;
		printf("%d...", vecFeats.size() - iFeatsCurr);

		printf("subsample...");

		fioBlurs[0].x /= 2;
		fioBlurs[0].y /= 2;
		fioBlurs[0].z /= 2;
		// Subsample sigma=2 image into first position
		fioSubSample(fioBlurs[BLURS_PER_OCTAVE], fioBlurs[0]);
		fioSubSampleInterpolate(fioBlurs[BLURS_PER_OCTAVE], fioBlurs[0]);

		//ppImgOut.InitializeSubImage( fioBlurs[0].z, fioBlurs[0].y, fioBlurs[0].y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioBlurs[0],  fioBlurs[0].x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "blur_first%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		//ppImgOut.InitializeSubImage( fioTemp.z, fioTemp.y, fioTemp.y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioTemp,  fioTemp.x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "temp_after%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		// Halve dimensions of all blur images
		// fioBlurs[0] is already done above
		fioTemp.x /= 2;
		fioTemp.y /= 2;
		fioTemp.z /= 2;
		fioImgBase.x /= 2;
		fioImgBase.y /= 2;
		fioImgBase.z /= 2;
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			fioBlurs[j].x /= 2;
			fioBlurs[j].y /= 2;
			fioBlurs[j].z /= 2;
			int iElementsPerImage = fioBlurs[j].z*fioBlurs[j].y*fioBlurs[j].x;
			fioBlurs[j].pfVectors = fioStack.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			fioDOGs[j].x /= 2;
			fioDOGs[j].y /= 2;
			fioDOGs[j].z /= 2;
			int iElementsPerImage = fioDOGs[j].z*fioDOGs[j].y*fioDOGs[j].x;
			fioDOGs[j].pfVectors = fioStackDOG.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < 6; j++)
		{
			fioEdgeXYZ[j].x /= 2;
			fioEdgeXYZ[j].y /= 2;
			fioEdgeXYZ[j].z /= 2;
			int iElementsPerImage = fioEdgeXYZ[j].z*fioEdgeXYZ[j].y*fioEdgeXYZ[j].x;
			fioEdgeXYZ[j].pfVectors = fioEdgeStack.pfVectors + j*iElementsPerImage;
		}

		fScale *= 2.0f;

		printf("done.\n");
	}

finish_up:

	delete[] lvaMaxima.plvz;
	delete[] lvaMinima.plvz;

	fioDelete(fioStackDOG);
	fioDelete(fioEdgeStack);
	fioDelete(fioStack);
	fioDelete(fioTemp);

	return 1;
}


//
// __computePixelEntropyBinary()
//
// Compute the entropy of a distribution at each pixel.
// fioImgIn contains single parameters of a binomial distribution.
//
void
__computePixelEntropyBinary(
	FEATUREIO	&fioImgIn,
	FEATUREIO	&fioImgH
)
{
	assert(fioImgIn.x == fioImgH.x);
	assert(fioImgIn.y == fioImgH.y);
	assert(fioImgIn.z == fioImgH.z);
	assert(fioImgIn.t == fioImgH.t == 1);
	assert(fioImgH.iFeaturesPerVector == 1);
	for (int z = 0; z < fioImgIn.z; z++)
	{
		for (int y = 0; y < fioImgIn.y; y++)
		{
			for (int x = 0; x < fioImgIn.x; x++)
			{
				float *pfProb = fioGetVector(fioImgIn, x, y, z);
				float fProbSum = 0;
				float fH = 0;
				for (int k = 0; k < fioImgIn.iFeaturesPerVector; k++)
				{
					fProbSum += pfProb[k];
					if (pfProb[k] > 0)
					{
						fH -= pfProb[k] * log(pfProb[k]);
					}
				}
				// Calculate last remaining probability
				float fProb = 1.0f - fProbSum;
				assert(fProb <= 1.0f);
				if (fProb > 0)
				{
					fH -= fProb*log(fProb);
				}
				float *pfH = fioGetVector(fioImgH, x, y, z);
				*pfH = fH;
			}
		}
	}
}



//
// __computePixelJointEntropy()
//
// Compute the joint entropy for two images, arbitrary number of features.
//
//void
//__computePixelJointEntropy(
//					 FEATUREIO	&fioImg1,
//					 FEATUREIO	&fioImg2,
//					 FEATUREIO	&fioImgH
//					  )
//{
//	assert( fioImg1.x == fioImgH.x );
//	assert( fioImg1.y == fioImgH.y );
//	assert( fioImg1.z == fioImgH.z );
//	assert( fioImg1.t == fioImgH.t == 1 );
//	assert( fioImgH.iFeaturesPerVector == 1 );
//
//	//
//	// Each voxel in fioImg1 & fioImg2 represents a Gaussian-weighted 
//	// histogram of intensities at that location.
//	//
//
//	for( int z = 0; z < fioImg1.z; z++ )
//	{
//		for( int y = 0; y < fioImg1.y; y++ )
//		{
//			for( int x = 0; x < fioImg1.x; x++ )
//			{
//				float *pfProb1 = fioGetVector( fioImg1, x, y, z );
//				float *pfProb2 = fioGetVector( fioImg2, x, y, z );
//				float fProbSum = 0;
//				float fH = 0;
//
//				for( int iF1 = 0; iF1 < fioImg1.iFeaturesPerVector; iF1++ )
//				{
//					for( int iF2 = 0; iF2 < fioImg2.iFeaturesPerVector; iF2++ )
//					{
//						float fProb = pfProb1[iF1]*pfProb2[iF2];
//						fProbSum += pfProb[k];
//						if( pfProb[k] > 0 )
//						{
//							fH -= pfProb[k]*log(pfProb[k]);
//						}
//					}
//				}
//				for( int k = 0; k < fioImgIn.iFeaturesPerVector; k++ )
//				{
//					fProbSum += pfProb[k];
//					if( pfProb[k] > 0 )
//					{
//						fH -= pfProb[k]*log(pfProb[k]);
//					}
//				}
//				// Calculate last remaining probability
//				float fProb = 1.0f - fProbSum;
//				assert( fProb <= 1.0f );
//				if( fProb > 0 )
//				{
//					fH -= fProb*log(fProb);
//				}
//				float *pfH = fioGetVector( fioImgH, x, y, z );
//				*pfH = fH;
//			}
//		}
//	}
//}

//
// __computePixelMutualInfo()
//
// Compute the mutual information for voxels in images, arbitrary number of features.
// fioImg contains.
//
void
__computePixelMutualInfo(
	FEATUREIO  *fioImgArray,
	int iI1Count,
	int iI2Count,
	FEATUREIO	&fioImgM
)
{
	assert(fioImgArray[0].x == fioImgM.x);
	assert(fioImgArray[0].y == fioImgM.y);
	assert(fioImgArray[0].z == fioImgM.z);
	assert(fioImgArray[0].t == fioImgM.t == 1);
	assert(fioImgM.iFeaturesPerVector == 1);
	//assert( iI1Count*iI2Count == fioImg.iFeaturesPerVector );

	//
	// Each voxel in fioImg & fioImg2 represents a Gaussian-weighted 
	// histogram of intensities at that location.
	//

	float pfProbMarg1[20];
	float pfProbMarg2[20];

	for (int z = 0; z < fioImgM.z; z++)
	{
		for (int y = 0; y < fioImgM.y; y++)
		{
			for (int x = 0; x < fioImgM.x; x++)
			{
				float fProbSum;
				float fH12 = 0;
				float fH1 = 0;
				float fH2 = 0;

				for (int iF1 = 0; iF1 < iI1Count; iF1++) pfProbMarg1[iF1] = 0;
				for (int iF2 = 0; iF2 < iI2Count; iF2++) pfProbMarg2[iF2] = 0;

				fProbSum = 0;
				for (int iF1 = 0; iF1 < iI1Count; iF1++)
				{
					for (int iF2 = 0; iF2 < iI2Count; iF2++)
					{
						float *pfProbVec = fioGetVector(fioImgArray[iF1*iI1Count + iF2], x, y, z);
						float fProb = *pfProbVec;
						fProbSum += fProb;
						if (fProb > 0)
						{
							fH12 -= fProb*log(fProb);
						}
					}
				}

				for (int iF1 = 0; iF1 < iI1Count; iF1++)
				{
					for (int iF2 = 0; iF2 < iI2Count; iF2++)
					{
						float *pfProbVec = fioGetVector(fioImgArray[iF1*iI1Count + iF2], x, y, z);
						float fProb = *pfProbVec;
						pfProbMarg1[iF1] += fProb;
					}
				}

				for (int iF2 = 0; iF2 < iI2Count; iF2++)
				{
					for (int iF1 = 0; iF1 < iI1Count; iF1++)
					{
						float *pfProbVec = fioGetVector(fioImgArray[iF1*iI1Count + iF2], x, y, z);
						float fProb = *pfProbVec;
						pfProbMarg2[iF2] += fProb;
					}
				}

				fProbSum = 0;
				for (int iF1 = 0; iF1 < iI1Count; iF1++)
				{
					float fProb = pfProbMarg1[iF1];
					fProbSum += fProb;
					if (fProb > 0)
					{
						fH1 -= fProb*log(fProb);
					}
				}
				fProbSum = 0;
				for (int iF2 = 0; iF2 < iI2Count; iF2++)
				{
					float fProb = pfProbMarg2[iF2];
					fProbSum += fProb;
					if (fProb > 0)
					{
						fH2 -= fProb*log(fProb);
					}
				}

				float *pfM = fioGetVector(fioImgM, x, y, z);
				*pfM = fH1 + fH2 - fH12;
			}
		}
	}
}

void
__computePixelMutualInfoInterleaved(
	FEATUREIO	&fioImg,
	int iI1Count,
	int iI2Count,
	FEATUREIO	&fioImgM
)
{
	assert(fioImg.x == fioImgM.x);
	assert(fioImg.y == fioImgM.y);
	assert(fioImg.z == fioImgM.z);
	assert(fioImg.t == fioImgM.t == 1);
	assert(fioImgM.iFeaturesPerVector == 1);
	assert(iI1Count*iI2Count == fioImg.iFeaturesPerVector);

	//
	// Each voxel in fioImg & fioImg2 represents a Gaussian-weighted 
	// histogram of intensities at that location.
	//

	float pfProbMarg1[20];
	float pfProbMarg2[20];

	for (int z = 0; z < fioImg.z; z++)
	{
		for (int y = 0; y < fioImg.y; y++)
		{
			for (int x = 0; x < fioImg.x; x++)
			{
				float *pfProbVec = fioGetVector(fioImg, x, y, z);
				float fProbSum;
				float fH12 = 0;
				float fH1 = 0;
				float fH2 = 0;

				for (int iF1 = 0; iF1 < iI1Count; iF1++) pfProbMarg1[iF1] = 0;
				for (int iF2 = 0; iF2 < iI2Count; iF2++) pfProbMarg2[iF2] = 0;

				fProbSum = 0;
				for (int iF1 = 0; iF1 < iI1Count; iF1++)
				{
					for (int iF2 = 0; iF2 < iI2Count; iF2++)
					{
						float fProb = pfProbVec[iF1*iI1Count + iF2];
						fProbSum += fProb;
						if (fProb > 0)
						{
							fH12 -= fProb*log(fProb);
						}
					}
				}

				for (int iF1 = 0; iF1 < iI1Count; iF1++)
				{
					for (int iF2 = 0; iF2 < iI2Count; iF2++)
					{
						float fProb = pfProbVec[iF1*iI1Count + iF2];
						pfProbMarg1[iF1] += fProb;
					}
				}

				for (int iF2 = 0; iF2 < iI2Count; iF2++)
				{
					for (int iF1 = 0; iF1 < iI1Count; iF1++)
					{
						float fProb = pfProbVec[iF1*iI1Count + iF2];
						pfProbMarg2[iF2] += fProb;
					}
				}

				fProbSum = 0;
				for (int iF1 = 0; iF1 < iI1Count; iF1++)
				{
					float fProb = pfProbMarg1[iF1];
					fProbSum += fProb;
					if (fProb > 0)
					{
						fH1 -= fProb*log(fProb);
					}
				}
				fProbSum = 0;
				for (int iF2 = 0; iF2 < iI2Count; iF2++)
				{
					float fProb = pfProbMarg2[iF2];
					fProbSum += fProb;
					if (fProb > 0)
					{
						fH2 -= fProb*log(fProb);
					}
				}

				float *pfM = fioGetVector(fioImgM, x, y, z);
				*pfM = fH1 + fH2 - fH12;
			}
		}
	}
}


int
msGeneratePyramidEntropy3D(
	FEATUREIO	&fioImg,
	vector<Feature3D> &vecFeats,
	float fInitialImageScale,
	int bDense,
	int bOutput
)
{
	int iScaleCount = 4;
	char pcFileName[300];

	PpImage ppImgOut;
	PpImage ppImgOutMem;
	ppImgOutMem.Initialize(1, fioImg.x*fioImg.y*fioImg.z, fioImg.x*fioImg.y*fioImg.z * sizeof(float), sizeof(float) * 8);

	// Allocate temporary arrays for blurring

	FEATUREIO fioTemp;
	FEATUREIO fioImgBase;
	fioImgBase = fioTemp = fioImg;
	fioImgBase.pfVectors = fioTemp.pfVectors = 0;
	fioAllocate(fioTemp);
	fioAllocate(fioImgBase);

	LOCATION_VALUE_XYZ_ARRAY lvaMaxima, lvaMinima;
	lvaMaxima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	lvaMinima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	assert(lvaMaxima.plvz);
	assert(lvaMinima.plvz);

	int i;

	// Blur pyramid is a 3D FEATUREIO
	FEATUREIO fioStack;
	FEATUREIO fioBlurs[BLURS_TOTAL];
	initFIOStack(fioImg, fioStack, (FEATUREIO*)&fioBlurs, BLURS_TOTAL);

	// Entropy pyramid, one for each blur
	FEATUREIO fioStackH;
	FEATUREIO fioBlursH[BLURS_TOTAL];
	initFIOStack(fioImg, fioStackH, (FEATUREIO*)&fioBlursH, BLURS_TOTAL);

	// Mutual information pyramid, one for each blur - 1
	FEATUREIO fioStackMI;
	FEATUREIO fioBlursMI[BLURS_TOTAL];
	initFIOStack(fioImg, fioStackMI, (FEATUREIO*)&fioBlursMI, BLURS_TOTAL);

	// DOG pyramid
	FEATUREIO fioStackDOG;
	FEATUREIO fioDOGs[BLURS_TOTAL - 1];
	initFIOStack(fioImg, fioStackDOG, (FEATUREIO*)&fioDOGs, BLURS_TOTAL - 1);

	// Edges 
	FEATUREIO fioEdgeStack;
	FEATUREIO fioEdgeXYZ[3];
	initFIOStack(fioImg, fioEdgeStack, (FEATUREIO*)&fioEdgeXYZ, 3);

	//
	// Here, the initial blur sigma is calculated along with the subsequent
	// sigma increments.
	//
	//float fSigma = 1.0f;
	float fSigmaInit = 0.5; // initial blurring: image resolution
	if (fInitialImageScale > 0) fSigmaInit /= fInitialImageScale;
	float fSigma = 1.6f; // desired initial blurring
	float fSigmaFactor = (float)pow(2.0, 1.0 / (double)BLURS_PER_OCTAVE);

	// Start initial blur: blur_2^2 = blur_1^2 + fSigmaExtra^2
	float fSigmaExtra = sqrt(fSigma*fSigma - fSigmaInit*fSigmaInit);
	gb3d_blur3d(fioImg, fioTemp, fioBlurs[0], fSigmaExtra, BLUR_PRECISION);

	// Output initial image
	ppImgOut.InitializeSubImage(
		fioImg.y,
		fioImg.x,
		fioImg.x * sizeof(float), sizeof(float) * 8,
		(unsigned char*)ppImgOutMem.ImageRow(0)
	);
	fioFeatureSliceXY(fioImg, fioImg.z / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "image.pgm");
	if (bOutput) output_float(ppImgOut, pcFileName);

	float fScale = fInitialImageScale;

	float pfBlurSigmas[BLURS_TOTAL];

	int iFeatDiff = 1;
	for (int i = 0; 1; i++)
	{
		printf("Scale %d: blur...", i);

		fSigma = 1.6f; // desired initial blurring

		pfBlurSigmas[0] = fSigma;

		// Compute entropy for lowest level
		__computePixelEntropyBinary(fioBlurs[0], fioBlursH[0]);

		// Generate blurs in octave
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			// How much extra blur required to raise fSigma-blurred image
			// to next bur level
			float fSigmaExtra = fSigma*sqrt(fSigmaFactor*fSigmaFactor - 1.0f);

			// Diffuse distributions p(I|x) at level (j-1) to p(I) at level (j) using p(x), fSigmaExtra
			// p(I) = sum_i p(I|x_i)p(x_i)
			int iReturn = gb3d_blur3d(fioBlurs[j - 1], fioTemp, fioBlurs[j], fSigmaExtra, BLUR_PRECISION);
			if (iReturn == 0)
				goto finish_up;

			// Compute entropy H(I) of p(I) at level (j)
			__computePixelEntropyBinary(fioBlurs[j], fioBlursH[j]);

			// Compute conditional entropy of p(I|x) by diffusing H(I|x): H(I|x) = sum_i p(x_i) H(I|x_i)
			//gb3d_blur3d( fioBlursH[j-1], fioTemp, fioBlursMI[j], fSigmaExtra, BLUR_PRECISION );

			fSigma *= fSigmaFactor;
			pfBlurSigmas[j] = fSigma;
		}

		// Output images to see
		for (int j = 0; j < BLURS_TOTAL; j++)
		{
			ppImgOut.InitializeSubImage(
				fioBlurs[j].y,
				fioBlurs[j].x,
				fioBlurs[j].x * sizeof(float), sizeof(float) * 8,
				(unsigned char*)ppImgOutMem.ImageRow(0)
			);
			fioFeatureSliceXY(fioBlurs[j], fioBlurs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "blur%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);

			ppImgOut.InitializeSubImage(
				fioBlursH[j].y,
				fioBlursH[j].x,
				fioBlursH[j].x * sizeof(float), sizeof(float) * 8,
				(unsigned char*)ppImgOutMem.ImageRow(0)
			);
			fioFeatureSliceXY(fioBlursH[j], fioBlursH[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "info%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);

			if (j > 0)
			{
				ppImgOut.InitializeSubImage(
					fioBlursMI[j].y,
					fioBlursMI[j].x,
					fioBlursMI[j].x * sizeof(float), sizeof(float) * 8,
					(unsigned char*)ppImgOutMem.ImageRow(0)
				);
				fioFeatureSliceXY(fioBlursMI[j], fioBlursMI[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
				sprintf(pcFileName, "conditional_info%2.2d.pgm", j);
				if (bOutput) output_float(ppImgOut, pcFileName);
			}
		}

		// Save 2*sigma image
		fioCopy(fioTemp, fioBlurs[BLURS_PER_OCTAVE]);

		printf("diff...");

		// Create difference of Gaussian
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			// fioMultSum( fioBlursH[j], fioBlursMI[j], fioDOGs[j-1], -1.0f );
			//fioMultSum( fioBlursH[j], fioBlursH[j-1], fioDOGs[j-1], -1.0f ); // Difference-of-entropy criterion
			fioMultSum(fioBlursH[j], fioBlursH[j - 1], fioDOGs[j - 1], 0.0f); // Max entropy criterion - simply pass image

																			  // Multiply by current scale
			float fSigma = pfBlurSigmas[j];
			fioMult(fioDOGs[j - 1], fioDOGs[j - 1], fSigma);
		}

		// Output dogs to see
		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			ppImgOut.InitializeSubImage(fioDOGs[j].y, fioDOGs[j].x, fioDOGs[j].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
			fioFeatureSliceXY(fioDOGs[j], fioDOGs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "mutual_info%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);
		}

		printf("peaks...");

		// Detect peaks and create features
		int iFeatsCurr = vecFeats.size();
		for (int j = 0; j < BLURS_PER_OCTAVE; j++)
		{
			// This is the only place yet there is a difference in 2D/3D

			// Perform peak selection
			detectExtrema4D(
				fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
				lvaMinima,
				lvaMaxima
			);

			// Generate edges, for orientation
			fioGenerateEdgeImages3D(
				fioBlurs[j + 1],
				fioEdgeXYZ[0], fioEdgeXYZ[1], fioEdgeXYZ[2]
			);

			// Output edge images to see
			int k;
			for (k = 0; k < 3; k++)
			{
				ppImgOut.InitializeSubImage(fioEdgeXYZ[k].z, fioEdgeXYZ[k].y, fioEdgeXYZ[k].y * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
				fioFeatureSliceZY(fioEdgeXYZ[k], fioEdgeXYZ[k].x / 2, 0, (float*)ppImgOut.ImageRow(0));
				sprintf(pcFileName, "edge%2.2d.pgm", k);
				if (bOutput) output_float(ppImgOut, pcFileName);
			}

			// Generate features for points found
			int iFirstFeat = vecFeats.size();
			lvaMinima.iCount = 0; // Only consider MI maxima Jan15,2010: it makes sense to consider both max/min of entropy change
								  //lvaMaxima.iCount = 0; // Only consider MI minima
			generateFeatures3D(
				lvaMinima, lvaMaxima,
				fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
				pfBlurSigmas[j], pfBlurSigmas[j + 1], pfBlurSigmas[j + 2],
				fioBlurs[j + 1], // MICCAI 09, NeuroImage 09
				fioEdgeXYZ[0],
				fioEdgeXYZ[1],
				fioEdgeXYZ[2],
				pfBlurSigmas[j + 1],
				vecFeats
			);

			// Update geometry to image space
			float fFactor = fScale;
			float fAddend = 0;//fFactor/2.0f;
			for (int iFU = iFirstFeat; iFU < vecFeats.size(); iFU++)
			{
				// Update feature scale to actual
				vecFeats[iFU].scale *= fFactor;
				// Should also update location...
				vecFeats[iFU].x = vecFeats[iFU].x*fFactor + fAddend;
				vecFeats[iFU].y = vecFeats[iFU].y*fFactor + fAddend;
				vecFeats[iFU].z = vecFeats[iFU].z*fFactor + fAddend;
			}
		}

		iFeatDiff = vecFeats.size() - iFeatsCurr;
		printf("%d...", vecFeats.size() - iFeatsCurr);

		printf("subsample...");

		fioBlurs[0].x /= 2;
		fioBlurs[0].y /= 2;
		fioBlurs[0].z /= 2;
		// Subsample sigma=2 image into first position
		//fioSubSample( fioTemp, fioBlurs[0] );
		fioSubSampleInterpolate(fioTemp, fioBlurs[0]);

		//ppImgOut.InitializeSubImage( fioBlurs[0].z, fioBlurs[0].y, fioBlurs[0].y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioBlurs[0],  fioBlurs[0].x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "blur_first%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		//ppImgOut.InitializeSubImage( fioTemp.z, fioTemp.y, fioTemp.y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioTemp,  fioTemp.x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "temp_after%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		// Halve dimensions of all blur images
		// fioBlurs[0] is already done above
		fioTemp.x /= 2;
		fioTemp.y /= 2;
		fioTemp.z /= 2;
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			fioBlurs[j].x /= 2;
			fioBlurs[j].y /= 2;
			fioBlurs[j].z /= 2;
			int iElementsPerImage = fioBlurs[j].z*fioBlurs[j].y*fioBlurs[j].x;
			fioBlurs[j].pfVectors = fioStack.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < BLURS_TOTAL; j++)
		{
			fioBlursH[j].x /= 2;
			fioBlursH[j].y /= 2;
			fioBlursH[j].z /= 2;
			int iElementsPerImage = fioBlursH[j].z*fioBlursH[j].y*fioBlursH[j].x;
			fioBlursH[j].pfVectors = fioStackH.pfVectors + j*iElementsPerImage;

			fioBlursMI[j].x /= 2;
			fioBlursMI[j].y /= 2;
			fioBlursMI[j].z /= 2;
			iElementsPerImage = fioBlursMI[j].z*fioBlursMI[j].y*fioBlursMI[j].x;
			fioBlursMI[j].pfVectors = fioStackMI.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			fioDOGs[j].x /= 2;
			fioDOGs[j].y /= 2;
			fioDOGs[j].z /= 2;
			int iElementsPerImage = fioDOGs[j].z*fioDOGs[j].y*fioDOGs[j].x;
			fioDOGs[j].pfVectors = fioStackDOG.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < 3; j++)
		{
			fioEdgeXYZ[j].x /= 2;
			fioEdgeXYZ[j].y /= 2;
			fioEdgeXYZ[j].z /= 2;
			int iElementsPerImage = fioEdgeXYZ[j].z*fioEdgeXYZ[j].y*fioEdgeXYZ[j].x;
			fioEdgeXYZ[j].pfVectors = fioEdgeStack.pfVectors + j*iElementsPerImage;
		}

		fScale *= 2.0f;

		printf("done.\n");
	}

finish_up:

	delete[] lvaMaxima.plvz;
	delete[] lvaMinima.plvz;

	fioDelete(fioEdgeStack);
	fioDelete(fioStackDOG);
	fioDelete(fioStackH);
	fioDelete(fioStackMI);
	fioDelete(fioStack);
	fioDelete(fioTemp);

	return 1;
}

int
msGeneratePyramidMutualInfo3D(
	FEATUREIO	&fioImg,
	vector<Feature3D> &vecFeats,
	float fInitialImageScale,
	int bDense,
	int bOutput
)
{
	int iScaleCount = 4;
	char pcFileName[300];

	PpImage ppImgOut;
	PpImage ppImgOutMem;
	ppImgOutMem.Initialize(1, fioImg.x*fioImg.y*fioImg.z, fioImg.x*fioImg.y*fioImg.z * sizeof(float), sizeof(float) * 8);

	// Allocate temporary arrays for blurring

	FEATUREIO fioTemp;
	FEATUREIO fioImgBase;
	fioImgBase = fioTemp = fioImg;
	fioImgBase.pfVectors = fioTemp.pfVectors = 0;
	fioAllocate(fioTemp);
	fioAllocate(fioImgBase);

	LOCATION_VALUE_XYZ_ARRAY lvaMaxima, lvaMinima;
	lvaMaxima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	lvaMinima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	assert(lvaMaxima.plvz);
	assert(lvaMinima.plvz);

	int i;

	// Blur pyramid is a 3D FEATUREIO
	FEATUREIO fioStack;
	FEATUREIO fioBlurs[BLURS_TOTAL];
	initFIOStack(fioImg, fioStack, (FEATUREIO*)&fioBlurs, BLURS_TOTAL);

	// Entropy pyramid, one for each blur
	FEATUREIO fioStackH;
	FEATUREIO fioBlursH[BLURS_TOTAL];
	initFIOStack(fioImg, fioStackH, (FEATUREIO*)&fioBlursH, BLURS_TOTAL);

	// Mutual information pyramid, one for each blur - 1
	FEATUREIO fioStackMI;
	FEATUREIO fioBlursMI[BLURS_TOTAL];
	initFIOStack(fioImg, fioStackMI, (FEATUREIO*)&fioBlursMI, BLURS_TOTAL);

	// DOG pyramid
	FEATUREIO fioStackDOG;
	FEATUREIO fioDOGs[BLURS_TOTAL - 1];
	initFIOStack(fioImg, fioStackDOG, (FEATUREIO*)&fioDOGs, BLURS_TOTAL - 1);

	// Edges 
	FEATUREIO fioEdgeStack;
	FEATUREIO fioEdgeXYZ[3];
	initFIOStack(fioImg, fioEdgeStack, (FEATUREIO*)&fioEdgeXYZ, 3);

	//
	// Here, the initial blur sigma is calculated along with the subsequent
	// sigma increments.
	//
	//float fSigma = 1.0f;
	float fSigmaInit = 0.5; // initial blurring: image resolution
	if (fInitialImageScale > 0) fSigmaInit /= fInitialImageScale;
	float fSigma = 1.6f; // desired initial blurring
	float fSigmaFactor = (float)pow(2.0, 1.0 / (double)BLURS_PER_OCTAVE);

	// Start initial blur: blur_2^2 = blur_1^2 + fSigmaExtra^2
	float fSigmaExtra = sqrt(fSigma*fSigma - fSigmaInit*fSigmaInit);
	gb3d_blur3d(fioImg, fioTemp, fioBlurs[0], fSigmaExtra, BLUR_PRECISION);

	// Output initial image
	ppImgOut.InitializeSubImage(
		fioImg.z,
		fioImg.y,
		fioImg.y * sizeof(float), sizeof(float) * 8,
		(unsigned char*)ppImgOutMem.ImageRow(0)
	);
	fioFeatureSliceZY(fioImg, fioImg.x / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "image.pgm");
	if (bOutput) output_float(ppImgOut, pcFileName);

	float fScale = fInitialImageScale;

	float pfBlurSigmas[BLURS_TOTAL];

	int iFeatDiff = 1;
	for (int i = 0; 1; i++)
	{
		printf("Scale %d: blur...", i);

		fSigma = 1.6f; // desired initial blurring

		pfBlurSigmas[0] = fSigma;

		// Compute entropy for lowest level
		__computePixelEntropyBinary(fioBlurs[0], fioBlursH[0]);

		// Generate blurs in octave
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			// How much extra blur required to raise fSigma-blurred image
			// to next bur level
			float fSigmaExtra = fSigma*sqrt(fSigmaFactor*fSigmaFactor - 1.0f);

			// Diffuse distributions p(I|x) at level (j-1) to p(I) at level (j) using p(x), fSigmaExtra
			// p(I) = sum_i p(I|x_i)p(x_i)
			int iReturn = gb3d_blur3d(fioBlurs[j - 1], fioTemp, fioBlurs[j], fSigmaExtra, BLUR_PRECISION);
			if (iReturn == 0)
				goto finish_up;

			// Compute entropy H(I) of p(I) at level (j)
			__computePixelEntropyBinary(fioBlurs[j], fioBlursH[j]);

			// Compute conditional entropy of p(I|x) by diffusing H(I|x): H(I|x) = sum_i p(x_i) H(I|x_i)
			gb3d_blur3d(fioBlursH[j - 1], fioTemp, fioBlursMI[j], fSigmaExtra, BLUR_PRECISION); // This is commented out in entropy features

			fSigma *= fSigmaFactor;
			pfBlurSigmas[j] = fSigma;
		}

		// Output images to see
		for (int j = 0; j < BLURS_TOTAL; j++)
		{
			ppImgOut.InitializeSubImage(
				fioBlurs[j].y,
				fioBlurs[j].x,
				fioBlurs[j].x * sizeof(float), sizeof(float) * 8,
				(unsigned char*)ppImgOutMem.ImageRow(0)
			);
			fioFeatureSliceXY(fioBlurs[j], fioBlurs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "blur%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);

			ppImgOut.InitializeSubImage(
				fioBlursH[j].y,
				fioBlursH[j].x,
				fioBlursH[j].x * sizeof(float), sizeof(float) * 8,
				(unsigned char*)ppImgOutMem.ImageRow(0)
			);
			fioFeatureSliceXY(fioBlursH[j], fioBlursH[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "info%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);

			if (j > 0)
			{
				ppImgOut.InitializeSubImage(
					fioBlursMI[j].y,
					fioBlursMI[j].x,
					fioBlursMI[j].x * sizeof(float), sizeof(float) * 8,
					(unsigned char*)ppImgOutMem.ImageRow(0)
				);
				fioFeatureSliceXY(fioBlursMI[j], fioBlursMI[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
				sprintf(pcFileName, "conditional_info%2.2d.pgm", j);
				if (bOutput) output_float(ppImgOut, pcFileName);
			}
		}

		// Save 2*sigma image
		fioCopy(fioTemp, fioBlurs[BLURS_PER_OCTAVE]);

		printf("diff...");

		// Create difference of Gaussian
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			fioMultSum(fioBlursH[j], fioBlursMI[j], fioDOGs[j - 1], -1.0f);
			//fioMultSum( fioBlursH[j], fioBlursH[j-1], fioDOGs[j-1], -1.0f ); // For entropy features

			// Multiply by current scale
			float fSigma = pfBlurSigmas[j];
			fioMult(fioDOGs[j - 1], fioDOGs[j - 1], fSigma);
		}

		// Output dogs to see
		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			ppImgOut.InitializeSubImage(fioDOGs[j].y, fioDOGs[j].x, fioDOGs[j].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
			fioFeatureSliceXY(fioDOGs[j], fioDOGs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "mutual_info%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);
		}

		printf("peaks...");

		// Detect peaks and create features
		int iFeatsCurr = vecFeats.size();
		for (int j = 0; j < BLURS_PER_OCTAVE; j++)
		{
			// This is the only place yet there is a difference in 2D/3D

			// Perform peak selection
			detectExtrema4D(
				fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
				lvaMinima,
				lvaMaxima
			);

			// Generate edges, for orientation
			fioGenerateEdgeImages3D(
				fioBlurs[j + 1],
				fioEdgeXYZ[0], fioEdgeXYZ[1], fioEdgeXYZ[2]
			);

			// Output edge images to see
			int k;
			for (k = 0; k < 3; k++)
			{
				ppImgOut.InitializeSubImage(fioEdgeXYZ[k].z, fioEdgeXYZ[k].y, fioEdgeXYZ[k].y * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
				fioFeatureSliceZY(fioEdgeXYZ[k], fioEdgeXYZ[k].x / 2, 0, (float*)ppImgOut.ImageRow(0));
				sprintf(pcFileName, "edge%2.2d.pgm", k);
				if (bOutput) output_float(ppImgOut, pcFileName);
			}

			// Generate features for points found
			int iFirstFeat = vecFeats.size();
			lvaMinima.iCount = 0; // Only consider MI maxima
								  //lvaMaxima.iCount = 0; // Only consider MI minima
			generateFeatures3D(
				lvaMinima, lvaMaxima,
				fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
				pfBlurSigmas[j], pfBlurSigmas[j + 1], pfBlurSigmas[j + 2],
				fioBlurs[j + 1], //MICCAI 09, NeuroImage 09
				fioEdgeXYZ[0],
				fioEdgeXYZ[1],
				fioEdgeXYZ[2],
				pfBlurSigmas[j + 1],
				vecFeats
			);

			// Update geometry to image space
			float fFactor = fScale;
			float fAddend = 0;//fFactor/2.0f;
			for (int iFU = iFirstFeat; iFU < vecFeats.size(); iFU++)
			{
				// Update feature scale to actual
				vecFeats[iFU].scale *= fFactor;
				// Should also update location...
				vecFeats[iFU].x = vecFeats[iFU].x*fFactor + fAddend;
				vecFeats[iFU].y = vecFeats[iFU].y*fFactor + fAddend;
				vecFeats[iFU].z = vecFeats[iFU].z*fFactor + fAddend;
			}
		}

		iFeatDiff = vecFeats.size() - iFeatsCurr;
		printf("%d...", vecFeats.size() - iFeatsCurr);

		printf("subsample...");

		fioBlurs[0].x /= 2;
		fioBlurs[0].y /= 2;
		fioBlurs[0].z /= 2;
		// Subsample sigma=2 image into first position
		//fioSubSample( fioTemp, fioBlurs[0] );
		fioSubSampleInterpolate(fioTemp, fioBlurs[0]);

		//ppImgOut.InitializeSubImage( fioBlurs[0].z, fioBlurs[0].y, fioBlurs[0].y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioBlurs[0],  fioBlurs[0].x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "blur_first%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		//ppImgOut.InitializeSubImage( fioTemp.z, fioTemp.y, fioTemp.y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioTemp,  fioTemp.x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "temp_after%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		// Halve dimensions of all blur images
		// fioBlurs[0] is already done above
		fioTemp.x /= 2;
		fioTemp.y /= 2;
		fioTemp.z /= 2;
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			fioBlurs[j].x /= 2;
			fioBlurs[j].y /= 2;
			fioBlurs[j].z /= 2;
			int iElementsPerImage = fioBlurs[j].z*fioBlurs[j].y*fioBlurs[j].x;
			fioBlurs[j].pfVectors = fioStack.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < BLURS_TOTAL; j++)
		{
			fioBlursH[j].x /= 2;
			fioBlursH[j].y /= 2;
			fioBlursH[j].z /= 2;
			int iElementsPerImage = fioBlursH[j].z*fioBlursH[j].y*fioBlursH[j].x;
			fioBlursH[j].pfVectors = fioStackH.pfVectors + j*iElementsPerImage;

			fioBlursMI[j].x /= 2;
			fioBlursMI[j].y /= 2;
			fioBlursMI[j].z /= 2;
			iElementsPerImage = fioBlursMI[j].z*fioBlursMI[j].y*fioBlursMI[j].x;
			fioBlursMI[j].pfVectors = fioStackMI.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			fioDOGs[j].x /= 2;
			fioDOGs[j].y /= 2;
			fioDOGs[j].z /= 2;
			int iElementsPerImage = fioDOGs[j].z*fioDOGs[j].y*fioDOGs[j].x;
			fioDOGs[j].pfVectors = fioStackDOG.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < 3; j++)
		{
			fioEdgeXYZ[j].x /= 2;
			fioEdgeXYZ[j].y /= 2;
			fioEdgeXYZ[j].z /= 2;
			int iElementsPerImage = fioEdgeXYZ[j].z*fioEdgeXYZ[j].y*fioEdgeXYZ[j].x;
			fioEdgeXYZ[j].pfVectors = fioEdgeStack.pfVectors + j*iElementsPerImage;
		}

		fScale *= 2.0f;

		printf("done.\n");
	}

finish_up:

	delete[] lvaMaxima.plvz;
	delete[] lvaMinima.plvz;

	fioDelete(fioEdgeStack);
	fioDelete(fioStackDOG);
	fioDelete(fioStackH);
	fioDelete(fioStackMI);
	fioDelete(fioStack);
	fioDelete(fioTemp);

	return 1;
}


//int
//msGeneratePyramidTwoImgMutualInfo3D(
//					 FEATUREIO	&fioImg1, // Interleaved features?
//					 FEATUREIO	&fioImg2,
//					 vector<Feature3D> &vecFeats,
//					 int bDense,
//					 float fInitialImageScale,
//					 int bOutput
//					 )
//{
//	int iScaleCount = 4;
//	char pcFileName[300];
//	
//	PpImage ppImgOut;
//	PpImage ppImgOutMem;
//	ppImgOutMem.Initialize( 1, fioImg.x*fioImg.y*fioImg.z, fioImg.x*fioImg.y*fioImg.z*sizeof(float), sizeof(float)*8 );
//
//	// Allocate temporary arrays for blurring
//	FEATUREIO fioTemp;
//	FEATUREIO fioImgBase;
//	fioImgBase = fioTemp = fioImg;
//	fioImgBase.iFeaturesPerVector = fioTemp.iFeaturesPerVector = 1;
//	fioImgBase.pfVectors = fioTemp.pfVectors = 0;
//	fioAllocate( fioTemp );
//	fioAllocate( fioImgBase );
//
//	LOCATION_VALUE_XYZ_ARRAY lvaMaxima, lvaMinima;
//	lvaMaxima.plvz = new LOCATION_VALUE_XYZ[fioImg1.x*fioImg1.y];
//	lvaMinima.plvz = new LOCATION_VALUE_XYZ[fioImg1.x*fioImg1.y];
//	assert( lvaMaxima.plvz );
//	assert( lvaMinima.plvz );
//	
//	int i;
//
//	// Primary difference
//	//	 - blur stacks are only at 1 level of the pyramid
//	FEATUREIO fioBlur1 = fioImg1;
//	fioAllocate( fioBlur1 );
//	FEATUREIO fioBlur2 = fioImg2;
//	fioAllocate( fioBlur2 );
//	FEATUREIO fioBlurTemp = fioImg2;
//	if( fioImg2.iFeaturesPerVector < fioImg1.iFeaturesPerVector )
//	{
//		// Allocate max possible number of features
//		fioBlurTemp.iFeaturesPerVector = fioImg1.iFeaturesPerVector;
//	}
//	fioAllocate( fioBlurTemp );
//
//	// Keep a temporary backup stack for changing octaves
//	FEATUREIO fioExample; // Example of a stack image (for dimensions)
//	fioExample = fioImg1;
//	fioExample.x /= 2; // Halve image dimensions
//	fioExample.y /= 2;
//	fioExample.z /= 2;
//	FEATUREIO fioBlur1Backup = fioExample;
//	FEATUREIO fioBlur2Backup = fioExample;
//	fioAllocate( fioBlur1Backup );
//	fioAllocate( fioBlur2Backup );
//
//	// Info/mutual info pyramid, one for each blur - 
//	FEATUREIO fioStackH;
//	FEATUREIO fioBlursH[BLURS_TOTAL];
//	fioExample = fioImg1;
//	fioExample.iFeaturesPerVector = 1;
//	initFIOStack( fioImg1, fioStackH, (FEATUREIO*)&fioBlursH, BLURS_TOTAL );
//
//	// DOG pyramid
//	FEATUREIO fioStackDOG;
//	FEATUREIO fioDOGs[BLURS_TOTAL-1];
//	fioExample = fioImg1;
//	fioExample.iFeaturesPerVector = 1;
//	initFIOStack( fioExample, fioStackDOG, (FEATUREIO*)&fioDOGs, BLURS_TOTAL-1 );
//
//	//
//	// Here, the initial blur sigma is calculated along with the subsequent
//	// sigma increments.
//	//
//	//float fSigma = 1.0f;
//	float fSigmaInit = 0.5; // initial blurring: image resolution
//	if( fInitialImageScale > 0 ) fSigmaInit /= fInitialImageScale;
//	float fSigma = 1.6f; // desired initial blurring
//	float fSigmaFactor = (float)pow( 2.0, 1.0/(double)BLURS_PER_OCTAVE );
//
//	// Start initial blur: blur_2^2 = blur_1^2 + fSigmaExtra^2
//	float fSigmaExtra = sqrt(fSigma*fSigma - fSigmaInit*fSigmaInit);
//	for( int iF1 = 0; iF1 < fioImg1.iFeaturesPerVector; iF1++ )
//	{
//		gb3d_blur3d_interleave( fioImg1, fioTemp, fioBlurTemp, iF1, fSigmaExtra, BLUR_PRECISION );
//	}
//	fioCopy( fioBlur1, fioBlurTemp );
//
//	for( int iF2 = 0; iF2 < fioImg2.iFeaturesPerVector; iF2++ )
//	{
//		gb3d_blur3d_interleave( fioImg2, fioTemp, fioBlurTemp, iF2, fSigmaExtra, BLUR_PRECISION );
//	}
//	fioCopy( fioBlur2, fioBlurTemp );
//
//	// Output initial image
//	ppImgOut.InitializeSubImage(
//		fioImg1.z,
//		fioImg1.y,
//		fioImg1.y*sizeof(float), sizeof(float)*8,
//		(unsigned char*)ppImgOutMem.ImageRow(0)
//		);
//	fioFeatureSliceZY( fioImg1,  fioImg1.x/2, 0, (float*)ppImgOut.ImageRow(0) );
//	sprintf( pcFileName, "image1.pgm" );
//	if( bOutput ) output_float( ppImgOut, pcFileName );
//	fioFeatureSliceZY( fioImg2,  fioImg2.x/2, 0, (float*)ppImgOut.ImageRow(0) );
//	sprintf( pcFileName, "image2.pgm" );
//	if( bOutput ) output_float( ppImgOut, pcFileName );
//
//	float fScale = fInitialImageScale;
//
//	float pfBlurSigmas[BLURS_TOTAL];
//
//	int iFeatDiff = 1;
//	for( int i = 0; 1; i++ )
//	{
//		printf( "Scale %d: blur...", i );
//
//		fSigma = 1.6f; // desired initial blurring
//
//		pfBlurSigmas[0] = fSigma;
//
//		// Generate entropy/MI for blur 0
//
//		__computePixelJointEntropy( fioBlur1, fioBlur2, fioBlursH[0] );
//
//		// Generate blurs in octave
//
//		for( int j = 1; j < BLURS_TOTAL; j++ )
//		{
//			// How much extra blur required to raise fSigma-blurred image
//			// to next bur level
//			float fSigmaExtra = fSigma*sqrt(fSigmaFactor*fSigmaFactor - 1.0f);
//
//			// Diffuse distributions p(I|x) at level (j-1) to p(I) at level (j) using p(x), fSigmaExtra
//			// p(I) = sum_i p(I|x_i)p(x_i)
//
//			for( int iF1 = 0; iF1 < fioImg1.iFeaturesPerVector; iF1++ )
//			{
//				int iReturn = gb3d_blur3d_interleave( fioBlur1, fioTemp, fioBlurTemp, iF1, fSigmaExtra, BLUR_PRECISION );
//				if( iReturn == 0 )
//					goto finish_up;
//			}
//			fioCopy( fioBlur1, fioBlurTemp );
//
//			for( int iF2 = 0; iF2 < fioImg2.iFeaturesPerVector; iF2++ )
//			{
//				int iReturn = gb3d_blur3d_interleave( fioBlur2, fioTemp, fioBlurTemp, iF2, fSigmaExtra, BLUR_PRECISION );
//				if( iReturn == 0 )
//					goto finish_up;
//			}
//			fioCopy( fioBlur2, fioBlurTemp );
//
//			if( j == BLURS_PER_OCTAVE )
//			{
//				// Save images at this point, for next octave
//				fioSubSample( fioBlur1, fioBlur1Backup );
//				fioSubSample( fioBlur2, fioBlur2Backup );
//			}
//
//			// Calculate entropy
//			
//			__computePixelJointEntropy( fioBlur1, fioBlur2, fioBlursH[j] );
//
//			fSigma *= fSigmaFactor;
//			pfBlurSigmas[j] = fSigma;
//		}
//
//		// Output images to see
//		for( int j = 0; j < BLURS_TOTAL; j++ )
//		{	
//			ppImgOut.InitializeSubImage(
//				fioBlurs[j].y,
//				fioBlurs[j].x,
//				fioBlurs[j].x*sizeof(float), sizeof(float)*8,
//				(unsigned char*)ppImgOutMem.ImageRow(0)
//				);
//			fioFeatureSliceXY( fioBlurs[j],  fioBlurs[j].z/2, 0, (float*)ppImgOut.ImageRow(0) );
//			sprintf( pcFileName, "blur%2.2d.pgm", j );
//			if( bOutput ) output_float( ppImgOut, pcFileName );	
//
//			ppImgOut.InitializeSubImage(
//				fioBlursH[j].y,
//				fioBlursH[j].x,
//				fioBlursH[j].x*sizeof(float), sizeof(float)*8,
//				(unsigned char*)ppImgOutMem.ImageRow(0)
//				);
//			fioFeatureSliceXY( fioBlursH[j],  fioBlursH[j].z/2, 0, (float*)ppImgOut.ImageRow(0) );
//			sprintf( pcFileName, "info%2.2d.pgm", j );
//			if( bOutput ) output_float( ppImgOut, pcFileName );
//		}
//
//		// Save 2*sigma image
//		//fioCopy( fioTemp, fioBlurs[BLURS_PER_OCTAVE] );
//
//		printf( "diff..." );
//
//		// Create difference of Gaussian
//		for( int j = 1; j < BLURS_TOTAL; j++ )
//		{
//			//fioMultSum( fioBlursH[j], fioBlursMI[j], fioDOGs[j-1], -1.0f );
//			//fioMultSum( fioBlursH[j], fioBlursH[j-1], fioDOGs[j-1], -1.0f ); // For entropy features
//			fioCopy( fioDOGs[j-1], fioBlursH[j] );
//
//			// Multiply by current scale
//			float fSigma = pfBlurSigmas[j];
//			fioMult( fioDOGs[j-1], fioDOGs[j-1], fSigma );
//		}
//
//		// Output dogs to see
//		for( int j = 0; j < BLURS_TOTAL-1; j++ )
//		{	
//			ppImgOut.InitializeSubImage( fioDOGs[j].y, fioDOGs[j].x, fioDOGs[j].x*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
//			fioFeatureSliceXY( fioDOGs[j],  fioDOGs[j].z/2, 0, (float*)ppImgOut.ImageRow(0) );
//			sprintf( pcFileName, "mutual_info%2.2d.pgm", j );
//			if( bOutput ) output_float( ppImgOut, pcFileName );
//		}
//
//		printf( "peaks..." );
//
//		// Detect peaks and create features
//		int iFeatsCurr = vecFeats.size();
//		for( int j = 0; j < BLURS_PER_OCTAVE; j++ )
//		{
//			// This is the only place yet there is a difference in 2D/3D
//
//			// Perform peak selection
//			detectExtrema4D(
//				fioDOGs[j], fioDOGs[j+1], fioDOGs[j+2],
//				lvaMinima,
//				lvaMaxima
//				);
//
//			// Generate edges, for orientation
//			fioGenerateEdgeImages3D(
//				fioBlurs[j+1],
//				fioEdgeXYZ[0], fioEdgeXYZ[1], fioEdgeXYZ[2]
//				);
//
//			// Output edge images to see
//			int k;
//			for( k = 0; k < 3; k++ )
//			{	
//				ppImgOut.InitializeSubImage( fioEdgeXYZ[k].z, fioEdgeXYZ[k].y, fioEdgeXYZ[k].y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
//				fioFeatureSliceZY( fioEdgeXYZ[k],  fioEdgeXYZ[k].x/2, 0, (float*)ppImgOut.ImageRow(0) );
//				sprintf( pcFileName, "edge%2.2d.pgm", k );
//				if( bOutput ) output_float( ppImgOut, pcFileName );
//			}
//
//			// Generate features for points found
//			int iFirstFeat = vecFeats.size();
//			lvaMinima.iCount = 0; // Only consider MI maxima
//			//lvaMaxima.iCount = 0; // Only consider MI minima
//			generateFeatures3D(
//				lvaMinima, lvaMaxima,
//				fioDOGs[j], fioDOGs[j+1], fioDOGs[j+2],
//				pfBlurSigmas[j],pfBlurSigmas[j+1],pfBlurSigmas[j+2],
//				fioBlurs[j+1], //MICCAI 09, NeuroImage 09
//				fioEdgeXYZ[0],
//				fioEdgeXYZ[1],
//				fioEdgeXYZ[2],
//				pfBlurSigmas[j+1],
//				vecFeats
//				);
//
//			// Update geometry to image space
//			float fFactor = fScale;
//			float fAddend = fFactor/2.0f;
//			for( int iFU = iFirstFeat; iFU < vecFeats.size(); iFU++ )
//			{
//				// Update feature scale to actual
//				vecFeats[iFU].scale *= fFactor;
//				// Should also update location...
//				vecFeats[iFU].x = vecFeats[iFU].x*fFactor + fAddend;
//				vecFeats[iFU].y = vecFeats[iFU].y*fFactor + fAddend;
//				vecFeats[iFU].z = vecFeats[iFU].z*fFactor + fAddend;
//			}
//		}
//
//		iFeatDiff = vecFeats.size() - iFeatsCurr;
//		printf( "%d...", vecFeats.size() - iFeatsCurr );
//
//		printf( "subsample..." );
//
//		fioBlurs[0].x /= 2;
//		fioBlurs[0].y /= 2;
//		fioBlurs[0].z /= 2;	
//		// Subsample sigma=2 image into first position
//		fioSubSample( fioTemp, fioBlurs[0] );
//
//		//ppImgOut.InitializeSubImage( fioBlurs[0].z, fioBlurs[0].y, fioBlurs[0].y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
//		//fioFeatureSliceZY( fioBlurs[0],  fioBlurs[0].x/2, 0, (float*)ppImgOut.ImageRow(0) );
//		//sprintf( pcFileName, "blur_first%2.2d.pgm", j );
//		//output_float( ppImgOut, pcFileName );
//
//		//ppImgOut.InitializeSubImage( fioTemp.z, fioTemp.y, fioTemp.y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
//		//fioFeatureSliceZY( fioTemp,  fioTemp.x/2, 0, (float*)ppImgOut.ImageRow(0) );
//		//sprintf( pcFileName, "temp_after%2.2d.pgm", j );
//		//output_float( ppImgOut, pcFileName );
//
//		// Halve dimensions of all blur images
//		// fioBlurs[0] is already done above
//		fioTemp.x /= 2;
//		fioTemp.y /= 2;
//		fioTemp.z /= 2;
//		for( int j = 1; j < BLURS_TOTAL; j++ )
//		{
//			fioBlurs[j].x /= 2;
//			fioBlurs[j].y /= 2;
//			fioBlurs[j].z /= 2;
//			int iElementsPerImage = fioBlurs[j].z*fioBlurs[j].y*fioBlurs[j].x;
//			fioBlurs[j].pfVectors = fioStack.pfVectors + j*iElementsPerImage;
//		}
//
//		for( int j = 0; j < BLURS_TOTAL; j++ )
//		{
//			fioBlursH[j].x /= 2;
//			fioBlursH[j].y /= 2;
//			fioBlursH[j].z /= 2;
//			int iElementsPerImage = fioBlursH[j].z*fioBlursH[j].y*fioBlursH[j].x;
//			fioBlursH[j].pfVectors = fioStackH.pfVectors + j*iElementsPerImage;
//			
//			fioBlursMI[j].x /= 2;
//			fioBlursMI[j].y /= 2;
//			fioBlursMI[j].z /= 2;
//			iElementsPerImage = fioBlursMI[j].z*fioBlursMI[j].y*fioBlursMI[j].x;
//			fioBlursMI[j].pfVectors = fioStackMI.pfVectors + j*iElementsPerImage;
//		}
//
//		for( int j = 0; j < BLURS_TOTAL-1; j++ )
//		{
//			fioDOGs[j].x /= 2;
//			fioDOGs[j].y /= 2;
//			fioDOGs[j].z /= 2;
//			int iElementsPerImage = fioDOGs[j].z*fioDOGs[j].y*fioDOGs[j].x;
//			fioDOGs[j].pfVectors = fioStackDOG.pfVectors + j*iElementsPerImage;
//		}
//
//		for( int j = 0; j < 3; j++ )
//		{
//			fioEdgeXYZ[j].x /= 2;
//			fioEdgeXYZ[j].y /= 2;
//			fioEdgeXYZ[j].z /= 2;
//			int iElementsPerImage = fioEdgeXYZ[j].z*fioEdgeXYZ[j].y*fioEdgeXYZ[j].x;
//			fioEdgeXYZ[j].pfVectors = fioEdgeStack.pfVectors + j*iElementsPerImage;
//		}
//
//		fScale *= 2.0f;
//
//		printf( "done.\n" );
//	}
//
//finish_up:
//
//	delete [] lvaMaxima.plvz;
//	delete [] lvaMinima.plvz;
//
//	fioDelete( fioEdgeStack );
//	fioDelete( fioStackDOG );
//	fioDelete( fioStackH );
//	fioDelete( fioStackMI );
//	fioDelete( fioStack );
//	fioDelete( fioTemp );
//
//	return 1;
//}


int
msGeneratePyramidTwoImgMutualInfo3DCondIndepIJ(
	FEATUREIO	&fioImg1, // Interleaved features?
	FEATUREIO	&fioImg2,
	vector<Feature3D> &vecFeats,
	float fInitialImageScale,
	int bDense,
	int bOutput
)
{
	int iScaleCount = 4;
	char pcFileName[300];

	// Create joint histogram image
	int iI1Count = fioImg1.iFeaturesPerVector;
	int iI2Count = fioImg2.iFeaturesPerVector;

	FEATUREIO &fioImg = fioImg1; // Reference to keep from changing througout function

	PpImage ppImgOut;
	PpImage ppImgOutMem;
	ppImgOutMem.Initialize(1, fioImg.x*fioImg.y*fioImg.z, fioImg.x*fioImg.y*fioImg.z * sizeof(float), sizeof(float) * 8);

	// Temporary array for blurring
	FEATUREIO fioTemp; // temporary
	fioTemp = fioImg;
	fioTemp.iFeaturesPerVector = 1;
	fioAllocate(fioTemp);
	FEATUREIO fioTemp2; // temporary
	fioTemp2 = fioImg;
	fioTemp2.iFeaturesPerVector = 1;
	fioAllocate(fioTemp2);

	LOCATION_VALUE_XYZ_ARRAY lvaMaxima, lvaMinima;
	lvaMaxima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	lvaMinima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	assert(lvaMaxima.plvz);
	assert(lvaMinima.plvz);

	int i;

	// Mutual information pyramid, one for each blur - 1
	FEATUREIO fioExample;
	fioExample = fioImg;
	fioExample.iFeaturesPerVector = 1;
	FEATUREIO fioStackMI;
	FEATUREIO fioBlursMI[BLURS_TOTAL];
	initFIOStack(fioExample, fioStackMI, (FEATUREIO*)&fioBlursMI, BLURS_TOTAL);

	// Blur image stack
	fioExample = fioImg;
	fioExample.iFeaturesPerVector = 1;
	FEATUREIO fioStackBlurs;
	FEATUREIO *fioBlurs = new FEATUREIO[iI1Count*iI2Count];
	initFIOStack(fioExample, fioStackBlurs, (FEATUREIO*)fioBlurs, iI1Count*iI2Count);
	fioInitJointImageArray(fioImg1, fioImg2, fioBlurs);

	// Subsample save stack, 1/2 size
	fioExample = fioImg;
	fioExample.iFeaturesPerVector = 1;
	fioExample.x = fioExample.x / 2;
	fioExample.y = fioExample.y / 2;
	fioExample.z = fioExample.z / 2 + 1;
	FEATUREIO fioStackSubSamples;
	FEATUREIO *fioSubSamples = new FEATUREIO[iI1Count*iI2Count];
	initFIOStack(fioExample, fioStackSubSamples, (FEATUREIO*)fioSubSamples, iI1Count*iI2Count);

	// Set reference
	FEATUREIO *fioDOGs = (FEATUREIO *)&(fioBlursMI[0]);

	//
	// Here, the initial blur sigma is calculated along with the subsequent
	// sigma increments.
	//
	//float fSigma = 1.0f;
	float fSigmaInit = 0.5; // initial blurring: image resolution
	if (fInitialImageScale > 0) fSigmaInit /= fInitialImageScale;
	float fSigma = 1.6f; // desired initial blurring
	float fSigmaFactor = (float)pow(2.0, 1.0 / (double)BLURS_PER_OCTAVE);

	// Start initial blur: blur_2^2 = blur_1^2 + fSigmaExtra^2
	float fSigmaExtra = sqrt(fSigma*fSigma - fSigmaInit*fSigmaInit);

	// Output initial image
	ppImgOut.InitializeSubImage(
		fioImg.y,
		fioImg.x,
		fioImg.x * sizeof(float), sizeof(float) * 8,
		(unsigned char*)ppImgOutMem.ImageRow(0)
	);
	for (int k = 0; k < iI1Count*iI2Count; k++)
	{
		fioFeatureSliceXY(fioBlurs[k], fioImg.z / 2, 0, (float*)ppImgOut.ImageRow(0));
		sprintf(pcFileName, "image_%d.pgm", k);
		if (bOutput) output_float(ppImgOut, pcFileName);

		fioCopy(fioTemp, fioBlurs[k]);
		gb3d_blur3d(fioTemp, fioTemp2, fioBlurs[k], fSigmaExtra, BLUR_PRECISION);

		fioFeatureSliceXY(fioBlurs[k], fioImg.z / 2, 0, (float*)ppImgOut.ImageRow(0));
		sprintf(pcFileName, "image_%d_aft.pgm", k);
		if (bOutput) output_float(ppImgOut, pcFileName);
	}

	float fScale = fInitialImageScale;

	float pfBlurSigmas[BLURS_TOTAL];

	int iFeatDiff = 1;
	for (int i = 0; 1; i++)
	{
		printf("Scale %d: blur...", i);

		fSigma = 1.6f; // desired initial blurring

		pfBlurSigmas[0] = fSigma;

		// Compute mutual info for lowest level

		fioNormalizeJointImageArray(iI1Count, iI2Count, fioBlurs);
		__computePixelMutualInfo(fioBlurs, iI1Count, iI2Count, fioBlursMI[0]);

		// Generate next blurs/mutual infos
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			// How much extra blur required to raise fSigma-blurred image
			// to next bur level
			float fSigmaExtra = fSigma*sqrt(fSigmaFactor*fSigmaFactor - 1.0f);

			// Diffuse distributions p(I|x) at level (j-1) to p(I) at level (j) using p(x), fSigmaExtra
			// p(I) = sum_i p(I|x_i)p(x_i)
			for (int k = 0; k < iI1Count*iI2Count; k++)
			{
				fioFeatureSliceXY(fioBlurs[k], fioBlurs[k].z / 2, 0, (float*)ppImgOut.ImageRow(0));
				sprintf(pcFileName, "image_i_before_blur%d.pgm", k);
				if (bOutput) output_float(ppImgOut, pcFileName);

				fioCopy(fioTemp, fioBlurs[k]);


				fioFeatureSliceXY(fioTemp, fioTemp.z / 2, 0, (float*)ppImgOut.ImageRow(0));
				sprintf(pcFileName, "image_i_before_blur_temp%d.pgm", k);
				if (bOutput) output_float(ppImgOut, pcFileName);


				int iReturn = gb3d_blur3d(fioTemp, fioTemp2, fioBlurs[k], fSigmaExtra, BLUR_PRECISION);
				if (iReturn == 0)
					goto finish_up;

				fioFeatureSliceXY(fioBlurs[k], fioBlurs[k].z / 2, 0, (float*)ppImgOut.ImageRow(0));
				sprintf(pcFileName, "image_i_after_blur%d.pgm", k);
				if (bOutput) output_float(ppImgOut, pcFileName);
			}

			if (j == BLURS_PER_OCTAVE)
			{
				// Save 2*sigma image
				for (int k = 0; k < iI1Count*iI2Count; k++)
				{
					fioSet(fioSubSamples[k], 0);
					//fioSubSample( fioBlurs[k], fioSubSamples[k] );
					fioSubSampleInterpolate(fioBlurs[k], fioSubSamples[k]);
				}

				//ppImgOut.InitializeSubImage( fioSubSamples[4].y, fioSubSamples[4].x, fioSubSamples[4].x*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
				//fioFeatureSliceXY( fioSubSamples[4],  fioSubSamples[4].z/2, 0, (float*)ppImgOut.ImageRow(0) );
				//sprintf( pcFileName, "fioSubSample%2.2d.pgm", j );
				//if( bOutput ) output_float( ppImgOut, pcFileName );
			}

			fioNormalizeJointImageArray(iI1Count, iI2Count, fioBlurs);
			__computePixelMutualInfo(fioBlurs, iI1Count, iI2Count, fioBlursMI[j]);

			fSigma *= fSigmaFactor;
			pfBlurSigmas[j] = fSigma;

			//ppImgOut.InitializeSubImage( fioBlurs[4].y, fioBlurs[4].x, fioBlurs[4].x*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
			//fioFeatureSliceXY( fioBlurs[4],  fioBlurs[4].z/2, 0, (float*)ppImgOut.ImageRow(0) );
			//sprintf( pcFileName, "blur%2.2d.pgm", j );
			//if( bOutput ) output_float( ppImgOut, pcFileName );
		}

		printf("diff...");

		// Scale normalize
		for (int j = 0; j < BLURS_TOTAL; j++)
		{
			// Multiply by current scale
			float fSigma = pfBlurSigmas[j];
			fioMult(fioDOGs[j], fioDOGs[j], fSigma);
		}

		// Output dogs to see
		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			ppImgOut.InitializeSubImage(fioDOGs[j].y, fioDOGs[j].x, fioDOGs[j].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
			fioFeatureSliceXY(fioDOGs[j], fioDOGs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "mutual_info%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);
		}

		printf("peaks...");

		// Detect peaks and create features
		int iFeatsCurr = vecFeats.size();
		for (int j = 0; j < BLURS_PER_OCTAVE; j++)
		{
			// This is the only place yet there is a difference in 2D/3D

			// Perform peak selection
			if (fioImg.z = 1)
			{
				detectExtrema3D(
					fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
					lvaMinima,
					lvaMaxima
				);
			}
			else
			{
				detectExtrema4D(
					fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
					lvaMinima,
					lvaMaxima
				);
			}

			// Generate features for points found
			int iFirstFeat = vecFeats.size();
			lvaMinima.iCount = 0; // Only consider MI maxima Jan15,2010: it makes sense to consider both max/min of entropy change
								  //lvaMaxima.iCount = 0; // Only consider MI minima
			generateFeatures3D(
				lvaMinima, lvaMaxima,
				fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
				pfBlurSigmas[j], pfBlurSigmas[j + 1], pfBlurSigmas[j + 2],
				pfBlurSigmas[j + 1],

				fioBlurs, iI1Count*iI2Count,

				vecFeats
			);

			// Update geometry to image space
			float fFactor = fScale;
			float fAddend = 0;//fFactor/2.0f;
			for (int iFU = iFirstFeat; iFU < vecFeats.size(); iFU++)
			{
				// Update feature scale to actual
				vecFeats[iFU].scale *= fFactor;
				// Should also update location...
				vecFeats[iFU].x = vecFeats[iFU].x*fFactor + fAddend;
				vecFeats[iFU].y = vecFeats[iFU].y*fFactor + fAddend;
				vecFeats[iFU].z = vecFeats[iFU].z*fFactor + fAddend;
			}
		}

		iFeatDiff = vecFeats.size() - iFeatsCurr;
		printf("%d...", vecFeats.size() - iFeatsCurr);

		printf("subsample...");

		// Copy save image into subsampled position
		for (int k = 0; k < iI1Count*iI2Count; k++)
		{

			ppImgOut.InitializeSubImage(fioBlurs[k].y, fioBlurs[k].x, fioBlurs[k].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
			fioFeatureSliceXY(fioBlurs[k], fioBlurs[k].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "subsamp_blur_before%2.2d.pgm", k);
			if (bOutput) output_float(ppImgOut, pcFileName);

			fioBlurs[k].x = fioBlurs[k].x / 2;
			fioBlurs[k].y = fioBlurs[k].y / 2;
			fioBlurs[k].z = fioBlurs[k].z / 2 + 1;

			fioCopy(fioBlurs[k], fioSubSamples[k]);

			ppImgOut.InitializeSubImage(fioBlurs[k].y, fioBlurs[k].x, fioBlurs[k].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
			fioFeatureSliceXY(fioBlurs[k], fioBlurs[k].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "subsamp_blur_after%2.2d.pgm", k);
			if (bOutput) output_float(ppImgOut, pcFileName);

			ppImgOut.InitializeSubImage(fioSubSamples[k].y, fioSubSamples[k].x, fioSubSamples[k].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
			fioFeatureSliceXY(fioSubSamples[k], fioSubSamples[k].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "subsamp_subsamp%2.2d.pgm", k);
			if (bOutput) output_float(ppImgOut, pcFileName);

			fioSubSamples[k].x = fioSubSamples[k].x / 2;
			fioSubSamples[k].y = fioSubSamples[k].y / 2;
			fioSubSamples[k].z = fioSubSamples[k].z / 2 + 1;
		}

		//ppImgOut.InitializeSubImage( fioBlurs[0].z, fioBlurs[0].y, fioBlurs[0].y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioBlurs[0],  fioBlurs[0].x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "blur_first%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		//ppImgOut.InitializeSubImage( fioTemp.z, fioTemp.y, fioTemp.y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioTemp,  fioTemp.x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "temp_after%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		// Halve dimensions of all blur images
		// fioBlurs[0] is already done above
		fioTemp.x = fioTemp.x / 2;
		fioTemp.y = fioTemp.y / 2;
		fioTemp.z = fioTemp.z / 2 + 1;
		fioTemp2.x = fioTemp2.x / 2;
		fioTemp2.y = fioTemp2.y / 2;
		fioTemp2.z = fioTemp2.z / 2 + 1;
		for (int j = 0; j < BLURS_TOTAL; j++)
		{
			fioBlursMI[j].x = fioBlursMI[j].x / 2;
			fioBlursMI[j].y = fioBlursMI[j].y / 2;
			fioBlursMI[j].z = fioBlursMI[j].z / 2 + 1;
			//int iElementsPerImage = fioBlursMI[j].z*fioBlursMI[j].y*fioBlursMI[j].x;
			//fioBlursMI[j].pfVectors = fioStackMI.pfVectors + j*iElementsPerImage;
		}

		fScale *= 2.0f;

		printf("done.\n");
	}

finish_up:

	delete[] lvaMaxima.plvz;
	delete[] lvaMinima.plvz;

	fioDelete(fioStackMI);
	fioDelete(fioStackSubSamples);
	fioDelete(fioStackBlurs);
	fioDelete(fioTemp);

	return 1;
}


int
msGeneratePyramidTwoImgMutualInfo3DIndepIJ(
	FEATUREIO	&fioImg1, // Interleaved features?
	FEATUREIO	&fioImg2,
	vector<Feature3D> &vecFeats,
	int bDense,
	float fInitialImageScale,
	int bOutput
)
{
	int iScaleCount = 4;
	char pcFileName[300];

	// Create joint histogram image
	int iI1Count = fioImg1.iFeaturesPerVector;
	int iI2Count = fioImg2.iFeaturesPerVector;

	FEATUREIO &fioImg = fioImg1; // Reference to keep from changing througout function

	PpImage ppImgOut;
	PpImage ppImgOutMem;
	ppImgOutMem.Initialize(1, fioImg.x*fioImg.y*fioImg.z, fioImg.x*fioImg.y*fioImg.z * sizeof(float), sizeof(float) * 8);

	// Temporary array for blurring
	FEATUREIO fioTemp; // temporary
	fioTemp = fioImg;
	fioTemp.iFeaturesPerVector = 1;
	fioAllocate(fioTemp);
	FEATUREIO fioTemp2; // temporary
	fioTemp2 = fioImg;
	fioTemp2.iFeaturesPerVector = 1;
	fioAllocate(fioTemp2);

	LOCATION_VALUE_XYZ_ARRAY lvaMaxima, lvaMinima;
	lvaMaxima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	lvaMinima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	assert(lvaMaxima.plvz);
	assert(lvaMinima.plvz);

	int i;

	// Mutual information pyramid, one for each blur - 1
	FEATUREIO fioExample;
	fioExample = fioImg;
	fioExample.iFeaturesPerVector = 1;
	FEATUREIO fioStackMI;
	FEATUREIO fioBlursMI[BLURS_TOTAL];
	initFIOStack(fioExample, fioStackMI, (FEATUREIO*)&fioBlursMI, BLURS_TOTAL);

	// Blur image stack
	fioExample = fioImg;
	fioExample.iFeaturesPerVector = 1;
	FEATUREIO fioStackBlurs;
	FEATUREIO *fioBlurs = new FEATUREIO[iI1Count*iI2Count];
	initFIOStack(fioExample, fioStackBlurs, (FEATUREIO*)fioBlurs, iI1Count*iI2Count);
	//fioInitJointImageArray( fioImg1, fioImg2, fioBlurs );

	// Blurs for image 1
	fioExample = fioImg;
	fioExample.iFeaturesPerVector = 1;
	FEATUREIO fioStackBlurs1;
	FEATUREIO *fioBlurs1 = new FEATUREIO[iI1Count];
	initFIOStack(fioExample, fioStackBlurs1, (FEATUREIO*)fioBlurs1, iI1Count);

	// Blurs for image 2
	fioExample = fioImg;
	fioExample.iFeaturesPerVector = 1;
	FEATUREIO fioStackBlurs2;
	FEATUREIO *fioBlurs2 = new FEATUREIO[iI2Count];
	initFIOStack(fioExample, fioStackBlurs2, (FEATUREIO*)fioBlurs2, iI2Count);

	// Subsample save stack for image 1, 1/2 size
	fioExample = fioImg;
	fioExample.iFeaturesPerVector = 1;
	fioExample.x = fioExample.x / 2;
	fioExample.y = fioExample.y / 2;
	fioExample.z = fioExample.z / 2 + 1;
	FEATUREIO fioStackSubSamples1;
	FEATUREIO *fioSubSamples1 = new FEATUREIO[iI1Count];
	initFIOStack(fioExample, fioStackSubSamples1, (FEATUREIO*)fioSubSamples1, iI1Count);

	// Subsample save stack for image 1, 1/2 size
	fioExample = fioImg;
	fioExample.iFeaturesPerVector = 1;
	fioExample.x = fioExample.x / 2;
	fioExample.y = fioExample.y / 2;
	fioExample.z = fioExample.z / 2 + 1;
	FEATUREIO fioStackSubSamples2;
	FEATUREIO *fioSubSamples2 = new FEATUREIO[iI2Count];
	initFIOStack(fioExample, fioStackSubSamples2, (FEATUREIO*)fioSubSamples2, iI2Count);



	// Set reference
	FEATUREIO *fioDOGs = (FEATUREIO *)&(fioBlursMI[0]);

	//
	// Here, the initial blur sigma is calculated along with the subsequent
	// sigma increments.
	//
	//float fSigma = 1.0f;
	float fSigmaInit = 0.5; // initial blurring: image resolution
	if (fInitialImageScale > 0) fSigmaInit /= fInitialImageScale;
	float fSigma = 1.6f; // desired initial blurring
	float fSigmaFactor = (float)pow(2.0, 1.0 / (double)BLURS_PER_OCTAVE);

	// Start initial blur: blur_2^2 = blur_1^2 + fSigmaExtra^2
	float fSigmaExtra = sqrt(fSigma*fSigma - fSigmaInit*fSigmaInit);

	// Output initial image
	ppImgOut.InitializeSubImage(
		fioImg.y,
		fioImg.x,
		fioImg.x * sizeof(float), sizeof(float) * 8,
		(unsigned char*)ppImgOutMem.ImageRow(0)
	);
	for (int k = 0; k < iI1Count; k++)
	{
		fioCopy(fioBlurs1[k], fioImg1, 0, k);

		fioFeatureSliceXY(fioBlurs1[k], fioImg.z / 2, 0, (float*)ppImgOut.ImageRow(0));
		sprintf(pcFileName, "image_%d.pgm", k);
		if (bOutput) output_float(ppImgOut, pcFileName);

		fioCopy(fioTemp, fioBlurs1[k]);
		gb3d_blur3d(fioTemp, fioTemp2, fioBlurs1[k], fSigmaExtra, BLUR_PRECISION);

		fioFeatureSliceXY(fioBlurs1[k], fioImg.z / 2, 0, (float*)ppImgOut.ImageRow(0));
		sprintf(pcFileName, "image_%d_aft.pgm", k);
		if (bOutput) output_float(ppImgOut, pcFileName);
	}
	for (int k = 0; k < iI2Count; k++)
	{
		fioCopy(fioBlurs2[k], fioImg2, 0, k);

		fioFeatureSliceXY(fioBlurs2[k], fioImg.z / 2, 0, (float*)ppImgOut.ImageRow(0));
		sprintf(pcFileName, "image_%d.pgm", k);
		if (bOutput) output_float(ppImgOut, pcFileName);

		fioCopy(fioTemp, fioBlurs2[k]);
		gb3d_blur3d(fioTemp, fioTemp2, fioBlurs2[k], fSigmaExtra, BLUR_PRECISION);

		fioFeatureSliceXY(fioBlurs2[k], fioImg.z / 2, 0, (float*)ppImgOut.ImageRow(0));
		sprintf(pcFileName, "image_%d_aft.pgm", k);
		if (bOutput) output_float(ppImgOut, pcFileName);
	}

	float fScale = fInitialImageScale;

	float pfBlurSigmas[BLURS_TOTAL];

	int iFeatDiff = 1;
	for (int i = 0; 1; i++)
	{
		printf("Scale %d: blur...", i);

		fSigma = 1.6f; // desired initial blurring

		pfBlurSigmas[0] = fSigma;

		// Compute mutual info for lowest level
		fioInitJointImageArray(fioBlurs1, fioBlurs2, iI1Count, iI2Count, fioBlurs);
		fioNormalizeJointImageArray(iI1Count, iI2Count, fioBlurs);
		__computePixelMutualInfo(fioBlurs, iI1Count, iI2Count, fioBlursMI[0]);

		// Generate next blurs/mutual infos
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			// How much extra blur required to raise fSigma-blurred image
			// to next bur level
			float fSigmaExtra = fSigma*sqrt(fSigmaFactor*fSigmaFactor - 1.0f);

			// Diffuse distributions p(I|x) at level (j-1) to p(I) at level (j) using p(x), fSigmaExtra
			// p(I) = sum_i p(I|x_i)p(x_i)
			for (int k = 0; k < iI1Count; k++)
			{
				fioCopy(fioTemp, fioBlurs1[k]);
				int iReturn = gb3d_blur3d(fioTemp, fioTemp2, fioBlurs1[k], fSigmaExtra, BLUR_PRECISION);
				if (iReturn == 0)
					goto finish_up;
			}
			for (int k = 0; k < iI2Count; k++)
			{
				fioCopy(fioTemp, fioBlurs2[k]);
				int iReturn = gb3d_blur3d(fioTemp, fioTemp2, fioBlurs2[k], fSigmaExtra, BLUR_PRECISION);
				if (iReturn == 0)
					goto finish_up;
			}

			if (j == BLURS_PER_OCTAVE)
			{
				// Save 2*sigma image
				for (int k = 0; k < iI1Count; k++)
				{
					//fioSubSample( fioBlurs1[k], fioSubSamples1[k] );
					fioSubSampleInterpolate(fioBlurs1[k], fioSubSamples1[k]);
				}
				for (int k = 0; k < iI2Count; k++)
				{
					//fioSubSample( fioBlurs2[k], fioSubSamples2[k] );
					fioSubSampleInterpolate(fioBlurs2[k], fioSubSamples2[k]);
				}
			}

			fioInitJointImageArray(fioBlurs1, fioBlurs2, iI1Count, iI2Count, fioBlurs);
			fioNormalizeJointImageArray(iI1Count, iI2Count, fioBlurs);
			__computePixelMutualInfo(fioBlurs, iI1Count, iI2Count, fioBlursMI[j]);

			fSigma *= fSigmaFactor;
			pfBlurSigmas[j] = fSigma;

			ppImgOut.InitializeSubImage(fioBlurs[2].y, fioBlurs[2].x, fioBlurs[2].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
			fioFeatureSliceXY(fioBlurs[2], fioBlurs[2].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "blur%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);
		}

		printf("diff...");

		// Scale normalize
		for (int j = 0; j < BLURS_TOTAL; j++)
		{
			// Multiply by current scale
			float fSigma = pfBlurSigmas[j];
			fioMult(fioDOGs[j], fioDOGs[j], fSigma);
		}

		// Output dogs to see
		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			ppImgOut.InitializeSubImage(fioDOGs[j].y, fioDOGs[j].x, fioDOGs[j].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
			fioFeatureSliceXY(fioDOGs[j], fioDOGs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "mutual_info%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);
		}

		printf("peaks...");

		// Detect peaks and create features
		int iFeatsCurr = vecFeats.size();
		for (int j = 0; j < BLURS_PER_OCTAVE; j++)
		{
			// This is the only place yet there is a difference in 2D/3D

			// Perform peak selection
			if (fioImg.z = 1)
			{
				detectExtrema3D(
					fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
					lvaMinima,
					lvaMaxima
				);
			}
			else
			{
				detectExtrema4D(
					fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
					lvaMinima,
					lvaMaxima
				);
			}

			// Generate features for points found
			int iFirstFeat = vecFeats.size();
			lvaMinima.iCount = 0; // Only consider MI maxima Jan15,2010: it makes sense to consider both max/min of entropy change
								  //lvaMaxima.iCount = 0; // Only consider MI minima
			generateFeatures3D(
				lvaMinima, lvaMaxima,
				fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
				pfBlurSigmas[j], pfBlurSigmas[j + 1], pfBlurSigmas[j + 2],
				pfBlurSigmas[j + 1],

				fioBlurs, iI1Count*iI2Count,

				vecFeats
			);

			// Update geometry to image space
			float fFactor = fScale;
			float fAddend = 0;//fFactor/2.0f;
			for (int iFU = iFirstFeat; iFU < vecFeats.size(); iFU++)
			{
				// Update feature scale to actual
				vecFeats[iFU].scale *= fFactor;
				// Should also update location...
				vecFeats[iFU].x = vecFeats[iFU].x*fFactor + fAddend;
				vecFeats[iFU].y = vecFeats[iFU].y*fFactor + fAddend;
				vecFeats[iFU].z = vecFeats[iFU].z*fFactor + fAddend;
			}
		}

		iFeatDiff = vecFeats.size() - iFeatsCurr;
		printf("%d...", vecFeats.size() - iFeatsCurr);

		printf("subsample...");

		// Copy save image into subsampled position
		for (int k = 0; k < iI1Count*iI2Count; k++)
		{
			fioBlurs[k].x = fioBlurs[k].x / 2;
			fioBlurs[k].y = fioBlurs[k].y / 2;
			fioBlurs[k].z = fioBlurs[k].z / 2 + 1;
		}

		for (int k = 0; k < iI1Count; k++)
		{
			fioBlurs1[k].x = fioBlurs1[k].x / 2;
			fioBlurs1[k].y = fioBlurs1[k].y / 2;
			fioBlurs1[k].z = fioBlurs1[k].z / 2 + 1;

			fioCopy(fioBlurs1[k], fioSubSamples1[k]);

			fioSubSamples1[k].x = fioSubSamples1[k].x / 2;
			fioSubSamples1[k].y = fioSubSamples1[k].y / 2;
			fioSubSamples1[k].z = fioSubSamples1[k].z / 2 + 1;
		}

		for (int k = 0; k < iI2Count; k++)
		{
			fioBlurs2[k].x = fioBlurs2[k].x / 2;
			fioBlurs2[k].y = fioBlurs2[k].y / 2;
			fioBlurs2[k].z = fioBlurs2[k].z / 2 + 1;

			fioCopy(fioBlurs2[k], fioSubSamples2[k]);

			fioSubSamples2[k].x = fioSubSamples2[k].x / 2;
			fioSubSamples2[k].y = fioSubSamples2[k].y / 2;
			fioSubSamples2[k].z = fioSubSamples2[k].z / 2 + 1;
		}



		//ppImgOut.InitializeSubImage( fioBlurs[0].z, fioBlurs[0].y, fioBlurs[0].y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioBlurs[0],  fioBlurs[0].x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "blur_first%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		//ppImgOut.InitializeSubImage( fioTemp.z, fioTemp.y, fioTemp.y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioTemp,  fioTemp.x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "temp_after%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		// Halve dimensions of all blur images
		// fioBlurs[0] is already done above
		fioTemp.x = fioTemp.x / 2;
		fioTemp.y = fioTemp.y / 2;
		fioTemp.z = fioTemp.z / 2 + 1;
		fioTemp2.x = fioTemp2.x / 2;
		fioTemp2.y = fioTemp2.y / 2;
		fioTemp2.z = fioTemp2.z / 2 + 1;
		for (int j = 0; j < BLURS_TOTAL; j++)
		{
			fioBlursMI[j].x = fioBlursMI[j].x / 2;
			fioBlursMI[j].y = fioBlursMI[j].y / 2;
			fioBlursMI[j].z = fioBlursMI[j].z / 2 + 1;
			int iElementsPerImage = fioBlursMI[j].z*fioBlursMI[j].y*fioBlursMI[j].x;
			fioBlursMI[j].pfVectors = fioStackMI.pfVectors + j*iElementsPerImage;
		}

		fScale *= 2.0f;

		printf("done.\n");
	}

finish_up:

	delete[] lvaMaxima.plvz;
	delete[] lvaMinima.plvz;

	fioDelete(fioStackMI);
	fioDelete(fioStackSubSamples1);
	fioDelete(fioStackSubSamples2);
	fioDelete(fioStackBlurs1);
	fioDelete(fioStackBlurs2);
	fioDelete(fioStackBlurs);
	fioDelete(fioTemp);
	fioDelete(fioTemp2);

	return 1;
}



//
// sampleImage2D()
//
// Need to know the scale difference between 
//
int
sampleImage2DRotationInvariant(
	Feature2D &feat2D,
	FEATUREIO &fioSample, // 1+2+4+8+16+32+64 = 127 feats,
	FEATUREIO &fioImg, // Original image
	FEATUREIO &fioDx, // Derivative images
	FEATUREIO &fioDy
)
{
	// We want to sample the image in a manner proportional to 
	// feat2D.scale

	// Image region is 2x the feature scale
	float fImageRad = 3.0f*feat2D.scale;
	int iRadMax = fImageRad + 2;
	float fImageRadSqr = fImageRad*fImageRad;

	if (
		feat2D.x - iRadMax < 0 ||
		feat2D.y - iRadMax < 0 ||
		feat2D.x + iRadMax >= fioImg.x ||
		feat2D.y + iRadMax >= fioImg.y)
	{
		// Feature out of image bounds
		return -1;
	}

	//
	// Sample pixels: bilinear interpolation
	// Here, (x,y) are in the sampled image coordinate system
	//
	//int iSampleRad = 8; // this gives 64pi=201 pixels samples...
	int iSampleRad = Feature2D::FEATURE_2D_DIM;

	float fScale = fImageRad / (float)(iSampleRad);
	float mag = fScale;

	PpImage ppImgDx;
	PpImage ppImgDy;
	PpImage ppImg2;
	ppImgDx.InitializeSubImage(fioImg.y, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8, (unsigned char*)fioDx.pfVectors);
	ppImgDy.InitializeSubImage(fioImg.y, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8, (unsigned char*)fioDy.pfVectors);
	ppImg2.Initialize(2 * iSampleRad + 1, 2 * iSampleRad + 1, (2 * iSampleRad + 1) * sizeof(float), sizeof(float) * 8);

	float fCenterRow1 = feat2D.y;
	float fCenterCol1 = feat2D.x;
	float fCenterRow2 = iSampleRad;
	float fCenterCol2 = iSampleRad;

	// Don't need angles, rotation invariant
	//float fCosAngle = (float)cos(-feat2D.theta);
	//float fSinAngle = (float)sin(-feat2D.theta);
	float fCosAngle = (float)cos(0.0f);
	float fSinAngle = (float)sin(0.0f);

	RotationInvariantDescriptor rid;
	fioSet(fioSample, 0);
	rid.InitBins(fioSample.pfVectors);
	//rid.InitBins( (float*)&(feat2D.data_yx[0][0]) );

	//PpImage ppImgOut1, ppImgOut2;
	//ppImgOut1.Initialize( (2*iSampleRad+1), (2*iSampleRad+1), (2*iSampleRad+1)*sizeof(float), 32 );
	//ppImgOut2.Initialize( (2*iSampleRad+1), (2*iSampleRad+1), (2*iSampleRad+1)*sizeof(float), 32 );
	//for( int i = 0; i < (2*iSampleRad+1); i++ )
	//{
	//	float *pfRow1 = (float*)ppImgOut1.ImageRow(i);
	//	float *pfRow2 = (float*)ppImgOut2.ImageRow(i);
	//	for( int j = 0; j < (2*iSampleRad+1); j++ )
	//	{
	//		pfRow1[j] =0;
	//		pfRow2[j] =0;
	//	}
	//}

	for (int i = 0; i < (2 * iSampleRad + 1); i++)
	{
		float fRowPos1 = (((float)i) - fCenterRow2) / mag;

		for (int j = 0; j < (2 * iSampleRad + 1); j++)
		{
			float fColPos1 = (((float)j) - fCenterCol2) / mag;

			// Calcuate rotation

			float fRowSource =
				fCosAngle*fRowPos1
				- fSinAngle*fColPos1
				+ fCenterRow1;

			float fColSource =
				fSinAngle*fRowPos1
				+ fCosAngle*fColPos1
				+ fCenterCol1;

			float fDC = fCenterCol1 - fColSource;
			float fDR = fCenterRow1 - fRowSource;
			float fRadiusSqr = fDR*fDR + fDC*fDC;
			if (fRadiusSqr > fImageRadSqr)
			{
				continue;
			}

			// Source pixel must be within bounds of source image.
			if ((fRowSource >= 1 && fRowSource < ppImgDx.Rows() - 1)
				&&
				(fColSource >= 1 && fColSource < ppImgDx.Cols() - 1)
				)
			{
				// Interpolate between pixels
				// Determine local image orientation at pixel
				float fPixDx = fioGetPixelBilinearInterp(fioDx, fColSource, fRowSource);
				float fPixDy = fioGetPixelBilinearInterp(fioDy, fColSource, fRowSource);
				float fPixMagSqr = fPixDx*fPixDx + fPixDy*fPixDy;
				if (fPixMagSqr <= 0)
				{
					continue;
				}

				float fPixMag = sqrt(fPixMagSqr);
				if (fPixMag <= 0)
				{
					continue;
				}

				float fPixOri = atan2(fPixDy, fPixDx);


				//float fPix = fioGetPixelBilinearInterp( fioImg, fColSource, fRowSource );
				//((float*)ppImgOut1.ImageRow( i ))[j] = fPix;
				//((float*)ppImgOut2.ImageRow( i ))[j] = fPixDy;

				// Determine angle between feature center/pixel
				float fFeatDx = (((float)j) - fCenterCol2);
				float fFeatDy = (((float)i) - fCenterRow2);
				float fPixRadSqr = fFeatDx*fFeatDx + fFeatDy*fFeatDy;
				if (fPixRadSqr <= 0.000001)
				{
					continue;
				}
				float fPixRad = sqrt(fPixRadSqr);
				float fFeatOri = atan2(fFeatDy, fFeatDx);

				// Normalize angle between feature center/pixel to unit vector
				fFeatDx /= fPixRad;
				fFeatDy /= fPixRad;

				float fPixFeat = fFeatDx*fPixDx + fFeatDy*fPixDy;
				float fCosTh = fPixFeat / fPixMag;
				if (fCosTh < -1)
					fCosTh = -1;
				if (fCosTh > 1)
					fCosTh = 1;
				float fTh = acos(fCosTh);

				// Calculate magnitude of projection in radial & circular directions
				float fMagRadial = fFeatDx*fPixDx + fFeatDy*fPixDy;
				float fMagCircul = fFeatDx*fPixDy + -fFeatDy*fPixDx;
				float fThan = atan2(fMagRadial, fMagCircul);
				fThan += PI;
				if (fThan < 0)
				{
					fThan = 0;
				}

				if (fMagRadial < 0) fMagRadial *= -1;
				if (fMagCircul < 0) fMagCircul *= -1;

				// Determine angular difference (invariant to rotation)
				float fOriDiff = fFeatOri - fPixOri;
				while (fOriDiff < 0) { fOriDiff += 2.0f*PI; }
				rid.AddVote(fThan, fMagRadial, fMagCircul);

				fPixMag = sqrt(fPixDx*fPixDx + fPixDy*fPixDy);
				if (fPixMagSqr <= 0)
				{
					continue;
				}

				//while( fFeatOri < 0 ) { fFeatOri += 2.0f*PI; }
				//rid.AddVote( fFeatOri, fPixRad, 1.0f );

				// Distribute into histogram bins using bilinear interpolation

				//float fPixel = fioGetPixelBilinearInterp( fioImg, fColSource, fRowSource );
				//((float*)(ppImg2.ImageRow( i )))[ j ] = fPixel;
			}
		}
	}

	//output_float( ppImgOut1, "img.pgm" );
	//output_float( ppImgOut1, "dx.pgm" );
	//output_float( ppImgOut2, "dy.pgm" );

	PpImage ppImgOut;
	//rid.OutputToImage( ppImgOut );
	//output_float( ppImgOut, "feat_rid.pgm" );

	//rid.OutputToTextFile( "feat_rid.txt" );

	//output_float( ppImg2, "feat.pgm" );

	return 0;
}

//
// sampleImage2D()
//
// Need to know the scale difference between 
//
int
sampleImage2D(
	Feature2D &feat2D,
	FEATUREIO &fioSample, // 5x5x5 = 125 feats?,
	FEATUREIO &fioImg, // Original image
	FEATUREIO &fioDx, // Derivative images
	FEATUREIO &fioDy
)
{
	// We want to sample the image in a manner proportional to 
	// feat2D.scale

	// Image region is 2x the feature scale
	float fImageRad = 2.0f*feat2D.scale;
	int iRadMax = fImageRad + 2;

	if (
		feat2D.x - iRadMax < 0 ||
		feat2D.y - iRadMax < 0 ||
		feat2D.x + iRadMax >= fioImg.x ||
		feat2D.y + iRadMax >= fioImg.y)
	{
		// Feature out of image bounds
		return -1;
	}

	//
	// Sample pixels: bilinear interpolation
	// Here, (x,y) are in the sampled image coordinate system
	//
	int iSampleRad = Feature2D::FEATURE_2D_DIM / 2;

	float fScale = fImageRad / (float)(iSampleRad);

	PpImage ppImg1;
	PpImage ppImg2;
	ppImg1.InitializeSubImage(fioImg.y, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8, (unsigned char*)fioImg.pfVectors);
	ppImg2.InitializeSubImage(fioSample.y, fioSample.x, fioSample.x * sizeof(float), sizeof(float) * 8, (unsigned char*)fioSample.pfVectors);

	similarity_transform_image_float(ppImg1, ppImg2,
		feat2D.theta, fScale,
		feat2D.y, feat2D.x,  //     Image1
		Feature2D::FEATURE_2D_DIM / 2, Feature2D::FEATURE_2D_DIM / 2
	);

	output_float(ppImg2, "feat.pgm");

	return 0;
}

//FILE *g_outfile;

//
// determineOrientation2D()
//
// Determine orientation component of feat2D.
//
int
determineOrientation2D(
	Feature2D &feat2D,
	FEATUREIO &fioDx,
	FEATUREIO &fioDy
)
{
	float fMat[2][2] = { { 0,0 },{ 0,0 } };
	float fRad = feat2D.scale;
	int iRad = (int)(fRad + 0.5);
	int iRadPlusOneSqr = (iRad + 1)*iRad;

	if (
		feat2D.x - iRad < 0 ||
		feat2D.y - iRad < 0 ||
		feat2D.x + iRad >= fioDx.x ||
		feat2D.y + iRad >= fioDy.y)
	{
		// Feature out of image bounds
		return -1;
	}

	// Generate Hessian of edge values: assume values are distributed
	// about zero mean (dx,dy,dz=0,0,0)

	float fAvg[2];
	fAvg[0] = 0;
	fAvg[1] = 0;
	for (int y = -iRad; y <= iRad; y++)
	{
		int iY = feat2D.y + y;
		for (int x = -iRad; x <= iRad; x++)
		{
			int iX = feat2D.x + x;
			if (y*y + x*x <= iRadPlusOneSqr)
			{
				// Keep this sample
				float pfEdge[2];
				pfEdge[0] = fioGetPixel(fioDx, iX, iY, 0);
				pfEdge[1] = fioGetPixel(fioDy, iX, iY, 0);
				fAvg[0] += pfEdge[0];
				fAvg[1] += pfEdge[1];

				for (int i = 0; i < 2; i++)
				{
					for (int j = 0; j < 2; j++)
					{
						fMat[i][j] += pfEdge[i] * pfEdge[j];
					}
				}
			}
		}
	}

	//fMat[0][0] = 16;
	//fMat[0][1] = 12;
	//fMat[1][0] = 12;
	//fMat[1][1] = 12;
	//feat2D.theta = atan2( fAvg[1], fAvg[0] );

	// Calculate rotation
	SingularValueDecomp<float, 2, 2>(fMat, feat2D.eigs, feat2D.ori);
	SortEigenDecomp<float, 2>(feat2D.eigs, feat2D.ori);

	feat2D.theta = atan2(feat2D.ori[1][0], feat2D.ori[0][0]);

	//fprintf( g_outfile, "%f\t%f\n", feat2D.ori[0][0], feat2D.ori[1][0] );

	return 0;
}

//
// generateFeature2D()
//
// Determine orientation parameters for feat2D, based on ocation & scale
// of feat2D.
//
int
generateFeature2D(
	Feature2D &feat2D, // Feature geometrical information
	FEATUREIO &fioSample, // Sub-image sample
	FEATUREIO &fioImg, // Original image
	FEATUREIO &fioDx, // Derivative images
	FEATUREIO &fioDy
)
{
	// Determine feature orientation
	if (determineOrientation2D(feat2D, fioDx, fioDy) != 0)
	{
		return -1;
	}

	// Determine feature image content
	if (sampleImage2DRotationInvariant(feat2D, fioSample, fioImg, fioDx, fioDy) != 0)
		//if( sampleImage2D( feat2D, fioSample, fioImg, fioDx, fioDy ) != 0 )
	{
		return -1;
	}

	//	output_feature_in_volume( fioImg, (int)feat2D.scale, feat2D.x, feat2D.y, feat2D.z, "original", 1 );
	//	output_feature_in_volume( fioSample, (int)feat2D.scale, fioSample.x/2, fioSample.y/2, fioSample.z/2, "feature" );

	// Copy 


	for (int yy = 0; yy < Feature2D::FEATURE_2D_DIM; yy++)
	{
		for (int xx = 0; xx < Feature2D::FEATURE_2D_DIM; xx++)
		{
			feat2D.data_yx[yy][xx] = fioGetPixel(fioSample, xx, yy, 0);
		}
	}

	return 0;
}

//
//
//
//
int
number_in_between(
	float f0,
	float f1,
	float f2
)
{
	if ((f1 - f0)*(f1 - f2) <= 0)
	{
		// Yes f1 is betweem f0 and f2
		return 1;
	}
	else
	{
		return 0;
	}
}

//
// get_max_2D()
//
// Return 1 if a valid peak can be interpolated, 0 otherwise
//
int
get_max_2D(
	LOCATION_VALUE_XYZ &lvMid,
	FEATUREIO &fio,
	float &fx,
	float &fy,
	int bPeak // boolean - peak = 1, valley = 0
)
{
	LOCATION_VALUE_XYZ lvMax;
	float fMax;
	fMax = fioGetPixel(fio, lvMid.x, lvMid.y);
	lvMax = lvMid;
	for (int y = lvMid.y - 2; y <= lvMid.y + 2; y++)
	{
		for (int x = lvMid.x - 2; x <= lvMid.x + 2; x++)
		{
			float fPix;
			fPix = fioGetPixel(fio, x, y);
			if (bPeak)
			{
				if (fPix > fMax)
				{
					fMax = fPix;
					lvMax.x = x;
					lvMax.y = y;
				}
			}
			else
			{
				if (fPix < fMax)
				{
					fMax = fPix;
					lvMax.x = x;
					lvMax.y = y;
				}
			}
		}
	}

	if (
		number_in_between(
			fioGetPixel(fio, lvMax.x - 1, lvMax.y, 0),
			fioGetPixel(fio, lvMax.x, lvMax.y, 0),
			fioGetPixel(fio, lvMax.x + 1, lvMax.y, 0)
		)
		||
		number_in_between(
			fioGetPixel(fio, lvMax.x, lvMax.y - 1, 0),
			fioGetPixel(fio, lvMax.x, lvMax.y, 0),
			fioGetPixel(fio, lvMax.x, lvMax.y + 1, 0)
		)
		)
	{
		return 0;
	}
	else
	{
		fx = interpolate_extremum_quadratic(
			lvMax.x - 1, lvMax.x, lvMax.x + 1,
			fioGetPixel(fio, lvMax.x - 1, lvMax.y, 0),
			fioGetPixel(fio, lvMax.x, lvMax.y, 0),
			fioGetPixel(fio, lvMax.x + 1, lvMax.y, 0)
		);

		fy = interpolate_extremum_quadratic(
			lvMax.y - 1, lvMax.y, lvMax.y + 1,
			fioGetPixel(fio, lvMax.x, lvMax.y - 1, 0),
			fioGetPixel(fio, lvMax.x, lvMax.y, 0),
			fioGetPixel(fio, lvMax.x, lvMax.y + 1, 0)
		);
		return 1;
	}
}

//
// get_max_3D()
//
// Find the maximum in a range of +/- 2 voxels around lvMid.
//
//
int
get_max_3D(
	LOCATION_VALUE_XYZ &lvMid,
	FEATUREIO &fio,
	float &fx,
	float &fy,
	float &fz,
	int bPeak // boolean - peak = 1, valley = 0
)
{
	LOCATION_VALUE_XYZ lvMax;
	float fMax;
	fMax = fioGetPixel(fio, lvMid.x, lvMid.y, lvMid.z);
	lvMax = lvMid;
	for (int z = lvMid.z - 2; z <= lvMid.z + 2; z++)
	{
		for (int y = lvMid.y - 2; y <= lvMid.y + 2; y++)
		{
			for (int x = lvMid.x - 2; x <= lvMid.x + 2; x++)
			{
				float fPix;
				fPix = fioGetPixel(fio, x, y, z);
				if (bPeak)
				{
					if (fPix > fMax)
					{
						fMax = fPix;
						lvMax.x = x;
						lvMax.y = y;
						lvMax.z = z;
					}
				}
				else
				{
					if (fPix < fMax)
					{
						fMax = fPix;
						lvMax.x = x;
						lvMax.y = y;
						lvMax.z = z;
					}
				}
			}
		}
	}

	// See if this is actually a peak
	if (
		number_in_between(
			fioGetPixel(fio, lvMax.x - 1, lvMax.y, lvMax.z),
			fioGetPixel(fio, lvMax.x, lvMax.y, lvMax.z),
			fioGetPixel(fio, lvMax.x + 1, lvMax.y, lvMax.z)
		)
		||
		number_in_between(
			fioGetPixel(fio, lvMax.x, lvMax.y - 1, lvMax.z),
			fioGetPixel(fio, lvMax.x, lvMax.y, lvMax.z),
			fioGetPixel(fio, lvMax.x, lvMax.y + 1, lvMax.z)
		)
		||
		number_in_between(
			fioGetPixel(fio, lvMax.x, lvMax.y, lvMax.z - 1),
			fioGetPixel(fio, lvMax.x, lvMax.y, lvMax.z),
			fioGetPixel(fio, lvMax.x, lvMax.y, lvMax.z + 1)
		)
		)
	{
		return 0;
	}
	else
	{
		fx = interpolate_extremum_quadratic(
			lvMax.x - 1, lvMax.x, lvMax.x + 1,
			fioGetPixel(fio, lvMax.x - 1, lvMax.y, lvMax.z),
			fioGetPixel(fio, lvMax.x, lvMax.y, lvMax.z),
			fioGetPixel(fio, lvMax.x + 1, lvMax.y, lvMax.z)
		);

		fy = interpolate_extremum_quadratic(
			lvMax.y - 1, lvMax.y, lvMax.y + 1,
			fioGetPixel(fio, lvMax.x, lvMax.y - 1, lvMax.z),
			fioGetPixel(fio, lvMax.x, lvMax.y, lvMax.z),
			fioGetPixel(fio, lvMax.x, lvMax.y + 1, lvMax.z)
		);

		fz = interpolate_extremum_quadratic(
			lvMax.z - 1, lvMax.z, lvMax.z + 1,
			fioGetPixel(fio, lvMax.x, lvMax.y, lvMax.z - 1),
			fioGetPixel(fio, lvMax.x, lvMax.y, lvMax.z),
			fioGetPixel(fio, lvMax.x, lvMax.y, lvMax.z + 1)
		);

		return 1;
	}
}


int
get_max_3D(
	float fxIn, float fyIn, float fzIn,
	FEATUREIO &fio,
	float &fx,
	float &fy,
	float &fz,
	int bPeak // boolean - peak = 1, valley = 0
)
{
	LOCATION_VALUE_XYZ lvMid;
	lvMid.x = fxIn; lvMid.y = fyIn; lvMid.z = fzIn;
	return get_max_3D(lvMid, fio, fx, fy, fz, bPeak);
}

//int
//get_max_2D(
//		   LOCATION_VALUE_XYZ &lvMid,
//			  FEATUREIO &fioH,	// Hi res image
//			  FEATUREIO &fioC,	// Center image: for interpolating feature geometry
//			  FEATUREIO &fioL,	// Lo res image
//			  int bPeak // boolean - peak = 1, valley = 0
//		   )
//{
//	LOCATION_VALUE_XYZ lvL;
//	LOCATION_VALUE_XYZ lvH;
//	float fLMax, fHMax;
//	fLMax = fioGetPixel( lvL, lvMid.x, lvMid.y );
//	fHMax = fioGetPixel( lvH, lvMid.x, lvMid.y );
//	for( int y = lvMid.y-2; y <= lvMid.y+2; y++ )
//	{
//		for( int x = lvMid.x-2; x <= lvMid.x+2; x++ )
//		{
//			float fPixL, fPixH;
//			fPixL = fioGetPixel( lvL, lvMid.x, lvMid.y );
//			fPixH = fioGetPixel( lvL, lvMid.x, lvMid.y );
//			if( bPeak )
//			{
//				if( fPixL > fLMax )
//				{
//					fLMax = fPixL;
//					lvL.x = x;
//					lvL.y = y;
//				}
//				if( fPixH > fLMax )
//				{
//					fHMax = fPixH;
//					lvH.x = x;
//					lvH.y = y;
//				}
//			}
//			else
//			{
//				if( fPixL < fLMax )
//				{
//					fLMax = fPixL;
//					lvL.x = x;
//					lvL.y = y;
//				}
//				if( fPixH < fLMax )
//				{
//					fHMax = fPixH;
//					lvH.x = x;
//					lvH.y = y;
//				}
//			}
//		}
//	}
//
//
//
//		feat2D.x = interpolate_extremum_quadratic(
//							   lvaMinima.plvz[i].x-1,lvaMinima.plvz[i].x,lvaMinima.plvz[i].x+1,
//							   fioGetPixel( fioC, lvaMinima.plvz[i].x-1, lvaMinima.plvz[i].y, 0 ),
//							   fioGetPixel( fioC, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, 0 ),
//							   fioGetPixel( fioC, lvaMinima.plvz[i].x+1, lvaMinima.plvz[i].y, 0 )
//							   );
//
//		feat2D.y = interpolate_extremum_quadratic(
//							   lvaMinima.plvz[i].y-1,lvaMinima.plvz[i].y,lvaMinima.plvz[i].y+1,
//							   fioGetPixel( fioC, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y-1, 0 ),
//							   fioGetPixel( fioC, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, 0 ),
//							   fioGetPixel( fioC, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y+1, 0 )
//							   );
//}


//
// generateFeatures2D()
//
// Generate a vector of features for all minima/maxima. Essentially,
// set location, scale & orientation parameters, then store in vector.
//
// The scale is interpolated by fitting a quadratic, this results in 
// more inliers (Mikolyczyk boat images img1.pgm, img5.pgm):
//   With scale interp: Inliers: 91 + 77
//   Without scale interp: Inliers: 84 + 72
//
int
generateFeatures2D(
	LOCATION_VALUE_XYZ_ARRAY	&lvaMinima,
	LOCATION_VALUE_XYZ_ARRAY	&lvaMaxima,

	FEATUREIO &fioH,	// Hi res image
	FEATUREIO &fioC,	// Center image: for interpolating feature geometry
	FEATUREIO &fioL,	// Lo res image

	float fScaleH,	// Scale factors: for interpolating feature geometry
	float fScaleC,
	float fScaleL,

	FEATUREIO &fioImg,
	FEATUREIO &fioDx,
	FEATUREIO &fioDy,
	float fScale,
	vector<Feature2D> &vecFeats
)
{
	int i;
	Feature2D feat2D;
	FEATUREIO featSample;
	featSample.x = featSample.y = Feature2D::FEATURE_2D_DIM;
	featSample.z = featSample.t = 1;
	featSample.iFeaturesPerVector = 1;
	fioAllocate(featSample);
	for (int i = 0; i < lvaMinima.iCount; i++)
	{
		feat2D.x = lvaMinima.plvz[i].x;
		feat2D.y = lvaMinima.plvz[i].y;

		feat2D.x = interpolate_extremum_quadratic(
			lvaMinima.plvz[i].x - 1, lvaMinima.plvz[i].x, lvaMinima.plvz[i].x + 1,
			fioGetPixel(fioC, lvaMinima.plvz[i].x - 1, lvaMinima.plvz[i].y, 0),
			fioGetPixel(fioC, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, 0),
			fioGetPixel(fioC, lvaMinima.plvz[i].x + 1, lvaMinima.plvz[i].y, 0)
		);

		feat2D.y = interpolate_extremum_quadratic(
			lvaMinima.plvz[i].y - 1, lvaMinima.plvz[i].y, lvaMinima.plvz[i].y + 1,
			fioGetPixel(fioC, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y - 1, 0),
			fioGetPixel(fioC, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, 0),
			fioGetPixel(fioC, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y + 1, 0)
		);

		//feat2D.scale = 3*fScale;
		feat2D.scale = 1 * interpolate_extremum_quadratic(
			fScaleH, fScaleC, fScaleL,
			fioGetPixel(fioH, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, 0),
			fioGetPixel(fioC, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, 0),
			fioGetPixel(fioL, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, 0)
		);
		if (generateFeature2D(feat2D, featSample, fioImg, fioDx, fioDy) == 0)
		{
			feat2D.m_uiInfo &= ~INFO_FLAG_MIN0MAX1;
			//feat2D.ori[0][0] = -1; // Flag as minimum

			float fLx, fLy;
			float fHx, fHy;
			if (get_max_2D(lvaMinima.plvz[i], fioL, fLx, fLy, 0)
				&& get_max_2D(lvaMinima.plvz[i], fioH, fHx, fHy, 0))
			{
				feat2D.ori[0][0] = atan2(fHy - fLy, fHx - fLx);
				vecFeats.push_back(feat2D);
			}
			else
			{
				feat2D.ori[0][0] = 0;
				vecFeats.push_back(feat2D);
			}
		}
	}
	for (int i = 0; i < lvaMaxima.iCount; i++)
	{
		feat2D.x = lvaMaxima.plvz[i].x;
		feat2D.y = lvaMaxima.plvz[i].y;

		feat2D.x = interpolate_extremum_quadratic(
			lvaMaxima.plvz[i].x - 1, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].x + 1,
			fioGetPixel(fioC, lvaMaxima.plvz[i].x - 1, lvaMaxima.plvz[i].y, 0),
			fioGetPixel(fioC, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, 0),
			fioGetPixel(fioC, lvaMaxima.plvz[i].x + 1, lvaMaxima.plvz[i].y, 0)
		);

		feat2D.y = interpolate_extremum_quadratic(
			lvaMaxima.plvz[i].y - 1, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].y + 1,
			fioGetPixel(fioC, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y - 1, 0),
			fioGetPixel(fioC, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, 0),
			fioGetPixel(fioC, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y + 1, 0)
		);


		//feat2D.scale = 3*fScale;
		feat2D.scale = 1 * interpolate_extremum_quadratic(
			fScaleH, fScaleC, fScaleL,
			fioGetPixel(fioH, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, 0),
			fioGetPixel(fioC, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, 0),
			fioGetPixel(fioL, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, 0)
		);
		if (generateFeature2D(feat2D, featSample, fioImg, fioDx, fioDy) == 0)
		{
			feat2D.m_uiInfo |= INFO_FLAG_MIN0MAX1;
			//feat2D.ori[0][0] = 1; // Flag as maximum

			float fLx, fLy;
			float fHx, fHy;
			if (
				get_max_2D(lvaMaxima.plvz[i], fioL, fLx, fLy, 1)
				&& get_max_2D(lvaMaxima.plvz[i], fioH, fHx, fHy, 1))
			{
				feat2D.ori[0][0] = atan2(fHy - fLy, fHx - fLx);
				vecFeats.push_back(feat2D);
			}
			else
			{
				feat2D.ori[0][0] = 0;
				vecFeats.push_back(feat2D);
			}
		}
	}
	fioDelete(featSample);

	return 0;
}

int
msGeneratePyramidDOG2D(
	FEATUREIO	&fioImg,
	vector<Feature2D> &vecFeats,
	float fInitialImageScale,
	int bDense,
	int bOutput
)
{
	int iScaleCount = 4;
	char pcFileName[300];

	PpImage ppImgOut;
	PpImage ppImgOutMem;
	ppImgOutMem.Initialize(1, fioImg.x*fioImg.y*fioImg.z, fioImg.x*fioImg.y*fioImg.z * sizeof(float), sizeof(float) * 8);

	// Allocate temporary arrays for blurring

	FEATUREIO fioTemp;
	FEATUREIO fioImgBase;
	fioImgBase = fioTemp = fioImg;
	fioImgBase.pfVectors = fioTemp.pfVectors = 0;
	fioAllocate(fioTemp);
	fioAllocate(fioImgBase);

	LOCATION_VALUE_XYZ_ARRAY lvaMaxima, lvaMinima;
	lvaMaxima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	lvaMinima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	assert(lvaMaxima.plvz);
	assert(lvaMinima.plvz);

	int i;

	// Blur pyramid is a 3D FEATUREIO
	FEATUREIO fioStack;
	FEATUREIO fioBlurs[BLURS_TOTAL];
	initFIOStack(fioImg, fioStack, (FEATUREIO*)&fioBlurs, BLURS_TOTAL);

	// DOG pyramid
	FEATUREIO fioStackDOG;
	FEATUREIO fioDOGs[BLURS_TOTAL - 1];
	initFIOStack(fioImg, fioStackDOG, (FEATUREIO*)&fioDOGs, BLURS_TOTAL - 1);

	// Edges 
	FEATUREIO fioEdgeStack;
	FEATUREIO fioEdgeXY[2];
	initFIOStack(fioImg, fioEdgeStack, (FEATUREIO*)&fioEdgeXY, 2);

	//
	// Here, the initial blur sigma is calculated along with the subsequent
	// sigma increments.
	//
	//float fSigma = 1.0f;
	float fSigmaInit = 0.5; // initial blurring: image resolution
	if (fInitialImageScale > 0) fSigmaInit /= fInitialImageScale;
	float fSigma = 1.6f; // desired initial blurring
	float fSigmaFactor = (float)pow(2.0, 1.0 / (double)BLURS_PER_OCTAVE);

	// Start initial blur: blur_2^2 = blur_1^2 + fSigmaExtra^2
	float fSigmaExtra = sqrt(fSigma*fSigma - fSigmaInit*fSigmaInit);
	gb3d_blur3d(fioImg, fioTemp, fioBlurs[0], fSigmaExtra, BLUR_PRECISION);

	// Output initial image
	ppImgOut.InitializeSubImage(
		fioImg.y,
		fioImg.x,
		fioImg.x * sizeof(float), sizeof(float) * 8,
		(unsigned char*)ppImgOutMem.ImageRow(0)
	);
	fioFeatureSliceXY(fioImg, fioImg.z / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "image.pgm");
	if (bOutput) output_float(ppImgOut, pcFileName);

	float fScale = fInitialImageScale;

	float pfBlurSigmas[BLURS_TOTAL];

	//g_outfile = fopen( "orientations.txt", "wt" );

	int iFeatDiff = 1;
	for (int i = 0; 1; i++)
	{
		printf("Scale %d: blur...", i);

		fSigma = 1.6f; // desired initial blurring

		pfBlurSigmas[0] = fSigma;

		if (fioBlurs[0].x < 2 || fioBlurs[0].y < 2)
			goto finish_up;

		// Generate blurs in octave
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			// How much extra blur required to raise fSigma-blurred image
			// to next bur level
			float fSigmaExtra = fSigma*sqrt(fSigmaFactor*fSigmaFactor - 1.0f);

			int iReturn = gb3d_blur3d(fioBlurs[j - 1], fioTemp, fioBlurs[j], fSigmaExtra, BLUR_PRECISION);
			if (iReturn == 0)
				goto finish_up;

			fSigma *= fSigmaFactor;
			pfBlurSigmas[j] = fSigma;
		}

		// Output blurs to see
		for (int j = 0; j < BLURS_TOTAL; j++)
		{
			ppImgOut.InitializeSubImage(
				fioBlurs[j].y,
				fioBlurs[j].x,
				fioBlurs[j].x * sizeof(float), sizeof(float) * 8,
				(unsigned char*)ppImgOutMem.ImageRow(0)
			);
			fioFeatureSliceXY(fioBlurs[j], fioBlurs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "blur%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);
		}

		// Save 2*sigma image
		fioCopy(fioTemp, fioBlurs[BLURS_PER_OCTAVE]);

		//ppImgOut.InitializeSubImage( fioTemp.y, fioTemp.x, fioTemp.x*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceXY( fioTemp,  fioTemp.z/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "temp%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		printf("diff...");

		// Create difference of Gaussian
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			fioMultSum(fioBlurs[j - 1], fioBlurs[j], fioDOGs[j - 1], -1.0f);
		}

		// Output dogs to see
		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			ppImgOut.InitializeSubImage(fioDOGs[j].y, fioDOGs[j].x, fioDOGs[j].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
			fioFeatureSliceXY(fioDOGs[j], fioDOGs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "dog%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);
		}

		printf("peaks...");

		// Detect peaks and create features
		int iFeatsCurr = vecFeats.size();
		for (int j = 0; j < BLURS_PER_OCTAVE; j++)
		{
			// This is the only place yet there is a difference in 2D/3D

			// Perform peak selection
			detectExtrema3D(
				fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
				lvaMinima,
				lvaMaxima
			);

			// Generate edges, for orientation
			fioGenerateEdgeImages2D(
				fioBlurs[j + 1],
				fioEdgeXY[0], fioEdgeXY[1]
			);

			// Output edge images to see
			int k;
			for (k = 0; k < 2; k++)
			{
				ppImgOut.InitializeSubImage(fioEdgeXY[k].y, fioEdgeXY[k].x, fioEdgeXY[k].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
				fioFeatureSliceXY(fioEdgeXY[k], fioEdgeXY[k].z / 2, 0, (float*)ppImgOut.ImageRow(0));
				sprintf(pcFileName, "edge%2.2d.pgm", k);
				if (bOutput) output_float(ppImgOut, pcFileName);
			}

			// Generate features for points found
			int iFirstFeat = vecFeats.size();
			generateFeatures2D(
				lvaMinima, lvaMaxima,
				fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
				pfBlurSigmas[j], pfBlurSigmas[j + 1], pfBlurSigmas[j + 2],
				fioBlurs[j],
				fioEdgeXY[0],
				fioEdgeXY[1],
				pfBlurSigmas[j + 1],
				vecFeats
			);

			// Update geometry to image space
			float fFactor = fScale;
			float fAddend = 0;//fFactor/2.0f;
			for (int iFU = iFirstFeat; iFU < vecFeats.size(); iFU++)
			{
				// Update feature scale to actual
				vecFeats[iFU].scale *= fFactor;
				// Should also update location...
				vecFeats[iFU].x = vecFeats[iFU].x*fFactor + fAddend;
				vecFeats[iFU].y = vecFeats[iFU].y*fFactor + fAddend;
				//vecFeats[iFU].OutputVisualFeature( "feat_raw.pgm" );
				//vecFeats[iFU].AbsoluteValueData();
				//vecFeats[iFU].OutputVisualFeature( "feat_abs.pgm" );
			}

		}

		iFeatDiff = vecFeats.size() - iFeatsCurr;
		printf("%d...", vecFeats.size() - iFeatsCurr);

		printf("subsample...");

		fioBlurs[0].x /= 2;
		fioBlurs[0].y /= 2;
		//fioBlurs[0].z /= 2;	
		// Subsample sigma=2 image into first position
		//fioSubSample( fioTemp, fioBlurs[0] );
		fioSubSampleInterpolate(fioTemp, fioBlurs[0]);

		//ppImgOut.InitializeSubImage( fioBlurs[0].z, fioBlurs[0].y, fioBlurs[0].y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioBlurs[0],  fioBlurs[0].x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "blur_first%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		//ppImgOut.InitializeSubImage( fioTemp.z, fioTemp.y, fioTemp.y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioTemp,  fioTemp.x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "temp_after%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		// Halve dimensions of all blur images
		// fioBlurs[0] is already done above
		fioTemp.x /= 2;
		fioTemp.y /= 2;
		//fioTemp.z /= 2;
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			fioBlurs[j].x /= 2;
			fioBlurs[j].y /= 2;
			//fioBlurs[j].z /= 2;
			int iElementsPerImage = fioBlurs[j].z*fioBlurs[j].y*fioBlurs[j].x;
			fioBlurs[j].pfVectors = fioStack.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			fioDOGs[j].x /= 2;
			fioDOGs[j].y /= 2;
			//fioDOGs[j].z /= 2;
			int iElementsPerImage = fioDOGs[j].z*fioDOGs[j].y*fioDOGs[j].x;
			fioDOGs[j].pfVectors = fioStackDOG.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < 2; j++)
		{
			fioEdgeXY[j].x /= 2;
			fioEdgeXY[j].y /= 2;
			int iElementsPerImage = fioEdgeXY[j].z*fioEdgeXY[j].y*fioEdgeXY[j].x;
			fioEdgeXY[j].pfVectors = fioEdgeStack.pfVectors + j*iElementsPerImage;
		}

		fScale *= 2.0f;

		printf("done.\n");
	}

finish_up:

	delete[] lvaMaxima.plvz;
	delete[] lvaMinima.plvz;

	fioDelete(fioStackDOG);
	fioDelete(fioEdgeStack);
	fioDelete(fioStack);
	fioDelete(fioTemp);

	//fclose( g_outfile );

	return 1;
}

//
// fsFindNeighbor()
//
// Find neighbor of fi in aTracks array.
//
// Searches in a radius of fi.scale around fi.(x/y/z)
//
// Distance between fio.(xyz) and fiNeighbor.(xyz) must be less 
// that minimum scale of fioNeighbor.scaleMin.
//
int
fsFindNeighbor(
	Feature3DInfo &fi,
	FEATUREIO &aTracks,
	vector<Feature3DStringInfo> &vecFeats
)
{
	float fNearestDist = -1;
	int iNearestDistIndex = -1;
	int iZStart = fi.z - fi.scale < 0 ? 0 : fi.z - fi.scale;
	int iZFinsh = fi.z + fi.scale > aTracks.z - 1 ? aTracks.z - 1 : fi.z + fi.scale;
	for (int z = iZStart; z <= iZFinsh; z++)
	{
		int iyStart = fi.y - fi.scale < 0 ? 0 : fi.y - fi.scale;
		int iyFinsh = fi.y + fi.scale > aTracks.y - 1 ? aTracks.y - 1 : fi.y + fi.scale;

		for (int y = iyStart; y <= iyFinsh; y++)
		{
			int ixStart = fi.x - fi.scale < 0 ? 0 : fi.x - fi.scale;
			int ixFinsh = fi.x + fi.scale > aTracks.x - 1 ? aTracks.x - 1 : fi.x + fi.scale;
			for (int x = ixStart; x <= ixFinsh; x++)
			{
				int iIndex = (int)fioGetPixel(aTracks, x, y, z);
				if (iIndex >= 0)
				{
					// SET-linking across scales
					Feature3DStringInfo &fiN = vecFeats[iIndex];
					float fDist = sqrt(fiN.DistSqrXYZ(fi));
					fDist /= fiN.scaleMax;
					if (fDist < 0.5)
					{
						// Found a neighbor - keep nearest
						fNearestDist = fDist;
						iNearestDistIndex = iIndex;
					}
				}
			}
		}
	}

	// Delete old, update new
	if (iNearestDistIndex >= 0)
	{
		// Found neighbor, update by saving current max scale
		// SET-orientation, last factor, location also?
		Feature3DStringInfo &fiN = vecFeats[iNearestDistIndex];
		if (fiN.scaleMax < fi.scale)
		{
			// Has not already been update (with latest scale)
			float dx = fiN.x - fi.x;
			float dy = fiN.y - fi.y;
			float ori = atan2(dy, dx);
			fiN.x = fi.x;
			fiN.y = fi.y;
			fiN.ori[0][0] = ori;
			fiN.scaleMax = fi.scale;
			fiN.iScaleCount++;
		}
		else
		{
			//
			printf("Already updated!\n");
		}
	}

	return iNearestDistIndex;
}

//
// fsCleanUp()
//
// Remove any feature strings that were not updated with current scale.
//
int
fsCleanUp(
	FEATUREIO &aTracks,
	vector<Feature3DStringInfo> &vecFeats,
	float fCurrentScale
)
{
	for (int z = 0; z < aTracks.z; z++)
	{
		for (int y = 0; y < aTracks.y; y++)
		{
			for (int x = 0; x < aTracks.x; x++)
			{
				float *pfIndex = fioGetVector(aTracks, x, y, z);
				int iIndex = (int)(*pfIndex);
				if (iIndex >= 0)
				{
					Feature3DStringInfo &fiN = vecFeats[iIndex];
					if (fiN.scaleMax < fCurrentScale)
					{
						// This feature has not been updated
						*pfIndex = -1;
					}
				}
			}
		}
	}
	return 0;
}

int
fsAccumulateFeatureStrings(
	LOCATION_VALUE_XYZ_ARRAY	&lvaMinima,
	LOCATION_VALUE_XYZ_ARRAY	&lvaMaxima,
	FEATUREIO &fioC, // criterion image
	FEATUREIO &fioImg,
	FEATUREIO &fioDx,
	FEATUREIO &fioDy,
	float fScale,
	float fScaleFactor, // Multiplicative scale factor for these features
	FEATUREIO &aTracksMin,
	FEATUREIO &aTracksMax,
	vector<Feature3DStringInfo> &vecFeats
)
{
	int i;
	Feature3DStringInfo ft3di;
	FEATUREIO featSample;
	featSample.x = featSample.y = Feature2D::FEATURE_2D_DIM;
	featSample.z = featSample.t = 1;
	featSample.iFeaturesPerVector = 1;
	fioAllocate(featSample);
	ft3di.m_uiInfo = 0;
	for (int i = 0; i < lvaMinima.iCount; i++)
	{
		ft3di.x = lvaMinima.plvz[i].x;
		ft3di.y = lvaMinima.plvz[i].y;
		ft3di.z = lvaMinima.plvz[i].z;

		ft3di.x = interpolate_extremum_quadratic(
			lvaMinima.plvz[i].x - 1, lvaMinima.plvz[i].x, lvaMinima.plvz[i].x + 1,
			fioGetPixel(fioC, lvaMinima.plvz[i].x - 1, lvaMinima.plvz[i].y, 0),
			fioGetPixel(fioC, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, 0),
			fioGetPixel(fioC, lvaMinima.plvz[i].x + 1, lvaMinima.plvz[i].y, 0)
		);

		ft3di.y = interpolate_extremum_quadratic(
			lvaMinima.plvz[i].y - 1, lvaMinima.plvz[i].y, lvaMinima.plvz[i].y + 1,
			fioGetPixel(fioC, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y - 1, 0),
			fioGetPixel(fioC, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y, 0),
			fioGetPixel(fioC, lvaMinima.plvz[i].x, lvaMinima.plvz[i].y + 1, 0)
		);
		ft3di.x *= fScaleFactor;
		ft3di.y *= fScaleFactor;
		ft3di.z *= fScaleFactor;
		ft3di.scale = fScale*fScaleFactor;

		int iIndex = fsFindNeighbor(ft3di, aTracksMin, vecFeats);
		if (iIndex >= 0)
		{
			// Found neighbor, update by saving current max scale
			// SET-orientation, last factor, location also?
			//Feature3DStringInfo &fiN = vecFeats[iIndex];
			//float dx = fiN.x-ft3di.x;
			//float dy = fiN.y-ft3di.y;
			//float ori = atan2( dy, dx );
			//fiN.x = ft3di.x;
			//fiN.y = ft3di.y;
			//fiN.ori[0][0] = ori;
			//fiN.scaleMax = fScale*fScaleFactor;
			//fiN.iScaleCount++;
		}
		else
		{
			// No neighbor, create a new feature string, put index in array
			float *pfIndex = fioGetVector(aTracksMin, ft3di.x + 0.5, ft3di.y + 0.5, ft3di.z + 0.5);
			*pfIndex = vecFeats.size();
			ft3di.scaleMin = ft3di.scaleMax = fScale*fScaleFactor;
			ft3di.m_uiInfo &= ~INFO_FLAG_MIN0MAX1; // Flag as min
			ft3di.iScaleCount = 1;
			vecFeats.push_back(ft3di);
		}
	}
	for (int i = 0; i < lvaMaxima.iCount; i++)
	{
		ft3di.x = lvaMaxima.plvz[i].x;
		ft3di.y = lvaMaxima.plvz[i].y;
		ft3di.z = lvaMaxima.plvz[i].z;

		ft3di.x = interpolate_extremum_quadratic(
			lvaMaxima.plvz[i].x - 1, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].x + 1,
			fioGetPixel(fioC, lvaMaxima.plvz[i].x - 1, lvaMaxima.plvz[i].y, 0),
			fioGetPixel(fioC, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, 0),
			fioGetPixel(fioC, lvaMaxima.plvz[i].x + 1, lvaMaxima.plvz[i].y, 0)
		);

		ft3di.y = interpolate_extremum_quadratic(
			lvaMaxima.plvz[i].y - 1, lvaMaxima.plvz[i].y, lvaMaxima.plvz[i].y + 1,
			fioGetPixel(fioC, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y - 1, 0),
			fioGetPixel(fioC, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y, 0),
			fioGetPixel(fioC, lvaMaxima.plvz[i].x, lvaMaxima.plvz[i].y + 1, 0)
		);

		ft3di.x *= fScaleFactor;
		ft3di.y *= fScaleFactor;
		ft3di.z *= fScaleFactor;
		ft3di.scale = fScale*fScaleFactor;

		int iIndex = fsFindNeighbor(ft3di, aTracksMax, vecFeats);
		if (iIndex >= 0)
		{
			// SET-orientation, last factor, location also?
			// Found neighbor, update by saving current max scale
			//Feature3DStringInfo &fiN = vecFeats[iIndex];
			//float dx = fiN.x-ft3di.x;
			//float dy = fiN.y-ft3di.y;
			//fiN.x = ft3di.x;
			//fiN.y = ft3di.y;
			//float ori = atan2( dy, dx );
			//fiN.ori[0][0] = ori;
			//fiN.scaleMax = fScale*fScaleFactor;
			//fiN.iScaleCount++;
		}
		else
		{
			// No neighbor, create a new feature string, put index in array
			float *pfIndex = fioGetVector(aTracksMax, ft3di.x + 0.5, ft3di.y + 0.5, ft3di.z + 0.5);
			*pfIndex = vecFeats.size();
			ft3di.scaleMin = ft3di.scaleMax = fScale*fScaleFactor;
			ft3di.m_uiInfo |= INFO_FLAG_MIN0MAX1; // Flag as max
			ft3di.iScaleCount = 1;
			vecFeats.push_back(ft3di);
		}
	}
	fioDelete(featSample);

	fsCleanUp(aTracksMin, vecFeats, fScale*fScaleFactor);
	fsCleanUp(aTracksMax, vecFeats, fScale*fScaleFactor);

	return 0;
}




bool
sortFeatStringLargeToSmall(const Feature3DStringInfo &elem1, const Feature3DStringInfo &elem2)
{
	return elem1.iScaleCount > elem2.iScaleCount;
}

void
output_features_count(
	FEATUREIO	&fioImg,
	vector<Feature3DStringInfo> &vecFeatString
)
{
	sort(vecFeatString.begin(), vecFeatString.end(), sortFeatStringLargeToSmall);
	int iFrst = 0;
	PpImage ppImgTemp;
	ppImgTemp.Initialize(fioImg.y, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
	fioFeatureSliceXY(fioImg, fioImg.z / 2, 0, (float*)ppImgTemp.ImageRow(0));

	unsigned char rgbW[3] = { 255,255,255 };
	unsigned char rgbB[3] = { 0,0,0 };

	// Output old image
	char pcFileName[400];
	for (int i = 0; i < vecFeatString.size(); i++)
	{
		Feature3DStringInfo &ftCurr = vecFeatString[i];
		if (ftCurr.iScaleCount != vecFeatString[iFrst].iScaleCount)
		{
			sprintf(pcFileName, "count%3.3d.pgm", vecFeatString[iFrst].iScaleCount);
			output_float(ppImgTemp, pcFileName);

			iFrst = i;

			// Start a new one
			fioFeatureSliceXY(fioImg, fioImg.z / 2, 0, (float*)ppImgTemp.ImageRow(0));
		}

		//float *pfVec = (float*)ppImgTemp.ImageRow( ftCurr.y );
		//pfVec += (int)ftCurr.x;
		//*pfVec = (ftCurr.m_uiInfo & INFO_FLAG_MIN0MAX1) ? 0 : 255;

		unsigned char *pucRBGB = (ftCurr.m_uiInfo & INFO_FLAG_MIN0MAX1) ? rgbB : rgbW;

		of_paint_keypoint(ftCurr.y, ftCurr.x, ftCurr.scaleMin, ftCurr.ori[0][0], ppImgTemp, pucRBGB);
		//of_paint_circle( ftCurr.y, ftCurr.x, ftCurr.scaleMin, ppImgTemp, pucRBGB );
	}
	sprintf(pcFileName, "count%3.3d.pgm", vecFeatString[iFrst].iScaleCount);
	output_float(ppImgTemp, pcFileName);
}

void
output_points(
	FEATUREIO	&fioImg,
	LOCATION_VALUE_XYZ_ARRAY &lvaMaxima,
	int iColor,
	char *pcFileName
)
{
	FEATUREIO fio;
	fio = fioImg;
	fioAllocate(fio);
	fioCopy(fio, fioImg);
	for (int i = 0; i < lvaMaxima.iCount; i++)
	{
		LOCATION_VALUE_XYZ &lv = lvaMaxima.plvz[i];
		float *pfPix = fioGetVector(fio, lv.x, lv.y, lv.z);
		*pfPix = iColor;
	}

	PpImage ppImgTemp;
	fioimgInitReference(ppImgTemp, fio);

	output_float(ppImgTemp, pcFileName);
	fioDelete(fio);
}

int
msGeneratePyramidGauss2DTriangle(
	FEATUREIO	&fioImg,
	vector<Feature2D> &vecFeats,
	float fInitialImageScale,
	int bDense,
	int bOutput
)
{
	int iScaleCount = 4;
	char pcFileName[300];

	int iMinDim = fioImg.x;
	if (fioImg.y < iMinDim) iMinDim = fioImg.y;
	if (fioImg.z < iMinDim) iMinDim = fioImg.z;
	float fOctaves = log((float)iMinDim) / log(2.0);

	vector<Feature3DStringInfo> vecFeatString;

	//#define HASH_DX 0.5f
	//#define HASH_DS 1.5f
	//	ScaleSpaceXYZHash ssh;
	float pfInc[3] = { 1,1,1 };
	float pfMin[3] = { 0,0,0 };
	float pfMax[3] = { 0,0,0 };
	pfMax[0] = fioImg.x; pfMax[1] = fioImg.y; pfMax[2] = fioImg.z;
	//	float fScaleMin = 1.6f;
	//	float fScaleMax = pow( 1.6f, fOctaves );
	//	float pfIncPerScale[3] = { HASH_DX, HASH_DX, HASH_DX };
	//	float fScaleInc = HASH_DS;
	//	ssh.Init( 2, pfMin, pfMax, pfIncPerScale, fScaleMin, fScaleMax, fScaleInc );

	FEATUREIO aTracksMin = fioImg;
	FEATUREIO aTracksMax = fioImg;
	aTracksMin.iFeaturesPerVector = 1;
	aTracksMax.iFeaturesPerVector = 1;
	fioAllocate(aTracksMin);
	fioAllocate(aTracksMax);
	fioSet(aTracksMin, -1);
	fioSet(aTracksMax, -1);
	//aTracksMin.Init( 2, pfMin, pfMax, pfInc );
	//aTracksMax.Init( 2, pfMin, pfMax, pfInc );

	PpImage ppImgOut;
	PpImage ppImgOutMem;
	ppImgOutMem.Initialize(1, fioImg.x*fioImg.y*fioImg.z, fioImg.x*fioImg.y*fioImg.z * sizeof(float), sizeof(float) * 8);

	// Allocate temporary arrays for blurring

	FEATUREIO fioTemp;
	FEATUREIO fioImgBase;
	fioImgBase = fioTemp = fioImg;
	fioImgBase.pfVectors = fioTemp.pfVectors = 0;
	fioAllocate(fioTemp);
	fioAllocate(fioImgBase);

	LOCATION_VALUE_XYZ_ARRAY lvaMaxima, lvaMinima;
	lvaMaxima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	lvaMinima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	assert(lvaMaxima.plvz);
	assert(lvaMinima.plvz);

	int i;

	// Blur pyramid is a 3D FEATUREIO
	FEATUREIO fioStack;
	FEATUREIO fioBlurs[BLURS_TOTAL];
	initFIOStack(fioImg, fioStack, (FEATUREIO*)&fioBlurs, BLURS_TOTAL);

	// DOG pyramid
	FEATUREIO fioStackDOG;
	FEATUREIO fioDOGs[BLURS_TOTAL - 1];
	initFIOStack(fioImg, fioStackDOG, (FEATUREIO*)&fioDOGs, BLURS_TOTAL - 1);

	// Edges 
	FEATUREIO fioEdgeStack;
	FEATUREIO fioEdgeXY[2];
	initFIOStack(fioImg, fioEdgeStack, (FEATUREIO*)&fioEdgeXY, 2);

	//
	// Here, the initial blur sigma is calculated along with the subsequent
	// sigma increments.
	//
	//float fSigma = 1.0f;
	float fSigmaInit = 0.5; // initial blurring: image resolution
	if (fInitialImageScale > 0) fSigmaInit /= fInitialImageScale;
	float fSigma = 1.6f; // desired initial blurring
	float fSigmaFactor = (float)pow(2.0, 1.0 / (double)BLURS_PER_OCTAVE);

	// Start initial blur: blur_2^2 = blur_1^2 + fSigmaExtra^2
	float fSigmaExtra = sqrt(fSigma*fSigma - fSigmaInit*fSigmaInit);
	gb3d_blur3d(fioImg, fioTemp, fioBlurs[0], fSigmaExtra, BLUR_PRECISION);

	// Output initial image
	ppImgOut.InitializeSubImage(
		fioImg.y,
		fioImg.x,
		fioImg.x * sizeof(float), sizeof(float) * 8,
		(unsigned char*)ppImgOutMem.ImageRow(0)
	);
	fioFeatureSliceXY(fioImg, fioImg.z / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "image.pgm");
	if (bOutput) output_float(ppImgOut, pcFileName);

	float fScale = fInitialImageScale;

	float pfBlurSigmas[BLURS_TOTAL];

	//g_outfile = fopen( "orientations.txt", "wt" );

	int iFeatDiff = 1;
	for (int i = 0; 1; i++)
	{
		printf("Scale %d: blur...", i);

		fSigma = 1.6f; // desired initial blurring

		pfBlurSigmas[0] = fSigma;

		if (fioBlurs[0].x < 5 || fioBlurs[0].y < 5) goto finish_up;

		// Generate blurs in octave
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			// How much extra blur required to raise fSigma-blurred image
			// to next bur level
			float fSigmaExtra = fSigma*sqrt(fSigmaFactor*fSigmaFactor - 1.0f);

			int iReturn = gb3d_blur3d(fioBlurs[j - 1], fioTemp, fioBlurs[j], fSigmaExtra, BLUR_PRECISION);
			if (iReturn == 0)
				goto finish_up;

			fSigma *= fSigmaFactor;
			pfBlurSigmas[j] = fSigma;
		}

		// Output blurs to see
		for (int j = 0; j < BLURS_TOTAL; j++)
		{
			ppImgOut.InitializeSubImage(
				fioBlurs[j].y,
				fioBlurs[j].x,
				fioBlurs[j].x * sizeof(float), sizeof(float) * 8,
				(unsigned char*)ppImgOutMem.ImageRow(0)
			);
			fioFeatureSliceXY(fioBlurs[j], fioBlurs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "blur%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);
		}

		// Save 2*sigma image
		fioCopy(fioTemp, fioBlurs[BLURS_PER_OCTAVE]);

		printf("diff...");

		// Create difference of Gaussian
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			fioMultSum(fioBlurs[j - 1], fioBlurs[j], fioDOGs[j - 1], -1.0f);
		}

		// Output dogs to see
		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			ppImgOut.InitializeSubImage(fioDOGs[j].y, fioDOGs[j].x, fioDOGs[j].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
			fioFeatureSliceXY(fioDOGs[j], fioDOGs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "dog%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);
		}

		printf("peaks...");

		// Detect peaks and create features
		int iFeatsCurr = vecFeats.size();
		for (int j = 0; j < BLURS_PER_OCTAVE; j++)
		{
			//FEATUREIO *pfioCriterion = &fioDOGs[j];
			FEATUREIO *pfioCriterion = &fioBlurs[j];

			// Perform peak selection
			detectExtrema2D(
				*pfioCriterion,
				//fioDOGs[j],
				//fioBlurs[j],
				lvaMinima,
				lvaMaxima
			);

			float fMin, fMax;
			fioFindMinMax(fioDOGs[j], fMin, fMax);

			sprintf(pcFileName, "level_%2.2d_max_%2.2d.pgm", i, j);
			output_points(fioDOGs[j], lvaMaxima, fMin, pcFileName);
			sprintf(pcFileName, "level_%2.2d_min_%2.2d.pgm", i, j);
			output_points(fioDOGs[j], lvaMinima, fMax, pcFileName);



			// Generate edges, for orientation
			fioGenerateEdgeImages2D(
				fioBlurs[j],
				fioEdgeXY[0], fioEdgeXY[1]
			);

			// Output edge images to see
			int k;
			for (k = 0; k < 2; k++)
			{
				ppImgOut.InitializeSubImage(fioEdgeXY[k].y, fioEdgeXY[k].x, fioEdgeXY[k].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
				fioFeatureSliceXY(fioEdgeXY[k], fioEdgeXY[k].z / 2, 0, (float*)ppImgOut.ImageRow(0));
				sprintf(pcFileName, "edge%2.2d.pgm", k);
				if (bOutput) output_float(ppImgOut, pcFileName);
			}

			//// Generate features for points found
			int iFirstFeat = vecFeats.size();
			//generateFeatures2D(
			//	lvaMinima, lvaMaxima,
			//	fioDOGs[j], fioDOGs[j+1], fioDOGs[j+2],
			//	pfBlurSigmas[j],pfBlurSigmas[j+1],pfBlurSigmas[j+2],
			//	fioBlurs[j],
			//	fioEdgeXY[0],
			//	fioEdgeXY[1],
			//	pfBlurSigmas[j+1],
			//	vecFeats
			//	);

			float fFactor = fScale;
			fsAccumulateFeatureStrings(
				lvaMinima, lvaMaxima,
				*pfioCriterion,//fioDOGs[j],
				fioBlurs[j],
				fioEdgeXY[0],
				fioEdgeXY[1],
				pfBlurSigmas[j], fScale,
				aTracksMin, aTracksMax,
				vecFeatString
			);

			// Update geometry to image space
			//float fAddend = 0;//fFactor/2.0f;
			//for( int iFU = iFirstFeat; iFU < vecFeats.size(); iFU++ )
			//{
			//	// Update feature scale to actual
			//	vecFeats[iFU].scale *= fFactor;
			//	// Should also update location...
			//	vecFeats[iFU].x = vecFeats[iFU].x*fFactor + fAddend;
			//	vecFeats[iFU].y = vecFeats[iFU].y*fFactor + fAddend;
			//	//vecFeats[iFU].OutputVisualFeature( "feat_raw.pgm" );
			//	//vecFeats[iFU].AbsoluteValueData();
			//	//vecFeats[iFU].OutputVisualFeature( "feat_abs.pgm" );
			//}

		}

		iFeatDiff = vecFeats.size() - iFeatsCurr;
		printf("%d...", vecFeats.size() - iFeatsCurr);

		printf("subsample...");

		fioBlurs[0].x /= 2;
		fioBlurs[0].y /= 2;
		//fioBlurs[0].z /= 2;	
		// Subsample sigma=2 image into first position
		//fioSubSample( fioTemp, fioBlurs[0] );
		fioSubSampleInterpolate(fioTemp, fioBlurs[0]);

		// Halve dimensions of all blur images
		// fioBlurs[0] is already done above
		fioTemp.x /= 2;
		fioTemp.y /= 2;
		//fioTemp.z /= 2;
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			fioBlurs[j].x /= 2;
			fioBlurs[j].y /= 2;
			//fioBlurs[j].z /= 2;
			int iElementsPerImage = fioBlurs[j].z*fioBlurs[j].y*fioBlurs[j].x;
			fioBlurs[j].pfVectors = fioStack.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			fioDOGs[j].x /= 2;
			fioDOGs[j].y /= 2;
			//fioDOGs[j].z /= 2;
			int iElementsPerImage = fioDOGs[j].z*fioDOGs[j].y*fioDOGs[j].x;
			fioDOGs[j].pfVectors = fioStackDOG.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < 2; j++)
		{
			fioEdgeXY[j].x /= 2;
			fioEdgeXY[j].y /= 2;
			int iElementsPerImage = fioEdgeXY[j].z*fioEdgeXY[j].y*fioEdgeXY[j].x;
			fioEdgeXY[j].pfVectors = fioEdgeStack.pfVectors + j*iElementsPerImage;
		}

		fScale *= 2.0f;

		printf("done.\n");
	}

finish_up:

	// Now identify pairs of stable features that are 1) in same scale range 2) spatial neighbours 

	//if( bOutput )
	output_features_count(fioImg, vecFeatString);

	delete[] lvaMaxima.plvz;
	delete[] lvaMinima.plvz;

	fioDelete(fioStackDOG);
	fioDelete(fioEdgeStack);
	fioDelete(fioStack);
	fioDelete(fioTemp);

	//fclose( g_outfile );

	return 1;
}


int
msGeneratePyramidEntropy2D(
	FEATUREIO	&fioImg,
	vector<Feature2D> &vecFeats,
	float fInitialImageScale,
	int bDense,
	int bOutput
)
{
	int iScaleCount = 4;
	char pcFileName[300];

	PpImage ppImgOut;
	PpImage ppImgOutMem;
	ppImgOutMem.Initialize(1, fioImg.x*fioImg.y*fioImg.z, fioImg.x*fioImg.y*fioImg.z * sizeof(float), sizeof(float) * 8);

	// Allocate temporary arrays for blurring

	FEATUREIO fioTemp;
	FEATUREIO fioImgBase;
	fioImgBase = fioTemp = fioImg;
	fioImgBase.pfVectors = fioTemp.pfVectors = 0;
	fioAllocate(fioTemp);
	fioAllocate(fioImgBase);

	LOCATION_VALUE_XYZ_ARRAY lvaMaxima, lvaMinima;
	lvaMaxima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	lvaMinima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	assert(lvaMaxima.plvz);
	assert(lvaMinima.plvz);

	int i;

	// Blur pyramid is a 3D FEATUREIO
	FEATUREIO fioStack;
	FEATUREIO fioBlurs[BLURS_TOTAL];
	initFIOStack(fioImg, fioStack, (FEATUREIO*)&fioBlurs, BLURS_TOTAL);

	// Entropy pyramid, one for each blur
	FEATUREIO fioStackH;
	FEATUREIO fioBlursH[BLURS_TOTAL];
	initFIOStack(fioImg, fioStackH, (FEATUREIO*)&fioBlursH, BLURS_TOTAL);

	// Mutual information pyramid, one for each blur - 1
	FEATUREIO fioStackMI;
	FEATUREIO fioBlursMI[BLURS_TOTAL];
	initFIOStack(fioImg, fioStackMI, (FEATUREIO*)&fioBlursMI, BLURS_TOTAL);

	// DOG pyramid
	FEATUREIO fioStackDOG;
	FEATUREIO fioDOGs[BLURS_TOTAL - 1];
	initFIOStack(fioImg, fioStackDOG, (FEATUREIO*)&fioDOGs, BLURS_TOTAL - 1);

	// Edges 
	FEATUREIO fioEdgeStack;
	FEATUREIO fioEdgeXY[2];
	initFIOStack(fioImg, fioEdgeStack, (FEATUREIO*)&fioEdgeXY, 2);

	//
	// Here, the initial blur sigma is calculated along with the subsequent
	// sigma increments.
	//
	//float fSigma = 1.0f;
	float fSigmaInit = 0.5; // initial blurring: image resolution
	if (fInitialImageScale > 0) fSigmaInit /= fInitialImageScale;
	float fSigma = 1.6f; // desired initial blurring
	float fSigmaFactor = (float)pow(2.0, 1.0 / (double)BLURS_PER_OCTAVE);

	// Start initial blur: blur_2^2 = blur_1^2 + fSigmaExtra^2
	float fSigmaExtra = sqrt(fSigma*fSigma - fSigmaInit*fSigmaInit);
	gb3d_blur3d(fioImg, fioTemp, fioBlurs[0], fSigmaExtra, BLUR_PRECISION);

	// Output initial image
	ppImgOut.InitializeSubImage(
		fioImg.y,
		fioImg.x,
		fioImg.x * sizeof(float), sizeof(float) * 8,
		(unsigned char*)ppImgOutMem.ImageRow(0)
	);
	fioFeatureSliceXY(fioImg, fioImg.z / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "image.pgm");
	if (bOutput) output_float(ppImgOut, pcFileName);

	float fScale = fInitialImageScale;

	float pfBlurSigmas[BLURS_TOTAL];

	//g_outfile = fopen( "orientations.txt", "wt" );	

	int iFeatDiff = 1;
	for (int i = 0; 1; i++)
	{
		printf("Scale %d: blur...", i);

		fSigma = 1.6f; // desired initial blurring

		pfBlurSigmas[0] = fSigma;

		if (fioBlurs[0].x <= 2 || fioBlurs[0].y <= 2)
			goto finish_up; // Quit if too small

							// Compute entropy for lowest level
		__computePixelEntropyBinary(fioBlurs[0], fioBlursH[0]);

		// Generate blurs in octave
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			// How much extra blur required to raise fSigma-blurred image
			// to next bur level
			float fSigmaExtra = fSigma*sqrt(fSigmaFactor*fSigmaFactor - 1.0f);

			// Diffuse distributions p(I|x) at level (j-1) to p(I) at level (j) using p(x), fSigmaExtra
			// p(I) = sum_i p(I|x_i)p(x_i)
			int iReturn = gb3d_blur3d(fioBlurs[j - 1], fioTemp, fioBlurs[j], fSigmaExtra, BLUR_PRECISION);
			if (iReturn == 0)
				goto finish_up;

			// Compute entropy H(I) of p(I) at level (j)
			__computePixelEntropyBinary(fioBlurs[j], fioBlursH[j]);

			// Compute conditional entropy of p(I|x) by diffusing H(I|x): H(I|x) = sum_i p(x_i) H(I|x_i)
			//gb3d_blur3d( fioBlursH[j-1], fioTemp, fioBlursMI[j], fSigmaExtra, BLUR_PRECISION );

			fSigma *= fSigmaFactor;
			pfBlurSigmas[j] = fSigma;
		}

		// Output images to see
		for (int j = 0; j < BLURS_TOTAL; j++)
		{
			ppImgOut.InitializeSubImage(
				fioBlurs[j].y,
				fioBlurs[j].x,
				fioBlurs[j].x * sizeof(float), sizeof(float) * 8,
				(unsigned char*)ppImgOutMem.ImageRow(0)
			);
			fioFeatureSliceXY(fioBlurs[j], fioBlurs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "blur%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);

			ppImgOut.InitializeSubImage(
				fioBlursH[j].y,
				fioBlursH[j].x,
				fioBlursH[j].x * sizeof(float), sizeof(float) * 8,
				(unsigned char*)ppImgOutMem.ImageRow(0)
			);
			fioFeatureSliceXY(fioBlursH[j], fioBlursH[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "info%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);

			if (j > 0)
			{
				ppImgOut.InitializeSubImage(
					fioBlursMI[j].y,
					fioBlursMI[j].x,
					fioBlursMI[j].x * sizeof(float), sizeof(float) * 8,
					(unsigned char*)ppImgOutMem.ImageRow(0)
				);
				fioFeatureSliceXY(fioBlursMI[j], fioBlursMI[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
				sprintf(pcFileName, "conditional_info%2.2d.pgm", j);
				if (bOutput) output_float(ppImgOut, pcFileName);
			}
		}

		// Save 2*sigma image
		fioCopy(fioTemp, fioBlurs[BLURS_PER_OCTAVE]);

		//ppImgOut.InitializeSubImage( fioTemp.y, fioTemp.x, fioTemp.x*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceXY( fioTemp,  fioTemp.z/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "temp%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		printf("diff...");

		// Create difference of entropies, mutual information
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			// fioMultSum( fioBlursH[j], fioBlursMI[j], fioDOGs[j-1], -1.0f );
			fioMultSum(fioBlursH[j], fioBlursH[j - 1], fioDOGs[j - 1], -1.0f);

			//fioCopy( fioDOGs[j-1], fioBlursH[j-1] );

			// Multiply by current scale
			float fSigma = pfBlurSigmas[j];
			fioMult(fioDOGs[j - 1], fioDOGs[j - 1], fSigma);
		}

		// Output dogs to see 
		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			ppImgOut.InitializeSubImage(fioDOGs[j].y, fioDOGs[j].x, fioDOGs[j].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
			fioFeatureSliceXY(fioDOGs[j], fioDOGs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "mutual_info%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);
		}

		printf("peaks...");

		// Detect peaks and create features
		int iFeatsCurr = vecFeats.size();
		for (int j = 0; j < BLURS_PER_OCTAVE; j++)
		{
			// This is the only place yet there is a difference in 2D/3D

			// Perform peak selection
			detectExtrema3D(
				fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
				lvaMinima,
				lvaMaxima
			);

			//lvEnforceMinAbsValue( lvaMinima, 0.01 );
			//lvEnforceMinAbsValue( lvaMaxima, 0.01 );

			// Generate edges, for orientation
			fioGenerateEdgeImages2D(
				fioBlurs[j + 1],
				fioEdgeXY[0], fioEdgeXY[1]
			);

			// Output edge images to see
			int k;
			for (k = 0; k < 2; k++)
			{
				ppImgOut.InitializeSubImage(fioEdgeXY[k].y, fioEdgeXY[k].x, fioEdgeXY[k].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
				fioFeatureSliceXY(fioEdgeXY[k], fioEdgeXY[k].z / 2, 0, (float*)ppImgOut.ImageRow(0));
				sprintf(pcFileName, "edge%2.2d.pgm", k);
				if (bOutput) output_float(ppImgOut, pcFileName);
			}

			// Generate features for points found
			int iFirstFeat = vecFeats.size();
			//lvaMinima.iCount = 0; // Only consider MI maxima
			//lvaMaxima.iCount = 0; // Only consider MI minima
			generateFeatures2D(
				lvaMinima, lvaMaxima,
				fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
				pfBlurSigmas[j], pfBlurSigmas[j + 1], pfBlurSigmas[j + 2],
				fioBlurs[j],
				fioEdgeXY[0],
				fioEdgeXY[1],
				pfBlurSigmas[j + 1],
				vecFeats
			);

			// Update geometry to image space
			float fFactor = fScale;
			float fAddend = 0;//fFactor/2.0f;
			for (int iFU = iFirstFeat; iFU < vecFeats.size(); iFU++)
			{
				// Update feature scale to actual
				vecFeats[iFU].scale *= fFactor;
				// Should also update location...
				vecFeats[iFU].x = vecFeats[iFU].x*fFactor + fAddend;
				vecFeats[iFU].y = vecFeats[iFU].y*fFactor + fAddend;
			}

		}

		iFeatDiff = vecFeats.size() - iFeatsCurr;
		printf("%d...", vecFeats.size() - iFeatsCurr);

		printf("subsample...");

		fioBlurs[0].x /= 2;
		fioBlurs[0].y /= 2;
		//fioBlurs[0].z /= 2;	
		// Subsample sigma=2 image into first position
		//fioSubSample( fioTemp, fioBlurs[0] );
		fioSubSampleInterpolate(fioTemp, fioBlurs[0]);

		//ppImgOut.InitializeSubImage( fioBlurs[0].z, fioBlurs[0].y, fioBlurs[0].y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioBlurs[0],  fioBlurs[0].x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "blur_first%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		//ppImgOut.InitializeSubImage( fioTemp.z, fioTemp.y, fioTemp.y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioTemp,  fioTemp.x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "temp_after%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		// Halve dimensions of all blur images
		// fioBlurs[0] is already done above
		fioTemp.x /= 2;
		fioTemp.y /= 2;
		//fioTemp.z /= 2;
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			fioBlurs[j].x /= 2;
			fioBlurs[j].y /= 2;
			//fioBlurs[j].z /= 2;
			int iElementsPerImage = fioBlurs[j].z*fioBlurs[j].y*fioBlurs[j].x;
			fioBlurs[j].pfVectors = fioStack.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < BLURS_TOTAL; j++)
		{
			fioBlursH[j].x /= 2;
			fioBlursH[j].y /= 2;
			//fioBlursH[j].z /= 2;
			int iElementsPerImage = fioBlursH[j].z*fioBlursH[j].y*fioBlursH[j].x;
			fioBlursH[j].pfVectors = fioStackH.pfVectors + j*iElementsPerImage;

			fioBlursMI[j].x /= 2;
			fioBlursMI[j].y /= 2;
			//fioBlursMI[j].z /= 2;
			iElementsPerImage = fioBlursMI[j].z*fioBlursMI[j].y*fioBlursMI[j].x;
			fioBlursMI[j].pfVectors = fioStackMI.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			fioDOGs[j].x /= 2;
			fioDOGs[j].y /= 2;
			//fioDOGs[j].z /= 2;
			int iElementsPerImage = fioDOGs[j].z*fioDOGs[j].y*fioDOGs[j].x;
			fioDOGs[j].pfVectors = fioStackDOG.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < 2; j++)
		{
			fioEdgeXY[j].x /= 2;
			fioEdgeXY[j].y /= 2;
			int iElementsPerImage = fioEdgeXY[j].z*fioEdgeXY[j].y*fioEdgeXY[j].x;
			fioEdgeXY[j].pfVectors = fioEdgeStack.pfVectors + j*iElementsPerImage;
		}

		fScale *= 2.0f;

		printf("done.\n");
	}

finish_up:

	delete[] lvaMaxima.plvz;
	delete[] lvaMinima.plvz;

	fioDelete(fioStackDOG);
	fioDelete(fioEdgeStack);
	fioDelete(fioStack);
	fioDelete(fioTemp);

	//fclose( g_outfile );

	return 1;
}

int
msGeneratePyramidMutualInfo2D(
	FEATUREIO	&fioImg,
	vector<Feature2D> &vecFeats,
	float fInitialImageScale,
	int bDense,
	int bOutput
)
{
	int iScaleCount = 4;
	char pcFileName[300];

	PpImage ppImgOut;
	PpImage ppImgOutMem;
	ppImgOutMem.Initialize(1, fioImg.x*fioImg.y*fioImg.z, fioImg.x*fioImg.y*fioImg.z * sizeof(float), sizeof(float) * 8);

	// Allocate temporary arrays for blurring

	FEATUREIO fioTemp;
	FEATUREIO fioImgBase;
	fioImgBase = fioTemp = fioImg;
	fioImgBase.pfVectors = fioTemp.pfVectors = 0;
	fioAllocate(fioTemp);
	fioAllocate(fioImgBase);

	LOCATION_VALUE_XYZ_ARRAY lvaMaxima, lvaMinima;
	lvaMaxima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	lvaMinima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	assert(lvaMaxima.plvz);
	assert(lvaMinima.plvz);

	int i;

	// Blur pyramid is a 3D FEATUREIO
	FEATUREIO fioStack;
	FEATUREIO fioBlurs[BLURS_TOTAL];
	initFIOStack(fioImg, fioStack, (FEATUREIO*)&fioBlurs, BLURS_TOTAL);

	// Entropy pyramid, one for each blur
	FEATUREIO fioStackH;
	FEATUREIO fioBlursH[BLURS_TOTAL];
	initFIOStack(fioImg, fioStackH, (FEATUREIO*)&fioBlursH, BLURS_TOTAL);

	// Mutual information pyramid, one for each blur - 1
	FEATUREIO fioStackMI;
	FEATUREIO fioBlursMI[BLURS_TOTAL];
	initFIOStack(fioImg, fioStackMI, (FEATUREIO*)&fioBlursMI, BLURS_TOTAL);

	// DOG pyramid
	FEATUREIO fioStackDOG;
	FEATUREIO fioDOGs[BLURS_TOTAL - 1];
	initFIOStack(fioImg, fioStackDOG, (FEATUREIO*)&fioDOGs, BLURS_TOTAL - 1);

	// Edges 
	FEATUREIO fioEdgeStack;
	FEATUREIO fioEdgeXY[2];
	initFIOStack(fioImg, fioEdgeStack, (FEATUREIO*)&fioEdgeXY, 2);

	//
	// Here, the initial blur sigma is calculated along with the subsequent
	// sigma increments.
	//
	//float fSigma = 1.0f;
	float fSigmaInit = 0.5; // initial blurring: image resolution
	if (fInitialImageScale > 0) fSigmaInit /= fInitialImageScale;
	float fSigma = 1.6f; // desired initial blurring
	float fSigmaFactor = (float)pow(2.0, 1.0 / (double)BLURS_PER_OCTAVE);

	// Start initial blur: blur_2^2 = blur_1^2 + fSigmaExtra^2
	float fSigmaExtra = sqrt(fSigma*fSigma - fSigmaInit*fSigmaInit);
	gb3d_blur3d(fioImg, fioTemp, fioBlurs[0], fSigmaExtra, BLUR_PRECISION);

	// Output initial image
	ppImgOut.InitializeSubImage(
		fioImg.y,
		fioImg.x,
		fioImg.x * sizeof(float), sizeof(float) * 8,
		(unsigned char*)ppImgOutMem.ImageRow(0)
	);
	fioFeatureSliceXY(fioImg, fioImg.z / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "image.pgm");
	if (bOutput) output_float(ppImgOut, pcFileName);

	float fScale = fInitialImageScale;

	float pfBlurSigmas[BLURS_TOTAL];

	//g_outfile = fopen( "orientations.txt", "wt" );	

	int iFeatDiff = 1;
	for (int i = 0; 1; i++)
	{
		printf("Scale %d: blur...", i);

		fSigma = 1.6f; // desired initial blurring

		pfBlurSigmas[0] = fSigma;

		if (fioBlurs[0].x <= 2 || fioBlurs[0].y <= 2)
			goto finish_up; // Quit if too small

							// Compute entropy for lowest level
		__computePixelEntropyBinary(fioBlurs[0], fioBlursH[0]);

		// Generate blurs in octave
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			// How much extra blur required to raise fSigma-blurred image
			// to next bur level
			float fSigmaExtra = fSigma*sqrt(fSigmaFactor*fSigmaFactor - 1.0f);

			// Diffuse distributions p(I|x) at level (j-1) to p(I) at level (j) using p(x), fSigmaExtra
			// p(I) = sum_i p(I|x_i)p(x_i)
			int iReturn = gb3d_blur3d(fioBlurs[j - 1], fioTemp, fioBlurs[j], fSigmaExtra, BLUR_PRECISION);
			if (iReturn == 0)
				goto finish_up;

			// Compute entropy H(I) of p(I) at level (j)
			__computePixelEntropyBinary(fioBlurs[j], fioBlursH[j]);

			// Compute conditional entropy of p(I|x) by diffusing H(I|x): H(I|x) = sum_i p(x_i) H(I|x_i)
			gb3d_blur3d(fioBlursH[j - 1], fioTemp, fioBlursMI[j], fSigmaExtra, BLUR_PRECISION);

			fSigma *= fSigmaFactor;
			pfBlurSigmas[j] = fSigma;
		}

		// Output images to see
		for (int j = 0; j < BLURS_TOTAL; j++)
		{
			ppImgOut.InitializeSubImage(
				fioBlurs[j].y,
				fioBlurs[j].x,
				fioBlurs[j].x * sizeof(float), sizeof(float) * 8,
				(unsigned char*)ppImgOutMem.ImageRow(0)
			);
			fioFeatureSliceXY(fioBlurs[j], fioBlurs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "blur%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);

			ppImgOut.InitializeSubImage(
				fioBlursH[j].y,
				fioBlursH[j].x,
				fioBlursH[j].x * sizeof(float), sizeof(float) * 8,
				(unsigned char*)ppImgOutMem.ImageRow(0)
			);
			fioFeatureSliceXY(fioBlursH[j], fioBlursH[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "info%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);

			if (j > 0)
			{
				ppImgOut.InitializeSubImage(
					fioBlursMI[j].y,
					fioBlursMI[j].x,
					fioBlursMI[j].x * sizeof(float), sizeof(float) * 8,
					(unsigned char*)ppImgOutMem.ImageRow(0)
				);
				fioFeatureSliceXY(fioBlursMI[j], fioBlursMI[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
				sprintf(pcFileName, "conditional_info%2.2d.pgm", j);
				if (bOutput) output_float(ppImgOut, pcFileName);
			}
		}

		// Save 2*sigma image
		fioCopy(fioTemp, fioBlurs[BLURS_PER_OCTAVE]);

		//ppImgOut.InitializeSubImage( fioTemp.y, fioTemp.x, fioTemp.x*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceXY( fioTemp,  fioTemp.z/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "temp%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		printf("diff...");

		// Create difference of entropies, mutual information
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			fioMultSum(fioBlursH[j], fioBlursMI[j], fioDOGs[j - 1], -1.0f);

			// Multiply by current scale
			float fSigma = pfBlurSigmas[j];
			fioMult(fioDOGs[j - 1], fioDOGs[j - 1], fSigma);
		}

		// Output dogs to see 
		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			ppImgOut.InitializeSubImage(fioDOGs[j].y, fioDOGs[j].x, fioDOGs[j].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
			fioFeatureSliceXY(fioDOGs[j], fioDOGs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "mutual_info%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);
		}

		printf("peaks...");

		// Detect peaks and create features
		int iFeatsCurr = vecFeats.size();
		for (int j = 0; j < BLURS_PER_OCTAVE; j++)
		{
			// This is the only place yet there is a difference in 2D/3D

			// Perform peak selection
			detectExtrema3D(
				fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
				lvaMinima,
				lvaMaxima
			);

			// Generate edges, for orientation
			fioGenerateEdgeImages2D(
				fioBlurs[j + 1],
				fioEdgeXY[0], fioEdgeXY[1]
			);

			// Output edge images to see
			int k;
			for (k = 0; k < 2; k++)
			{
				ppImgOut.InitializeSubImage(fioEdgeXY[k].y, fioEdgeXY[k].x, fioEdgeXY[k].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
				fioFeatureSliceXY(fioEdgeXY[k], fioEdgeXY[k].z / 2, 0, (float*)ppImgOut.ImageRow(0));
				sprintf(pcFileName, "edge%2.2d.pgm", k);
				if (bOutput) output_float(ppImgOut, pcFileName);
			}

			// Generate features for points found
			lvaMinima.iCount = 0; // Only consider MI maxima
								  //lvaMaxima.iCount = 0; // Only consider MI minima
			int iFirstFeat = vecFeats.size();
			generateFeatures2D(
				lvaMinima, lvaMaxima,
				fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
				pfBlurSigmas[j], pfBlurSigmas[j + 1], pfBlurSigmas[j + 2],
				fioBlurs[j],
				fioEdgeXY[0],
				fioEdgeXY[1],
				pfBlurSigmas[j + 1],
				vecFeats
			);

			// Update geometry to image space
			float fFactor = fScale;
			float fAddend = 0;//fFactor/2.0f;
			for (int iFU = iFirstFeat; iFU < vecFeats.size(); iFU++)
			{
				// Update feature scale to actual
				vecFeats[iFU].scale *= fFactor;
				// Should also update location...
				vecFeats[iFU].x = vecFeats[iFU].x*fFactor + fAddend;
				vecFeats[iFU].y = vecFeats[iFU].y*fFactor + fAddend;
			}

		}

		iFeatDiff = vecFeats.size() - iFeatsCurr;
		printf("%d...", vecFeats.size() - iFeatsCurr);

		printf("subsample...");

		fioBlurs[0].x /= 2;
		fioBlurs[0].y /= 2;
		//fioBlurs[0].z /= 2;	
		// Subsample sigma=2 image into first position
		//fioSubSample( fioTemp, fioBlurs[0] );
		fioSubSampleInterpolate(fioTemp, fioBlurs[0]);

		//ppImgOut.InitializeSubImage( fioBlurs[0].z, fioBlurs[0].y, fioBlurs[0].y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioBlurs[0],  fioBlurs[0].x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "blur_first%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		//ppImgOut.InitializeSubImage( fioTemp.z, fioTemp.y, fioTemp.y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioTemp,  fioTemp.x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "temp_after%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		// Halve dimensions of all blur images
		// fioBlurs[0] is already done above
		fioTemp.x /= 2;
		fioTemp.y /= 2;
		//fioTemp.z /= 2;
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			fioBlurs[j].x /= 2;
			fioBlurs[j].y /= 2;
			//fioBlurs[j].z /= 2;
			int iElementsPerImage = fioBlurs[j].z*fioBlurs[j].y*fioBlurs[j].x;
			fioBlurs[j].pfVectors = fioStack.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < BLURS_TOTAL; j++)
		{
			fioBlursH[j].x /= 2;
			fioBlursH[j].y /= 2;
			//fioBlursH[j].z /= 2;
			int iElementsPerImage = fioBlursH[j].z*fioBlursH[j].y*fioBlursH[j].x;
			fioBlursH[j].pfVectors = fioStackH.pfVectors + j*iElementsPerImage;

			fioBlursMI[j].x /= 2;
			fioBlursMI[j].y /= 2;
			//fioBlursMI[j].z /= 2;
			iElementsPerImage = fioBlursMI[j].z*fioBlursMI[j].y*fioBlursMI[j].x;
			fioBlursMI[j].pfVectors = fioStackMI.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			fioDOGs[j].x /= 2;
			fioDOGs[j].y /= 2;
			//fioDOGs[j].z /= 2;
			int iElementsPerImage = fioDOGs[j].z*fioDOGs[j].y*fioDOGs[j].x;
			fioDOGs[j].pfVectors = fioStackDOG.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < 2; j++)
		{
			fioEdgeXY[j].x /= 2;
			fioEdgeXY[j].y /= 2;
			int iElementsPerImage = fioEdgeXY[j].z*fioEdgeXY[j].y*fioEdgeXY[j].x;
			fioEdgeXY[j].pfVectors = fioEdgeStack.pfVectors + j*iElementsPerImage;
		}

		fScale *= 2.0f;

		printf("done.\n");
	}

finish_up:

	delete[] lvaMaxima.plvz;
	delete[] lvaMinima.plvz;

	fioDelete(fioStackDOG);
	fioDelete(fioEdgeStack);
	fioDelete(fioStack);
	fioDelete(fioTemp);

	//fclose( g_outfile );

	return 1;
}


//
// 
// Apparently this is precisely the same as:
//            msGeneratePyramidMutualInfo2D()
// Two spatial classes don't do anything - UNTIL YOU FIXED IT that is!! ;)
//
int
msGeneratePyramidMutualInfoSpatialClasses2D(
	FEATUREIO	&fioImg,
	vector<Feature2D> &vecFeats,
	float fInitialImageScale,
	int bDense,
	int bOutput
)
{
	int iScaleCount = 4;
	char pcFileName[300];

	PpImage ppImgOut;
	PpImage ppImgOutMem;
	ppImgOutMem.Initialize(1, fioImg.x*fioImg.y*fioImg.z, fioImg.x*fioImg.y*fioImg.z * sizeof(float), sizeof(float) * 8);

	// Allocate temporary arrays for blurring

	FEATUREIO fioTemp;
	FEATUREIO fioImgBase;
	fioImgBase = fioTemp = fioImg;
	fioImgBase.pfVectors = fioTemp.pfVectors = 0;
	fioAllocate(fioTemp);
	fioAllocate(fioImgBase);

	LOCATION_VALUE_XYZ_ARRAY lvaMaxima, lvaMinima;
	lvaMaxima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	lvaMinima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	assert(lvaMaxima.plvz);
	assert(lvaMinima.plvz);

	int i;

	// Blur pyramid is a 3D FEATUREIO
	FEATUREIO fioStack;
	FEATUREIO fioBlurs[BLURS_TOTAL];
	initFIOStack(fioImg, fioStack, (FEATUREIO*)&fioBlurs, BLURS_TOTAL);

	// Entropy pyramid, one for each blur
	FEATUREIO fioStackH;
	FEATUREIO fioBlursH[BLURS_TOTAL];
	initFIOStack(fioImg, fioStackH, (FEATUREIO*)&fioBlursH, BLURS_TOTAL);

	// Conditional entropy for spatial classes 1 and 2
	FEATUREIO fioStackHC1;
	FEATUREIO fioBlursHC1[BLURS_TOTAL];
	initFIOStack(fioImg, fioStackHC1, (FEATUREIO*)&fioBlursHC1, BLURS_TOTAL);
	FEATUREIO fioStackHC2;
	FEATUREIO fioBlursHC2[BLURS_TOTAL];
	initFIOStack(fioImg, fioStackHC2, (FEATUREIO*)&fioBlursHC2, BLURS_TOTAL);

	// Images for chaped probability functions, prior to conditional entropy calculation
	FEATUREIO fioBlurPC1;
	FEATUREIO fioBlurPC2;
	fioBlurPC1 = fioBlurPC2 = fioImg;
	fioBlurPC1.pfVectors = fioBlurPC2.pfVectors = 0;
	fioAllocate(fioBlurPC1);
	fioAllocate(fioBlurPC2);

	// DOG pyramid
	FEATUREIO fioStackDOG;
	FEATUREIO fioDOGs[BLURS_TOTAL - 1];
	initFIOStack(fioImg, fioStackDOG, (FEATUREIO*)&fioDOGs, BLURS_TOTAL - 1);

	// Edges 
	FEATUREIO fioEdgeStack;
	FEATUREIO fioEdgeXY[2];
	initFIOStack(fioImg, fioEdgeStack, (FEATUREIO*)&fioEdgeXY, 2);

	//
	// Here, the initial blur sigma is calculated along with the subsequent
	// sigma increments.
	//
	//float fSigma = 1.0f;
	float fSigmaInit = 0.5; // initial blurring: image resolution
	if (fInitialImageScale > 0) fSigmaInit /= fInitialImageScale;
	float fSigma = 1.6f; // desired initial blurring
	float fSigmaFactor = (float)pow(2.0, 1.0 / (double)BLURS_PER_OCTAVE);

	// Start initial blur: blur_2^2 = blur_1^2 + fSigmaExtra^2
	float fSigmaExtra = sqrt(fSigma*fSigma - fSigmaInit*fSigmaInit);
	gb3d_blur3d(fioImg, fioTemp, fioBlurs[0], fSigmaExtra, BLUR_PRECISION);

	// Output initial image
	ppImgOut.InitializeSubImage(
		fioImg.y,
		fioImg.x,
		fioImg.x * sizeof(float), sizeof(float) * 8,
		(unsigned char*)ppImgOutMem.ImageRow(0)
	);
	fioFeatureSliceXY(fioImg, fioImg.z / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "image.pgm");
	if (bOutput) output_float(ppImgOut, pcFileName);

	float fScale = fInitialImageScale;

	float pfBlurSigmas[BLURS_TOTAL];

	//g_outfile = fopen( "orientations.txt", "wt" );	

	int iFeatDiff = 1;
	for (int i = 0; 1; i++)
	{
		printf("Scale %d: blur...", i);

		fSigma = 1.6f; // desired initial blurring

		pfBlurSigmas[0] = fSigma;

		if (fioBlurs[0].x <= 2 || fioBlurs[0].y <= 2)
			goto finish_up; // Quit if too small

							// Compute entropy for lowest level
		__computePixelEntropyBinary(fioBlurs[0], fioBlursH[0]);

		// Generate blurs in octave
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			// How much extra blur required to raise fSigma-blurred image
			// to next bur level
			float fSigmaExtra = fSigma*sqrt(fSigmaFactor*fSigmaFactor - 1.0f);

			// Diffuse distributions p(I|x) at level (j-1) to p(I) at level (j) using p(x), fSigmaExtra
			// p(I) = sum_i p(I|x_i)p(x_i)
			int iReturn = gb3d_blur3d(fioBlurs[j - 1], fioTemp, fioBlurs[j], fSigmaExtra, BLUR_PRECISION);
			if (iReturn == 0)
				goto finish_up;

			// Compute entropy H(I) of p(I) at level (j)
			__computePixelEntropyBinary(fioBlurs[j], fioBlursH[j]);

			// Compute conditional entropy for different spatial classes
			//gb3d_separable_2_blur3d_interleave( fioBlursH[j-1], fioTemp, fioBlursHC1[j], fioBlursHC2[j], 0, fSigmaExtra, BLUR_PRECISION );
			gb3d_separable_2_blur3d_interleave(fioBlurs[j - 1], fioTemp, fioBlurPC1, fioBlurPC2, 0, fSigmaExtra, BLUR_PRECISION);
			__computePixelEntropyBinary(fioBlurPC1, fioBlursHC1[j]);
			__computePixelEntropyBinary(fioBlurPC2, fioBlursHC2[j]);

			//Fix this right here!!

			fSigma *= fSigmaFactor;
			pfBlurSigmas[j] = fSigma;
		}

		// Output images to see
		for (int j = 0; j < BLURS_TOTAL; j++)
		{
			ppImgOut.InitializeSubImage(
				fioBlurs[j].y,
				fioBlurs[j].x,
				fioBlurs[j].x * sizeof(float), sizeof(float) * 8,
				(unsigned char*)ppImgOutMem.ImageRow(0)
			);
			fioFeatureSliceXY(fioBlurs[j], fioBlurs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "blur%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);

			ppImgOut.InitializeSubImage(
				fioBlursH[j].y,
				fioBlursH[j].x,
				fioBlursH[j].x * sizeof(float), sizeof(float) * 8,
				(unsigned char*)ppImgOutMem.ImageRow(0)
			);
			fioFeatureSliceXY(fioBlursH[j], fioBlursH[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "info%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);

			if (j > 0)
			{
				//ppImgOut.InitializeSubImage(
				//	fioBlursMI[j].y,
				//	fioBlursMI[j].x,
				//	fioBlursMI[j].x*sizeof(float), sizeof(float)*8,
				//	(unsigned char*)ppImgOutMem.ImageRow(0)
				//	);
				//fioFeatureSliceXY( fioBlursMI[j],  fioBlursMI[j].z/2, 0, (float*)ppImgOut.ImageRow(0) );
				//sprintf( pcFileName, "conditional_info%2.2d.pgm", j );
				//output_float( ppImgOut, pcFileName );
			}
		}

		// Save 2*sigma image
		fioCopy(fioTemp, fioBlurs[BLURS_PER_OCTAVE]);

		//ppImgOut.InitializeSubImage( fioTemp.y, fioTemp.x, fioTemp.x*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceXY( fioTemp,  fioTemp.z/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "temp%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		printf("diff...");

		// Create difference of entropies to get mutual information: MI(I,C) = H(I) - sum_i p(Ci)H(I|Ci), p(Ci) = 0.5
		for (int j = 1; j < BLURS_TOTAL; j++)
		{

			//fioMultSum( fioBlursH[j], fioBlursMI[j], fioDOGs[j-1], -1.0f );
			fioMultSum(fioBlursH[j], fioBlursHC1[j], fioDOGs[j - 1], -0.5f);
			fioMultSum(fioDOGs[j - 1], fioBlursHC2[j], fioDOGs[j - 1], -0.5f);
			// Multiply by current scale
			float fSigma = pfBlurSigmas[j];
			fioMult(fioDOGs[j - 1], fioDOGs[j - 1], fSigma);

		}

		// Output dogs to see
		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			ppImgOut.InitializeSubImage(fioDOGs[j].y, fioDOGs[j].x, fioDOGs[j].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
			fioFeatureSliceXY(fioDOGs[j], fioDOGs[j].z / 2, 0, (float*)ppImgOut.ImageRow(0));
			sprintf(pcFileName, "mutual_info%2.2d.pgm", j);
			if (bOutput) output_float(ppImgOut, pcFileName);
		}

		printf("peaks...");

		// Detect peaks and create features
		int iFeatsCurr = vecFeats.size();
		for (int j = 0; j < BLURS_PER_OCTAVE; j++)
		{
			// This is the only place yet there is a difference in 2D/3D

			// Perform peak selection
			detectExtrema3D(
				fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
				lvaMinima,
				lvaMaxima
			);

			// Generate edges, for orientation
			fioGenerateEdgeImages2D(
				fioBlurs[j + 1],
				fioEdgeXY[0], fioEdgeXY[1]
			);

			// Output edge images to see
			int k;
			for (k = 0; k < 2; k++)
			{
				ppImgOut.InitializeSubImage(fioEdgeXY[k].y, fioEdgeXY[k].x, fioEdgeXY[k].x * sizeof(float), sizeof(float) * 8, (unsigned char*)ppImgOutMem.ImageRow(0));
				fioFeatureSliceXY(fioEdgeXY[k], fioEdgeXY[k].z / 2, 0, (float*)ppImgOut.ImageRow(0));
				sprintf(pcFileName, "edge%2.2d.pgm", k);
				if (bOutput) output_float(ppImgOut, pcFileName);
			}

			lvaMinima.iCount = 0; // Only consider MI maxima
								  //lvaMaxima.iCount = 0; // Only consider MI minima
								  // Generate features for points found
			int iFirstFeat = vecFeats.size();
			generateFeatures2D(
				lvaMinima, lvaMaxima,
				fioDOGs[j], fioDOGs[j + 1], fioDOGs[j + 2],
				pfBlurSigmas[j], pfBlurSigmas[j + 1], pfBlurSigmas[j + 2],
				fioBlurs[j],
				fioEdgeXY[0],
				fioEdgeXY[1],
				pfBlurSigmas[j + 1],
				vecFeats
			);

			// Update geometry to image space
			float fFactor = fScale;
			float fAddend = 0;//fFactor/2.0f;
			for (int iFU = iFirstFeat; iFU < vecFeats.size(); iFU++)
			{
				// Update feature scale to actual
				vecFeats[iFU].scale *= fFactor;
				// Should also update location...
				vecFeats[iFU].x = vecFeats[iFU].x*fFactor + fAddend;
				vecFeats[iFU].y = vecFeats[iFU].y*fFactor + fAddend;
			}

		}

		iFeatDiff = vecFeats.size() - iFeatsCurr;
		printf("%d...", vecFeats.size() - iFeatsCurr);

		printf("subsample...");

		fioBlurs[0].x /= 2;
		fioBlurs[0].y /= 2;
		//fioBlurs[0].z /= 2;	
		// Subsample sigma=2 image into first position
		//fioSubSample( fioTemp, fioBlurs[0] );
		fioSubSampleInterpolate(fioTemp, fioBlurs[0]);

		//ppImgOut.InitializeSubImage( fioBlurs[0].z, fioBlurs[0].y, fioBlurs[0].y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioBlurs[0],  fioBlurs[0].x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "blur_first%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		//ppImgOut.InitializeSubImage( fioTemp.z, fioTemp.y, fioTemp.y*sizeof(float), sizeof(float)*8, (unsigned char*)ppImgOutMem.ImageRow(0) );
		//fioFeatureSliceZY( fioTemp,  fioTemp.x/2, 0, (float*)ppImgOut.ImageRow(0) );
		//sprintf( pcFileName, "temp_after%2.2d.pgm", j );
		//output_float( ppImgOut, pcFileName );

		// Halve dimensions of all blur images
		// fioBlurs[0] is already done above
		fioTemp.x /= 2;
		fioTemp.y /= 2;
		//fioTemp.z /= 2;


		fioBlurPC1.x /= 2;
		fioBlurPC1.y /= 2;
		fioBlurPC2.x /= 2;
		fioBlurPC2.y /= 2;

		//
		// Below we resize stacks. Pointers have to be updated, so this is a little more complex
		// than simple images.
		//
		for (int j = 1; j < BLURS_TOTAL; j++)
		{
			fioBlurs[j].x /= 2;
			fioBlurs[j].y /= 2;
			//fioBlurs[j].z /= 2;
			int iElementsPerImage = fioBlurs[j].z*fioBlurs[j].y*fioBlurs[j].x;
			fioBlurs[j].pfVectors = fioStack.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < BLURS_TOTAL; j++)
		{
			fioBlursH[j].x /= 2;
			fioBlursH[j].y /= 2;
			//fioBlursH[j].z /= 2;
			int iElementsPerImage = fioBlursH[j].z*fioBlursH[j].y*fioBlursH[j].x;
			fioBlursH[j].pfVectors = fioStackH.pfVectors + j*iElementsPerImage;

			//fioBlursMI[j].x /= 2;
			//fioBlursMI[j].y /= 2;
			////fioBlursMI[j].z /= 2;
			//iElementsPerImage = fioBlursMI[j].z*fioBlursMI[j].y*fioBlursMI[j].x;
			//fioBlursMI[j].pfVectors = fioStackMI.pfVectors + j*iElementsPerImage;

			fioBlursHC1[j].x /= 2;
			fioBlursHC1[j].y /= 2;
			//fioBlursHC1[j].z /= 2;
			iElementsPerImage = fioBlursHC1[j].z*fioBlursHC1[j].y*fioBlursHC1[j].x;
			fioBlursHC1[j].pfVectors = fioStackHC1.pfVectors + j*iElementsPerImage;

			fioBlursHC2[j].x /= 2;
			fioBlursHC2[j].y /= 2;
			//fioBlursHC2[j].z /= 2;
			iElementsPerImage = fioBlursHC2[j].z*fioBlursHC2[j].y*fioBlursHC2[j].x;
			fioBlursHC2[j].pfVectors = fioStackHC2.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < BLURS_TOTAL - 1; j++)
		{
			fioDOGs[j].x /= 2;
			fioDOGs[j].y /= 2;
			//fioDOGs[j].z /= 2;
			int iElementsPerImage = fioDOGs[j].z*fioDOGs[j].y*fioDOGs[j].x;
			fioDOGs[j].pfVectors = fioStackDOG.pfVectors + j*iElementsPerImage;
		}

		for (int j = 0; j < 2; j++)
		{
			fioEdgeXY[j].x /= 2;
			fioEdgeXY[j].y /= 2;
			int iElementsPerImage = fioEdgeXY[j].z*fioEdgeXY[j].y*fioEdgeXY[j].x;
			fioEdgeXY[j].pfVectors = fioEdgeStack.pfVectors + j*iElementsPerImage;
		}

		fScale *= 2.0f;

		printf("done.\n");
	}

finish_up:

	delete[] lvaMaxima.plvz;
	delete[] lvaMinima.plvz;

	fioDelete(fioStackDOG);
	fioDelete(fioEdgeStack);
	fioDelete(fioStack);
	fioDelete(fioTemp);

	//fclose( g_outfile );

	return 1;
}

int
msFeature3DVectorOutput(
	vector<Feature3D> &vecFeats3D,
	char *pcFileName,
	int bAppearance
)
{
	FILE *outfile = fopen(pcFileName, "wb");
	if (!outfile)
	{
		return -1;
	}
	// Output header
	Feature3D junk;
	int iFeatSize = sizeof(Feature3D);
	if (!bAppearance)
	{
		// No appearance vector
		iFeatSize -= sizeof(junk.data_zyx);
	}
	int iFeatCount = vecFeats3D.size();

	fwrite(FEAT3DHEADER, 1, sizeof(FEAT3DHEADER), outfile);
	fwrite(&iFeatCount, 1, sizeof(iFeatCount), outfile);
	fwrite(&iFeatSize, 1, sizeof(iFeatSize), outfile);

	// Output features
	for (int i = 0; i < vecFeats3D.size(); i++)
	{
		vecFeats3D[i].ToFileBin(outfile, bAppearance);
	}
	fclose(outfile);
	return vecFeats3D.size();
}

void
_convertGradientToAzEl(
	float *pfEdge,
	float &fAz,
	float &fEl
)
{
	fAz = atan2(pfEdge[1], pfEdge[0]);
	float fMagXY = sqrt(pfEdge[0] * pfEdge[0] + pfEdge[1] * pfEdge[1]);
	fEl = atan2(pfEdge[2], fMagXY);
}

void
_convertAzElToGradient(
	float *pfEdge,
	float &fAz,
	float &fEl
)
{
	pfEdge[0] = cos(fEl)*cos(fAz);
	pfEdge[1] = cos(fEl)*sin(fAz);
	pfEdge[2] = sin(fEl);
}

//int
//msFeature3DVectorOutputText(
//				  vector<Feature3D> &vecFeats3D,
//				  char *pcFileName,
//				  float fEigThres
//)
//{
//	FILE *outfile = fopen( pcFileName, "wt" );
//	if( !outfile )
//	{
//		return -1;
//	}
//	int iFeatCount = 0; //vecFeats3D.size();
//
//	for( int i = 0; i < vecFeats3D.size(); i++ )
//	{
//		Feature3D &feat3D = vecFeats3D[i];
//
//		// Sphere, apply threshold
//		float fEigSum = feat3D.eigs[0] + feat3D.eigs[1] + feat3D.eigs[2];
//		float fEigPrd = feat3D.eigs[0]*feat3D.eigs[1]*feat3D.eigs[2];
//		float fEigSumProd = fEigSum*fEigSum*fEigSum;
//		if( fEigSumProd < fEigThres*fEigPrd || fEigThres < 0 )
//		{
//			iFeatCount++;
//		}
//	}
//
//	fprintf( outfile, "Features: %d\n", iFeatCount );
//	fprintf( outfile, "Scale-space location[x y z scale] orientation[o11 o12 o13 o21 o22 o23 o31 o32 o32] 2nd moment eigenvalues[e1 e2 e3] info flag[i1] descriptor[d1 .. d64]\n" );
//
//	for( int i = 0; i < vecFeats3D.size(); i++ )
//	{		
//		Feature3D &feat3D = vecFeats3D[i];
//
//		// Sphere, apply threshold
//		float fEigSum = feat3D.eigs[0] + feat3D.eigs[1] + feat3D.eigs[2];
//		float fEigPrd = feat3D.eigs[0]*feat3D.eigs[1]*feat3D.eigs[2];
//		float fEigSumProd = fEigSum*fEigSum*fEigSum;
//		if( fEigSumProd < fEigThres*fEigPrd || fEigThres < 0 )
//		{
//		}
//		else
//		{
//			continue;
//		}
//
//		// Location and scale
//		fprintf( outfile, "%f\t%f\t%f\t%f\t", vecFeats3D[i].x, vecFeats3D[i].y, vecFeats3D[i].z, vecFeats3D[i].scale );
//
//		// Orientation (could save a vector here)
//		for( int j = 0; j < 3; j++ )
//		{
//			for( int k = 0; k < 3; k++ )
//			{
//				fprintf( outfile, "%f\t", vecFeats3D[i].ori[j][k] );
//			}
//		}
//
//		// Eigenvalues of 2nd moment matrix
//		for( int j = 0; j < 3; j++ )
//		{
//			fprintf( outfile, "%f\t", vecFeats3D[i].eigs[j] );
//		}
//
//		// Info flag
//		fprintf( outfile, "%d\t", vecFeats3D[i].m_uiInfo );
//
//		// Output principal components, if set
//		for( int j = 0; j < Feature3DInfo::FEATURE_3D_PCS; j++ )
//		{
//			fprintf( outfile, "%i\t", (unsigned char)(vecFeats3D[i].m_pfPC[j]) );
//		}
//		fprintf( outfile, "\n" );
//	}
//
//	fclose( outfile );
//	return 0;
//}
//
//
//
//int
//msFeature3DVectorInputText(
//				  vector<Feature3DInfo> &vecFeats3D,
//				  char *pcFileName,
//				  float fEigThres
//)
//{
//	FILE *infile = fopen( pcFileName, "rt" );
//	if( !infile )
//	{
//		return -1;
//	}
//	int iFeatCount = vecFeats3D.size();
//
//	char buff[400];
//
//	fscanf( infile, "Features: %d\n", &iFeatCount );
//	fscanf( infile, "Scale-space location[x y z scale] orientation[o11 o12 o13 o21 o22 o23 o31 o32 o32] 2nd moment eigenvalues[e1 e2 e3] info flag[i1] descriptor[d1 .. d64]\n" );
//
//	vecFeats3D.resize( iFeatCount );
//
//	for( int i = 0; i < iFeatCount; i++ )
//	{
//		int iReturn = 
//			fscanf( infile, "%f\t%f\t%f\t%f\t", &(vecFeats3D[i].x), &(vecFeats3D[i].y), &(vecFeats3D[i].z), &(vecFeats3D[i].scale) );
//		assert( iReturn == 4 );
//
//		for( int j = 0; j < 3; j++ )
//		{
//			for( int k = 0; k < 3; k++ )
//			{
//				iReturn = fscanf( infile, "%f\t", &(vecFeats3D[i].ori[j][k]) );
//				assert( iReturn == 1 );
//			}
//		}
//
//		// Eigenvalues of 2nd moment matrix
//		for( int j = 0; j < 3; j++ )
//		{
//			iReturn = fscanf( infile, "%f\t", &(vecFeats3D[i].eigs[j]) );
//			assert( iReturn == 1 );
//		}
//
//		// Info flag
//		iReturn = fscanf( infile, "%d\t", &(vecFeats3D[i].m_uiInfo) );
//		assert( iReturn == 1 );
//
//		for( int j = 0; j < Feature3DInfo::FEATURE_3D_PCS; j++ )
//		{
//			iReturn = fscanf( infile, "%f\t", &(vecFeats3D[i].m_pfPC[j]) );
//			assert( iReturn == 1 );
//		}
//
//		Feature3DInfo &feat3D = vecFeats3D[i];
//		float fEigSum = feat3D.eigs[0] + feat3D.eigs[1] + feat3D.eigs[2];
//		float fEigPrd = feat3D.eigs[0]*feat3D.eigs[1]*feat3D.eigs[2];
//		float fEigSumProd = fEigSum*fEigSum*fEigSum;
//		if( fEigSumProd < fEigThres*fEigPrd || fEigThres < 0 )
//		{
//		}
//		else
//		{
//			i--;
//			iFeatCount--;
//		}
//	}
//
//	fclose( infile );
//
//	vecFeats3D.resize( iFeatCount );
//
//	return 0;
//}





int
msCompatibleFeatures(
	vector<Feature3D> &vecFeats1,
	vector<Feature3D> &vecFeats2
)
{
	int iCount = 0;
	vector< list<int> > vecDups;
	vecDups.resize(vecFeats1.size());
	for (int i = 0; i < vecFeats1.size(); i++)
	{
		vecDups[i].clear();
		for (int j = 0; j < vecFeats2.size(); j++)
		{
			if (i != j)
			{
				if (compatible_features(vecFeats1[i], vecFeats2[j], LOG_1_5, 0.5, 0.9f))
				{
					compatible_features(vecFeats1[i], vecFeats2[j], LOG_1_5, 0.5, 0.9f);
					vecDups[i].push_back(j);
					iCount++;
				}
			}
		}
	}

	vector< int > vecToDelete;
	vecToDelete.resize(vecFeats1.size());
	for (int i = 0; i < vecFeats1.size(); i++)
	{
		int iCountThis = vecDups[i].size();
		int iCountMaxOther = -1;
		int iIndexMinOther = i;
		for (list<int>::iterator it = vecDups[i].begin(); it != vecDups[i].end(); it++)
		{
			// If this feature has as many or more duplicate features
			// than others, then remove.
			int iIndexOther = *it;
			int iCountOther = vecDups[iIndexOther].size();
			if (iCountOther > iCountMaxOther)
			{
				// Save max count of other feature
				iCountMaxOther = iCountOther;
			}

			if (iCountOther == iCountThis)
			{
				// Save min index of other feature with same count
				if (iIndexOther < iIndexMinOther)
				{
					iIndexMinOther = iIndexOther;
				}
			}
		}

		if (
			(iCountMaxOther > iCountThis) ||
			(iCountMaxOther == iCountThis && iIndexMinOther < i)
			)
		{
			vecToDelete[i] = 1;
		}
		else
		{
			vecToDelete[i] = 0;
		}
	}

	// Erase all flagged features
	for (int i = vecFeats1.size() - 1; i >= 0; i--)
	{
		if (vecToDelete[i])
		{
			vecFeats1.erase(vecFeats1.begin() + i);
		}
	}

	return iCount;
}


int
msFeature3DVectorInput(
	vector<Feature3D> &vecFeats3D,
	char *pcFileName,
	float fEigThres
)
{
	FILE *infile = fopen(pcFileName, "rb");
	if (!infile)
	{
		return -1;
	}

	char pchHeader[sizeof(FEAT3DHEADER) + 1];
	int iFeatSize;
	int iFeatCount;

	fread(pchHeader, 1, sizeof(FEAT3DHEADER), infile);
	if (strncmp(pchHeader, FEAT3DHEADER, 6) != 0) // First 6 letters: FEAT3D
	{
		//printf( "Error: invalid feature 3D header.\n" );
		return -1;
	}

	Feature3D junk;
	fread(&iFeatCount, 1, sizeof(iFeatCount), infile);
	fread(&iFeatSize, 1, sizeof(iFeatSize), infile);
	Feature3DInfo junkInfo;

	int iSizeF3D__000 = 5392;// (info+data), no longer supported
	int iSizeF3DI_000 = 68;// 	(info), ok, small size
	int iSizeF3DI_001 = 260;//	(info+PC48) add pcs
	int iSizeF3D__001 = 5584;//	(info+PC48+data)
	int iSizeF3DI_001_PC64 = 324;//	(info+GOH/PC64)
	int iSizeF3D__001_PC64 = 5648;//	(info+GOH/PC64+data)

	if (
		iFeatSize != iSizeF3D__000
		&& iFeatSize != iSizeF3DI_000
		&& iFeatSize != iSizeF3D__001
		&& iFeatSize != iSizeF3DI_001
		&& iFeatSize != iSizeF3DI_001_PC64
		&& iFeatSize != iSizeF3D__001_PC64
		)
	{
		printf("Error: invalid feature 3D size.\n");
		return -1;
	}

	Feature3D feat3D;
	while (feat3D.FromFileBin(infile, iFeatSize))
	{
		int bAcceptFeature = 0;
		if (feat3D.m_uiInfo & INFO_FLAG_LINE)
		{
			// Line, accept all
			bAcceptFeature = 1;
		}
		else
		{
			// Sphere, apply threshold
			float fEigSum = feat3D.eigs[0] + feat3D.eigs[1] + feat3D.eigs[2];
			float fEigPrd = feat3D.eigs[0] * feat3D.eigs[1] * feat3D.eigs[2];
			float fEigSumProd = fEigSum*fEigSum*fEigSum;
			if (fEigSumProd < fEigThres*fEigPrd || fEigThres < 0)
			{
				bAcceptFeature = 1;
			}
		}

		if (bAcceptFeature)
		{
			vecFeats3D.push_back(feat3D);
			//feat3D.OutputVisualFeature( "good_feat" );
		}
		else
		{
			//printf( "bogus\n" );
			//vecFeats3D.push_back( feat3D );
			//feat3D.OutputVisualFeature( "bad_feat" );
		}
	}
	fclose(infile);
	int iReturn = vecFeats3D.size();
	return iReturn;
}

int
msFeature2DVectorOutput(
	vector<Feature2D> &vecFeats2D,
	char *pcFileName
)
{
	FILE *outfile = fopen(pcFileName, "wb");
	if (!outfile)
	{
		return -1;
	}

	for (int i = 0; i < vecFeats2D.size(); i++)
	{
		vecFeats2D[i].ToFileBin(outfile);
	}
	fclose(outfile);
	return vecFeats2D.size();
}

int
msFeature2DVectorOutputSIFTFormat(
	vector<Feature2D> &vecFeats2D,
	char *pcFileName,
	int bOutputAppearance
)
{
	FILE *outfile = fopen(pcFileName, "wt");
	if (!outfile)
	{
		return -1;
	}
	fprintf(outfile, "%d\t128\n", vecFeats2D.size());
	for (int i = 0; i < vecFeats2D.size(); i++)
	{
		Feature2D &feat = vecFeats2D[i];
		//feat.NormalizeData( feat.MeanData(), 100000, 1 );
		feat.NormalizeData(0, 100000, 1);
		//fprintf ( outfile, "%f\t%f\t%f\t%f\t", feat.y, feat.x, feat.scale, -feat.theta );
		fprintf(outfile, "%f\t%f\t%f\t%f\t", feat.y, feat.x, feat.scale, -feat.ori[0][0]);
		if (bOutputAppearance)
		{
			for (int k = 0; k < sizeof(feat.data_yx) / sizeof(float) && k < 128; k++)
			{
				unsigned char uch = feat.data_yx[0][k];
				if (feat.data_yx[0][k] > 255)
				{
					uch = 255;
				}
				fprintf(outfile, "%d ", uch);
			}
			for (int k = sizeof(feat.data_yx) / sizeof(float); k < 128; k++)
			{
				fprintf(outfile, "0 ");
			}
		}
		fprintf(outfile, "\n");
	}
	fclose(outfile);
	return vecFeats2D.size();
}

int
msFeature3DVectorOutputSIFTFormat(
	vector<Feature3D> &vecFeats3D,
	char *pcFileName,
	int bOutputAppearance
)
{
	FILE *outfile = fopen(pcFileName, "wt");
	if (!outfile)
	{
		return -1;
	}
	fprintf(outfile, "%d\t128\n", vecFeats3D.size());
	for (int i = 0; i < vecFeats3D.size(); i++)
	{
		Feature3D &feat = vecFeats3D[i];
		//feat.NormalizeData( feat.MeanData(), 100000, 1 );
		//fprintf ( outfile, "%f\t%f\t%f\t%f\t", feat.y, feat.x, feat.scale, -feat.theta );
		fprintf(outfile, "%f\t%f\t%f\t%f\t", feat.y, feat.x, feat.scale, -feat.ori[0][0]);
		if (bOutputAppearance)
		{
			for (int k = 0; k < sizeof(feat.data_zyx) / sizeof(float) && k < 128; k++)
			{
				unsigned char uch = feat.data_zyx[0][0][k];
				if (feat.data_zyx[0][0][k] > 255)
				{
					uch = 255;
				}
				fprintf(outfile, "%d ", uch);
			}
			for (int k = sizeof(feat.data_zyx) / sizeof(float); k < 128; k++)
			{
				fprintf(outfile, "0 ");
			}
		}
		fprintf(outfile, "\n");
	}
	fclose(outfile);
	return vecFeats3D.size();
}



int
msFeature2DVectorInput(
	vector<Feature2D> &vecFeats2D,
	char *pcFileName,
	float fEigThres
)
{
	FILE *infile = fopen(pcFileName, "rb");
	if (!infile)
	{
		return -1;
	}
	Feature2D feat2D;
	while (feat2D.FromFileBin(infile))
	{
		float fEigSum = feat2D.eigs[0] + feat2D.eigs[1];
		float fEigPrd = feat2D.eigs[0] * feat2D.eigs[1];
		float fEigSumProd = fEigSum*fEigSum;
		if (fEigSumProd < fEigThres*fEigPrd)
		{
			vecFeats2D.push_back(feat2D);
			//feat2D.OutputVisualFeature( "good_feat" );
		}
		else
		{
			//vecFeats2D.push_back( feat2D );
			//feat2D.OutputVisualFeature( "bad_feat" );
		}
	}
	fclose(infile);
	return vecFeats2D.size();
}

int
msBilateralFilter2D256(
	FEATUREIO	&fioImg,
	FEATUREIO	&fioImgOut,
	float fSigmaX, // spatial blur parameter
	float fSigmaI // intensity blur parameter
)
{
	char pcFileName[300];

	PpImage ppImgOut;
	PpImage ppImgOutMem;
	ppImgOutMem.Initialize(1, fioImg.x*fioImg.y*fioImg.z, fioImg.x*fioImg.y*fioImg.z * sizeof(float), sizeof(float) * 8);

	// Allocate temporary arrays for blurring, same size as fioImg
	FEATUREIO fioTemp;
	FEATUREIO fioBlur;
	FEATUREIO fioBinary;
	fioBinary = fioBlur = fioTemp = fioImg;
	fioBinary.pfVectors = fioBlur.pfVectors = fioTemp.pfVectors = 0;

	fioAllocate(fioTemp);
	fioAllocate(fioBinary);

	// Blur image will point to cube
	//fioAllocate( fioBlur ); 

	// Allocate cube for storing blurs
	FEATUREIO fioCube = fioImg;
	fioCube.z = 256;
	fioAllocate(fioCube);

	for (int i = 0; i < 256; i++)
	{
		// Create binary image
		for (int j = 0; j < fioImg.x*fioImg.y; j++)
		{
			if (fioImg.pfVectors[j] == i)
			{
				fioBinary.pfVectors[j] = 1;
			}
			else
			{
				fioBinary.pfVectors[j] = 0;
			}
		}

		fioBlur.pfVectors = fioGetVector(fioCube, 0, 0, i);

		gb3d_blur3d(fioBinary, fioTemp, fioBlur, fSigmaX, BLUR_PRECISION);

		// Output blurred intensity
		//ppImgOut.InitializeSubImage(
		//	fioBlur.y,
		//	fioBlur.x,
		//	fioBlur.x*sizeof(float), sizeof(float)*8,
		//	(unsigned char*)fioBlur.pfVectors
		//	);
		//sprintf( pcFileName, "intensity%3.3d.pgm", i );
		//output_float( ppImgOut, pcFileName );
	}

	// Blur all spatial columns

	// Create intensity filter
	PpImage ppImgFilter;
	int iFilterLen = calculate_gaussian_filter_size(fSigmaI, BLUR_PRECISION);
	ppImgFilter.Initialize(1, iFilterLen, iFilterLen * sizeof(float), sizeof(float) * 8);
	assert((iFilterLen % 2) == 1);
	generate_gaussian_filter1d(ppImgFilter, fSigmaI, iFilterLen / 2);
	float fSum = 0;
	float *pfFilter = (float*)ppImgFilter.ImageRow(0);
	for (int c = 0; c < ppImgFilter.Cols(); c++)
	{
		fSum += pfFilter[c];
	}
	for (int c = 0; c < ppImgFilter.Cols(); c++)
	{
		pfFilter[c] /= fSum;
	}
	fSum = 0;
	for (int c = 0; c < ppImgFilter.Cols(); c++)
	{
		fSum += pfFilter[c];
	}

	// Create temporary blur buffer
	int half_len = ppImgFilter.Cols() / 2;
	int iBufferDim = 256 + ppImgFilter.Cols();
	float *pfBufferIn = new float[iBufferDim];
	float *pfBufferOut = new float[iBufferDim];
	assert(pfBufferIn);
	assert(pfBufferOut);
	for (int c = 0; c < iBufferDim; c++)
	{
		// Zero borders
		pfBufferIn[c] = 0;
	}

	//// sum up - see if it looks like a gaussian
	//for( int yy = 0; yy < fioImg.y; yy++ )
	//{
	//	for( int xx = 0; xx < fioImg.x; xx++ )
	//	{
	//		//
	//		float fSum = 0;
	//		for( int i = 0; i < 256; i++ )
	//		{
	//			fSum += i*fioGetPixel( fioCube, xx, yy, i );
	//		}
	//		float *pfOut = fioGetVector( fioImgOut, xx, yy, 0 );
	//		*pfOut = fSum;
	//	}
	//}
	//return 0;

	//for( int yy = 0; yy < fioImg.y; yy++ )
	//{
	//	for( int xx = 0; xx < fioImg.x; xx++ )
	//	{
	//		// 
	//		for( int i = 0; i < 256; i++ )
	//		{
	//			//pfBufferIn[i+half_len] = i*fioGetPixel( fioCube, xx, yy, i );
	//			pfBufferIn[i+half_len] = i*fioGetPixel( fioCube, xx, yy, i );
	//		}

	//		// Filter
	//		filter_1d( pfFilter, pfBufferIn, pfBufferOut, iFilterLen, iBufferDim );

	//		// Copy back to image
	//		for( int i = 0; i < 256; i++ )
	//		{
	//			float *pf = fioGetVector( fioCube, xx, yy, i );
	//			*pf = pfBufferOut[i+half_len]; 
	//		}
	//	}
	//}

	// Don't blur, calculate the Gaussian around the pixel value
	for (int yy = 0; yy < fioImg.y; yy++)
	{
		for (int xx = 0; xx < fioImg.x; xx++)
		{
			float fPix = fioGetPixel(fioImg, xx, yy, 0);
			int iPix = (int)fPix;

			//float fPixBlur = fioGetPixel( fioCube, xx, yy, iPix );

			// Create 256-sample Gaussian
			//for( int i = 0; i < 256; i++ )
			//{
			//	pfBufferIn[i] = 0;
			//}
			float fSum = 0;
			for (int i = 0; i < 256; i++)
			{
				float fCubePixel = fioGetPixel(fioCube, xx, yy, i);
				int iFiltIndex = half_len + iPix - i;
				if (iFiltIndex >= 0 && iFiltIndex < iFilterLen)
				{
					pfBufferIn[i] = pfFilter[iFiltIndex] * fCubePixel;
				}
				else
				{
					pfBufferIn[i] = 0;
				}
				fSum += pfBufferIn[i];
			}
			// Normalize so buffer sums to 1
			if (fSum <= 0)
			{
				fSum = 0;
			}
			else
			{
				fSum = 1.0f / fSum;
			}
			//assert( fSum > 0 );
			for (int i = 0; i < 256; i++)
			{
				pfBufferIn[i] *= fSum;
			}

			// Calculate expected pixel
			float fPixelOutput = 0;
			for (int i = 0; i < 256; i++)
			{
				fPixelOutput += i*pfBufferIn[i];
			}

			float *pfOut = fioGetVector(fioImgOut, xx, yy, 0);
			*pfOut = fPixelOutput;
		}
	}

	//// Set output blur
	//for( int yy = 0; yy < fioImg.y; yy++ )
	//{
	//	for( int xx = 0; xx < fioImg.x; xx++ )
	//	{
	//		float fPix = fioGetPixel( fioImg, xx, yy, 0 );
	//		int iPix = (int)fPix;
	//		float fPixBlur = fioGetPixel( fioCube, xx, yy, iPix );

	//		float *pfOut = fioGetVector( fioImgOut, xx, yy, 0 );
	//		*pfOut = fPixBlur;
	//	}
	//}

	fioDelete(fioTemp);
	fioDelete(fioBinary);
	fioDelete(fioCube);

	return 0;
}


int
msGenerateScaleSpaceCube(
	FEATUREIO	&fioImg,
	FEATUREIO	&fioImgOut,
	float fInitialImageScale,
	float fSamplingRate,
	int bOutput
)
{
	char pcFileName[300];

	PpImage ppImgOut;
	PpImage ppImgOutMem;
	ppImgOutMem.Initialize(1, fioImg.x*fioImg.y*fioImg.z, fioImg.x*fioImg.y*fioImg.z * sizeof(float), sizeof(float) * 8);

	// Allocate temporary arrays for blurring
	FEATUREIO fioTemp;
	FEATUREIO fioImgBase;
	fioImgBase = fioTemp = fioImg;
	fioImgBase.pfVectors = fioTemp.pfVectors = 0;
	fioAllocate(fioTemp);
	fioAllocate(fioImgBase);

	LOCATION_VALUE_XYZ_ARRAY lvaMaxima, lvaMinima;
	lvaMaxima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	lvaMinima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	assert(lvaMaxima.plvz);
	assert(lvaMinima.plvz);

	int i;

	//
	// Here, the initial blur sigma is calculated along with the subsequent
	// sigma increments.
	//
	//float fSigma = 1.0f;
	float fSigmaInit = 0.5; // initial blurring: image resolution
	if (fInitialImageScale > 0) fSigmaInit /= fInitialImageScale;
	float fSigma = 1.6f; // desired initial blurring
						 //float fSigmaFactor = (float)pow( 2.0, 1.0/(double)fSamplingRate );
	float fSigmaFactor = (float)pow(2.0, (double)fSamplingRate);

	// Reference to blur output
	FEATUREIO fioBlur;
	fioBlur = fioImg;

	FEATUREIO fioBlurIn = fioImg;

	// Start initial blur: blur_2^2 = blur_1^2 + fSigmaExtra^2
	float fSigmaExtra = sqrt(fSigma*fSigma - fSigmaInit*fSigmaInit);
	fioBlur.pfVectors = fioGetVector(fioImgOut, 0, 0, 0);
	gb3d_blur3d(fioImg, fioTemp, fioBlur, fSigmaExtra, BLUR_PRECISION);

	// Output initial image
	ppImgOut.InitializeSubImage(
		fioImg.y,
		fioImg.x,
		fioImg.x * sizeof(float), sizeof(float) * 8,
		(unsigned char*)ppImgOutMem.ImageRow(0)
	);
	fioFeatureSliceXY(fioImg, fioImg.z / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "image.pgm");
	if (bOutput) output_float(ppImgOut, pcFileName);

	float fScale = fInitialImageScale;

	fSigma = 1.6f; // desired initial blurring

	for (int iLevel = 1; iLevel < fioImgOut.z; iLevel++)
	{
		printf("Scale %d: blur...", iLevel);

		// How much extra blur required to raise fSigma-blurred image
		// to next bur level
		float fSigmaExtra = fSigma*sqrt(fSigmaFactor*fSigmaFactor - 1.0f);

		fioBlurIn.pfVectors = fioGetVector(fioImgOut, 0, 0, iLevel - 1);
		fioBlur.pfVectors = fioGetVector(fioImgOut, 0, 0, iLevel);
		int iReturn = gb3d_blur3d(fioBlurIn, fioTemp, fioBlur, fSigmaExtra, BLUR_PRECISION);
		if (iReturn == 0)
		{
			return -1;
		}

		fSigma *= fSigmaFactor;
	}

	return 0;
}

int
msGenerateScaleSpaceCubeMI(
	FEATUREIO	&fioImg,
	FEATUREIO	&fioImgOut,
	float fInitialImageScale,
	float fSamplingRate,
	int bOutput
)
{
	char pcFileName[300];

	// Normalize to obtain a distribution
	fioNormalize(fioImg, 1.0f);

	PpImage ppImgOut;
	PpImage ppImgOutMem;
	ppImgOutMem.Initialize(1, fioImg.x*fioImg.y*fioImg.z, fioImg.x*fioImg.y*fioImg.z * sizeof(float), sizeof(float) * 8);

	// Allocate temporary arrays for blurring
	FEATUREIO fioTemp;
	FEATUREIO fioImgBase;
	fioImgBase = fioTemp = fioImg;
	fioImgBase.pfVectors = fioTemp.pfVectors = 0;
	fioAllocate(fioTemp);
	fioAllocate(fioImgBase);

	LOCATION_VALUE_XYZ_ARRAY lvaMaxima, lvaMinima;
	lvaMaxima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	lvaMinima.plvz = new LOCATION_VALUE_XYZ[fioImg.x*fioImg.y];
	assert(lvaMaxima.plvz);
	assert(lvaMinima.plvz);

	// Blur stack, 0 : current, 1 : previous
	FEATUREIO fioStack;
	FEATUREIO fioBlurs[2];
	initFIOStack(fioImg, fioStack, (FEATUREIO*)&fioBlurs, 2);

	// Entropy pyramid, 0 : current, 1 : previous
	FEATUREIO fioStackH;
	FEATUREIO fioBlursH[2];
	initFIOStack(fioImg, fioStackH, (FEATUREIO*)&fioBlursH, 2);

	// Conditional entropy, only at current level
	FEATUREIO fioHCond;
	fioHCond = fioImg;
	fioHCond.pfVectors = 0;
	fioAllocate(fioHCond);

	int i;

	//
	// Here, the initial blur sigma is calculated along with the subsequent
	// sigma increments.
	//
	//float fSigma = 1.0f;
	float fSigmaInit = 0.5; // initial blurring: image resolution
	if (fInitialImageScale > 0) fSigmaInit /= fInitialImageScale;
	float fSigma = 1.6f; // desired initial blurring
						 //float fSigmaFactor = (float)pow( 2.0, 1.0/(double)fSamplingRate );
	float fSigmaFactor = (float)pow(2.0, (double)fSamplingRate);

	// Reference to blur output
	FEATUREIO fioBlur;
	fioBlur = fioImg;

	FEATUREIO fioBlurIn = fioImg;

	// Start initial blur: blur_2^2 = blur_1^2 + fSigmaExtra^2
	float fSigmaExtra = sqrt(fSigma*fSigma - fSigmaInit*fSigmaInit);
	fioBlur.pfVectors = fioGetVector(fioImgOut, 0, 0, 0);
	gb3d_blur3d(fioImg, fioTemp, fioBlurs[0], fSigmaExtra, BLUR_PRECISION);

	// Output initial image
	ppImgOut.InitializeSubImage(
		fioImg.y,
		fioImg.x,
		fioImg.x * sizeof(float), sizeof(float) * 8,
		(unsigned char*)ppImgOutMem.ImageRow(0)
	);
	fioFeatureSliceXY(fioImg, fioImg.z / 2, 0, (float*)ppImgOut.ImageRow(0));
	sprintf(pcFileName, "image.pgm");
	if (bOutput) output_float(ppImgOut, pcFileName);

	float fScale = fInitialImageScale;

	fioSet(fioImgOut, 0);

	// Compute entropy for lowest level
	__computePixelEntropyBinary(fioBlurs[0], fioBlursH[0]);

	for (int iLevel = 1; iLevel < fioImgOut.z; iLevel++)
	{
		printf("Scale %d: blur...", iLevel);

		// Copy to previous
		fioCopy(fioBlurs[1], fioBlurs[0]);
		fioCopy(fioBlursH[1], fioBlursH[0]);

		// How much extra blur required to raise fSigma-blurred image
		// to next bur level
		float fSigmaExtra = fSigma*sqrt(fSigmaFactor*fSigmaFactor - 1.0f);

		// Blur
		int iReturn = gb3d_blur3d(fioBlurs[1], fioTemp, fioBlurs[0], fSigmaExtra, BLUR_PRECISION);
		if (iReturn == 0)
		{
			return -1;
		}

		// Compute entropy H(I) of p(I) at current level (j)
		__computePixelEntropyBinary(fioBlurs[0], fioBlursH[0]);

		FEATUREIO fioBlurMI;
		fioBlurMI = fioImg;
		fioBlurMI.z = 1;
		fioBlurMI.pfVectors = fioGetVector(fioImgOut, 0, 0, iLevel);

		// Compute conditional entropy of p(I|x) by diffusing H(I|x): H(I|x) = sum_i p(x_i) H(I|x_i)
		gb3d_blur3d(fioBlursH[1], fioTemp, fioHCond, fSigmaExtra, BLUR_PRECISION); // This is commented out in entropy features

		fSigma *= fSigmaFactor;

		// Calculate MI		
		fioMultSum(fioBlursH[0], fioHCond, fioBlurMI, -1.0f);

		int ix, iy, iz;
		fioFindMin(fioBlurMI, ix, iy, iz);
		float fMaxPix = fioGetPixel(fioBlurMI, ix, iy, iz);
		printf("Max: %f\t", fMaxPix);

		ppImgOut.InitializeSubImage(
			fioImg.y,
			fioImg.x,
			fioImg.x * sizeof(float), sizeof(float) * 8,
			(unsigned char*)fioBlurMI.pfVectors
		);
		//output_float( ppImgOut, "level.pgm" );

		// Multiply by current scale
		//fioMult( fioBlurMI, fioBlurMI, fSigma );
	}

	return 0;
}



void
Feature3DInfo::ZeroData(
)
{
	for (int i = 0; i < Feature3DInfo::FEATURE_3D_PCS; i++)
	{
		m_pfPC[i] = 0;
	}

	for (int i = 0; i < 3; i++)
	{
		eigs[i] = 0;
		for (int j = 0; j < 3; j++)
		{
			ori[i][j] = 0;
		}
	}

	m_uiInfo = 0;

	x = 0;
	y = 0;
	z = 0;
	scale = 0;
}

void
Feature3DInfo::SimilarityTransform(
	float *pfCenter0,
	float *pfCenter1,
	float *rot01,
	float fScaleDiff
)
{
	float pfP0[3];
	float pfP1[3];

	// Transform point location
	pfP0[0] = x; pfP0[1] = y; pfP0[2] = z;
	similarity_transform_3point(pfP0, pfP1, pfCenter0, pfCenter1, rot01, fScaleDiff);
	x = pfP1[0]; y = pfP1[1]; z = pfP1[2];

	// Transform scale
	scale *= fScaleDiff;

	// Transform orientation/rotation matrix
	float ori0_copy[3][3];
	float rot01_copy[3][3];
	float result_trans[3][3];
	memcpy(&(ori0_copy[0][0]), &(ori[0][0]), sizeof(ori0_copy));
	memcpy(&(rot01_copy[0][0]), &(rot01[0]), sizeof(rot01_copy));

	// ori array vectors are in columns, not rows, so have
	// do perform this multiplication
	mult_3x3_trasposed_matrix<float, float>(rot01_copy, ori0_copy, result_trans);
	transpose_3x3<float, float>(result_trans, ori);
}

void
Feature3DInfo::SimilarityTransform(
	float *pfMat4x4
)
{
	float pfP0[4];
	float pfP1[4];

	// Transform point location
	pfP0[0] = x; pfP0[1] = y; pfP0[2] = z; pfP0[3] = 1;
	mult_4x4_vector(pfMat4x4, pfP0, pfP1);
	x = pfP1[0]; y = pfP1[1]; z = pfP1[2];

	float fScaleSum = 0;
	float pfScale[3];
	float pfRot3x3[3][3];

	for (int i = 0; i < 3; i++)
	{
		// Figure out scaling - should be isotropic
		pfScale[i] = vec3D_mag(&pfMat4x4[4 * i]);
		fScaleSum += pfScale[i];

		// Create rotation matrix - should be able to rescale
		memcpy(&pfRot3x3[i][0], &pfMat4x4[4 * i], 3 * sizeof(float));
		vec3D_norm_3d(&pfRot3x3[i][0]);
	}
	fScaleSum /= 3;

	// Transform scale
	scale *= fScaleSum;

	// Transform orientation/rotation matrix
	float pfOri[3][3];
	float pfOriOut[3][3];
	transpose_3x3<float, float>(ori, pfOri);
	mult_3x3_matrix<float, float>(pfRot3x3, pfOri, pfOriOut);
	transpose_3x3<float, float>(pfOriOut, ori);
}

Feature3DInfo::Feature3DInfo(
)
{
	memset(m_pfPC, 0, sizeof(m_pfPC));
	ZeroData();
}

float
Feature3DInfo::DistSqrPCs(
	const Feature3DInfo &feat3D,
	int iPCs
) const
{
	float fSumSqr = 0;
	for (int i = 0; i < iPCs; i++)
	{
		float fDiff = (m_pfPC[i] - feat3D.m_pfPC[i]);
		fSumSqr += fDiff*fDiff;
	}
	return fSumSqr;
}

void
Feature3DInfo::InitFromFeature3D(
	Feature3D &feat3D
)
{
	m_uiInfo = feat3D.m_uiInfo;
	x = feat3D.x;
	y = feat3D.y;
	z = feat3D.z;
	scale = feat3D.scale;
	memcpy(&(m_pfPC[0]), &(feat3D.m_pfPC[0]), sizeof(m_pfPC));
	memcpy(&(ori[0][0]), &(feat3D.ori[0][0]), sizeof(ori));
	memcpy(&(eigs[0]), &(feat3D.eigs[0]), sizeof(eigs));
	//for( int i = 0; i < 3; i++ )
	//{
	//	for( int j = 0; j < 3; j++ )
	//	{
	//		ori[i][j] = feat3D.ori[i][j];
	//	}
	//	eigs[i] = feat3D.eigs[i];
	//}
}

void
Feature3D::InitFromFeature3D(
	Feature3D &feat3D
)
{
	m_uiInfo = feat3D.m_uiInfo;
	x = feat3D.x;
	y = feat3D.y;
	z = feat3D.z;
	scale = feat3D.scale;
	memcpy(&(m_pfPC[0]), &(feat3D.m_pfPC[0]), sizeof(m_pfPC));
	memcpy(&(ori[0][0]), &(feat3D.ori[0][0]), sizeof(ori));
	memcpy(&(eigs[0]), &(feat3D.eigs[0]), sizeof(eigs));
	memcpy(&(data_zyx[0][0][0]), &(feat3D.data_zyx[0][0][0]), sizeof(data_zyx));
	//for( int i = 0; i < 3; i++ )
	//{
	//	for( int j = 0; j < 3; j++ )
	//	{
	//		ori[i][j] = feat3D.ori[i][j];
	//	}
	//	eigs[i] = feat3D.eigs[i];
	//}
}

int
Feature3D::InitFromFeature3DInfo(
	Feature3DInfo &feat3D
)
{
	m_uiInfo = feat3D.m_uiInfo;
	x = feat3D.x;
	y = feat3D.y;
	z = feat3D.z;
	scale = feat3D.scale;
	memcpy(&(m_pfPC[0]), &(feat3D.m_pfPC[0]), sizeof(m_pfPC));
	memcpy(&(ori[0][0]), &(feat3D.ori[0][0]), sizeof(ori));
	memcpy(&(eigs[0]), &(feat3D.eigs[0]), sizeof(eigs));
	memset(&(data_zyx[0][0][0]), 0, sizeof(data_zyx));
	return 0;
}

int
Feature3DInfo::OutputVisualFeatureInVolume(
	FEATUREIO &fioImg,
	char *pcFileNameBase,
	int bShowFeats,
	int bOneImage
)
{
	char pcFileName[400];
	PpImage ppImgOut;
	FEATUREIO fioTmp;

	if (bOneImage)
	{
		PpImage ppImgOutSub;
		PpImage ppBigOut;

		ppBigOut.Initialize(fioImg.z + fioImg.x, fioImg.y + fioImg.x, (fioImg.y + fioImg.x) * sizeof(float), sizeof(float) * 8);
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		for (int yy = 0; yy < ppBigOut.Rows(); yy++)
		{
			for (int xx = 0; xx < ppBigOut.Cols(); xx++)
			{
				((float*)ppBigOut.ImageRow(yy))[xx] = 0;
			}
		}

		ppImgOutSub.InitializeSubImage(
			fioImg.z, fioImg.y,
			ppBigOut.RowInc(), ppBigOut.BitsPerPixel(),
			(unsigned char*)ppBigOut.ImageRow(0)
		);
		ppImgOut.Initialize(fioImg.z, fioImg.y, fioImg.y * sizeof(float), sizeof(float) * 8);

		int iXSample = x;
		if (iXSample < 0) iXSample = 0;
		if (iXSample >= fioImg.x) iXSample = fioImg.x - 1;
		fioFeatureSliceZY(fioImg, iXSample, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		if (bShowFeats)
			of_paint_white_circle(z, y, 2 * scale, ppImgOut);
		for (int yy = 0; yy < fioImg.z; yy++)
		{
			for (int xx = 0; xx < fioImg.y; xx++)
			{
				((float*)ppImgOutSub.ImageRow(yy))[xx] =
					((float*)ppImgOut.ImageRow(yy))[xx];
			}
		}
		output_float(ppBigOut, pcFileName);

		ppImgOutSub.InitializeSubImage(
			fioImg.y, fioImg.x,
			ppBigOut.RowInc(), ppBigOut.BitsPerPixel(),
			(unsigned char*)ppBigOut.ImageRow(fioImg.z)
		);
		ppImgOut.Initialize(fioImg.y, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);

		int iZSample = z;
		if (iZSample < 0) iZSample = 0;
		if (iZSample >= fioImg.z) iZSample = fioImg.z - 1;
		fioFeatureSliceXY(fioImg, iZSample, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		if (bShowFeats)
			of_paint_white_circle(y, x, 2 * scale, ppImgOut);
		for (int yy = 0; yy < fioImg.y; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				((float*)ppImgOutSub.ImageRow(xx))[yy] =
					((float*)ppImgOut.ImageRow(yy))[fioImg.x - 1 - xx];
			}
		}
		output_float(ppBigOut, pcFileName);

		ppImgOutSub.InitializeSubImage(
			fioImg.z, fioImg.x,
			ppBigOut.RowInc(), ppBigOut.BitsPerPixel(),
			(unsigned char*)((float*)ppBigOut.ImageRow(0) + fioImg.y)
		);
		ppImgOut.Initialize(fioImg.z, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);

		int iYSample = y;
		if (iYSample < 0) iYSample = 0;
		if (iYSample >= fioImg.y) iYSample = fioImg.y - 1;
		fioFeatureSliceZX(fioImg, iYSample, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		if (bShowFeats)
			of_paint_white_circle(z, x, 2 * scale, ppImgOut);
		for (int yy = 0; yy < fioImg.z; yy++)
		{
			for (int xx = 0; xx < fioImg.x; xx++)
			{
				((float*)ppImgOutSub.ImageRow(yy))[xx] =
					((float*)ppImgOut.ImageRow(yy))[xx];
			}
		}
		output_float(ppBigOut, pcFileName);
	}
	else
	{

		ppImgOut.Initialize(fioImg.z, fioImg.y, fioImg.y * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceZY(fioImg, x, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_zy.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		if (bShowFeats)
			of_paint_white_circle(z, y, 2 * scale, ppImgOut);
		output_float(ppImgOut, pcFileName);

		ppImgOut.Initialize(fioImg.y, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceXY(fioImg, z, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_xy.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		if (bShowFeats)
			of_paint_white_circle(y, x, 2 * scale, ppImgOut);
		output_float(ppImgOut, pcFileName);

		ppImgOut.Initialize(fioImg.z, fioImg.x, fioImg.x * sizeof(float), sizeof(float) * 8);
		fioFeatureSliceZX(fioImg, y, 0, (float*)ppImgOut.ImageRow(0));
		fioimgInitReferenceToImage(ppImgOut, fioTmp); fioNormalize(fioTmp, 255.0f);
		sprintf(pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_zx.pgm", pcFileNameBase, (int)x, (int)y, (int)z);
		if (bShowFeats)
			of_paint_white_circle(z, x, 2 * scale, ppImgOut);
		output_float(ppImgOut, pcFileName);
	}

	return 1;
}

int
msComputeNearestNeighbors(
	vector<Feature3D> &vecFeats1,
	vector<Feature3D> &vecFeats2,
	vector<int>		&vecMatchIndex12,
	vector<float>		&vecMatchDist12,
	int iPCs
)
{
	if (iPCs <= 0)
	{
		for (int i = 0; i < vecFeats1.size(); i++)
		{
			float fMinDist = vecFeats1[i].DistSqr(vecFeats2[0]);
			float iMinDistIndex = 0;
			for (int j = 1; j < vecFeats2.size(); j++)
			{
				float fDist = vecFeats1[i].DistSqr(vecFeats2[j]);
				if (fDist < fMinDist || iMinDistIndex == -1)
				{
					fMinDist = fDist;
					iMinDistIndex = j;
				}
			}
			vecMatchIndex12.push_back(iMinDistIndex);
			vecMatchDist12.push_back(fMinDist);
		}
	}
	else
	{
		// Calculate from principal components
		if (iPCs > Feature3DInfo::FEATURE_3D_PCS)
		{
			iPCs = Feature3DInfo::FEATURE_3D_PCS;
		}
		for (int i = 0; i < vecFeats1.size(); i++)
		{
			float fMinDist = vecFeats1[i].DistSqrPCs(vecFeats2[0], iPCs);
			float iMinDistIndex = 0;
			for (int j = 1; j < vecFeats2.size(); j++)
			{
				float fDist = vecFeats1[i].DistSqrPCs(vecFeats2[j], iPCs);
				if (fDist < fMinDist || iMinDistIndex == -1)
				{
					fMinDist = fDist;
					iMinDistIndex = j;
				}
			}
			vecMatchIndex12.push_back(iMinDistIndex);
			vecMatchDist12.push_back(fMinDist);
		}
	}
	return 0;
}

int
msComputeNearestNeighborDistanceRatio(
	vector<Feature3D> &vecFeats1,
	vector<Feature3D> &vecFeats2,
	vector<int>		&vecMatchIndex12,
	vector<float>		&vecMatchDist12,
	int iPCs
)
{
	int iCompatible = 0;

	if (iPCs <= 0)
	{
		for (int i = 0; i < vecFeats1.size(); i++)
		{
			float fMinDist = vecFeats1[i].DistSqr(vecFeats2[0]);
			float iMinDistIndex = 0;
			for (int j = 1; j < vecFeats2.size(); j++)
			{
				float fDist = vecFeats1[i].DistSqr(vecFeats2[j]);
				if (fDist < fMinDist || iMinDistIndex == -1)
				{
					fMinDist = fDist;
					iMinDistIndex = j;
				}
			}
			vecMatchIndex12.push_back(iMinDistIndex);
			vecMatchDist12.push_back(fMinDist);
		}
	}
	else
	{

		// Calculate from principal components
		if (iPCs > Feature3DInfo::FEATURE_3D_PCS)
		{
			iPCs = Feature3DInfo::FEATURE_3D_PCS;
		}

		iPCs = 64;

		for (int i = 0; i < vecFeats1.size(); i++)
		{
			float fMinDist = vecFeats1[i].DistSqrPCs(vecFeats2[0], iPCs);
			//float fMinDist = vecFeats1[i].DistSqr( vecFeats2[0], iPCs );//*vecFeats1[i].DistSqrPCs( vecFeats2[0], 32 );
			int iMinDistIndex = 0;

			float fMinDist2 = vecFeats1[i].DistSqrPCs(vecFeats2[1], iPCs);
			//float fMinDist2 = vecFeats1[i].DistSqr( vecFeats2[1], iPCs );//*vecFeats1[i].DistSqrPCs( vecFeats2[1], 32 );
			int iMinDist2Index = 1;
			if (fMinDist2 < fMinDist)
			{
				float fTemp = fMinDist;
				fMinDist = fMinDist2;
				fMinDist2 = fTemp;
				iMinDistIndex = 1;
				iMinDist2Index = 0;
			}

			for (int j = 2; j < vecFeats2.size(); j++)
			{
				float fDist = vecFeats1[i].DistSqrPCs(vecFeats2[j], iPCs);
				//float fDist = vecFeats1[i].DistSqr( vecFeats2[j], iPCs );//*vecFeats1[i].DistSqrPCs( vecFeats2[j], 32 );
				if (fDist < fMinDist2)
				{
					// 1st and 2nd matches shoud not be compatible

					if (fDist < fMinDist)
					{
						if (1)//!compatible_features( vecFeats2[j], vecFeats2[iMinDistIndex] ) ) // Add this line for dense features
						{
							// Closest feature is not geometrically compatible with first
							// Shuffle down 1st and 2nd nearest distances
							fMinDist2 = fMinDist;
							iMinDist2Index = iMinDistIndex;
							fMinDist = fDist;
							iMinDistIndex = j;
						}
						else
						{
							// Closest feature is geometrically compatible with first
							// Replace only 1st feature, we have found a better instance of the same thing
							fMinDist = fDist;
							iMinDistIndex = j;
						}
					}
					else
					{
						if (1) //!compatible_features( vecFeats2[j], vecFeats2[iMinDistIndex] ) ) // Add this line for dense features
						{
							// Second closest feature is not geometrically compatible with first
							// Shuffle down 2nd nearest distance
							fMinDist2 = fDist;
							iMinDist2Index = j;
						}
						else
						{
							//printf( "worse \n" );
							// Second closest feature is geometrically compatible with first
							// It is a worse instance of the current closest feature, ignore it
						}
					}
				}
			}

			if (compatible_features(vecFeats2[iMinDist2Index], vecFeats2[iMinDistIndex]))
			{
				iCompatible++;
				//fMinDist = 1;
				//fMinDist2 = 10000;
			}

			vecMatchIndex12.push_back(iMinDistIndex);
			//vecMatchDist12.push_back( fMinDist );
			vecMatchDist12.push_back(fMinDist / fMinDist2);
		}
	}
	return 0;
}

//int
//msComputeNearestNeighborDistanceRatio(
//									  vector<Feature3D> &vecFeats1,
//									  vector<Feature3D> &vecFeats2,
//									  vector<int>		&vecMatchIndex12,
//									  vector<float>		&vecMatchDist12,
//									  vector<float>		&vecFeats2Dist ** implement this here - suppose to be an array of distance thresholds...
//									  )
//{
//	int iCompatible = 0;
//	assert( Feature3DInfo::FEATURE_3D_PCS == 64 )
//
//	for( int i = 0; i < vecFeats1.size(); i++ )
//	{
//		float fMinDist = vecFeats1[i].DistSqrPCs( vecFeats2[0], iPCs );
//		//float fMinDist = vecFeats1[i].DistSqr( vecFeats2[0], iPCs );//*vecFeats1[i].DistSqrPCs( vecFeats2[0], 32 );
//		int iMinDistIndex = 0;
//
//		float fMinDist2 = vecFeats1[i].DistSqrPCs( vecFeats2[1], iPCs );
//		//float fMinDist2 = vecFeats1[i].DistSqr( vecFeats2[1], iPCs );//*vecFeats1[i].DistSqrPCs( vecFeats2[1], 32 );
//		int iMinDist2Index = 1;
//		if( fMinDist2 < fMinDist )
//		{
//			float fTemp = fMinDist;
//			fMinDist = fMinDist2;
//			fMinDist2 = fTemp;
//			iMinDistIndex = 1;
//			iMinDist2Index = 0;
//		}
//
//		for( int j = 2; j < vecFeats2.size(); j++ )
//		{
//			float fDist = vecFeats1[i].DistSqrPCs( vecFeats2[j], iPCs );
//			//float fDist = vecFeats1[i].DistSqr( vecFeats2[j], iPCs );//*vecFeats1[i].DistSqrPCs( vecFeats2[j], 32 );
//			if( fDist < fMinDist2 )
//			{
//				// 1st and 2nd matches shoud not be compatible
//
//				if( fDist < fMinDist )
//				{
//					if( 1 )//!compatible_features( vecFeats2[j], vecFeats2[iMinDistIndex] ) ) // Add this line for dense features
//					{
//						// Closest feature is not geometrically compatible with first
//						// Shuffle down 1st and 2nd nearest distances
//						fMinDist2 = fMinDist;
//						iMinDist2Index = iMinDistIndex;
//						fMinDist = fDist;
//						iMinDistIndex = j;
//					}
//					else
//					{
//						// Closest feature is geometrically compatible with first
//						// Replace only 1st feature, we have found a better instance of the same thing
//						fMinDist = fDist;
//						iMinDistIndex = j;
//					}
//				}
//				else
//				{
//					if( 1 ) //!compatible_features( vecFeats2[j], vecFeats2[iMinDistIndex] ) ) // Add this line for dense features
//					{
//						// Second closest feature is not geometrically compatible with first
//						// Shuffle down 2nd nearest distance
//						fMinDist2 = fDist;
//						iMinDist2Index = j;
//					}
//					else
//					{
//						//printf( "worse \n" );
//						// Second closest feature is geometrically compatible with first
//						// It is a worse instance of the current closest feature, ignore it
//					}
//				}
//			}
//		}
//
//		if( compatible_features( vecFeats2[iMinDist2Index], vecFeats2[iMinDistIndex] ) )
//		{
//			iCompatible++;
//			//fMinDist = 1;
//			//fMinDist2 = 10000;
//		}
//
//		vecMatchIndex12.push_back( iMinDistIndex );
//		//vecMatchDist12.push_back( fMinDist );
//		vecMatchDist12.push_back( fMinDist/fMinDist2 );
//	}
//	return 0;
//}


int
msComputeNearestNeighborDistanceRatioTest(
	vector<Feature3D> &vecFeats1,
	vector<Feature3D> &vecFeats2,
	vector<int>		&vecMatchIndex12,
	vector<float>		&vecMatchDist12,
	int iPCs
)
{
	int iCompatible = 0;

	iPCs = 32;
	int iFeats = 64;

	for (int i = 0; i < vecFeats1.size(); i++)
	{
		float fMinDist = vecFeats1[i].DistSqrPCs(vecFeats2[0], iPCs);
		//float fMinDist = vecFeats1[i].DistSqr( vecFeats2[0], iPCs )*vecFeats1[i].DistSqrPCs( vecFeats2[0], 32 );
		int iMinDistIndex = 0;

		float fMinDist2 = vecFeats1[i].DistSqrPCs(vecFeats2[1], iPCs);
		//float fMinDist2 = vecFeats1[i].DistSqr( vecFeats2[1], iPCs )*vecFeats1[i].DistSqrPCs( vecFeats2[1], 32 );
		int iMinDist2Index = 1;
		if (fMinDist2 < fMinDist)
		{
			float fTemp = fMinDist;
			fMinDist = fMinDist2;
			fMinDist2 = fTemp;
			iMinDistIndex = 1;
			iMinDist2Index = 0;
		}

		for (int j = 2; j < vecFeats2.size(); j++)
		{
			float fDist = vecFeats1[i].DistSqrPCs(vecFeats2[j], iPCs);
			//float fDist = vecFeats1[i].DistSqr( vecFeats2[j], iPCs )*vecFeats1[i].DistSqrPCs( vecFeats2[j], 32 );
			if (fDist < fMinDist2)
			{
				// 1st and 2nd matches shoud not be compatible

				if (fDist < fMinDist)
				{
					// Update 1st and 2nd nearest distances
					fMinDist2 = fMinDist;
					iMinDist2Index = iMinDistIndex;
					fMinDist = fDist;
					iMinDistIndex = j;
				}
				else
				{
					// Update 2nd nearest distance
					fMinDist2 = fDist;
					iMinDist2Index = j;
				}
			}
		}

		if (compatible_features(vecFeats2[iMinDist2Index], vecFeats2[iMinDistIndex]))
		{
			iCompatible++;
			//fMinDist = 1;
			//fMinDist2 = 10000;
		}

		vecMatchIndex12.push_back(iMinDistIndex);
		//vecMatchDist12.push_back( fMinDist );
		vecMatchDist12.push_back(fMinDist / fMinDist2);
	}

	for (int i = 0; i < vecFeats1.size(); i++)
	{
		//float fMinDist = vecFeats1[i].DistSqrPCs( vecFeats2[0], iPCs );
		float fMinDist = vecFeats1[i].DistSqr(vecFeats2[0], iFeats);
		int iMinDistIndex = 0;

		//float fMinDist2 = vecFeats1[i].DistSqrPCs( vecFeats2[1], iPCs );
		float fMinDist2 = vecFeats1[i].DistSqr(vecFeats2[1], iFeats);
		int iMinDist2Index = 1;
		if (fMinDist2 < fMinDist)
		{
			float fTemp = fMinDist;
			fMinDist = fMinDist2;
			fMinDist2 = fTemp;
			iMinDistIndex = 1;
			iMinDist2Index = 0;
		}

		for (int j = 2; j < vecFeats2.size(); j++)
		{
			//float fDist = vecFeats1[i].DistSqrPCs( vecFeats2[j], iPCs );
			float fDist = vecFeats1[i].DistSqr(vecFeats2[j], iFeats);
			if (fDist < fMinDist2)
			{
				// 1st and 2nd matches shoud not be compatible

				if (fDist < fMinDist)
				{
					// Update 1st and 2nd nearest distances
					fMinDist2 = fMinDist;
					iMinDist2Index = iMinDistIndex;
					fMinDist = fDist;
					iMinDistIndex = j;
				}
				else
				{
					// Update 2nd nearest distance
					fMinDist2 = fDist;
					iMinDist2Index = j;
				}
			}
		}

		if (iMinDistIndex == vecMatchIndex12[i])
		{
			// 
			//vecMatchDist12[i] = fMinDist;
		}
		else
		{
			// Bump up
			vecMatchDist12[i] *= 1000;
		}

		//vecMatchIndex12.push_back( iMinDistIndex );
		//vecMatchDist12.push_back( fMinDist );
		//vecMatchDist12.push_back( fMinDist/fMinDist2 );
	}

	return 0;
}

static bool pairIntFloatLeastToGreatest(const pair<int, float> &ii1, const pair<int, float> &ii2)
{
	return ii1.second < ii2.second;
}

int
determineMeanLocation(
	vector<Feature3D> &vecFeats,
	float &fX,
	float &fY,
	float &fZ
)
{
	fX = fY = fZ = 0;
	if (vecFeats.size() <= 0)
	{
		return -1;
	}
	for (int i = 0; i < vecFeats.size(); i++)
	{
		fX += vecFeats[i].x;
		fY += vecFeats[i].y;
		fZ += vecFeats[i].z;
	}

	fX /= (float)vecFeats.size();
	fY /= (float)vecFeats.size();
	fZ /= (float)vecFeats.size();
	return 0;

}

int
msComputeNearestNeighbors(
	char *pcFeats1,
	char *pcFeats2,
	char *pcPCs
)
{
	vector<Feature3D> vecFeats1;
	vector<Feature3D> vecFeats2;
	vector<Feature3D> vecPCs;
	msFeature3DVectorInput(vecFeats1, pcFeats1, 500.0f);
	msFeature3DVectorInput(vecFeats2, pcFeats2, 500.0f);
	//msFeature3DVectorInput( vecFeats1, pcFeats1 );
	//msFeature3DVectorInput( vecFeats2, pcFeats2 );
	if (pcPCs)
	{
		msFeature3DVectorInput(vecPCs, pcPCs);
	}

	for (int i = 0; i < vecFeats1.size(); i++)
	{
		//for( int j = 0; j < 5; j++ )
		int iCode = 0;
		//vecFeats1[i].DetermineOrientationCube( iCode, (float*)(&(vecFeats1[i].ori[0][0])) );
		//vecFeats1[i].NormalizeData();
		//vecFeats1[i].ProjectToPCs( vecPCs );
		//vecFeats1[i].CubeDescriptor(iCode);
		//vecFeats1[i].PlanesDescriptor(iCode);
		//vecFeats1[i].OrientationInvariantDescriptor(iCode);
		//     vecFeats1[i].DeterminePartialOrientationCube(iCode);
		//vecFeats1[i].DiffPCs(16);
		//vecFeats1[i].NormalizeDataRankedPCs();
		//for( int j = 0; j < 64; j++ ) vecFeats1[i].m_pfPC[j] = vecFeats1[i].m_pfPC[j] >= 32 ? 1 : 0; // convert to binary, for fun

		//vecFeats1[i].NormalizeDataPositivePCs();
	}
	for (int i = 0; i < vecFeats2.size(); i++)
	{
		int iCode = 0;
		//vecFeats2[i].DetermineOrientationCube( iCode, (float*)(&(vecFeats2[i].ori[0][0])) );
		//vecFeats2[i].NormalizeData();
		//vecFeats2[i].ProjectToPCs( vecPCs );
		//vecFeats2[i].CubeDescriptor(iCode);
		//vecFeats2[i].PlanesDescriptor(iCode);
		//vecFeats2[i].OrientationInvariantDescriptor(iCode);
		//     vecFeats2[i].DeterminePartialOrientationCube(iCode);
		//vecFeats2[i].DiffPCs(16);
		//vecFeats2[i].NormalizeDataRankedPCs();
		//for( int j = 0; j < 64; j++ ) vecFeats2[i].m_pfPC[j] = vecFeats2[i].m_pfPC[j] >= 32 ? 1 : 0; // convert to binary, for fun
		//vecFeats2[i].NormalizeDataPositivePCs();
	}

	//vector<Feature3D> vecFeatPCs;
	//msCalculatePC_SVD( vecFeatsWithData, vecFeatPCs );

	vector<int>		vecMatchIndex12;
	vector<float>	vecMatchDist12;

	//msComputeNearestNeighborDistanceRatioTest( vecFeats1, vecFeats2, vecMatchIndex12, vecMatchDist12, Feature3DInfo::FEATURE_3D_PCS );
	msComputeNearestNeighborDistanceRatio(vecFeats1, vecFeats2, vecMatchIndex12, vecMatchDist12, Feature3DInfo::FEATURE_3D_PCS);
	//msComputeNearestNeighbors( vecFeats1, vecFeats2, vecMatchIndex12, vecMatchDist12, Feature3DInfo::FEATURE_3D_PCS );
	//msComputeNearestNeighbors( vecFeats1, vecFeats2, vecMatchIndex12, vecMatchDist12 );

	//outputIntVector( vecMatchIndex12, "match.txt" );

	FEATUREIO fio1;
	FEATUREIO fio2;

	char pcFileName[300];
	char *pch;

	sprintf(pcFileName, "%s", pcFeats1);
	if (pch = strrchr(pcFileName, '.'))
	{
		*pch = 0;
	}
	fioRead(fio1, pcFileName);
	sprintf(pcFileName, "%s", pcFeats2);
	if (pch = strrchr(pcFileName, '.'))
	{
		*pch = 0;
	}
	fioRead(fio2, pcFileName);

	vector< pair<int, float> > vecBestMatches;
	for (int i = 0; i < vecMatchIndex12.size(); i++)
	{
		vecBestMatches.push_back(pair<int, float>(i, vecMatchDist12[i]));
	}
	sort(vecBestMatches.begin(), vecBestMatches.end(), pairIntFloatLeastToGreatest);

	// Allocate an image to identify correlated orienations
	PpImage ppImgOriCorr;
	ppImgOriCorr.Initialize(11, 11, 11 * sizeof(float), sizeof(float) * 8);
	FEATUREIO fioOriCorr;
	fioimgInitReferenceToImage(ppImgOriCorr, fioOriCorr);

	vector<float> pfPoints0;
	vector<float> pfPoints1;
	vector<float> pfScales0;
	vector<float> pfScales1;
	vector<float> pfOris0;
	vector<float> pfOris1;
	vector<int> piInliers;
	int iMatchCount = 0;
	// Save indices
	vector<int> vecIndex0;
	vector<int> vecIndex1;

	vector< float > vecMatchProb;

	map<float, int> mapModelXtoIndex;

	for (int i = 0; i < vecBestMatches.size() && i < 300; i++)
	{
		int iIndex = vecBestMatches[i].first;
		float fDist = vecBestMatches[i].second;
		//if( fDist > 0.8 )break;

		sprintf(pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_1", i, iIndex, vecMatchIndex12[iIndex]);
		vecFeats1[iIndex].OutputVisualFeatureInVolume(fio1, pcFileName, 1, 1);


		sprintf(pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_2", i, iIndex, vecMatchIndex12[iIndex]);
		vecFeats2[vecMatchIndex12[iIndex]].OutputVisualFeatureInVolume(fio2, pcFileName, 1, 1);

		Feature3DInfo &featModel = vecFeats1[iIndex];
		Feature3DInfo &featInput = vecFeats2[vecMatchIndex12[iIndex]];

		// Keep track of indentical location correspondences ... 
		map<float, int>::iterator itXI = mapModelXtoIndex.find(featInput.x);
		if (itXI != mapModelXtoIndex.end())
		{
			int iPrevModelIndex = itXI->second;
			Feature3DInfo &featInputPrev = vecFeats2[iPrevModelIndex];
			if (
				featInputPrev.x == featInput.x &&
				featInputPrev.y == featInput.y &&
				featInputPrev.z == featInput.z)
			{
				// Match to same location/scale as another feature
				// skip
				continue;
			}
		}
		// Insert into map
		mapModelXtoIndex.insert(pair<float, int>(featInput.x, vecMatchIndex12[iIndex]));



		pfPoints0.push_back(featModel.x);
		pfPoints0.push_back(featModel.y);
		pfPoints0.push_back(featModel.z);

		pfPoints1.push_back(featInput.x);
		pfPoints1.push_back(featInput.y);
		pfPoints1.push_back(featInput.z);

		pfScales0.push_back(featModel.scale);
		pfScales1.push_back(featInput.scale);

		for (int o1 = 0; o1<3; o1++)
		{
			for (int o2 = 0; o2<3; o2++)
			{
				pfOris0.push_back(featModel.ori[o1][o2]);
				pfOris1.push_back(featInput.ori[o1][o2]);
			}
		}

		vecIndex0.push_back(iIndex);
		vecIndex1.push_back(vecMatchIndex12[iIndex]);

		// Save prior probability of this feature
		float fFeatsInThreshold = 1;//FeaturesWithinThreshold( vecOrderToFeat[ iIndex ] );
		vecMatchProb.push_back(fFeatsInThreshold);

		iMatchCount++;
	}

	// Now determine transform

	float pfModelCenter[3];
	determineMeanLocation(vecFeats1, pfModelCenter[0], pfModelCenter[1], pfModelCenter[2]);
	//pfModelCenter[0] = m_piModelImageSizeXYZ[0]/2.0f;
	//pfModelCenter[1] = m_piModelImageSizeXYZ[1]/2.0f;
	//pfModelCenter[2] = m_piModelImageSizeXYZ[2]/2.0f;

	// Output parameters, the result of determine_similarity_transform_ransac()
	float rot[3 * 3];
	float pfModelCenter2[3];
	float fScaleDiff = 1;

	// Save inliers

	int iInliers = -1;
	if (iMatchCount >= 3)
	{
		piInliers.resize(iMatchCount, 0);
		iInliers =
			determine_similarity_transform_hough(
				&(pfPoints0[0]), &(pfPoints1[0]),
				&(pfScales0[0]), &(pfScales1[0]),
				&(pfOris0[0]), &(pfOris1[0]),
				&(vecMatchProb[0]),
				iMatchCount, 1000, pfModelCenter,
				pfModelCenter2, rot, fScaleDiff, &(piInliers[0])
			);

		// Output image is size of fio2
		FEATUREIO fioOut = fio1;
		fioAllocate(fioOut);
		//similarity_transform_image( fioOut, fio2, pfModelCenter, pfModelCenter2, rot, fScaleDiff );
		fioWrite(fioOut, "_transformed");

		vector<float> pfPointsIn0;
		vector<float> pfPointsIn1;
		vector<float> pfScalesIn0;
		vector<float> pfScalesIn1;

		vector<float> vecIndexError;
		vecIndexError.resize(64, 0);

		// Output inliers
		for (int i = 0; i < iMatchCount; i++)
		{
			//piInliers[i]=1;
			if (piInliers[i])
			{
				sprintf(pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_1", i, vecIndex0[i], vecIndex1[i]);
				vecFeats1[vecIndex0[i]].OutputVisualFeatureInVolume(fio1, pcFileName, 1, 1);


				sprintf(pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_2", i, vecIndex0[i], vecIndex1[i]);
				vecFeats2[vecIndex1[i]].OutputVisualFeatureInVolume(fio2, pcFileName, 1, 1);

				for (int j = 0; j < 64; j++)
				{
					int iIndex = (int)(vecFeats1[vecIndex0[i]].m_pfPC[j]);
					int iError = (int)fabs(vecFeats1[vecIndex0[i]].m_pfPC[j] - vecFeats2[vecIndex1[i]].m_pfPC[j]);

					vecIndexError[iIndex] += iError;
				}

				// Save inliers for later
				pfPointsIn0.push_back(pfPoints0[i * 3 + 0]);
				pfPointsIn0.push_back(pfPoints0[i * 3 + 1]);
				pfPointsIn0.push_back(pfPoints0[i * 3 + 2]);

				pfPointsIn1.push_back(pfPoints1[i * 3 + 0]);
				pfPointsIn1.push_back(pfPoints1[i * 3 + 1]);
				pfPointsIn1.push_back(pfPoints1[i * 3 + 2]);

				pfScalesIn0.push_back(pfScales0[i]);
				pfScalesIn1.push_back(pfScales1[i]);

			}
		}

		FILE *outfile2;
		outfile2 = fopen("index_error.txt", "wt");
		for (int j = 0; j < 64; j++)
		{
			fprintf(outfile2, "%d\t%f\n", j, vecIndexError[j]);
		}
		fclose(outfile2);

		if (iInliers >= 3)
		{
			float rotIn[3 * 3];
			float pfModelCenter2In[3];
			float fScaleDiffIn = 1;
			int iInliersRefined = refine_similarity_transform_ransac(
				&(pfPointsIn0[0]), &(pfPointsIn1[0]),
				&(pfScalesIn0[0]), &(pfScalesIn1[0]),
				&(vecMatchProb[0]),
				iInliers, 500, pfModelCenter,
				pfModelCenter2In, rotIn, fScaleDiffIn, &(piInliers[0])
			);

			similarity_transform_image(fioOut, fio2, pfModelCenter, pfModelCenter2In, rotIn, fScaleDiffIn);

			sprintf(pcFileName, "%s.reg", pcFeats2);
			fioWrite(fioOut, pcFileName);

			// Invert similarity transform
			similarity_transform_invert(pfModelCenter, pfModelCenter2In, rotIn, fScaleDiffIn);

			vector<Feature3D> vecFeats2XFM;
			msFeature3DVectorInput(vecFeats2XFM, pcFeats2);
			for (int i = 0; i < vecFeats2XFM.size(); i++)
			{
				vecFeats2XFM[i].SimilarityTransform(pfModelCenter, pfModelCenter2In, rotIn, fScaleDiffIn);
			}

			sprintf(pcFileName, "%s.reg.k3d", pcFeats2);
			msFeature3DVectorOutput(vecFeats2XFM, pcFileName, 0);

		}

		fioDelete(fioOut);
	}


	//for( int i = 0; i < vecBestMatches.size(); i++ )
	////for( int i = vecBestMatches.size()-1; i >= 0; i-- ) // biggest to smallest
	//{
	//	int iIndex = vecBestMatches[i].first;
	//	float fDist = vecBestMatches[i].second;

	//	Feature3D &feat1 = vecFeats1[iIndex];
	//	Feature3D &feat2 = vecFeats2[vecMatchIndex12[iIndex] ];

	//	float fDot0 = vec3D_dot_3d( &feat1.ori[0][0], &feat2.ori[0][0] );
	//	float fDot1 = vec3D_dot_3d( &feat1.ori[1][0], &feat2.ori[1][0] );
	//	float fDot2 = vec3D_dot_3d( &feat1.ori[2][0], &feat2.ori[2][0] );

	//	float pfx1[3];
	//	float pfx2[3];
	//	pfx1[0] = feat1.x;pfx1[1] = feat1.y;pfx1[2] = feat1.z;
	//	pfx2[0] = feat2.x;pfx2[1] = feat2.y;pfx2[2] = feat2.z;
	//	float fDistFeats = vec3D_dist_3d(pfx1,pfx2);

	//	//if( fDot > 0.8f )
	//	//if( fDistFeats < 30.0f )
	//	{
	//		sprintf( pcFileName, "feat%4.4d_1", i );
	//		vecFeats1[iIndex].OutputVisualFeatureInVolume( fio1, pcFileName, 1, 1 );
	//		//vecFeats1[iIndex].OutputVisualFeature( pcFileName );
	//		sprintf( pcFileName, "feat%4.4d_2", i );
	//		vecFeats2[ vecMatchIndex12[iIndex] ].OutputVisualFeatureInVolume( fio2, pcFileName, 1, 1 );
	//		//vecFeats2[ vecMatchIndex12[iIndex] ].OutputVisualFeature( pcFileName );

	//		//
	//		// Testing: compute correlation array
	//		// See how many corresponding features have some similar primary orientation component
	//		//
	//		//fioSet( fioOriCorr, 0 );
	//		//for( int ii = 0; ii < 11; ii++ )
	//		//{
	//		//	float *po1 = &(feat1.data_zyx[0][ii][0]);
	//		//	float *po12 = &(feat1.data_zyx[0][ii][3]);
	//		//	if( vec3D_mag( po1 ) < 0.9f )
	//		//	{
	//		//		// No more orientation vectors
	//		//		break;
	//		//	}
	//		//	for( int jj =0; jj < 11; jj++ )
	//		//	{
	//		//		float *po2 = &(feat2.data_zyx[0][jj][0]);
	//		//		float *po22 = &(feat2.data_zyx[0][jj][3]);
	//		//		if( vec3D_mag( po2 ) < 0.9f )
	//		//		{
	//		//			// No more orientation vectors
	//		//			break;
	//		//		}

	//		//		// Compute dot product
	//		//		float fDot = vec3D_dot_3d( po1, po2 );
	//		//		float fDot2 = vec3D_dot_3d( po12, po22 );
	//		//		float *pfPix = fioGetVector( fioOriCorr, ii, jj );
	//		//		*pfPix = fDot*fDot2;
	//		//	}
	//		//}
	//		//int mx, my, mz;
	//		//float fMaxOriCorr;
	//		//fioFindMax( fioOriCorr, mx, my, mz, fMaxOriCorr );

	//		//float *pfPixNull = fioGetVector( fioOriCorr, 10, 10 );
	//		//*pfPixNull = 1;
	//		sprintf( pcFileName, "match%3.3d_ori.pgm", i );
	//		//output_float( ppImgOriCorr, pcFileName ); 
	//	}

	//	int iCode;
	//	iCode = iIndex*10+0;
	//	//vecFeats1[iIndex].DeterminePartialOrientationCube(iCode);
	//	iCode = iIndex*10+1;
	//	//vecFeats2[vecMatchIndex12[iIndex]].DeterminePartialOrientationCube(iCode);
	//}

	printf("Inliers: %d\n", iInliers);

	fioDelete(fio1);
	fioDelete(fio2);

	return 0;
}

int
msComputeNNTransform(
	vector<Feature3D> &vecFeats1,
	vector<Feature3D> &vecFeats2,

	char *pcFeats1,
	char *pcFeats2
)
{
	vector<int>		vecMatchIndex12;
	vector<float>	vecMatchDist12;

	//msComputeNearestNeighborDistanceRatioTest( vecFeats1, vecFeats2, vecMatchIndex12, vecMatchDist12, Feature3DInfo::FEATURE_3D_PCS );
	msComputeNearestNeighborDistanceRatio(vecFeats1, vecFeats2, vecMatchIndex12, vecMatchDist12, Feature3DInfo::FEATURE_3D_PCS);
	//msComputeNearestNeighbors( vecFeats1, vecFeats2, vecMatchIndex12, vecMatchDist12, Feature3DInfo::FEATURE_3D_PCS );
	//msComputeNearestNeighbors( vecFeats1, vecFeats2, vecMatchIndex12, vecMatchDist12 );

	//outputIntVector( vecMatchIndex12, "match.txt" );

	FEATUREIO fio1;
	FEATUREIO fio2;

	char pcFileName[300];
	char *pch;

	sprintf(pcFileName, "%s", pcFeats1);
	if (pch = strrchr(pcFileName, '.'))
	{
		*pch = 0;
	}
	fioRead(fio1, pcFileName);
	sprintf(pcFileName, "%s", pcFeats2);
	if (pch = strrchr(pcFileName, '.'))
	{
		*pch = 0;
	}
	fioRead(fio2, pcFileName);

	vector< pair<int, float> > vecBestMatches;
	for (int i = 0; i < vecMatchIndex12.size(); i++)
	{
		vecBestMatches.push_back(pair<int, float>(i, vecMatchDist12[i]));
	}
	sort(vecBestMatches.begin(), vecBestMatches.end(), pairIntFloatLeastToGreatest);

	// Allocate an image to identify correlated orienations
	PpImage ppImgOriCorr;
	ppImgOriCorr.Initialize(11, 11, 11 * sizeof(float), sizeof(float) * 8);
	FEATUREIO fioOriCorr;
	fioimgInitReferenceToImage(ppImgOriCorr, fioOriCorr);

	vector<float> pfPoints0;
	vector<float> pfPoints1;
	vector<float> pfScales0;
	vector<float> pfScales1;
	vector<float> pfOris0;
	vector<float> pfOris1;
	vector<int> piInliers;
	int iMatchCount = 0;
	// Save indices
	vector<int> vecIndex0;
	vector<int> vecIndex1;

	vector< float > vecMatchProb;

	map<float, int> mapModelXtoIndex;

	for (int i = 0; i < vecBestMatches.size() && i < 1000; i++)
	{
		int iIndex = vecBestMatches[i].first;
		float fDist = vecBestMatches[i].second;
		//if( fDist > 0.8 )break;

		Feature3DInfo &featModel = vecFeats1[iIndex];
		Feature3DInfo &featInput = vecFeats2[vecMatchIndex12[iIndex]];

		// Keep track of indentical location correspondences ... 
		map<float, int>::iterator itXI = mapModelXtoIndex.find(featInput.x);
		if (itXI != mapModelXtoIndex.end())
		{
			int iPrevModelIndex = itXI->second;
			Feature3DInfo &featInputPrev = vecFeats2[iPrevModelIndex];
			if (
				featInputPrev.x == featInput.x &&
				featInputPrev.y == featInput.y &&
				featInputPrev.z == featInput.z)
			{
				// Match to same location/scale as another feature
				// skip
				continue;
			}
		}
		// Insert into map
		mapModelXtoIndex.insert(pair<float, int>(featInput.x, vecMatchIndex12[iIndex]));



		pfPoints0.push_back(featModel.x);
		pfPoints0.push_back(featModel.y);
		pfPoints0.push_back(featModel.z);

		pfPoints1.push_back(featInput.x);
		pfPoints1.push_back(featInput.y);
		pfPoints1.push_back(featInput.z);

		pfScales0.push_back(featModel.scale);
		pfScales1.push_back(featInput.scale);

		for (int o1 = 0; o1<3; o1++)
		{
			for (int o2 = 0; o2<3; o2++)
			{
				pfOris0.push_back(featModel.ori[o1][o2]);
				pfOris1.push_back(featInput.ori[o1][o2]);
			}
		}

		vecIndex0.push_back(iIndex);
		vecIndex1.push_back(vecMatchIndex12[iIndex]);

		// Save prior probability of this feature
		float fFeatsInThreshold = 1;//FeaturesWithinThreshold( vecOrderToFeat[ iIndex ] );
		vecMatchProb.push_back(fFeatsInThreshold);

		iMatchCount++;
	}

	// Now determine transform

	float pfModelCenter[3];
	determineMeanLocation(vecFeats1, pfModelCenter[0], pfModelCenter[1], pfModelCenter[2]);
	//pfModelCenter[0] = m_piModelImageSizeXYZ[0]/2.0f;
	//pfModelCenter[1] = m_piModelImageSizeXYZ[1]/2.0f;
	//pfModelCenter[2] = m_piModelImageSizeXYZ[2]/2.0f;

	// Output parameters, the result of determine_similarity_transform_ransac()
	float rot[3 * 3];
	float pfModelCenter2[3];
	float fScaleDiff = 1;

	// Save inliers

	int iInliers = -1;
	if (iMatchCount >= 3)
	{
		piInliers.resize(iMatchCount, 0);
		iInliers =
			determine_similarity_transform_hough(
				&(pfPoints0[0]), &(pfPoints1[0]),
				&(pfScales0[0]), &(pfScales1[0]),
				&(pfOris0[0]), &(pfOris1[0]),
				&(vecMatchProb[0]),
				iMatchCount, 1000, pfModelCenter,
				pfModelCenter2, rot, fScaleDiff, &(piInliers[0])
			);

		// Output inliers
		for (int i = 0; i < iMatchCount; i++)
		{
			//piInliers[i]=1;
			if (piInliers[i])
			{
				sprintf(pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_1", i, vecIndex0[i], vecIndex1[i]);
				vecFeats1[vecIndex0[i]].OutputVisualFeatureInVolume(fio1, pcFileName, 1, 1);


				sprintf(pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_2", i, vecIndex0[i], vecIndex1[i]);
				vecFeats2[vecIndex1[i]].OutputVisualFeatureInVolume(fio2, pcFileName, 1, 1);
			}
		}

	}

	printf("Inliers: %d\n", iInliers);

	fioDelete(fio1);
	fioDelete(fio2);

	return 0;
}


void
msCalculatePC_SVD(
	vector<Feature3D> &vecFeats,
	vector<Feature3D> &vecFeatPCs
)
{
#define IMAGE_SIZE Feature3DData::FEATURE_3D_DIM*Feature3DData::FEATURE_3D_DIM*Feature3DData::FEATURE_3D_DIM
	double *pdCov = new double[IMAGE_SIZE*IMAGE_SIZE];
	double *pdEig = new double[IMAGE_SIZE];
	double *pdVec = new double[IMAGE_SIZE*IMAGE_SIZE];

	double **pdCovPt = new double*[IMAGE_SIZE];
	double **pdVecPt = new double*[IMAGE_SIZE];
	for (int i = 0; i < IMAGE_SIZE; i++)
	{
		pdCovPt[i] = &pdCov[i*IMAGE_SIZE];
		pdVecPt[i] = &pdVec[i*IMAGE_SIZE];
		for (int j = 0; j < IMAGE_SIZE; j++)
		{
			pdCovPt[i][j] = 0;
			pdVecPt[i][j] = 0;
		}
	}

	// Compute mean
	double *pdMean = new double[IMAGE_SIZE];
	for (int i = 0; i < IMAGE_SIZE; i++)
	{
		pdMean[i] = 0;
	}
	for (int iFeat = 0; iFeat < vecFeats.size(); iFeat++)
	{
		for (int i = 0; i < IMAGE_SIZE; i++)
		{
			pdMean[i] += vecFeats[iFeat].data_zyx[0][0][i];
		}
	}
	for (int i = 0; i < IMAGE_SIZE; i++)
	{
		pdMean[i] /= (float)vecFeats.size();
	}

	// Accumulate covariance matrix
	int iSamples = 0;
	for (int iFeat = 0; iFeat < 2000; iFeat++)
		//for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	{
		Feature3D &feat = vecFeats[iFeat];
		for (int i = 0; i < IMAGE_SIZE; i++)
		{
			feat.data_zyx[0][0][i] -= pdMean[i];
		}
		for (int i = 0; i < IMAGE_SIZE; i++)
		{
			for (int j = 0; j < IMAGE_SIZE; j++)
			{
				pdCovPt[i][j] += feat.data_zyx[0][0][i] * feat.data_zyx[0][0][j];
				//pdCovPt[i][j] += (feat.data_zyx[0][0][i]-pdMean[i])*(feat.data_zyx[0][0][j]-pdMean[j]);
			}
		}
		if (iFeat % 1000 == 0)
		{
			printf("Feature: %d of %d\n", iFeat, vecFeats.size());
		}
		iSamples++;
	}

	// Normalize number of samples
	for (int i = 0; i < IMAGE_SIZE; i++)
	{
		for (int j = 0; j < IMAGE_SIZE; j++)
		{
			pdCovPt[i][j] /= (double)iSamples;
		}
	}

	// Determine principal components
	svdcmp_iterations(pdCovPt, IMAGE_SIZE, IMAGE_SIZE, pdEig, pdVecPt, 1000);

	reorder_descending(IMAGE_SIZE, IMAGE_SIZE, pdEig, pdVecPt);

	// Print out 1st 20 components
	for (int iFeat = 0; iFeat < 50; iFeat++)
	{
		Feature3D feat;
		feat.x = feat.y = feat.z = feat.scale = 0;
		for (int i = 0; i < IMAGE_SIZE; i++)
		{
			feat.data_zyx[0][0][i] = pdVecPt[i][iFeat];
		}

		// Save eigen value
		feat.eigs[0] = pdEig[iFeat];

		vecFeatPCs.push_back(feat);

		char pcFName[400];
		sprintf(pcFName, "pc%3.3d_%5.5f.pgm", iFeat, pdEig[iFeat]);
		feat.OutputVisualFeature(pcFName);
	}

	delete[] pdMean;
	delete[] pdCov;
	delete[] pdEig;
	delete[] pdVec;
	delete[] pdCovPt;
	delete[] pdVecPt;
}

//
// determine_rotation_3pt()
//
// Determine rotation matrix between 3 pairs of corresponding points.
//
int
determine_rotation_3point(
	float *pfP01, float *pfP02, float *pfP03,
	float *pfP11, float *pfP12, float *pfP13,
	float *rot
)
{
	float pfV0_12[3];
	float pfV0_13[3];
	float pfV0_nm[3];

	float pfV1_12[3];
	float pfV1_13[3];
	float pfV1_nm[3];

	// Subtract point 1 to convert to vectors
	vec3D_diff_3d(pfP01, pfP02, pfV0_12);
	vec3D_diff_3d(pfP01, pfP03, pfV0_13);
	vec3D_diff_3d(pfP11, pfP12, pfV1_12);
	vec3D_diff_3d(pfP11, pfP13, pfV1_13);

	// Normalize vectors
	vec3D_norm_3d(pfV0_12);
	vec3D_norm_3d(pfV0_13);
	vec3D_norm_3d(pfV1_12);
	vec3D_norm_3d(pfV1_13);

	// Cross product between 2 vectors to get normal 
	vec3D_cross_3d(pfV0_12, pfV0_13, pfV0_nm);
	vec3D_cross_3d(pfV1_12, pfV1_13, pfV1_nm);
	vec3D_norm_3d(pfV0_nm);
	vec3D_norm_3d(pfV1_nm);

	// Cross product between 1st vectors and normal to get orthogonal 3rd vector
	vec3D_cross_3d(pfV0_nm, pfV0_12, pfV0_13);
	vec3D_cross_3d(pfV1_nm, pfV1_12, pfV1_13);
	vec3D_norm_3d(pfV0_13);
	vec3D_norm_3d(pfV1_13);

	// Generate euler rotation
	vec3D_euler_rotation(
		pfV0_12, pfV0_13, pfV0_nm,
		pfV1_12, pfV1_13, pfV1_nm,
		rot);

	//float ori_here[3][3];
	//memcpy( &(ori_here[0][0]), ori, sizeof(float)*3*3 );

	//// Rotate template
	//for( int zz = 0; zz < fioImg.z; zz++ )
	//{
	//	for( int yy = 0; yy < fioImg.y; yy++ )
	//	{
	//		for( int xx = 0; xx < fioImg.x; xx++ )
	//		{
	//			float pf1[3] = {xx-fRadius,yy-fRadius,zz-fRadius};
	//			float pf2[3];

	//			mult_3x3<float,float>( ori_here, pf1, pf2 );

	//			// Add center offset
	//			pf2[0] += fRadius;
	//			pf2[1] += fRadius;
	//			pf2[2] += fRadius;

	//			float fPix = fioGetPixelTrilinearInterp( fioImg, pf2[0], pf2[1], pf2[2] );
	//			fT0.data_zyx[zz][yy][xx] = fPix;
	//		}
	//	}
	//}
	return 1;
}

//
// determine_rotation_3point()
//
// Determines a 3D rotation matrix from 3 points to the cartesian frame.
// The first point pfP01 is at the origin (0,0,0)=(x,y,z)
// Vector pfP01->pfP02 defines the x-axis (1,0,0)=(x,y,z)
// The cross product of vectors pfP01->pfP02 and pfP01->pfP03 (pfV0_nm) defines the z-axis (0,0,1)=(x,y,z)
// The cross product of vectors pfP01->pfP02 and pfV0_nm definex the y-axis (0,1,0)=(x,y,z)
//
int
determine_rotation_3point(
	float *pfP01, float *pfP02, float *pfP03, // points in image 1 (3d)
	float *rot // rotation matrix  (3x3)
)
{
	float pfV0_12[3];
	float pfV0_13[3];
	float pfV0_nm[3];

	// Subtract point 1 to convert to vectors
	vec3D_diff_3d(pfP01, pfP02, pfV0_12);
	vec3D_diff_3d(pfP01, pfP03, pfV0_13);

	// Normalize vectors
	vec3D_norm_3d(pfV0_12);
	vec3D_norm_3d(pfV0_13);

	// Cross product between 2 vectors to get normal 
	vec3D_cross_3d(pfV0_12, pfV0_13, pfV0_nm);
	vec3D_norm_3d(pfV0_nm);

	// Cross product between 1st vectors and normal to get orthogonal 3rd vector
	vec3D_cross_3d(pfV0_nm, pfV0_12, pfV0_13);
	vec3D_norm_3d(pfV0_13);

	rot[0] = pfV0_12[0];
	rot[1] = pfV0_12[1];
	rot[2] = pfV0_12[2];

	rot[3] = pfV0_13[0];
	rot[4] = pfV0_13[1];
	rot[5] = pfV0_13[2];

	rot[6] = pfV0_nm[0];
	rot[7] = pfV0_nm[1];
	rot[8] = pfV0_nm[2];

	return 1;
}

//
// determine_similarity_transform_3point()
//
// Determines a 3 point similarity transform in 3D.
//
// Inputs:
//    Corresponding points
//		float *pfP01, float *pfP02, float *pfP03,
//		float *pfP11, float *pfP12, float *pfP13,
//
// Outputs
//		rot0: rotation matrix, about the first pair of corresponding points pfP01<->pfP11.
//      fScaleDiff: scale change.
//		(rot1: used internally)
//
int
determine_similarity_transform_3point(
	float *pfP01, float *pfP02, float *pfP03,
	float *pfP11, float *pfP12, float *pfP13,

	// Output
	// Rotation about point pfP01/pfP11
	float *rot0,
	float *rot1,

	// Scale change (magnification) from image 1 to image 2
	float &fScaleDiff
)
{
	// Points cannot be equal or colinear

	float fDist012 = vec3D_dist_3d(pfP01, pfP02);
	float fDist013 = vec3D_dist_3d(pfP01, pfP03);
	float fDist023 = vec3D_dist_3d(pfP02, pfP03);
	float fDist112 = vec3D_dist_3d(pfP11, pfP12);
	float fDist113 = vec3D_dist_3d(pfP11, pfP13);
	float fDist123 = vec3D_dist_3d(pfP12, pfP13);

	if (
		fDist012 == 0 ||
		fDist013 == 0 ||
		fDist023 == 0 ||
		fDist112 == 0 ||
		fDist113 == 0 ||
		fDist123 == 0)
	{
		// Points have to be distinct
		return -1;
	}

	// Scale change calculated from correspondences 1 & 2
	// Calculate geometric mean
	//fScaleDiff =
	//	log( fDist112/fDist012 ) +
	//	log( fDist113/fDist013 ) +
	//	log( fDist123/fDist023 );
	//fScaleDiff /= 3.0f;
	//fScaleDiff = exp( fScaleDiff );

	//	fScaleDiff = fDist112/fDist012;
	fScaleDiff = (fDist112 + fDist113 + fDist123) / (fDist012 + fDist013 + fDist023);

	// Determine rotation about point pfP01/pfP11

	int iReturn;

	iReturn = determine_rotation_3point(pfP01, pfP02, pfP03, rot0);
	iReturn = determine_rotation_3point(pfP11, pfP12, pfP13, rot1);

	// Create 
	float ori0[3][3];
	memcpy(&(ori0[0][0]), rot0, sizeof(float) * 3 * 3);
	float ori1[3][3];
	memcpy(&(ori1[0][0]), rot1, sizeof(float) * 3 * 3);

	float ori1trans[3][3];
	transpose_3x3<float, float>(ori1, ori1trans);

	float rot_one[3][3];
	mult_3x3_matrix<float, float>(ori1trans, ori0, rot_one);

	memcpy(rot0, &(rot_one[0][0]), sizeof(float) * 3 * 3);

	return iReturn;
}

//
// similarity_transform_invert()
//
// Invert a similarity transform.
//
void
similarity_transform_invert(
	float *pfCenter0,
	float *pfCenter1,
	float *rot01,
	float &fScaleDiff
)
{
	float pfP0[3];

	// Exchange centroids
	memcpy(pfP0, pfCenter0, sizeof(pfP0));
	memcpy(pfCenter0, pfCenter1, sizeof(pfP0));
	memcpy(pfCenter1, pfP0, sizeof(pfP0));

	// Invert scale
	fScaleDiff = 1.0f / fScaleDiff;

	// Transform orientation/rotation matrix
	float rot01_copy[3][3];
	float rot01_trans[3][3];
	memcpy(&(rot01_copy[0][0]), &(rot01[0]), sizeof(rot01_copy));
	transpose_3x3<float, float>(rot01_copy, rot01_trans);
	memcpy(&(rot01[0]), &(rot01_trans[0][0]), sizeof(rot01_copy));
}


//
// similarity_transform_3point()
//
// Transform a point pfP0 from image 1 to a point pfP1 in image 2
// via similarity transform.
//
// This function was experimental and is now not used.
//
//int
//similarity_transform_3point(
//							float *pfP0,
//							float *pfP1,
//
//							float *pfCenter0,
//							float *pfCenter1,
//						float *rot0,
//						float *rot1,
//					   float fScaleDiff
//					   )
//{
//	// Subtract origin from point
//	float pfP0Diff[3];
//	vec3D_diff_3d( pfCenter0, pfP0, pfP0Diff );
//
//	{
//		// This code here works
//		float ori_here[3][3];
//		memcpy( &(ori_here[0][0]), rot0, sizeof(float)*3*3 );
//
//		// Project onto axis
//		float pfP0new[3];
//		mult_3x3<float,float>( ori_here, pfP0Diff, pfP0new );
//
//		// Sum up components in new axis
//		pfP1[0] = pfP0new[0]*rot1[0] + pfP0new[1]*rot1[3] + pfP0new[2]*rot1[6];
//		pfP1[1] = pfP0new[0]*rot1[1] + pfP0new[1]*rot1[4] + pfP0new[2]*rot1[7];
//		pfP1[2] = pfP0new[0]*rot1[2] + pfP0new[1]*rot1[5] + pfP0new[2]*rot1[8];
//	}
//
//	{
//		// Try to create 1 nice rotation matrix
//
//		float ori0[3][3];
//		memcpy( &(ori0[0][0]), rot0, sizeof(float)*3*3 );
//		float ori1[3][3];
//		memcpy( &(ori1[0][0]), rot1, sizeof(float)*3*3 );
//
//		float ori1trans[3][3];
//		transpose_3x3<float,float>( ori1, ori1trans );
//
//		float rot_one[3][3];
//		mult_3x3_matrix<float,float>( ori1trans, ori0, rot_one );
//
//		// Project onto axis
//		float pfP0new[3];
//		mult_3x3<float,float>( rot_one, pfP0Diff, pfP1 );
//	}
//
//	// Scale
//	pfP1[0] *= fScaleDiff;
//	pfP1[1] *= fScaleDiff;
//	pfP1[2] *= fScaleDiff;
//
//	// Add to origin
//	vec3D_summ_3d( pfP1, pfCenter1, pfP1 );
//
//	return 0;
//}

//
// similarity_transform_3point()
//
// Transform a point pfP0 from image 1 to a point pfP1 in image 2
// via similarity transform.
//
int
similarity_transform_3point(
	float *pfP0,
	float *pfP1,

	float *pfCenter0,
	float *pfCenter1,
	float *rot01,
	float fScaleDiff
)
{
	// Subtract origin from point
	float pfP0Diff[3];
	vec3D_diff_3d(pfCenter0, pfP0, pfP0Diff);

	// Rotate from image 0 to image 1
	float rot_one[3][3];
	memcpy(&(rot_one[0][0]), rot01, sizeof(float) * 3 * 3);
	mult_3x3<float, float>(rot_one, pfP0Diff, pfP1);

	// Scale
	pfP1[0] *= fScaleDiff;
	pfP1[1] *= fScaleDiff;
	pfP1[2] *= fScaleDiff;

	// Add to origin
	vec3D_summ_3d(pfP1, pfCenter1, pfP1);

	return 0;
}

int
similarity_transform_image(
	FEATUREIO &fio0,
	FEATUREIO &fio1,
	float *pfCenter0,
	float *pfCenter1,
	float *rot,
	float fScaleDiff,
	float fDefaultPixelValue
)
{
	for (int zz = 0; zz < fio0.z; zz++)
	{
		for (int yy = 0; yy < fio0.y; yy++)
		{
			for (int xx = 0; xx < fio0.x; xx++)
			{
				float pfP0[3];
				pfP0[0] = xx;
				pfP0[1] = yy;
				pfP0[2] = zz;

				float pfP1[3];

				// Determine location of point P1 in fio1
				similarity_transform_3point(
					pfP0, pfP1,
					pfCenter0, pfCenter1,
					rot, fScaleDiff
				);

				float fPix1 = fDefaultPixelValue;
				if (pfP1[0] >= 0 && pfP1[0] <= fio1.x
					&& pfP1[1] >= 0 && pfP1[1] <= fio1.y
					&& pfP1[2] >= 0 && pfP1[2] <= fio1.z)
				{
					// Get pixel - trilinear interpolation requires adding 0.5 apparently
					fPix1 = fioGetPixelTrilinearInterp(
						fio1,
						pfP1[0] + 0.5f,
						pfP1[1] + 0.5f,
						pfP1[2] + 0.5f);
				}

				float *pfVec0 = fioGetVector(fio0, xx, yy, zz);
				*pfVec0 = fPix1;
			}
		}
	}
	return 1;
}

//
// similarity_transform_feature_list()
//
// Transform points in image 1/P1 to image 0/P0.
//
int
similarity_transform_feature_list(
	float *pfCenter0,
	float *pfCenter1,
	float *rot,
	float fScaleDiff,
	float *pf0, // Points in image 1
	float *pf1, // Points in image 2
	float *pfs0, // Scale for points in image 1
	float *pfs1, // Scale for points in image 2
	int iCount
)
{
	float pfC0[3];
	float pfC1[3];
	float rotInv[9];
	float fScaleDiffInv;
	memcpy(pfC0, pfCenter0, sizeof(pfC0));
	memcpy(pfC1, pfCenter1, sizeof(pfC1));
	memcpy(rotInv, rot, sizeof(rotInv));
	fScaleDiffInv = fScaleDiff;
	similarity_transform_invert(pfC0, pfC1, rotInv, fScaleDiffInv);

	for (int i = 0; i < iCount; i++)
	{
		float *pfP1 = pf1 + i * 3;
		float *pfP0 = pf0 + i * 3;

		// Determine location of point P1 in space of P0
		similarity_transform_3point(
			pfP1, pfP0,
			pfC0, pfC1,
			rotInv, fScaleDiffInv
		);

		pfs0[i] = pfs1[i] * fScaleDiffInv;
	}
	return 1;
}


//
// refine_feature_matches()
//
// Here, apply correlation to fine-tune point locations
//
//
int
refine_feature_matches(
	FEATUREIO &fio0,
	FEATUREIO &fio1,
	float *pf0, // Points in image 1
	float *pf1, // Points in image 2
	float *pfs0, // Scale for points in image 1
	float *pfs1, // Scale for points in image 2
	int iCount,
	int iSearchRadius,
	float *pfNCC
)
{
	FEATUREIO fioLike1;
	fioLike1 = fio1;
	fioAllocate(fioLike1);
	fioSet(fioLike1, 0.5f);

	LOCATION_VALUE_XYZ_ARRAY lvaPeaks;
	memset(&lvaPeaks, 0, sizeof(lvaPeaks));
	lvaPeaks.plvz = new LOCATION_VALUE_XYZ[1000];

	for (int i = 0; i < iCount; i++)
	{
		int piCoord1[3];
		int piSize1[3];
		int piCoord2[3];
		int piSize2[3];
		fioSet(fioLike1, 0);

		piCoord1[0] = pf0[i * 3 + 0];			piCoord1[1] = pf0[i * 3 + 1];			piCoord1[2] = pf0[i * 3 + 2];
		piSize1[0] = 2 * pfs1[i] + 1;	piSize1[1] = 2 * pfs1[i] + 1;		piSize1[2] = 2 * pfs1[i] + 1;

		piCoord2[0] = pf1[i * 3 + 0];			piCoord2[1] = pf1[i * 3 + 1];			piCoord2[2] = pf1[i * 3 + 2];
		piSize2[0] = iSearchRadius;		piSize2[1] = iSearchRadius;			piSize2[2] = iSearchRadius;

		hfGenerateLikelihoodImageCorrelation(
			//hfGenerateProbMILikelihoodImage(
			piCoord1,	// Center of box in fio1, xyzt, col/row/z/time
			piSize1,  // Size of box in fio1
			piCoord2,	// Center of box (search area) in fio2
			piSize2,	// Center of box (search area) in fio2

			fio0,
			fio1, fioLike1
		);

		// Write out latest peaks
		regFindFEATUREIOPeaks(lvaPeaks, fioLike1);
		//sprintf( pcFileName, "peaks%3.3d.txt", i );
		lvSortHighLow(lvaPeaks);

		char pcFileName[400];

		Feature3DInfo feat;

		feat.x = pf1[i * 3 + 0];
		feat.y = pf1[i * 3 + 1];
		feat.z = pf1[i * 3 + 2];
		feat.scale = pfs1[i];
		//sprintf( pcFileName, "feat%3.3d_img1", i );
		//feat.OutputVisualFeatureInVolume( fio1, pcFileName, 1 );

		feat.x = pf0[i * 3 + 0];
		feat.y = pf0[i * 3 + 1];
		feat.z = pf0[i * 3 + 2];
		feat.scale = pfs1[i];
		//sprintf( pcFileName, "feat%3.3d_img0", i );
		//feat.OutputVisualFeatureInVolume( fio0, pcFileName, 1 );

		float fx;
		float fy;
		float fz;
		if (lvaPeaks.iCount > 0)
		{
			get_max_3D(lvaPeaks.plvz[0].x, lvaPeaks.plvz[0].y, lvaPeaks.plvz[0].z, fioLike1, fx, fy, fz, 1);

			pf1[i * 3 + 0] = feat.x = fx;
			pf1[i * 3 + 1] = feat.y = fy;
			pf1[i * 3 + 2] = feat.z = fz;
			if (pfNCC)
			{
				pfNCC[i] = lvaPeaks.plvz[0].fValue;
			}
		}
		else
		{
			// No peaks found
			if (pfNCC)
			{
				pfNCC[i] = 0.5;
			}
		}

		//sprintf( pcFileName, "feat%3.3d_img1_reg", i );
		//feat.OutputVisualFeatureInVolume( fio1, pcFileName, 1 );

		//sprintf( pcFileName, "feat%3.3d_atlas_likelihood", i );
		//feat.OutputVisualFeatureInVolume( fioLike1, pcFileName, 1 );
	}

	delete[] lvaPeaks.plvz;
	fioDelete(fioLike1);

	return 1;
}

//
// determine_similarity_transform_ransac()
//
// 
//
//
int
determine_similarity_transform_ransac(
	float *pf0, // Points in image 1
	float *pf1, // Points in image 2
	float *pfs0, // Scale for points in image 1
	float *pfs1, // Scale for points in image 2
	float *pfProb, // Probability associated with match

	int iPoints, // number of points
	int iIterations, // number of iterations

	float *pfC0, // Reference center point in image 1

				 // Output

				 // Reference center in image 2
	float *pfC1,

	// Rotation about point pfP01/pfP11
	float *rot,

	// Scale change (magnification) from image 1 to image 2
	float &fScaleDiff,

	// Flag inliers
	int *piInliers,


	float fScaleDiffThreshold,
	float fShiftThreshold,
	float fCosineAngleThreshold
)
{
	//ScaleSpaceXYZHash ssh;

	int iMaxInliers = -1;
	int iMaxInlier1 = -1;
	int iMaxInlier2 = -1;
	int iMaxInlier3 = -1;

	float fMaxInlierProb = 0;

	for (int i = 0; i < iIterations; i++)
	{
		// Find a set of unique points
		int i1 = iPoints*(rand() / (RAND_MAX + 1.0f));
		int i2 = iPoints*(rand() / (RAND_MAX + 1.0f));
		int i3 = iPoints*(rand() / (RAND_MAX + 1.0f));
		while (i1 == i2 || i1 == i3 || i2 == i3)
		{
			i1 = iPoints*(rand() / (RAND_MAX + 1.0f));
			i2 = iPoints*(rand() / (RAND_MAX + 1.0f));
			i3 = iPoints*(rand() / (RAND_MAX + 1.0f));
		}

		// Calculate transform
		float rot_here0[3][3];
		float rot_here1[3][3];
		float fScaleDiffHere;
		determine_similarity_transform_3point(
			pf0 + i1 * 3, pf0 + i2 * 3, pf0 + i3 * 3,
			pf1 + i1 * 3, pf1 + i2 * 3, pf1 + i3 * 3,
			(float*)(&(rot_here0[0][0])),
			(float*)(&(rot_here1[0][0])),
			fScaleDiffHere
		);

		//fScaleDiffHere = 1.0f / fScaleDiffHere;

		// Count the number of inliers
		int iInliers = 0;
		float fInlierProb = 0;
		float pfTest[3];
		for (int j = 0; j < iPoints; j++)
		{
			// Transform point j in image 1
			//similarity_transform_3point(
			//	pf0+j*3, pfTest,
			//	pf0+i1*3, pf1+i1*3,
			//	(float*)(&(rot_here0[0][0])), (float*)(&(rot_here1[0][0])), fScaleDiff );


			similarity_transform_3point(
				pf0 + j * 3, pfTest,
				pf0 + i1 * 3, pf1 + i1 * 3,
				(float*)(&(rot_here0[0][0])), fScaleDiffHere);


			// Evaluate
			Feature3DInfo f1Real;
			Feature3DInfo f1Test;
			f1Real.x = pf1[j * 3 + 0];
			f1Real.y = pf1[j * 3 + 1];
			f1Real.z = pf1[j * 3 + 2];
			f1Real.scale = pfs1[j];

			f1Test.x = pfTest[0];
			f1Test.y = pfTest[1];
			f1Test.z = pfTest[2];
			f1Test.scale = pfs0[j] * fScaleDiffHere;

			if (compatible_features(f1Real, f1Test, fScaleDiffThreshold, fShiftThreshold, fCosineAngleThreshold))
			{
				iInliers++;
				fInlierProb += pfProb[j];
			}
		}
		//if( iInliers > iMaxInliers )
		if (fInlierProb > fMaxInlierProb)
		{

			fMaxInlierProb = fInlierProb;

			iMaxInliers = iInliers;
			iMaxInlier1 = i1;
			iMaxInlier2 = i2;
			iMaxInlier3 = i3;

			// Calculate transform
			float rot_here0[3][3];
			float rot_here1[3][3];
			float fScaleDiffHere;
			determine_similarity_transform_3point(
				pf0 + i1 * 3, pf0 + i2 * 3, pf0 + i3 * 3,
				pf1 + i1 * 3, pf1 + i2 * 3, pf1 + i3 * 3,
				(float*)(&(rot_here0[0][0])),
				(float*)(&(rot_here1[0][0])),
				fScaleDiffHere
			);



			Feature3DInfo f1Real;
			Feature3DInfo f1Test;

			// Save inliers
			float pfTest[3];
			for (int j = 0; j < iPoints; j++)
			{
				similarity_transform_3point(
					pf0 + j * 3, pfTest,
					pf0 + i1 * 3, pf1 + i1 * 3,
					(float*)(&(rot_here0[0][0])), fScaleDiffHere);

				// Evaluate
				f1Real.x = pf1[j * 3 + 0];
				f1Real.y = pf1[j * 3 + 1];
				f1Real.z = pf1[j * 3 + 2];
				f1Real.scale = pfs1[j];

				f1Test.x = pfTest[0];
				f1Test.y = pfTest[1];
				f1Test.z = pfTest[2];
				f1Test.scale = pfs0[j] * fScaleDiffHere;

				if (piInliers)
				{
					if (compatible_features(f1Real, f1Test, fScaleDiffThreshold, fShiftThreshold, fCosineAngleThreshold))
					{
						piInliers[j] = 1;
					}
					else
					{
						piInliers[j] = 0;
					}
				}
			}

			// Save output transform parameters

			// Calculate pfC1, by transforming pfC0 -> pfC1
			similarity_transform_3point(
				pfC0, pfC1,
				pf0 + i1 * 3, pf1 + i1 * 3,
				(float*)(&(rot_here0[0][0])), fScaleDiffHere);

			// Save rotation matrix
			memcpy(rot, &(rot_here0[0][0]), sizeof(rot_here0));

			// Save scale change
			fScaleDiff = fScaleDiffHere;
		}
	}
	return iMaxInliers;
}

int
refine_similarity_transform_ransac(
	float *pf0, // Points in image 1
	float *pf1, // Points in image 2
	float *pfs0, // Scale for points in image 1
	float *pfs1, // Scale for points in image 2
	float *pfProb, // Probability associated with match

	int iPoints, // number of points
	int iIterations, // number of iterations

	float *pfC0, // Reference center point in image 1

				 // Output

				 // Reference center in image 2
	float *pfC1,

	// Rotation about point pfP01/pfP11
	float *rot,

	// Scale change (magnification) from image 1 to image 2
	float &fScaleDiff,

	// Flag inliers
	int *piInliers,


	float fScaleDiffThreshold,
	float fShiftThreshold,
	float fCosineAngleThreshold
)
{
	//ScaleSpaceXYZHash ssh;

	int iMaxInliers = -1;
	int iMaxInlier1 = -1;
	int iMaxInlier2 = -1;
	int iMaxInlier3 = -1;

	float fMaxInlierProb = 0;
	float fMinResidualError = -1;

	for (int i = 0; i < iIterations; i++)
	{
		// Find a set of unique points
		int i1 = iPoints*(rand() / (RAND_MAX + 1.0f));
		int i2 = iPoints*(rand() / (RAND_MAX + 1.0f));
		int i3 = iPoints*(rand() / (RAND_MAX + 1.0f));
		while (i1 == i2 || i1 == i3 || i2 == i3)
		{
			i1 = iPoints*(rand() / (RAND_MAX + 1.0f));
			i2 = iPoints*(rand() / (RAND_MAX + 1.0f));
			i3 = iPoints*(rand() / (RAND_MAX + 1.0f));
		}

		// Calculate transform
		float rot_here0[3][3];
		float rot_here1[3][3];
		float fScaleDiffHere;
		determine_similarity_transform_3point(
			pf0 + i1 * 3, pf0 + i2 * 3, pf0 + i3 * 3,
			pf1 + i1 * 3, pf1 + i2 * 3, pf1 + i3 * 3,
			(float*)(&(rot_here0[0][0])),
			(float*)(&(rot_here1[0][0])),
			fScaleDiffHere
		);

		//fScaleDiffHere = 1.0f / fScaleDiffHere;

		// Compute residual error
		int iInliers = 0;
		float fInlierProb = 0;
		float pfTest[3];
		float fResidualError = 0;
		for (int j = 0; j < iPoints; j++)
		{
			// Transform point j in image 1
			//similarity_transform_3point(
			//	pf0+j*3, pfTest,
			//	pf0+i1*3, pf1+i1*3,
			//	(float*)(&(rot_here0[0][0])), (float*)(&(rot_here1[0][0])), fScaleDiff );


			similarity_transform_3point(
				pf0 + j * 3, pfTest,
				pf0 + i1 * 3, pf1 + i1 * 3,
				(float*)(&(rot_here0[0][0])), fScaleDiffHere);


			// Evaluate
			Feature3DInfo f1Real;
			Feature3DInfo f1Test;
			f1Real.x = pf1[j * 3 + 0];
			f1Real.y = pf1[j * 3 + 1];
			f1Real.z = pf1[j * 3 + 2];
			f1Real.scale = pfs1[j];

			f1Test.x = pfTest[0];
			f1Test.y = pfTest[1];
			f1Test.z = pfTest[2];
			f1Test.scale = pfs0[j] * fScaleDiffHere;

			float dx = f1Real.x - f1Test.x;
			float dy = f1Real.y - f1Test.y;
			float dz = f1Real.z - f1Test.z;
			float fError = dx*dx + dy*dy + dz*dz;
			if (fError > 0)
			{
				fError = sqrt(fError);
			}

			//fError /= f1Real.scale;

			fResidualError += fError;

		}
		//if( iInliers > iMaxInliers )
		if (fMinResidualError < 0 || (fMinResidualError > 0 && fResidualError < fMinResidualError))
		{
			fMinResidualError = fResidualError;
			iMaxInliers = fResidualError;

			// Save output transform parameters

			// Calculate pfC1, by transforming pfC0 -> pfC1
			similarity_transform_3point(
				pfC0, pfC1,
				pf0 + i1 * 3, pf1 + i1 * 3,
				(float*)(&(rot_here0[0][0])), fScaleDiffHere);

			//memcpy( pfC0, pf0+i1*3, sizeof(float)*3 );
			//memcpy( pfC1, pf1+i1*3, sizeof(float)*3 );

			// Save rotation matrix
			memcpy(rot, &(rot_here0[0][0]), sizeof(rot_here0));

			// Save scale change
			fScaleDiff = fScaleDiffHere;
		}
	}
	return iMaxInliers;
}


//
// feature_to_three_points()
//
// Convert a scale-invariant feature to three points
//
int
feature_to_three_points(
	float *pfPoint,
	float *pfOri, // 3x3 Orientation
	float fScale,
	float *pfPoints // 3 output points
)
{
	//
	// Nasty bug - took 1 1/2 days to figure out!!! 
	//
	//pfPoints[0*3+0] = pfPoint[0] + fScale*pfOri[0*3+0];
	//pfPoints[0*3+1] = pfPoint[1] + fScale*pfOri[1*3+0];
	//pfPoints[0*3+2] = pfPoint[2] + fScale*pfOri[2*3+0];
	//
	//pfPoints[1*3+0] = pfPoint[0] + fScale*pfOri[0*3+1];
	//pfPoints[1*3+1] = pfPoint[1] + fScale*pfOri[1*3+1];
	//pfPoints[1*3+2] = pfPoint[2] + fScale*pfOri[2*3+1];
	//
	//pfPoints[2*3+0] = pfPoint[0] + fScale*pfOri[0*3+2];
	//pfPoints[2*3+1] = pfPoint[1] + fScale*pfOri[1*3+2];
	//pfPoints[2*3+2] = pfPoint[2] + fScale*pfOri[2*3+2];

	pfPoints[0 * 3 + 0] = pfPoint[0] + fScale*pfOri[0 * 3 + 0];
	pfPoints[0 * 3 + 1] = pfPoint[1] + fScale*pfOri[0 * 3 + 1];
	pfPoints[0 * 3 + 2] = pfPoint[2] + fScale*pfOri[0 * 3 + 2];

	pfPoints[1 * 3 + 0] = pfPoint[0] + fScale*pfOri[1 * 3 + 0];
	pfPoints[1 * 3 + 1] = pfPoint[1] + fScale*pfOri[1 * 3 + 1];
	pfPoints[1 * 3 + 2] = pfPoint[2] + fScale*pfOri[1 * 3 + 2];

	pfPoints[2 * 3 + 0] = pfPoint[0] + fScale*pfOri[2 * 3 + 0];
	pfPoints[2 * 3 + 1] = pfPoint[1] + fScale*pfOri[2 * 3 + 1];
	pfPoints[2 * 3 + 2] = pfPoint[2] + fScale*pfOri[2 * 3 + 2];



	return 1;
}

int
determine_similarity_transform_hough(
	float *pf0, // Points in image 1
	float *pf1, // Points in image 2
	float *pfs0, // Scale for points in image 1
	float *pfs1, // Scale for points in image 2
	float *pfo0, // 3x3 Orientation for points in image 1
	float *pfo1, // 3x3 Orientation for points in image 2
	float *pfProb, // Probability associated with match

	int iPoints, // number of points
	int iIterations, // number of iterations

	float *pfC0, // Reference center point in image 1

				 // Output

				 // Reference center in image 2
	float *pfC1,

	// Rotation about point pfP01/pfP11
	float *rot,

	// Scale change (magnification) from image 1 to image 2
	float &fScaleDiff,

	// Flag inliers
	int *piInliers
)
{
	//ScaleSpaceXYZHash ssh;

	int iMaxInliers = -1;
	int iMaxInlier1 = -1;
	int iMaxInlier2 = -1;
	int iMaxInlier3 = -1;

	float fMaxInlierProb = 0;

	// Go through entire set of points
	for (int i = 0; i < iPoints; i++)
	{
		float pts0[3 * 3];
		float pts1[3 * 3];

		feature_to_three_points(pf0 + i * 3, pfo0 + i * 3 * 3, pfs0[i], pts0);
		feature_to_three_points(pf1 + i * 3, pfo1 + i * 3 * 3, pfs1[i], pts1);

		// Calculate transform
		float rot_here0[3][3];
		float rot_here1[3][3];
		float fScaleDiffHere;
		determine_similarity_transform_3point(
			pts0 + 0 * 3, pts0 + 1 * 3, pts0 + 2 * 3,
			pts1 + 0 * 3, pts1 + 1 * 3, pts1 + 2 * 3,
			(float*)(&(rot_here0[0][0])),
			(float*)(&(rot_here1[0][0])),
			fScaleDiffHere
		);

		//fScaleDiffHere = 1.0f / fScaleDiffHere;

		// Count the number of inliers
		int iInliers = 0;
		float fInlierProb = 0;
		float pfTest[3];
		for (int j = 0; j < iPoints; j++)
		{
			// Transform point j in image 1
			similarity_transform_3point(
				pf0 + j * 3, pfTest,
				pf0 + i * 3, pf1 + i * 3,
				(float*)(&(rot_here0[0][0])), fScaleDiffHere);

			//float pfStuff[3] = {0,0,0};
			//float pfCenter2[3] = {0,0,0};
			//similarity_transform_3point(
			//	pfStuff, pfCenter2,
			//	pf0+i*3, pf1+i*3,
			//	(float*)(&(rot_here0[0][0])), fScaleDiffHere );

			//similarity_transform_3point(
			//	pf0+j*3, pfTest,
			//	pfStuff, pfCenter2,
			//	(float*)(&(rot_here0[0][0])), fScaleDiffHere );


			// Evaluate
			Feature3DInfo f1Real;
			Feature3DInfo f1Test;
			f1Real.x = pf1[j * 3 + 0];
			f1Real.y = pf1[j * 3 + 1];
			f1Real.z = pf1[j * 3 + 2];
			f1Real.scale = pfs1[j];

			f1Test.x = pfTest[0];
			f1Test.y = pfTest[1];
			f1Test.z = pfTest[2];
			f1Test.scale = pfs0[j] * fScaleDiffHere;

			// These parameters tuned from M. Styners Krabbe disease dataset
#define HOUGH_THRES_SCALE 1.0
#define HOUGH_THRES_TRANS 2.0
#define HOUGH_THRES_ORIEN 0.7
			//#define HOUGH_THRES_SCALE 0.5
			//#define HOUGH_THRES_TRANS 2.0
			//#define HOUGH_THRES_ORIEN 0.7

			if (compatible_features(f1Real, f1Test, HOUGH_THRES_SCALE, HOUGH_THRES_TRANS))
			{
				float rot_temp[3][3];
				memcpy(&(f1Real.ori[0][0]), pfo1 + j * 3 * 3, 3 * 3 * sizeof(float));
				memcpy(&(rot_temp[0][0]), pfo0 + j * 3 * 3, 3 * 3 * sizeof(float));
				transpose_3x3<float, float>(rot_temp, f1Test.ori);
				mult_3x3_matrix<float, float>(rot_here0, f1Test.ori, rot_temp);
				transpose_3x3<float, float>(rot_temp, f1Test.ori);
				if (compatible_features(f1Real, f1Test, HOUGH_THRES_SCALE, HOUGH_THRES_TRANS, HOUGH_THRES_ORIEN))
				{
					iInliers++;
					fInlierProb += pfProb[j];
				}
			}
		}
		//if( iInliers > iMaxInliers )
		if (fInlierProb > fMaxInlierProb)
		{

			fMaxInlierProb = fInlierProb;

			iMaxInliers = iInliers;

			feature_to_three_points(pf0 + i * 3, pfo0 + i * 3 * 3, pfs0[i], pts0);
			feature_to_three_points(pf1 + i * 3, pfo1 + i * 3 * 3, pfs1[i], pts1);

			// Calculate transform
			float rot_here0[3][3];
			float rot_here1[3][3];
			float fScaleDiffHere;
			determine_similarity_transform_3point(
				pts0 + 0 * 3, pts0 + 1 * 3, pts0 + 2 * 3,
				pts1 + 0 * 3, pts1 + 1 * 3, pts1 + 2 * 3,
				(float*)(&(rot_here0[0][0])),
				(float*)(&(rot_here1[0][0])),
				fScaleDiffHere
			);

			Feature3DInfo f1Real;
			Feature3DInfo f1Test;

			// Save inliers
			float pfTest[3];
			for (int j = 0; j < iPoints; j++)
			{
				// Transform point j in image 1
				similarity_transform_3point(
					pf0 + j * 3, pfTest,
					pf0 + i * 3, pf1 + i * 3,
					(float*)(&(rot_here0[0][0])), fScaleDiffHere);

				// Evaluate
				f1Real.x = pf1[j * 3 + 0];
				f1Real.y = pf1[j * 3 + 1];
				f1Real.z = pf1[j * 3 + 2];
				f1Real.scale = pfs1[j];

				f1Test.x = pfTest[0];
				f1Test.y = pfTest[1];
				f1Test.z = pfTest[2];
				f1Test.scale = pfs0[j] * fScaleDiffHere;

				if (piInliers)
				{
					if (compatible_features(f1Real, f1Test, HOUGH_THRES_SCALE, HOUGH_THRES_TRANS))
					{
						float rot_temp[3][3];
						memcpy(&(f1Real.ori[0][0]), pfo1 + j * 3 * 3, 3 * 3 * sizeof(float));
						memcpy(&(rot_temp[0][0]), pfo0 + j * 3 * 3, 3 * 3 * sizeof(float));
						transpose_3x3<float, float>(rot_temp, f1Test.ori);
						mult_3x3_matrix<float, float>(rot_here0, f1Test.ori, rot_temp);
						transpose_3x3<float, float>(rot_temp, f1Test.ori);
						if (compatible_features(f1Real, f1Test, HOUGH_THRES_SCALE, HOUGH_THRES_TRANS, HOUGH_THRES_ORIEN))
						{
							piInliers[j] = 1;
						}
					}
					else
					{
						piInliers[j] = 0;
					}
				}
			}

			// Save output transform parameters

			// Calculate pfC1, by transforming pfC0 -> pfC1
			similarity_transform_3point(
				pfC0, pfC1,
				pf0 + i * 3, pf1 + i * 3,
				(float*)(&(rot_here0[0][0])), fScaleDiffHere);

			// Save rotation matrix
			memcpy(rot, &(rot_here0[0][0]), sizeof(rot_here0));

			// Save scale change
			fScaleDiff = fScaleDiffHere;
		}
	}
	return iMaxInliers;
}


//
// determine_similarity_transform_hough_expanded()
//
// The standard hough transform looks for inliers according to 
// a set of thresholds in maximum permissible parameter deviation.
//
// This function attempts to expand the standard hough transform.
// Rather than keep one maximal set of inliers, we identify all inliers of inliers.
// That is, all data that are not inliers of the globally maximal parameter configuration,
// but which are inliers of parameter configurations consistent with inliers of the globally maximal parameter configuration.
//
// This enables identifying identifying correspondences between deformable objects.
//
// This function must be called after determine_similarity_transform_hough()
//
int
determine_similarity_transform_hough_expanded(
	float *pf0, // Points in image 1
	float *pf1, // Points in image 2
	float *pfs0, // Scale for points in image 1
	float *pfs1, // Scale for points in image 2
	float *pfo0, // 3x3 Orientation for points in image 1
	float *pfo1, // 3x3 Orientation for points in image 2
	float *pfProb, // Probability associated with match

	int iPoints, // number of points
	int iIterations, // number of iterations

	float *pfC0, // Reference center point in image 1

				 // Output

				 // Reference center in image 2
	float *pfC1,

	// Rotation about point pfP01/pfP11
	float *rot,

	// Scale change (magnification) from image 1 to image 2
	float &fScaleDiff,

	// Flag inliers
	int *piInliers
)
{
	//ScaleSpaceXYZHash ssh;

	int iMaxInliers = -1;
	int iMaxInlier1 = -1;
	int iMaxInlier2 = -1;
	int iMaxInlier3 = -1;

	float fMaxInlierProb = 0;

	int iInlierOriginal = 0;

	int *piInliersOriginal = new int[iPoints];

	// Could original inliers
	for (int i = 0; i < iPoints; i++)
	{
		piInliersOriginal[i] = piInliers[i];
		if (piInliers[i] > 0)
		{
			iInlierOriginal++;
		}
	}

	// Go through entire set of points
	for (int i = 0; i < iPoints; i++)
	{
		// Do not process non-inliers
		if (piInliersOriginal[i] <= 0)
		{
			continue;
		}

		// Create transform associated with this inlier
		float pts0[3 * 3];
		float pts1[3 * 3];

		feature_to_three_points(pf0 + i * 3, pfo0 + i * 3 * 3, pfs0[i], pts0);
		feature_to_three_points(pf1 + i * 3, pfo1 + i * 3 * 3, pfs1[i], pts1);

		// Calculate transform
		float rot_here0[3][3];
		float rot_here1[3][3];
		float fScaleDiffHere;
		determine_similarity_transform_3point(
			pts0 + 0 * 3, pts0 + 1 * 3, pts0 + 2 * 3,
			pts1 + 0 * 3, pts1 + 1 * 3, pts1 + 2 * 3,
			(float*)(&(rot_here0[0][0])),
			(float*)(&(rot_here1[0][0])),
			fScaleDiffHere
		);

		//fScaleDiffHere = 1.0f / fScaleDiffHere;

		// Count the number of inliers
		int iInliers = 0;
		float fInlierProb = 0;
		float pfTest[3];
		for (int j = 0; j < iPoints; j++)
		{
			// Transform point j in image 1
			similarity_transform_3point(
				pf0 + j * 3, pfTest,
				pf0 + i * 3, pf1 + i * 3,
				(float*)(&(rot_here0[0][0])), fScaleDiffHere);

			// Evaluate
			Feature3DInfo f1Real;
			Feature3DInfo f1Test;
			f1Real.x = pf1[j * 3 + 0];
			f1Real.y = pf1[j * 3 + 1];
			f1Real.z = pf1[j * 3 + 2];
			f1Real.scale = pfs1[j];

			f1Test.x = pfTest[0];
			f1Test.y = pfTest[1];
			f1Test.z = pfTest[2];
			f1Test.scale = pfs0[j] * fScaleDiffHere;

			// These parameters tuned from M. Styners Krabbe disease dataset
#define HOUGH_THRES_SCALE 1.0
#define HOUGH_THRES_TRANS 2.0
#define HOUGH_THRES_ORIEN 0.7
			//#define HOUGH_THRES_SCALE 0.5
			//#define HOUGH_THRES_TRANS 2.0
			//#define HOUGH_THRES_ORIEN 0.7

			if (compatible_features(f1Real, f1Test, HOUGH_THRES_SCALE, HOUGH_THRES_TRANS))
			{
				float rot_temp[3][3];
				memcpy(&(f1Real.ori[0][0]), pfo1 + j * 3 * 3, 3 * 3 * sizeof(float));
				memcpy(&(rot_temp[0][0]), pfo0 + j * 3 * 3, 3 * 3 * sizeof(float));
				transpose_3x3<float, float>(rot_temp, f1Test.ori);
				mult_3x3_matrix<float, float>(rot_here0, f1Test.ori, rot_temp);
				transpose_3x3<float, float>(rot_temp, f1Test.ori);
				if (compatible_features(f1Real, f1Test, HOUGH_THRES_SCALE, HOUGH_THRES_TRANS, HOUGH_THRES_ORIEN))
				{
					piInliers[j] = 1;
				}
			}
		}
	}

	int iInlierReturn = 0;

	// Could new inliers
	for (int i = 0; i < iPoints; i++)
	{
		if (piInliers[i] > 0)
		{
			iInlierReturn++;
		}
	}

	delete[] piInliersOriginal;

	return iInlierReturn;
}

int
msMirrorYFeaturesGradientOrientationHistogram(
	Feature3DInfo &feat3D
)
{

	// Consider 8 orientation angles
#define GRAD_ORI_ORIBINS 8
	// Reference to grad orientation info
#define GRAD_ORI_SPACEBINS 2
	float fBinSize = Feature3D::FEATURE_3D_DIM / (float)GRAD_ORI_SPACEBINS;
	FEATUREIO fioImgGradOri;
	fioImgGradOri.x = fioImgGradOri.z = fioImgGradOri.y = 2;
	assert(Feature3DInfo::FEATURE_3D_PCS >= 64);
	fioImgGradOri.pfVectors = &(feat3D.m_pfPC[0]);
	fioImgGradOri.pfMeans = 0;
	fioImgGradOri.pfVarrs = 0;
	fioImgGradOri.iFeaturesPerVector = GRAD_ORI_ORIBINS;
	fioImgGradOri.t = 1;

	float pfTmpOri[GRAD_ORI_ORIBINS];

	// First flip all spatial bins about y
	for (int z = 0; z < fioImgGradOri.z / 2; z++)
	{
		for (int y = 0; y < fioImgGradOri.y; y++)
		{
			for (int x = 0; x < fioImgGradOri.x; x++)
			{
				float *pf0 = fioGetVector(fioImgGradOri, x, y, z);
				float *pf1 = fioGetVector(fioImgGradOri, x, y, fioImgGradOri.z - 1 - z);
				memcpy(pfTmpOri, pf0, sizeof(pfTmpOri));
				memcpy(pf0, pf1, sizeof(pfTmpOri));
				memcpy(pf1, pfTmpOri, sizeof(pfTmpOri));
			}
		}
	}

	// Now go and flip angles
	// As it turns out, the individual orientation buckets are indexed 
	// in reverse order: zyx, rather than xyz
	for (int z = 0; z < fioImgGradOri.z; z++)
	{
		for (int y = 0; y < fioImgGradOri.y; y++)
		{
			for (int x = 0; x < fioImgGradOri.x; x++)
			{
				float *pf0 = fioGetVector(fioImgGradOri, x, y, z);
				float fTmp;

				fTmp = pf0[0]; pf0[0] = pf0[1]; pf0[1] = fTmp;
				fTmp = pf0[4]; pf0[4] = pf0[5]; pf0[5] = fTmp;
				fTmp = pf0[2]; pf0[2] = pf0[3]; pf0[3] = fTmp;
				fTmp = pf0[6]; pf0[6] = pf0[7]; pf0[7] = fTmp;
			}
		}
	}
	return 1;
}


//int
//msMirrorYFeaturesGradientOrientationHistogram_backup(
//					   Feature3DInfo &feat3D
//	)
//{
//
//	// Consider 8 orientation angles
//#define GRAD_ORI_ORIBINS 8
//	float pfOriAngles[GRAD_ORI_ORIBINS][3] = 
//	{
//		{ 1,	 1,	 1},
//		{ 1,	 1,	-1},
//		{ 1,	-1,	 1},
//		{ 1,	-1,	-1},
//		{-1,	 1,	 1},
//		{-1,	 1,	-1},
//		{-1,	-1,	 1},
//		{-1,	-1,	-1},
//	};
//
//
//	// Reference to grad orientation info
//#define GRAD_ORI_SPACEBINS 2
//	float fBinSize = Feature3D::FEATURE_3D_DIM/(float)GRAD_ORI_SPACEBINS;
//	FEATUREIO fioImgGradOri;
//	fioImgGradOri.x = fioImgGradOri.z = fioImgGradOri.y = 2;
//	assert( Feature3DInfo::FEATURE_3D_PCS >= 64 );
//	fioImgGradOri.pfVectors = &(feat3D.m_pfPC[0]);
//	fioImgGradOri.pfMeans = 0;
//	fioImgGradOri.pfVarrs = 0;
//	fioImgGradOri.iFeaturesPerVector = GRAD_ORI_ORIBINS;
//	fioImgGradOri.t = 1;
//
//	float pfTmpOri[GRAD_ORI_ORIBINS];
//	
//	// First flip all spatial bins about y
//	for( int z = 0; z < 2; z++ )
//	{
//		for( int y = 0; y < fioImgGradOri.y/2; y++ )
//		{
//			for( int x = 0; x < 2; x++ )
//			{
//				float *pf0 = fioGetVector( fioImgGradOri, x, y, z );
//				float *pf1 = fioGetVector( fioImgGradOri, x, fioImgGradOri.y-1-y, z );
//				memcpy( pfTmpOri, pf0, sizeof(pfTmpOri) );
//				memcpy( pf0, pf1, sizeof(pfTmpOri) );
//				memcpy( pf1, pfTmpOri, sizeof(pfTmpOri) );
//			}
//		}
//	}
//
//	// Now go and flip angles
//	for( int z = 0; z < fioImgGradOri.z; z++ )
//	{
//		for( int y = 0; y < fioImgGradOri.y/2; y++ )
//		{
//			for( int x = 0; x < fioImgGradOri.x; x++ )
//			{
//				float *pf0 = fioGetVector( fioImgGradOri, x, y, z );
//				float fTmp;
//
//				fTmp = pf0[0]; pf0[0] = pf0[2]; pf0[2] = fTmp;
//				fTmp = pf0[1]; pf0[1] = pf0[3]; pf0[3] = fTmp;
//				fTmp = pf0[4]; pf0[4] = pf0[6]; pf0[6] = fTmp;
//				fTmp = pf0[5]; pf0[5] = pf0[7]; pf0[7] = fTmp;
//			}
//		}
//	}
//	return 1;
//}


int
msInvertedIntensity(
	Feature3DInfo &feat
)
{
	// Invert first two orientations
	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			feat.ori[i][j] = -feat.ori[i][j];
		}
	}
	// Flip

	//unsigned char chIndices[64] = {27,	26,	25,	24,	31,	30,	29,	24,	19,	18,	17,	16,	23,	22,	21,	16,	11,	10,	9,	8,	15,	14,	13,	8,	3,	2,	1,	0,	7,	6,	5,	0,	59,	58,	57,	56,	63,	62,	61,	56,	51,	50,	49,	48,	55,	54,	53,	48,	43,	42,	41,	40,	47,	46,	45,	40,	35,	34,	33,	32,	39,	38,	37,	32};
	unsigned char chIndices[64] = { 25,	24,	27,	26,	29,	28,	31,	30,	17,	16,	19,	18,	21,	20,	23,	22,	9,	8,	11,	10,	13,	12,	15,	14,	1,	0,	3,	2,	5,	4,	7,	6,	57,	56,	59,	58,	61,	60,	63,	62,	49,	48,	51,	50,	53,	52,	55,	54,	41,	40,	43,	42,	45,	44,	47,	46,	33,	32,	35,	34,	37,	36,	39,	38 };

	float pfTemp[64];

	for (int i = 0; i < 64; i++)
	{
		pfTemp[i] = feat.m_pfPC[i];
	}
	for (int i = 0; i < 64; i++)
	{
		feat.m_pfPC[i] = pfTemp[chIndices[i]];
	}

	return 1;
}

//
// msExpandMultiModal()
//
// Turn a feature vector into a multimodal feature vector.
// Append to itself, invert.
//
int
msExpandMultiModal(
	vector<Feature3DInfo> &vecFeats
)
{
	int iSize = vecFeats.size();
	for (int iFeat = 0; iFeat < iSize; iFeat++)
	{
		Feature3DInfo &feat1 = vecFeats[1 * iFeat];
		Feature3DInfo feat2;
		feat2 = feat1;
		msInvertedIntensity(feat2);
		vecFeats.push_back(feat2);
	}

	// Original code that introduced bugs but seemed to work
	//int iSize = vecFeats.size();
	//vecFeats.resize( 2*iSize );
	//for( int iFeat = 0; iFeat < iSize; iFeat++ )
	//{
	//	Feature3DInfo &feat1 = vecFeats[1*iFeat];
	//	Feature3DInfo &feat2 = vecFeats[2*iFeat];
	//	feat2 = feat1;
	//	msInvertedIntensity( feat2 );
	//}

	return 1;
}


//int
//determine_similarity_transform_ransac(
//									  float *pf0, // Points in image 1
//									  float *pf1, // Points in image 2
//									  float *pfs0, // Scale for points in image 1
//									  float *pfs1, // Scale for points in image 2
//									  int   *pil0, // Integer labels for points in image 1
//									  int   *pil1, // Integer labels for points in image 2
//
//									  int iPoints, // number of points
//									  int iIterations, // number of iterations
//
//									  float *pfC0, // Reference center point in image 1
//
//									  // Output
//
//									  // Reference center in image 2
//									  float *pfC1, 
//
//									  // Rotation about point pfP01/pfP11
//									  float *rot,
//
//									  // Scale change (magnification) from image 1 to image 2
//									  float &fScaleDiff,
//
//									  // Flag inliers
//									  int *piInliers
//									  )
//{
//	//ScaleSpaceXYZHash ssh;
//
//	int iMaxInliers = -1;
//	int iMaxInlier1 = -1;
//	int iMaxInlier2 = -1;
//	int iMaxInlier3 = -1;
//
//	for( int i = 0; i < iIterations; i++ )
//	{
//		// Find a set of unique points
//		int i1 = iPoints*(rand()/(RAND_MAX+1.0f));
//		int i2 = iPoints*(rand()/(RAND_MAX+1.0f));
//		int i3 = iPoints*(rand()/(RAND_MAX+1.0f));
//		while( i1 == i2 || i1 == i3 || i2 == i3 )
//		{
//			i1 = iPoints*(rand()/(RAND_MAX+1.0f));
//			i2 = iPoints*(rand()/(RAND_MAX+1.0f));
//			i3 = iPoints*(rand()/(RAND_MAX+1.0f));
//		}
//
//		// Calculate transform
//		float rot_here0[3][3];
//		float rot_here1[3][3];
//		float fScaleDiffHere;
//		determine_similarity_transform_3point(
//					   pf0+i1*3, pf0+i2*3, pf0+i3*3,
//					   pf1+i1*3, pf1+i2*3, pf1+i3*3,
//					   (float*)(&(rot_here0[0][0])),
//					   (float*)(&(rot_here1[0][0])),
//					   fScaleDiffHere
//					   );
//
//		//fScaleDiffHere = 1.0f / fScaleDiffHere;
//
//		// Count the number of inliers
//		int iInliers = 0;
//		float pfTest[3];
//		for( int j = 0; j < iPoints; j++ )
//		{
//			// Transform point j in image 1
//			//similarity_transform_3point(
//			//	pf0+j*3, pfTest,
//			//	pf0+i1*3, pf1+i1*3,
//			//	(float*)(&(rot_here0[0][0])), (float*)(&(rot_here1[0][0])), fScaleDiff );
//
//
//			similarity_transform_3point(
//				pf0+j*3, pfTest,
//				pf0+i1*3, pf1+i1*3,
//				(float*)(&(rot_here0[0][0])), fScaleDiffHere );
//
//
//			// Evaluate
//			Feature3DInfo f1Real;
//			Feature3DInfo f1Test;
//			f1Real.x = pf1[j*3+0];
//			f1Real.y = pf1[j*3+1];
//			f1Real.z = pf1[j*3+2];
//			f1Real.scale = pfs1[j];
//
//			f1Test.x = pfTest[0];
//			f1Test.y = pfTest[1];
//			f1Test.z = pfTest[2];
//			f1Test.scale = pfs0[j]*fScaleDiffHere;
//
//			if( compatible_features( f1Real, f1Test, 0.6, 2.0 ) )
//			{
//				iInliers++;
//			}
//		}
//		if( iInliers > iMaxInliers )
//		{
//			iMaxInliers = iInliers;
//			iMaxInlier1 = i1;
//			iMaxInlier2 = i2;
//			iMaxInlier3 = i3;
//
//			// Calculate transform
//			float rot_here0[3][3];
//			float rot_here1[3][3];
//			float fScaleDiffHere;
//			determine_similarity_transform_3point(
//				pf0+i1*3, pf0+i2*3, pf0+i3*3,
//				pf1+i1*3, pf1+i2*3, pf1+i3*3,
//				(float*)(&(rot_here0[0][0])),
//				(float*)(&(rot_here1[0][0])),
//				fScaleDiffHere
//				);
//
//
//
//			Feature3DInfo f1Real;
//			Feature3DInfo f1Test;
//
//			// Save inliers
//			float pfTest[3];
//			for( int j = 0; j < iPoints; j++ )
//			{
//				similarity_transform_3point(
//					pf0+j*3, pfTest,
//					pf0+i1*3, pf1+i1*3,
//					(float*)(&(rot_here0[0][0])), fScaleDiffHere );
//
//				// Evaluate
//				f1Real.x = pf1[j*3+0];
//				f1Real.y = pf1[j*3+1];
//				f1Real.z = pf1[j*3+2];
//				f1Real.scale = pfs1[j];
//
//				f1Test.x = pfTest[0];
//				f1Test.y = pfTest[1];
//				f1Test.z = pfTest[2];
//				f1Test.scale = pfs0[j]*fScaleDiffHere;
//
//				if( piInliers )
//				{
//					if( compatible_features( f1Real, f1Test, 0.6, 2.0 ) )
//					{
//						piInliers[j] = 1;
//					}
//					else
//					{
//						piInliers[j] = 0;
//					}
//				}
//			}
//
//			// Save output transform parameters
//
//			// Calculate pfC1, by transforming pfC0 -> pfC1
//			similarity_transform_3point(
//					pfC0, pfC1,
//					pf0+i1*3, pf1+i1*3,
//					(float*)(&(rot_here0[0][0])), fScaleDiffHere );
//
//			// Save rotation matrix
//			memcpy( rot, &(rot_here0[0][0]), sizeof(rot_here0) );
//
//			// Save scale change
//			fScaleDiff = fScaleDiffHere;
//		}
//	}
//	return iMaxInliers;
//}
