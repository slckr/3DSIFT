
#ifndef __DATASET_H__
#define __DATASET_H__

typedef struct _DATASET
{
	double	*pfVectors;
	int		iFeaturesPerVector;
	int		iVectorCount;
} DATASET;

int
dsReadDataset(
			char	*pcFileName,
			DATASET &ds
			);


int
dsWriteDataset(
			char	*pcFileName,
			DATASET &ds
			);

int
dsDeleteDataset(
				DATASET &ds
				);

#endif
