

#ifndef __MIKOFEATURES_H__
#define __MIKOFEATURES_H__

#define FEATUREOFFSET 5

typedef struct _MIKOFEATURES
{
	int iVectors;
	int iFeaturesPerVector;
	float *pfVectors;
} MIKOFEATURES;

//
// mfRead()
//
// Reads a file of MIKOFEATURES into mf. Returns 1 on
// success, 0 on failure.
//
int
mfRead(
	char *pcFileName,
	MIKOFEATURES &mf
);

//
// mfWrite()
//
// Write features to file. Return 1 on success, 0 on failure.
//
int
mfWrite(
	char *pcFileName,
	MIKOFEATURES &mf
);

//
// mfMiko2Lowe()
//
// Converts a file of miko features to lowe features.
//
int
mfMiko2Lowe(
	char *pcFileMiko,
	char *pcFileLowe,
	char cOutputType = 'd' // could be 'f'float
);

//
// mfMatch()
//
// Matches a pair of MIKOFEATURES.
//
int
mfMatch(
		MIKOFEATURES &mf1,
		MIKOFEATURES &mf2
);

//
// mfMatchVector()
//
// Finds the best match for vector iVector1 of mf1
// to mf2, returns the result in iVector2. Returns the 
// distance squared.
//
float
mfMatchVectorFeatures(
		const MIKOFEATURES	&mf1,
		const MIKOFEATURES	&mf2,
		const int			&iVector1,
		int					&iVector2
);

int
mfMatchVectorCoordinates(
		const MIKOFEATURES	&mf1,
		const MIKOFEATURES	&mf2,
		const int			&iVector1,
		int					&iVector2
);


//
// mfFeatureDistance()
//
// Returns the squared distance between vector iVector1 in mf1
// to iVector2 in mf2.
//
float
mfFeatureDistance(
		const MIKOFEATURES	&mf1,
		const MIKOFEATURES	&mf2,
		const int			&iVector1,
		const int			&iVector2
);

//
// mfCoordinateDistance()
//
// Returns the sqared (row,col) distance between vector iVector1 in mf1
// to iVector2 in mf2.
//
//
float
mfCoordinateDistance(
		const MIKOFEATURES	&mf1,
		const MIKOFEATURES	&mf2,
		const int			&iVector1,
		const int			&iVector2
);


float
mfCol(
	  const MIKOFEATURES	&mf,
	  const int				&iIndex
);

float
mfRow(
	  const MIKOFEATURES	&mf,
	  const int				&iIndex
);

float
mfCornerness(
	  const MIKOFEATURES	&mf,
	  const int				&iIndex
);

float
mfScale(
	  const MIKOFEATURES	&mf,
	  const int				&iIndex
);

//
// mfMatchVectorFeatures2Best()
//
// Calculates first and 2nd best matches.
//
float
mfMatchVectorFeatures2Best(
		const MIKOFEATURES	&mf1,
		const MIKOFEATURES	&mf2,
		const int			&iVector1,
		int					&iIndexMatch1,	// 1st best match
		int					&iIndexMatch2,	// 2nd best match
		float				&fScore1,		// 1st best match score
		float				&fScore2		// 2nd best match score
);

#endif