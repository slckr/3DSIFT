
//
// File: MemoryStore.h
// Developer: Matt Toews
// Desc: A class that stores and reallocates memory.
// Notes:
//	There are data size & data pointer pairs. If the data size is 0,
//	it means the data pointer is externally allocated. If the data
//	size is not zero, the data pointer is internally allocated, and
//	references a valid block of memory.
//	Conditions
//		data size	data pointer
//		---------	------------
//			== 0		== 0		no data
//			== 0		!= 0		externally allocated data
//			!= 0		== 0		error
//			!= 0		!= 0		internally allocated data
//

#ifndef __MEMORYSTORE_H__
#define __MEMORYSTORE_H__

class MemoryStore
{
public:

    MemoryStore();

    ~MemoryStore();

	//
	// Delete()
	//
	// Deletes the internal memory.
	//
	int
	Delete(
	);

	//
	// Reallocate()
	//
	// Reallocates the internal memory.
	//
	int
	Reallocate(
		const int iNewDataSize
	);

	//
	// SetInternalData()
	//
	// Reallocates internal memory, copies
	// external data to internally data.
	//
	int
	SetInternalData(
		const int iExternalDataSize,
		const unsigned char *pcExternalData
	);

	//
	// SetExternalData()
	//
	// Sets an external data pointer.
	//
	int
	SetExternalData(
		const unsigned char *pcExternalData
	);

	//
	// GetData()
	//
	// Returns the current data pointer.
	//
	unsigned char *
	GetData(
	) const;

	//
	// GetDataSize()
	//
	// Returns the current data size.
	//
	int
	GetDataSize(
	) const;

private:

	// Size of stored data

	int				m_iDataSize;

	// Data pointer

	unsigned char	*m_pcData;
};


#ifndef _DEBUG

inline unsigned char *
MemoryStore::GetData(
	) const
{
	return m_pcData;
}

inline int
MemoryStore::GetDataSize(
	) const
{
	return m_iDataSize;
}

#endif


#endif
