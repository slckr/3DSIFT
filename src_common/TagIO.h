
//
// File: TagIO.h
// Desc: Interface for tag file output from SampleTransform,
//       specifying pairs of coordinates for linear/non-linear warping
//       of images.
//

#ifndef __TAGIO_H__
#define __TAGIO_H__

#define MAX_POINTS 50

#include "LocationValue.h"

typedef struct _TAGIO
{
		char *img1FileName;
		char *img2FileName;
		LOCATION_VALUE_XYZ_ARRAY img1points;
		LOCATION_VALUE_XYZ_ARRAY img2points;
} TAGIO;


// tagioRead()
//
// Reads a tag file.
//
int
tagioRead(
		TAGIO &tagio,
		char *pcName
		);

//
// tagioAllocate()
//
// Allocates memory for a TAGIO struct.
//
int
tagioAllocate(
		TAGIO &tagio
		);

//
// tagioDelete()
//
// Delete memory allocated for a TAGIO struct.
//
int
tagioDelete(
		TAGIO &tagio
		);

//
// tagioToMincCoord()
//
// Transforms coordinates in tag file to Minc World Coordinates for 3D volumes.
//
int
tagioToMincCoord(
		TAGIO &tagio
		);

//
// tagioToMincCoord2D()
//
// Transforms coordinates in tag file to Minc World Coordinates for 2D slices.
//
int
tagioToMincCoord2D(
		TAGIO &tagio,
		char plane,				// which plane was sliced (y for xz-plane and so on)
		int plane_location	    // location value of sliced plane
		);

//
// tagioSlice(XY, YZ, XZ)
//
// Performs the coordinate transformations based on which slice was
// used. 
//
int
tagioSliceXY(
		TAGIO &tagio,
		int plane_location
		);

int
tagioSliceYZ(
		TAGIO &tagio,
		int plane_location
		);

int
tagioSliceXZ(
		TAGIO &tagio,
		int plane_location
		);


//
// tagioWrite()
//
// Writes a tag file.
//
int
tagioWrite(
		TAGIO &tagio,
		char *pcName
		);

//
// tagioToLVC()
//
// Fills a LOCATION_VALUE_XYZ_COLLECTION with the coordinates
// in the tagio struct
//
int
tagioToLVC(
		   TAGIO &tagio,
		   LOCATION_VALUE_XYZ_COLLECTION &lvcPoints
		   );

#endif
