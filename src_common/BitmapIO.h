
#ifndef __BITMAPIO_H__
#define __BITMAPIO_H__

#include "Windows.h"

int
bitmapRead(
		   BITMAP &bmp,
		   BITMAPFILEHEADER &bmf,
		   BITMAPINFOHEADER &bmih,
		   char *pcFileName
		   );

int
bitmapWrite(
		   BITMAP &bmp,
		   BITMAPFILEHEADER &bmf,
		   BITMAPINFOHEADER &bmih,
		   char *pcFileName
		   );

int
bitmapNewCopy(
		   BITMAP &bmp,
		   BITMAPFILEHEADER &bmf,
		   BITMAPINFOHEADER &bmih,

		   BITMAP &bmpCopy,
		   BITMAPFILEHEADER &bmfCopy,
		   BITMAPINFOHEADER &bmihCopy
		   );

int
bitmapDelete(
		   BITMAP &bmp,
		   BITMAPFILEHEADER &bmf,
		   BITMAPINFOHEADER &bmih
		   );

#endif