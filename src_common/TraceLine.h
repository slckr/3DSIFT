
//
// File: TraceLine.cpp
// Desc: Traces a line on a discrete grid.
// Dev: Copyright Matt Toews
//

#ifndef __TRACELINE_H__
#define __TRACELINE_H__

//
// TraceCircle.h
//
// Description:
//	Traces a digital circle in a 2D integer coordinate system,
// from a start point to a finish point. At each point along the line,
// a user-defined callback function is called.
//

// Function overriden by user

typedef int tlCallback( int iRow, int iCol, void *pArg );

// Traces line from start coord to finish coord
// Stops when callback function returns (0).

void
tlTraceLine(
		  int			iRowStart,		// Start Row
		  int			iColStart,		// Start Col
		  int			iRowFinish,		// Finish Row
		  int			iColFinish,		// Finish Col
		  tlCallback	*pProcess,		// Function to process pixel
		  void			*pArg			// Argument passed to FFCallback
);

// Traces line from start coord with the slope
// Stops when callback function returns (0).

void
tlTraceLineSlope(
		  int			iRowStart,	// Start Row
		  int			iColStart,	// Start Col
		  int			iDRow,		// Row slope derivative
		  int			iDCol,		// Col slope derivative
		  tlCallback	*pProcess,	// Function to process pixel
		  void			*pArg		// Argument passed to FFCallback
);

#endif
