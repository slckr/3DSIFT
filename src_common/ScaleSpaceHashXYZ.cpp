
#include "ScaleSpaceHashXYZ.h"
//#include "PpImage.h"
//#include "src_common.h"
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

ScaleSpaceXYZHash::ScaleSpaceXYZHash()
{
	m_paArrays = 0;
	int		m_iScaleLen = -1;
}

ScaleSpaceXYZHash::~ScaleSpaceXYZHash()
{
	if( m_paArrays )
	{
		assert( m_iScaleLen > 0 );
		delete [] m_paArrays;
	}
}

int
ScaleSpaceXYZHash::Init(	
		int iDim,
		float *pfMin, // Min/Max/increment values for each dimension
		float *pfMax,
		float *pfIncPerScale, // Increment value per scale (e.g. 0.5)

		float fScaleMin, // Min/Max scale - pixels, this is unused, max scale is calculated from fMinS and fScaleBinMultiple
		float fScaleMax, 
		float fScaleMult
		)
{
	if( iDim <= 0 || iDim > 4 )
	{
		return -1;
	}

	if( m_paArrays )
	{
		assert( m_iScaleLen > 0 );
		delete [] m_paArrays;
		m_paArrays = 0;
	}

	// Save coords
	m_fScaleMin = fScaleMin;
	m_fScaleMax = fScaleMax;
	m_fScaleMult = fScaleMult;

	// Save coords in log space
	m_fScaleMinLog = log( fScaleMin );
	m_fScaleMaxLog = log( fScaleMax );
	m_fScaleIncLog = log( fScaleMult );

	// Calculate array length
	m_iScaleLen = (int)((m_fScaleMaxLog - m_fScaleMinLog)/m_fScaleIncLog) + 1;
	m_paArrays = new XYZArray[m_iScaleLen];
	assert( m_paArrays );
	if( !m_paArrays )
	{
		return -1;
	}
	for( int iSBin = 0; iSBin < m_iScaleLen; iSBin++ )
	{
		//printf( "Array: iDim: %d, ssb: %d, iTotalBins: %d\n", m_paArrays[iSBin].m_iDim, (int)m_paArrays[iSBin].m_pssb, m_paArrays[iSBin].m_iTotalBins );
	}

	// Init one xyzArray for each scale level
	float fCurrScale = m_fScaleMin*fScaleMult;
	for( int iSBin = 0; iSBin < m_iScaleLen; iSBin++ )
	{
		float pfIncs[10];
		for( int i = 0; i < iDim; i++ )
		{
			pfIncs[i] = pfIncPerScale[i]*fCurrScale;
		}
		m_paArrays[iSBin].Init( iDim, pfMin, pfMax, pfIncs );
		fCurrScale *= fScaleMult;
	}

	for( int i = 0; i < iDim; i++ )
	{
		m_pfIncPerScale[i] = pfIncPerScale[i];
	}

	return 0;
}

int
ScaleSpaceXYZHash::InitOther(
								   ScaleSpaceXYZHash &sshOther
		)
{
	return sshOther.Init( 
		m_paArrays[0].GetDim(),
		m_paArrays[0].GetMin(), // Min/Max/increment values for each dimension
		m_paArrays[0].GetMax(),
		m_pfIncPerScale,
		m_fScaleMin,
		m_fScaleMax, 
		m_fScaleMult
		);
}


int
ScaleSpaceXYZHash::CreateFrequencyImage(
		float fScale,
		float *pfFreqImg
		)
{
	XYZArray	*pArr = 0;
	if( fScale < m_fScaleMin )
	{	
		pArr = &m_paArrays[0];
	}
	else if( fScale > m_fScaleMax )
	{
		pArr = &m_paArrays[m_iScaleLen-1];
	}
	else
	{
		float fScaleLog = log( fScale );
		int iScaleBin = (int)((fScaleLog-m_fScaleMinLog)/m_fScaleIncLog);
		pArr = &m_paArrays[iScaleBin];
	}

	int piXYZBinCurr[20];
	int piXYZBinCounts[20];
	memset( piXYZBinCounts, 0, sizeof(piXYZBinCounts) );
	memset( piXYZBinCounts, 0, sizeof(piXYZBinCounts) );

	pArr->GetXYZBinCounts( piXYZBinCounts );

	int &z = piXYZBinCurr[2];
	int &y = piXYZBinCurr[1];
	int &x = piXYZBinCurr[0];

	z=0;
	//for( z = 0; z < piXYZBinCounts[2]; z++ )
	//{
		for( y = 0; y < piXYZBinCounts[1]; y++ )
		{
			for( x = 0; x < piXYZBinCounts[0]; x++ )
			{
				int iIndex = z*piXYZBinCounts[1]*piXYZBinCounts[0] + y*piXYZBinCounts[0] + x;

				ScaleSpaceXYZBin *pssb = pArr->GetBinFromIndex( piXYZBinCurr );
				if( pssb->size() >= 1 )
				{
					pfFreqImg[iIndex] = (float)log((float)pssb->size());
				}
				else
				{
					pfFreqImg[iIndex] = 0;
				}
			}
		}
	//}

	return 1;

}

ScaleSpaceXYZBin *
ScaleSpaceXYZHash::GetBin(
		float *pfXYZ,
		float fScale
		)
{
	if( fScale < m_fScaleMin )
	{	
		return m_paArrays[0].GetBin( pfXYZ );
	}
	if( fScale > m_fScaleMax )
	{
		return m_paArrays[m_iScaleLen-1].GetBin( pfXYZ );
	}

	float fScaleLog = log( fScale );
	int iScaleBin = (int)((fScaleLog-m_fScaleMinLog)/m_fScaleIncLog);
	return m_paArrays[iScaleBin].GetBin( pfXYZ );
}

ScaleSpaceXYZBin *
ScaleSpaceXYZHash::GetNeighborBin(
		float *pfXYZ,
		float fScale,
		int iScaleInc,
		int	*piNeighborInc 
		)
{
	int iScaleBin;

	if( fScale < m_fScaleMin )
	{
		iScaleBin = 0;
	}
	else if( fScale > m_fScaleMax )
	{
		iScaleBin = m_iScaleLen-1;
	}
	else
	{
		float fScaleLog = log( fScale );
		iScaleBin = (int)((fScaleLog-m_fScaleMinLog)/m_fScaleIncLog);
	}

	iScaleBin += iScaleInc;
	if( iScaleBin < 0 )
	{
		return 0; // No valid neighbor bin
	}
	else if( iScaleBin >= m_iScaleLen )
	{
		return 0; // No valid neighbor bin
	}

	return m_paArrays[iScaleBin].GetNeighborBin( pfXYZ, piNeighborInc );
}

int
ScaleSpaceXYZHash::Insert(
		float *pfXYZ,
		float fScale,
		int iValue
		)
{
	int iScaleBin;

	if( fScale < m_fScaleMin )
	{
		iScaleBin = 0;
	}
	else if( fScale > m_fScaleMax )
	{
		iScaleBin = m_iScaleLen-1;
	}
	else
	{
		float fScaleLog = log( fScale );
		iScaleBin = (int)((fScaleLog-m_fScaleMinLog)/m_fScaleIncLog);
	}

	return m_paArrays[iScaleBin].Add( pfXYZ, iValue );
}


int
ScaleSpaceXYZHash::EmptyAllBins(
	)
{
	for( int iScaleBin = 0; iScaleBin < m_iScaleLen; iScaleBin++ )
	{
		m_paArrays[iScaleBin].EmptyAllBins();
	}
	return 0;
}

int
ScaleSpaceXYZHash::GetScaleBinCount(
		int &iScaleBinCount
	)
{
	iScaleBinCount = m_iScaleLen;
	return iScaleBinCount;
}

int
ScaleSpaceXYZHash::GetXYZBinCounts(
		const int iScaleBin,
		int *piXYZBinCounts
	)
{
	if( iScaleBin < 0 || iScaleBin >= m_iScaleLen )
	{
		return -1;
	}
	else
	{
		return m_paArrays[iScaleBin].GetXYZBinCounts( piXYZBinCounts );
	}
}

ScaleSpaceXYZBin *
ScaleSpaceXYZHash::GetBinFromIndex(
		int iScaleBin,
		int *piXYZBins
		)
{
	if( iScaleBin < 0 || iScaleBin >= m_iScaleLen )
	{
		// Out of bounds, return 0
		return 0;
	}
	else
	{
		return m_paArrays[iScaleBin].GetBinFromIndex( piXYZBins );
	}
}

ScaleSpaceXYZBin *
ScaleSpaceXYZHash::GetNeighborBinFromIndex(
		int iScaleBin, int *piXYZBins,
		int iScaleInc, int *piNeighborInc,
		int &iScaleBinNew, int *piXYZBinsNew 
		)
{
	if( iScaleBin < 0 || iScaleBin >= m_iScaleLen )
	{
		return 0;
	}

	// Get XYZ location of bin at current scale/index
	float pfXYZ[10];
	if( m_paArrays[iScaleBin].GetXYZFromBinIndex( pfXYZ, piXYZBins ) == 0 )
	{
		return 0;
	}

	// Calculate new scale
	iScaleBinNew = iScaleBin + iScaleInc;
	if( iScaleBinNew < 0 || iScaleBinNew >= m_iScaleLen )
	{
		// Invalid new scale
		return 0;
	}

	// Get the bin location at the new scale
	m_paArrays[iScaleBinNew].GetBin( pfXYZ, piXYZBinsNew );

	// Increment by specified neighbor increment
	for( int iDim = 0; iDim < m_paArrays[iScaleBinNew].GetDim(); iDim++ )
	{
		piXYZBinsNew[iDim] += piNeighborInc[iDim];
	}
	
	// Now get neighbor bin
	return m_paArrays[iScaleBinNew].GetBinFromIndex( piXYZBinsNew );
}

int
ScaleSpaceXYZHash::Copy(
		ScaleSpaceXYZHash &pssh
	)
{
	int iScaleBins;
	GetScaleBinCount( iScaleBins );

	for( int iScaleBin = 0; iScaleBin < iScaleBins; iScaleBin++ )
	{
		int piXYZBinCounts[3];
		int piXYZBin[3];
		int &x = piXYZBin[0];
		int &y = piXYZBin[1];
		int &z = piXYZBin[2];
		GetXYZBinCounts( iScaleBin, piXYZBinCounts );

		int iZeros = 0;
		int iOnes = 0;
		int iMoreThanOnes = 0;

		for( x = 0; x < piXYZBinCounts[0]; x++ )
		{
			for( y = 0; y < piXYZBinCounts[1]; y++ )
			{
				for( z = 0; z < piXYZBinCounts[2]; z++ )
				{
					ScaleSpaceXYZBin *pssb = GetBinFromIndex( iScaleBin, piXYZBin );
					assert( pssb );
					ScaleSpaceXYZBin *pssbFrom = pssh.GetBinFromIndex( iScaleBin, piXYZBin );
					assert( pssbFrom );

					pssb->clear();

					if( pssbFrom->size() > 0 )
					{
						for( ScaleSpaceXYZBin::iterator it = pssbFrom->begin(); it != pssbFrom->end(); it++ )
						{
							int iFeat = *it;
							pssb->push_front( iFeat );
						}
					}
				}
			}
		}
	}
	return 0;
}

float
ScaleSpaceXYZHash::GetBinScaleFromIndex(
		int iScaleBin
		)
{
	float fScaleLog = iScaleBin*m_fScaleIncLog+m_fScaleMinLog;
	float fScale = exp( fScaleLog );
	return fScale;
	// iScaleBin = (int)((fScaleLog-m_fScaleMinLog)/m_fScaleIncLog);
}
