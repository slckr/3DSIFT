
#include <assert.h>
#include <memory.h>

//#include "GlobalDefs.h"
#define TRUE 1
#define FALSE 0

#include "MemoryStore.h"


MemoryStore::MemoryStore()
{
	m_iDataSize = 0;
	m_pcData = 0;
}

MemoryStore::~MemoryStore()
{
	Delete();
}

int
MemoryStore::Delete(
)
{
	if( m_iDataSize != 0 )
	{
		assert( m_pcData );
		delete [] m_pcData;
	}

	m_iDataSize = 0;
	m_pcData = 0;

	return TRUE;
}

int
MemoryStore::Reallocate(
		const int iNewDataSize
	)
{
	if( m_iDataSize != 0 )
	{
		if( iNewDataSize <= m_iDataSize )
		{
			// Enough memory already allocated
			return TRUE;
		}

		assert( m_pcData );
		delete [] m_pcData;
		m_pcData = 0;

	}
	m_pcData = new unsigned char [ iNewDataSize ];
	assert( m_pcData );

	if( !m_pcData )
	{
		return FALSE;
	}

	m_iDataSize = iNewDataSize;

	return TRUE;
}

int
MemoryStore::SetInternalData(
	const int iExternalDataSize,
	const unsigned char *pcExternalData
)
{
	assert( pcExternalData );
	assert( iExternalDataSize > 0 );
	
	if( !Reallocate( iExternalDataSize ) )
	{
		return FALSE;
	}

	memcpy( m_pcData, pcExternalData, iExternalDataSize );

	return TRUE;
}

int
MemoryStore::SetExternalData(
		const unsigned char *pcExternalData
	)
{
	Delete();
	m_pcData = (unsigned char*)pcExternalData;
	return TRUE;
}

#ifdef _DEBUG

unsigned char *
MemoryStore::GetData(
	) const
{
	return m_pcData;
}

int
MemoryStore::GetDataSize(
	) const
{
	return m_iDataSize;
}

#endif
