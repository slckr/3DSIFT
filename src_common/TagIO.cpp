
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "TagIO.h"

//
// read_tag_file()
//
// Reads and stores the coordinates inside a tag file
//
static int
read_tag_file(
			   TAGIO &tagio,
			   char *pcFileName
				)
{
	FILE *infile = fopen( pcFileName, "rt" );
	if( !infile )
	{
		return 0;
	}

	if( !tagioAllocate( tagio ) )
	{
		fclose( infile );
		return 0;
	}

	char pcLine[300];
    char *pdest;
	int  result;
	int i = 0;
	int fileCount = 0;

	while( fgets( pcLine, sizeof(pcLine), infile ) )
	{
		if( strchr( pcLine, '%') && ( fileCount == 0) )
		{
			sscanf( pcLine, "%%Volume: %s.mnc\n", tagio.img1FileName);
			pdest = strstr( tagio.img1FileName, ".mnc" );				// search and remove extension
			result = (int)(pdest - tagio.img1FileName + 1);
			tagio.img1FileName[result - 1] = '\0';
			fileCount++;
		}
		
		else if( strchr( pcLine, '%') && ( fileCount == 1) )
		{
			sscanf( pcLine, "%%Volume: %s.mnc\n", tagio.img2FileName);
			pdest = strstr( tagio.img2FileName, ".mnc" );				// search and remove extension
			result = (int)(pdest - tagio.img2FileName + 1);
			tagio.img2FileName[result - 1] = '\0';
			fileCount++;
		}

		else if( strchr( pcLine, '""' ) )
		{
			sscanf( pcLine, " %d %d %d %d %d %d \"\"\n", 
					&tagio.img1points.plvz[i].x, &tagio.img1points.plvz[i].y, &tagio.img1points.plvz[i].z, 
					&tagio.img2points.plvz[i].x, &tagio.img2points.plvz[i].y, &tagio.img2points.plvz[i].z);
			i++;
			tagio.img1points.iCount = i;
			tagio.img2points.iCount = i;
		}
	}

	fclose( infile );

	return 1;
}

//
// write_tag_file()
//
// Writes a tag file
//
static int
write_tag_file(
			   TAGIO &tagio,
			   char *pcFileName
				)
{
	FILE *outfile = fopen( pcFileName, "wt" );
	if( !outfile )
	{
		return 0;
	}

	fprintf( outfile, "MNI Tag Point File\n");
	fprintf( outfile, "Volumes = 2;\n");
	fprintf( outfile, "%cVolume: %s.mnc\n", '%', tagio.img1FileName );
	fprintf( outfile, "%cVolume: %s.mnc\n\n", '%', tagio.img2FileName );
	fprintf( outfile, "Points =\n");

	for( int i = 0; i < tagio.img1points.iCount; i++ )
	{
		LOCATION_VALUE_XYZ &lvCurrImg1 = tagio.img1points.plvz[i];
		LOCATION_VALUE_XYZ &lvCurrImg2 = tagio.img2points.plvz[i];

		// output the coordinates in the required tag file format

		fprintf( outfile, " %d %d %d", lvCurrImg1.x, lvCurrImg1.y, lvCurrImg1.z );
		fprintf( outfile, " %d %d %d", lvCurrImg2.x, lvCurrImg2.y, lvCurrImg2.z );
		
		if (i == (tagio.img1points.iCount - 1))
		{
			fprintf( outfile, " \"\";\n");		
		}
		else
		{
			fprintf( outfile, " \"\"\n");
		}
	}	

	fclose( outfile);

	return 1;
}

int
tagioAllocate(
			TAGIO &tagio
			)
{
	tagio.img1FileName = new char[301];
	tagio.img2FileName = new char[301];

	if( !tagio.img1FileName )
	{
		return 0;
	}
	if( !tagio.img2FileName )
	{
		return 0;
	}

	tagio.img1points.plvz = new LOCATION_VALUE_XYZ[MAX_POINTS];
	tagio.img2points.plvz = new LOCATION_VALUE_XYZ[MAX_POINTS];
	
	if( !tagio.img1points.plvz )
	{
		return 0;
	}
	if( !tagio.img2points.plvz )
	{
		return 0;
	}

	return 1;
}

int
tagioRead(
		TAGIO &tagio,
		char *pcName
		)
{
	char pcFileName[200];

	sprintf( pcFileName, "%s.tag", pcName );
	if( !read_tag_file( tagio, pcFileName ) )
	{
		return 0;
	}
	
	return 1;
}

int
tagioDelete(
		TAGIO &tagio
		)
{
	if( tagio.img1FileName )
	{
		delete [] tagio.img1FileName;
		tagio.img1FileName = 0;
	}
	if( tagio.img2FileName )
	{
		delete [] tagio.img2FileName;
		tagio.img2FileName = 0;
	}

	if( tagio.img1points.plvz )
	{
		delete [] tagio.img1points.plvz;
		tagio.img1points.plvz = 0;
	}
		if( tagio.img2points.plvz )
	{
		delete [] tagio.img2points.plvz;
		tagio.img2points.plvz = 0;
	}
	return 1;
}

int
tagioToMincCoord(
		TAGIO &tagio
		)
{
	for( int i = 0; i < tagio.img1points.iCount; i++ )
	{
		// change the coordinates to Minc World Coordinates
		tagio.img1points.plvz[i].x = tagio.img1points.plvz[i].x - 90;
		tagio.img1points.plvz[i].y = tagio.img1points.plvz[i].y - 126;
		tagio.img1points.plvz[i].z = tagio.img1points.plvz[i].z - 72;

		tagio.img2points.plvz[i].x = tagio.img2points.plvz[i].x - 90;
		tagio.img2points.plvz[i].y = tagio.img2points.plvz[i].y - 126;
		tagio.img2points.plvz[i].z = tagio.img2points.plvz[i].z - 72;
	}

	return 1;
}

int
tagioToMincCoord2D(
		TAGIO &tagio,
		char plane,				// which plane was sliced (y for xz-plane and so on)
		int plane_location	    // location value of sliced plane
		)
{
	assert( plane_location >= 0);
	
	switch( plane )
	{
		case 'z':
		case 'Z':
				tagioSliceXY( tagio, plane_location );
				break;
				
		case 'x':
		case 'X':
				tagioSliceYZ( tagio, plane_location );
				break;
				
		case 'y':
		case 'Y':
				tagioSliceXZ( tagio, plane_location );
				break;
				
		default:
				return 0;
	}

	return 1;
}

int
tagioSliceXY(
		TAGIO &tagio,
		int plane_location
		)
{
	for( int i = 0; i < tagio.img1points.iCount; i++ )
	{
		// change the coordinates to Minc World Coordinates
		tagio.img1points.plvz[i].x = tagio.img1points.plvz[i].x - 90;
		tagio.img1points.plvz[i].y = tagio.img1points.plvz[i].y - 126;
		tagio.img1points.plvz[i].z = plane_location - 72;

		tagio.img2points.plvz[i].x = tagio.img2points.plvz[i].x - 90;
		tagio.img2points.plvz[i].y = tagio.img2points.plvz[i].y - 126;
		tagio.img2points.plvz[i].z = plane_location - 72;
	}	

	return 1;
}

int
tagioSliceYZ(
		TAGIO &tagio,
		int plane_location
		)
{
	for( int i = 0; i < tagio.img1points.iCount; i++ )
	{
		// change the coordinates to Minc World Coordinates
		tagio.img1points.plvz[i].z = tagio.img1points.plvz[i].y - 72;
		tagio.img1points.plvz[i].y = tagio.img1points.plvz[i].x - 126;
		tagio.img1points.plvz[i].x = plane_location - 90;

		tagio.img2points.plvz[i].z = tagio.img2points.plvz[i].y - 72;
		tagio.img2points.plvz[i].y = tagio.img2points.plvz[i].x - 126;
		tagio.img2points.plvz[i].x = plane_location - 90;
	}	

	return 1;
}

int
tagioSliceXZ(
		TAGIO &tagio,
		int plane_location	
		)
{
	for( int i = 0; i < tagio.img1points.iCount; i++ )
	{
		// change the coordinates to Minc World Coordinates
		tagio.img1points.plvz[i].z = tagio.img1points.plvz[i].y - 72;
		tagio.img1points.plvz[i].x = tagio.img1points.plvz[i].x - 90;
		tagio.img1points.plvz[i].y = plane_location - 126;

		tagio.img2points.plvz[i].z = tagio.img2points.plvz[i].y - 72;
		tagio.img2points.plvz[i].x = tagio.img2points.plvz[i].x - 90;
		tagio.img2points.plvz[i].y = plane_location - 126;
	}	

	return 1;
}

int
tagioWrite(
		TAGIO &tagio,
		char *pcName
		)
{
	char pcFileName[200];

	sprintf( pcFileName, "%s.tag", pcName );
	if( !write_tag_file( tagio, pcFileName ) )
	{
		return 0;
	}

	return 1;
}

int
tagioToLVC(
		   TAGIO &tagio,
		   LOCATION_VALUE_XYZ_COLLECTION &lvcPoints
		   )
{
	for( int i = 0; i < tagio.img1points.iCount; i++ )
	{
		lvcPoints.plvza[i].plvz = new LOCATION_VALUE_XYZ[2];
		lvcPoints.plvza[i].iCount = 2;
		
		lvcPoints.plvza[i].plvz[0].x = tagio.img1points.plvz[i].x;
		lvcPoints.plvza[i].plvz[0].y = tagio.img1points.plvz[i].y;
		lvcPoints.plvza[i].plvz[0].z = tagio.img1points.plvz[i].z;

		lvcPoints.plvza[i].plvz[1].x = tagio.img2points.plvz[i].x;
		lvcPoints.plvza[i].plvz[1].y = tagio.img2points.plvz[i].y;
		lvcPoints.plvza[i].plvz[1].z = tagio.img2points.plvz[i].z;
	}

	return 1;
}
