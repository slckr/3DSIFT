/////////////////////////////////////////////////////////////////////////////
// Vectors.cpp
/////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG

#include <memory.h>
#include <math.h>

/////////////////////////////////////////////////////////////////////////////

void
VecSum(float* varray, int size, short n, float* vres)
{
memset(vres, 0, n * sizeof(float));
int i;
while (size)
	{ for (i = 0;  i < n;  i++)
		{ vres[i] += varray[i]; }
	  varray += n;
	  size--;
	}
}
/////////////////////////////////////////////////////////////////////////////

void
VecSum(float* v1, float* v2, short n, float* vres)
{
for (int i = 0;  i < n;  i++)
	{ vres[i] = v1[i] + v2[i]; }
}
/////////////////////////////////////////////////////////////////////////////

void
VecSum(float* v1, float* v2, short n)
{
for (int i = 0;  i < n;  i++)
	{ v1[i] += v2[i]; }
}
/////////////////////////////////////////////////////////////////////////////

void
VecSum(double* v1, float* v2, short n)
{
for (int i = 0;  i < n;  i++)
	{ v1[i] += v2[i]; }
}
/////////////////////////////////////////////////////////////////////////////

void
VecSum(double* v1, unsigned short* v2, short n)
{
for (int i = 0;  i < n;  i++)
	{ v1[i] += (v2[i] / 65535.0); }
}
void
VecSum(double* v1, unsigned char* v2, short n)
{
for (int i = 0;  i < n;  i++)
	{ v1[i] += (v2[i] / 65535.0); }
}
/////////////////////////////////////////////////////////////////////////////

void
VecSum(double* v1, double* v2, short n)
{
for (int i = 0;  i < n;  i++)
	{ v1[i] += v2[i]; }
}
/////////////////////////////////////////////////////////////////////////////

void
VecDiv(float* v, double d, short n, float* vres)
{
for (int i = 0;  i < n;  i++)
	{ vres[i] = (float)(v[i] / d); }
}
/////////////////////////////////////////////////////////////////////////////

void
VecDiv(float* v, double d, short n)
{
for (int i = 0;  i < n;  i++)
	{ v[i] = (float)(v[i] / d); }
}
/////////////////////////////////////////////////////////////////////////////

void
VecDiv(double* v, double d, short n)
{
for (int i = 0;  i < n;  i++)
	{ v[i] /= d; }
}

void
VecNorm(double* v, double d, short n)
{
	// Calculate sum of squares
	double dSumSqr = 0;
	for(int i = 0;  i < n;  i++)
	{
		dSumSqr += v[i]*v[i];
	}
	// Here, d is the desired normalization length
	double dDivisor = d / sqrt( dSumSqr );
	for( int i = 0;  i < n;  i++)
	{
		v[i] *= dDivisor;
	}
	// Check
	dSumSqr = 0;
	for( int i = 0;  i < n;  i++)
	{
		dSumSqr += v[i]*v[i];
	}
}

/////////////////////////////////////////////////////////////////////////////
/*
double
VecDist(float* v1, float* v2, short n)
{
double d, dist = 0;
for (int i = 0;  i < n;  i++)
	{ d = v1[i] - v2[i];
	  dist += (d * d);
	}
return(dist);
}*/
/////////////////////////////////////////////////////////////////////////////

double
VecDist(float* v1, float* v2, short n)
{
double d, dist = 0;
for (;  n;  n--)
	{ d = (*v1++) - (*v2++);
	  dist += (d * d);
	}
return(dist);
}

double
VecDist(float* v1, unsigned char* v2, short n)
{
double d, dist = 0;
for (;  n;  n--)
	{ d = (*v1++) - (*v2++);
	  dist += (d * d);
	}
return(dist);
}

/////////////////////////////////////////////////////////////////////////////

double
VecDist(double* v1, unsigned short* v2, short n)
{
double d, dist = 0;
for (;  n;  n--)
	{ d = (*v1++) - (*v2++ / 65535.0);
	  dist += (d * d);
	}
return(dist);
}
/////////////////////////////////////////////////////////////////////////////
/*
double
VecDist(unsigned short* v1, double* v2, short n)
{
double d, dist = 0;
for (int i = 0;  i < n;  i++)
	{ d = v1[i] - v2[i];
	  dist += (d * d);
	}
return(dist);
}*/
/////////////////////////////////////////////////////////////////////////////

double
VecDist(double* v1, double * v2, short n)
{
double d, dist = 0;
for (;  n;  n--)
	{ d = (*v1++) - (*v2++);
	  dist += (d * d);
	}
return(dist);
}

double
VecDist(unsigned short* v1, double* v2, short n)
{
double d, dist = 0;
for (;  n;  n--)
	{ d = (*v1++) - (*v2++);
	  dist += (d * d);
	}
return(dist);
}

double
VecDist(float* v1, double* v2, short n)
{
double d, dist = 0;
for (;  n;  n--)
	{ d = (*v1++) - (*v2++);
	  dist += (d * d);
	}
return(dist);
}
/////////////////////////////////////////////////////////////////////////////

void
VecMean(float* varray, int size, short n, float* vres)
{
VecSum(varray, size, n, vres);
VecDiv(vres, size, n, vres);
}
/////////////////////////////////////////////////////////////////////////////

void
VecMean(float* varray1, int size1, float* varray2, int size2, short n, float* vres)
{
float* sum1 = new float[n];
float* sum2 = new float[n];
VecSum(varray1, size1, n, sum1);
VecSum(varray2, size2, n, sum2);
VecSum(sum1, sum1, n, vres);
VecDiv(vres, size1 + size2, n, vres);
}
/////////////////////////////////////////////////////////////////////////////

double
VecMeanDist(float* varray, int size, float* vmean, short n)
{
double dist = 0;
int s = size;
while (s)
	{ dist += VecDist(varray, vmean, n);
	  varray += n;  s--;
	}
dist /= size;
return(dist);
}
/////////////////////////////////////////////////////////////////////////////

double
VecMeanDist(float* varray, int size, short n)
{
float* vmean = new float[n];
VecMean(varray, size, n, vmean);
double dist = 0;
int s = size;
while (s)
	{ dist += VecDist(varray, vmean, n);
	  varray += n;  s--;
	}
dist /= size;
return(dist);
}
/////////////////////////////////////////////////////////////////////////////

#endif