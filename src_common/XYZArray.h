

#ifndef __XYZARRAY__
#define __XYZARRAY__

#pragma warning(disable:4786)

#include <list>

using namespace std;

typedef list<int> ScaleSpaceXYZBin;

//
// An array of lists of integers over XYZ space.
//

class XYZArray
{
public:

	XYZArray(
		);

	~XYZArray(
		);

	//
	// Init()
	//
	// Initialize hash table to 
	//
	int
	Init(
		int iDim,
		float *pfMin,
		float *pfMax,
		float *pfInc
		);

	//
	// Add()
	//
	// Add a value at an xyz location.
	//
	int
	Add(
		float *pfXYZ,
		int iValue
	);

	//
	// GetBin()
	//
	// Get a scale-space bin corresponding to an XYZ coordinate.
	// Optionally, return the indices of the bin in the array.
	//
	ScaleSpaceXYZBin *
	GetBin(
		float *pfXYZ,
		int *piXYZBins = 0
	);

	//
	// GetNeighborBin()
	//
	// Return neighbor bin. Return null if out of bounds/no neighbor
	//
	ScaleSpaceXYZBin *
	GetNeighborBin(
			float *pfXYZ,
			int	*piNeighborInc // Neighbor bin
			);

	//
	// EmptyAllBins()
	//
	// Empties all bins
	//
	int
	EmptyAllBins(
	);

	//
	// GetXYZBinCounts()
	//
	// Gets the number of XYZ bins in array. For interating
	// through array.
	//
	int
	GetXYZBinCounts(
		int *piXYZBinCounts
	);

	//
	// GetBinFromIndex()
	//
	// Returns a bin based an array of indices.
	//
	ScaleSpaceXYZBin *
	GetBinFromIndex(
		int *piXYZBins
		);
	//
	// GetBinFromAbsoluteIndex()
	//
	// Returns a bin, useful for scrolling through all bins.
	//
	ScaleSpaceXYZBin *
	GetBinFromIndexAbsolute(
		int iBin
		);

	//
	// GetBinXYZFromIndex()
	//
	// Sets the XYZ coordinates in pfXYZ[] corresponding to the bin
	// specified by indices piXYZBins[].
	//
	int
	GetXYZFromBinIndex(
			float *pfXYZ,
			int	*piXYZBins
			);

	//
	// GetDim()
	//
	// Accessor functions.
	//
	int
	GetDim()	{ return m_iDim; }
	float *
	GetMin() {  return (float*)(&(m_pfMin[0]));}
	float *
	GetMax() {  return (float*)(&(m_pfMax[0]));}
	float *
	GetInc() {  return (float*)(&(m_pfInc[0]));}
	float *
	GetLen() {  return (float*)(&(m_piLen[0]));}
	int
	GetTotalBins() { return m_iTotalBins; }

public:

	int	m_iDim;

	float	m_pfMin[10];
	float	m_pfMax[10];
	float	m_pfInc[10];
	int		m_piLen[10];

	int m_iTotalBins;
	ScaleSpaceXYZBin	*m_pssb;

};

#endif 

