
//
// File: TraceLine.cpp
// Desc: Traces a line on a discrete grid.
// Dev: Copyright Matt Toews
//

#include <stdlib.h>
#include "TraceLine.h"

void
tlTraceLine(
		  int			iRowStart,		// Start Row
		  int			iColStart,		// Start Col
		  int			iRowFinish,		// Finish Row
		  int			iColFinish,		// Finish Col
		  tlCallback	*pProcess,		// Function to process pixel
		  void			*pArg			// Argument passed to FFCallback
)
{
	int iRowDiff = abs( iRowFinish - iRowStart );
	int iColDiff = abs( iColFinish - iColStart );

	int iRowInc = ( iRowFinish < iRowStart ? -1 : 1 );
	int iColInc = ( iColFinish < iColStart ? -1 : 1 );

	int iLinePoints = ( iRowDiff > iColDiff ? iRowDiff + 1 : iColDiff + 1 );

	int iRowCurr = iRowStart;
	int iColCurr = iColStart;

	if( iRowDiff > iColDiff )
	{
		int iMinorCount = iColDiff - iRowDiff/2;
		for( int i = 0; i < iLinePoints; i++ )
		{
			if( pProcess( iRowCurr, iColCurr, pArg ) == 0 )
			{
				return;
			}
			if( iMinorCount >= 0 )
			{
				iColCurr += iColInc;
				iMinorCount -= iRowDiff;
			}
			iRowCurr += iRowInc;
			iMinorCount += iColDiff;
		}
	}
	else
	{
		int iMinorCount = iRowDiff - iColDiff/2;
		for( int i = 0; i < iLinePoints; i++ )
		{
			if( pProcess( iRowCurr, iColCurr, pArg ) == 0 )
			{
				return;
			}
			if( iMinorCount >= 0 )
			{
				iRowCurr += iRowInc;
				iMinorCount -= iColDiff;
			}
			iColCurr += iColInc;
			iMinorCount += iRowDiff;
		}
	}
}

void
tlTraceLineSlope(
		  int			iRowStart,	// Start Row
		  int			iColStart,	// Start Col
		  int			iDRow,		// Row slope derivative
		  int			iDCol,		// Col slope derivative
		  tlCallback	*pProcess,	// Function to process pixel
		  void			*pArg		// Argument passed to FFCallback
)
{
	int iRowDiff = iDRow;
	int iColDiff = iDCol;

	int iRowInc = ( iRowDiff < 0 ? -1 : 1 );
	int iColInc = ( iColDiff < 0 ? -1 : 1 );

	int iLinePoints = 100000;//( iRowDiff > iColDiff ? iRowDiff + 1 : iColDiff + 1 );

	int iRowCurr = iRowStart;
	int iColCurr = iColStart;

	int iAbsRowDiff = ( iRowDiff < 0 ? -iRowDiff : iRowDiff );
	int iAbsColDiff = ( iColDiff < 0 ? -iColDiff : iColDiff );

	if( iAbsRowDiff > iAbsColDiff )
	{
		int iMinorCount = iAbsColDiff - iAbsRowDiff/2;
		iMinorCount /= 2;
		for( int i = 0; i < iLinePoints; i++ )
		{
			if( pProcess( iRowCurr, iColCurr, pArg ) == 0 )
			{
				return;
			}
			if( iMinorCount >= 0 )
			{
				iColCurr += iColInc;
				iMinorCount -= iAbsRowDiff;
			}
			iRowCurr += iRowInc;
			iMinorCount += iAbsColDiff;
		}
	}
	else
	{
		int iMinorCount = iAbsRowDiff - iAbsColDiff/2;
		iMinorCount /= 2;
		for( int i = 0; i < iLinePoints; i++ )
		{
			if( pProcess( iRowCurr, iColCurr, pArg ) == 0 )
			{
				return;
			}
			if( iMinorCount >= 0 )
			{
				iRowCurr += iRowInc;
				iMinorCount -= iAbsColDiff;
			}
			iColCurr += iColInc;
			iMinorCount += iAbsRowDiff;
		}
	}
}
