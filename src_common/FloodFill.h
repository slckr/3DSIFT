
#ifndef __FLOODFILL_H__
#define __FLOODFILL_H__

// Function overriden by user

typedef int FFCallback( int iRow, int iCol, void *pArg );

// Floods 4 nearest neighbours

void
ffFlood2d_4neighbours(
		  int			*pStack,		// Stack big enough for all data
		  int			iSeedRow,		// Start Row
		  int			iSeedCol,		// Start Col
		  FFCallback	*pProcess,		// Function to process pixel: return whatever you want, doesn't make a difference
		  FFCallback	*pPushOnStack,	// Function to put pixel on stack: return 1 if yes, 0 if no
		  void			*pArg			// Argument passed to FFCallback
);

// Floods 8 nearest neighbours

void
ffFlood2d_8neighbours(
		  int			*pStack,		// Stack big enough for all data
		  int			iSeedRow,		// Start Row
		  int			iSeedCol,		// Start Col
		  FFCallback	*pProcess,		// Function to process pixel: return whatever you want, doesn't make a difference
		  FFCallback	*pPushOnStack,	// Function to put pixel on stack: return 1 if yes, 0 if no
		  void			*pArg			// Argument passed to FFCallback
);

#endif
