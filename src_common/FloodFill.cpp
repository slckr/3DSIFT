
#include "FloodFill.h"

static void
ffPushStack(
		   int *&pStack,
		   const int &iRow,
		   const int &iCol
			)
{
	pStack[0] = iRow;
	pStack[1] = iCol;
	pStack += 2;
}

static void
ffPopStack(
		   int *&pStack,
		   int &iRow,
		   int &iCol
		   )
{
	pStack -= 2;
	iRow = pStack[0];
	iCol = pStack[1];
}

void
ffFlood2d_4neighbours(
		  int			*pStack,
		  int			iSeedRow,
		  int			iSeedCol,
		  FFCallback	*pPushOnStack,
		  FFCallback	*pProcess,
		  void			*pArg
)
{
	int *pCurrStack = pStack;
	ffPushStack( pCurrStack, iSeedRow, iSeedCol );

	while( pCurrStack != pStack )
	{
		int iCurrRow, iCurrCol;
		ffPopStack( pCurrStack, iCurrRow, iCurrCol );

		pProcess( iCurrRow, iCurrCol, pArg );

		if( pPushOnStack( iCurrRow, iCurrCol + 1, pArg ) )
		{
			ffPushStack( pCurrStack, iCurrRow, iCurrCol + 1 );
		}
		if( pPushOnStack( iCurrRow, iCurrCol - 1, pArg ) )
		{
			ffPushStack( pCurrStack, iCurrRow, iCurrCol - 1 );
		}
		if( pPushOnStack( iCurrRow + 1, iCurrCol, pArg ) )
		{
			ffPushStack( pCurrStack, iCurrRow + 1, iCurrCol );
		}
		if( pPushOnStack( iCurrRow - 1, iCurrCol, pArg ) )
		{
			ffPushStack( pCurrStack, iCurrRow - 1, iCurrCol );
		}
	}
}


void
ffFlood2d_8neighbours(
		  int			*pStack,
		  int			iSeedRow,
		  int			iSeedCol,
		  FFCallback	*pPushOnStack,
		  FFCallback	*pProcess,
		  void			*pArg
)
{
	int *pCurrStack = pStack;
	ffPushStack( pCurrStack, iSeedRow, iSeedCol );

	while( pCurrStack != pStack )
	{
		int iCurrRow, iCurrCol;
		ffPopStack( pCurrStack, iCurrRow, iCurrCol );

		if( !pProcess( iCurrRow, iCurrCol, pArg ) )
		{
			// Exit condition
			return;
		}

		if( pPushOnStack( iCurrRow - 1, iCurrCol - 1, pArg ) )
		{
			ffPushStack( pCurrStack, iCurrRow - 1, iCurrCol - 1 );
		}
		if( pPushOnStack( iCurrRow - 1, iCurrCol, pArg ) )
		{
			ffPushStack( pCurrStack, iCurrRow - 1, iCurrCol );
		}
		if( pPushOnStack( iCurrRow - 1, iCurrCol + 1, pArg ) )
		{
			ffPushStack( pCurrStack, iCurrRow - 1, iCurrCol + 1 );
		}

		if( pPushOnStack( iCurrRow, iCurrCol - 1, pArg ) )
		{
			ffPushStack( pCurrStack, iCurrRow, iCurrCol - 1 );
		}
		if( pPushOnStack( iCurrRow, iCurrCol + 1, pArg ) )
		{
			ffPushStack( pCurrStack, iCurrRow, iCurrCol + 1 );
		}

		if( pPushOnStack( iCurrRow + 1, iCurrCol - 1, pArg ) )
		{
			ffPushStack( pCurrStack, iCurrRow + 1, iCurrCol - 1 );
		}
		if( pPushOnStack( iCurrRow + 1, iCurrCol, pArg ) )
		{
			ffPushStack( pCurrStack, iCurrRow + 1, iCurrCol );
		}
		if( pPushOnStack( iCurrRow + 1, iCurrCol + 1, pArg ) )
		{
			ffPushStack( pCurrStack, iCurrRow + 1, iCurrCol + 1 );
		}
	}
}
