
#ifndef __MATRIX_H__
#define __MATRIX_H__

class matrix
{
public:
	
	matrix();
	
	matrix(
		const int &iRows,
		const int &iCols
		);
	
	~matrix();
	
	int
		Rows(
		) const;

	int
		Cols(
		) const;
	
	double **
		RowPointers(
		) const;
	
	double *
		RowPointer(
		const int &iRow
		) const;
	
	double &
		operator()(
				   const int &iRow,
				   const int &iCol
				   );
	double
		operator()(
				   const int &iRow,
				   const int &iCol
				   ) const;
	
	matrix &
		operator=(
			const matrix	&mat
				   );

	int
		multiply_scalar(
			const double &dFactor
			);

	int
		add_matrix(
			const matrix &mat
			);
	
	double
		length(
			) const;
	
	int
		writeFile(
			const char *pchFileName
			) const;

private:

	int
	delete_data(
	);

	int
	new_data(
		const int &iRows,
		const int &iCols
	);
	
private:

	double *m_pdData;
	double **m_pdDataRows;
	int	m_iRows;
	int	m_iCols;
};

#endif
