
#include "RotateRegion.h"
#include <math.h>
#include <string.h>
#include <assert.h>
#define PI 3.1415926535897932384626433832795

static float
nearest_pixel(
				  const PpImage &ppImg,
				  const float &fRow,
				  const float &fCol
				  )
{
	int iIntRow;
	int iIntCol;
	iIntRow = (int)floor( fRow );
	iIntCol = (int)floor( fCol );
	return ppImg.ImageRow( iIntRow )[ iIntCol ];
}

static float
median_pixel(
				  const PpImage &ppImg,
				  const float &fRow,
				  const float &fCol
				  )
{
	int iIntRow = (int)floor(fRow);
	int iIntCol = (int)floor(fCol);

	unsigned char *pImgRow0 = ppImg.ImageRow( iIntRow );
	unsigned char *pImgRow1 = ppImg.ImageRow( iIntRow + 1 );

	float fResultPixel =
		(
		(float)(pImgRow0[ iIntCol ] + pImgRow0[ iIntCol + 1 ]
		+ pImgRow1[ iIntCol ] + pImgRow1[ iIntCol + 1 ])
		/ 4.0f);

	return fResultPixel;
}

static float
interpolate_pixel_float(
				  const PpImage &ppImgFloat,
				  const float &fRow,
				  const float &fCol
				  )
{
	int iIntRow = (int)floor(fRow);
	int iIntCol = (int)floor(fCol);
	float fRowFact = 1.0f - (fRow - (float)floor( fRow ));
	float fColFact = 1.0f - (fCol - (float)floor( fCol ));

	float *pfImgRow0 = (float *)ppImgFloat.ImageRow( iIntRow );
	float *pfImgRow1 = (float *)ppImgFloat.ImageRow( iIntRow + 1 );

	float fResultPixel = 
		(      fRowFact)*( (fColFact)*(pfImgRow0[ iIntCol ]) + (1.0f - fColFact)*(pfImgRow0[ iIntCol + 1 ]) )
		+
		(1.0f - fRowFact)*( (fColFact)*(pfImgRow1[ iIntCol ]) + (1.0f - fColFact)*(pfImgRow1[ iIntCol + 1 ]) );

	return fResultPixel;
}

static float
interpolate_pixel(
				  const PpImage &ppImg,
				  const float &fRow,
				  const float &fCol
				  )
{
	int iIntRow = (int)floor(fRow);
	int iIntCol = (int)floor(fCol);
	float fRowFact = 1.0f - (fRow - (float)floor( fRow ));
	float fColFact = 1.0f - (fCol - (float)floor( fCol ));

	unsigned char *pImgRow0 = ppImg.ImageRow( iIntRow );
	unsigned char *pImgRow1 = ppImg.ImageRow( iIntRow + 1 );

	float fResultPixel = 
		(      fRowFact)*( (fColFact)*(pImgRow0[ iIntCol ]) + (1.0f - fColFact)*(pImgRow0[ iIntCol + 1 ]) )
		+
		(1.0f - fRowFact)*( (fColFact)*(pImgRow1[ iIntCol ]) + (1.0f - fColFact)*(pImgRow1[ iIntCol + 1 ]) );

	return fResultPixel;
}

static void
interpolate_pixel_char(
				  const PpImage &ppImgRGB,
				  const float &fRow, 
				  const float &fCol,
				  unsigned char *pucRGB
				  )
{
	assert( ppImgRGB.BitsPerPixel() == 8
		||
		ppImgRGB.BitsPerPixel() == 24 );

	float fRowCont;
	float fColCont;
	
	int iRowCoord;
	int iColCoord;
	
	if( fRow < 0.5f )
	{
		iRowCoord = 0;
		fRowCont = 1.0f;
	}
	else if( fRow >= ((float)(ppImgRGB.Rows() - 0.5f)) )
	{
		iRowCoord = ppImgRGB.Rows() - 2;
		fRowCont = 0.0f;
	}
	else
	{
		float fMinusHalf = fRow - 0.5f;
		iRowCoord = (int)floor( fMinusHalf );
		fRowCont = 1.0f - (fMinusHalf - ((float)iRowCoord));
	}
	
	if( fCol < 0.5f )
	{
		iColCoord = 0;
		fColCont = 1.0f;
	}
	else if( fCol >= ((float)(ppImgRGB.Cols() - 0.5f)) )
	{
		iColCoord = ppImgRGB.Cols() - 2;
		fColCont = 0.0f;
	}
	else
	{
		float fMinusHalf = fCol - 0.5f;
		iColCoord = (int)floor( fMinusHalf );
		fColCont = 1.0f - (fMinusHalf - ((float)iColCoord));
	}

	int iFeaturesPerVector = ppImgRGB.BitsPerPixel()/8;
	for( int i = 0; i < iFeaturesPerVector; i++ )
	{
		unsigned char *pucRow0 = ppImgRGB.ImageRow(iRowCoord);
		unsigned char *pucRow1 = ppImgRGB.ImageRow(iRowCoord+1);

		float f00 = pucRow0[(iColCoord)  *iFeaturesPerVector+i];
		float f01 = pucRow0[(iColCoord+1)*iFeaturesPerVector+i];
		float f10 = pucRow1[(iColCoord)  *iFeaturesPerVector+i];
		float f11 = pucRow1[(iColCoord+1)*iFeaturesPerVector+i];

		pucRGB[i] = fRowCont*(f00*fColCont + f01*(1.0f - fColCont))
			+ (1.0f - fRowCont)*(f10*fColCont + f11*(1.0f - fColCont));
	}
}

void
rotate_region(
			  const PpImage &ppImg,
			  PpImage &ppImgOut,
			  const float &angle,	// Rotation angle
			  const float &mag_row,	// Row magnification
			  const float &mag_col,	// Col magnification
			  const int iRow,
			  const int iCol,
			  const int iRows,
			  const int iCols
			  )
{
	if( ppImgOut.Rows() != iRows || ppImgOut.Cols() != iCols )
	{
		ppImgOut.Initialize(
			iRows, iCols,
			iCols*sizeof(unsigned char), sizeof(unsigned char)*8
			);
	}

	float fCenterRow = (float)(iRow + iRows / 2);
	float fCenterCol = (float)(iCol + iCols / 2);
	float fCosAngle = (float)cos(-angle);
	float fSinAngle = (float)sin(-angle);

	for( int i = iRow; i < iRow + iRows; i++ )
	{
		float fRowPos = (((float)i) - fCenterRow) / mag_row;

		for( int j = iCol; j < iCol + iCols; j++ )
		{
			float fColPos = (((float)j) - fCenterCol) / mag_col;

			// Calcuate rotation

			float fRowSource =
				fCosAngle*fRowPos
				- fSinAngle*fColPos
				+ fCenterRow;

			float fColSource =
				fSinAngle*fRowPos
				+ fCosAngle*fColPos
				+ fCenterCol;

			// Interpolate between pixels

			float fResultPixel =
				interpolate_pixel( ppImg, fRowSource, fColSource );

			ppImgOut.ImageRow( i - iRow )[ j - iCol ] = (unsigned char)fResultPixel;

		}
	}
}

void
rotate_region_float(
			  const PpImage &ppImgFloat,
			  PpImage &ppImgOutFloat,
			  const float &angle,	// Rotation angle
			  const float &mag_row,	// Row magnification
			  const float &mag_col,	// Col magnification
			  const int iRow,
			  const int iCol,
			  const int iRows,
			  const int iCols,
			  float fCenterRow,
	          float fCenterCol
			  )
{
	if( ppImgOutFloat.Rows() != iRows || ppImgOutFloat.Cols() != iCols )
	{
		ppImgOutFloat.Initialize(
			iRows, iCols,
			iCols*sizeof(float), sizeof(float)*8
			);
	}

	assert( ppImgOutFloat.BitsPerPixel() == 32 );
	assert( ppImgFloat.BitsPerPixel() == 32 );

	if( fCenterRow == -1.0f )
	{
		fCenterRow = (float)(iRow + iRows / 2);
	}
	if( fCenterRow == -1.0f )
	{
		fCenterCol = (float)(iCol + iCols / 2);
	}

	float fCosAngle = (float)cos(-angle);
	float fSinAngle = (float)sin(-angle);

	for( int i = iRow; i < iRow + iRows; i++ )
	{
		float fRowPos = (((float)i) - fCenterRow) / mag_row;

		for( int j = iCol; j < iCol + iCols; j++ )
		{
			float fColPos = (((float)j) - fCenterCol) / mag_col;

			// Calcuate rotation

			float fRowSource =
				fCosAngle*fRowPos
				- fSinAngle*fColPos
				+ fCenterRow;

			float fColSource =
				fSinAngle*fRowPos
				+ fCosAngle*fColPos
				+ fCenterCol;

			// Interpolate between pixels

			float fResultPixel =
				interpolate_pixel_float( ppImgFloat, fRowSource, fColSource );

			((float*)ppImgOutFloat.ImageRow( i - iRow ))[ j - iCol ] = fResultPixel;

		}
	}
}


void
rotate_region_circular_float(
			  const PpImage &ppImgFloat,
			  PpImage &ppImgOutFloat,
			  const float &angle,	// Rotation angle
			  const float &mag_row,	// Row magnification
			  const float &mag_col,	// Col magnification
			  const int iCenterRow,
			  const int iCenterCol,
			  const int iRadius
			  )
{
	int iRow = iCenterRow - iRadius;
	int iRows = 2*iRadius + 1;

	int iCol = iCenterCol - iRadius;
	int iCols = 2*iRadius + 1;

	if( ppImgOutFloat.Rows() <= iRadius + 1 || ppImgOutFloat.Cols() <= iRadius + 1 )
	{
		ppImgOutFloat.Initialize(
			iRows, iCols,
			iCols*sizeof(float), sizeof(float)*8
			);
		memset( ppImgOutFloat.ImageRow(0), 0, ppImgOutFloat.Rows()*ppImgOutFloat.Cols()*sizeof(float) );
	}

	assert( ppImgOutFloat.BitsPerPixel() == 32 );
	assert( ppImgFloat.BitsPerPixel() == 32 );

	float fCenterRow = (float)(iCenterRow);
	float fCenterCol = (float)(iCenterCol);
	float fCosAngle = (float)cos(-angle);
	float fSinAngle = (float)sin(-angle);

	int iRadiusPlusOneSqr = (iRadius+1)*iRadius;

	for( int i = iRow; i < iRow + iRows; i++ )
	{
		float fRowPos = (((float)i) - fCenterRow) / mag_row;

		int iDiffRow = iCenterRow - i;

		for( int j = iCol; j < iCol + iCols; j++ )
		{
			float fColPos = (((float)j) - fCenterCol) / mag_col;
			
			int iDiffCol = iCenterCol - j;

			if( iDiffRow*iDiffRow + iDiffCol*iDiffCol >= iRadiusPlusOneSqr )
			{
				// Only copy pixels in the circular region
				continue;
			}

			// Calcuate rotation

			float fRowSource =
				fCosAngle*fRowPos
				- fSinAngle*fColPos
				+ fCenterRow;

			float fColSource =
				fSinAngle*fRowPos
				+ fCosAngle*fColPos
				+ fCenterCol;

			// Interpolate between pixels
			
			float fResultPixel = 0;

			// Pixel must be within image bounds
			if( (fRowSource >= 1 && fRowSource < ppImgFloat.Rows() - 1)
				&&
				(fColSource >= 1 && fColSource < ppImgFloat.Cols() - 1)
				)
			{

				fResultPixel =
					interpolate_pixel_float( ppImgFloat, fRowSource, fColSource );
			}

			((float*)ppImgOutFloat.ImageRow( i - iRow ))[ j - iCol ] = fResultPixel;

		}
	}
}

void
rotate_region_circular(
			  const PpImage &ppImg,	// Input image
			  PpImage &ppImgOut,	// Output image (dimensions iRows,iCols)
			  const float &angle,	// Rotation angle
			  const float &mag_row,	// Row magnification
			  const float &mag_col,	// Col magnification
			  const int iCenterRow,
			  const int iCenterCol,
			  const int iRadius
			  )
{
	int iRow = iCenterRow - iRadius;
	int iRows = 2*iRadius + 1;

	int iCol = iCenterCol - iRadius;
	int iCols = 2*iRadius + 1;

	if( ppImgOut.Rows() <= iRadius + 1 || ppImgOut.Cols() <= iRadius + 1 )
	{
		ppImgOut.Initialize(
			iRows, iCols,
			iCols*ppImg.BitsPerPixel()/8, ppImg.BitsPerPixel()
			);
		memset( ppImgOut.ImageRow(0), 0, ppImgOut.Rows()*ppImgOut.RowInc() );
	}

	int iBytesPerPixel = ppImgOut.BitsPerPixel()/8;

	assert( ppImg.BitsPerPixel() == 8 || ppImg.BitsPerPixel() == 24 );
	assert( ppImgOut.BitsPerPixel() == 8 || ppImgOut.BitsPerPixel() == 24 );

	float fCenterRow = (float)(iCenterRow);
	float fCenterCol = (float)(iCenterCol);
	float fCosAngle = (float)cos(-angle);
	float fSinAngle = (float)sin(-angle);

	int iRadiusPlusOneSqr = (iRadius+1)*iRadius;

	for( int i = iRow; i < iRow + iRows; i++ )
	{
		float fRowPos = (((float)i) - fCenterRow) / mag_row;

		int iDiffRow = iCenterRow - i;

		for( int j = iCol; j < iCol + iCols; j++ )
		{
			float fColPos = (((float)j) - fCenterCol) / mag_col;
			
			int iDiffCol = iCenterCol - j;

			if( iDiffRow*iDiffRow + iDiffCol*iDiffCol >= iRadiusPlusOneSqr )
			{
				// Only copy pixels in the circular region
				continue;
			}

			// Calcuate rotation

			float fRowSource =
				fCosAngle*fRowPos
				- fSinAngle*fColPos
				+ fCenterRow;

			float fColSource =
				fSinAngle*fRowPos
				+ fCosAngle*fColPos
				+ fCenterCol;

			// Interpolate between pixels

			float fResultPixel = 0;

			// Pixel must be within image bounds
			if( (fRowSource >= 1 && fRowSource < ppImg.Rows() - 1)
				&&
				(fColSource >= 1 && fColSource < ppImg.Cols() - 1)
				)
			{
				unsigned char *pucOutRow = ppImgOut.ImageRow( i - iRow );

				interpolate_pixel_char(
					ppImg, fRowSource, fColSource,
					pucOutRow + iBytesPerPixel*(j - iCol)
					);
			}
		}
	}
}

//
//
// Similarity transform.
//
//

//
// coordsToSimilarity()
//
// Converts two (x,y) coordinates to similarity transform parameters.
//
void
coordsToSimilarity(
				   float x1, float y1, float x2, float y2,
				   float &x, float &y, float &angle, float &mag
				   )
{
	x = (x1 + x2)*0.5;
	y = (y1 + y2)*0.5;

	float dx = x2-x1;
	float dy = y2-y1;

	mag = (float)sqrt( dx*dx + dy*dy );
	angle = (float)atan2( dy, dx );
}



void
similarity_transform_coord(
						   float x1, float y1,
						   float &x2, float &y2,
			  const float &angle,	// Rotation angle
			  const float &mag,	// Magnification
			  const float fCenterRow1,  // Center of rotation
			  const float fCenterCol1,  //     Image1
			  const float fCenterRow2,  // Center of rotation
			  const float fCenterCol2   //     Image2
			  )
{
	float fCosAngle = (float)cos(-angle);
	float fSinAngle = (float)sin(-angle);
	float fRowPos2 = (((float)y1) - fCenterRow1)*mag;
	float fColPos2 = (((float)x1) - fCenterCol1)*mag;

	float fRowSource =
		fCosAngle*fRowPos2
		- fSinAngle*fColPos2
		+ fCenterRow2;

	float fColSource =
		fSinAngle*fRowPos2
		+ fCosAngle*fColPos2
		+ fCenterCol2;
	y2 = fRowSource;
	x2 = fColSource;
}

void
similarity_transform_image(
			  const PpImage &ppImg1,	// Input image
			  PpImage &ppImg2,	// Output image (dimensions iRows,iCols)
			  const float &angle,	// Rotation angle
			  const float &mag,	// Magnification
			  const float fCenterRow1,  // Center of rotation
			  const float fCenterCol1,  //     Image1
			  const float fCenterRow2,  // Center of rotation
			  const float fCenterCol2   //     Image2
			  )
{
	assert( ppImg1.BitsPerPixel() == ppImg2.BitsPerPixel() );
	assert( ppImg1.BitsPerPixel() == 8 || ppImg1.BitsPerPixel() == 24 );
	memset( ppImg2.ImageRow(0), 0, ppImg2.Rows()*ppImg2.Cols()*ppImg1.BitsPerPixel()/8 );
	int iBytesPerPixel = ppImg1.BitsPerPixel()/8;

	float fCosAngle = (float)cos(-angle);
	float fSinAngle = (float)sin(-angle);

	unsigned char pucPix[3];

	for( int i = 0; i < ppImg2.Rows(); i++ )
	{
		float fRowPos1 = (((float)i) - fCenterRow2) / mag;

		for( int j = 0; j < ppImg2.Cols(); j++ )
		{
			float fColPos1 = (((float)j) - fCenterCol2) / mag;

			// Calcuate rotation

			float fRowSource =
				fCosAngle*fRowPos1
				- fSinAngle*fColPos1
				+ fCenterRow1;

			float fColSource =
				fSinAngle*fRowPos1
				+ fCosAngle*fColPos1
				+ fCenterCol1;

			// Interpolate between pixels
			pucPix[0] = 0; pucPix[1] = 0; pucPix[2] = 0;

			// Source pixel must be within bounds of source image.
			if( (fRowSource >= 1 && fRowSource < ppImg1.Rows() - 1)
				&&
				(fColSource >= 1 && fColSource < ppImg1.Cols() - 1)
				)
			{
				interpolate_pixel_char( ppImg1, fRowSource, fColSource, pucPix );
			}

			// Set pixels into destination image.
			for( int k = 0; k < iBytesPerPixel; k++ )
			{
				(ppImg2.ImageRow( i ))[ j*iBytesPerPixel + k ] = pucPix[k];
			}
		}
	}
}

void
similarity_transform_image_float(
			  const PpImage &ppImg1,	// Input image (float)
			  PpImage &ppImg2,	// Output image (float)
			  const float &angle,	// Rotation angle
			  const float &mag,	// Magnification
			  const float fCenterRow1,  // Center of rotation
			  const float fCenterCol1,  //     Image1
			  const float fCenterRow2,  // Center of rotation
			  const float fCenterCol2   //     Image2
			  )
{
	assert( ppImg1.BitsPerPixel() == 32 );
	assert( ppImg1.BitsPerPixel() == 32 );
	memset( ppImg2.ImageRow(0), 0, ppImg2.Rows()*ppImg2.Cols()*ppImg1.BitsPerPixel()/8 );
	int iBytesPerPixel = ppImg1.BitsPerPixel()/8;

	float fCosAngle = (float)cos(-angle);
	float fSinAngle = (float)sin(-angle);

	for( int i = 0; i < ppImg2.Rows(); i++ )
	{
		float fRowPos1 = (((float)i) - fCenterRow2) / mag;

		for( int j = 0; j < ppImg2.Cols(); j++ )
		{
			float fColPos1 = (((float)j) - fCenterCol2) / mag;

			// Calcuate rotation

			float fRowSource =
				fCosAngle*fRowPos1
				- fSinAngle*fColPos1
				+ fCenterRow1;

			float fColSource =
				fSinAngle*fRowPos1
				+ fCosAngle*fColPos1
				+ fCenterCol1;

			// Interpolate between pixels
			float fPixel = 0;

			// Source pixel must be within bounds of source image.
			if( (fRowSource >= 1 && fRowSource < ppImg1.Rows() - 1)
				&&
				(fColSource >= 1 && fColSource < ppImg1.Cols() - 1)
				)
			{
				fPixel = interpolate_pixel_float( ppImg1, fRowSource, fColSource );
			}

			((float*)(ppImg2.ImageRow( i )))[ j ] = fPixel;

		}
	}
}

//
//
// Homography transform.
//
//

inline double
dotProduct(
		   POINT3x1 v1,
		   POINT3x1 v2
		   )
{
	double res;

	res=v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2];
	return res;
}

inline void
crossProduct(
			 POINT3x1 v1,
			 POINT3x1 v2,
			POINT3x1 &res
			 ) 
{ 
		res[0]=v1[1]*v2[2]-v1[2]*v2[1];
		res[1]=v1[2]*v2[0]-v1[0]*v2[2];
		res[2]=v1[0]*v2[1]-v1[1]*v2[0];
}

inline void
scalarMultiply(
		POINT3x1 v,
		double l,
		POINT3x1 &res
	)
{
		res[0]=l*v[0];
		res[1]=l*v[1];
		res[2]=l*v[2];
}

void
matrixMultiply(
			   MATRIX3x3 m1,
			   MATRIX3x3 m2,
			   MATRIX3x3 &result
			   )
{
	int i,j,k;
	double res;


	for( int i = 0; i < 3; i++)
	{
		for(j = 0; j < 3; j++)
		{
			res = 0.0; 
			for(k = 0; k < 3; k++) 
			{
				res += (m1[i][k] * m2[k][j]);
			}
			result[i][j] = res;
		}
	}

}

int
coords_to_homography(
					 POINT3x1 i1[4],
					 POINT3x1 i2[4],
					 MATRIX3x3 &H
					 )
{
	int i,j;
	POINT3x1 cp[6],col[3],lig[3],pv[3];
	double det[6];
	MATRIX3x3 H1inv,H2,tmp;

	crossProduct(i2[2],i2[3],cp[0]);
	crossProduct(i2[0],i2[3],cp[1]);
	crossProduct(i2[2],i2[0],cp[2]);

	crossProduct(i1[2],i1[3],cp[3]);
	crossProduct(i1[0],i1[3],cp[4]);
	crossProduct(i1[2],i1[0],cp[5]);


	det[0]=dotProduct(i2[0],cp[0]);
	det[1]=dotProduct(i2[1],cp[1]);
	det[2]=dotProduct(i2[1],cp[2]);
	det[3]=dotProduct(i1[0],cp[3]);
	det[4]=dotProduct(i1[1],cp[4]);
	det[5]=dotProduct(i1[1],cp[5]);

	scalarMultiply(i2[1],det[0],col[0]);
	scalarMultiply(i2[2],det[1],col[1]);
	scalarMultiply(i2[3],det[2],col[2]);

	crossProduct(i1[2], i1[3],pv[0]);
	crossProduct(i1[3], i1[1],pv[1]);
	crossProduct(i1[1], i1[2],pv[2]);

	if ((det[0]!=0.0)&&(det[1]!=0.0)&&(det[2]!=0.0)\
		&&(det[3]!=0.0)&&(det[4]!=0.0)&&(det[5]!=0.0))
	{
		// Non-degenerate case
		scalarMultiply(pv[0],1.0/det[3],lig[0]);
		scalarMultiply(pv[1],1.0/det[4],lig[1]);
		scalarMultiply(pv[2],1.0/det[5],lig[2]);


		H1inv[0][0]=lig[0][0];
		H1inv[0][1]=lig[0][1];
		H1inv[0][2]=lig[0][2];

		H1inv[1][0]=lig[1][0];
		H1inv[1][1]=lig[1][1];
		H1inv[1][2]=lig[1][2];

		H1inv[2][0]=lig[2][0];
		H1inv[2][1]=lig[2][1];
		H1inv[2][2]=lig[2][2];

		H2[0][0]=col[0][0];
		H2[1][0]=col[0][1];
		H2[2][0]=col[0][2];

		H2[0][1]=col[1][0];
		H2[1][1]=col[1][1];
		H2[2][1]=col[1][2];

		H2[0][2]=col[2][0];
		H2[1][2]=col[2][1];
		H2[2][2]=col[2][2];


		matrixMultiply(H2,H1inv,tmp);

		// Normalize...

		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
				H[i][j]=(tmp[i][j]/tmp[2][2]);
		}
		return 1;
	}
	else 
	{ 
		// Degenerate case: return Id matrix.
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++) H[i][j]=((i==j)? 1.0 : 0.0);
		}
		return 0;
	}  
}

void
homography_transform_point(
			MATRIX3x3 H,
			double x, double y,
			double &xout, double &yout
			)
{
	double xx,yy,zz;

	xx=H[0][0]*x+H[0][1]*y+H[0][2];
	yy=H[1][0]*x+H[1][1]*y+H[1][2];
	zz=H[2][0]*x+H[2][1]*y+H[2][2];

	xout = xx/zz;
	yout = yy/zz;
}

void
homography_transform_image(
						   const PpImage &ppImg1,	// Input image
						   PpImage &ppImg2,	// Output image
						   MATRIX3x3 &H
						   )
{
	assert( ppImg1.BitsPerPixel() == ppImg2.BitsPerPixel() );
	assert( ppImg1.BitsPerPixel() == 8 || ppImg1.BitsPerPixel() == 24 );
	//memset( ppImg2.ImageRow(0), 0, ppImg2.Rows()*ppImg2.Cols()*ppImg1.BitsPerPixel()/8 );
	int iBytesPerPixel = ppImg1.BitsPerPixel()/8;

	unsigned char pucPix[3];

	for( int i = 0; i < ppImg2.Rows(); i++ )
	{
		for( int j = 0; j < ppImg2.Cols(); j++ )
		{
			// Calculate source pixel location
			double dCol;
			double dRow;
            homography_transform_point(
				H, (double)j, (double)i,
				dCol, dRow   );
			float fRowSource = (float)dRow;
			float fColSource = (float)dCol;

			// Interpolate between pixels
			pucPix[0] = 0; pucPix[1] = 0; pucPix[2] = 0;

			// Source pixel must be within bounds of source image.
			if( (fRowSource >= 1 && fRowSource < ppImg1.Rows() - 1)
				&&
				(fColSource >= 1 && fColSource < ppImg1.Cols() - 1)
				)
			{
				interpolate_pixel_char( ppImg1, fRowSource, fColSource, pucPix );
				// Set pixels into destination image.
				for( int k = 0; k < iBytesPerPixel; k++ )
				{
					(ppImg2.ImageRow( i ))[ j*iBytesPerPixel + k ] = pucPix[k];
				}
			}

		}
	}
}

void
rotate_region_circular(
			  const PpImage &ppImg,	// Input image
			  PpImage &ppImgOut,	// Output image (dimensions iRows,iCols)
			  const float &angle,	// Rotation angle
			  const float &mag_row,	// Row magnification
			  const float &mag_col,	// Col magnification
			  const int iCenterRow,
			  const int iCenterCol,
			  const int iRadius,
			  // This is the center of the output image ppImgOut
			  const int iOutCenterRow,
			  const int iOutCenterCol
			  )
{
	int iRow = iCenterRow - iRadius;
	int iRows = 2*iRadius + 1;

	int iCol = iCenterCol - iRadius;
	int iCols = 2*iRadius + 1;

	if( ppImgOut.Rows() <= iRadius + 1 || ppImgOut.Cols() <= iRadius + 1 )
	{
		ppImgOut.Initialize(
			iRows, iCols,
			iCols*ppImg.BitsPerPixel()/8, ppImg.BitsPerPixel()
			);
		memset( ppImgOut.ImageRow(0), 0, ppImgOut.Rows()*ppImgOut.RowInc() );
	}

	int iBytesPerPixel = ppImgOut.BitsPerPixel()/8;

	assert( ppImg.BitsPerPixel() == 8 || ppImg.BitsPerPixel() == 24 );
	assert( ppImgOut.BitsPerPixel() == 8 || ppImgOut.BitsPerPixel() == 24 );

	float fCenterRow = (float)(iCenterRow);
	float fCenterCol = (float)(iCenterCol);
	float fCosAngle = (float)cos(-angle);
	float fSinAngle = (float)sin(-angle);

	int iRadiusPlusOneSqr = (iRadius+1)*iRadius;

	for( int i = iRow; i < iRow + iRows; i++ )
	{
		float fRowPos = (((float)i) - fCenterRow) / mag_row;

		int iDiffRow = iCenterRow - i;

		for( int j = iCol; j < iCol + iCols; j++ )
		{
			float fColPos = (((float)j) - fCenterCol) / mag_col;
			
			int iDiffCol = iCenterCol - j;

			if( iDiffRow*iDiffRow + iDiffCol*iDiffCol >= iRadiusPlusOneSqr )
			{
				// Only copy pixels in the circular region
				continue;
			}

			// Calcuate rotation

			float fRowSource =
				fCosAngle*fRowPos
				- fSinAngle*fColPos
				+ fCenterRow;

			float fColSource =
				fSinAngle*fRowPos
				+ fCosAngle*fColPos
				+ fCenterCol;

			// Interpolate between pixels

			float fResultPixel = 0;

			// Pixel must be within image bounds
			if( (fRowSource >= 1 && fRowSource < ppImg.Rows() - 1)
				&&
				(fColSource >= 1 && fColSource < ppImg.Cols() - 1)
				&&
				(i - iRow + iOutCenterRow >= 1 && i - iRow + iOutCenterRow < ppImgOut.Rows() - 1)
				&&
				(j - iCol + iOutCenterCol >= 1 && j - iCol + iOutCenterCol < ppImgOut.Cols() - 1)
				)
			{
				unsigned char *pucOutRow = ppImgOut.ImageRow( i - iRow + iOutCenterRow );

				interpolate_pixel_char(
					ppImg, fRowSource, fColSource,
					pucOutRow + iBytesPerPixel*(j - iCol + iOutCenterCol )
					);
			}
		}
	}
}

void
rotate_region_circular_float_coord(
			  const PpImage &ppImg,	// Input image
			  PpImage &ppImgOut,	// Output image (dimensions iRows,iCols)
			  const float &angle,	// Rotation angle
			  const float &mag_row,	// Row magnification
			  const float &mag_col,	// Col magnification
			  const float fCenterRow,
			  const float fCenterCol,
			  // This is the center of the output image ppImgOut
			  const int iRadius,
			  const int iOutCenterRow,
			  const int iOutCenterCol
			  )
{
	int iRows = 2*iRadius + 1;
	int iCols = 2*iRadius + 1;

	if( ppImgOut.Rows() <= iRadius + 1 || ppImgOut.Cols() <= iRadius + 1 )
	{
		ppImgOut.Initialize(
			iRows, iCols,
			iCols*ppImg.BitsPerPixel()/8, ppImg.BitsPerPixel()
			);
		memset( ppImgOut.ImageRow(0), 0, ppImgOut.Rows()*ppImgOut.RowInc() );
	}

	int iBytesPerPixel = ppImgOut.BitsPerPixel()/8;

	assert( ppImg.BitsPerPixel() == 8 || ppImg.BitsPerPixel() == 24 );
	assert( ppImgOut.BitsPerPixel() == 8 || ppImgOut.BitsPerPixel() == 24 );

	float fCosAngle = (float)cos(-angle);
	float fSinAngle = (float)sin(-angle);

	int iRadiusPlusOneSqr = (iRadius+1)*iRadius;

	for( int i = 0; i < iRows; i++ )
	{
		float fRowPos = (float)(i-iRadius) / mag_row;

		int iDiffRow = i - iRadius;

		for( int j = 0; j < iCols; j++ )
		{
			float fColPos = (float)(j-iRadius) / mag_col;

			int iDiffCol = j - iRadius;

			if( iDiffRow*iDiffRow + iDiffCol*iDiffCol >= iRadiusPlusOneSqr )
			{
				// Only copy pixels in the circular region
				continue;
			}

			// Calcuate rotation

			float fRowSource =
				fCosAngle*fRowPos
				- fSinAngle*fColPos
				+ fCenterRow;

			float fColSource =
				fSinAngle*fRowPos
				+ fCosAngle*fColPos
				+ fCenterCol;

			// Interpolate between pixels

			float fResultPixel = 0;

			int iOutRow = iOutCenterRow-iRadius+i;
			int iOutCol = iOutCenterCol-iRadius+j;

			// Pixel must be within image bounds
			if( (fRowSource >= 1 && fRowSource < ppImg.Rows() - 1)
				&&
				(fColSource >= 1 && fColSource < ppImg.Cols() - 1)
				&&
				(iOutRow >= 0 && iOutRow < ppImgOut.Rows())
				&&
				(iOutCol >= 0 && iOutCol < ppImgOut.Cols())
				)
			{
				unsigned char *pucOutRow = ppImgOut.ImageRow( iOutRow );

				interpolate_pixel_char(
					ppImg, fRowSource, fColSource,
					pucOutRow + iBytesPerPixel*iOutCol
					);
			}
		}
	}
}
