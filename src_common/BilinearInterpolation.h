
#ifndef __BILINEARINTERPOLATION_H__
#define __BILINEARINTERPOLATION_H__

#include "PpImage.h"

//
// bilinear_interpolation_float()
//
// Performs bilinear interpolation on an image of float vectors
//
int
bilinear_interpolation_float(
							 const PpImage &ppImgFloat,
							 const float &fRow, 
							 const float &fCol,
							 float *pfVector			// Return vector
							 );

//
// bilinear_interpolation_char()
//
// Performs bilinear interpolation on an image of char.
//
float
bilinear_interpolation_char(
							const PpImage &ppImgChar,
							const float &fRow, 
							const float &fCol
							);



//
// bilinear_interpolation_char()
//
// Performs bilinear interpolation on an image of RGB characters
//
int
bilinear_interpolation_RGB(
							const GenericImage &ppImgChar,
							const float &fRow, 
							const float &fCol,
							unsigned char *pucRGB
							);

//
// bilinear_interpolation_paint_float()
//
// Paints a pixel on an image using bilinear interpolation.
//
int
bilinear_interpolation_paint_float(
								   PpImage &ppImgFloat,
								   const float &fValue,
								   const float &fRow,
								   const float &fCol
								   );

#endif
