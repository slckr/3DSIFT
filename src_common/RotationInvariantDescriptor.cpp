
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include "RotationInvariantDescriptor.h"

#define PI 3.1415926536

RotationInvariantDescriptor::RotationInvariantDescriptor(
	)
{
	m_pfBins[0] = 0;
	m_pfBins[1] = 0;
}

RotationInvariantDescriptor::~RotationInvariantDescriptor(
	)
{
}

int
RotationInvariantDescriptor::InitBins(
		float *pfBins
		)
{
	if( pfBins )
	{
		m_pfBins[0] = pfBins;
		m_pfBins[1] = pfBins + 64;
		for( int i = 0; i < 64; i++ )
		{
			m_pfBins[0][i] = 0;
			m_pfBins[1][i] = 0;
		}
	}
	else
	{
		m_pfBins[0] = 0;
		m_pfBins[1] = 0;
	}
	return 0;
}

float *
RotationInvariantDescriptor::GetBins(
	)
{
	return m_pfBins[0];
}

int
RotationInvariantDescriptor::AddVote(
		float fOri, // Orientation, 0..2PI
		float fMagRadial, // Magnitude in radial direction
		float fMagCircular  // Magnitude in circular direction
	)
{
	// Find radius bin
	int iRadBin = (fOri/(2.0f*PI))*64;
	assert( iRadBin >= 0 );
	if( iRadBin >= 64 )
	{
		iRadBin = 63;
	}

	// Add votes
	m_pfBins[0][iRadBin] += fMagRadial;
	m_pfBins[1][iRadBin] += fMagCircular;

	return 0;
}

int
RotationInvariantDescriptor::OutputToTextFile(
	char *pcFileName
	)
{
	FILE *outfile = fopen( pcFileName, "wt" );
	if( !outfile )
	{
		return -1;
	}

	int iCurrSize = 0;
	for( int i = 0; i < 2; i++ )
	{
		for( int iBin = 0; iBin < 64; iBin++ )
		{
			fprintf( outfile, "%f\t", m_pfBins[i][iBin] );
		}
		fprintf( outfile, "\n" );
	}

	fclose( outfile );
	return 0;
}

int
RotationInvariantDescriptor::OutputToImage(
		PpImage &ppImg
	)
{
	ppImg.Initialize( 8, 8, sizeof(float)*8, sizeof(float)*8 );
	memset( ppImg.ImageRow(0), 0, ppImg.Rows()*ppImg.RowInc() );
	int iCurrSize = 0;
	for( int i = 0; i < 2; i++ )
	{
		iCurrSize += (1<<i);
		float *pfImgRow = (float*)ppImg.ImageRow(i);
		for( int iBin = 0; iBin < 64; iBin++ )
		{
			pfImgRow[iBin] = m_pfBins[i][iBin];
		}
	}
	return 0;
}
