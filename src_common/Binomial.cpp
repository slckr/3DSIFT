
#include <assert.h>
#include <math.h>
#include "Binomial.h"

double
factorial_150(
		  int iN
		  )
{
	assert( iN <= 150 ); // Does not work for numbers greater than 150 - use a Sterling approximation
	double dFact = 1;
	for( int i = 1; i <= iN; i++ )
	{
		dFact *= i;
	}
	return dFact;
}

double
combination_150(
			int iN,
			int iK
			)
{
	assert( iK >= 0 );
	assert( iK <= iN );
	assert( iN <= 150 ); // Does not work for numbers greater than 150 - use a Sterling approximation
	double dFactN = factorial_150( iN );
	double dFactK = factorial_150( iK );
	double dFactN_K = factorial_150( iN-iK );
	return (dFactN/(dFactK*dFactN_K));
}

double
cumm_binomial_p_value(
				 double dP0,	// Prior probability of 0
				 double dP1,  // Prior probability of 1
				 int iD0,    // Data samples of 0
				 int iD1		// Data samples of 1
				 )
{
	int iN = iD0+iD1;
	assert( iD0 >= 0 );
	assert( iD1 >= 0 );
	assert( iN > 0 );

	// Data probability
	double dDP0 = (double)iD0 / (double)iN;
	double dDP1 = (double)iD1 / (double)iN;

	double dPValue = 0;

	if( dDP0 < dP0 )
	{
		// Data probability of event O less than expected
		for( int iK = 0; iK <= iD0; iK++ )
		{
			double da2K   = pow( dP0, iK );
			double db2N_K = pow( dP1, iN - iK );
			double dCoeff = combination_150( iN, iK );
			double dProb  = da2K*db2N_K*dCoeff;
			dPValue += dProb;
		}
	}
	else
	{
		// Data probability of event 0 greater than expected
		for( int iK = iN; iK >= iD0; iK-- )
		{
			double da2K   = pow( dP0, iK );
			double db2N_K = pow( dP1, iN - iK );
			double dCoeff = combination_150( iN, iK );
			double dProb  = da2K*db2N_K*dCoeff;
			dPValue += dProb;
		}
	}

	return dPValue;
}

