/////////////////////////////////////////////////////////////////////////////
// Vectors.h
/////////////////////////////////////////////////////////////////////////////

#ifndef __VECTORS_H__
#define __VECTORS_H__

#include <math.h>

void VecSum(float* varray, int size, short n, float* vres);

void VecSum(float* v1, float* v2, short n);

void VecSum(double* v1, float* v2, short n);

void VecSum(double* v1, unsigned short* v2, short n);

void VecSum(double* v1, unsigned char* v2, short n);

void VecSum(double* v1, double* v2, short n);

void VecSum(float* v1, float* v2, short n, float* vres);

void VecMean(float* varray1, int size1, float* varray2, int size2, short n, float* vres);

void VecDiv(float* v, double d, short n, float* vres);

void VecDiv(float* v, double d, short n);

void VecDiv(double* v, double d, short n);

void VecNorm(double* v, double d, short n);

double VecDist(float* v1, float* v2, short n);

double VecDist(unsigned short* v1, double* v2, short n);

double VecDist(float* v1, double* v2, short n);

double VecDist(double* v1, unsigned short* v2, short n);

double VecDist(float* v1, unsigned char* v2, short n);

double VecDist(double* v1, double * v2, short n);

void VecMean(float* varray, int size, short n, float* vres);

double VecMeanDist(float* varray, int size, float* vmean, short n);

double VecMeanDist(float* varray, int size, short n);

#ifndef _DEBUG

#include <memory.h>

/////////////////////////////////////////////////////////////////////////////

inline void
VecSum(float* varray, int size, short n, float* vres)
{
memset(vres, 0, n * sizeof(float));
int i;
while (size)
	{ for (i = 0;  i < n;  i++)
		{ vres[i] += varray[i]; }
	  varray += n;
	  size--;
	}
}
/////////////////////////////////////////////////////////////////////////////

inline void
VecSum(float* v1, float* v2, short n, float* vres)
{
for (int i = 0;  i < n;  i++)
	{ vres[i] = v1[i] + v2[i]; }
}
/////////////////////////////////////////////////////////////////////////////

inline void
VecSum(float* v1, float* v2, short n)
{
for (int i = 0;  i < n;  i++)
	{ v1[i] += v2[i]; }
}
/////////////////////////////////////////////////////////////////////////////

inline void
VecSum(double* v1, float* v2, short n)
{
for (int i = 0;  i < n;  i++)
	{ v1[i] += v2[i]; }
}
/////////////////////////////////////////////////////////////////////////////

inline void
VecSum(double* v1, unsigned short* v2, short n)
{
for (int i = 0;  i < n;  i++)
	{ v1[i] += (v2[i] / 65535.0); }
}

inline void
VecSum(double* v1, unsigned char* v2, short n)
{
for (int i = 0;  i < n;  i++)
	{ v1[i] += (v2[i] / 65535.0); }
}
/////////////////////////////////////////////////////////////////////////////

inline void
VecSum(double* v1, double* v2, short n)
{
for (int i = 0;  i < n;  i++)
	{ v1[i] += v2[i]; }
}
/////////////////////////////////////////////////////////////////////////////

inline void
VecDiv(float* v, double d, short n, float* vres)
{
for (int i = 0;  i < n;  i++)
	{ vres[i] = (float)(v[i] / d); }
}
/////////////////////////////////////////////////////////////////////////////

inline void
VecDiv(float* v, double d, short n)
{
for (int i = 0;  i < n;  i++)
	{ v[i] = (float)(v[i] / d); }
}
/////////////////////////////////////////////////////////////////////////////

inline void
VecDiv(double* v, double d, short n)
{
for (int i = 0;  i < n;  i++)
	{ v[i] /= d; }
}

inline void
VecNorm(double* v, double d, short n)
{
	// Calculate sum of squares
	double dSumSqr = 0;
	for(int i = 0;  i < n;  i++)
	{
		dSumSqr += v[i]*v[i];
	}
	// Here, d is the desired normalization length
	double dDivisor = d / sqrt( dSumSqr );
	for( int i = 0;  i < n;  i++)
	{
		v[i] *= dDivisor;
	}
	// Check
	dSumSqr = 0;
	for( int i = 0;  i < n;  i++)
	{
		dSumSqr += v[i]*v[i];
	}
}
/////////////////////////////////////////////////////////////////////////////
/*
inline double
VecDist(float* v1, float* v2, short n)
{
double d, dist = 0;
for (int i = 0;  i < n;  i++)
	{ d = v1[i] - v2[i];
	  dist += (d * d);
	}
return(dist);
}*/
/////////////////////////////////////////////////////////////////////////////

inline double
VecDist(float* v1, float* v2, short n)
{
double d, dist = 0;
for (;  n;  n--)
	{ d = (*v1++) - (*v2++);
	  dist += (d * d);
	}
return(dist);
}
/////////////////////////////////////////////////////////////////////////////

inline double
VecDist(double* v1, unsigned short* v2, short n)
{
double d, dist = 0;
for (;  n;  n--)
	{ d = (*v1++) - (*v2++ / 65535.0);
	  dist += (d * d);
	}
return(dist);
}

inline double
VecDist(float* v1, unsigned char* v2, short n)
{
double d, dist = 0;
for (;  n;  n--)
	{ d = (*v1++) - (*v2++);
	  dist += (d * d);
	}
return(dist);
}

/////////////////////////////////////////////////////////////////////////////
/*
inline double
VecDist(unsigned short* v1, double* v2, short n)
{
double d, dist = 0;
for (int i = 0;  i < n;  i++)
	{ d = v1[i] - v2[i];
	  dist += (d * d);
	}
return(dist);
}*/
/////////////////////////////////////////////////////////////////////////////

inline double
VecDist(double* v1, double * v2, short n)
{
double d, dist = 0;
for (;  n;  n--)
	{ d = (*v1++) - (*v2++);
	  dist += (d * d);
	}
return(dist);
}

inline double
VecDist(unsigned short* v1, double* v2, short n)
{
double d, dist = 0;
for (;  n;  n--)
	{ d = (*v1++) - (*v2++);
	  dist += (d * d);
	}
return(dist);
}

inline double
VecDist(float* v1, double* v2, short n)
{
double d, dist = 0;
for (;  n;  n--)
	{ d = (*v1++) - (*v2++);
	  dist += (d * d);
	}
return(dist);
}
/////////////////////////////////////////////////////////////////////////////

inline void
VecMean(float* varray, int size, short n, float* vres)
{
VecSum(varray, size, n, vres);
VecDiv(vres, size, n, vres);
}
/////////////////////////////////////////////////////////////////////////////

inline void
VecMean(float* varray1, int size1, float* varray2, int size2, short n, float* vres)
{
float* sum1 = new float[n];
float* sum2 = new float[n];
VecSum(varray1, size1, n, sum1);
VecSum(varray2, size2, n, sum2);
VecSum(sum1, sum1, n, vres);
VecDiv(vres, size1 + size2, n, vres);
}
/////////////////////////////////////////////////////////////////////////////

inline double
VecMeanDist(float* varray, int size, float* vmean, short n)
{
double dist = 0;
int s = size;
while (s)
	{ dist += VecDist(varray, vmean, n);
	  varray += n;  s--;
	}
dist /= size;
return(dist);
}
/////////////////////////////////////////////////////////////////////////////

inline double
VecMeanDist(float* varray, int size, short n)
{
float* vmean = new float[n];
VecMean(varray, size, n, vmean);
double dist = 0;
int s = size;
while (s)
	{ dist += VecDist(varray, vmean, n);
	  varray += n;  s--;
	}
dist /= size;
return(dist);
}
/////////////////////////////////////////////////////////////////////////////



#endif	// #ifndef _DEBUG


#endif	// #ifndef __VECTORS_H__
