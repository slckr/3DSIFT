
//
// File: TraceCircle.cpp
// Desc: Traces a circle on a discrete grid.
// Dev: Copyright Matt Toews
//

#include <stdlib.h>
#include "TraceCircle.h"


//
//
//  tcTraceCircle()
//
// Traces a circle from the 8 symetric faces. Here is the current
// order of drawing:
//
//                         |
//                    (4)  | (3) 
//                         |
//                         |
//                         |
//                         |
//                         |
//                         |
//                         |
//                         |
//                         |
//                         |
//    (8)                  |                (7)
//                         | 
//  --------------------------------------------> Col
//                         |
//    (6)                  |                (5)
//                         |
//                         |
//                         |
//                         |
//                         |
//                         |
//                         |
//                         |
//                    (2)  |  (1)
//                         |
//                         V
//                        Row
// 
//

void
tcTraceCircle(
			  float fRowCenter,		// Circle center row
			  float fColCenter,		// Circle center col
			  float fRadius,		// Circle radius
			  tcCallback	*pProcess,		// Function to process pixel
			  void			*pArg			// Argument passed to tcCallback
			  )
{
	int iColCurr = 0;
	int iRowCurr = fRadius;
	int g = 3 - 2*fRadius;
	int diagonalInc = 10 - 4*fRadius;
	int rightInc = 6;

	while( iColCurr <= iRowCurr )
	{
		// Paint the 8 symetric faces faces

		pProcess( fRowCenter - iRowCurr, fColCenter - iColCurr, pArg ); //(4)
		pProcess( fRowCenter - iRowCurr, fColCenter + iColCurr, pArg ); //(3)
		
		pProcess( fRowCenter - iColCurr, fColCenter - iRowCurr, pArg ); //(8)
		pProcess( fRowCenter - iColCurr, fColCenter + iRowCurr, pArg ); //(7)

		pProcess( fRowCenter + iColCurr, fColCenter - iRowCurr, pArg ); //(6)
		pProcess( fRowCenter + iColCurr, fColCenter + iRowCurr, pArg ); //(5)

		pProcess( fRowCenter + iRowCurr, fColCenter - iColCurr, pArg ); //(2)
		pProcess( fRowCenter + iRowCurr, fColCenter + iColCurr, pArg ); //(1)


		if( g >= 0 )
		{
			g += diagonalInc; 
			diagonalInc += 8;
			iRowCurr -= 1;
		}
		else
		{
			g += rightInc;
			diagonalInc += 4;
		}
		rightInc += 4;
		iColCurr += 1;
	}

	return;
}

