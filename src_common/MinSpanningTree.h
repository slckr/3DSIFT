
#ifndef __MINSPANNINGTREE_H__
#define __MINSPANNINGTREE_H__

typedef int INDEXPAIR[2];

typedef struct _FOREST
{
	// Array of indicies to TREE structures
	int	*piTreeIndicies;

	// Currently used number of indicies to TREE structures in piTreeIndicies
	int	iTreeIndexCountUsed;

	// Max number of indicies to TREE structures in piTreeIndicies
	int	iTreeIndexCountMax;
} FOREST;

//
// initForest()
//
// Initialized a FOREST structure, allocates memory.
//
int
initForest(
		   FOREST	&fF1,
		   int		iMaxTrees
		   );

//
// copyForest()
//
// Deep copy of fF2 to fF1
//
int
copyForest(
		   FOREST		&fF1,
		   const FOREST	&fF2
		   );

int
deleteForest(
		   FOREST		&fF1
		   );

typedef struct _TREE
{
	// Index to an INDEXPAIR array of edges
	int iEdgesIndex;
	// Number of INDEXPAIR structures in INDEXPAIR array
	int iEdgesCount;

	// Index to an int array of points
	int iPointsIndex;
	// Number of ints in int array
	int iPointsCount;
} TREE;

//
// FLOAT_VECTOR_DISTANCE_FUNCTION()
//
// Prototype for a user-defined function returning a floating point
// distance between two vectors. pArg can be used to pass and extra
// argument.
//
typedef float FLOAT_VECTOR_DISTANCE_FUNCTION(
											 const int iVectorLengthBytes,
											 const void *pVector1,
											 const void *pVector2,
											 const void *pArg
											 );

//
// floatVectorDistanceEuclidean()
// Example for floating point vector distance.
//
float
floatVectorDistanceEuclidean(
							 const int iVectorLengthBytes,
							 const void *pVector1,
							 const void *pVector2,
							 const void *pArg
							 );

//
// ucharVectorDistanceEuclidean()
// Example for unsigned char vector distance.
//
float
ucharVectorDistanceEuclidean(
							 const int iVectorLengthBytes,
							 const void *pVector1,
							 const void *pVector2,
							 const void *pArg
							 );

//
// FLOAT_MIN_TREE_DISTANCE_FUNCTION()
//
// Prototype for a user-defined function returning the the minimum distance
// between two trees. This function returns the trees that will be merged
// in the next step of the algorithm
//
// Indicies of the actual points resulting in the min distance are returned
// in ipBestPair.
//
typedef float FLOAT_MIN_TREE_DISTANCE_FUNCTION(
			const void	*pVectors,
			const int	&iVectorLengthBytes,
			const int	*piPointIndicies,
			const TREE	&tree1,
			const TREE	&tree2,
			void		*pArg,
			INDEXPAIR	&ipBestPair
			);

//
// computeMinSpanningTree()
//
// Computes the minumum spanning tree of a set of point vectors. All point
// vectors can be connected to each other in a complete graph.
//
// Returns the result in array pipEdges - an array of INDEXPAIR
// of size (iPointVectorCount - 1) containing index pairs of points
// in pPointVectors corresponding to edges in the minimum
// spanning tree.
//
// In this function, the distance function between two vectors floatDist 
// is specified by the user. An internal tree distance function evaluates
// tree distance by the closest two vectors between two trees.
//
int
computeMinSpanningTree(
					   void			*pPointVectors,
					   int			iPointVectorLengthBytes,
					   int			iPointVectorCount,
					   FLOAT_VECTOR_DISTANCE_FUNCTION floatDist,
					   INDEXPAIR	*pipEdges
					   );

//
// computeMinSpanningTree()
//
// Computes the minumum spanning tree of a set of point vectors. All point
// vectors can be connected to each other in a complete graph.
//
// Returns the result in array pipEdges - an array of INDEXPAIR
// of size (iPointVectorCount - 1) containing index pairs of points
// in pPointVectors corresponding to edges in the minimum
// spanning tree.
//
// In this function, the tree distance function floatMinTreeDist is 
// specified by the user. A distance function between two vectors is
// not required as above, because it is only evaluated within
// floatMinTreeDist.
//
int
computeMinSpanningTree(
					   void			*pPointVectors,
					   int			iPointVectorLengthBytes,
					   int			iPointVectorCount,
					   FLOAT_MIN_TREE_DISTANCE_FUNCTION floatMinTreeDist,
					   void			*pArg,
					   INDEXPAIR	*pipEdges
					   );

//
// mstCutLinks()
//
// Here, we go through cutting tree links to form contiguous
// clusters. All pointers must be allocated externally 
// (piPointToTreeMap
//
// This code is currently inefficient, passes through all nodes
// N times to propagate new tree labels after each cut, so it
// is O(NN) complexity at worst case.
//

typedef void MST_CUT_LINK_FUNCTION(
								   int *piPointToTreeMap,	// Array of iPointVectorCount indices
								   int iPointVectorCount,	// Number of points
								   void *pArg
								   );

int
mstCutLinks(
			void			*pPointVectors,		// Point data
			int			iPointVectorLengthBytes, // Point length in bytes
			int			iPointVectorCount,	// Number of points
			FLOAT_VECTOR_DISTANCE_FUNCTION floatDist, // Point distance function
			INDEXPAIR	*pipEdges,  // Array of iPointVectorCount edges
			int *piPointToTreeMap,	// Array of iPointVectorCount indices
							// mapping pPointVectors to a tree
			MST_CUT_LINK_FUNCTION *pfuncCutLink = 0, // Called every time a link is cut
			void *pArg = 0
			);

//
//
//
//
//
int
mstForestStats(
			   void			*pPointVectors,		// Point data
			int			iPointVectorLengthBytes, // Point length in bytes
			int			iPointVectorCount,	// Number of points
			FLOAT_VECTOR_DISTANCE_FUNCTION floatDist, // Point distance function
			INDEXPAIR	*pipEdges,  // Array of iPointVectorCount edges
			int *piPointToTreeMap	// Array of iPointVectorCount indices
							// mapping pPointVectors to a tree
			   );

#endif
