

#include "register.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#ifdef WIN32
#include <sys\timeb.h>
#endif

#include "PpImage.h"
#include "Vector.h"
#include "Transform.h"

#include "Posterior.h"
#include "LocationValue.h"

#include "output_functions.h"
#include "PpImageFloatOutput.h"

#include "FeatureIO.h"

#define PIXEL_VALUES	256
#define PI				3.1415926535897932384626433832795
#define COS30			0.86602540378443864676372317075294

//#define POSTERIOR_FUNC	posterior_generate_first_order_doughnut
#define POSTERIOR_FUNC	posterior_generate_second_order_doughnut

//FILE *g_outfile;

static int
location_xyz_dist_sqr(
				  LOCATION_VALUE_XYZ &lv1,
				  LOCATION_VALUE_XYZ &lv2
				  )
{
	int iDist = lv1.x - lv2.x;
	int iDistSqr = iDist*iDist;
	
	iDist = lv1.y - lv2.y;
	iDistSqr += iDist*iDist;
	
	iDist = lv1.z - lv2.z;
	iDistSqr += iDist*iDist;

	return iDistSqr;
}

void
regSampleToIndices(
				   int *piIndices,
				   LOCATION_VALUE_XYZ_ARRAY &lvaSample,
				   LOCATION_VALUE_XYZ_COLLECTION &lvcInstances
				   )
{
	assert( lvaSample.iCount == lvcInstances.iCount );
	for( int i = 0; i < lvaSample.iCount; i++ )
	{
		LOCATION_VALUE_XYZ &lvCurrSample = lvaSample.plvz[i];
		LOCATION_VALUE_XYZ_ARRAY &lvaCurrInstances = lvcInstances.plvza[i];

		piIndices[i] = -1;

		for( int j = 0; j < lvaCurrInstances.iCount && piIndices[i] == -1; j++ )
		{
			LOCATION_VALUE_XYZ &lvCurrInstance = lvaCurrInstances.plvz[j];
			if( lvCurrSample.x == lvCurrInstance.x
				&& lvCurrSample.y == lvCurrInstance.y
				&& lvCurrSample.z == lvCurrInstance.z )
			{
				piIndices[i] = j;
			}
		}
		assert( piIndices[i] != -1 );
	}
}

void
regIndicesToSample(
				   int *piIndices,
				   LOCATION_VALUE_XYZ_ARRAY &lvaSample,
				   LOCATION_VALUE_XYZ_COLLECTION &lvcInstances
				   )
{
	assert( lvaSample.iCount == lvcInstances.iCount );
	for( int i = 0; i < lvaSample.iCount; i++ )
	{
		assert( piIndices[i] >= 0 );
		assert( piIndices[i] < lvcInstances.plvza[i].iCount );
		lvaSample.plvz[i] = lvcInstances.plvza[i].plvz[piIndices[i]];
	}
}

//
// regFindLikelihoodPeaks()
//
// Finds likelihood peaks for a set of reference vectors. The likelihood peaks
// are returned as exponents.
//

int
regFindLikelihoodPeaks(
					   float *pfRefVectors,			// Array of reference vectors
					   LOCATION_VALUE_XYZ_COLLECTION &lvcPeaks,
					   FEATUREIO &fio
					   )
{
	// Set up neighbourhood

	int iNeighbourIndexCount;
	int piNeighbourIndices[27];

	int iZStart, iZCount;

	if( fio.z > 1 )
	{
		// 3D
		iNeighbourIndexCount = 26;
		iZStart = 1;
		iZCount = fio.z - 1;

		piNeighbourIndices[0]  = -fio.x*fio.y - fio.x - 1;
		piNeighbourIndices[1]  = -fio.x*fio.y - fio.x;
		piNeighbourIndices[2]  = -fio.x*fio.y - fio.x + 1;
		piNeighbourIndices[3]  = -fio.x*fio.y - 1;
		piNeighbourIndices[4]  = -fio.x*fio.y;
		piNeighbourIndices[5]  = -fio.x*fio.y + 1;
		piNeighbourIndices[6]  = -fio.x*fio.y + fio.x - 1;
		piNeighbourIndices[7]  = -fio.x*fio.y + fio.x;
		piNeighbourIndices[8]  = -fio.x*fio.y + fio.x + 1;
		
		piNeighbourIndices[9]  = -fio.x - 1;
		piNeighbourIndices[10] = -fio.x;
		piNeighbourIndices[11] = -fio.x + 1;
		piNeighbourIndices[12] = -1;
		piNeighbourIndices[13] =  1;
		piNeighbourIndices[14] = fio.x - 1;
		piNeighbourIndices[15] = fio.x;
		piNeighbourIndices[16] = fio.x + 1;
		
		piNeighbourIndices[17] = fio.x*fio.y - fio.x - 1;
		piNeighbourIndices[18] = fio.x*fio.y - fio.x;
		piNeighbourIndices[19] = fio.x*fio.y - fio.x + 1;
		piNeighbourIndices[20] = fio.x*fio.y - 1;
		piNeighbourIndices[21] = fio.x*fio.y;
		piNeighbourIndices[22] = fio.x*fio.y + 1;
		piNeighbourIndices[23] = fio.x*fio.y + fio.x - 1;
		piNeighbourIndices[24] = fio.x*fio.y + fio.x;
		piNeighbourIndices[25] = fio.x*fio.y + fio.x + 1;
	}
	else
	{
		// 2D
		iNeighbourIndexCount = 8;
		iZStart = 0;
		iZCount = 1;

		piNeighbourIndices[0] = -fio.x-1;
		piNeighbourIndices[1] = -fio.x;
		piNeighbourIndices[2] = -fio.x+1;
		piNeighbourIndices[3] = -1;
		piNeighbourIndices[4] =  1;
		piNeighbourIndices[5] = fio.x-1;
		piNeighbourIndices[6] = fio.x;
		piNeighbourIndices[7] = fio.x+1;
	}

	int x, y, z, r, n;

	int iRefVectors = lvcPeaks.iCount;

	// Set counts to 0
	for( r = 0; r < iRefVectors; r++ )
	{
		lvcPeaks.plvza[r].iCount = 0;
	}

	for( int z = iZStart; z < iZCount; z++ )
	{
		int iZIndex = z*fio.x*fio.y;
		for( int y = 1; y < fio.y - 1; y++ )
		{
			int iYIndex = iZIndex + y*fio.x;
			for( int x = 1; x < fio.x - 1; x++ )
			{
				int iIndex = iYIndex + x;

				float *pfDomainVecCenter = fio.pfVectors + iIndex*fio.iFeaturesPerVector;

				// Loop over reference vectors, to minimize memory accesses to fio array.
				// For each reference vector, see if pfDomainVecCenter represents a peak

				for( r = 0; r < iRefVectors; r++ )
				{
					int bPeak = 1;
					float fCenterValue;
					
					//fCenterValue = vector_sqr_distance( pfDomainVecCenter, pfRefVectors + r*fio.iFeaturesPerVector, fio.iFeaturesPerVector );
					fCenterValue = vector_sqr_distance_varr( pfDomainVecCenter, pfRefVectors + r*fio.iFeaturesPerVector, fio.iFeaturesPerVector, fio.pfVarrs );

					for( int n = 0; n < iNeighbourIndexCount && bPeak; n++ )
					{
						int iNeighbourIndex = iIndex + piNeighbourIndices[n];
						float *pfDomainVecNeigh = fio.pfVectors + iNeighbourIndex*fio.iFeaturesPerVector;
						
						float fNeighValue;
						//fNeighValue = vector_sqr_distance( pfDomainVecNeigh, pfRefVectors + r*fio.iFeaturesPerVector, fio.iFeaturesPerVector );
						fNeighValue = vector_sqr_distance_varr( pfDomainVecNeigh, pfRefVectors + r*fio.iFeaturesPerVector, fio.iFeaturesPerVector, fio.pfVarrs );

						// A peak means all neighbours are futher away from reference
						// than the center value.
						bPeak &= (fNeighValue > fCenterValue);
					}

					if( bPeak )
					{	
						lvcPeaks.plvza[r].plvz[ lvcPeaks.plvza[r].iCount ].x = x;
						lvcPeaks.plvza[r].plvz[ lvcPeaks.plvza[r].iCount ].y = y;
						lvcPeaks.plvza[r].plvz[ lvcPeaks.plvza[r].iCount ].z = z;
						lvcPeaks.plvza[r].plvz[ lvcPeaks.plvza[r].iCount ].fValue =
							//(float)(-fCenterValue*PIXEL_VARR_DIV); // Likelihood exponent
							-fCenterValue; // Likelihood exponent
						lvcPeaks.plvza[r].iCount++;
					}
				}	
			}
		}
	}

	return 1;
}

//
// regFindPeaks()
//
// Finds peaks in a FEATUREIO image, returns
//

int
regFindFEATUREIOPeaks(
					  LOCATION_VALUE_XYZ_ARRAY &lvaPeaks,
					  FEATUREIO &fio,
					  CALCUATE_VALUE_FUNC *pfunkValue
					  )
{
	// Set up neighbourhood

	int iNeighbourIndexCount;
	int piNeighbourIndices[27];

	int iZStart, iZCount;

	if( fio.z > 1 )
	{
		// 3D
		iNeighbourIndexCount = 26;
		iZStart = 1;
		iZCount = fio.z - 1;

		piNeighbourIndices[0]  = -fio.x*fio.y - fio.x - 1;
		piNeighbourIndices[1]  = -fio.x*fio.y - fio.x;
		piNeighbourIndices[2]  = -fio.x*fio.y - fio.x + 1;
		piNeighbourIndices[3]  = -fio.x*fio.y - 1;
		piNeighbourIndices[4]  = -fio.x*fio.y;
		piNeighbourIndices[5]  = -fio.x*fio.y + 1;
		piNeighbourIndices[6]  = -fio.x*fio.y + fio.x - 1;
		piNeighbourIndices[7]  = -fio.x*fio.y + fio.x;
		piNeighbourIndices[8]  = -fio.x*fio.y + fio.x + 1;
		
		piNeighbourIndices[9]  = -fio.x - 1;
		piNeighbourIndices[10] = -fio.x;
		piNeighbourIndices[11] = -fio.x + 1;
		piNeighbourIndices[12] = -1;
		piNeighbourIndices[13] =  1;
		piNeighbourIndices[14] = fio.x - 1;
		piNeighbourIndices[15] = fio.x;
		piNeighbourIndices[16] = fio.x + 1;
		
		piNeighbourIndices[17] = fio.x*fio.y - fio.x - 1;
		piNeighbourIndices[18] = fio.x*fio.y - fio.x;
		piNeighbourIndices[19] = fio.x*fio.y - fio.x + 1;
		piNeighbourIndices[20] = fio.x*fio.y - 1;
		piNeighbourIndices[21] = fio.x*fio.y;
		piNeighbourIndices[22] = fio.x*fio.y + 1;
		piNeighbourIndices[23] = fio.x*fio.y + fio.x - 1;
		piNeighbourIndices[24] = fio.x*fio.y + fio.x;
		piNeighbourIndices[25] = fio.x*fio.y + fio.x + 1;
	}
	else
	{
		// 2D
		iNeighbourIndexCount = 8;
		iZStart = 0;
		iZCount = 1;

		piNeighbourIndices[0] = -fio.x-1;
		piNeighbourIndices[1] = -fio.x;
		piNeighbourIndices[2] = -fio.x+1;
		piNeighbourIndices[3] = -1;
		piNeighbourIndices[4] =  1;
		piNeighbourIndices[5] = fio.x-1;
		piNeighbourIndices[6] = fio.x;
		piNeighbourIndices[7] = fio.x+1;
	}

	int x, y, z, n;

	lvaPeaks.iCount = 0;

	for( int z = iZStart; z < iZCount; z++ )
	{
		int iZIndex = z*fio.x*fio.y;
		for( int y = 1; y < fio.y - 1; y++ )
		{
			int iYIndex = iZIndex + y*fio.x;
			for( int x = 1; x < fio.x - 1; x++ )
			{
				int iIndex = iYIndex + x;

				int bPeak = 1;
				float fCenterValue = fio.pfVectors[iIndex*fio.iFeaturesPerVector];

				for( int n = 0; n < iNeighbourIndexCount && bPeak; n++ )
				{
					int iNeighbourIndex = iIndex + piNeighbourIndices[n];
					float fNeighValue = fio.pfVectors[iNeighbourIndex*fio.iFeaturesPerVector];
					
					// A peak means all neighbours are lower than the center value
					bPeak &= (fNeighValue < fCenterValue);
				}

				if( bPeak )
				{
					if( lvaPeaks.plvz )
					{
						lvaPeaks.plvz[ lvaPeaks.iCount ].x = x;
						lvaPeaks.plvz[ lvaPeaks.iCount ].y = y;
						lvaPeaks.plvz[ lvaPeaks.iCount ].z = z;
						lvaPeaks.plvz[ lvaPeaks.iCount ].fValue = fCenterValue;
					}
					lvaPeaks.iCount++;
				}
			}
		}
	}

	return 1;
}

//
// regFindPeaks()
//
// Finds peaks in a FEATUREIO image, returns
//

int
regFindFEATUREIOValleys(
					  LOCATION_VALUE_XYZ_ARRAY &lvaPeaks,
					  FEATUREIO &fio
					  )
{
	// Set up neighbourhood

	int iNeighbourIndexCount;
	int piNeighbourIndices[27];

	int iZStart, iZCount;

	if( fio.z > 1 )
	{
		// 3D
		iNeighbourIndexCount = 26;
		iZStart = 1;
		iZCount = fio.z - 1;

		piNeighbourIndices[0]  = -fio.x*fio.y - fio.x - 1;
		piNeighbourIndices[1]  = -fio.x*fio.y - fio.x;
		piNeighbourIndices[2]  = -fio.x*fio.y - fio.x + 1;
		piNeighbourIndices[3]  = -fio.x*fio.y - 1;
		piNeighbourIndices[4]  = -fio.x*fio.y;
		piNeighbourIndices[5]  = -fio.x*fio.y + 1;
		piNeighbourIndices[6]  = -fio.x*fio.y + fio.x - 1;
		piNeighbourIndices[7]  = -fio.x*fio.y + fio.x;
		piNeighbourIndices[8]  = -fio.x*fio.y + fio.x + 1;
		
		piNeighbourIndices[9]  = -fio.x - 1;
		piNeighbourIndices[10] = -fio.x;
		piNeighbourIndices[11] = -fio.x + 1;
		piNeighbourIndices[12] = -1;
		piNeighbourIndices[13] =  1;
		piNeighbourIndices[14] = fio.x - 1;
		piNeighbourIndices[15] = fio.x;
		piNeighbourIndices[16] = fio.x + 1;
		
		piNeighbourIndices[17] = fio.x*fio.y - fio.x - 1;
		piNeighbourIndices[18] = fio.x*fio.y - fio.x;
		piNeighbourIndices[19] = fio.x*fio.y - fio.x + 1;
		piNeighbourIndices[20] = fio.x*fio.y - 1;
		piNeighbourIndices[21] = fio.x*fio.y;
		piNeighbourIndices[22] = fio.x*fio.y + 1;
		piNeighbourIndices[23] = fio.x*fio.y + fio.x - 1;
		piNeighbourIndices[24] = fio.x*fio.y + fio.x;
		piNeighbourIndices[25] = fio.x*fio.y + fio.x + 1;
	}
	else
	{
		// 2D
		iNeighbourIndexCount = 8;
		iZStart = 0;
		iZCount = 1;

		piNeighbourIndices[0] = -fio.x-1;
		piNeighbourIndices[1] = -fio.x;
		piNeighbourIndices[2] = -fio.x+1;
		piNeighbourIndices[3] = -1;
		piNeighbourIndices[4] =  1;
		piNeighbourIndices[5] = fio.x-1;
		piNeighbourIndices[6] = fio.x;
		piNeighbourIndices[7] = fio.x+1;
	}

	int x, y, z, n;

	lvaPeaks.iCount = 0;

	for( int z = iZStart; z < iZCount; z++ )
	{
		int iZIndex = z*fio.x*fio.y;
		for( int y = 1; y < fio.y - 1; y++ )
		{
			int iYIndex = iZIndex + y*fio.x;
			for( int x = 1; x < fio.x - 1; x++ )
			{
				int iIndex = iYIndex + x;

				int bPeak = 1;
				float fCenterValue = fio.pfVectors[iIndex*fio.iFeaturesPerVector];

				for( int n = 0; n < iNeighbourIndexCount && bPeak; n++ )
				{
					int iNeighbourIndex = iIndex + piNeighbourIndices[n];
					float fNeighValue = fio.pfVectors[iNeighbourIndex*fio.iFeaturesPerVector];
					
					// A valley means all neighbours are higher than the center value
					bPeak &= (fNeighValue > fCenterValue);
				}

				if( bPeak )
				{
					if( lvaPeaks.plvz )
					{
						lvaPeaks.plvz[ lvaPeaks.iCount ].x = x;
						lvaPeaks.plvz[ lvaPeaks.iCount ].y = y;
						lvaPeaks.plvz[ lvaPeaks.iCount ].z = z;
						lvaPeaks.plvz[ lvaPeaks.iCount ].fValue = fCenterValue;
					}
					lvaPeaks.iCount++;
				}
			}
		}
	}

	return 1;
}

//
// find_likelihoods()
//
// Returns likelihoods as exponents.
//
int
regFindLikelihoodPeaks(
			   FEATUREIO &fio1,
			   FEATUREIO &fio2,
			   LOCATION_VALUE_XYZ_ARRAY &lvaVariables,	// Array of points in fio1
			   LOCATION_VALUE_XYZ_COLLECTION &lvcMarginals	// Array of likelihoods in fio2
			   )
{
	assert( fio1.iFeaturesPerVector == fio2.iFeaturesPerVector );

	int r;
	float *pfRefVectors = new float[lvaVariables.iCount*fio1.iFeaturesPerVector];

	// Initialize array of reference vectors

	for( r = 0; r < lvaVariables.iCount; r++ )
	{
		int iRefVectorIndex =
			  lvaVariables.plvz[r].z*fio1.x*fio1.y
			+ lvaVariables.plvz[r].y*fio1.x
			+ lvaVariables.plvz[r].x;
		for( int j = 0; j < fio1.iFeaturesPerVector; j++ )
		{
			pfRefVectors[r*fio1.iFeaturesPerVector + j] =
				fio1.pfVectors[iRefVectorIndex*fio1.iFeaturesPerVector + j];
		}

		// How much to allocate? Should we count first?

		lvcMarginals.plvza[r].plvz =
			new LOCATION_VALUE_XYZ[(fio2.x*fio2.y*fio2.z)/9];

		lvcMarginals.plvza[r].lvzOrigin = lvaVariables.plvz[r];

		lvcMarginals.plvza[r].iCount = 0;
	}

	// Find lists of likelihood peaks - as exponents

	regFindLikelihoodPeaks( pfRefVectors, lvcMarginals, fio2 );

	// Sort peaks

	for( r = 0; r < lvaVariables.iCount; r++ )
	{
		lvSortHighLow( lvcMarginals.plvza[r] );
	}

	delete [] pfRefVectors;

	return 1;
}

int
regReadLikelihoodPeaks(
				   char *pcFileNameBase,
				   LOCATION_VALUE_XYZ_ARRAY &lvaVariables,	// Array of points in fio1
				   LOCATION_VALUE_XYZ_COLLECTION &lvcMarginals	// Array of likelihoods in fio2
				   )
{
	// Initialize array of reference vectors

	for( int r = 0; r < lvaVariables.iCount; r++ )
	{
		char pcFileName[300];
		FEATUREIO fio;
		sprintf( pcFileName, pcFileNameBase,
			lvaVariables.plvz[r].x,
			lvaVariables.plvz[r].y,
			lvaVariables.plvz[r].z );
		if( !fioRead( fio, pcFileName ) )
		{
			printf( "Could not read likelihood FEATUREIO image: %s\n", pcFileName );
			return 0;
		}
		
		for( int z = 0; z < fio.z; z++ )
		{
			int iZIndex = z*fio.x*fio.y;
			
			for( int y = 0; y < fio.y; y++ )
			{
				int iYIndex = iZIndex + y*fio.x;
				
				for( int x = 0; x < fio.x; x++ )
				{
					int iIndex = iYIndex + x;
					

					
						

					
					
					
					fio.pfVectors[iIndex] = log( fio.pfVectors[iIndex] );

					

//					((float*)ppImgOutTemp1.ImageRow(y))[x] = fioTemp1.pfVectors[iOffset + iIndex];//lv.fValue;
//					((float*)ppImgOutTemp2.ImageRow(y))[x] = fioTemp2.pfVectors[iOffset + iIndex];//lv.fValue;
//					((float*)ppImgOutTemp4.ImageRow(y))[x] = fioTemp2.pfVectors[iOffset + iIndex - 3];//lv.fValue;
//					((float*)ppImgOutTemp3.ImageRow(y))[x] = lv.fValue;
				}
			}
		}


		// How much to allocate? Should we count first?

		lvcMarginals.plvza[r].plvz =
			new LOCATION_VALUE_XYZ[(fio.x*fio.y*fio.z)/9];

		lvcMarginals.plvza[r].lvzOrigin = lvaVariables.plvz[r];

		lvcMarginals.plvza[r].iCount = 0;

		if( !regFindFEATUREIOPeaks( lvcMarginals.plvza[r], fio ) )
		{
			printf( "Error finding peaks in FEATUREIO image: %s\n", pcFileName );
			return 0;
		}
	}

	// Sort peaks

	for( int r = 0; r < lvaVariables.iCount; r++ )
	{
		lvSortHighLow( lvcMarginals.plvza[r] );
	}

	return 1;
}

int
regReadLikelihoodRegions(
				   char *pcFileNameBase,
				   LOCATION_VALUE_XYZ_ARRAY &lvaVariables,	// Array of points in fio1
				   LOCATION_VALUE_XYZ_COLLECTION &lvcMarginals,	// Array of likelihoods in fio2
				   int iXWindowSize, // X size of region around point position to keep
				   int iYWindowSize, // Y size of region around point position to keep
				   int iZWindowSize,	 // Z size of region around point position to keep
				   FEATUREIO &fioTemp1,
				   FEATUREIO &fioTemp2
				   )
{
	// Initialize array of reference vectors

	int i;

//	PpImage ppImgOutTemp1;
//	PpImage ppImgOutTemp2;
//	PpImage ppImgOutTemp3;
//	PpImage ppImgOutTemp4;
//	ppImgOutTemp1.Initialize( iYWindowSize, iXWindowSize, iXWindowSize*sizeof(float), 32 );
//	ppImgOutTemp2.Initialize( iYWindowSize, iXWindowSize, iXWindowSize*sizeof(float), 32 );
//	ppImgOutTemp3.Initialize( iYWindowSize, iXWindowSize, iXWindowSize*sizeof(float), 32 );
//	ppImgOutTemp4.Initialize( iYWindowSize, iXWindowSize, iXWindowSize*sizeof(float), 32 );

	for( int i = 0; i < lvaVariables.iCount; i++ )
	{
		char pcFileName[300];
		FEATUREIO fio;
		sprintf( pcFileName, pcFileNameBase,
			lvaVariables.plvz[i].x,
			lvaVariables.plvz[i].y,
			lvaVariables.plvz[i].z );
		if( !fioRead( fio, pcFileName ) )
		{
			printf( "Could not read likelihood FEATUREIO image: %s\n", pcFileName );
			return 0;
		}

		iXWindowSize = (fio.x > iXWindowSize ? iXWindowSize : fio.x);
		iYWindowSize = (fio.y > iYWindowSize ? iYWindowSize : fio.y);
		iZWindowSize = (fio.z > iZWindowSize ? iZWindowSize : fio.z);

		lvcMarginals.plvza[i].lvzOrigin = lvaVariables.plvz[i];
		lvcMarginals.plvza[i].plvz =
			new LOCATION_VALUE_XYZ[iXWindowSize*iYWindowSize*iZWindowSize];

		if( iXWindowSize*iYWindowSize*iZWindowSize > 2000000 )
		{
			printf( "Warning: allocating large likelihood arrays: %d\n", iXWindowSize*iYWindowSize*iZWindowSize );
		}

		lvcMarginals.plvza[i].iCount = iXWindowSize*iYWindowSize*iZWindowSize;

		int iXStart = lvaVariables.plvz[i].x - iXWindowSize/2;
		if( iXStart < 0 )
		{
			iXStart = 0;
		}
		else if( iXStart + iXWindowSize >= fio.x - 1 )
		{
			iXStart = fio.x - iXWindowSize - 1;
		}
		int iYStart = lvaVariables.plvz[i].y - iYWindowSize/2;
		if( iYStart < 0 )
		{
			iYStart = 0;
		}
		else if( iYStart + iXWindowSize >= fio.x - 1 )
		{
			iYStart = fio.x - iXWindowSize - 1;
		}

		int iOffset =
			//fio.x*fio.y*(lvaVariables.plvz[i].z - iZWindowSize/2)
			+ fio.x*(iYStart)
			+ (iXStart);

		double dSum = 0;

		for( int z = 0; z < iZWindowSize; z++ )
		{
			int iZIndex = z*fio.x*fio.y;
			int iZList = z*iXWindowSize*iYWindowSize;
			for( int y = 0; y < iYWindowSize; y++ )
			{
				int iYIndex = iZIndex + y*fio.x;
				int iYList = iZList + y*iXWindowSize;
				for( int x = 0; x < iXWindowSize; x++ )
				{
					int iIndex = iYIndex + x;
					int iListIndex = iYList + x;

					LOCATION_VALUE_XYZ &lv =
						lvcMarginals.plvza[i].plvz[iListIndex];

					lv.x = x + lvaVariables.plvz[i].x - iXWindowSize/2;
					lv.y = y + lvaVariables.plvz[i].y - iYWindowSize/2;
					lv.z = z + lvaVariables.plvz[i].z - iZWindowSize/2;
					lv.fValue = log( fio.pfVectors[iOffset + iIndex] );

					dSum += fio.pfVectors[iOffset + iIndex];

//					((float*)ppImgOutTemp1.ImageRow(y))[x] = fioTemp1.pfVectors[iOffset + iIndex];//lv.fValue;
//					((float*)ppImgOutTemp2.ImageRow(y))[x] = fioTemp2.pfVectors[iOffset + iIndex];//lv.fValue;
//					((float*)ppImgOutTemp4.ImageRow(y))[x] = fioTemp2.pfVectors[iOffset + iIndex - 3];//lv.fValue;
//					((float*)ppImgOutTemp3.ImageRow(y))[x] = lv.fValue;
				}
			}
		}

//		output_float( ppImgOutTemp1, "window1.pgm" );
//		output_float( ppImgOutTemp2, "window2.pgm" );
//		output_float( ppImgOutTemp3, "window_like_1_2.pgm" );
//		output_float( ppImgOutTemp4, "window_max_like_1_2.pgm" );
/*
		// This is a check to see that the entropy calculated over this likelihood
		// is the same as the entropy comming from FeatureCompare.exe.
		double dEntropy = 0;

		for( int z = 0; z < iZWindowSize; z++ )
		{
			int iZIndex = z*fio.x*fio.y;
			int iZList = z*iXWindowSize*iYWindowSize;
			for( int y = 0; y < iYWindowSize; y++ )
			{
				int iYIndex = iZIndex + y*fio.x;
				int iYList = iZList + y*iXWindowSize;
				for( int x = 0; x < iXWindowSize; x++ )
				{
					int iIndex = iYIndex + x;
					int iListIndex = iYList + x;

					LOCATION_VALUE_XYZ &lv =
						lvcMarginals.plvza[i].plvz[iListIndex];

					double dProb = fio.pfVectors[iOffset + iIndex] / dSum;
					if( dProb > 0 )
					{
						dEntropy += dProb*log( dProb );
					}
				}
			}
		}

		double dEntropyDivisor = log(iZWindowSize*iYWindowSize*iXWindowSize);
		dEntropy = dEntropy / dEntropyDivisor;
*/
	}

	// Sort peaks

	for( int i = 0; i < lvaVariables.iCount; i++ )
	{
		lvSortHighLow( lvcMarginals.plvza[i] );
	}

	return 1;
}

//
// regEnforceGeometricPrior()
//
// Enforces the geometric prior of psNeighbour on lvaVarrCurr. The prior
// is exponential, and exponents are added to lvaVarrCurr.
//
void
regEnforceGeometricPrior(
						LOCATION_VALUE_XYZ_ARRAY	&lvaVarrCurr,		// Current variable on which to enforce geometric prior
						LOCATION_VALUE_XYZ			&lvaNeighbourInstance,		// Current variable on which to enforce geometric prior
						PRIOR_STRUCT				&psNeighbour
						)
{
	// Loop through all neighbours
	
	//fprintf( g_outfile, "%d\t%100.100f\n", psNeighbour.iDistSqr, psNeighbour.fDenominator );

	for( int iValue = 0; iValue < lvaVarrCurr.iCount; iValue++ )
	{
		// The current value for iVar2
		
		LOCATION_VALUE_XYZ &lvCurrVarr = lvaVarrCurr.plvz[iValue];
		
		// Calculate and accumulate current probability

		int iDistSqr = location_xyz_dist_sqr(
			lvaNeighbourInstance,lvCurrVarr);

		int iDistSqrDiff = iDistSqr - psNeighbour.iDistSqr;
		
		//float fDistSqrDiffSqr = (float)(iDistSqrDiff*iDistSqrDiff);
		float fDistSqrDiffSqr = (float)iDistSqrDiff;
		fDistSqrDiffSqr *= fDistSqrDiffSqr;
		
		float fPower = -fDistSqrDiffSqr*psNeighbour.fDenominator;

		assert( fPower <= 0.0 );
		
		lvCurrVarr.fValue += fPower;
	}
}

static void
calculate_prior_structs(
						int iVar1,
						int *piVariableOrder,
						LOCATION_VALUE_XYZ_COLLECTION &lvcVariables,
						PRIOR_STRUCT *ppsPriorStructs
					   )
{
	for( int iv2 = 0; iv2 < lvcVariables.iCount; iv2++ )
	{
		int iVar2 = piVariableOrder[iv2];
		ppsPriorStructs[iVar2].iVarX = lvcVariables.plvza[iVar1].lvzOrigin.x;
		ppsPriorStructs[iVar2].iVarY = lvcVariables.plvza[iVar1].lvzOrigin.y;
		ppsPriorStructs[iVar2].iVarZ = lvcVariables.plvza[iVar1].lvzOrigin.z;
		
		ppsPriorStructs[iVar2].iDistSqrX = -1000; // Not used
		ppsPriorStructs[iVar2].iDistSqrY = -1000; // Not used
		ppsPriorStructs[iVar2].iDistSqrZ = -1000; // Not used

		ppsPriorStructs[iVar2].iDistSqr = location_xyz_dist_sqr(
			lvcVariables.plvza[iVar1].lvzOrigin,
			lvcVariables.plvza[iVar2].lvzOrigin );

		ppsPriorStructs[iVar2].iDistQuad =
			ppsPriorStructs[iVar2].iDistSqr*ppsPriorStructs[iVar2].iDistSqr;

		assert( ppsPriorStructs[iVar2].iDistQuad >= 0 );
		
		ppsPriorStructs[iVar2].fDenominator = 1.0f / (CLIQUE2_VARR*ppsPriorStructs[iVar2].iDistQuad);
	}
}

float
regGreedySample(
				int *piVariableOrdering,
				int *piVariableEvidence,
				LOCATION_VALUE_XYZ_COLLECTION &lvcLikes,
				PpImage &ppImgNeighbours,
				LOCATION_VALUE_XYZ_ARRAY &lvaSample
				)
{
	// Organize so that all evidence variables are evaluted first

	int iv1;
	int icv;

	int *piVO = new int[lvcLikes.iCount];//piVariableOrdering;
	int *piVE = new int[lvcLikes.iCount];//piVariableEvidence;

	int iEvidenceVariables = 0;

	// Put all fixed evidence variables first in order

	for( iv1 = 0; iv1 < lvcLikes.iCount; iv1++ )
	{
		if( piVariableEvidence[iv1] >= 0 )
		{
			// Save variable and evidence at new locations
			piVO[iEvidenceVariables] = piVariableOrdering[iv1];
			piVE[iEvidenceVariables] = piVariableEvidence[iv1];
			iEvidenceVariables++;
		}
	}

	assert( iEvidenceVariables <= lvcLikes.iCount );

	// Put all non-fixed variables next in order

	int iCount = iEvidenceVariables;

	for( iv1 = 0; iv1 < lvcLikes.iCount; iv1++ )
	{
		if( piVariableEvidence[iv1] < 0 )
		{
			piVO[iCount] = piVariableOrdering[iv1];
			piVE[iCount] = piVariableEvidence[iv1];
			iCount++;
		}
	}

	assert( iCount == lvcLikes.iCount );

	LOCATION_VALUE_XYZ_COLLECTION lvcProbs;
	lvcProbs = lvcLikes;
	lvcProbs.plvza = new LOCATION_VALUE_XYZ_ARRAY[lvcLikes.iCount];
	assert( lvcProbs.plvza );

	// Counter for variables

	int *piVarCount = new int[lvcLikes.iCount];

	PRIOR_STRUCT *ppsPostInfo = new PRIOR_STRUCT[lvcLikes.iCount*lvcLikes.iCount];

	// Probability/exponent stack
	float *pfVarExponents = new float[lvcLikes.iCount];

	for( iv1 = 0; iv1 < lvcLikes.iCount; iv1++ )
	{
		int iVar1 = piVO[iv1];

		// Copy information from likelihood list to list of probabilities

		lvcProbs.plvza[iVar1] = lvcLikes.plvza[iVar1];
		lvcProbs.plvza[iVar1].plvz = new LOCATION_VALUE_XYZ[lvcLikes.plvza[iVar1].iCount];
		assert( lvcProbs.plvza[iVar1].plvz );

		piVarCount[iVar1] = -1;	// Invalidate
		
		// Set exponent stack

		pfVarExponents[iVar1] = 0.0f;

		// Generate prior information 

		PRIOR_STRUCT *ppsPostInfoRow = ppsPostInfo + iVar1*lvcLikes.iCount;
		calculate_prior_structs( iVar1, piVO, lvcLikes, ppsPostInfoRow );
	}

	// Start off by generating distributions for all fixed variables

	//g_outfile = fopen( "prior1.txt", "wt" );

	for( icv = 0; icv < iEvidenceVariables; icv++ )
	{
		int iCurrVarr = piVO[icv];

		assert( piVarCount[iCurrVarr] == -1 );
		assert( piVE[icv] != -1 );
		piVarCount[iCurrVarr] = piVE[icv];	// Set fixed evidence value

		// Copy likelihoods into P(iCurrVarr)
		assert( lvcProbs.plvza[iCurrVarr].iCount == lvcLikes.plvza[iCurrVarr].iCount );
		assert( lvcProbs.plvza[iCurrVarr].lvzOrigin.x == lvcLikes.plvza[iCurrVarr].lvzOrigin.x );
		assert( lvcProbs.plvza[iCurrVarr].lvzOrigin.y == lvcLikes.plvza[iCurrVarr].lvzOrigin.y );
		assert( lvcProbs.plvza[iCurrVarr].lvzOrigin.z == lvcLikes.plvza[iCurrVarr].lvzOrigin.z );
		
		memcpy( lvcProbs.plvza[iCurrVarr].plvz, lvcLikes.plvza[iCurrVarr].plvz,
			sizeof(LOCATION_VALUE_XYZ)*lvcLikes.plvza[iCurrVarr].iCount
			);

		if( icv > 0 )
		{
			// Add exponent from previous distribution to P(iCurrVarr)
			
			assert( icv > 0 );
			int iPrevVarr = piVO[icv-1];
			
			assert( piVarCount[iPrevVarr] >= 0 );  // Make sure previous variable is valid
			assert( piVarCount[iPrevVarr] < lvcProbs.plvza[iPrevVarr].iCount );
			
			for( int iValue = 0; iValue < lvcProbs.plvza[iCurrVarr].iCount; iValue++ )
			{
				lvcProbs.plvza[iCurrVarr].plvz[iValue].fValue +=
					lvcProbs.plvza[iPrevVarr].plvz[ piVarCount[iPrevVarr] ].fValue;
			}
			
			for( iv1 = 0; iv1 < icv; iv1 ++ )
			{
				int iVar1 = piVO[iv1];

				// Variables 0 to iVar1 already have instantiations,
				// create P(iCurrVarr|0..iVar1) for all neighbouring iCurrVarr
				// and iVar1.
				
				if( !(ppImgNeighbours.ImageRow(iCurrVarr))[iVar1] )
				{
					continue;
				}
				
				assert( iCurrVarr != iVar1 );
				
				// Factor in Prior information for each value of iVar2
				
				assert( iVar1 >= 0 );
				assert( piVarCount[iVar1] < lvcProbs.plvza[iVar1].iCount );
				
				LOCATION_VALUE_XYZ &lvVar1 =
					lvcProbs.plvza[iVar1].plvz[piVarCount[iVar1]];

				regEnforceGeometricPrior( lvcProbs.plvza[iCurrVarr], lvVar1,
					ppsPostInfo[iCurrVarr*lvcLikes.iCount + iVar1]
					);
			}
		}
		// Don't sort here, variables are fixed
	}

	// Set up and evaluate free variables

	assert( icv == iEvidenceVariables );
	for( icv = iEvidenceVariables; icv < lvcLikes.iCount; icv++ )
	{
		int iCurrVarr = piVO[icv];

		piVarCount[iCurrVarr] = 0;	// This will be the greedy choice for this variable...

		assert( lvcProbs.plvza[iCurrVarr].iCount == lvcLikes.plvza[iCurrVarr].iCount );
		assert( lvcProbs.plvza[iCurrVarr].lvzOrigin.y == lvcLikes.plvza[iCurrVarr].lvzOrigin.y );	
		memcpy( lvcProbs.plvza[iCurrVarr].plvz, lvcLikes.plvza[iCurrVarr].plvz,
			sizeof(LOCATION_VALUE_XYZ)*lvcLikes.plvza[iCurrVarr].iCount
			);

		// Enforce priors if relevant

		if( icv > 0 )
		{		
			// Add exponent from previous distribution to P(iCurrVarr)
	
			assert( icv > 0 );
			int iPrevVarr = piVO[icv-1];
			
			assert( piVarCount[iPrevVarr] >= 0 );  // Make sure previous variable is valid
			assert( piVarCount[iPrevVarr] < lvcProbs.plvza[iPrevVarr].iCount );
			
			for( int iValue = 0; iValue < lvcProbs.plvza[iCurrVarr].iCount; iValue++ )
			{
				lvcProbs.plvza[iCurrVarr].plvz[iValue].fValue +=
					lvcProbs.plvza[iPrevVarr].plvz[ piVarCount[iPrevVarr] ].fValue;
			}

			for( iv1 = 0; iv1 < icv; iv1++ )
			{
				int iVar1 = piVO[iv1];
				
				// Variables 0 to iVar1 already have instantiations,
				// create P(iCurrVarr|0..iVar1) for all neighbouring iCurrVarr
				// and iVar1.
				
				if( !(ppImgNeighbours.ImageRow(iCurrVarr))[iVar1] )
				{
					continue;
				}
				
				assert( iCurrVarr != iVar1 );
				
				// Factor in Prior information for each value of iVar2
				
				assert( iVar1 >= 0 );
				assert( piVarCount[iVar1] < lvcProbs.plvza[iVar1].iCount );
				
				LOCATION_VALUE_XYZ &lvVar1 =
					lvcProbs.plvza[iVar1].plvz[piVarCount[iVar1]];
				
				regEnforceGeometricPrior( lvcProbs.plvza[iCurrVarr], lvVar1,
					ppsPostInfo[iCurrVarr*lvcLikes.iCount + iVar1]
					);
			}
		}

		// Instead of sorting, scroll though & find highest value
		piVarCount[iCurrVarr] = 0;
		for( int i = 0; i < lvcProbs.plvza[iCurrVarr].iCount; i++ )
		{
			if( lvcProbs.plvza[iCurrVarr].plvz[i].fValue > 
				lvcProbs.plvza[iCurrVarr].plvz[piVarCount[iCurrVarr]].fValue )
			{
				piVarCount[iCurrVarr] = i;
			}
		}

		// Sort and select greedy sample (the first variable instance after sorting...
		//lvSortHighLow( lvcProbs.plvza[iCurrVarr] );
		//piVarCount[iCurrVarr] = 0;
	}

	//fclose( g_outfile );

	// At this point, the greedy sample is chosen, extract the value
	icv = lvcLikes.iCount - 1;
	int iCurrVarr = piVO[icv];
	float fReturn = lvcProbs.plvza[iCurrVarr].plvz[ piVarCount[iCurrVarr] ].fValue;

	for( iv1 = 0; iv1 < lvcLikes.iCount; iv1++ )
	{
		int iVar1 = piVO[iv1];
		lvaSample.plvz[iVar1] = lvcProbs.plvza[iVar1].plvz[ piVarCount[iVar1] ];
	}

	delete [] pfVarExponents;
	delete [] ppsPostInfo;
	delete [] piVarCount;

	for( iv1 = 0; iv1 < lvcLikes.iCount; iv1++ )
	{
		int iVar1 = piVO[iv1];
		delete [] lvcProbs.plvza[iVar1].plvz;
	}
	delete [] lvcProbs.plvza;

	delete [] piVO;
	delete [] piVE;

	return fReturn;
}








float
regGreedyAscentSample(
				int *piVariableOrdering,
				int *piVariableEvidence,
				LOCATION_VALUE_XYZ_COLLECTION &lvcLikes,
				PpImage &ppImgNeighbours,
				LOCATION_VALUE_XYZ_ARRAY &lvaSample
				)
{
	assert( lvaSample.iCount == lvcLikes.iCount );

	// Organize so that all evidence variables are evaluted first

	int iv1;
	int icv;

	int *piVO = new int[lvcLikes.iCount];//piVariableOrdering;
	int *piVE = new int[lvcLikes.iCount];//piVariableEvidence;

	int iEvidenceVariables = 0;

	// Put all fixed evidence variables first in order

	for( iv1 = 0; iv1 < lvcLikes.iCount; iv1++ )
	{
		if( piVariableEvidence[iv1] >= 0 )
		{
			// Save variable and evidence at new locations
			piVO[iEvidenceVariables] = piVariableOrdering[iv1];
			piVE[iEvidenceVariables] = piVariableEvidence[iv1];
			iEvidenceVariables++;
		}
	}

	assert( iEvidenceVariables <= lvcLikes.iCount );

	// Put all non-fixed variables next in order

	int iCount = iEvidenceVariables;

	for( iv1 = 0; iv1 < lvcLikes.iCount; iv1++ )
	{
		if( piVariableEvidence[iv1] < 0 )
		{
			piVO[iCount] = piVariableOrdering[iv1];
			piVE[iCount] = piVariableEvidence[iv1];
			iCount++;
		}
	}

	assert( iCount == lvcLikes.iCount );

	LOCATION_VALUE_XYZ_COLLECTION lvcProbs;
	lvcProbs = lvcLikes;
	lvcProbs.plvza = new LOCATION_VALUE_XYZ_ARRAY[lvcLikes.iCount];
	assert( lvcProbs.plvza );

	// Counter for variables

	int *piVarCount = new int[lvcLikes.iCount];
	assert( piVarCount );

	PRIOR_STRUCT *ppsPostInfo = new PRIOR_STRUCT[lvcLikes.iCount*lvcLikes.iCount];
	assert( ppsPostInfo );

	for( iv1 = 0; iv1 < lvcLikes.iCount; iv1++ )
	{
		int iVar1 = piVO[iv1];

		// Copy information from likelihood list to list of probabilities

		lvcProbs.plvza[iVar1] = lvcLikes.plvza[iVar1];
		lvcProbs.plvza[iVar1].plvz = new LOCATION_VALUE_XYZ[lvcLikes.plvza[iVar1].iCount];
		assert( lvcProbs.plvza[iVar1].plvz );

		// Generate prior information 

		PRIOR_STRUCT *ppsPostInfoRow = ppsPostInfo + iVar1*lvcLikes.iCount;
		calculate_prior_structs( iVar1, piVO, lvcLikes, ppsPostInfoRow );
	}

	regSampleToIndices( piVarCount, lvaSample, lvcLikes );

	int bChange = 1;
	while( bChange )
	{
		// Generate all probabilities based on current sample instantiation
		// We want to see if the probability of changing a variable can result
		// in a posterior probability increase, considering the markov neighbourhood
		// of that variable.

		for( icv = 0; icv < lvcLikes.iCount; icv++ )
		{
			int iCurrVarr = piVO[icv];

			// Copy likelihoods into P(iCurrVarr)

			assert( lvcProbs.plvza[iCurrVarr].iCount == lvcLikes.plvza[iCurrVarr].iCount );
			assert( lvcProbs.plvza[iCurrVarr].lvzOrigin.x == lvcLikes.plvza[iCurrVarr].lvzOrigin.x );
			assert( lvcProbs.plvza[iCurrVarr].lvzOrigin.y == lvcLikes.plvza[iCurrVarr].lvzOrigin.y );
			assert( lvcProbs.plvza[iCurrVarr].lvzOrigin.z == lvcLikes.plvza[iCurrVarr].lvzOrigin.z );
			
			memcpy( lvcProbs.plvza[iCurrVarr].plvz, lvcLikes.plvza[iCurrVarr].plvz,
				sizeof(LOCATION_VALUE_XYZ)*lvcLikes.plvza[iCurrVarr].iCount
				);
			
			// Enforce all relevant priors

			for( iv1 = 0; iv1 < lvcLikes.iCount; iv1 ++ )
			{
				int iVar1 = piVO[iv1];

				if( !(ppImgNeighbours.ImageRow(iCurrVarr))[iVar1]
					|| iCurrVarr == iVar1 )
				{
					continue;
				}
				
				assert( iCurrVarr != iVar1 );
				
				// Factor in Prior information for each value of iVar2
				
				assert( iVar1 >= 0 );
				assert( piVarCount[iVar1] < lvcProbs.plvza[iVar1].iCount );
				
				LOCATION_VALUE_XYZ &lvVar1 =
					lvcLikes.plvza[iVar1].plvz[piVarCount[iVar1]];
				
				regEnforceGeometricPrior( lvcProbs.plvza[iCurrVarr], lvVar1,
					ppsPostInfo[iCurrVarr*lvcLikes.iCount + iVar1]
					);
			}
			
			// Don't sort here, keep fixed
		}

		// Find out changing which variable will result in the highest
		// probability gain

		int iMaxExpIncreaseVarr = -1;
		int iMaxExpIncreaseInstance = -1;
		float fMaxExpIncrease = MIN_EXPONENT;

		for( icv = 0; icv < lvcLikes.iCount; icv++ )
		{
			int iCurrVarr = piVO[icv];
			int iCurrEv   = piVE[icv];

			if( iCurrEv != -1 )
			{
				// Don't consider, this variable is fixed
				assert( piVarCount[iCurrVarr] == iCurrEv );
				continue;
			}

			int iMaxNewExpInstance = -1;
			float fMaxNewExp = MIN_EXPONENT;

			LOCATION_VALUE_XYZ_ARRAY &lvaCurrInstances = lvcProbs.plvza[iCurrVarr];

			for( int j = 0; j < lvaCurrInstances.iCount; j++ )
			{
				if( lvaCurrInstances.plvz[j].fValue > fMaxNewExp )
				{
					fMaxNewExp = lvaCurrInstances.plvz[j].fValue;
					iMaxNewExpInstance = j;
				}
			}
			assert( iMaxNewExpInstance != -1 );

			float fExpIncrease =
				fMaxNewExp - lvaCurrInstances.plvz[ piVarCount[iCurrVarr] ].fValue;

			if( fExpIncrease > fMaxExpIncrease )
			{
				// Save new max increase
				iMaxExpIncreaseVarr = iCurrVarr;
				fMaxExpIncrease = fExpIncrease;
				iMaxExpIncreaseInstance = iMaxNewExpInstance;
			}
		}

		// Change probability only if a positive change exists
		
		if( fMaxExpIncrease > 0.0f )
		{
			assert( iMaxExpIncreaseVarr != -1 );
			piVarCount[iMaxExpIncreaseVarr] = iMaxExpIncreaseInstance;
		}
		else
		{
			// No change
			bChange = 0;
		}
	}

	// Calculate return value, set new sample

	for( icv = 0; icv < lvcLikes.iCount; icv++ )
	{
		int iCurrVarr = piVO[icv];

		// Copy likelihoods into P(iCurrVarr)
		assert( lvcProbs.plvza[iCurrVarr].iCount == lvcLikes.plvza[iCurrVarr].iCount );
		assert( lvcProbs.plvza[iCurrVarr].lvzOrigin.x == lvcLikes.plvza[iCurrVarr].lvzOrigin.x );
		assert( lvcProbs.plvza[iCurrVarr].lvzOrigin.y == lvcLikes.plvza[iCurrVarr].lvzOrigin.y );
		assert( lvcProbs.plvza[iCurrVarr].lvzOrigin.z == lvcLikes.plvza[iCurrVarr].lvzOrigin.z );
		
		memcpy( lvcProbs.plvza[iCurrVarr].plvz, lvcLikes.plvza[iCurrVarr].plvz,
			sizeof(LOCATION_VALUE_XYZ)*lvcLikes.plvza[iCurrVarr].iCount
			);

		if( icv > 0 )
		{
			
			// Add exponent from previous distribution to P(iCurrVarr)
			
			assert( icv > 0 );
			int iPrevVarr = piVO[icv-1];
			
			assert( piVarCount[iPrevVarr] >= 0 );  // Make sure previous variable is valid
			assert( piVarCount[iPrevVarr] < lvcProbs.plvza[iPrevVarr].iCount );
			
			for( int iValue = 0; iValue < lvcProbs.plvza[iCurrVarr].iCount; iValue++ )
			{
				lvcProbs.plvza[iCurrVarr].plvz[iValue].fValue +=
					lvcProbs.plvza[iPrevVarr].plvz[ piVarCount[iPrevVarr] ].fValue;
			}
			
			for( iv1 = 0; iv1 < icv; iv1 ++ )
			{
				int iVar1 = piVO[iv1];
				
				// Variables 0 to iVar1 already have instantiations,
				// create P(iCurrVarr|0..iVar1) for all neighbouring iCurrVarr
				// and iVar1.
				
				if( !(ppImgNeighbours.ImageRow(iCurrVarr))[iVar1] )
				{
					continue;
				}
				
				assert( iCurrVarr != iVar1 );
				
				// Factor in Prior information for each value of iVar2
				
				assert( iVar1 >= 0 );
				assert( piVarCount[iVar1] < lvcProbs.plvza[iVar1].iCount );
				
				LOCATION_VALUE_XYZ &lvVar1 =
					lvcProbs.plvza[iVar1].plvz[piVarCount[iVar1]];
				
				regEnforceGeometricPrior( lvcProbs.plvza[iCurrVarr], lvVar1,
					ppsPostInfo[iCurrVarr*lvcLikes.iCount + iVar1]
					);
			}
		}

		LOCATION_VALUE_XYZ_ARRAY &lvaCurrInstances =
			lvcProbs.plvza[iCurrVarr];
		LOCATION_VALUE_XYZ &lvCurrInstance =
			lvaCurrInstances.plvz[ piVarCount[iCurrVarr] ];

		lvaSample.plvz[iCurrVarr] = lvCurrInstance;
	}

	icv = lvcLikes.iCount - 1;
	int iCurrVarr = piVO[icv];
	float fReturn = lvcProbs.plvza[iCurrVarr].plvz[ piVarCount[iCurrVarr] ].fValue;

	delete [] ppsPostInfo;
	delete [] piVarCount;

	for( iv1 = 0; iv1 < lvcLikes.iCount; iv1++ )
	{
		int iVar1 = piVO[iv1];
		delete [] lvcProbs.plvza[iVar1].plvz;
	}
	delete [] lvcProbs.plvza;

	delete [] piVO;
	delete [] piVE;

	return fReturn;
}











float
regFindTransformMinProbCuttoff(
								int *piVariableOrdering,
								int *piVariableEvidence,
								LOCATION_VALUE_XYZ_COLLECTION &lvcLikes,
								PpImage &ppImgNeighbours,
								LOCATION_VALUE_XYZ_ARRAY &lvaBestSolution
								)
{
	// Organize so that all evidence variables are evaluted first
	
	int iv1;
	int icv;

	int *piVO = new int[lvcLikes.iCount];//piVariableOrdering;
	int *piVE = new int[lvcLikes.iCount];//piVariableEvidence;

	int iEvidenceVariables = 0;

	// Put all fixed evidence variables first in order

	for( iv1 = 0; iv1 < lvcLikes.iCount; iv1++ )
	{
		if( piVariableEvidence[iv1] >= 0 )
		{
			// Save variable and evidence at new locations
			piVO[iEvidenceVariables] = piVariableOrdering[iv1];
			piVE[iEvidenceVariables] = piVariableEvidence[iv1];
			iEvidenceVariables++;
		}
	}

	assert( iEvidenceVariables <= lvcLikes.iCount );

	// Put all non-fixed variables next in order

	int iCount = iEvidenceVariables;

	for( iv1 = 0; iv1 < lvcLikes.iCount; iv1++ )
	{
		if( piVariableEvidence[iv1] < 0 )
		{
			piVO[iCount] = piVariableOrdering[iv1];
			piVE[iCount] = piVariableEvidence[iv1];
			iCount++;
		}
	}

	assert( iCount == lvcLikes.iCount );

	LOCATION_VALUE_XYZ_COLLECTION lvcProbs;
	lvcProbs = lvcLikes;
	lvcProbs.plvza = new LOCATION_VALUE_XYZ_ARRAY[lvcLikes.iCount];
	assert( lvcProbs.plvza );

	// For saving the max probability

	float fMaxExponent = (float)MIN_EXPONENT;
	LOCATION_VALUE_XYZ *plvMaxConfig = lvaBestSolution.plvz; //new LOCATION_VALUE_XYZ[lvcLikes.iCount];

	// Counter for variables

	int *piVarCount = new int[lvcLikes.iCount];

	PRIOR_STRUCT *ppsPostInfo = new PRIOR_STRUCT[lvcLikes.iCount*lvcLikes.iCount];

	// Probability/exponent stack
	float *pfVarExponents = new float[lvcLikes.iCount];
	
	// An array of the maximum likelihood remaining, for making a faster cutoff
	// in the alpha-beta search at each level of the tree.
	float *pfMaxRemainingLikelihood = new float[lvcLikes.iCount];

	for( iv1 = 0; iv1 < lvcLikes.iCount; iv1++ )
	{
		int iVar1 = piVO[iv1];

		// Copy information from likelihood list to list of probabilities

		lvcProbs.plvza[iVar1] = lvcLikes.plvza[iVar1];
		lvcProbs.plvza[iVar1].plvz = new LOCATION_VALUE_XYZ[lvcLikes.plvza[iVar1].iCount];
		assert( lvcProbs.plvza[iVar1].plvz );

		piVarCount[iVar1] = -1;	// Invalidate
		
		// Set exponent stack

		pfVarExponents[iVar1] = 0.0f;

		// Generate prior information 

		PRIOR_STRUCT *ppsPostInfoRow = ppsPostInfo + iVar1*lvcLikes.iCount;
		calculate_prior_structs( iVar1, piVO, lvcLikes, ppsPostInfoRow );
	}

	//
	// Create maximum remaining likelihood array.
	//
	// This is the product of the maximum likelihood values
	// for all remaining variable further down in the search tree.
	//
	// It is multiplied with the current probability in the search
	// tree to give a tighter upper bound on the maximum possible
	// probability of the current search tree branch.
	//
	for( iv1 = 0; iv1 < lvcLikes.iCount; iv1++ )
	{
		int iVar1 = piVO[iv1];

		pfMaxRemainingLikelihood[iVar1] = 0.0f;

		// Caluculate 
		for( int iv2 = iv1 + 1; iv2 < lvcLikes.iCount; iv2++ )
		{
			int iVar2 = piVO[iv2];

			// Search for maximum likelihood value of iVar2

			float fMaxLikeVar2 = (float)MIN_EXPONENT;
			for( int iValue = 0; iValue < lvcLikes.plvza[iVar2].iCount; iValue++ )
			{
				if( lvcLikes.plvza[iVar2].plvz[iValue].fValue > fMaxLikeVar2 )
				{
					fMaxLikeVar2 = lvcLikes.plvza[iVar2].plvz[iValue].fValue;
				}
			}
			pfMaxRemainingLikelihood[iVar1] += fMaxLikeVar2;
		}
	}

	// Start off by generating distributions for all fixed variables

	for( icv = 0; icv < iEvidenceVariables; icv++ )
	{
		int iCurrVarr = piVO[icv];

		assert( piVarCount[iCurrVarr] == -1 );
		assert( piVE[icv] != -1 );
		piVarCount[iCurrVarr] = piVE[icv];	// Set fixed evidence value

		// Copy likelihoods into P(iCurrVarr)
		assert( lvcProbs.plvza[iCurrVarr].iCount == lvcLikes.plvza[iCurrVarr].iCount );
		assert( lvcProbs.plvza[iCurrVarr].lvzOrigin.x == lvcLikes.plvza[iCurrVarr].lvzOrigin.x );
		assert( lvcProbs.plvza[iCurrVarr].lvzOrigin.y == lvcLikes.plvza[iCurrVarr].lvzOrigin.y );
		assert( lvcProbs.plvza[iCurrVarr].lvzOrigin.z == lvcLikes.plvza[iCurrVarr].lvzOrigin.z );
		
		memcpy( lvcProbs.plvza[iCurrVarr].plvz, lvcLikes.plvza[iCurrVarr].plvz,
			sizeof(LOCATION_VALUE_XYZ)*lvcLikes.plvza[iCurrVarr].iCount
			);

		if( icv == 0 )
		{
			continue;
		}

		// Add exponent from previous distribution to P(iCurrVarr)
		
		assert( icv > 0 );
		int iPrevVarr = piVO[icv-1];
		
		assert( piVarCount[iPrevVarr] >= 0 );  // Make sure previous variable is valid
		assert( piVarCount[iPrevVarr] < lvcProbs.plvza[iPrevVarr].iCount );
		
		for( int iValue = 0; iValue < lvcProbs.plvza[iCurrVarr].iCount; iValue++ )
		{
			lvcProbs.plvza[iCurrVarr].plvz[iValue].fValue +=
				lvcProbs.plvza[iPrevVarr].plvz[ piVarCount[iPrevVarr] ].fValue;
		}
		
		for( iv1 = 0; iv1 < icv; iv1 ++ )
		{
			int iVar1 = piVO[iv1];

			// Variables 0 to iVar1 already have instantiations,
			// create P(iCurrVarr|0..iVar1) for all neighbouring iCurrVarr
			// and iVar1.
			
			if( !(ppImgNeighbours.ImageRow(iCurrVarr))[iVar1] )
			{
				continue;
			}
			
			assert( iCurrVarr != iVar1 );
			
			// Factor in Prior information for each value of iVar2
			
			assert( iVar1 >= 0 );
			assert( piVarCount[iVar1] < lvcProbs.plvza[iVar1].iCount );
			
			LOCATION_VALUE_XYZ &lvVar1 =
				lvcProbs.plvza[iVar1].plvz[piVarCount[iVar1]];

			regEnforceGeometricPrior( lvcProbs.plvza[iCurrVarr], lvVar1,
				ppsPostInfo[iCurrVarr*lvcLikes.iCount + iVar1]
				);
		}

		// Don't sort here, keep fixed
	}

	// Set up first non-fixed variable

	assert( icv == iEvidenceVariables );

	if( iEvidenceVariables < lvcLikes.iCount )
	{
		
		icv = iEvidenceVariables;

		int iCurrVarr = piVO[icv];

		piVarCount[iCurrVarr] = 0;

		assert( lvcProbs.plvza[iCurrVarr].iCount == lvcLikes.plvza[iCurrVarr].iCount );
		assert( lvcProbs.plvza[iCurrVarr].lvzOrigin.y == lvcLikes.plvza[iCurrVarr].lvzOrigin.y );	
		memcpy( lvcProbs.plvza[iCurrVarr].plvz, lvcLikes.plvza[iCurrVarr].plvz,
			sizeof(LOCATION_VALUE_XYZ)*lvcLikes.plvza[iCurrVarr].iCount
			);

		// Enforce priors if relevant

		if( icv > 0 )
		{		
			// Add exponent from previous distribution to P(iCurrVarr)
		
			assert( icv > 0 );
			int iPrevVarr = piVO[icv-1];
			
			assert( piVarCount[iPrevVarr] >= 0 );  // Make sure previous variable is valid
			assert( piVarCount[iPrevVarr] < lvcProbs.plvza[iPrevVarr].iCount );
			
			for( int iValue = 0; iValue < lvcProbs.plvza[iCurrVarr].iCount; iValue++ )
			{
				lvcProbs.plvza[iCurrVarr].plvz[iValue].fValue +=
					lvcProbs.plvza[iPrevVarr].plvz[ piVarCount[iPrevVarr] ].fValue;
			}
			
			for( iv1 = 0; iv1 < icv; iv1 ++ )
			{
				int iVar1 = piVO[iv1];
				
				// Variables 0 to iVar1 already have instantiations,
				// create P(iCurrVarr|0..iVar1) for all neighbouring iCurrVarr
				// and iVar1.
				
				if( !(ppImgNeighbours.ImageRow(iCurrVarr))[iVar1] )
				{
					continue;
				}
				
				assert( iCurrVarr != iVar1 );
				
				// Factor in Prior information for each value of iVar2
				
				assert( iVar1 >= 0 );
				assert( piVarCount[iVar1] < lvcProbs.plvza[iVar1].iCount );
				
				LOCATION_VALUE_XYZ &lvVar1 =
					lvcProbs.plvza[iVar1].plvz[piVarCount[iVar1]];
				
				regEnforceGeometricPrior( lvcProbs.plvza[iCurrVarr], lvVar1,
					ppsPostInfo[iCurrVarr*lvcLikes.iCount + iVar1]
					);
			}
		}

		// Sort
		lvSortHighLow( lvcProbs.plvza[iCurrVarr] );
		// Set count to 0
		piVarCount[iCurrVarr] = 0;
	}

	// Finally, perform search for maximum transform

	while( icv >= iEvidenceVariables && icv < lvcLikes.iCount )
	{
		int iCurrVarr = piVO[icv];

		// At the start of the loop, the current variable always references
		// a valid distribution.

		assert( iCurrVarr < lvcProbs.iCount );
		assert( iCurrVarr >= 0 );
		assert( piVarCount[iCurrVarr] >= 0 );
		assert( piVarCount[iCurrVarr] <= lvcProbs.plvza[iCurrVarr].iCount );

		//if( iCurrVarr == lvcProbs.iCount - 1 )
		if( icv == lvcProbs.iCount - 1 )
		{
			// At the last variable, find and set maximum probability

			assert( piVarCount[iCurrVarr] == 0 );

			int &iValue = piVarCount[iCurrVarr];

			for( iValue = 0; iValue < lvcProbs.plvza[iCurrVarr].iCount; iValue++ )
			{
				if( lvcProbs.plvza[iCurrVarr].plvz[iValue].fValue > fMaxExponent )
				{
					// Found a maximum configuration, save...
					fMaxExponent = lvcProbs.plvza[iCurrVarr].plvz[iValue].fValue;

					for( iv1 = 0; iv1 < lvcProbs.iCount; iv1++ )
					{
						int iVar1 = piVO[iv1];
						plvMaxConfig[iVar1] = lvcProbs.plvza[iVar1].plvz[ piVarCount[iVar1] ];
					}
				}
			}

			// No more values left in current distribution, invalidate
			// and back up to previous variable

			piVarCount[iCurrVarr] = -1;
			icv = icv - 1;
			if( icv > 0 )
			{
				// Increment previous variable
				iCurrVarr = piVO[icv];
				piVarCount[iCurrVarr]++;
			}
		}
		else if( piVarCount[iCurrVarr] > -1 )
		{
			// Distribution exists for current variable, find next value that is not
			// already lower than the maximum...

			while(
				piVarCount[iCurrVarr] < lvcProbs.plvza[iCurrVarr].iCount
				//&& lvcProbs.plvza[iCurrVarr].plvz[ piVarCount[iCurrVarr] ].fValue < fMaxExponent
				&& lvcProbs.plvza[iCurrVarr].plvz[ piVarCount[iCurrVarr] ].fValue + pfMaxRemainingLikelihood[iCurrVarr] < fMaxExponent
				)
			{
				piVarCount[iCurrVarr] = piVarCount[iCurrVarr] + 1;
			}

			if( piVarCount[iCurrVarr] == lvcProbs.plvza[iCurrVarr].iCount )
			{
				// No more values left in current distribution capable of
				// producing a better maximum, invalidate distribution
				// and back up to previous variable

				piVarCount[iCurrVarr] = -1;
				icv = icv - 1;
				if( icv >= 0 )
				{
					// Increment previous variable
					iCurrVarr = piVO[icv];
					piVarCount[iCurrVarr]++;
				}
				continue;
			}

			// A possible value of current variable found, generate a new distribution
			// for the next variable

			icv = icv + 1;
			iCurrVarr = piVO[icv];
			assert( piVarCount[iCurrVarr] == -1 );

			// Copy likelihoods into P(iCurrVarr)
			assert( lvcProbs.plvza[iCurrVarr].iCount == lvcLikes.plvza[iCurrVarr].iCount );
			assert( lvcProbs.plvza[iCurrVarr].lvzOrigin.x == lvcLikes.plvza[iCurrVarr].lvzOrigin.x );
			assert( lvcProbs.plvza[iCurrVarr].lvzOrigin.y == lvcLikes.plvza[iCurrVarr].lvzOrigin.y );
			assert( lvcProbs.plvza[iCurrVarr].lvzOrigin.z == lvcLikes.plvza[iCurrVarr].lvzOrigin.z );
			
			memcpy( lvcProbs.plvza[iCurrVarr].plvz, lvcLikes.plvza[iCurrVarr].plvz,
				sizeof(LOCATION_VALUE_XYZ)*lvcLikes.plvza[iCurrVarr].iCount
				);

			// Add exponent from previous distribution to P(iCurrVarr)

			assert( icv > 0 );
			int iPrevVarr = piVO[icv-1];

			assert( piVarCount[iPrevVarr] >= 0 );  // Make sure previous variable is valid
			assert( piVarCount[iPrevVarr] < lvcProbs.plvza[iPrevVarr].iCount );

			for( int iValue = 0; iValue < lvcProbs.plvza[iCurrVarr].iCount; iValue++ )
			{
				lvcProbs.plvza[iCurrVarr].plvz[iValue].fValue +=
					lvcProbs.plvza[iPrevVarr].plvz[ piVarCount[iPrevVarr] ].fValue;
			}

			for( iv1 = 0; iv1 < icv; iv1 ++ )
			{
				int iVar1 = piVO[iv1];
				// Variables 0 to iVar1 already have instantiations,
				// create P(iCurrVarr|0..iVar1) for all neighbouring iCurrVarr
				// and iVar1.

				if( !(ppImgNeighbours.ImageRow(iCurrVarr))[iVar1] )
				{
					continue;
				}

				assert( iCurrVarr != iVar1 );

				// Factor in Prior information for each value of iVar2

				assert( iVar1 >= 0 );
				assert( piVarCount[iVar1] < lvcProbs.plvza[iVar1].iCount );

				LOCATION_VALUE_XYZ &lvVar1 =
					lvcProbs.plvza[iVar1].plvz[piVarCount[iVar1]];
				
				regEnforceGeometricPrior( lvcProbs.plvza[iCurrVarr], lvVar1,
					ppsPostInfo[iCurrVarr*lvcLikes.iCount + iVar1]
					);
			}

			lvSortHighLow( lvcProbs.plvza[iCurrVarr] );

			// Set count to 0
			piVarCount[iCurrVarr] = 0;
		}
		else
		{
			assert( 0 );
		}
	}

	delete [] pfMaxRemainingLikelihood;
	delete [] pfVarExponents;
	delete [] ppsPostInfo;
	delete [] piVarCount;

	for( iv1 = 0; iv1 < lvcLikes.iCount; iv1++ )
	{
		int iVar1 = piVO[iv1];
		delete [] lvcProbs.plvza[iVar1].plvz;
	}
	delete [] lvcProbs.plvza;

	delete [] piVO;
	delete [] piVE;

	return fMaxExponent;
}

float
regSampleProbability(
				   LOCATION_VALUE_XYZ_ARRAY			&lvaSample,
				   PpImage							&ppImgNeighbours,
				   LOCATION_VALUE_XYZ_COLLECTION	&lvcLikes,
				   LOCATION_VALUE_XYZ_ARRAY			&lvaLikes,
				   LOCATION_VALUE_XYZ_ARRAY			&lvaPrior
				   )
{
	int *piIndices = new int[lvcLikes.iCount];
	assert( piIndices );
	
	regSampleToIndices( piIndices, lvaSample, lvcLikes );
	
	float fProbExpSum = 0.0f;

	//g_outfile = fopen( "prior2.txt", "wt" );

	for( int icv = 0; icv < lvcLikes.iCount; icv++ )
	{
		int iCurrVarr = icv;

		LOCATION_VALUE_XYZ_ARRAY &lvaCurrVarr = lvcLikes.plvza[iCurrVarr];
		LOCATION_VALUE_XYZ &lvaCurrOrg = lvaCurrVarr.lvzOrigin;
		LOCATION_VALUE_XYZ &lvaCurrNew = lvaCurrVarr.plvz[ piIndices[iCurrVarr] ];

		assert( lvaSample.plvz[icv].x == lvaCurrNew.x );
		assert( lvaSample.plvz[icv].y == lvaCurrNew.y );
		assert( lvaSample.plvz[icv].z == lvaCurrNew.z );

		// Add likelihood

		fProbExpSum += lvaCurrNew.fValue;

		lvaLikes.plvz[icv] = lvaCurrNew;

		lvaPrior.plvz[icv] = lvaCurrNew;
		lvaPrior.plvz[icv].fValue = 0;

		for( int iv1 = 0; iv1 < lvcLikes.iCount; iv1++ )
		{
			int iVar1 = iv1;

			if( iCurrVarr == iVar1 )
			{
				continue;
			}

			if( !(ppImgNeighbours.ImageRow(iCurrVarr))[iVar1] )
			{
				continue;
			}

			LOCATION_VALUE_XYZ_ARRAY &lvaVarr1 = lvcLikes.plvza[ iVar1 ];
			LOCATION_VALUE_XYZ &lvaVarr1Org = lvaVarr1.lvzOrigin;
			LOCATION_VALUE_XYZ &lvaVarr1New = lvaVarr1.plvz[ piIndices[iVar1] ];

			assert( lvaSample.plvz[iv1].x == lvaVarr1New.x );
			assert( lvaSample.plvz[iv1].y == lvaVarr1New.y );
			assert( lvaSample.plvz[iv1].z == lvaVarr1New.z );

			int iOrgDistSqr = location_xyz_dist_sqr( lvaCurrOrg, lvaVarr1Org );
			int iNewDistSqr = location_xyz_dist_sqr( lvaCurrNew, lvaVarr1New );

			int iDistSqrDiff = iNewDistSqr - iOrgDistSqr;

			//float fDistSqrDiffSqr = (float)(iDistSqrDiff*iDistSqrDiff);
			float fDistSqrDiffSqr = (float)iDistSqrDiff;
			fDistSqrDiffSqr *= fDistSqrDiffSqr;

			int iOrgDistQuad = iOrgDistSqr*iOrgDistSqr;
			float fDenominator = 1.0 / (CLIQUE2_VARR*iOrgDistQuad);

			float fPower = -fDistSqrDiffSqr*fDenominator;

			fProbExpSum += fPower;

			lvaPrior.plvz[icv].fValue += fPower;

			assert( fProbExpSum <= 0.0 );

			//fprintf( g_outfile, "%d\t%100.100f\n", iOrgDistSqr, fDenominator );

		}
	}

	//fclose( g_outfile );

	delete [] piIndices;

	return fProbExpSum;
}

//
// regSampleProbability()
//
// Returns the probability of a sample. Points in lvaSample
// correspond to variables in lvcLikes. lvcLikes contains the 
// likelihood and the prior information to calculate the probabiltiy.
//
float
regSampleProbability(
				   LOCATION_VALUE_XYZ_ARRAY			&lvaSample,
				   PpImage							&ppImgNeighbours,
				   LOCATION_VALUE_XYZ_COLLECTION	&lvcLikes
				   )
{
	int *piIndices = new int[lvcLikes.iCount];
	assert( piIndices );
	
	regSampleToIndices( piIndices, lvaSample, lvcLikes );
	
	float fProbExpSum = 0.0f;

	//g_outfile = fopen( "prior2.txt", "wt" );

	for( int icv = 0; icv < lvcLikes.iCount; icv++ )
	{
		int iCurrVarr = icv;

		LOCATION_VALUE_XYZ_ARRAY &lvaCurrVarr = lvcLikes.plvza[iCurrVarr];
		LOCATION_VALUE_XYZ &lvaCurrOrg = lvaCurrVarr.lvzOrigin;
		LOCATION_VALUE_XYZ &lvaCurrNew = lvaCurrVarr.plvz[ piIndices[iCurrVarr] ];

		assert( lvaSample.plvz[icv].x == lvaCurrNew.x );
		assert( lvaSample.plvz[icv].y == lvaCurrNew.y );
		assert( lvaSample.plvz[icv].z == lvaCurrNew.z );

		// Add likelihood

		fProbExpSum += lvaCurrNew.fValue;

		for( int iv1 = 0; iv1 < icv; iv1++ )
		{
			int iVar1 = iv1;

			if( !(ppImgNeighbours.ImageRow(iCurrVarr))[iVar1] )
			{
				continue;
			}

			LOCATION_VALUE_XYZ_ARRAY &lvaVarr1 = lvcLikes.plvza[ iVar1 ];
			LOCATION_VALUE_XYZ &lvaVarr1Org = lvaVarr1.lvzOrigin;
			LOCATION_VALUE_XYZ &lvaVarr1New = lvaVarr1.plvz[ piIndices[iVar1] ];

			assert( lvaSample.plvz[iv1].x == lvaVarr1New.x );
			assert( lvaSample.plvz[iv1].y == lvaVarr1New.y );
			assert( lvaSample.plvz[iv1].z == lvaVarr1New.z );

			int iOrgDistSqr = location_xyz_dist_sqr( lvaCurrOrg, lvaVarr1Org );
			int iNewDistSqr = location_xyz_dist_sqr( lvaCurrNew, lvaVarr1New );

			int iDistSqrDiff = iNewDistSqr - iOrgDistSqr;

			//float fDistSqrDiffSqr = (float)(iDistSqrDiff*iDistSqrDiff);
			float fDistSqrDiffSqr = (float)iDistSqrDiff;
			fDistSqrDiffSqr *= fDistSqrDiffSqr;

			int iOrgDistQuad = iOrgDistSqr*iOrgDistSqr;
			float fDenominator = 1.0 / (CLIQUE2_VARR*iOrgDistQuad);

			float fPower = -fDistSqrDiffSqr*fDenominator;

			fProbExpSum += fPower;

			assert( fProbExpSum <= 0.0 );

			//fprintf( g_outfile, "%d\t%100.100f\n", iOrgDistSqr, fDenominator );

		}
	}

	//fclose( g_outfile );

	delete [] piIndices;

	return fProbExpSum;
}

//
// regPosteriorProbability()
//
// Converts a list of likelihood values to their posterior probabilities,
// given a set of fixed transform points.
// Returns the index of the maximum posterior value in lvaLikelihoods.
//
int
regPosteriorProbability(
				   LOCATION_VALUE_XYZ_ARRAY			&lvaLikelihoods,	// list of likelihood values for a new point
				   LOCATION_VALUE_XYZ_COLLECTION	&lvcFixedPoints,		// list of fixed transform points
				   int bgFlag											// 1 for basal ganglion to full brain, 0 for
																		// same size matches
				   )
{	
	float fMaxProb = 0.0f;
	int iMaxProbIndex = -1;
	LOCATION_VALUE_XYZ lvzOrigin;

	if( bgFlag == 1 )
	{
		lvzOrigin.x = lvaLikelihoods.lvzOrigin.x + 50;
		lvzOrigin.y = lvaLikelihoods.lvzOrigin.y + 60;
		lvzOrigin.z = lvaLikelihoods.lvzOrigin.z + 50;
	}
	else
	{
		lvzOrigin.x = lvaLikelihoods.lvzOrigin.x;
		lvzOrigin.y = lvaLikelihoods.lvzOrigin.y;
		lvzOrigin.z = lvaLikelihoods.lvzOrigin.z;
	}
	
	// Calculate the posterior for each point in the likelihood list, update the 
	// likelihood value by the posterior

	for( int i = 0; i < lvaLikelihoods.iCount; i++ )
	{
		LOCATION_VALUE_XYZ &lvaCurrVal = lvaLikelihoods.plvz[i];

		for( int j = 0; j < lvcFixedPoints.iCount; j++ )
		{
			LOCATION_VALUE_XYZ_ARRAY &lvaCurrFixed = lvcFixedPoints.plvza[j];

			// Distance in I1
			int iOrgDistSqr = location_xyz_dist_sqr( lvaCurrFixed.plvz[0], lvzOrigin );
			// Distance in I2
			int iNewDistSqr = location_xyz_dist_sqr( lvaCurrFixed.plvz[1], lvaCurrVal );				

			int iDistSqrDiff = iNewDistSqr - iOrgDistSqr;

			float fDistSqrDiffSqr = (float)iDistSqrDiff;
			fDistSqrDiffSqr *= fDistSqrDiffSqr;

			int iOrgDistQuad = iOrgDistSqr*iOrgDistSqr;
			float fDenominator = 1.0 / (CLIQUE2_VARR*iOrgDistQuad);

			float fPower = -fDistSqrDiffSqr*fDenominator;

			lvaCurrVal.fValue += fPower;								// Add prior to likelihood
		}

		if( lvaCurrVal.fValue > fMaxProb || iMaxProbIndex == -1 )
		{
			fMaxProb = lvaCurrVal.fValue;
			iMaxProbIndex = i;
		}
	}

	return iMaxProbIndex;
}

int
regCalculate_likelihood(
					 float *pfFeatures,
					 FEATUREIO &fioDomain,
					 FEATUREIO &fioLike
					 )
{
	for( int r = 0; r < fioDomain.y; r++ )
	{
		for( int c = 0; c < fioDomain.x; c++ )
		{
			//float fResult = vector_normalized_dot_product( pfFeatures, fioDomain.pfVectors + (r*fioDomain.x + c)*fioDomain.iFeaturesPerVector, fioDomain.iFeaturesPerVector );
			//fioLike.pfVectors[r*fioLike.x + c] = fResult;

			float fResult = vector_sqr_distance_varr( pfFeatures, fioDomain.pfVectors + (r*fioDomain.x + c)*fioDomain.iFeaturesPerVector, fioDomain.iFeaturesPerVector, fioDomain.pfVarrs );
			fioLike.pfVectors[r*fioLike.x + c] = (float)exp( -fResult );
		}
	}

	return 1;
}

int
regCalculate_likelihood(
					 int iRow,
					 int iCol,
					 FEATUREIO &fio1,
					 FEATUREIO &fio2,
					 FEATUREIO &fioLike
					 )
{

	float *pfFeatureVec = fio1.pfVectors + (iRow*fio1.x + iCol)*fio1.iFeaturesPerVector;

	return regCalculate_likelihood( pfFeatureVec, fio2, fioLike );
}

int
regCalculate_likelihood_robust(
					 float *pfFeatures,
					 FEATUREIO &fioDomain,
					 FEATUREIO &fioLike,
					 const float &fRobustVarr
					 )
{
	for( int r = 0; r < fioDomain.y; r++ )
	{
		for( int c = 0; c < fioDomain.x; c++ )
		{
			//float fResult = vector_normalized_dot_product( pfFeatures, fioDomain.pfVectors + (r*fioDomain.x + c)*fioDomain.iFeaturesPerVector, fioDomain.iFeaturesPerVector );
			//fioLike.pfVectors[r*fioLike.x + c] = fResult;

			float fResult = vector_sqr_distance_varr( pfFeatures, fioDomain.pfVectors + (r*fioDomain.x + c)*fioDomain.iFeaturesPerVector, fioDomain.iFeaturesPerVector, fioDomain.pfVarrs );
			fioLike.pfVectors[r*fioLike.x + c] = (float)(fRobustVarr / (fRobustVarr + fResult));
		}
	}

	return 1;
}

int
regCalculate_likelihood_robust(
					 int iRow,
					 int iCol,
					 FEATUREIO &fio1,
					 FEATUREIO &fio2,
					 FEATUREIO &fioLike,
					 const float &fRobustVarr
					 )
{

	float *pfFeatureVec = fio1.pfVectors + (iRow*fio1.x + iCol)*fio1.iFeaturesPerVector;

	return regCalculate_likelihood_robust( pfFeatureVec, fio2, fioLike, fRobustVarr );
}
