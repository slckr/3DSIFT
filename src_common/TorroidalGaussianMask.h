
//
// What is a torroidal Gaussian?
// The exponent looks like this:
//
//  exp -(
//		( (distance)^2 - (expected distance)^2  )^2
//			/ varr^2
//	)
//
// where (expected distance) and (varr) are constants, and distance
// is varriable:
//	distance^2 = ( row - mean row)^2 + ( col - mean col)^2
//

#ifndef __TORROIDALGAUSSIANMASK_H__
#define __TORROIDALGAUSSIANMASK_H__

#include "PpImage.h"

//
// calculate_torroidal_gaussian_filter_size()
//
// Calculates the size of a torroidal Gaussian filter in 1 dimension,
// such that the cut-off of the Gaussian is less than or equal
// to fMinValue multiplied by the maximum value of the Gaussian (center)
//
int
calculate_torroidal_gaussian_filter_size(
										 const float &fSigmaQuad,		// denominator of exponent!
										 const float &fExpectedDistance,	// expected distance
										 const float &fMinValue
										 );

// 
// generate_gaussian_filter2d()
//
//	Note: center of discrete pixel at (0,0) is considered (0.5, 0.5)
//
// Generates a 2d gaussian pattern specified by the given parameters.
// The maximum value is exp(0) = 1.0, no scaling is done.
//
//	ppTmp - output image, data type float (32-bits per pixel)
//	fSigmaRow - standard deviation of row gaussian component
//	fSigmaCol - standard deviation of col gaussian component
//	fMeanRow - mean of row gaussian component
//	fMeanCol - mean of col gaussian component
//
int
generate_torroidal_gaussian_filter2d(
									 PpImage	&ppTmp,
									 const float &fSigmaQuad,	// denominator of exponent!
									 const float &fExpectedDistance,
									 const float &fMeanRow,
									 const float &fMeanCol
									 );

//
// generate_torroidal_gaussian_filter2d_exponent()
//
// Same as generate_torroidal_gaussian_filter2d(), but calcualtes
// only the exponent.
//
int
generate_torroidal_gaussian_filter2d_exponent(
									 PpImage	&ppTmp,
									 const float &fSigmaQuad,	// denominator of exponent!
									 const float &fExpectedDistance,
									 const float &fMeanRow,
									 const float &fMeanCol
									 );

#endif
