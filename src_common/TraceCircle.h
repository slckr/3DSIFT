
//
// File: TraceCircle.cpp
// Desc: Traces a circle on a discrete grid.
// Dev: Copyright Matt Toews
//

#ifndef __TRACECIRCLE_H__
#define __TRACECIRCLE_H__

//
// TraceCircle.h
//
// Description:
//	Traces a digital circle in a 2D integer coordinate system,
// from a starting angle. At each point along the circle,
// a user-defined callback function is called.
//

// Function overriden by user

typedef int tcCallback( int iRow, int iCol, void *pArg );

// Traces line from start coord to finish coord
// Stops when callback function returns (0).

void
tcTraceCircle(
		  float fRowCenter,		// Circle center row
		  float fColCenter,		// Circle center col
		  float fRadius,		// Circle radius
		  tcCallback	*pProcess,		// Function to process pixel
		  void			*pArg			// Argument passed to FFCallback
);


#endif
