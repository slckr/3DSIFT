
#ifndef __ROTATIONINVARIANTDESCRIPTOR_H__
#define __ROTATIONINVARIANTDESCRIPTOR_H__

#define BINS_RAD 8

#include "PpImage.h"

class RotationInvariantDescriptor
{
public:

	RotationInvariantDescriptor();

	~RotationInvariantDescriptor();

	//
	// InitBins()
	//
	// Set pointer to bin data, set all bin counts to 0
	//
	int
	InitBins(
		float *pfBins
		);

	float *
	GetBins(
	);

	int
	AddVote(
		float fOri, // Orientation, 0..2PI
		float fRad, // Radius
		float fMag = 1.0f // Magnitude of vote
	);

	//
	// OutputToTextFile()
	// 
	// Write to a text file.
	//
	int
	OutputToTextFile(
		char *pcFileName
		);

	//
	// OutputToImage()
	// 
	// Write to an image to visualize
	//
	int
	OutputToImage(
		PpImage &ppImg
	);

private:

	float *m_pfBinPointers[BINS_RAD]; // 0, 1, 2, 4, 8, 16, 32, 64

	// Global variables 

	static float m_pfRadBinThresholds[BINS_RAD];

};

#endif
