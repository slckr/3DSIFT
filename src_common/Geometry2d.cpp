
#include <math.h>
#include <assert.h>
#include "Geometry2d.h"

#define PI 3.1415926536

int
g2dMakeLine(
			float *pfDxDy, // float array dx dy
			float *pfXY, // float array x y 
			g2dLine line // line
			)
{
	line[0][0] = pfXY[0];
	line[0][1] = pfXY[1];

	line[1][0] = pfXY[0]+pfDxDy[0];
	line[1][1] = pfXY[1]+pfDxDy[1];

	return 0;
}

// Find the intersection of a line and a circle
int
g2dInterceptLineCircle(
					   g2dLine line,
					   g2dCircle circle,
					   g2dPoint p1,
					   g2dPoint p2
			)
{
	float dx = line[1][0]-line[0][0];
	float dy = line[1][1]-line[0][1];
	if( dx == 0 && dy == 0 )
	{
		// degenerate line, no slope
		return -1;
	}

	// Circle coordinates: x, y, radius
	float xc = circle[0];
	float yc = circle[1];
	float rc = circle[2];

	// Line point coordinates
	float xl = line[0][0];
	float yl = line[0][1];

	if( fabs( dx ) < fabs( dy ) )
	{
		// x = my + b
		float m = dx / dy;
		float b = xl - m*yl;

		// set up quadratic intersection
		float qt = (b-xc);
		float qa = (1+m*m);
		float qb = 2.0f*(m*qt-yc);
		float qc = yc*yc + qt*qt - rc*rc;

		// roots are y coordinates
		if( g2dSolveQuad( qa, qb, qc, p1+1, p2+1 ) == 0 )
		{
			// calculate x coordinates
			p1[0] = m*p1[1]+b;
			p2[0] = m*p2[1]+b;
		}
		else
		{
			// no solution
			p1[0] = 0; p1[1] = 0;
			p2[0] = 0; p2[1] = 0;
			return -1;
		}


	}
	else
	{
		// y = mx + b
		float m = dy / dx;
		float b = yl - m*xl;

		// set up quadratic intersection
		float qt = (b-yc);
		float qa = (1+m*m);
		float qb = 2.0f*(m*qt-xc);
		float qc = xc*xc + qt*qt - rc*rc;

		// roots are x coordinates
		if( g2dSolveQuad( qa, qb, qc, p1+0, p2+0 ) == 0 )
		{
			// calculate y coordinates
			p1[1] = m*p1[0]+b;
			p2[1] = m*p2[0]+b;
		}
		else
		{
			// no solution
			p1[0] = 0; p1[1] = 0;
			p2[0] = 0; p2[1] = 0;
			return -1;
		}
	}
	return 0;
}

int
g2dSolveQuad(
			 float a,
			 float b,
			 float c,
			 float *px1,	// root 1
			 float *px2		// root 2
			 )
{
	float bb = b*b;
	float ac4 = 4.0f*a*c;
	float fchunk_sqr = bb - ac4;
	if( fchunk_sqr <= 0 || a == 0 )
	{
		// no valid roots, infinity
		return -1;
	}

	float fchunk = sqrt( fchunk_sqr );

	*px1 = (-b + fchunk) / (2.0f*a);
	*px2 = (-b - fchunk) / (2.0f*a);

	return 0;
}

// Find the intersection of two lines (could be infinity...)
int
g2dInterceptLineLine(
					   g2dLine *pline1,
					   g2dLine *pline2,
					   g2dPoint *pi1
			)
{
	// not implemented
	return -1;
}

int
g2dDistance(
					   g2dPoint p1,
					   g2dPoint p2,
					   float *pfDist
			)
{
	float f0 = p1[0]-p2[0];
	float f1 = p1[1]-p2[1];
	*pfDist = (float)sqrt(f0*f0+f1*f1);
	return 0;
}

//int
//g2dCircleOverlapArea(
//					 g2dCircle pc1,
//					 g2dCircle pc2,
//					 float *pfArea
//					 )
//{
//	float fDistC1C2;
//	g2dDistance( pc1, pc2, &fDistC1C2 );
//
//	// Algorithm in terms of a small and a big circle
//	float *pcSmall, *pcBig;
//	if( pc1[2] > pc2[2] )
//	{
//		pcSmall = pc1;
//		pcBig = pc2;
//	}
//	else
//	{
//		pcSmall = pc2;
//		pcBig = pc1;
//	}
//
//	if( fDistC1C2 <= pcSmall[2] + pcBig[2] )
//	{
//		// No overlap
//		*pfArea = 0;
//		return 0;
//	}
//	assert( 0);
//	return-1;
//}

float
g2dCircleOverlapArea(
			  float x1, float y1, float r1,
			  float x2, float y2, float r2
			  )
{
	float fRSml = r1 < r2 ? r1 : r2;
	float fRBig = r1 < r2 ? r2 : r1;
	float fRSmlsqr = fRSml*fRSml;
	float fRBigsqr = fRBig*fRBig;
	float dsqr = (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2);
	float d = 0;
	if( dsqr > 0 )
		d = sqrt( dsqr );
	if( d >= fRBig + fRSml )
	{
		// Small circle fully outside of big circle, no overlap
		return 0;
	}
	if( fRBig >= d + fRSml )
	{
		// Small circle fully within big circle
		return PI*fRSmlsqr;
	}
	float f1 = fRSmlsqr*acos( (dsqr + fRSmlsqr - fRBigsqr)/(2*d*fRSml)  );
	float f2 = fRBigsqr*acos( (dsqr + fRBigsqr - fRSmlsqr)/(2*d*fRBig)  );
	float f3 = -0.5*sqrt(
		(-d + fRSml + fRBig)*
		( d - fRSml + fRBig)*
		( d + fRSml - fRBig)*
		( d + fRSml + fRBig)
		);
	float fAreaUnion = f1+f2+f3;
	return fAreaUnion;
}
