
#include "diagonalgmm.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

#define PI 3.1415926535897932384626433832795

static int
sort_double(
		   const void *p1,
		   const void *p2
		   )
{
	double f1 = *((double*)p1);
	double f2 = *((double*)p2);
	if( f1 > f2 )
	{
		return -1;
	}
	else if( f1 < f2 )
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int
dgmmInit(
		 DIAG_GMM	&dgmm,
		 int		iComponents,
		 int		iFeaturesPerVector
		 )
{
	dgmm.iComponents		= iComponents;
	dgmm.iFeaturesPerVector = iFeaturesPerVector;
	dgmm.pfWeights			= new double[iComponents];
	dgmm.pfMeans			= new double[iComponents*iFeaturesPerVector];
	dgmm.pfVarriances		= new double[iComponents*iFeaturesPerVector];

	assert( dgmm.pfMeans );
	assert( dgmm.pfVarriances );
	assert( dgmm.pfWeights );

	if( dgmm.pfMeans == 0 || dgmm.pfVarriances == 0 || dgmm.pfWeights == 0 )
	{
		dgmm.pfMeans = 0;
		dgmm.pfVarriances = 0;
		dgmm.pfWeights = 0;
		dgmm.iComponents = 0;
		return 0;
	}

	return 1;
}

int
dgmmInitRandom(
		 DIAG_GMM	&dgmm,
		 DATASET	&ds,
		 int		iComponents
		 )
{
	assert( iComponents > 0 );
	assert( ds.iVectorCount > 0 );
	assert( ds.iFeaturesPerVector > 0 );
	assert( ds.pfVectors != 0 );

	if( !dgmmInit( dgmm, iComponents, ds.iFeaturesPerVector ) )
	{
		return 0;
	}

	// Array to flag vectors selected to initialize the mean
	int *piVectorFlag = new int[ds.iVectorCount];
	assert( piVectorFlag );
	memset( piVectorFlag, 0, sizeof(int)*ds.iVectorCount );

	for( int i = 0; i < iComponents; i++ )
	{
		// Init uniform weights
		dgmm.pfWeights[i] = 1.0f / (double)iComponents;

		// Select a random mean vector
		int iMeanVector = (int)((rand()*ds.iVectorCount) / (RAND_MAX + 1.0));
		while( piVectorFlag[iMeanVector] == 1 )
		{
			iMeanVector = (int)((rand()*ds.iVectorCount) / (RAND_MAX + 1.0));
		}
		// Flag mean vector as selected
		piVectorFlag[iMeanVector] = 1;

		for( int j = 0; j < ds.iFeaturesPerVector; j++ )
		{
			// Init mean vector
			dgmm.pfMeans[i*ds.iFeaturesPerVector + j] =
				ds.pfVectors[iMeanVector*ds.iFeaturesPerVector + j];

			// Init equal unit varriances
			dgmm.pfVarriances[i*ds.iFeaturesPerVector + j] = 1000.0;
		}
	}

	delete [] piVectorFlag;

	return 1;
}

int
dgmmInitBinaryInOutComponents(
		 DIAG_GMM	&dgmm,
		 DATASET	&ds,
		 int		iComponents
		 )
{
	assert( iComponents == 2 );
	assert( ds.iVectorCount > 0 );
	assert( ds.iFeaturesPerVector > 0 );
	assert( ds.pfVectors != 0 );

	if( !dgmmInit( dgmm, iComponents, ds.iFeaturesPerVector ) )
	{
		return 0;
	}

	for( int i = 0; i < 2; i++ )
	{
		// Init uniform weights
		dgmm.pfWeights[i] = 0.5f;

		// Select first vector of mean of both classes
		int iMeanVector = 0;

		for( int j = 0; j < ds.iFeaturesPerVector; j++ )
		{
			// Init mean vector
			dgmm.pfMeans[i*ds.iFeaturesPerVector + j] =
				ds.pfVectors[iMeanVector*ds.iFeaturesPerVector + j];

			// Make the variance of the first component half
			// that of the second (background)  component.
			dgmm.pfVarriances[i*ds.iFeaturesPerVector + j] = (i+1)*10000;
		}
	}

	return 1;
}

//
// dgmmInitDistance()
//
// Initialize dgmm components by choosing points the furthest away from 
// already instantiated components
//
//
int
dgmmInitDistance(
		 DIAG_GMM	&dgmm,
		 DATASET	&ds,
		 int		iComponents
		 )
{
	assert( iComponents > 0 );
	assert( ds.iVectorCount > 0 );
	assert( ds.iFeaturesPerVector > 0 );
	assert( ds.pfVectors != 0 );

	if( !dgmmInit( dgmm, iComponents, ds.iFeaturesPerVector ) )
	{
		return 0;
	}

	// Array to flag vectors selected to initialize the mean
	int *piVectorFlag = new int[ds.iVectorCount];
	assert( piVectorFlag );
	memset( piVectorFlag, 0, sizeof(int)*ds.iVectorCount );

	// Init original vector to the first in list
	int iComponent = 0;
	int iMeanVector = (int)((rand()*ds.iVectorCount) / (RAND_MAX + 1.0));
	for( int j = 0; j < ds.iFeaturesPerVector; j++ )
	{
		// Init mean vector
		dgmm.pfMeans[iComponent*ds.iFeaturesPerVector + j] =
			ds.pfVectors[iMeanVector*ds.iFeaturesPerVector + j];

		// Init equal unit varriances
		dgmm.pfVarriances[iComponent*ds.iFeaturesPerVector + j] = 100.0;
	}
	piVectorFlag[iMeanVector] = 1;

	// Choose the next mean as the vector the farthest from any in the list
	for( iComponent = 1; iComponent < iComponents; iComponent++ )
	{
		double dMaxCompDistSqr = 0;
		int iMaxVecIndex = -1;
		for( int iVec = 0; iVec < ds.iVectorCount; iVec++ )
		{
			double *pdCurrVec = ds.pfVectors + iVec*ds.iFeaturesPerVector;

			// Calculate the Euclidean distances from this vector to all 
			// components currently instantiated...
			double dMinCompDistSqr = 999e99;
			for( int c = 0; c < iComponent; c++ )
			{
				// Calculate distance from point to 
				double dCurrDistSqr = 0;
				double *pdCurrComp = dgmm.pfMeans + c*ds.iFeaturesPerVector;
				for( int iFeat = 0; iFeat < ds.iFeaturesPerVector; iFeat++ )
				{
					double dDiff = pdCurrComp[iFeat] - pdCurrVec[iFeat];
					double dDiffSqr = dDiff*dDiff;
					dCurrDistSqr += dDiffSqr;
				}

				// Save the minimum distance to a current component
				if( dCurrDistSqr < dMinCompDistSqr )
				{
					dMinCompDistSqr = dCurrDistSqr;
				}
				//dCompDistSum += sqrt( dCurrDistSqr );
			}

			// Save the vector with the greatest distance from all components instantiated
			// and which has not already been chosen
			if( dMinCompDistSqr > dMaxCompDistSqr )
			{
				if( piVectorFlag[iVec] == 1 )
				{
					dMaxCompDistSqr = dMaxCompDistSqr;
					iMaxVecIndex = iMaxVecIndex;
				}
				else
				{
					dMaxCompDistSqr = dMinCompDistSqr;
					iMaxVecIndex = iVec;
				}
			}
		}

		// Select a random mean vector
		iMeanVector = iMaxVecIndex;
		for( int j = 0; j < ds.iFeaturesPerVector; j++ )
		{
			// Init mean vector
			dgmm.pfMeans[iComponent*ds.iFeaturesPerVector + j] =
				ds.pfVectors[iMeanVector*ds.iFeaturesPerVector + j];
			
			// Init equal unit varriances
			dgmm.pfVarriances[iComponent*ds.iFeaturesPerVector + j] = 100.0;
		}
		piVectorFlag[iMeanVector] = 1;
	}

	// Init weights
	for( iComponent = 0; iComponent < iComponents; iComponent++ )
	{
		dgmm.pfWeights[iComponent] = 1.0f / (double)iComponents;
	}

	return 1;
}

int
dgmmTrain(
		 DIAG_GMM	&dgmm,
		 DATASET	&ds,
		 int		iIterations
		  )
{
	int iVector, iFeature, iComponent;

	double *pfLikeDivisor	= new double[dgmm.iComponents];
	double *pfLikeProbs		= new double[dgmm.iComponents*ds.iVectorCount];

	double *pfMeanSum		= new double[dgmm.iComponents*ds.iFeaturesPerVector];
	double *pfVarrSum		= new double[dgmm.iComponents*ds.iFeaturesPerVector];

	assert( pfLikeDivisor );
	assert( pfLikeProbs );
	assert( pfMeanSum );
	assert( pfVarrSum );

	FILE *probout = fopen( "like_prob.txt", "wt" );

	while( iIterations > 0 )
	{
		// Precalculate factors for likelihood
		printf( "-----------------------\n" );

		// 2-feature per vector data
		//printf( "%f %f %f %f %f\n", dgmm.pfWeights[0], dgmm.pfMeans[0], dgmm.pfMeans[1], dgmm.pfVarriances[0], dgmm.pfVarriances[1] );
		//printf( "%f %f %f %f %f\n", dgmm.pfWeights[1], dgmm.pfMeans[2], dgmm.pfMeans[3], dgmm.pfVarriances[2], dgmm.pfVarriances[3] );
		//printf( "%f %f %f %f %f\n", dgmm.pfWeights[2], dgmm.pfMeans[4], dgmm.pfMeans[5], dgmm.pfVarriances[4], dgmm.pfVarriances[5] );

		// 3-feature per fector data
		for( int i = 0; i < dgmm.iComponents; i++ )
		{
			int j;
			printf( "%f ", dgmm.pfWeights[i] );
			for( int j = 0; j < dgmm.iFeaturesPerVector; j++ )
			{
				printf( "%f ", dgmm.pfMeans[i*dgmm.iFeaturesPerVector + j] );
			}
			for( int j = 0; j < dgmm.iFeaturesPerVector; j++ )
			{
				printf( "%f ", dgmm.pfVarriances[i*dgmm.iFeaturesPerVector + j] );
			}
			printf( "\n" );
		}

		double dProb;
		dgmmDatasetProbability( dgmm, ds, 0, &dProb );
		printf( "%e \n", dProb );
		fprintf( probout, "%e \n", dProb );

		for( iComponent = 0; iComponent < dgmm.iComponents; iComponent++ )
		{
			double *pfCurrVarr		= dgmm.pfVarriances	+ iComponent*ds.iFeaturesPerVector;
			double fVarrProd = 1.0;
			for( iFeature = 0; iFeature < ds.iFeaturesPerVector; iFeature++ )
			{
				fVarrProd *= pfCurrVarr[iFeature];
			}
			double dTest = pow( 2.0*PI,ds.iFeaturesPerVector );

			pfLikeDivisor[iComponent] = (double)( 1.0 / sqrt( pow( 2.0*PI,ds.iFeaturesPerVector )*fVarrProd ) );
			assert( pfLikeDivisor[iComponent] > 0.0 );
		}

		// Calculate likelihoods

		for( iVector = 0; iVector < ds.iVectorCount; iVector++ )
		{
			double *pfCurrVector		= ds.pfVectors	+ iVector*ds.iFeaturesPerVector;
			double *pfCurrLikeProbs	= pfLikeProbs	+ iVector*dgmm.iComponents;
			double fPosteriorSum		= 0.0;

			for( iComponent = 0; iComponent < dgmm.iComponents; iComponent++ )
			{
				// Sum up exponent

				double *pfCurrMeans		= dgmm.pfMeans		+ iComponent*ds.iFeaturesPerVector;
				double *pfCurrVarr		= dgmm.pfVarriances	+ iComponent*ds.iFeaturesPerVector;
				double *pfCurrWeights	= dgmm.pfWeights;
				
				double fExponentSum = 0.0;

				for( iFeature = 0; iFeature < ds.iFeaturesPerVector; iFeature++ )
				{
					double fDiff = pfCurrVector[iFeature] - pfCurrMeans[iFeature];
					fDiff *= fDiff;
					fDiff /= pfCurrVarr[iFeature];
					fExponentSum += fDiff;
				}

				fExponentSum *= -0.5;

				// This is likely to go to zero - we should have a way of
				pfCurrLikeProbs[iComponent] = (double)
					(pfCurrWeights[iComponent] * exp( fExponentSum ) * pfLikeDivisor[iComponent]);

				assert( pfCurrLikeProbs[iComponent] >= 0.0 );

				fPosteriorSum += pfCurrLikeProbs[iComponent];
			}

			// Normalize component mixture

			assert( fPosteriorSum > 0.0 );
			fPosteriorSum = (double)(1.0 / fPosteriorSum);
			//assert( fPosteriorSum != 0.0 );
			for( iComponent = 0; iComponent < dgmm.iComponents; iComponent++ )
			{
				pfCurrLikeProbs[iComponent] *= fPosteriorSum;
				//assert( pfCurrLikeProbs[iComponent] != 0.0 );
			}
		}

		// Zero sum arrays

		for( iComponent = 0; iComponent < dgmm.iComponents; iComponent++ )
		{
			dgmm.pfWeights[iComponent] = 0;

			double *pfCurrMeanSumVector = pfMeanSum + iComponent*ds.iFeaturesPerVector;
			double *pfCurrVarrSumVector = pfVarrSum + iComponent*ds.iFeaturesPerVector;
			
			for( iFeature = 0; iFeature < ds.iFeaturesPerVector; iFeature++ )
			{
				pfCurrMeanSumVector[iFeature] = 0;
				pfCurrVarrSumVector[iFeature] = 0;
			}
		}

		// Sum up likelihood probabilities for each component

		for( iVector = 0; iVector < ds.iVectorCount; iVector++ )
		{
			double *pfCurrVector		= ds.pfVectors	+ iVector*ds.iFeaturesPerVector;
			double *pfCurrLikeProbs	= pfLikeProbs	+ iVector*dgmm.iComponents;

			for( iComponent = 0; iComponent < dgmm.iComponents; iComponent++ )
			{
				dgmm.pfWeights[iComponent] += pfCurrLikeProbs[iComponent];

				double *pfCurrMeanSumVector = pfMeanSum + iComponent*ds.iFeaturesPerVector;
				double *pfCurrVarrSumVector = pfVarrSum + iComponent*ds.iFeaturesPerVector;

				for( iFeature = 0; iFeature < ds.iFeaturesPerVector; iFeature++ )
				{
					pfCurrMeanSumVector[iFeature]	+= pfCurrLikeProbs[iComponent]*pfCurrVector[iFeature];
					pfCurrVarrSumVector[iFeature]	+= pfCurrLikeProbs[iComponent]*pfCurrVector[iFeature]*pfCurrVector[iFeature];
				}
			}
		}

		// Update parameters

		for( iComponent = 0; iComponent < dgmm.iComponents; iComponent++ )
		{
			double *pfCurrMeanSumVector = pfMeanSum + iComponent*ds.iFeaturesPerVector;
			double *pfCurrVarrSumVector = pfVarrSum + iComponent*ds.iFeaturesPerVector;

			double *pfCurrMeanVector = dgmm.pfMeans		+ iComponent*ds.iFeaturesPerVector;
			double *pfCurrVarrVector = dgmm.pfVarriances	+ iComponent*ds.iFeaturesPerVector;
			
			for( iFeature = 0; iFeature < ds.iFeaturesPerVector; iFeature++ )
			{
				pfCurrMeanVector[iFeature] = pfCurrMeanSumVector[iFeature] / dgmm.pfWeights[iComponent];
				pfCurrVarrVector[iFeature] = (pfCurrVarrSumVector[iFeature] / dgmm.pfWeights[iComponent])
					- (pfCurrMeanVector[iFeature]*pfCurrMeanVector[iFeature]);

				if( pfCurrVarrVector[iFeature] <= 0.0 ) { pfCurrVarrVector[iFeature] = 0.00001; }
				//assert( pfCurrVarrVector[iFeature] > 0.0 );
			}
			
			dgmm.pfWeights[iComponent] = dgmm.pfWeights[iComponent] / (double)ds.iVectorCount;
		}
		iIterations--;
	}

	fclose( probout );

	delete [] pfMeanSum;
	delete [] pfVarrSum;

	delete [] pfLikeProbs;
	delete [] pfLikeDivisor;

	return 1;
}

//
// Uses logarithmic values to prevent underflow. Should produce the same
// results as dgmmTrain().
//
int
dgmmTrainLog(
		 DIAG_GMM	&dgmm,
		 DATASET	&ds,
		 int		iIterations
		  )
{
	int iVector, iFeature, iComponent;

	double *pfLikeDivisor	= new double[dgmm.iComponents];
	double *pfLikeDivisorL	= new double[dgmm.iComponents];
	double *pfLikeProbs		= new double[dgmm.iComponents*ds.iVectorCount];
	double *pfLikeProbsL	= new double[dgmm.iComponents*ds.iVectorCount];

	double *pfMeanSum		= new double[dgmm.iComponents*ds.iFeaturesPerVector];
	double *pfVarrSum		= new double[dgmm.iComponents*ds.iFeaturesPerVector];

	assert( pfLikeDivisor );
	assert( pfLikeProbs );
	assert( pfMeanSum );
	assert( pfVarrSum );

	FILE *probout = fopen( "like_prob.txt", "wt" );

	while( iIterations > 0 )
	{
		// Precalculate factors for likelihood
		printf( "-----------------------\n" );

		// 2-feature per vector data
		//printf( "%f %f %f %f %f\n", dgmm.pfWeights[0], dgmm.pfMeans[0], dgmm.pfMeans[1], dgmm.pfVarriances[0], dgmm.pfVarriances[1] );
		//printf( "%f %f %f %f %f\n", dgmm.pfWeights[1], dgmm.pfMeans[2], dgmm.pfMeans[3], dgmm.pfVarriances[2], dgmm.pfVarriances[3] );
		//printf( "%f %f %f %f %f\n", dgmm.pfWeights[2], dgmm.pfMeans[4], dgmm.pfMeans[5], dgmm.pfVarriances[4], dgmm.pfVarriances[5] );

		// 3-feature per fector data
		for( int i = 0; i < dgmm.iComponents; i++ )
		{
			int j;
			printf( "%f ", dgmm.pfWeights[i] );
			for( int j = 0; j < dgmm.iFeaturesPerVector; j++ )
			{
				printf( "%f ", dgmm.pfMeans[i*dgmm.iFeaturesPerVector + j] );
			}
			for( int j = 0; j < dgmm.iFeaturesPerVector; j++ )
			{
				printf( "%f ", dgmm.pfVarriances[i*dgmm.iFeaturesPerVector + j] );
			}
			printf( "\n" );
		}

		double dProb;
		dgmmDatasetProbability( dgmm, ds, 0, &dProb );
		printf( "%e \n", dProb );
		fprintf( probout, "%e \n", dProb );

		for( iComponent = 0; iComponent < dgmm.iComponents; iComponent++ )
		{
			double *pfCurrVarr		= dgmm.pfVarriances	+ iComponent*ds.iFeaturesPerVector;
			double fVarrProd = 1.0;
			double fVarrProdLog = 0;
			for( iFeature = 0; iFeature < ds.iFeaturesPerVector; iFeature++ )
			{
				fVarrProd *= pfCurrVarr[iFeature];
				fVarrProdLog += log( pfCurrVarr[iFeature] );
			}

			pfLikeDivisor[iComponent]  = (double)( 1.0 / sqrt( pow( 2.0*PI,ds.iFeaturesPerVector )*fVarrProd ) );
			pfLikeDivisorL[iComponent] = (double)-.5*( log(2.0*PI)*ds.iFeaturesPerVector+fVarrProdLog );
			assert( pfLikeDivisor[iComponent] > 0.0 );
		}

		// Calculate likelihoods

		for( iVector = 0; iVector < ds.iVectorCount; iVector++ )
		{
			double *pfCurrVector		= ds.pfVectors	+ iVector*ds.iFeaturesPerVector;
			double *pfCurrLikeProbs	= pfLikeProbs	+ iVector*dgmm.iComponents;
			double *pfCurrLikeProbsL	= pfLikeProbsL	+ iVector*dgmm.iComponents;
			double fPosteriorSum		= 0.0;

			int iMaxLogSet = 0;
			double dMaxLog = 0;

			for( iComponent = 0; iComponent < dgmm.iComponents; iComponent++ )
			{
				// Sum up exponent

				double *pfCurrMeans		= dgmm.pfMeans		+ iComponent*ds.iFeaturesPerVector;
				double *pfCurrVarr		= dgmm.pfVarriances	+ iComponent*ds.iFeaturesPerVector;
				double *pfCurrWeights	= dgmm.pfWeights;
				
				double fExponentSum = 0.0;

				for( iFeature = 0; iFeature < ds.iFeaturesPerVector; iFeature++ )
				{
					double fDiff = pfCurrVector[iFeature] - pfCurrMeans[iFeature];
					fDiff *= fDiff;
					fDiff /= pfCurrVarr[iFeature];
					fExponentSum += fDiff;
				}

				fExponentSum *= -0.5;

				// This is likely to go to zero - we should have a way of
				pfCurrLikeProbs[iComponent] = (double)
					(pfCurrWeights[iComponent] * exp( fExponentSum ) * pfLikeDivisor[iComponent]);
				pfCurrLikeProbsL[iComponent] = (double)
					(log(pfCurrWeights[iComponent])+fExponentSum+pfLikeDivisorL[iComponent]);

				assert( pfCurrLikeProbs[iComponent] >= 0.0 );
				assert( pfCurrLikeProbsL[iComponent] <= 0.0 );

				if( iMaxLogSet == 0 || pfCurrLikeProbsL[iComponent] > dMaxLog )
				{
					iMaxLogSet = 1;
					dMaxLog = pfCurrLikeProbsL[iComponent];
				}

				fPosteriorSum += pfCurrLikeProbs[iComponent];
			}

			double dLogLikeProbSum = 0;
			// Remove max log from log components (same as dividing)
			for( iComponent = 0; iComponent < dgmm.iComponents; iComponent++ )
			{
				pfCurrLikeProbsL[iComponent] -= dMaxLog;
				pfCurrLikeProbsL[iComponent] = exp(pfCurrLikeProbsL[iComponent]);
				dLogLikeProbSum += pfCurrLikeProbsL[iComponent];
			}

			// Normalize component mixture

			assert( dLogLikeProbSum > 0.0 );
			fPosteriorSum = (double)(1.0 / dLogLikeProbSum);
			//assert( fPosteriorSum != 0.0 );
			for( iComponent = 0; iComponent < dgmm.iComponents; iComponent++ )
			{
				pfCurrLikeProbs[iComponent] = pfCurrLikeProbsL[iComponent]*fPosteriorSum;
			}
		}

		// Zero sum arrays

		for( iComponent = 0; iComponent < dgmm.iComponents; iComponent++ )
		{
			dgmm.pfWeights[iComponent] = 0;

			double *pfCurrMeanSumVector = pfMeanSum + iComponent*ds.iFeaturesPerVector;
			double *pfCurrVarrSumVector = pfVarrSum + iComponent*ds.iFeaturesPerVector;
			
			for( iFeature = 0; iFeature < ds.iFeaturesPerVector; iFeature++ )
			{
				pfCurrMeanSumVector[iFeature] = 0;
				pfCurrVarrSumVector[iFeature] = 0;
			}
		}

		// Sum up likelihood probabilities for each component

		for( iVector = 0; iVector < ds.iVectorCount; iVector++ )
		{
			double *pfCurrVector		= ds.pfVectors	+ iVector*ds.iFeaturesPerVector;
			double *pfCurrLikeProbs	= pfLikeProbs	+ iVector*dgmm.iComponents;

			for( iComponent = 0; iComponent < dgmm.iComponents; iComponent++ )
			{
				dgmm.pfWeights[iComponent] += pfCurrLikeProbs[iComponent];

				double *pfCurrMeanSumVector = pfMeanSum + iComponent*ds.iFeaturesPerVector;
				double *pfCurrVarrSumVector = pfVarrSum + iComponent*ds.iFeaturesPerVector;

				for( iFeature = 0; iFeature < ds.iFeaturesPerVector; iFeature++ )
				{
					pfCurrMeanSumVector[iFeature]	+= pfCurrLikeProbs[iComponent]*pfCurrVector[iFeature];
					pfCurrVarrSumVector[iFeature]	+= pfCurrLikeProbs[iComponent]*pfCurrVector[iFeature]*pfCurrVector[iFeature];
				}
			}
		}

		// Update parameters

		for( iComponent = 0; iComponent < dgmm.iComponents; iComponent++ )
		{
			double *pfCurrMeanSumVector = pfMeanSum + iComponent*ds.iFeaturesPerVector;
			double *pfCurrVarrSumVector = pfVarrSum + iComponent*ds.iFeaturesPerVector;

			double *pfCurrMeanVector = dgmm.pfMeans		+ iComponent*ds.iFeaturesPerVector;
			double *pfCurrVarrVector = dgmm.pfVarriances	+ iComponent*ds.iFeaturesPerVector;
			
			for( iFeature = 0; iFeature < ds.iFeaturesPerVector; iFeature++ )
			{
				pfCurrMeanVector[iFeature] = pfCurrMeanSumVector[iFeature] / dgmm.pfWeights[iComponent];
				pfCurrVarrVector[iFeature] = (pfCurrVarrSumVector[iFeature] / dgmm.pfWeights[iComponent])
					- (pfCurrMeanVector[iFeature]*pfCurrMeanVector[iFeature]);

				if( pfCurrVarrVector[iFeature] <= 0.0 ) { pfCurrVarrVector[iFeature] = 0.00001; }
				//assert( pfCurrVarrVector[iFeature] > 0.0 );
			}
			
			dgmm.pfWeights[iComponent] = dgmm.pfWeights[iComponent] / (double)ds.iVectorCount;
		}
		iIterations--;
	}

	fclose( probout );

	delete [] pfMeanSum;
	delete [] pfVarrSum;

	delete [] pfLikeDivisorL;
	delete [] pfLikeProbsL;
	delete [] pfLikeProbs;
	delete [] pfLikeDivisor;

	return 1;
}

//
// dgmmVectorProbabilityComponent()
//
// Returns the probability of the vector comming from the
// iComponent component.
//
int
dgmmVectorProbabilityComponent(
		 DIAG_GMM	&dgmm,
		 int		iComponent,
		 double		*pfVector,
		 double		*pfProb
		  )
{
	int iFeature;

	double *pfCurrMean = dgmm.pfMeans		+ iComponent*dgmm.iFeaturesPerVector;
	double *pfCurrVarr = dgmm.pfVarriances	+ iComponent*dgmm.iFeaturesPerVector;
	
	double fVarrProd = 1.0;
	double fExponentSum = 0.0;
	
	for( iFeature = 0; iFeature < dgmm.iFeaturesPerVector; iFeature++ )
	{
		double fDiff = pfVector[iFeature] - pfCurrMean[iFeature];
		fDiff *= fDiff;
		assert( pfCurrVarr[iFeature] != 0 );
		fDiff /= pfCurrVarr[iFeature];
		fExponentSum += fDiff;
		
		fVarrProd *= pfCurrVarr[iFeature];
	}
	
	fExponentSum *= -0.5;
	double fGaussianDivisor = (double)( 1.0 / sqrt( pow( 2.0*PI,dgmm.iFeaturesPerVector )*fVarrProd ) );
	double pfCompProb = (double) (dgmm.pfWeights[iComponent] * exp( fExponentSum ) * fGaussianDivisor );
	
	assert( pfCompProb >= 0.0 );
	

	*pfProb = pfCompProb;

	return 1;
}

int
dgmmVectorProbability(
		 DIAG_GMM	&dgmm,
		 double		*pfVector,
		 double		*pfProb
		  )
{
	int iFeature, iComponent;

	double fProbSum = 0.0;

	for( iComponent = 0; iComponent < dgmm.iComponents; iComponent++ )
	{
		double *pfCurrMean = dgmm.pfMeans		+ iComponent*dgmm.iFeaturesPerVector;
		double *pfCurrVarr = dgmm.pfVarriances	+ iComponent*dgmm.iFeaturesPerVector;

		double fVarrProd = 1.0;
		double fExponentSum = 0.0;

		for( iFeature = 0; iFeature < dgmm.iFeaturesPerVector; iFeature++ )
		{
			double fDiff = pfVector[iFeature] - pfCurrMean[iFeature];
			fDiff *= fDiff;
			assert( pfCurrVarr[iFeature] != 0 );
			fDiff /= pfCurrVarr[iFeature];
			fExponentSum += fDiff;

			fVarrProd *= pfCurrVarr[iFeature];
		}

		fExponentSum *= -0.5;
		double fGaussianDivisor = (double)( 1.0 / sqrt( pow( 2.0*PI,dgmm.iFeaturesPerVector )*fVarrProd ) );
		double pfCompProb = (double) (dgmm.pfWeights[iComponent] * exp( fExponentSum ) * fGaussianDivisor );

		assert( pfCompProb >= 0.0 );

		fProbSum += pfCompProb;
	}

	*pfProb = fProbSum;

	return 1;
}

int
dgmmDatasetProbability(
		 DIAG_GMM	&dgmm,
		 DATASET	&ds,
		 double		*pfProbs,
		 double		*pfProbSum
		  )
{
	int iVector;

	double fProbSum = 0;

	for( iVector = 0; iVector < ds.iVectorCount; iVector++ )
	{
		double *pfCurrVector = ds.pfVectors + iVector*ds.iFeaturesPerVector;
		double fProb;
		if( !dgmmVectorProbability( dgmm, pfCurrVector, &fProb ))
		{
			return 0;
		}
		if( pfProbs != 0 )
		{
			pfProbs[iVector] = fProb;
		}
		fProbSum += fProb;
	}
	
	if( pfProbSum != 0 )
	{
		*pfProbSum = fProbSum;
	}

	return 1;
}

int
dgmmRead(
		  char		*pcFileName,
		  DIAG_GMM	&dgmm
		  )
{
	FILE *infile = fopen( pcFileName, "rt" );
	if( !infile )
	{
		return 0;
	}

	// Component count
	fscanf( infile, "%d\n", &dgmm.iComponents );
	// features per vector
	fscanf( infile, "%d\n", &dgmm.iFeaturesPerVector );

	if( !dgmmInit( dgmm, dgmm.iComponents, dgmm.iFeaturesPerVector ) )
	{
		return 0;
	}

	// components weights
	for( int i = 0; i < dgmm.iComponents; i++ )
	{
		float fData;
		fscanf( infile, "%e\t", &fData );
		dgmm.pfWeights[i] = (double)fData;
	}

	// means
	for( int i = 0; i < dgmm.iComponents; i++ )
	{
		for( int j = 0; j < dgmm.iFeaturesPerVector; j++ )
		{
			float fData;
			fscanf( infile, "%e\t", &fData );
			dgmm.pfMeans[i*dgmm.iFeaturesPerVector + j] = (double)fData;
		}
	}

	// varriances
	for( int i = 0; i < dgmm.iComponents; i++ )
	{
		for( int j = 0; j < dgmm.iFeaturesPerVector; j++ )
		{
			float fData;
			fscanf( infile, "%e\t", &fData );
			dgmm.pfVarriances[i*dgmm.iFeaturesPerVector + j] = (double)fData;
		}
	}

	fclose( infile );
	return 1;
}

int
dgmmWrite(
		  char		*pcFileName,
		  DIAG_GMM	&dgmm
		  )
{
	FILE *outfile = fopen( pcFileName, "wt" );
	if( !outfile )
	{
		return 0;
	}

	// Write component count
	fprintf( outfile, "%d\n", dgmm.iComponents );
	// Write features per vector
	fprintf( outfile, "%d\n", dgmm.iFeaturesPerVector );

	// Write components weights
	for( int i = 0; i < dgmm.iComponents; i++ )
	{
		fprintf( outfile, "%e\t", dgmm.pfWeights[i] );
	}
	fprintf( outfile, "\n" );

	// Write means
	for( int i = 0; i < dgmm.iComponents; i++ )
	{
		for( int j = 0; j < dgmm.iFeaturesPerVector; j++ )
		{
			fprintf( outfile, "%e\t", dgmm.pfMeans[i*dgmm.iFeaturesPerVector + j] );
		}
		fprintf( outfile, "\n" );
	}

	// Write varriances
	for( int i = 0; i < dgmm.iComponents; i++ )
	{
		for( int j = 0; j < dgmm.iFeaturesPerVector; j++ )
		{
			fprintf( outfile, "%e\t", dgmm.pfVarriances[i*dgmm.iFeaturesPerVector + j] );
		}
		fprintf( outfile, "\n" );
	}

	fclose( outfile );
	return 1;
}

int
dgmmDelete(
		 DIAG_GMM &dgmm
		 )
{
	if( dgmm.pfMeans != 0 )
	{
		delete [] dgmm.pfMeans;
	}
	if( dgmm.pfVarriances != 0 )
	{
		delete [] dgmm.pfVarriances;
	}
	if( dgmm.pfWeights != 0 )
	{
		delete [] dgmm.pfWeights;
	}

	dgmm.pfMeans = 0;
	dgmm.pfVarriances = 0;
	dgmm.pfWeights = 0;
	dgmm.iComponents = 0;

	return 1;
}

