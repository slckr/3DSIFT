
#ifndef __CLUSTERUTILS_H__
#define __CLUSTERUTILS_H__

typedef float SAMPLE_TYPE;

//
// ITERATION_FUNCTION()
//
// Function to be called at each iteration of the clustering algorithm.
//
typedef void ITERATION_FUNCTION(
								int iIteration,
								SAMPLE_TYPE *pcluster_centers,
								unsigned char *pcluster_indicies,
								void *pData
								);

typedef void ITERATION_FUNCTION_SIFT(
				  int iIteration,
				  int iFeatures,
				  int iClusters,
				  unsigned char *pcluster_centers, // SIFT cluster centers (128)
				  int *pcluster_indicies,	// For each feature, cluster indices
				  int *pcluster_counts, // For each cluster, count
				  void *pData
								);

//
// cuInitCluster()
//
// Initializes the cluster centers before clustering. Generating intelligent
// starting clusters improves the performace of the clustering algorithm.
// *This must be called to initialize array of feature cluster centers before
// calling cuCMeansCluster() or cuKMeansCluster().
//
void
cuInitCluster(
			  SAMPLE_TYPE *samples,	// Array of raw features
			  const int vec_size,		// Number of features per vector
			  const int num_samples,	// Number of vectors
			  const int num_clusters,	// Number of clusters
			  SAMPLE_TYPE *pcluster_centers,	// Array of feature cluster centers
			  unsigned char* cluster				// Array of indicies mapping vectors to cluster centers
			  );

//
// cuInitRandomClusters()
//
// Initializes the cluster centers to random feature vectors before clustering.
// *This must be called to initialize array of feature cluster centers before
// calling cuCMeansCluster() or cuKMeansCluster().
//
void
cuInitRandomClusters(
			  SAMPLE_TYPE *samples,	// Array of raw features
			  const int vec_size,		// Number of features per vector
			  const int num_samples,	// Number of vectors
			  const int num_clusters,	// Number of clusters
			  SAMPLE_TYPE *pcluster_centers,	// Array of feature cluster centers
			  unsigned char* cluster				// Array of indicies mapping vectors to cluster centers
			  );


//
// cuInitSeparatedClusters()
//
// Initializes a set of maximally separated clusters. Algorithm: 1) choose a 
// cluster at random from samples, 2) choose a new cluster from samples that is
// maximally separated from the set of clusters 3) repeat step 2 until 
// set of clusters reaches size num_clusters.
//
void
cuInitSeparatedClusters(
			  SAMPLE_TYPE *samples,	// Array of raw features
			  const int vec_size,		// Number of features per vector
			  const int num_samples,	// Number of vectors
			  const int num_clusters,	// Number of clusters to generate
			  SAMPLE_TYPE *pcluster_centers,	// Array of feature cluster centers
			  int *picluster_center_indices = 0,	// optional array of indices into samples
			  ITERATION_FUNCTION ifFunc = 0,
			void *pData = 0,
			int iIterations = 0
			  );

//
// cuAssignClusters()
//
// Assign nearest clusters, count the number of samples assigned to each cluster,
// and maintain an array of indices indicating to which cluster each sample
// is assigned.
//
void
cuAssignClusters(
			  SAMPLE_TYPE *samples,	// Array of raw features
			  const int vec_size,		// Number of features per vector
			  const int num_samples,	// Number of vectors
			  const int num_clusters,	// Number of clusters
			  SAMPLE_TYPE *pcluster_centers,	// Array of feature cluster centers
			  int *picluster_counts,		// counts for each cluster
			  int *picluster_indices,	// indices for each cluster
			  float *pfcluster_distances	// distances for each cluster
			  );

//
// cuCMeansCluster()
//
// Uses the fuzzy C means algorithm to cluster feature vectors. Returns the cluster
// centers found, and fills out an index array with cluster index values.
//
int
cuCMeansCluster(
				SAMPLE_TYPE *pfeatures,		// Array of raw features
				const int features_per_vector,		// Number of features per vector
				const int vectors,						// Number of vectors
				const int clusters,					// Number of clusters
				SAMPLE_TYPE *pcluster_centers,				// Array of feature cluster centers
				unsigned char *pcluster_indicies,	// Array of indicies mapping feature vectors to cluster centers
				ITERATION_FUNCTION ifFunc,
				void *pData
				);

//
// cuKMeansCluster()
//
// Uses the K means algorithm to cluster feature vectors. Returns the cluster
// centers found, and fills out an index array with cluster index values.
//
int
cuKMeansCluster(
				SAMPLE_TYPE *samples,	// Array of raw features
				const int vec_size,		// Number of features per vector
				const int num_samples,	// Number of vectors
				const int num_clusters,	// Number of clusters
				SAMPLE_TYPE *pcluster_centers,	// Array of feature cluster centers
				unsigned char* pcluster_indicies, // Array of indicies mapping vectors to cluster centers
				ITERATION_FUNCTION ifFunc,
				void *pData,
				int iIterations = 100
				);

//
// Special for many SIFT features
//
int
cuKMeansClusterSIFT(
				unsigned char *samples,	// Array of raw features
				const int vec_size,		// Number of features per vector (128)
				const int num_samples,	// Number of vectors
				const int num_clusters,	// Number of clusters
				unsigned char *pcluster_centers,	// Array of feature cluster centers
				int *pcluster_indicies, // Array of indicies mapping vectors to cluster centers
				ITERATION_FUNCTION_SIFT ifFunc,
				void *pData,
				int iIterations = 100
				);

//
// cuAssignNearestClusters()
//
// Assigns the nearest cluster to each feature vector.
//
void
cuAssignNearestClusters(
						const SAMPLE_TYPE *pfeatures,	// Array of raw features
						const int features_per_vector,	// Number of features per vector
						const int vectors,					// Number of vectors
						const int clusters,				// Number of clusters
						const SAMPLE_TYPE *pcluster_centers,		// Array of feature cluster centers
						unsigned char* pcluster_indicies	// Array of indicies mapping vectors to cluster centers
						);

//
// cuAssignNearestClusters()
//
// Assigns the nearest cluster to each feature vector, generates statistics
// on row/col position of clusters in an image.
//
void
cuAssignNearestClusters(
						const SAMPLE_TYPE *pfeatures,	// Array of raw features
						const int features_per_vector,	// Number of features per vector
						const int rows,					// Number of rows
						const int cols,					// Number of cols
						const int clusters,				// Number of clusters
						const SAMPLE_TYPE *pcluster_centers,		// Array of feature cluster centers
						unsigned int *pClusterRowSum,	// Sum of pixel rows
						unsigned int *pClusterColSum,// Sum of pixel cols
						unsigned int *pClusterPixelCount, // Pixel count
						unsigned char* pcluster_indicies	// Array of indicies mapping vectors to cluster centers
						);

//
// cuAssignProbabilisticClusters()
//
// Assigns probabilistic 
//
//
void
cuAssignProbabilisticClusters(
			const SAMPLE_TYPE *pfeatures,		// Array of raw features
			const int features_per_vector,		// Number of features per vector
			const int vectors,
			const int clusters,					// Number of clusters
			const SAMPLE_TYPE *pcluster_centers,	// Array of feature cluster centers
			SAMPLE_TYPE *pprobabilities,		// Array of cluster probabilities
			unsigned char *pucClusterIndices
			   );

void
cuAssignProbabilisticClustersGaussian(
			const SAMPLE_TYPE *pfeatures,		// Array of raw features
			const int features_per_vector,		// Number of features per vector
			const int vectors,
			const int clusters,					// Number of clusters
			const SAMPLE_TYPE *pcluster_centers,	// Array of feature cluster centers
			SAMPLE_TYPE *pprobabilities,		// Array of cluster probabilities
			unsigned char *pucClusterIndices
			   );

//
// cuAssignClusterDistance()
//
// Assigns distances for each vector to it's cluster center. Can be used
// to see where the clusters fit the least.
//
void
cuAssignClusterDistances(
			const SAMPLE_TYPE *pfeatures,		// Array of raw features
			const int features_per_vector,		// Number of features per vector
			const int vectors,

			const int clusters,						// Number of clusters
			const SAMPLE_TYPE *pcluster_centers,	// Array of feature cluster centers

			SAMPLE_TYPE *pdistances					// Array of cluster distance
			);

#endif
