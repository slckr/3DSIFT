
#ifndef __GAUSSIANBLURFEATURES_H__
#define  __GAUSSIANBLURFEATURES_H__

#include "PpImage.h"

typedef struct _FEATURE_STRUCT
{
	PpImage ppImgIn;
	PpImage ppImgFeatures;
	int iFeaturesPerVector;
	float fBlurSigma;
	int iSubSample;
} FEATURE_STRUCT;

int
generateFeaturesGaussian(
						 FEATURE_STRUCT &fsStruct
							);

//
// generateFeaturesGaussianEdge()
//
// Features composed of edge magnitude weighted by Gaussian window.
//
int
generateFeaturesGaussianEdge(
						 FEATURE_STRUCT &fsStruct
							);

//
// generateFeaturesGaussian()
//
// Returns an array of feature vectors
//
float *
generateFeaturesGaussian(
							const PpImage	&ppImgIn,		// Image of 8-bit greyscale
							const int		&iBlurLevels,	// Levels of blurring (features per vector)
							float			fSigma1,		// Initial blurring sigma
							int				iSubSample = 1	// Subsampling
							);

//
// convertFeaturesDiffOfGauss()
//
// Converts Gaussian features created with generateFeaturesGaussian()
// to difference of Gaussian features
//
int
convertFeaturesDiffOfGauss(
			   float *pfFeatures,
			   int iFeaturesPerVector,
			   int iVectors
			   );

int
reverseFeatures(
			   float *pfFeatures,
			   int iFeaturesPerVector,
			   int iVectors
			   );

//
// deleteFeaturesGaussian()
//
// Deletes an array of feature vectors created by generateFeaturesGaussian()
//
int
deleteFeaturesGaussian(
			   float *pfFeatures
			   );

#endif

