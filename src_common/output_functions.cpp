
#include "output_functions.h"

#include <string.h>
#include <math.h>
#include <stdio.h>
#include <assert.h>

#define PI 3.1415926535897932384626433832795

#include "TraceLine.h"
#include "TraceCircle.h"
#include "PpImageFloatOutput.h"

typedef struct _DRAW_LINE_STRUCT
{
	PpImage *pImg;
	unsigned char pucRGB[3];

	float fAlphaValue;	// Blending value from 1.0 (100% pucRGB)  to 0.0 (100% pImgSrc)
	PpImage *pImgSrc;	// Source image for alpha value blending
} DRAW_LINE_STRUCT;

int
drawLineCallback(
				 int iRow,
				 int iCol,
				 void *pArg
				 )
{
	
	DRAW_LINE_STRUCT *pDLS = (DRAW_LINE_STRUCT*)pArg;
	
	if( iRow < 0 )
	{
		iRow = 0;
	}
	if( iRow >= pDLS->pImg->Rows() )
	{
		iRow = pDLS->pImg->Rows() - 1;
	}
	if( iCol < 0 )
	{
		iCol = 0;
	}
	if( iCol >= pDLS->pImg->Cols() )
	{
		iCol = pDLS->pImg->Cols() - 1;
	}

	int iBytesPerPixel = pDLS->pImg->BitsPerPixel() / 8;
	unsigned char *pucPixel = pDLS->pImg->ImageRow( iRow ) + iCol*iBytesPerPixel;

	unsigned char *pucPixelSrc = pDLS->pImgSrc->ImageRow( iRow ) + iCol*iBytesPerPixel;

	for( int iByte = 0; iByte < iBytesPerPixel; iByte++ )
	{
		// Calculated blended alpha value
		float fPixel =
			       (pDLS->fAlphaValue)*pDLS->pucRGB[iByte]
			+ (1.0f-pDLS->fAlphaValue)*pucPixelSrc[iByte];
		//pucPixel[iByte] = pDLS->pucRGB[iByte];
		pucPixel[iByte] = fPixel;
	}
	return 1;
}

int
drawCircleCallback(
				 int iRow,
				 int iCol,
				 void *pArg
				 )
{
	
	DRAW_LINE_STRUCT *pDLS = (DRAW_LINE_STRUCT*)pArg;
	
	if( iRow < 0 )
	{
		iRow = 0;
	}
	if( iRow >= pDLS->pImg->Rows() )
	{
		iRow = pDLS->pImg->Rows() - 1;
	}
	if( iCol < 0 )
	{
		iCol = 0;
	}
	if( iCol >= pDLS->pImg->Cols() )
	{
		iCol = pDLS->pImg->Cols() - 1;
	}
	
	int iBytesPerPixel = pDLS->pImg->BitsPerPixel() / 8;

	if( iBytesPerPixel == 4 )
	{
		float *pfPixel = (float*)pDLS->pImg->ImageRow( iRow );
		pfPixel[iCol] = (float)pDLS->pucRGB[0];
	}
	else if( iBytesPerPixel == 1 || iBytesPerPixel == 3 )
	{
		unsigned char *pucPixel = pDLS->pImg->ImageRow( iRow ) + iCol*iBytesPerPixel;
		unsigned char *pucPixelSrc = pDLS->pImgSrc->ImageRow( iRow ) + iCol*iBytesPerPixel;
		for( int iByte = 0; iByte < iBytesPerPixel; iByte++ )
		{
			// Calculated blended alpha value
			float fPixel =
					(pDLS->fAlphaValue)*pDLS->pucRGB[iByte]
				+ (1.0f-pDLS->fAlphaValue)*pucPixelSrc[iByte];
			//pucPixel[iByte] = pDLS->pucRGB[iByte];
			pucPixel[iByte] = fPixel;
		}
	}
	else if( iBytesPerPixel == 2 )
	{
		unsigned char *pucPixel = pDLS->pImg->ImageRow( iRow ) + iCol*iBytesPerPixel;
		unsigned short* psRow0 = (unsigned short*)pucPixel;
		psRow0[0] = (0xFF00&psRow0[0]) || 0xFF;
	}
	return 1;
}


//
// output_transform_neighbours()
//
// Outputs an image of the transform neighbourhood relationships
//
int
output_transform_neighbours(
							const PpImage &ppImgTr,		// Transform
							const PpImage &ppImgDomain,	// Image over domain
							const int &iMult,				// Tells how big to stretch the output domain
							char *pcFileName,				// File name
							int bOverlayOnImage			// If true, overlays arrows on ppImgDomain
							)
{
	// Output color image
	
	PpImage ppImgOut;
	ppImgOut.Initialize( ppImgDomain.Rows()*iMult, ppImgDomain.Cols()*iMult, ppImgDomain.Cols()*iMult*sizeof(RGBCHAR), sizeof(RGBCHAR)*8 );
	memset( ppImgOut.ImageRow(0), 0, ppImgOut.Rows()*ppImgOut.RowInc() );
	
	int r, c, iClique;
	DRAW_LINE_STRUCT dls;
	dls.pImg = &ppImgOut;
	dls.pImgSrc = &ppImgOut;
	
	if( bOverlayOnImage )
	{
		for( r = 0; r < ppImgOut.Rows(); r++ )
		{
			unsigned char *pcGrayRow = ppImgDomain.ImageRow( r/iMult );
			RGBCHAR *prgbRow = (RGBCHAR *)ppImgOut.ImageRow(r);
			for( int c = 0; c < ppImgOut.Cols(); c++ )
			{
				prgbRow[c][0] = pcGrayRow[c];
				prgbRow[c][1] = pcGrayRow[c];
				prgbRow[c][2] = pcGrayRow[c];
			}
		}
	}
	
	// Draw lines connecting nodes in green
	
	dls.pucRGB[0] = 0;
	dls.pucRGB[1] = 255;
	dls.pucRGB[2] = 0;
	
	TRANS_STRUCT *ptsBase = (TRANS_STRUCT*)ppImgTr.ImageRow(0);
	
	for( r = 0; r < ppImgTr.Rows(); r++ )
	{
		TRANS_STRUCT *ptsRowCur = (TRANS_STRUCT*)ppImgTr.ImageRow(r);
		
		for( int c = 0; c < ppImgTr.Cols(); c++ )
		{
			TRANS_STRUCT &tsCurr = ptsRowCur[c];
			int iR1 = (int)(ptsRowCur[c].pt2dI2.fRow*iMult + 0.5);
			int iC1 = (int)(ptsRowCur[c].pt2dI2.fCol*iMult + 0.5);
			
			for( iClique = 0; iClique < tsCurr.i2Cliques; iClique++ )
			{
				TRANS_STRUCT &tsNeigh = ptsBase[ tsCurr.piNeighbours[iClique] ];
				int iR2 = (int)(tsNeigh.pt2dI2.fRow*iMult + 0.5);
				int iC2 = (int)(tsNeigh.pt2dI2.fCol*iMult + 0.5);
				tlTraceLine( iR1, iC1, iR2, iC2, drawLineCallback, (void*)&dls );
			}
			
			//ppImgOut.WriteToFile( "shit.pgm" );
			//memset( ppImgOut.ImageRow(0), 0, ppImgOut.Rows()*ppImgOut.RowInc() );
			//for( iClique = 0; iClique < tsCurr.i3Cliques; iClique++ )
			//{
			//	TRANS_STRUCT &tsPtNeig1 = ptsBase[ tsCurr.piNeighbours[ tsCurr.pip3Cliques[iClique][0] ] ];
			//	TRANS_STRUCT &tsPtNeig2 = ptsBase[ tsCurr.piNeighbours[ tsCurr.pip3Cliques[iClique][1] ] ];
			//	int iR1 = (int)(tsPtNeig1.pt2dI2.fRow*iMult + 0.5);
			//	int iC1 = (int)(tsPtNeig1.pt2dI2.fCol*iMult + 0.5);
			//	int iR2 = (int)(tsPtNeig2.pt2dI2.fRow*iMult + 0.5);
			//	int iC2 = (int)(tsPtNeig2.pt2dI2.fCol*iMult + 0.5);
			//	tlTraceLine( iR1, iC1, iR2, iC2, drawLineCallback, (void*)&dls );
			//	ppImgOut.WriteToFile( "shit.pgm" );
			//	memset( ppImgOut.ImageRow(0), 0, ppImgOut.Rows()*ppImgOut.RowInc() );
			//}
			
		}
	}
	
	// Draw nodes in red
	
	dls.pucRGB[0] = 255;
	dls.pucRGB[1] = 0;
	dls.pucRGB[2] = 0;
	
	for( r = 0; r < ppImgTr.Rows(); r++ )
	{
		TRANS_STRUCT *ptsRowCur = (TRANS_STRUCT*)ppImgTr.ImageRow(r);
		
		for( int c = 0; c < ppImgTr.Cols(); c++ )
		{
			int iRow = (int)(ptsRowCur[c].pt2dI2.fRow*iMult + 0.5);
			int iCol = (int)(ptsRowCur[c].pt2dI2.fCol*iMult + 0.5);
			
			if( iRow < 0 )
			{
				iRow = 0;
			}
			if( iRow >= ppImgOut.Rows() )
			{
				iRow = ppImgOut.Rows() - 1;
			}
			if( iCol < 0 )
			{
				iCol = 0;
			}
			if( iCol >= ppImgOut.Cols() )
			{
				iCol = ppImgOut.Cols() - 1;
			}
			
			unsigned char *pucImgRow = ppImgOut.ImageRow(iRow);
			
			pucImgRow[3*iCol+0] = dls.pucRGB[0];
			pucImgRow[3*iCol+1] = dls.pucRGB[1];
			pucImgRow[3*iCol+2] = dls.pucRGB[2];
		}
	}
	
	ppImgOut.WriteToFile( pcFileName );
	
	return 0;
}

int
output_transform_neighbours(
							const LOCATION_VALUE_ARRAY &lvaLocations,
							const int iLocations,
							const PpImage &ppImgNeighbours,
							const PpImage &ppImgDomain,	 // Image over domain
							const int &iMultOutput, // Tells how big to stretch output
							const int &iMult, // Tells how big to stretch the locations
							char *pcFileName
							)
{
	// Output color image
	
	PpImage ppImgOut;
	ppImgOut.Initialize( ppImgDomain.Rows()*iMultOutput, ppImgDomain.Cols()*iMultOutput, ppImgDomain.Cols()*sizeof(RGBCHAR)*iMultOutput, sizeof(RGBCHAR)*8 );
	memset( ppImgOut.ImageRow(0), 0, ppImgOut.Rows()*ppImgOut.RowInc() );
	
	int r, c, i;
	DRAW_LINE_STRUCT dls;
	dls.pImg = &ppImgOut;
	dls.pImgSrc = &ppImgOut;

	for( r = 0; r < ppImgOut.Rows(); r++ )
	{
		unsigned char *pcGrayRow = ppImgDomain.ImageRow( r/iMultOutput );
		RGBCHAR *prgbRow = (RGBCHAR *)ppImgOut.ImageRow(r);
		for( int c = 0; c < ppImgOut.Cols(); c++ )
		{
			prgbRow[c][0] = pcGrayRow[c/iMultOutput];
			prgbRow[c][1] = pcGrayRow[c/iMultOutput];
			prgbRow[c][2] = pcGrayRow[c/iMultOutput];
		}
	}
	
	// Draw lines connecting nodes in green
	
	dls.pucRGB[0] = 0;
	dls.pucRGB[1] = 255;
	dls.pucRGB[2] = 0;

	for( int i = 0; i < iLocations; i++ )
	{
		int iR1 = lvaLocations.plvs[i].iRow*iMult*iMultOutput;
		int iC1 = lvaLocations.plvs[i].iCol*iMult*iMultOutput;

		unsigned char *pucNeighbours = ppImgNeighbours.ImageRow(i);

		for( int j = 0; j < iLocations; j++ )
		{
			if( pucNeighbours[j] )
			{
				int iR2 = lvaLocations.plvs[j].iRow*iMult*iMultOutput;
				int iC2 = lvaLocations.plvs[j].iCol*iMult*iMultOutput;
				tlTraceLine( iR1, iC1, iR2, iC2, drawLineCallback, (void*)&dls );
			}
		}
	}

	
	// Draw nodes in red
	
	dls.pucRGB[0] = 255;
	dls.pucRGB[1] = 0;
	dls.pucRGB[2] = 0;

	for( int i = 0; i < iLocations; i++ )
	{
		int iRow = lvaLocations.plvs[i].iRow*iMult*iMultOutput;
		int iCol = lvaLocations.plvs[i].iCol*iMult*iMultOutput;

		if( iRow < 0 )
		{
			iRow = 0;
		}
		if( iRow >= ppImgOut.Rows() )
		{
			iRow = ppImgOut.Rows() - 1;
		}
		if( iCol < 0 )
		{
			iCol = 0;
		}
		if( iCol >= ppImgOut.Cols() )
		{
			iCol = ppImgOut.Cols() - 1;
		}
		
		unsigned char *pucImgRow = ppImgOut.ImageRow(iRow);
		
		pucImgRow[3*iCol+0] = dls.pucRGB[0];
		pucImgRow[3*iCol+1] = dls.pucRGB[1];
		pucImgRow[3*iCol+2] = dls.pucRGB[2];
	}
	
	ppImgOut.WriteToFile( pcFileName );
	
	return 0;
}

int
output_transform_bubbles(
							const LOCATION_VALUE_ARRAY &lvaLocations,
							const int iLocations,
							const PpImage &ppImgNeighbours,
							const PpImage &ppImgDomain,	 // Image over domain
							const int &iMultOutput, // Tells how big to stretch output
							const int &iMult, // Tells how big to stretch the locations
							char *pcFileName,				// File name
							unsigned char *pucRGB
							)
{
	// Output color image
	
	PpImage ppImgOut;
	ppImgOut.Initialize( ppImgDomain.Rows()*iMultOutput, ppImgDomain.Cols()*iMultOutput, ppImgDomain.Cols()*sizeof(RGBCHAR)*iMultOutput, sizeof(RGBCHAR)*8 );
	memset( ppImgOut.ImageRow(0), 0, ppImgOut.Rows()*ppImgOut.RowInc() );
	
	int r, c, i;
	DRAW_LINE_STRUCT dls;
	dls.pImg = &ppImgOut;
	dls.pImgSrc = &ppImgOut;

	for( r = 0; r < ppImgOut.Rows(); r++ )
	{
		unsigned char *pcGrayRow = ppImgDomain.ImageRow( r/iMultOutput );
		RGBCHAR *prgbRow = (RGBCHAR *)ppImgOut.ImageRow(r);
		for( int c = 0; c < ppImgOut.Cols(); c++ )
		{
			prgbRow[c][0] = pcGrayRow[c/iMultOutput];
			prgbRow[c][1] = pcGrayRow[c/iMultOutput];
			prgbRow[c][2] = pcGrayRow[c/iMultOutput];
		}
	}
	
	
	if( pucRGB )
	{
		dls.pucRGB[0] = pucRGB[0];
		dls.pucRGB[1] = pucRGB[1];
		dls.pucRGB[2] = pucRGB[2];
	}
	else
	{// Draw lines connecting nodes in red
		dls.pucRGB[0] = 255;
		dls.pucRGB[1] = 0;
		dls.pucRGB[2] = 0;
	}
	for( int i = 0; i < iLocations; i++ )
	{
		int iRow = lvaLocations.plvs[i].iRow*iMult*iMultOutput;
		int iCol = lvaLocations.plvs[i].iCol*iMult*iMultOutput;
		tcTraceCircle( iRow, iCol, 8, drawCircleCallback, &dls );
		RGBCHAR *prgbRow = (RGBCHAR *)ppImgOut.ImageRow(iRow);
		// Add a little green dot right in the center
		prgbRow[iCol][0] = 0;
		prgbRow[iCol][1] = 255;
		prgbRow[iCol][2] = 0;
	}

	ppImgOut.WriteToFile( pcFileName );
	
	return 0;
}

int
output_transform_bubbles(
							const LOCATION_VALUE_XYZ_ARRAY &lvaLocations,
							const int iLocations,
							const PpImage &ppImgNeighbours,
							const PpImage &ppImgDomain,	 // Image over domain
							const int &iMultOutput, // Tells how big to stretch output
							const int &iMult, // Tells how big to stretch the locations
							char *pcFileName,				// File name
							unsigned char *pucRGB
							)
{
	// Output color image

	PpImage ppImgOut;
	ppImgOut.Initialize( ppImgDomain.Rows()*iMultOutput, ppImgDomain.Cols()*iMultOutput, ppImgDomain.Cols()*sizeof(RGBCHAR)*iMultOutput, sizeof(RGBCHAR)*8 );
	memset( ppImgOut.ImageRow(0), 0, ppImgOut.Rows()*ppImgOut.RowInc() );
	
	int r, c, i;
	DRAW_LINE_STRUCT dls;
	dls.pImg = &ppImgOut;
	dls.pImgSrc = &ppImgOut;
	dls.fAlphaValue = 1.0f;

	for( r = 0; r < ppImgOut.Rows(); r++ )
	{
		unsigned char *pcGrayRow = ppImgDomain.ImageRow( r/iMultOutput );
		RGBCHAR *prgbRow = (RGBCHAR *)ppImgOut.ImageRow(r);
		for( int c = 0; c < ppImgOut.Cols(); c++ )
		{
			prgbRow[c][0] = pcGrayRow[c/iMultOutput];
			prgbRow[c][1] = pcGrayRow[c/iMultOutput];
			prgbRow[c][2] = pcGrayRow[c/iMultOutput];
		}
	}
	
	
	if( pucRGB )
	{
		dls.pucRGB[0] = pucRGB[0];
		dls.pucRGB[1] = pucRGB[1];
		dls.pucRGB[2] = pucRGB[2];
	}
	else
	{// Draw lines connecting nodes in red
		dls.pucRGB[0] = 255;
		dls.pucRGB[1] = 0;
		dls.pucRGB[2] = 0;
	}
	for( int i = 0; i < iLocations; i++ )
	{
		int iRow = lvaLocations.plvz[i].y*iMult*iMultOutput;
		int iCol = lvaLocations.plvz[i].x*iMult*iMultOutput;
		tcTraceCircle( iRow, iCol, lvaLocations.plvz[i].z, drawCircleCallback, &dls );
		RGBCHAR *prgbRow = (RGBCHAR *)ppImgOut.ImageRow(iRow);
		// Add a little green dot right in the center
		prgbRow[iCol][0] = 0;
		prgbRow[iCol][1] = 255;
		prgbRow[iCol][2] = 0;
	}

	ppImgOut.WriteToFile( pcFileName );
	
	return 0;
}

int
output_transform_bubbles_old(
							const LOCATION_VALUE_XYZ_ARRAY &lvaLocations,
							const int iLocations,
							const PpImage &ppImgNeighbours,
							const PpImage &ppImgDomain,	 // Image over domain
							const int &iMultOutput, // Tells how big to stretch output
							const int &iMult, // Tells how big to stretch the locations
							char *pcFileName,				// File name
							unsigned char *pucRGB
							)
{
	LOCATION_VALUE_ARRAY lvaArray;

	lvaArray.plvs = new LOCATION_VALUE[lvaLocations.iCount];
	assert( lvaArray.plvs );

	lvaArray.iRow = lvaLocations.lvzOrigin.y;
	lvaArray.iCol = lvaLocations.lvzOrigin.x;
	lvaArray.fEntropy = lvaLocations.lvzOrigin.fValue;
	lvaArray.ilpCount = lvaLocations.iCount;

	for( int i = 0; i < lvaLocations.iCount; i++ )
	{
		lvaArray.plvs[i].fProb = lvaLocations.plvz[i].fValue;
		lvaArray.plvs[i].iRow = lvaLocations.plvz[i].y;
		lvaArray.plvs[i].iCol = lvaLocations.plvz[i].x;
	}

	int iReturn = output_transform_bubbles( lvaArray, lvaArray.ilpCount,
							ppImgNeighbours,
							ppImgDomain,
							iMultOutput,
							iMult,
							pcFileName
							);

	delete [] lvaArray.plvs;

	return iReturn;
}

//
// output_transform_arrows()
//
// Outputs an image of the transform arrows from their position
// in image 1 to image 2
//
int
output_transform_arrows(
						const PpImage &ppImgTr,		// Transform
						const PpImage &ppImgDomain,	// Tells how big the output domain is
						const int &iMult,				// Tells how big to stretch the output domain
						char *pcFileName,				// File name
						int bOverlayOnImage			// If true, overlays arrows on ppImgDomain
						)
{
	// Output color image
	
	PpImage ppImgOut;
	ppImgOut.Initialize( ppImgDomain.Rows()*iMult, ppImgDomain.Cols()*iMult, ppImgDomain.Cols()*iMult*sizeof(RGBCHAR), sizeof(RGBCHAR)*8 );
	memset( ppImgOut.ImageRow(0), 0, ppImgOut.Rows()*ppImgOut.RowInc() );
	
	int r, c;
	DRAW_LINE_STRUCT dls;
	dls.pImg = &ppImgOut;
	dls.pImgSrc = &ppImgOut;
	
	if( bOverlayOnImage )
	{
		for( r = 0; r < ppImgOut.Rows(); r++ )
		{
			unsigned char *pcGrayRow = ppImgDomain.ImageRow( r/iMult );
			RGBCHAR *prgbRow = (RGBCHAR *)ppImgOut.ImageRow(r);
			for( int c = 0; c < ppImgOut.Cols(); c++ )
			{
				prgbRow[c][0] = pcGrayRow[c];
				prgbRow[c][1] = pcGrayRow[c];
				prgbRow[c][2] = pcGrayRow[c];
			}
		}
	}
	
	// Draw lines connecting nodes in green
	
	dls.pucRGB[0] = 0;
	dls.pucRGB[1] = 255;
	dls.pucRGB[2] = 0;
	
	for( r = 0; r < ppImgTr.Rows(); r++ )
	{
		TRANS_STRUCT *ptsRowCur =
			(TRANS_STRUCT*)ppImgTr.ImageRow(r + 0);
		
		for( int c = 0; c < ppImgTr.Cols(); c++ )
		{
			int iR1 = (int)(ptsRowCur[c].pt2dI1.fRow*iMult + 0.5);
			int iC1 = (int)(ptsRowCur[c].pt2dI1.fCol*iMult + 0.5);
			int iR2 = (int)(ptsRowCur[c].pt2dI2.fRow*iMult + 0.5);
			int iC2 = (int)(ptsRowCur[c].pt2dI2.fCol*iMult + 0.5);
			
			tlTraceLine( iR1, iC1, iR2, iC2, drawLineCallback, (void*)&dls );
			
			unsigned char *pucImgRow;
			
			if( iR1 < 0 ) { iR1 = 0; }
			if( iR2 < 0 ) { iR2 = 0; }
			if( iC1 < 0 ) { iC1 = 0; }
			if( iC2 < 0 ) { iC2 = 0; }
			
			if( iR1 >= ppImgOut.Rows() ) { iR1 = ppImgOut.Rows() - 1; }
			if( iR2 >= ppImgOut.Rows() ) { iR2 = ppImgOut.Rows() - 1; }
			if( iC1 >= ppImgOut.Cols() ) { iC1 = ppImgOut.Cols() - 1; }
			if( iC2 >= ppImgOut.Cols() ) { iC2 = ppImgOut.Cols() - 1; }
			
			// Point image 1 point in red
			pucImgRow = ppImgOut.ImageRow(iR1);
			pucImgRow[3*iC1+0] = 255;
			pucImgRow[3*iC1+1] = 0;
			pucImgRow[3*iC1+2] = 0;
			
			// Paint image 2 point in blue
			pucImgRow = ppImgOut.ImageRow(iR2);
			pucImgRow[3*iC2+0] = 0;
			pucImgRow[3*iC2+1] = 0;
			pucImgRow[3*iC2+2] = 255;
			
		}
	}
	
	ppImgOut.WriteToFile( pcFileName );
	
	return 0;
}

int
output_list_float(
				  const PpImage &ppImg,
				  const LOCATION_VALUE *plp,
				  const int &iLocationProbs,
				  char *pcFileName
				  )
{
	PpImage ppImgOut;
	ppImgOut.Initialize( ppImg.Rows(), ppImg.Cols(), ppImg.Cols()*sizeof(float), sizeof(float)*8 );
	memset( ppImgOut.ImageRow(0), 0, ppImgOut.Rows()*ppImgOut.RowInc() );
	
	for( int i = 0; i < iLocationProbs; i++ )
	{
		int r = plp[i].iRow;
		int c = plp[i].iCol;
		((float*)(ppImgOut.ImageRow(r)))[c] = plp[i].fProb;
	}
	
	output_float( ppImgOut, pcFileName );
	return 1;
}

int
output_neighbourhood(
					 const PpImage &ppImgTr,
					 const int &iRow,
					 const int &iCol,
					 char *pcFileName
					 )
{
	PpImage ppImgOut;
	ppImgOut.Initialize( ppImgTr.Rows(), ppImgTr.Cols(), ppImgTr.Cols()*sizeof(float), 8*sizeof(float) );
	memset( ppImgOut.ImageRow(0), 0, ppImgOut.Rows()*ppImgOut.RowInc() );
	
	for( int r = iRow - 1; r <= iRow + 1; r++ )
	{
		TRANS_STRUCT *ptsRow = (TRANS_STRUCT*)ppImgTr.ImageRow(r);
		for( int c = iCol - 1; c <= iCol + 1; c++ )
		{
			bilinear_interpolation_paint_float( ppImgOut, 1.0f, ptsRow[c].pt2dI2.fRow, ptsRow[c].pt2dI2.fCol );
		}
	}
	
	output_float( ppImgOut, pcFileName );
	
	return 1;
}

int
of_paint_white_circle(
					  int iRow,
					  int iCol,
					  int iRadius,
					  PpImage &ppImgOut
			 )
{
	unsigned char pucRGB[3];
	pucRGB[0] = 255;
	pucRGB[1] = 255;
	pucRGB[2] = 255;
	return of_paint_circle( iRow, iCol, iRadius, ppImgOut, pucRGB );
}

int
of_paint_keypoint(
				  float fRow,
				  float fCol,
				  float fRadius,
				  float fOrientation,
				  PpImage &ppImgOut,
				  unsigned char *pucRGB,
				  float fAlphaValue,
				  PpImage *ppImgSrc
				  )
{
	of_paint_circle(
		fRow + 0.5, fCol + 0.5,
		fRadius + 0.5, ppImgOut, pucRGB, fAlphaValue, ppImgSrc
					);

	of_paint_line_length_orientation(
		fRow, fCol, fRadius, fOrientation, ppImgOut, pucRGB, fAlphaValue, ppImgSrc
		);

	return 1;
}

int
of_paint_grey_circle(
					  int iRow,
					  int iCol,
					  int iRadius,
					  PpImage &ppImgOut
			 )
{
	unsigned char pucRGB[3];
	pucRGB[0] = 127;
	pucRGB[1] = 127;
	pucRGB[2] = 127;
	return of_paint_circle( iRow, iCol, iRadius, ppImgOut, pucRGB );
}

int
of_paint_black_circle(
					  int iRow,
					  int iCol,
					  int iRadius,
					  PpImage &ppImgOut
			 )
{
	unsigned char pucRGB[3];
	pucRGB[0] = 0;
	pucRGB[1] = 0;
	pucRGB[2] = 0;
	return of_paint_circle( iRow, iCol, iRadius, ppImgOut, pucRGB );
}

int
of_paint_circle(
					  int iRow,
					  int iCol,
					  int iRadius,
					  PpImage &ppImgOut,
					  unsigned char *pucRGB,
					  float fAlphaValue,
				  PpImage *ppImgSrc
			 )
{
	DRAW_LINE_STRUCT dls;
	dls.pImg = &ppImgOut;
	dls.pImgSrc = &ppImgOut;
	if( ppImgSrc )
	{
		dls.pImgSrc = ppImgSrc;
	}

	if( pucRGB )
	{
		dls.pucRGB[0] = pucRGB[0];
		dls.pucRGB[1] = pucRGB[1];
		dls.pucRGB[2] = pucRGB[2];
	}
	else
	{
		dls.pucRGB[0] = 255;
		dls.pucRGB[1] = 255;
		dls.pucRGB[2] = 255;
	}

	dls.fAlphaValue = fAlphaValue;

	tcTraceCircle(
		iRow, iCol,
		iRadius, drawCircleCallback, &dls
			);
	return 1;
}

int
of_paint_line(
			  int iRow1,
			  int iCol1,
			  int iRow2,
			  int iCol2,
			  PpImage &ppImgOut,
					  unsigned char *pucRGB,
					  float fAlphaValue,
				  PpImage *ppImgSrc
			  )
{
	DRAW_LINE_STRUCT dls;
	dls.pImg = &ppImgOut;
	dls.pImgSrc = &ppImgOut;
	if( ppImgSrc )
	{
		dls.pImgSrc = ppImgSrc;
	}

	if( pucRGB )
	{
		dls.pucRGB[0] = pucRGB[0];
		dls.pucRGB[1] = pucRGB[1];
		dls.pucRGB[2] = pucRGB[2];
	}
	else
	{
		dls.pucRGB[0] = 255;	// White by default
		dls.pucRGB[1] = 255;
		dls.pucRGB[2] = 255;
	}

	dls.fAlphaValue = fAlphaValue;

	tlTraceLine(
		iRow1, iCol1, iRow2, iCol2,
		drawCircleCallback, &dls
			);
	//tlTraceLine(
	//	iRow1, iCol1, iRow2, iCol2,
	//	drawLineCallback, &dls
	//		);
	return 1;
}

int
of_paint_line_length_orientation(
			  float fRow,
			  float fCol,
			  float fLength,
			  float fOrientation,
			  PpImage &ppImgOut,
					  unsigned char *pucRGB,
					  float fAlphaValue,
				  PpImage *ppImgSrc
			 )
{
	int iRow1 = fRow + 0.5;
	int iCol1 = fCol + 0.5;

	int iRow2 = (int)(fLength*sin(fOrientation) + fRow + 0.5);
	int iCol2 = (int)(fLength*cos(fOrientation) + fCol + 0.5);

	of_paint_line(
			  iRow1, iCol1,
			  iRow2, iCol2,
			  ppImgOut,
			  pucRGB, fAlphaValue, ppImgSrc
			  );

	return 1;
}

static void
output_slices(
			  FEATUREIO &fio,			// Feature image
			  LOCATION_VALUE_XYZ &lv,	// Location of feature
			  int iFeature,				// Index of feature to ouput
			  PpImage &ppImgFloat,		// Float image
			  PpImage &ppImgChar,		// Char image
			  char *pcFileNameBase		
			  )
{
	char pcFileName[300];

	ppImgFloat.Initialize( fio.y, fio.x, fio.x*sizeof(float), sizeof(float)*8 );
	fioFeatureSliceXY( fio, lv.z, iFeature, (float*)ppImgFloat.ImageRow(0) );
	output_float( ppImgFloat, ppImgChar );
	sprintf( pcFileName, "%s_xy.pgm", pcFileNameBase );
	of_paint_white_circle( lv.y, lv.x, 5, ppImgChar );
	ppImgChar.WriteToFile( pcFileName );

	if( fio.z > 1 )
	{
		ppImgFloat.Initialize( fio.z, fio.x, fio.x*sizeof(float), sizeof(float)*8 );
		fioFeatureSliceZX( fio, lv.y, iFeature, (float*)ppImgFloat.ImageRow(0) );
		output_float( ppImgFloat, ppImgChar );
		sprintf( pcFileName, "%s_zx.pgm", pcFileNameBase );
		of_paint_white_circle( lv.z, lv.x, 5, ppImgChar );
		ppImgChar.WriteToFile( pcFileName );

		ppImgFloat.Initialize( fio.z, fio.y, fio.y*sizeof(float), sizeof(float)*8 );
		fioFeatureSliceZY( fio, lv.x, iFeature, (float*)ppImgFloat.ImageRow(0) );
		output_float( ppImgFloat, ppImgChar );
		sprintf( pcFileName, "%s_zy.pgm", pcFileNameBase );
		of_paint_white_circle( lv.z, lv.y, 5, ppImgChar );
		ppImgChar.WriteToFile( pcFileName );
	}
}

int
output_lvarray_slices(
				  FEATUREIO &fio,
				  LOCATION_VALUE_XYZ_ARRAY &lva,
				  int iFeature,				// Index of feature to ouput
				  char *pcFileNameBase,
				  int iPoints
			   )
{
	char pcFileNameBase2[300];
	PpImage ppImgFloat;
	PpImage ppImgChar;

	LOCATION_VALUE_XYZ_ARRAY &lvaCurr = lva;

	if( iPoints == -1 )
	{
		iPoints = lvaCurr.iCount;
	}

	for( int i = 0; i < iPoints; i++ )
	{
		LOCATION_VALUE_XYZ &lv = lvaCurr.plvz[i];
		sprintf( pcFileNameBase2, "%s_%2.2d", pcFileNameBase, i );
		output_slices( fio, lv, 0, ppImgFloat, ppImgChar, pcFileNameBase2 );
	}
	return 1;
}

void
output_lvcollection_slices(
				  FEATUREIO &fio1,
				  FEATUREIO &fio2,
				  LOCATION_VALUE_XYZ_COLLECTION &lvcMarginals,	// Array of likelihoods in fio2
				  char *pcFileNameBase
				  )
{
	char pcFileNameBase2[300];
	PpImage ppImgFloat;
	PpImage ppImgChar;

	for( int i = 0; i < lvcMarginals.iCount; i++ )
	{
		LOCATION_VALUE_XYZ_ARRAY &lvaCurr = lvcMarginals.plvza[i];
		sprintf( pcFileNameBase2, "%s_src_%2.2d", pcFileNameBase, i );
		output_slices( fio1, lvaCurr.lvzOrigin, 0, ppImgFloat, ppImgChar, pcFileNameBase2 );

		sprintf( pcFileNameBase2, "%s_dst_%2.2d", pcFileNameBase, i );
		output_lvarray_slices( fio2, lvaCurr, 0, pcFileNameBase2, 5 );

		if( fio2.z == 1 )
		{
			sprintf( pcFileNameBase2, "%s_all_points_%2.2d", pcFileNameBase, i );
			output_lvarray_points_2d( fio2, lvaCurr, 0, pcFileNameBase2 );
		}
		else
		{
			sprintf( pcFileNameBase2, "%s_all_points_%2.2d", pcFileNameBase, i );
			output_lvarray_points( fio2, lvaCurr, 0, pcFileNameBase2 );
		}
	}
}

int
output_lvarray_points_2d(
				  FEATUREIO &fio,
				  LOCATION_VALUE_XYZ_ARRAY &lva,
				  int iFeature,				// Index of feature to ouput
				  char *pcFileNameBase
			   )
{
	assert( fio.z == 1 );
	if( fio.z > 1 )
	{
		return 0;
	}

	PpImage ppImgFloat;
	PpImage ppImgChar;

	char pcFileName[300];

	ppImgFloat.Initialize( fio.y, fio.x, fio.x*sizeof(float), sizeof(float)*8 );
	fioFeatureSliceXY( fio, 0, iFeature, (float*)ppImgFloat.ImageRow(0) );
	output_float( ppImgFloat, "test_float.pgm" );
	output_float( ppImgFloat, ppImgChar );
	sprintf( pcFileName, "%s_xy.pgm", pcFileNameBase );

	ppImgChar.WriteToFile( "test.pgm" );

	for( int i = 0; i < lva.iCount; i++ )
	{
		LOCATION_VALUE_XYZ &lv = lva.plvz[i];

		(ppImgChar.ImageRow( lv.y ))[lv.x] = 0;
	}

	ppImgChar.WriteToFile( pcFileName );

	return 1;
}

int
output_lvarray_points(
				  FEATUREIO &fio,
				  LOCATION_VALUE_XYZ_ARRAY &lva,
				  int iFeature,				// Index of feature to ouput
				  char *pcFileNameBase
			   )
{
	PpImage ppImgFloat;
	PpImage ppImgChar;

	char pcFileName[300];

	ppImgFloat.Initialize( fio.y, fio.x, fio.x*sizeof(float), sizeof(float)*8 );
	fioFeatureSliceXY( fio, 50, iFeature, (float*)ppImgFloat.ImageRow(0) );
	output_float( ppImgFloat, ppImgChar );
	sprintf( pcFileName, "%s_xy.pgm", pcFileNameBase );

	for( int i = 0; i < lva.iCount; i++ )
	{
		LOCATION_VALUE_XYZ &lv = lva.plvz[i];

		(ppImgChar.ImageRow( lv.y ))[lv.x] = 255;
	}

	ppImgChar.WriteToFile( pcFileName );

	if( fio.z > 1 )
	{
		ppImgFloat.Initialize( fio.z, fio.x, fio.x*sizeof(float), sizeof(float)*8 );
		fioFeatureSliceZX( fio, 50, iFeature, (float*)ppImgFloat.ImageRow(0) );
		output_float( ppImgFloat, ppImgChar );
		sprintf( pcFileName, "%s_zx.pgm", pcFileNameBase );
		
		for( int i = 0; i < lva.iCount; i++ )
		{
			LOCATION_VALUE_XYZ &lv = lva.plvz[i];

			(ppImgChar.ImageRow( lv.z ))[lv.x] = 255;
		}

		ppImgChar.WriteToFile( pcFileName );

		ppImgFloat.Initialize( fio.z, fio.y, fio.y*sizeof(float), sizeof(float)*8 );
		fioFeatureSliceZY( fio, 60, iFeature, (float*)ppImgFloat.ImageRow(0) );
		output_float( ppImgFloat, ppImgChar );
		sprintf( pcFileName, "%s_zy.pgm", pcFileNameBase );

		for( int i = 0; i < lva.iCount; i++ )
		{
			LOCATION_VALUE_XYZ &lv = lva.plvz[i];

			(ppImgChar.ImageRow( lv.z ))[lv.y] = 255;
		}

		ppImgChar.WriteToFile( pcFileName );
	}

	return 1;
}

int
output_keypoint_image(
		   PpImage &ppImg,
		   unsigned char *pucDesc
		   )
{
	if( ppImg.Rows() < 12 || ppImg.Cols() < 12 )
	{
		ppImg.Initialize( 12, 12, 12, 8 );
	}

	// (8 orientations per group)*(4 groups per row)

	for( int r = 0; r < 4; r++ )
	{
		for( int c = 0; c < 4; c++ )
		{
			unsigned char *pucK = pucDesc + (r*4 + c)*8;

			for( int rr = 0; rr < 3; rr++ )
			{
				unsigned char *pucRow = ppImg.ImageRow(r*3+rr);
				for( int cc = 0; cc < 3; cc++ )
				{
					if( rr == 1 && cc == 1 )
					{
						// Skip middle pixel
						pucRow[c*3+cc] = 0;
					}
					else
					{
						pucRow[c*3+cc] = *pucK++;
					}
				}
			}
		}
	}
	return 0;
}
