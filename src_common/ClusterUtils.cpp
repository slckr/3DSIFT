
#include "ClusterUtils.h"

#include "Vectors.h"
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

#define NULL 0

#define MAX_FUZZYCM_ITERATIONS	 8
#define MAX_ITERATION_DIFFERENCE 0.025

#define PI 3.1415926535897932384626433832795

void
cuInitCluster(
			  SAMPLE_TYPE *samples,	// Array of raw features
			  const int vec_size,		// Number of features per vector
			  const int num_samples,	// Number of vectors
			  const int num_clusters,	// Number of clusters
			  SAMPLE_TYPE *pcluster_centers,	// Array of feature cluster centers
			  unsigned char* cluster				// Array of indicies mapping vectors to cluster centers
			  )
{
	for(int k = 0; k < num_clusters; k++)
	{
		for(int n = 0; n < vec_size; n++)
		{
			pcluster_centers[k*vec_size + n] =
				samples[(k*(int)(num_samples / num_clusters))*vec_size + n];
		}
	}
}

void
cuInitRandomClusters(
			  SAMPLE_TYPE *samples,	// Array of raw features
			  const int vec_size,		// Number of features per vector
			  const int num_samples,	// Number of vectors
			  const int num_clusters,	// Number of clusters
			  SAMPLE_TYPE *pcluster_centers,	// Array of feature cluster centers
			  unsigned char* cluster				// Array of indicies mapping vectors to cluster centers
			  )
{
	for(int k = 0; k < num_clusters; k++)
	{
		double dRand = ((double)rand()) / ((double)RAND_MAX);
		int iRandVector = (int)((double)num_samples*dRand);
		for(int n = 0; n < vec_size; n++)
		{
			pcluster_centers[k*vec_size + n] =
				samples[iRandVector*vec_size + n];
		}
	}
}

static inline int
NearestCluster(
	float *pPaddedFeatureVector,
	float *pPaddedCenterFeatureVectors,
	const int iClusterCenters,
	const int iPaddedVecSize,
	const int iVecSize
	)
{
	int iReturn = 0;
	double dist = VecDist( pPaddedFeatureVector, pPaddedCenterFeatureVectors, iVecSize );

	for( int k = 0; k < iClusterCenters; k++ )
	{
		double tmp =
			VecDist( pPaddedFeatureVector, pPaddedCenterFeatureVectors + k*iPaddedVecSize, iVecSize );

		if(tmp < dist )
		{
			dist = tmp;
			iReturn = k;
		}
	}

	return iReturn;
}

int
cuKMeansCluster(
		  SAMPLE_TYPE* samples,	// Array of raw features
		  const int vec_size,		// Number of features per vector
		  const int num_samples,	// Number of vectors
		  const int num_clusters,	// Number of clusters
		  SAMPLE_TYPE* pcluster_centers, // Array of feature cluster centers
		  unsigned char* cluster,		 // Array of indicies mapping vectors to cluster centers
			ITERATION_FUNCTION ifFunc,
			void *pData,
			int iIterations
		  )
{
	int  i, k, n;
	SAMPLE_TYPE inv_num_clusters = 1.f/num_clusters;
	
	int converged = 0;
	int NumIter = 0;
	double error = 0.0;

	//memory allocation 
	double** center = new double*[ num_clusters ];
	double** old_center = new double*[ num_clusters ];
	double*	 center_data = new double[ 2*num_clusters*vec_size ];
	int*	 counter = new int[ num_clusters ];
	float* cluster_half_dist = new float[ num_clusters ];
	float* center_float = new float[ num_clusters*vec_size ];

	assert( num_samples && num_clusters && vec_size );
	
	{
		// Set pointers

		double *ptemp = center_data;
		for ( i = 0; i < num_clusters; i++ )
		{
			center[i] = ptemp;
			ptemp += vec_size;
			old_center[i] = ptemp;
			ptemp += vec_size;
		}
	}

	// Set initial cluster centers

	for (k = 0; k < num_clusters; k++)
	{ 
		for (n = 0; n < vec_size; n++)
		{
			 old_center[k][n] = center[k][n] = pcluster_centers[k*vec_size + n];
//			center[k][n] = samples[(k* (int)(num_samples * inv_num_clusters))*vec_size + n]; 
//			old_center[k][n] = center[k][n];
		}
	}
	
	while( !converged )//(error > 0.01) && (NumIter < 100))
	{   
		error = 0.0;
		NumIter++;

		// Save centers in float array

		for (k = 0; k < num_clusters; k++)
		{ 
			for (n = 0; n < vec_size; n++)
			{
				 center_float[k*vec_size + n] = (float)old_center[k][n];
			}
		}

		// Calculate half-distances

		for(k = 0; k < num_clusters; k++)
		{
			double dist = -1;

			for(n = 0; n < num_clusters; n++)
			{
				if( n == k )
				{
					continue;
				}

				float newdist = (float)VecDist( center_float + k*vec_size, center_float + n*vec_size, vec_size );

				if( newdist < dist || dist == -1 )
				{
					dist = newdist;
				}
			}

			cluster_half_dist[k] = (float)(dist / 4.0);
		}

		//assign samples to clusters
		for (i = 0; i < num_samples; i++)
		{ 
			float dist = (float)VecDist( center_float + cluster[i]*vec_size, samples + i*vec_size, vec_size);

			// This shouldn't have to be called every time

			if( dist >= cluster_half_dist[cluster[i]] )
			{
				cluster[i] = NearestCluster( samples + i*vec_size, center_float, num_clusters, vec_size, vec_size );
				assert( cluster[i] == NearestCluster( samples + i*vec_size, center_float, num_clusters, vec_size, vec_size ) );
			}
			else
			{
				assert( cluster[i] == NearestCluster( samples + i*vec_size, center_float, num_clusters, vec_size, vec_size ) );
			}
		}

		/* compute mean of every cluster */
		/* reset clusters and counters   */

		// Set counter to zero
		for (k = 0; k < num_clusters; k++)
		{
			counter[k] = 0; 
			memset((void*)center[k], 0, vec_size * sizeof(SAMPLE_TYPE) );
		}

		// Count clusters, generate sums of cluster vectors
		for (i = 0; i < num_samples; i++)
		{
			int clus = cluster[i];
			//p_cvAddVector_32f ( samples[i], center[clus], center[clus], vec_size);
			VecSum( center[clus], samples + i*vec_size, vec_size );
			counter[clus]++;  
		}

		// Scale sums of cluster vectors
		for (k = 0; k < num_clusters; k++)
		{
//			assert( counter[k] >= 1 );
			if( counter[k] > 1 )
			{
				VecDiv( center[k], (SAMPLE_TYPE)counter[k], vec_size );
			}
		} 

		// Calculate difference between current cluster centers & previous cluster centers

		for (k = 0; k < num_clusters; k++)
		{
			error += VecDist( center[k], old_center[k], vec_size );
		}
		
		/* copy curr_clusters to prev_clusters */
		{
			double** tmp = center;
			center = old_center;
			old_center = tmp;
		}
		
//		converged = ( (termcrit.type&CV_TERMCRIT_EPS) && ( error < termcrit.epsilon ) );
//		converged |= ( (termcrit.type&CV_TERMCRIT_ITER) && ( NumIter == termcrit.maxIter ) );

		for (k = 0; k < num_clusters; k++)
		{ 
			for (n = 0; n < vec_size; n++)
			{
				pcluster_centers[k*vec_size + n] = (float)center[k][n];
			}
		}

		if( ifFunc != 0 )
		{
			ifFunc( NumIter, pcluster_centers, cluster, pData );
		}

		converged = (error < 0.00001) || (NumIter > iIterations);
		
	}

	for (k = 0; k < num_clusters; k++)
	{ 
		for (n = 0; n < vec_size; n++)
		{
			pcluster_centers[k*vec_size + n] = (float)center[k][n];
		}
	}

	
	int *piClusterHistogram = new int[num_clusters];
	if( piClusterHistogram )
	{
		memset( piClusterHistogram, 0, num_clusters*sizeof(int) );
		for (i = 0; i < num_samples; i++)
		{ 
			piClusterHistogram[ cluster[i] ]++;
		}

		printf( "Cluster distribution: " );
		for( int cluster = 0; cluster < num_clusters; cluster++ )
		{
			printf( "%d\t", piClusterHistogram[cluster] );
		}
		delete [] piClusterHistogram;
	}

	delete [] cluster_half_dist;
	delete [] center_float;
	delete [] center;
	delete [] old_center;
	delete [] center_data;
	delete [] counter;

	return NumIter;
}


static inline int
NearestCluster(
	unsigned char*pPaddedFeatureVector,
	float *pPaddedCenterFeatureVectors,
	const int iClusterCenters,
	const int iPaddedVecSize,
	const int iVecSize
	)
{
	int iReturn = -1;

	double dist = 65000.0*65000.0;

	for( int k = 0; k < iClusterCenters; k++ )
	{
		double tmp =
			VecDist( pPaddedCenterFeatureVectors + k*iPaddedVecSize, pPaddedFeatureVector, iVecSize );

		if(tmp < dist )
		{
			dist = tmp;
			iReturn = k;
		}
	}

	return iReturn;
}

int
cuKMeansClusterSIFT(
				unsigned char *samples,	// Array of raw features
				const int vec_size,		// Number of features per vector (128)
				const int num_samples,	// Number of vectors
				const int num_clusters,	// Number of clusters
				unsigned char *pcluster_centers,	// Array of feature cluster centers
				int *cluster, // Array of indicies mapping vectors to cluster centers
				ITERATION_FUNCTION_SIFT ifFunc,
				void *pData,
				int iIterations
				)
{
	int  i, k, n;
	double inv_num_clusters = 1.f/num_clusters;

	int converged = 0;
	int NumIter = 0;
	double error = 0.0;

	//memory allocation 
	double** center = new double*[ num_clusters ];
	double** old_center = new double*[ num_clusters ];
	double*	 center_data = new double[ 2*num_clusters*vec_size ];
	int*	 counter = new int[ num_clusters ];
	float* cluster_half_dist = new float[ num_clusters ];
	float* center_float = new float[ num_clusters*vec_size ];

	assert( num_samples && num_clusters && vec_size );

	{
		// Set pointers

		double *ptemp = center_data;
		for ( i = 0; i < num_clusters; i++ )
		{
			center[i] = ptemp;
			ptemp += vec_size;
			old_center[i] = ptemp;
			ptemp += vec_size;
		}
	}

	// Set initial cluster centers

	for (k = 0; k < num_clusters; k++)
	{ 
		for (n = 0; n < vec_size; n++)
		{
			 old_center[k][n] = center[k][n] = pcluster_centers[k*vec_size + n];
//			center[k][n] = samples[(k* (int)(num_samples * inv_num_clusters))*vec_size + n]; 
//			old_center[k][n] = center[k][n];
		}
	 	VecNorm( center[k], 512.0f, vec_size );
		VecNorm( old_center[k], 512.0f, vec_size );
	}

				ifFunc(
				0, num_samples, num_clusters,
				pcluster_centers,
				cluster, counter, pData );
	
	while( !converged )//(error > 0.01) && (NumIter < 100))
	{   
		error = 0.0;
		NumIter++;

		// Save centers in float array

		for (k = 0; k < num_clusters; k++)
		{ 
			for (n = 0; n < vec_size; n++)
			{
				 center_float[k*vec_size + n] = (float)old_center[k][n];
			}
		}

		// Calculate half-distances

		for(k = 0; k < num_clusters; k++)
		{
			double dist = 9990000e128;

			for(n = 0; n < num_clusters; n++)
			{
				if( n == k )
				{
					continue;
				}

				float newdist = (float)VecDist( center_float + k*vec_size, center_float + n*vec_size, vec_size );

				if( newdist < dist )
				{
					dist = newdist;
				}
			}

			cluster_half_dist[k] = (float)(dist / 4.0);
		}

		//assign samples to clusters
		for (i = 0; i < num_samples; i++)
		{ 
			//float dist = (float)VecDist( center_float + cluster[i]*vec_size, samples + i*vec_size, vec_size);
			//if( dist >= cluster_half_dist[cluster[i]] )
			{
				cluster[i] =
					NearestCluster( samples + i*vec_size, center_float, num_clusters, vec_size, vec_size );
				assert( cluster[i] == NearestCluster( samples + i*vec_size, center_float, num_clusters, vec_size, vec_size ) );
			}
		}

		/* compute mean of every cluster */
		/* reset clusters and counters   */

		// Set counter to zero
		for (k = 0; k < num_clusters; k++)
		{
			counter[k] = 0; 
			
			for (n = 0; n < vec_size; n++)
			{
				center[k][n] = 0;
			}
			//memset((void*)center[k], 0, vec_size * sizeof(SAMPLE_TYPE) );
		}

		// Count clusters, generate sums of cluster vectors
		for (i = 0; i < num_samples; i++)
		{
			int clus = cluster[i];
			//p_cvAddVector_32f ( samples[i], center[clus], center[clus], vec_size);
			VecSum( center[clus], samples + i*vec_size, vec_size );
			counter[clus]++;  
		}

		// Scale sums of cluster vectors
		for (k = 0; k < num_clusters; k++)
		{
//			assert( counter[k] >= 1 );
			if( counter[k] > 1 )
			{
				// SIFT vectors must be scaled to length 512
				VecNorm( center[k], 512.0f, vec_size );
				//VecDiv( center[k], (SAMPLE_TYPE)counter[k], vec_size );
			}
		} 

		// Calculate difference between current cluster centers & previous cluster centers

		for (k = 0; k < num_clusters; k++)
		{
			error += VecDist( center[k], old_center[k], vec_size );
		}
		
//		converged = ( (termcrit.type&CV_TERMCRIT_EPS) && ( error < termcrit.epsilon ) );
//		converged |= ( (termcrit.type&CV_TERMCRIT_ITER) && ( NumIter == termcrit.maxIter ) );

		for (k = 0; k < num_clusters; k++)
		{ 
			for (n = 0; n < vec_size; n++)
			{
				pcluster_centers[k*vec_size + n] = (float)center[k][n];
			}
		}

		if( ifFunc != 0 )
		{
			ifFunc(
				NumIter, num_samples, num_clusters,
				pcluster_centers,
				cluster, counter, pData );

		}

		/* copy curr_clusters to prev_clusters */
		{
			double** tmp = center;
			center = old_center;
			old_center = tmp;
		}

		printf( "Kmeans iteration %d\n", NumIter );

		converged = (error < 0.00001) || (NumIter > iIterations);
		
	}

	for (k = 0; k < num_clusters; k++)
	{ 
		for (n = 0; n < vec_size; n++)
		{
			pcluster_centers[k*vec_size + n] = (unsigned char)center[k][n];
		}
	}

	delete [] cluster_half_dist;
	delete [] center_float;
	delete [] center;
	delete [] old_center;
	delete [] center_data;
	delete [] counter;

	return NumIter;
}

void
cuMeanFeature(
			  SAMPLE_TYPE *samples,	// Array of raw features
			  const int vec_size,		// Number of features per vector
			  const int num_samples,	// Number of vectors
			  SAMPLE_TYPE *mean
			 )
{
	for( int iSample = 0; iSample < num_samples; iSample++ )
	{
	}
}

void
cuInitSeparatedClusters(
			  SAMPLE_TYPE *samples,	// Array of raw features
			  const int vec_size,		// Number of features per vector
			  const int num_samples,	// Number of vectors
			  const int num_clusters,	// Number of clusters to generate
			  SAMPLE_TYPE *pcluster_centers,	// Array of feature cluster centers
			  int *picluster_center_indices,	// optional array of indices into samples
			  ITERATION_FUNCTION ifFunc,
			void *pData,
			int iIterations
			  )
{
	// Choose initial cluster - should probably be randomly chosen, or max dist from mean...
	int iInitCluster = num_samples / 2;
	memcpy( pcluster_centers, samples+vec_size*iInitCluster, vec_size*sizeof(SAMPLE_TYPE) );

	// Create an array to keep track of minimum distances
	SAMPLE_TYPE *pfMinDistances = new SAMPLE_TYPE[num_samples];
	assert( pfMinDistances );
	// Init min distances
	for( int iSample = 0; iSample < num_samples; iSample++ )
	{
		pfMinDistances[iSample] = VecDist( pcluster_centers, samples+vec_size*iSample, vec_size );
	}

	if( picluster_center_indices )
	{
		picluster_center_indices[0] = iInitCluster;
	}

	// Choose other clusters
	for( int i = 1; i < num_clusters; i++ )
	{
		// The next cluster is the sample most distant from all current clusters
		int iMaxMinSampleIndex = -1;
		float fMaxMinSampleDist = 0;

		for( int iSample = 0; iSample < num_samples; iSample++ )
		{
			int iMinClusterIndex = -1;
			float fMinClusterDist = 0;

			//// Evaluate distance to each cluster, save min distance
			//for( int j = 0; j < i; j++ )
			//{
			//	float fDist = VecDist(
			//		pcluster_centers+vec_size*j,
			//		samples+vec_size*iSample,
			//		vec_size );
			//	if( iMinClusterIndex == -1 || fDist < fMinClusterDist )
			//	{
			//		fMinClusterDist = fDist;
			//		iMinClusterIndex = i;
			//	}
			//}

			// Evaluate distance to each cluster, save min distance
			// Only update min distances of most recent cluster

			float fDist = VecDist(
				pcluster_centers+vec_size*(i-1),
				samples+vec_size*iSample,
				vec_size );
			fMinClusterDist = pfMinDistances[iSample];
			if( fDist < fMinClusterDist )
			{
				fMinClusterDist = fDist;
				pfMinDistances[iSample] = fDist;
			}

			// If the distance from this sample to the nearest cluster is 
			// greater than the previously stored min distance, save it
			if( iMaxMinSampleIndex == -1 || fMinClusterDist > fMaxMinSampleDist )
			{
				iMaxMinSampleIndex = iSample;
				fMaxMinSampleDist = fMinClusterDist;
			}
		}

		// Save the sample whose minimum distance to the closest cluster
		// is the greatest
		assert( iMaxMinSampleIndex > -1 );
		assert( fMaxMinSampleDist > 0 );
		memcpy(
			pcluster_centers+vec_size*i,
			samples+vec_size*iMaxMinSampleIndex,
			vec_size*sizeof(SAMPLE_TYPE)
			);
		printf( "%3.3d\t%f\n", i, fMaxMinSampleDist );

		if( picluster_center_indices )
		{
			picluster_center_indices[i] = iMaxMinSampleIndex;
		}
	}

	delete [] pfMinDistances;
}

void
cuAssignClusters(
			  SAMPLE_TYPE *samples,	// Array of raw features
			  const int vec_size,		// Number of features per vector
			  const int num_samples,	// Number of vectors
			  const int num_clusters,	// Number of clusters
			  SAMPLE_TYPE *pcluster_centers,	// Array of feature cluster centers
			  int *picluster_counts,		// counts for each cluster
			  int *picluster_indices,	// indices for each cluster
			  float *pfcluster_distances	// distances for each cluster
			  )
{
	for( int iCluster = 0; iCluster < num_clusters; iCluster++ )
	{
		picluster_counts[iCluster] = 0;
	}
	for( int iSample = 0; iSample < num_samples; iSample++ )
	{
		int iMinClusterIndex = -1;
		float fMinClusterDist = 0;

		// Evaluate distance to each cluster, save min distance
		for( int iCluster = 0; iCluster < num_clusters; iCluster++ )
		{
			float fDist = VecDist(
				pcluster_centers+vec_size*iCluster,
				samples+vec_size*iSample,
				vec_size );
			if( iMinClusterIndex == -1 || fDist < fMinClusterDist )
			{
				fMinClusterDist = fDist;
				iMinClusterIndex = iCluster;
			}
		}

		picluster_counts[iMinClusterIndex]++;
		picluster_indices[iSample] = iMinClusterIndex;
		pfcluster_distances[iSample] = fMinClusterDist;
	}
}

int
cuCMeansCluster(
			SAMPLE_TYPE *pfeatures,		// Array of raw features
			const int features,
			const int vectors,
			const int clusters,
			float *pcluster_centers,				// Array of feature cluster centers
			unsigned char* pcluster_indicies,	// Array of indicies mapping vectors to cluster centers
			ITERATION_FUNCTION ifFunc,
			void *pData
			)
{
	float	*pclustering = new float[vectors * clusters];
	double	*cluster_prob_sums = new double[clusters];
	float	*new_cluster_prob = new float[clusters];
	double	*cluster_centers = new double[ clusters*features ];

	float cl_prob;
	float *pclprob = pclustering;
	unsigned short cluster;
	unsigned short cl2;
	unsigned short feature;
	unsigned short init_cl_prob = 0;
	float *pfeature;
	unsigned int size;

	memset(pclustering, 0, vectors * clusters * sizeof(float));
	
	double *pclcenter;
	double dsum;
	double clprob;
	double di;
	double dcl;
	double pdiff;
	double max_pdiff;

	//pick initial cluster centers
	for(int k = 0; k < clusters; k++)
	{ 
		for(int n = 0; n < features; n++)
		{
			cluster_centers[k*features + n] = pcluster_centers[k*features + n];
//			cluster_centers[k*features + n] = pfeatures[(k*(int)(vectors / clusters))*features + n];
		}
	}

	pclcenter = cluster_centers;
	
	short iteration = 0;
	goto CLUSTER_MEMBERSHIP_MATRIX;

	while( 1 )
	{
		// First we compute new cluster centroids based on existing cluster
		// membership values

		memset(cluster_prob_sums, 0, clusters * sizeof(double));
		pclprob = pclustering;
		for (size = vectors;  size;  size--)
		{
			for (cluster = 0;  cluster < clusters;  cluster++)
			{
				cl_prob = *pclprob++;
				cluster_prob_sums[cluster] += (cl_prob * cl_prob);
			}
		}
		memset(cluster_centers, 0, clusters * features * sizeof(double));
		for (cluster = 0;  cluster < clusters;  cluster++)
		{
			pclprob = pclustering;
			pfeature = (float*)pfeatures;
			pclcenter = cluster_centers + cluster * features;
			for (size = vectors;  size;  size--)
			{
				cl_prob = pclprob[cluster];
				{
					cl_prob *= cl_prob;  // cluster membership probability ^ 2
					for (feature = 0;  feature < features;  feature++)
					{
						pclcenter[feature] += cl_prob * pfeature[feature];
					}
				}
				pfeature += features;  // Next pixel (each one has one float for each feature)
				pclprob += clusters;  // Next pixel (each one has one float for each of the clusters)
			}
			pclcenter = cluster_centers + cluster * features;
			for (feature = 0;  feature < features;  feature++)
			{
				if( cluster_prob_sums[cluster] == 0 )
				{
					cluster_prob_sums[cluster] = 1;
				}
				pclcenter[feature] /= cluster_prob_sums[cluster];
			}
		}

		// Then we calculate new cluster membership matrix
CLUSTER_MEMBERSHIP_MATRIX:

		pclprob = pclustering;
		pfeature = (float*)pfeatures;
		max_pdiff = 0;
		for (size = vectors;  size;  size--)
		{
			for (cluster = 0;  cluster < clusters;  cluster++)
			{
				dcl = VecDist(pfeature, cluster_centers + cluster * features, features);
				dsum = 0;
				pclcenter = cluster_centers;
				for (cl2 = 0;  cl2 < clusters;  cl2++)
				{
					if (cl2 == cluster)
						di = dcl;
					else
						di = VecDist(pfeature, pclcenter, features);

					if( di == 0 )
					{
						di = 1;
					}
					dsum += (1.0 / di);
					pclcenter += features;
				}

				if( dcl * dsum == 0 )
				{
					if( dcl == 0 )
					{
						dcl = 1;
					}
					if( dsum == 0 )
					{
						dsum = 1;
					}
				}
				clprob = 1.0 / (dcl * dsum);
				new_cluster_prob[cluster] = (float) clprob;			  
			}
			pdiff = VecDist(pclprob, new_cluster_prob, clusters);
			if (pdiff > max_pdiff) max_pdiff = pdiff;
			memcpy(pclprob, new_cluster_prob, clusters * sizeof(float));
			pfeature += features;
			pclprob += clusters;
		}
		iteration++;
		if (iteration >= MAX_FUZZYCM_ITERATIONS)
			break;
		if (max_pdiff <= MAX_ITERATION_DIFFERENCE)
			break;
	}

	// Copy cluster centers

	if( pcluster_centers )
	{
		for( int i = 0; i < clusters*features; i++ )
		{
			pcluster_centers[i] = (float)cluster_centers[i];
		}
	}

	delete [] cluster_centers;
	delete [] cluster_prob_sums;
	delete [] new_cluster_prob;

	unsigned char cl, *pres = pcluster_indicies;
	pclprob = pclustering;
	float prob, max_prob;
	for(size = vectors;  size;  size--)
	{
		max_prob = 0;
		cl = 0;
		for (cluster = 0;  cluster < clusters;  cluster++)
		{
			prob = *pclprob++;
			if(prob > max_prob) 
			{
				max_prob = prob; 
				cl = (unsigned char) cluster;
			}
		}
		*pres++ = cl;
	}

	delete [] pclustering;

	return iteration;
}

void
cuAssignNearestClusters(
			const SAMPLE_TYPE *pfeatures,	// Array of raw features
			const int features_per_vector,	// Number of features per vector
			const int vectors,					// Number of vectors
			const int clusters,				// Number of clusters
			const SAMPLE_TYPE *pcluster_centers,		// Array of feature cluster centers
			unsigned char* pcluster_indicies	// Array of indicies mapping vectors to cluster centers
			   )
{
	int *piClusterHistogram = new int[clusters];
	
	if( piClusterHistogram )
	{
		memset( piClusterHistogram, 0, clusters*sizeof(int) );
	}

	for( int i = 0; i < vectors; i++ )
	{
		double dMinDist = 9000000.0;
		int iMinDistCluster = -1;
		SAMPLE_TYPE *pCurrFeature =
			(SAMPLE_TYPE *)(pfeatures + i*features_per_vector);

		for( int cluster = 0; cluster < clusters; cluster++ )
		{
			double dDist = 
				VecDist(pCurrFeature, (SAMPLE_TYPE*)pcluster_centers + cluster * features_per_vector, features_per_vector);

			if( dDist < dMinDist || cluster == 0 )
			{
				dMinDist = dDist;
				iMinDistCluster = cluster;
			}
		}

		assert( iMinDistCluster != -1 );
		pcluster_indicies[i] = iMinDistCluster;

		if( piClusterHistogram )
		{
			piClusterHistogram[iMinDistCluster]++;
		}
	}

	if( piClusterHistogram )
	{
		printf( "Initial cluster distribution: " );
		for( int cluster = 0; cluster < clusters; cluster++ )
		{
			printf( "%d ", piClusterHistogram[cluster] );
		}
		printf( "\n" );
		delete [] piClusterHistogram;
	}
}

void
cuAssignNearestClusters(
			const SAMPLE_TYPE *pfeatures,	// Array of raw features
			const int features_per_vector,	// Number of features per vector
			const int rows,					// Number of rows
			const int cols,					// Number of cols
			const int clusters,				// Number of clusters
			const SAMPLE_TYPE *pcluster_centers,		// Array of feature cluster centers
			unsigned int *pClusterRowSum,	// Sum of pixel rows
			unsigned int *pClusterColSum,// Sum of pixel cols
			unsigned int *pClusterPixelCount, // Pixel count
			unsigned char* pcluster_indicies	// Array of indicies mapping vectors to cluster centers
			   )
{
	int vectors = rows*cols;

	for( int i = 0; i < rows; i++ )
	{
		for( int j = 0; j < cols; j++ )
		{
			SAMPLE_TYPE dMinDist = 65000.0;
			int iMinDistCluster = -1;
			SAMPLE_TYPE *pCurrFeature =
				(SAMPLE_TYPE *)(pfeatures + (i*cols + j)*features_per_vector);

			for( int cluster = 0; cluster < clusters; cluster++ )
			{
				SAMPLE_TYPE dDist = 
					VecDist(pCurrFeature, (SAMPLE_TYPE*)pcluster_centers + cluster * features_per_vector, features_per_vector);

				if( dDist < dMinDist )
				{
					dMinDist = dDist;
					iMinDistCluster = cluster;
				}
			}

			assert( iMinDistCluster != -1 );
			pcluster_indicies[(i*cols + j)] = iMinDistCluster;

			pClusterRowSum[ iMinDistCluster ] += i;
			pClusterColSum[ iMinDistCluster ] += j;
			pClusterPixelCount[ iMinDistCluster ]++;
		}
	}
}

void
cuAssignProbabilisticClusters(
			const SAMPLE_TYPE *pfeatures,		// Array of raw features
			const int features_per_vector,		// Number of features per vector
			const int vectors,
			const int clusters,					// Number of clusters
			const SAMPLE_TYPE *pcluster_centers,	// Array of feature cluster centers
			SAMPLE_TYPE *pprobabilities,		// Array of cluster probabilities
			unsigned char *pucClusterIndices
			   )
{
	// For each feature sample...

	int piMinDistCounts[20];
	memset( piMinDistCounts, 0, sizeof(piMinDistCounts) );

	for( int i = 0; i < vectors; i++ )
	{
		SAMPLE_TYPE fProbSum      = 0.0f;
		SAMPLE_TYPE *pCurrFeature = (SAMPLE_TYPE *)pfeatures + i*features_per_vector;

		float fMinDist = 9999e99;
		int iMinDistCluster = -1;

		int c;

		// ... calcualte distances to each cluster ...
		for( int c = 0; c < clusters; c++ )
		{
			SAMPLE_TYPE dDist = 
				VecDist(
					pCurrFeature,
					(SAMPLE_TYPE *)pcluster_centers + c*features_per_vector,
					features_per_vector
				);
			//pprobabilities[ i*clusters + c ] = exp( -dDist );
			pprobabilities[ i*clusters + c ] = 1.0 / (dDist + 10);//1000);
			fProbSum += pprobabilities[ i*clusters + c ];

			if( iMinDistCluster == -1 || dDist < fMinDist )
			{
				iMinDistCluster = c;
				fMinDist = dDist;
			}
		}

		//assert( iMinDistCluster == pucClusterIndices[i] );
		
		float fMaxProb = -1.0f;
		int iMaxProbCluster = -1;

		assert( fProbSum > 0.0f );

		// ... and normalize to make a probability
		for( int c = 0; c < clusters; c++ )
		{
			pprobabilities[ i*clusters + c ] /= fProbSum;

			SAMPLE_TYPE fProb = pprobabilities[ i*clusters + c ];
			
			if( iMaxProbCluster == -1 || fProb > fMaxProb )
			{
				iMaxProbCluster = c;
				fMaxProb = fProb;
			}
			assert( pprobabilities[ i*clusters + c ] >= 0.0f && pprobabilities[ i*clusters + c ] <= 1.0f );
		}

		piMinDistCounts[iMaxProbCluster]++;

		//assert( iMaxProbCluster == pucClusterIndices[i] );
	}
}

void
cuAssignProbabilisticClustersGaussian(
			const SAMPLE_TYPE *pfeatures,		// Array of raw features
			const int features_per_vector,		// Number of features per vector
			const int vectors,
			const int clusters,					// Number of clusters
			const SAMPLE_TYPE *pcluster_centers,	// Array of feature cluster centers
			SAMPLE_TYPE *pprobabilities,		// Array of cluster probabilities
			unsigned char *pucClusterIndices
			   )
{
	// For each feature sample...

	int piMinDistCounts[20];
	memset( piMinDistCounts, 0, sizeof(piMinDistCounts) );

	// Calculate cluster means & variances

	int *piClusterCounts = new int[clusters];
	double *pdClusterMeans = new double[clusters*features_per_vector];
	double *pdClusterVars  = new double[clusters*features_per_vector];

	memset( piClusterCounts, 0, sizeof(int)*clusters );
	for( int i = 0; i < clusters*features_per_vector; i++ )
	{
		pdClusterMeans[i] = 0;
		pdClusterVars[i] = 0;
	}

	for( int i = 0; i < vectors; i++ )
	{
		SAMPLE_TYPE *pCurrFeature = (SAMPLE_TYPE *)pfeatures + i*features_per_vector;

		float fMinDist;
		int iMinDistCluster = -1;

		int c;

		// ... calcualte distances to each cluster ...
		for( int c = 0; c < clusters; c++ )
		{
			SAMPLE_TYPE dDist = 
				VecDist(
					pCurrFeature,
					(SAMPLE_TYPE *)pcluster_centers + c*features_per_vector,
					features_per_vector
				);
			if( iMinDistCluster == -1 || dDist < fMinDist )
			{
				iMinDistCluster = c;
				fMinDist = dDist;
			}
		}

		// Update squares & sums of squares

		piClusterCounts[iMinDistCluster]++;

		for( int iFeat = 0; iFeat < features_per_vector; iFeat++ )
		{
			pdClusterMeans[iMinDistCluster*features_per_vector + iFeat] += pCurrFeature[iFeat];
			pdClusterVars [iMinDistCluster*features_per_vector + iFeat] += pCurrFeature[iFeat]*pCurrFeature[iFeat];
		}
	}

	// Calculate varriances & means from sums
	for( int c = 0; c < clusters; c++ )
	{
		for( int i = 0; i < features_per_vector; i++ )
		{
			pdClusterMeans[c*features_per_vector+i] /= (double)piClusterCounts[c];
			pdClusterVars[c*features_per_vector+i] =
				(pdClusterVars[c*features_per_vector+i]/(double)piClusterCounts[c])
				- (pdClusterMeans[c*features_per_vector+i]*pdClusterMeans[c*features_per_vector+i]);
		}
	}

	// Assign Gaussian probabilities
	for( int i = 0; i < vectors; i++ )
	{
		SAMPLE_TYPE *pCurrFeature = (SAMPLE_TYPE *)pfeatures + i*features_per_vector;

		double dProbSum = 0;

		// Calculate Gaussian probabilities wrt each cluster
		for( int c = 0; c < clusters; c++ )
		{
			double dExpSum = 0;
			double dVarrProd = 1.0;
			for( int iFeat = 0; iFeat < features_per_vector; iFeat++ )
			{
				double dDiff = pCurrFeature[iFeat] - pdClusterMeans[c*features_per_vector+iFeat];
				double dDiffSqr = dDiff*dDiff;
				double dExp = dDiffSqr / pdClusterVars[c*features_per_vector+iFeat];
				dExpSum += dExp;
				dVarrProd *= pdClusterVars[c*features_per_vector+iFeat];
			}
			dExpSum *= -0.5;
			
			double fGaussianDivisor = (double)( 1.0 / sqrt( pow( 2.0*PI,features_per_vector )*dVarrProd ) );
			double pfCompProb = (double)( exp( dExpSum ) * fGaussianDivisor );

			pprobabilities[ i*clusters + c ] = (float)pfCompProb;
			dProbSum += pfCompProb;
		}

		// Normalize probability

		for( int c = 0; c < clusters; c++ )
		{
			pprobabilities[ i*clusters + c ] /= (float)dProbSum;
		}
	}

	delete [] piClusterCounts;
	delete [] pdClusterMeans;
	delete [] pdClusterVars;
}

void
cuAssignClusterDistances(

			const SAMPLE_TYPE *pfeatures,		// Array of raw features
			const int features_per_vector,		// Number of features per vector
			const int vectors,

			const int clusters,						// Number of clusters
			const SAMPLE_TYPE *pcluster_centers,	// Array of feature cluster centers

			SAMPLE_TYPE *pdistances					// Array of cluster distance
			   )
{
	// For each feature sample...

	for( int i = 0; i < vectors; i++ )
	{
		SAMPLE_TYPE *pCurrFeature = (SAMPLE_TYPE *)pfeatures + i*features_per_vector;
		int c;

		// ... calcualte distances to each cluster ...
		for( int c = 0; c < clusters; c++ )
		{
			SAMPLE_TYPE dDist = 
				VecDist(
					pCurrFeature,
					(SAMPLE_TYPE *)pcluster_centers + c*features_per_vector,
					features_per_vector
				);
			pdistances[ i*clusters + c ] = dDist;
		}
	}
}
