
#ifndef __FEATUREEOL_H__
#define __FEATUREEOL_H__

#include "FeatureIO.h"

//
// calc_eol_pixel_sums()
//
// Calculates the EOL a 2D array of feature vectors.
//
// The probability density function is a multivariate Gaussian, 
// with independent components. The mean vector is pfFeatureVec,
// and the varriance terms are in fio2.pfVarr.
//
// p(Y=X) = exp( -( y1-x1 / varr1 + y2-x2 / varr1 + ...) )
//
float
calc_eol_pixel_sums(
			   const int &iRowStart,			// Domain start position
			   const int &iColStart,
			   const int &iRows,				// Domain size
			   const int &iCols,
			   const float *pfFeatureVec,
			   FEATUREIO &fio2					// Domain vectors
			   );
float
calc_eol_pixel_sums_robust(
			   const int &iRowStart,			// Domain start position
			   const int &iColStart,
			   const int &iRows,				// Domain size
			   const int &iCols,
			   const float *pfFeatureVec,
			   FEATUREIO &fio2,					// Domain vectors
			   const float &fRobustVarr
			   );

//
// calculate_eol_image()
//
// Calculates the EOL for an entire array of feature vectors fio1.
//
int
calculate_eol_image(
					const int &iRowOffset,	// Offset of a pixel in fio1 to fio2
					const int &iColOffset,
					const int &iRows,		// Domain size in fio2
					const int &iCols,		//
					FEATUREIO &fio1,
					FEATUREIO &fio2,
					FEATUREIO &fioEOL
					);
int
calculate_eol_image_robust(
					const int &iRowOffset,	// Offset of a pixel in fio1 to fio2
					const int &iColOffset,
					const int &iRows,		// Domain size in fio2
					const int &iCols,		//
					FEATUREIO &fio1,
					FEATUREIO &fio2,
					FEATUREIO &fioEOL,
					const float &fRobustVarr
					);

#endif
