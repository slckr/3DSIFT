
#include <assert.h>
#include "XYZArray.h"

XYZArray::XYZArray(
		)
{
	m_iDim = -1;
	m_pssb = 0;
	m_iTotalBins = 0;
}

XYZArray::~XYZArray(
		)
{
	if( m_iDim > 0 )
	{
		assert( m_pssb );
		delete [] m_pssb;
		m_pssb = 0;
		m_iTotalBins = 0;
	}
}

int
XYZArray::Init(
		int iDim,
		float *pfMin,
		float *pfMax,
		float *pfInc
		)
{
	if( m_iDim > 0 )
	{
		assert( m_pssb );
		delete [] m_pssb;
		m_pssb = 0;
		m_iTotalBins = 0;
	}

	m_iDim = iDim;

	if( iDim <= 0 || iDim >= 10 )
	{
		return -1;
	}

	int iDataSize = 1;
	for( int i = 0; i < iDim; i++ )
	{
		m_pfMin[i]=pfMin[i];
		m_pfMax[i]=pfMax[i];
		m_pfInc[i]=pfInc[i];

		assert( m_pfMax[i] > m_pfMin[i] );
		assert( pfInc[i] > 0 );

		float fLength = m_pfMax[i]-m_pfMin[i];
		int iSize = (int)(fLength/pfInc[i])+1;
		m_piLen[i] = iSize;
		iDataSize*=iSize;
	}

	m_pssb = new ScaleSpaceXYZBin[iDataSize];
	if( !m_pssb )
	{
		return -1;
	}
	m_iTotalBins = iDataSize;

	return 0;
}

int
XYZArray::Add(
		float *pfXYZ,
		int iValue
	)
{
	ScaleSpaceXYZBin *pssb = GetBin( pfXYZ );
	if( pssb )
	{
		pssb->push_front( iValue );
		return 0;
	}
	return -1;
}

ScaleSpaceXYZBin *
XYZArray::GetBin(
				 float *pfXYZ,
				 int *piXYZBins
				 )
{
	if( m_iDim <= 0 )
	{
		return 0;
	}

	int iIndexDimProd = 1;
	int iIndexDimSum = 0;
	for( int i = 0; i < m_iDim; i++ )
	{
		//assert( pfXYZ[i] >= m_pfMin[i] );
		//assert( pfXYZ[i] <= m_pfMax[i] );
		int iIndexDim = (int)((pfXYZ[i]-m_pfMin[i])/m_pfInc[i]);
		assert( iIndexDim >= 0 );
		assert( iIndexDim < m_piLen[i] );
		if( piXYZBins )
		{
			piXYZBins[i] = iIndexDim;
		}
		iIndexDimSum += iIndexDimProd*iIndexDim;
		iIndexDimProd *= m_piLen[i];
	}

	return m_pssb + iIndexDimSum;
}


ScaleSpaceXYZBin *
XYZArray::GetNeighborBin(
			float *pfXYZ,
			int	*piNeighborInc // Neighbor bin
			)
{
	if( m_iDim <= 0 )
	{
		return 0;
	}

	int iIndexDimProd = 1;
	int iIndexDimSum = 0;
	for( int i = 0; i < m_iDim; i++ )
	{
		assert( pfXYZ[i] >= m_pfMin[i] );
		assert( pfXYZ[i] <= m_pfMax[i] );
		int iIndexDim = (int)((pfXYZ[i]-m_pfMin[i])/m_pfInc[i]);

		// Add neighbor increment, check bounds
		iIndexDim += piNeighborInc[i];
		if( iIndexDim < 0 )
		{
			return 0; // No neighbor here, out of bounds
		}
		else if( iIndexDim >= m_piLen[i] )
		{
			return 0; // No neighbor here either, out of bounds
		}

		iIndexDimSum += iIndexDimProd*iIndexDim; // Add neighbor increment
		iIndexDimProd *= m_piLen[i];
	}

	return m_pssb + iIndexDimSum;
}

int
XYZArray::EmptyAllBins(
	)
{
	int iBinCount = 1;
	for( int i = 0; i < m_iDim; i++ )
	{
		iBinCount *= m_piLen[i];
	}
	for( int iBin = 0; iBin < iBinCount; iBin++ )
	{
		m_pssb[iBin].clear();
	}
	return 0;
}

int
XYZArray::GetXYZBinCounts(
		int *piXYZBinCounts
	)
{
	for( int i = 0; i < m_iDim; i++ )
	{
		piXYZBinCounts[i] = m_piLen[i];
	}
	return 0;
}


ScaleSpaceXYZBin *
XYZArray::GetBinFromIndex(
		int *piXYZBins
		)
{
	if( m_iDim <= 0 )
	{
		return 0;
	}

	int iIndexDimProd = 1;
	int iIndexDimSum = 0;
	for( int i = 0; i < m_iDim; i++ )
	{
		//assert( piXYZBins[i] >= 0 );
		//assert( piXYZBins[i] < m_piLen[i] );
		if( piXYZBins[i] < 0 || piXYZBins[i] >= m_piLen[i] )
		{
			// Out of bounds, return 0
			return 0;
		}
		int iIndexDim = piXYZBins[i];
		iIndexDimSum += iIndexDimProd*iIndexDim;
		iIndexDimProd *= m_piLen[i];
	}

	return m_pssb + iIndexDimSum;
}

int
XYZArray::GetXYZFromBinIndex(
			float *pfXYZ,
			int	*piXYZBins
			)
{
	if( m_iDim <= 0 )
	{
		return 0;
	}

	for( int i = 0; i < m_iDim; i++ )
	{
		assert( piXYZBins[i] >= 0 );
		assert( piXYZBins[i] < m_piLen[i] );
		pfXYZ[i] = m_pfInc[i]*(piXYZBins[i]+0.5f) + m_pfMin[i];
	}

	return 1;
}

ScaleSpaceXYZBin *
XYZArray::GetBinFromIndexAbsolute(
		int iBin
		)
{
	if( m_iDim <= 0 )
	{
		return 0;
	}

	if( iBin < 0 || iBin >= m_iTotalBins )
	{
		return 0;
	}

	return m_pssb + iBin;
}

