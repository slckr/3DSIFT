
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

#include "MikoFeatures.h"

//#define FEATUREOFFSET 12

int
mfRead(
	char *pcFileName,
	MIKOFEATURES &mf
)
{
	FILE *infile = fopen( pcFileName, "rt" );
	if( !infile )
	{
		return 0;
	}

	float f = 2.50f;

	fscanf( infile, "%d", &mf.iFeaturesPerVector );
	fscanf( infile, "%d", &mf.iVectors );
	mf.iFeaturesPerVector += FEATUREOFFSET;

	mf.pfVectors = new float[mf.iVectors*mf.iFeaturesPerVector];
	if( !mf.pfVectors )
	{
		fclose( infile );
		return 0;
	}

	for( int i = 0; i < mf.iVectors; i++ )
	{
		for( int j = 0; j < mf.iFeaturesPerVector; j++ )
		{
			fscanf( infile, "%f", &(mf.pfVectors[i*mf.iFeaturesPerVector + j]) );
		}
	}

	fclose( infile );

	return 1;
}

int
mfWrite(
	char *pcFileName,
	MIKOFEATURES &mf
)
{
	FILE *outfile = fopen( pcFileName, "wt" );
	if( !outfile )
	{
		return 0;
	}

	float f = 2.50f;

	fprintf( outfile, "%d\n", mf.iFeaturesPerVector-FEATUREOFFSET );
	fprintf( outfile, "%d\n", mf.iVectors );

	for( int i = 0; i < mf.iVectors; i++ )
	{
		for( int j = 0; j < mf.iFeaturesPerVector; j++ )
		{
			fprintf( outfile, "%f ", mf.pfVectors[i*mf.iFeaturesPerVector + j] );
		}
		fprintf( outfile, "\n" );
	}

	fclose( outfile );

	return 1;
}


int
mfMiko2Lowe(
	char *pcFileMiko,
	char *pcFileLowe,
	char cOutputType
)
{
	FILE *infile = fopen( pcFileMiko, "rt" );
	if( !infile )
	{
		return 0;
	}
	FILE *outfile = fopen( pcFileLowe, "wt" );
	if( !outfile )
	{
		return 0;
	}

	float f = 2.50f;

	int iFeatureCount;
	int iFeatureLength;
	fscanf( infile, "%d", &iFeatureLength );
	fscanf( infile, "%d", &iFeatureCount );
	if( iFeatureLength != 128 )
	{
		printf( "Feature length not 128!!\n" );
		//assert( iFeatureLength == 128 );
		//return -1;
	}
	fprintf( outfile, "%d %d\n", iFeatureCount, 128 );

	float			pfVecInfo[FEATUREOFFSET];
	float	pucVecs[128];

	for( int i = 0; i < iFeatureCount; i++ )
	{
		for( int j = 0; j < FEATUREOFFSET; j++ )
		{
			fscanf( infile, "%f", &(pfVecInfo[j]) );
		}
		for( int j = 0; j < iFeatureLength && j < 128; j++ )
		{
			float fData;
			fscanf( infile, "%f", &fData );
			//assert( iData >= 0 && iData <= 255 );
			pucVecs[j] = fData;
		}
		// Pad out with zeros, if necessary
		for( int j = iFeatureLength; j < 128; j++ )
		{
			pucVecs[j] = 0;
		}

		// print coordinates
		fprintf( outfile, "%f %f %f %f\n", pfVecInfo[1],pfVecInfo[0],pfVecInfo[3],pfVecInfo[4] );

		// print features
		for( int j = 0; j < 128; j++ )
		{
			//fprintf( outfile, "%d ", pucVecs[j] );
			if( cOutputType == 'f' )
			{
				fprintf( outfile, "%f ", pucVecs[j] );
			}
			else
			{
				// integer output by default
				fprintf( outfile, "%d ", (int)pucVecs[j] );
			}
		}
		fprintf( outfile, "\n" );
	}

	fclose( outfile );

	fclose( infile );

	return 1;
}


float
mfMatchDistance(
		const MIKOFEATURES	&mf1,
		const MIKOFEATURES	&mf2,
		const int			&iVector1,
		const int			&iVector2
)
{
	float fDistanceSum = 0.0f;
	for( int i = FEATUREOFFSET; i < mf1.iFeaturesPerVector; i++ )
	{
		float fDiff = mf1.pfVectors[iVector1*mf1.iFeaturesPerVector + i]
			- mf2.pfVectors[iVector2*mf2.iFeaturesPerVector + i];
		fDistanceSum += fDiff*fDiff;
	}
	return fDistanceSum;
}

float
mfMatchVectorFeatures(
		const MIKOFEATURES	&mf1,
		const MIKOFEATURES	&mf2,
		const int			&iVector1,
		int					&iVector2
)
{
	int iMinDistIndex = 0;
	float fMinDist = mfMatchDistance( mf1, mf2, iVector1, 0 );

	for( int i = 0; i < mf2.iVectors; i++ )
	{
		float fDist = mfMatchDistance( mf1, mf2, iVector1, i );
		if( fDist < fMinDist )
		{
			fMinDist = fDist;
			iMinDistIndex = i;
		}
	}

	iVector2 = iMinDistIndex;

	return fMinDist;
}

float
mfMatchVectorFeatures2Best(
		const MIKOFEATURES	&mf1,
		const MIKOFEATURES	&mf2,
		const int			&iVector1,
		int					&iIndexMatch1,	// 1st best match
		int					&iIndexMatch2,	// 2nd best match
		float				&fScore1,		// 1st best match score
		float				&fScore2		// 2nd best match score
)
{
	int iMinDistIndex1 = 0;
	float fMinDist1 = mfMatchDistance( mf1, mf2, iVector1, 0 );

	int iMinDistIndex2 = 1;
	float fMinDist2 = mfMatchDistance( mf1, mf2, iVector1, 1 );

	int iSwap;
	float fSwap;

	// fMinDist1 is always less than fMinDist2

	if( fMinDist1 > fMinDist2 )
	{
		iSwap = iMinDistIndex1;
		fSwap = fMinDist1;
		
		iMinDistIndex1 = iMinDistIndex2;
		fMinDist1 = fMinDist2;
		
		iMinDistIndex2 = iSwap;
		fMinDist2 = fSwap;
	}

	for( int i = 2; i < mf2.iVectors; i++ )
	{
		float fDist = mfMatchDistance( mf1, mf2, iVector1, i );

		assert( fMinDist1 <= fMinDist2 );

		if( fDist < fMinDist2 )
		{
			// Change one or both of fMinDists

			if( fDist < fMinDist1 )
			{
				// first becomes new, second becomes first

				iMinDistIndex2 = iMinDistIndex1;
				fMinDist2 = fMinDist1;

				iMinDistIndex1 = i;
				fMinDist1 = fDist;
			}
			else if( fDist < fMinDist2 )
			{
				// first doesn't change, second becomes new

				iMinDistIndex2 = i;
				fMinDist2 = fDist;
			}
		}
	}

	iIndexMatch1 = iMinDistIndex1;
	iIndexMatch2 = iMinDistIndex2;
	fScore1 = fMinDist1;
	fScore2 = fMinDist2;

	return fMinDist1;
}

float
mfCoordinateDistance(
		const MIKOFEATURES	&mf1,
		const MIKOFEATURES	&mf2,
		const int			&iVector1,
		const int			&iVector2
)
{
	float fDistanceSum = 0.0f;
	for( int i = 0; i < 2; i++ )
	{
		float fDiff = mf1.pfVectors[iVector1*mf1.iFeaturesPerVector + i]
			- mf2.pfVectors[iVector2*mf2.iFeaturesPerVector + i];
		fDistanceSum += fDiff*fDiff;
	}
	return fDistanceSum;
}

int
mfMatchVectorCoordinates(
		const MIKOFEATURES	&mf1,
		const MIKOFEATURES	&mf2,
		const int			&iVector1,
		int					&iVector2
)
{
	int iMinDistIndex = 0;
	float fMinDist = mfMatchDistance( mf1, mf2, iVector1, 0 );

	for( int i = 0; i < mf2.iVectors; i++ )
	{
		float fDist = mfCoordinateDistance( mf1, mf2, iVector1, i );
		if( fDist < fMinDist )
		{
			fMinDist = fDist;
			iMinDistIndex = i;
		}
	}

	iVector2 = iMinDistIndex;

	return 1;
}


float
mfCol(
	  const MIKOFEATURES	&mf,
	  const int				&iIndex
)
{
	return mf.pfVectors[iIndex*mf.iFeaturesPerVector + 0];
}

float
mfRow(
	  const MIKOFEATURES	&mf,
	  const int				&iIndex
)
{
	return mf.pfVectors[iIndex*mf.iFeaturesPerVector + 1];
}

float
mfCornerness(
	  const MIKOFEATURES	&mf,
	  const int				&iIndex
)
{
	return mf.pfVectors[iIndex*mf.iFeaturesPerVector + 2];
}

float
mfScale(
	  const MIKOFEATURES	&mf,
	  const int				&iIndex
)
{
	return mf.pfVectors[iIndex*mf.iFeaturesPerVector + 3];
}
