
#include "FloodNeighbours.h"

inline static void
ffPushStack(
		   int *&pStack,
		   const int &iLocation
			)
{
	*pStack = iLocation;
	pStack++;
}

inline static void
ffPopStack(
		   int *&pStack,
		   int &iLocation
		   )
{
	pStack--;
	iLocation = *pStack;
}

void
fnFloodNeighbours(
		  int			*pStack,					// Stack big enough for all data
		  int			iStartLocation,				// Start location
		  int			*piNeighourDisplacements,	// Neighbour displacements
		  int			iNeighbours,				// Neighbour displacement count
		  FNCallback	*pProcess,					// Function to process pixel
		  FNCallback	*pPushOnStack,				// Function to put pixel on stack
		  void			*pArg						// Argument passed to FNCallback
)
{
	int *pCurrStack = pStack;
	ffPushStack( pCurrStack, iStartLocation );

	while( pCurrStack != pStack )
	{
		int iCurrLocation;
		ffPopStack( pCurrStack, iCurrLocation );

		if( !pProcess( iCurrLocation, pArg ) )
		{
			// Exit condition
			return;
		}

		for( int i = 0; i < iNeighbours; i++ )
		{
			int iNeighbour = iCurrLocation + piNeighourDisplacements[i];
			if( pPushOnStack( iNeighbour, pArg ) )
			{
				ffPushStack( pCurrStack, iNeighbour );
			}
		}
	}
}
