
#ifndef __VECTOR_H__
#define __VECTOR_H__

float
vector_sqr_distance(
				 const float *pfVec1,
				 const float *pfVec2,
				 const int iVecLength = 2
				 );

//
// vector_sqr_distance_varr()
//
// Multiplies each distance term by a varriance term.
//
float
vector_sqr_distance_varr(
				 const float *pfVec1,
				 const float *pfVec2,
				 const int iVecLength,
				 const float *pfVarr
				 );
inline float
vector_sqr_distance_varr(
				 const float *pfVec1,
				 const float *pfVec2,
				 const int iVecLength,
				 const float *pfVarr
				 )
{
	float fSqrDiffSum = 0.0f;
	for( int i = 0; i < iVecLength; i++ )
	{
		float fDiff = pfVec1[i] - pfVec2[i];
		fSqrDiffSum += fDiff*fDiff*pfVarr[i];
	}
	return fSqrDiffSum;
}

float
vector_sqr_mag(
				 const float *pfVec1,
				 const int iVecLength = 2
				 );

float
vector_dot_product(
				 const float *pfVec1,
				 const float *pfVec2,
				 const int iVecLength = 2
				 );


float
vector_normalized_dot_product(
				 const float *pfVec1,
				 const float *pfVec2,
				 const int iVecLength = 2
				 );

#endif
