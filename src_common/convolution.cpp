
#include <string.h>
#include <math.h>

#include "convolution.h"

//
// convolve_reflection()
//
// Convolves image ppImgIn with mask ppTmpIn with reflection at edges at point
// iImgRowCenter, iImgColCenter. Returns floating point result in fConvResult.
//
//	ppImgIn - input greyscale image, type char (8-bits per pixel)
//	ppTmpIn - input convolution mask image, type float (32-bits per pixel)
//	iImgRowCenter - center row position of convolution in ppImgIn
//	iImgColCenter - center col position of convolution in ppImgIn
//	fNormalization - normalization factor to divide convolution result.
//					if 0.0, ignored.
//	fConvResult - result of convolution
//
void
convolve_reflection_char(
	const PpImage &ppImg,
	const PpImage &ppTmp,
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  )
{
	// Compute averages
	
	int iStartRow = iImgRowCenter - ppTmp.Rows()/2;
	int iStartCol;

	int iRowInc = (iStartRow < 0 ? -1 : 1);
	int iColInc;

	int iCurrRow = (iStartRow < 0 ? -iStartRow : iStartRow);
	int iCurrCol;

//	FILE *outfile = fopen( "temp.txt", "wt" );

	double dSum = 0.0;

	for( int i = 0; i < ppTmp.Rows(); i++ )
	{
		iStartCol = iImgColCenter - ppTmp.Cols()/2;
		iColInc = (iStartCol < 0 ? -1 : 1);
		iCurrCol = (iStartCol < 0 ? -iStartCol : iStartCol);

		unsigned char *pcImgRow = ppImg.ImageRow( iCurrRow );
		float *pfFiltRow = (float*)ppTmp.ImageRow( i );

		for( int j = 0; j < ppTmp.Cols(); j++ )
		{
			dSum += (double)( ((float)pcImgRow[ iCurrCol ])*pfFiltRow[ j ] );

//			fprintf( outfile, "%d\n", (int)ppImg.ImageRow( iCurrRow )[ iCurrCol ] );

			if( iCurrCol + iColInc < 0 
				|| iCurrCol + iColInc >= ppImg.Cols() )
			{
				iColInc = -iColInc;
			}
			iCurrCol += iColInc;
		}
		if( iCurrRow + iRowInc < 0
			|| iCurrRow + iRowInc >= ppImg.Rows() )
		{
			iRowInc = -iRowInc;
		}
		iCurrRow += iRowInc;
	}

//	fclose( outfile );
	fConvResult = (float)dSum;
}

void
convolve_reflection_float(
	const PpImage &ppImg,
	const PpImage &ppTmp,
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  )
{
	// Compute averages
	
	int iStartRow = iImgRowCenter - ppTmp.Rows()/2;
	int iStartCol;

	int iRowInc = (iStartRow < 0 ? -1 : 1);
	int iColInc;

	int iCurrRow = (iStartRow < 0 ? -iStartRow : iStartRow);
	int iCurrCol;

//	FILE *outfile = fopen( "temp.txt", "wt" );

	double dSum = 0.0;

	for( int i = 0; i < ppTmp.Rows(); i++ )
	{
		iStartCol = iImgColCenter - ppTmp.Cols()/2;
		iColInc = (iStartCol < 0 ? -1 : 1);
		iCurrCol = (iStartCol < 0 ? -iStartCol : iStartCol);

		float *pfImgRow = (float*)ppImg.ImageRow( iCurrRow );
		float *pfFiltRow = (float*)ppTmp.ImageRow( i );

		for( int j = 0; j < ppTmp.Cols(); j++ )
		{
			dSum += (double)( pfImgRow[ iCurrCol ]*pfFiltRow[ j ] );

//			fprintf( outfile, "%d\n", (int)ppImg.ImageRow( iCurrRow )[ iCurrCol ] );

			if( iCurrCol + iColInc < 0 
				|| iCurrCol + iColInc >= ppImg.Cols() )
			{
				iColInc = -iColInc;
			}
			iCurrCol += iColInc;
		}
		if( iCurrRow + iRowInc < 0
			|| iCurrRow + iRowInc >= ppImg.Rows() )
		{
			iRowInc = -iRowInc;
		}
		iCurrRow += iRowInc;
	}

//	fclose( outfile );
	fConvResult = (float)dSum;
}

void
convolve_zeroborder_char(
	const PpImage &ppImg,
	const PpImage &ppTmp,
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  )
{
	// Compute averages
	
	int iStartRow = iImgRowCenter - ppTmp.Rows()/2;
	int iStartCol;

	int iRowInc = (iStartRow < 0 ? -1 : 1);
	int iColInc;

	int iCurrRow = (iStartRow < 0 ? -iStartRow : iStartRow);
	int iCurrCol;

//	FILE *outfile = fopen( "temp.txt", "wt" );

	double dSum = 0.0;

	for( int i = 0; i < ppTmp.Rows(); i++ )
	{
		iStartCol = iImgColCenter - ppTmp.Cols()/2;
		iColInc = (iStartCol < 0 ? -1 : 1);
		iCurrCol = (iStartCol < 0 ? -iStartCol : iStartCol);

		unsigned char *pcImgRow = ppImg.ImageRow( iCurrRow );
		float *pfFiltRow = (float*)ppTmp.ImageRow( i );

		for( int j = 0; j < ppTmp.Cols(); j++ )
		{
			if( iColInc >= 0 && iRowInc >= 0 )
			{
				dSum += (double)( ((float)pcImgRow[ iCurrCol ])*pfFiltRow[ j ] );
			}

//			fprintf( outfile, "%d\n", (int)ppImg.ImageRow( iCurrRow )[ iCurrCol ] );

			if( iCurrCol + iColInc < 0 
				|| iCurrCol + iColInc >= ppImg.Cols() )
			{
				iColInc = -iColInc;
			}
			iCurrCol += iColInc;
		}
		if( iCurrRow + iRowInc < 0
			|| iCurrRow + iRowInc >= ppImg.Rows() )
		{
			iRowInc = -iRowInc;
		}
		iCurrRow += iRowInc;
	}

//	fclose( outfile );
	fConvResult = (float)dSum;
}

void
convolve_zeroborder_float(
	const PpImage &ppImg,
	const PpImage &ppTmp,
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  )
{
	// Compute averages
	
	int iStartRow = iImgRowCenter - ppTmp.Rows()/2;
	int iStartCol;

	int iRowInc = (iStartRow < 0 ? -1 : 1);
	int iColInc;

	int iCurrRow = (iStartRow < 0 ? -iStartRow : iStartRow);
	int iCurrCol;

//	FILE *outfile = fopen( "temp.txt", "wt" );

	double dSum = 0.0;

	for( int i = 0; i < ppTmp.Rows(); i++ )
	{
		iStartCol = iImgColCenter - ppTmp.Cols()/2;
		iColInc = (iStartCol < 0 ? -1 : 1);
		iCurrCol = (iStartCol < 0 ? -iStartCol : iStartCol);

		float *pfImgRow = (float*)ppImg.ImageRow( iCurrRow );
		float *pfFiltRow = (float*)ppTmp.ImageRow( i );

		for( int j = 0; j < ppTmp.Cols(); j++ )
		{
			if( iColInc >= 0 && iRowInc >= 0 )
			{
				dSum += (double)( pfImgRow[ iCurrCol ]*pfFiltRow[ j ] );
			}

//			fprintf( outfile, "%d\n", (int)ppImg.ImageRow( iCurrRow )[ iCurrCol ] );

			if( iCurrCol + iColInc < 0 
				|| iCurrCol + iColInc >= ppImg.Cols() )
			{
				iColInc = -iColInc;
			}
			iCurrCol += iColInc;
		}
		if( iCurrRow + iRowInc < 0
			|| iCurrRow + iRowInc >= ppImg.Rows() )
		{
			iRowInc = -iRowInc;
		}
		iCurrRow += iRowInc;
	}

//	fclose( outfile );
	fConvResult = (float)dSum;
}

void
covariance_zeroborder_float(
	const PpImage &ppImg,
	const PpImage &ppTmp,
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  )
{
	// Compute averages
	
	int iStartRow = iImgRowCenter - ppTmp.Rows()/2;
	int iStartCol;

	int iRowInc = (iStartRow < 0 ? -1 : 1);
	int iColInc;

	int iCurrRow = (iStartRow < 0 ? -iStartRow : iStartRow);
	int iCurrCol;

//	FILE *outfile = fopen( "temp.txt", "wt" );

	double dSum = 0.0;
	double dSumTmp = 0.0;
	double dSumImg = 0.0;
	int iCount = 0;

	for( int i = 0; i < ppTmp.Rows(); i++ )
	{
		iStartCol = iImgColCenter - ppTmp.Cols()/2;
		iColInc = (iStartCol < 0 ? -1 : 1);
		iCurrCol = (iStartCol < 0 ? -iStartCol : iStartCol);

		float *pfImgRow = (float*)ppImg.ImageRow( iCurrRow );
		float *pfFiltRow = (float*)ppTmp.ImageRow( i );

		for( int j = 0; j < ppTmp.Cols(); j++ )
		{
			if( iColInc >= 0 && iRowInc >= 0 )
			{
				dSum += (double)( pfImgRow[ iCurrCol ]*pfFiltRow[ j ] );
				dSumImg += (double)( pfImgRow[ iCurrCol ] );
				dSumTmp += (double)( pfFiltRow[ j ] );
				iCount++;
			}

//			fprintf( outfile, "%d\n", (int)ppImg.ImageRow( iCurrRow )[ iCurrCol ] );

			if( iCurrCol + iColInc < 0 
				|| iCurrCol + iColInc >= ppImg.Cols() )
			{
				iColInc = -iColInc;
			}
			iCurrCol += iColInc;
		}
		if( iCurrRow + iRowInc < 0
			|| iCurrRow + iRowInc >= ppImg.Rows() )
		{
			iRowInc = -iRowInc;
		}
		iCurrRow += iRowInc;
	}

//	fclose( outfile );
	fConvResult = (float)(dSum - dSumTmp*dSumImg / (float)(iCount));
}

void
ssd_zeroborder_float(
	const PpImage &ppImg,
	const PpImage &ppTmp,
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  )
{
	// Compute averages
	
	int iStartRow = iImgRowCenter - ppTmp.Rows()/2;
	int iStartCol;

	int iRowInc = (iStartRow < 0 ? -1 : 1);
	int iColInc;

	int iCurrRow = (iStartRow < 0 ? -iStartRow : iStartRow);
	int iCurrCol;

//	FILE *outfile = fopen( "temp.txt", "wt" );

	double dSum = 0.0;
	int iCount = 0;

	for( int i = 0; i < ppTmp.Rows(); i++ )
	{
		iStartCol = iImgColCenter - ppTmp.Cols()/2;
		iColInc = (iStartCol < 0 ? -1 : 1);
		iCurrCol = (iStartCol < 0 ? -iStartCol : iStartCol);

		float *pfImgRow = (float*)ppImg.ImageRow( iCurrRow );
		float *pfFiltRow = (float*)ppTmp.ImageRow( i );

		for( int j = 0; j < ppTmp.Cols(); j++ )
		{
			if( iColInc >= 0 && iRowInc >= 0 )
			{
				float fDiff = pfImgRow[ iCurrCol ] - pfFiltRow[ j ];
				dSum += (double)( fDiff*fDiff );
				iCount++;
			}

//			fprintf( outfile, "%d\n", (int)ppImg.ImageRow( iCurrRow )[ iCurrCol ] );

			if( iCurrCol + iColInc < 0 
				|| iCurrCol + iColInc >= ppImg.Cols() )
			{
				iColInc = -iColInc;
			}
			iCurrCol += iColInc;
		}
		if( iCurrRow + iRowInc < 0
			|| iCurrRow + iRowInc >= ppImg.Rows() )
		{
			iRowInc = -iRowInc;
		}
		iCurrRow += iRowInc;
	}

//	fclose( outfile );
	fConvResult = (float)(dSum / (float)(iCount));
}


void
covariance_reflection_float(
	const PpImage &ppImg,
	const PpImage &ppTmp,
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  )
{
	// Compute averages
	
	int iStartRow = iImgRowCenter - ppTmp.Rows()/2;
	int iStartCol;

	int iRowInc = (iStartRow < 0 ? -1 : 1);
	int iColInc;

	int iCurrRow = (iStartRow < 0 ? -iStartRow : iStartRow);
	int iCurrCol;

//	FILE *outfile = fopen( "temp.txt", "wt" );

	double dSum = 0.0;
	double dSumTmp = 0.0;
	double dSumImg = 0.0;

	for( int i = 0; i < ppTmp.Rows(); i++ )
	{
		iStartCol = iImgColCenter - ppTmp.Cols()/2;
		iColInc = (iStartCol < 0 ? -1 : 1);
		iCurrCol = (iStartCol < 0 ? -iStartCol : iStartCol);

		float *pfImgRow = (float*)ppImg.ImageRow( iCurrRow );
		float *pfFiltRow = (float*)ppTmp.ImageRow( i );

		for( int j = 0; j < ppTmp.Cols(); j++ )
		{
			dSum += (double)( pfImgRow[ iCurrCol ]*pfFiltRow[ j ] );
			dSumTmp += (double)( pfImgRow[ iCurrCol ] );
			dSumImg += (double)( pfFiltRow[ j ] );

//			fprintf( outfile, "%d\n", (int)ppImg.ImageRow( iCurrRow )[ iCurrCol ] );

			if( iCurrCol + iColInc < 0 
				|| iCurrCol + iColInc >= ppImg.Cols() )
			{
				iColInc = -iColInc;
			}
			iCurrCol += iColInc;
		}
		if( iCurrRow + iRowInc < 0
			|| iCurrRow + iRowInc >= ppImg.Rows() )
		{
			iRowInc = -iRowInc;
		}
		iCurrRow += iRowInc;
	}

//	fclose( outfile );
	fConvResult = (float)(dSum - dSumTmp*dSumImg / (float)(ppTmp.Rows()*ppTmp.Cols()));
}

void
ssd_reflection_float(
	const PpImage &ppImg,
	const PpImage &ppTmp,
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  )
{
	// Compute averages
	
	int iStartRow = iImgRowCenter - ppTmp.Rows()/2;
	int iStartCol;

	int iRowInc = (iStartRow < 0 ? -1 : 1);
	int iColInc;

	int iCurrRow = (iStartRow < 0 ? -iStartRow : iStartRow);
	int iCurrCol;

//	FILE *outfile = fopen( "temp.txt", "wt" );

	double dSum = 0.0;

	for( int i = 0; i < ppTmp.Rows(); i++ )
	{
		iStartCol = iImgColCenter - ppTmp.Cols()/2;
		iColInc = (iStartCol < 0 ? -1 : 1);
		iCurrCol = (iStartCol < 0 ? -iStartCol : iStartCol);

		float *pfImgRow = (float*)ppImg.ImageRow( iCurrRow );
		float *pfFiltRow = (float*)ppTmp.ImageRow( i );

		for( int j = 0; j < ppTmp.Cols(); j++ )
		{
			float fDiff = pfImgRow[ iCurrCol ] - pfFiltRow[ j ];
			dSum += (double)( fDiff*fDiff );

//			fprintf( outfile, "%d\n", (int)ppImg.ImageRow( iCurrRow )[ iCurrCol ] );

			if( iCurrCol + iColInc < 0 
				|| iCurrCol + iColInc >= ppImg.Cols() )
			{
				iColInc = -iColInc;
			}
			iCurrCol += iColInc;
		}
		if( iCurrRow + iRowInc < 0
			|| iCurrRow + iRowInc >= ppImg.Rows() )
		{
			iRowInc = -iRowInc;
		}
		iCurrRow += iRowInc;
	}

//	fclose( outfile );
	fConvResult = (float)(dSum / (float)(ppTmp.Rows()*ppTmp.Cols()));
}

//
// ssd_reflection_float_remove_mean()
//
// Calculates the ssd, but removes the mean from both
// signals.
//
void
ssd_reflection_float_remove_mean(
	const PpImage &ppImg,
	const PpImage &ppTmp,
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  )
{
	// Compute averages
	
	int iStartRow = iImgRowCenter - ppTmp.Rows()/2;
	int iStartCol;

	int iRowInc = (iStartRow < 0 ? -1 : 1);
	int iColInc;

	int iCurrRow = (iStartRow < 0 ? -iStartRow : iStartRow);
	int iCurrCol;

//	FILE *outfile = fopen( "temp.txt", "wt" );

	double dSumXX = 0.0;
	double dSumXY = 0.0;
	double dSumYY = 0.0;
	double dSumX = 0.0;
	double dSumY = 0.0;

	for( int i = 0; i < ppTmp.Rows(); i++ )
	{
		iStartCol = iImgColCenter - ppTmp.Cols()/2;
		iColInc = (iStartCol < 0 ? -1 : 1);
		iCurrCol = (iStartCol < 0 ? -iStartCol : iStartCol);

		float *pfImgRow = (float*)ppImg.ImageRow( iCurrRow );
		float *pfFiltRow = (float*)ppTmp.ImageRow( i );

		for( int j = 0; j < ppTmp.Cols(); j++ )
		{
			dSumXX += pfImgRow[ iCurrCol ]*pfImgRow[ iCurrCol ];
			dSumXY += pfImgRow[ iCurrCol ]*pfFiltRow[ j ];
			dSumYY += pfFiltRow[ j ]*pfFiltRow[ j ];
			dSumX += pfImgRow[ iCurrCol ];
			dSumY += pfFiltRow[ j ];

//			fprintf( outfile, "%d\n", (int)ppImg.ImageRow( iCurrRow )[ iCurrCol ] );

			if( iCurrCol + iColInc < 0 
				|| iCurrCol + iColInc >= ppImg.Cols() )
			{
				iColInc = -iColInc;
			}
			iCurrCol += iColInc;
		}
		if( iCurrRow + iRowInc < 0
			|| iCurrRow + iRowInc >= ppImg.Rows() )
		{
			iRowInc = -iRowInc;
		}
		iCurrRow += iRowInc;
	}

	float fCount = ppTmp.Rows()*ppTmp.Cols();
	dSumX /= fCount;
	dSumY /= fCount;
	float dSum = dSumXX + dSumYY - 2.0*dSumXY
		+ fCount*(2.0*dSumX*dSumY - dSumX*dSumX - dSumY*dSumY);

//	fclose( outfile );
	fConvResult = (float)(dSum / fCount);
}

void
variance_zeroborder_float(
	const PpImage &ppImg,
	const PpImage &ppTmp,
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  )
{
	// Compute averages
	
	int iStartRow = iImgRowCenter - ppTmp.Rows()/2;
	int iStartCol;

	int iRowInc = (iStartRow < 0 ? -1 : 1);
	int iColInc;

	int iCurrRow = (iStartRow < 0 ? -iStartRow : iStartRow);
	int iCurrCol;

//	FILE *outfile = fopen( "temp.txt", "wt" );

	double dSum = 0.0;
	double dSumImg = 0.0;
	int iCount = 0;

	for( int i = 0; i < ppTmp.Rows(); i++ )
	{
		iStartCol = iImgColCenter - ppTmp.Cols()/2;
		iColInc = (iStartCol < 0 ? -1 : 1);
		iCurrCol = (iStartCol < 0 ? -iStartCol : iStartCol);

		float *pfImgRow = (float*)ppImg.ImageRow( iCurrRow );

		for( int j = 0; j < ppTmp.Cols(); j++ )
		{
			if( iColInc >= 0 && iRowInc >= 0 )
			{
				dSum += (double)( pfImgRow[ iCurrCol ]*pfImgRow[ iCurrCol ] );
				dSumImg += (double)( pfImgRow[ iCurrCol ] );
				iCount++;
			}

//			fprintf( outfile, "%d\n", (int)ppImg.ImageRow( iCurrRow )[ iCurrCol ] );

			if( iCurrCol + iColInc < 0 
				|| iCurrCol + iColInc >= ppImg.Cols() )
			{
				iColInc = -iColInc;
			}
			iCurrCol += iColInc;
		}
		if( iCurrRow + iRowInc < 0
			|| iCurrRow + iRowInc >= ppImg.Rows() )
		{
			iRowInc = -iRowInc;
		}
		iCurrRow += iRowInc;
	}

//	fclose( outfile );
	fConvResult = (float)(dSum - dSumImg*dSumImg / (float)(iCount));
}

void
variance_reflection_float(
	const PpImage &ppImg,
	const PpImage &ppTmp,
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  )
{
	// Compute averages
	
	int iStartRow = iImgRowCenter - ppTmp.Rows()/2;
	int iStartCol;

	int iRowInc = (iStartRow < 0 ? -1 : 1);
	int iColInc;

	int iCurrRow = (iStartRow < 0 ? -iStartRow : iStartRow);
	int iCurrCol;

//	FILE *outfile = fopen( "temp.txt", "wt" );

	double dSum = 0.0;
	double dSumImg = 0.0;

	for( int i = 0; i < ppTmp.Rows(); i++ )
	{
		iStartCol = iImgColCenter - ppTmp.Cols()/2;
		iColInc = (iStartCol < 0 ? -1 : 1);
		iCurrCol = (iStartCol < 0 ? -iStartCol : iStartCol);

		float *pfImgRow = (float*)ppImg.ImageRow( iCurrRow );

		for( int j = 0; j < ppTmp.Cols(); j++ )
		{
			dSum += (double)( pfImgRow[ iCurrCol ]*pfImgRow[ iCurrCol ] );
			dSumImg += (double)( pfImgRow[ iCurrCol ] );

//			fprintf( outfile, "%d\n", (int)ppImg.ImageRow( iCurrRow )[ iCurrCol ] );

			if( iCurrCol + iColInc < 0 
				|| iCurrCol + iColInc >= ppImg.Cols() )
			{
				iColInc = -iColInc;
			}
			iCurrCol += iColInc;
		}
		if( iCurrRow + iRowInc < 0
			|| iCurrRow + iRowInc >= ppImg.Rows() )
		{
			iRowInc = -iRowInc;
		}
		iCurrRow += iRowInc;
	}

//	fclose( outfile );
	fConvResult = (float)(dSum - dSumImg*dSumImg / (float)(ppTmp.Rows()*ppTmp.Cols()));
}


//
// convolve_image_reflection()
//
// Convolves image ppImgIn with mask ppTmpIn, puts result in ppImgOut.
//
//	ppImgIn - input greyscale image, type char (8-bits per pixel)
//	ppTmpIn - input convolution mask image, type integer (32-bits per pixel)
//  ppImgOut - output of convolution, type float (32-bits per pixel)
//
void
convolve_image_reflection_char(
				  const PpImage &ppImgIn,	// Input char image
				  const PpImage &ppTmpIn,	// Input float mask
				  PpImage		&ppImgOut	// Output float image
					)
{
	float fConvResult;

	for( int i = 0; i < ppImgIn.Rows(); i++ )
	{
		float *pImgOutRow = (float*)ppImgOut.ImageRow( i );

		for( int j = 0; j < ppImgIn.Cols(); j++ )
		{
			convolve_reflection_char(
				ppImgIn, ppTmpIn,
				i, j,
				fConvResult
				);

			pImgOutRow[ j ]  = fConvResult;
		}
	}
}

void
convolve_image_reflection_float(
				  const PpImage &ppImgIn,	// Input float image
				  const PpImage &ppTmpIn,	// Input float mask
				  PpImage		&ppImgOut	// Output float image
					)
{
	float fConvResult;

	for( int i = 0; i < ppImgIn.Rows(); i++ )
	{
		float *pImgOutRow = (float*)ppImgOut.ImageRow( i );

		for( int j = 0; j < ppImgIn.Cols(); j++ )
		{
			convolve_reflection_float(
				ppImgIn, ppTmpIn,
				i, j,
				fConvResult
				);

			pImgOutRow[ j ]  = fConvResult;
		}
	}
}

void
convolve_image_zeroborder_float(
				  const PpImage &ppImgIn,	// Input float image
				  const PpImage &ppTmpIn,	// Input float mask
				  PpImage		&ppImgOut	// Output float image
					)
{
	float fConvResult;

	for( int i = 0; i < ppImgIn.Rows(); i++ )
	{
		float *pImgOutRow = (float*)ppImgOut.ImageRow( i );

		for( int j = 0; j < ppImgIn.Cols(); j++ )
		{
			convolve_zeroborder_float(
				ppImgIn, ppTmpIn,
				i, j,
				fConvResult
				);

			pImgOutRow[ j ]  = fConvResult;
		}
	}
}

void
covariance_image_reflection_float(
				  const PpImage &ppImgIn,	// Input float image
				  const PpImage &ppTmpIn,	// Input float mask
				  PpImage		&ppImgOut	// Output float image
					)
{
	float fConvResult;

	for( int i = 0; i < ppImgIn.Rows(); i++ )
	{
		float *pImgOutRow = (float*)ppImgOut.ImageRow( i );

		for( int j = 0; j < ppImgIn.Cols(); j++ )
		{
			covariance_reflection_float(
				ppImgIn, ppTmpIn,
				i, j,
				fConvResult
				);

			pImgOutRow[ j ]  = fConvResult;
		}
	}
}

void
variance_image_zeroborder_float(
				  const PpImage &ppImgIn,	// Input float image
				  const PpImage &ppTmpIn,	// Input filter size
				  PpImage		&ppImgOut	// Output float image
					)
{	
	float fConvResult;

	for( int i = 0; i < ppImgIn.Rows(); i++ )
	{
		float *pImgOutRow = (float*)ppImgOut.ImageRow( i );

		for( int j = 0; j < ppImgIn.Cols(); j++ )
		{
			variance_zeroborder_float(
				ppImgIn, ppTmpIn,
				i, j,
				fConvResult
				);

			pImgOutRow[ j ]  = fConvResult;
		}
	}
}

void
variance_image_reflection_float(
				  const PpImage &ppImgIn,	// Input float image
				  const PpImage &ppTmpIn,	// Input filter size
				  PpImage		&ppImgOut	// Output float image
					)
{
	float fConvResult;

	for( int i = 0; i < ppImgIn.Rows(); i++ )
	{
		float *pImgOutRow = (float*)ppImgOut.ImageRow( i );

		for( int j = 0; j < ppImgIn.Cols(); j++ )
		{
			variance_reflection_float(
				ppImgIn, ppTmpIn,
				i, j,
				fConvResult
				);

			pImgOutRow[ j ]  = fConvResult;
		}
	}
}

void
normalized_covariance_image_reflection_float(
				  const PpImage &ppImgIn,		// Input float image
				  const PpImage &ppTmpIn,		// Input float mask
				  const PpImage &ppImgVarrIn,	// Image of varriances for ppImgIn
				  const float	&fVarrTmp,		// Varriance of ppTempIn
				  PpImage		&ppImgOut		// Output float image
					)
{
	float fConvResult;

	if( fVarrTmp == 0 )
	{
		// I
		memset( ppImgOut.ImageRow(0), 0, ppImgOut.Rows()*ppImgOut.Cols()*sizeof(float) );
		return;
	}
	float fStdDevTmp = (float)sqrt( fVarrTmp );

	for( int i = 0; i < ppImgIn.Rows(); i++ )
	{
		float *pImgOutRow = (float*)ppImgOut.ImageRow( i );
		float *pfVarrRow = (float*)ppImgVarrIn.ImageRow( i );

		for( int j = 0; j < ppImgIn.Cols(); j++ )
		{
			if( pfVarrRow[j] == 0 )
			{
				pImgOutRow[ j ] = 0;
			}
			else
			{
				covariance_reflection_float(
					ppImgIn, ppTmpIn,
					i, j,
					fConvResult
					);
				pImgOutRow[ j ]  = (float)(fConvResult / ( fStdDevTmp*sqrt(pfVarrRow[j]) ));
			}
		}
	}
}

void
normalized_covariance_image_zeroborder_float(
				  const PpImage &ppImgIn,		// Input float image
				  const PpImage &ppTmpIn,		// Input float mask
				  const PpImage &ppImgVarrIn,	// Image of varriances for ppImgIn
				  const float	&fVarrTmp,		// Varriance of ppTempIn
				  PpImage		&ppImgOut		// Output float image
					)
{
	float fConvResult;

	if( fVarrTmp == 0 )
	{
		// I
		memset( ppImgOut.ImageRow(0), 0, ppImgOut.Rows()*ppImgOut.Cols()*sizeof(float) );
		return;
	}
	float fStdDevTmp = (float)sqrt( fVarrTmp );

	for( int i = 0; i < ppImgIn.Rows(); i++ )
	{
		float *pImgOutRow = (float*)ppImgOut.ImageRow( i );
		float *pfVarrRow = (float*)ppImgVarrIn.ImageRow( i );

		for( int j = 0; j < ppImgIn.Cols(); j++ )
		{
			if( pfVarrRow[j] == 0 )
			{
				pImgOutRow[ j ] = 0;
			}
			else
			{
				covariance_zeroborder_float(
					ppImgIn, ppTmpIn,
					i, j,
					fConvResult
					);
				pImgOutRow[ j ]  = (float)(fConvResult / ( fStdDevTmp*sqrt(pfVarrRow[j]) ));
			}
		}
	}
}

void
ssd_image_zeroborder_float(
				  const PpImage &ppImgIn,	// Input float image
				  const PpImage &ppTmpIn,	// Input filter size
				  PpImage		&ppImgOut	// Output float image
					)
{	
	float fResult;

	for( int i = 0; i < ppImgIn.Rows(); i++ )
	{
		float *pImgOutRow = (float*)ppImgOut.ImageRow( i );

		for( int j = 0; j < ppImgIn.Cols(); j++ )
		{
			ssd_zeroborder_float(
				ppImgIn, ppTmpIn,
				i, j,
				fResult
				);

			pImgOutRow[ j ] = fResult;
		}
	}
}

void
ssd_image_reflection_float(
				  const PpImage &ppImgIn,	// Input float image
				  const PpImage &ppTmpIn,	// Input filter size
				  PpImage		&ppImgOut	// Output float image
					)
{
	float fResult;

	for( int i = 0; i < ppImgIn.Rows(); i++ )
	{
		float *pImgOutRow = (float*)ppImgOut.ImageRow( i );

		for( int j = 0; j < ppImgIn.Cols(); j++ )
		{
			ssd_reflection_float_remove_mean(
			//ssd_reflection_float(
				ppImgIn, ppTmpIn,
				i, j,
				fResult
				);
			pImgOutRow[ j ] = fResult;
		}
	}
}