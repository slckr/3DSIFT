
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "SVD.h"

#include "PpImageFloatOutput.h"
#include "featureio_image.h"
#include "HistogramFeatures.h"

#define PI 3.1415926535897932384626433832795

//
// computeEntropy()
//
// Compute entropy for a vector of positive floats.
//
float
computeEntropy(
			   int iCount,
			   float *pfProbVec
			   )
{
	float fSum = 0;
	for( int k = 0; k < iCount; k++ )
	{
		fSum += pfProbVec[k];
	}

	float fEntropy = 0;
	if( fSum < 0 )
	{
		fEntropy = log( (float)iCount );
	}
	else
	{
		float fDen = 1.0f/fSum;
		for( int k = 0; k < iCount; k++ )
		{
			if( pfProbVec[k] > 0 )
			{
				float fProb = pfProbVec[k]*fDen;
				fEntropy -= fProb*log(fProb);
			}
		}
	}

	return fEntropy;
}

void
hfGenerateHistogramFeatureVectorChar(
						   PpImage &ppImg,
						   int iRowStart,
						   int iColStart,
						   int iRows,
						   int iCols,
						   int iBins,
						   float *pfFeatures
						   )
{
	int iMin = 256;
	int iMax = -1;

	int piArray[256];
	memset( piArray, 0, sizeof(piArray) );

	int r, c;

	for( r = 0; r < iBins; r++ )
	{
		pfFeatures[r] = 0.0f;
	}

	for( r = iRowStart; r < iRowStart + iRows; r++ )
	{
		unsigned char *pucRow = ppImg.ImageRow(r);
		for( int c = iColStart; c < iColStart + iCols; c++ )
		{
			piArray[pucRow[c]]++;
			if( pucRow[c] > iMax )
			{
				iMax = pucRow[c];
			}
			if( pucRow[c] < iMin )
			{
				iMin = pucRow[c];
			}
		}
	}

	float fBinSize = (iMax - iMin) / (float)iBins;
	
	for( r = iRowStart; r < iRowStart + iRows; r++ )
	{
		unsigned char *pucRow = ppImg.ImageRow(r);
		for( int c = iColStart; c < iColStart + iCols; c++ )
		{
			int iBinIndex = (int)(pucRow[c]*fBinSize);
			assert( iBinIndex >= 0 );
			assert( iBinIndex < iBins );
			pfFeatures[iBinIndex] += 1.0f;
		}
	}
}

int
hfGenerateHistogramFeaturesChar(
							PpImage &ppImg,
							FEATUREIO &fioHist,
							int iWindowRows,
							int iWindowCols
							)
{
	int iBins = fioHist.iFeaturesPerVector;

	for( int r = 0; r < ppImg.Rows(); r++ )
	{
		int iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > ppImg.Rows() )
		{
			iRowStart = ppImg.Rows() - iWindowRows;
		}

		for( int c = 0; c < ppImg.Cols(); c++ )
		{			
			int iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > ppImg.Cols() )
			{
				iColStart = ppImg.Cols() - iWindowCols;
			}

			hfGenerateHistogramFeatureVectorChar( ppImg, iRowStart, iColStart,
						   iWindowRows, iWindowCols, iBins, fioHist.pfVectors + r*fioHist.x + c );

		}
	}

	return 1;
}

static int
sort_float(
		   const void *p1,
		   const void *p2
		   )
{
	float f1 = *((float*)p1);
	float f2 = *((float*)p2);
	if( f1 < f2 )
	{
		return -1;
	}
	else if( f1 > f2 )
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void
hfGenerateHistogramFeatureVectorFloat(
						   FEATUREIO &fioImg,
						   int iRowStart,
						   int iColStart,
						   int iRows,
						   int iCols,
						   int iBins,
						   float *pfFeatures
						   )
{
	float fMin = fioImg.pfVectors[iRowStart*fioImg.x + iColStart];
	float fMax = fioImg.pfVectors[iRowStart*fioImg.x + iColStart];

	int r, c;

	for( r = 0; r < iBins; r++ )
	{
		pfFeatures[r] = 0.0f;
	}

	for( r = iRowStart; r < iRowStart + iRows; r++ )
	{
		float *pfRow = fioImg.pfVectors + r*fioImg.x;
		for( int c = iColStart; c < iColStart + iCols; c++ )
		{
			if( pfRow[c] > fMax )
			{
				fMax = pfRow[c];
			}
			if( pfRow[c] < fMin )
			{
				fMin = pfRow[c];
			}
		}
	}

	float fBinSize = (fMax - fMin) / (float)iBins;

	for( r = iRowStart; r < iRowStart + iRows; r++ )
	{
		float *pfRow = fioImg.pfVectors + r*fioImg.x;
		for( int c = iColStart; c < iColStart + iCols; c++ )
		{
			int iBinIndex = (int)( (pfRow[c]-fMin) / fBinSize );
			assert( iBinIndex >= 0 );
			assert( iBinIndex <= iBins );
			if( iBinIndex == iBins )
			{
				iBinIndex--;
			}
			pfFeatures[iBinIndex] += 1.0f;
		}
	}
}

int
hfGenerateHistogramFeaturesFloat(
							FEATUREIO &fioImg,
							FEATUREIO &fioHist,
							int iWindowRows,
							int iWindowCols
							)
{
	int iBins = fioHist.iFeaturesPerVector;

	for( int r = 0; r < fioImg.y; r++ )
	{
		int iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fioImg.y )
		{
			iRowStart = fioImg.y - iWindowRows;
		}

		for( int c = 0; c < fioImg.x; c++ )
		{			
			int iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fioImg.x )
			{
				iColStart = fioImg.x - iWindowCols;
			}

			hfGenerateHistogramFeatureVectorFloat( fioImg, iRowStart, iColStart,
						   iWindowRows, iWindowCols, iBins, fioHist.pfVectors + (r*fioHist.x + c)*fioHist.iFeaturesPerVector );
		}
	}

	return 1;
}

//
// hfGenerateHistogramRangesUniformProbabilityVectorFloat()
//
// Generates histogram ranges such that intensity frequencies
// are uniformly distributed across ranges.
//
void
hfGenerateHistogramRangesUniformProbabilityVectorFloat(
						   FEATUREIO &fioImg,
						   int iRowStart,
						   int iColStart,
						   int iRows,
						   int iCols,
						   int iRanges,
						   float *pfRanges,
						   float *pfIntensities
						   )
{
	int r, c;

	for( r = 0; r < iRows; r++ )
	{
		float *pfRow = fioImg.pfVectors + (iRowStart + r)*fioImg.x;
		for( int c = 0; c < iCols; c++ )
		{
			pfIntensities[r*iCols + c] = pfRow[iColStart+c];
		}
	}

	qsort(pfIntensities, iRows*iCols, sizeof(float), sort_float );

	for( r = 0; r < iRanges; r++ )
	{
		pfRanges[r] = pfIntensities[ ((r+1)*iRows*iCols) / (iRanges+1) ];
	}
}

//
// hfGenerateHistogramRangesUniformIntensityVectorFloat()
//
// Generates histogram ranges uniformly between min & max intensities
// within the current image window.
//
void
hfGenerateHistogramRangesUniformIntensityVectorFloat(
						   FEATUREIO &fioImg,
						   int iRowStart,
						   int iColStart,
						   int iRows,
						   int iCols,
						   int iRanges,
						   float *pfRanges,
						   float *pfIntensities
						   )
{
	int r, c;

	float fMin = fioImg.pfVectors[iRowStart*fioImg.x + iColStart];
	float fMax = fioImg.pfVectors[iRowStart*fioImg.x + iColStart];

	for( r = 0; r < iRows; r++ )
	{
		float *pfRow = fioImg.pfVectors + (iRowStart + r)*fioImg.x;
		for( int c = 0; c < iCols; c++ )
		{
			if( pfRow[iColStart+c] < fMin )
			{
				fMin = pfRow[iColStart+c];
			}
			if( pfRow[iColStart+c] > fMax )
			{
				fMax = pfRow[iColStart+c];
			}
		}
	}

	//fMin = 0; fMax = 250;
	float fBinSize = (fMax - fMin) / (float)(iRanges + 1);

	pfRanges[0] = fBinSize + fMin;
	for( r = 1; r < iRanges; r++ )
	{
		pfRanges[r] = fBinSize + pfRanges[r-1];
	}
}

//
// hfGenerateHistogramRangesMSTVectorFloat()
//
// Quantize histogram using a minimum spanning tree segmentation approach.
//
void
hfGenerateHistogramRangesMSTVectorFloat(
						   FEATUREIO &fioImg,
						   int iRowStart,
						   int iColStart,
						   int iRows,
						   int iCols,
						   int iRanges,
						   float *pfRanges,
						   float *pfIntensities
						   )
{	int r, c;

	for( r = 0; r < iRows; r++ )
	{
		float *pfRow = fioImg.pfVectors + (iRowStart + r)*fioImg.x;
		for( int c = 0; c < iCols; c++ )
		{
			pfIntensities[r*iCols + c] = pfRow[iColStart+c];
		}
	}

	qsort(pfIntensities, iRows*iCols, sizeof(float), sort_float );

	for( r = 0; r < iRanges; r++ )
	{
		pfRanges[r] = pfIntensities[ ((r+1)*iRows*iCols) / (iRanges+1) ];
	}
}

//
// hfGenerateHistogramRangesLloydMaxVectorFloat()
//
// Quantize histogram to minimized squared error distortion:
//
//   Distortion = sum[ P(x_i)(mean_i - x_i)(mean_i - x_i) ]
//       or
//   Distortion = E[(mean_i - x_i)(mean_i - x_i)]
//
void
hfGenerateHistogramRangesLloydMaxVectorFloat(
						   FEATUREIO &fioImg,
						   int iRowStart,
						   int iColStart,
						   int iRows,
						   int iCols,
						   int iRanges,
						   float *pfRanges,
						   float *pfIntensities
						   )
{
	int r, c;

	float	pfMeans[500];
	float	pfSums[500];
	int		piCounts[500];
	int		piSwitchValues[500];
	if( iRanges >= sizeof( pfMeans ) / sizeof(float) )
	{
		printf( "Error: more ranges than means in hfGenerateHistogramRangesLloydMaxVectorFloat()\n" );
		return;
	}

	// Sort intensities
	//FILE *outfile;
	//outfile = fopen( "intensities.txt", "wt" );
	for( r = 0; r < iRows; r++ )
	{
		float *pfRow = fioImg.pfVectors + (iRowStart + r)*fioImg.x;
		for( int c = 0; c < iCols; c++ )
		{
			pfIntensities[r*iCols + c] = pfRow[iColStart+c];
			//fprintf( outfile, "%f\n", pfIntensities[r*iCols + c] );
		}
	}
	//fclose( outfile );

	int iValues = iRows*iCols;
	qsort(pfIntensities, iValues, sizeof(float), sort_float );

	// Init means
	int iMeans = iRanges + 1;
	int iStep = (iValues) / iMeans;
	for( r = 0; r < iMeans; r++ )
	{
		pfMeans[r] = pfIntensities[(iStep/2) + r*iStep];
		//pfMeans[r] = pfIntensities[r];
	}

	// Lloyd algorithm to minimize mean squared distortion
	
	float fDistPrev = 0, fDistCurr = 0;
	int iIteration = 0;
	//char pcFileName[300];
	do
	{
		fDistPrev = fDistCurr;

		int iCurrMean = 0; // the current closest mean

		pfSums[iCurrMean] = 0;
		piCounts[iCurrMean] = 0;

		// Classify, average
		int iValue;
		for( iValue = 0; iValue < iValues && iCurrMean + 1 < iMeans; iValue++ )
		{
			float fDist1 = pfIntensities[iValue] - pfMeans[iCurrMean];
			float fDist2 = pfIntensities[iValue] - pfMeans[iCurrMean+1];
			fDist1 *= fDist1;
			fDist2 *= fDist2;
			if( fDist2 > fDist1 )
			{
				// Current mean is the closest
				pfSums[iCurrMean] += pfIntensities[iValue];
				piCounts[iCurrMean] += 1;
			}
			else
			{
				// New mean is the closest
				piSwitchValues[iCurrMean] = iValue;
				iCurrMean++;
				pfSums[iCurrMean] = pfIntensities[iValue];
				piCounts[iCurrMean] = 1;
			}
		}
		for( iValue; iValue < iValues; iValue++ )
		{
			// Count up values remaining
			pfSums[iCurrMean] += pfIntensities[iValue];
			piCounts[iCurrMean] += 1;
		}

		// Calculate Means

		for( r = 0; r < iMeans; r++ )
		{
			pfMeans[r] = pfSums[r] / (float)piCounts[r];
		}

		// Calculate Distortion

		//sprintf( pcFileName, "intensity_class_%3.3d.txt", iIteration );
		//outfile = fopen( pcFileName, "wt" );
		fDistCurr = 0;
		iCurrMean = 0;
		for( iValue = 0; iValue < iValues; iValue++ )
		{
			if( iValue == piSwitchValues[iCurrMean] )
			{
				iCurrMean++;
			}
			float fDist = pfIntensities[iValue] - pfMeans[iCurrMean];
			fDistCurr += fDist*fDist;
			//fprintf( outfile, "%f\n", pfMeans[iCurrMean] );
		}
		//fclose( outfile );

		fDistCurr /= (float)iValues;
		iIteration++;

		//printf( "prev curr: %f %f\n", fDistPrev, fDistCurr );

	} while( fDistPrev != fDistCurr );
	
	//printf( "prev curr: %f %f\n", fDistPrev, fDistCurr );

	// Set ranges

	for( r = 0; r < iRanges; r++ )
	{
		pfRanges[r] = pfIntensities[ piSwitchValues[r] ];
	}
}

float
randf()
{
	return (float)(((double)(rand())) / ((double)(RAND_MAX + 1.0)));
}

#ifdef CHANGE_THIS_GMM_CODE

float
hfGmmProb(
		  float fValue,
			DIAG_GMM &dgmm
			)
{
	float fProbSum = 0;
	for( int iClass = 0; iClass < dgmm.iClasses; iClass++ )
	{
		float fDiff = fValue - dgmm.pfMeans[iClass];
		float fDiffSqr = fDiff*fDiff;
		float fExp = fDiffSqr / (-2.0*dgmm.pfVariances[iClass]);
		float fDivisor = sqrt( 2.0*PI*dgmm.pfVariances[iClass] );
		float pfClassProb = dgmm.pfPriors[iClass]*exp( fExp ) / fDivisor;
		fProbSum += pfClassProb;
	}
	return fProbSum;
}
float
hfGmmProb(
		  float fValue,
		  float *pfClassProbs,
			DIAG_GMM &dgmm
			)
{
	float fProbSum = 0;
	for( int iClass = 0; iClass < dgmm.iClasses; iClass++ )
	{
		float fDiff = fValue - dgmm.pfMeans[iClass];
		float fDiffSqr = fDiff*fDiff;
		float fExp = fDiffSqr / (-2.0*dgmm.pfVariances[iClass]);
		float fDivisor = sqrt( 2.0*PI*dgmm.pfVariances[iClass] );
		pfClassProbs[iClass] = dgmm.pfPriors[iClass]*exp( fExp ) / fDivisor;
		fProbSum += pfClassProbs[iClass];
	}
	return fProbSum;
}

//
// hfGenerateHistogramRangesGMMVectorFloat()
//
// Quantize histogram according to a diagonal GMM model:
//
// K classes: K means, K variances, K weights
//
//
void
hfGenerateHistogramRangesGMMVectorFloat(

						   FEATUREIO &fioImg,
						   int iRowStart,
						   int iColStart,
						   int iRows,
						   int iCols,

						   DIAG_GMM &dgmm,

						   float *pfIntensities,
						   float *pfTempArray		// should be of size iClasses*iRows*iCols
						   )
{
	int r, c;
	int iClasses = dgmm.iClasses;
	float *pfMeans = dgmm.pfMeans;
	float *pfVariances = dgmm.pfVariances;
	float *pfPriors = dgmm.pfPriors;

	// Sort intensities
	//FILE *outfile;
	//outfile = fopen( "intensities.txt", "wt" );
	for( r = 0; r < iRows; r++ )
	{
		float *pfRow = fioImg.pfVectors + (iRowStart + r)*fioImg.x;
		for( int c = 0; c < iCols; c++ )
		{
			pfIntensities[r*iCols + c] = pfRow[iColStart+c];
			//fprintf( outfile, "%f\n", pfIntensities[r*iCols + c] );
		}
	}
	//fclose( outfile );

	int iValues = iRows*iCols;
	qsort(pfIntensities, iValues, sizeof(float), sort_float );

	//FILE *outfile;
	//outfile = fopen( "intensities.txt", "wt" );
	//for( r = 0; r < iValues; r++ )
	//{
	//	fprintf( outfile, "%f\n", pfIntensities[r] );
	//}
	//fclose( outfile );

	// Init means, variances, classes
	int piPrevIndicies[MAX_GMM_CLASSES];
	int iClass;
	int iStep = iValues / (2*iClasses);
	for( iClass = 0; iClass < iClasses; iClass++ )
	{
		//pfMeans[iClass] = pfIntensities[(iStep/2) + iClass*iStep];
		int iIndex = (int)(randf()*(iValues - 1));
		for( int i = 0; i < iClass; i++ )
		{
			if( iIndex == piPrevIndicies[i] )
			{
				iIndex = (int)(randf()*(iValues - 1));
				i = 0;
			}
		}
		pfMeans[iClass] = pfIntensities[ iIndex ];
		pfVariances[iClass] = (pfIntensities[iRows*iCols - 1] - pfIntensities[0]);
		pfPriors[iClass] = 1.0f / (float)iClasses;
	}

	float fProbPrev = 0.0f, fProbCurr = 1.0f;
	int iIteration = 0;
	//char pcFileName[300];
	while( fProbPrev < fProbCurr )
	{
		fProbPrev = fProbCurr;

		// Classify, average
		for( int iValue = 0; iValue < iValues; iValue++ )
		{
			// Calculate p(Class|Value) = p(Value|Class)*p(Class)*K

			float fProbSum = 0;
			float fValue = pfIntensities[iValue];
			float *pfLike = pfTempArray + iValue*iClasses;
			for( iClass = 0; iClass < iClasses; iClass++ )
			{
				float fDiff = fValue - pfMeans[iClass];
				float fDiffSqr = fDiff*fDiff;
				float fExp = fDiffSqr / (-2.0*pfVariances[iClass]);
				float fDivisor = sqrt( 2.0*PI*pfVariances[iClass] );
				pfLike[iClass] = pfPriors[iClass]*exp( fExp ) / fDivisor;
				fProbSum += pfLike[iClass];
			}
			// Normalize 
			for( iClass = 0; iClass < iClasses; iClass++ )
			{
				pfLike[iClass] = pfLike[iClass] / fProbSum;
			}
		}

		// Recalculate means, variances
		for( iClass = 0; iClass < iClasses; iClass++ )
		{
			pfPriors[iClass] = 0;
			pfMeans[iClass] = 0;
			pfVariances[iClass] = 0;
		}

		for( iValue = 0; iValue < iValues; iValue++ )
		{
			float *pfLike = pfTempArray + iValue*iClasses;
			float fValue = pfIntensities[iValue];
			for( iClass = 0; iClass < iClasses; iClass++ )
			{
				pfPriors[iClass] += pfLike[iClass];
				pfMeans[iClass] += pfLike[iClass]*fValue;
				pfVariances[iClass] += pfLike[iClass]*fValue*fValue;
			}
		}
		for( iClass = 0; iClass < iClasses; iClass++ )
		{
			pfMeans[iClass] = pfMeans[iClass] / pfPriors[iClass];
			pfVariances[iClass] = (pfVariances[iClass] / pfPriors[iClass]) - pfMeans[iClass]*pfMeans[iClass];
			pfPriors[iClass] = pfPriors[iClass] / (float)iValues;
		}

		// Calculate Distortion/probability of data - should be getting more probable.

		//sprintf( pcFileName, "intensity_class_%3.3d.txt", iIteration );
		//outfile = fopen( pcFileName, "wt" );
		fProbCurr = 0;

		for( iValue = 0; iValue < iValues; iValue++ )
		{
			float *pfLike = pfTempArray + iValue*iClasses;
			float fValue = pfIntensities[iValue];
			for( iClass = 0; iClass < iClasses; iClass++ )
			{
				float fDiff = fValue - pfMeans[iClass];
				float fDiffSqr = fDiff*fDiff;
				float fExp = fDiffSqr / (-2.0*pfVariances[iClass]);
				float fDivisor = sqrt( 2.0*PI*pfVariances[iClass] );
				pfLike[iClass] = pfPriors[iClass]*exp( fExp ) / fDivisor;
				fProbCurr += pfLike[iClass];
			}
		}
		//fclose( outfile );

		//printf( "Prob: %f\n", fProbCurr );

		iIteration++;

		//printf( "prev curr: %f %f\n", fDistPrev, fDistCurr );

	}

	//printf( "prev curr: %f %f\n", fDistPrev, fDistCurr );
}

//
int
hfGenerateMILikelihoodGMMImage(
							   int iRow,
							   int iCol,
							   int iWindowRows,
							   int iWindowCols,
							   FEATUREIO &fioImg1,
							   FEATUREIO &fioImg2,
							   FEATUREIO &fioGMM1,
							   FEATUREIO &fioGMM2,
							   FEATUREIO &fioMI
							   )
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = MAX_GMM_CLASSES;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	FEATUREIO fioMarg2;
	fioMarg2.iFeaturesPerVector = 1;
	fioMarg2.z = 1;
	fioMarg2.x = MAX_GMM_CLASSES;
	fioMarg2.y = 1;
	fioMarg2.t = 1;
	fioAllocate( fioMarg2 );
	float *pfMarg2 = fioMarg2.pfVectors;

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 1;
	fioJoint.x = fioMarg1.x;
	fioJoint.y = fioMarg2.x;
	fioJoint.t = 1;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	float pfM1[MAX_GMM_CLASSES];
	float pfM2[MAX_GMM_CLASSES];

	PpImage ppImgTemp;
	fioimgInitReference( ppImgTemp, fioJoint );

	int iRowStart = iRow - iWindowRows/2;
	if( iRowStart < 0 )
	{
		iRowStart = 0;
	}
	else if( iRowStart + iWindowRows > fioImg1.y )
	{
		iRowStart = fioImg1.y - iWindowRows;
	}

	int iColStart = iCol - iWindowCols/2;
	if( iColStart < 0 )
	{
		iColStart = 0;
	}
	else if( iColStart + iWindowCols > fioImg1.x )
	{
		iColStart = fioImg1.x - iWindowCols;
	}

	float *pfWind1 = fioImg1.pfVectors + iRowStart*fioImg1.x + iColStart;
	DIAG_GMM &dgmm1 = ((DIAG_GMM*)fioGMM1.pfVectors)[iRow*fioGMM1.x + iCol];

	for( int r = 0; r < fioImg2.y; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fioImg2.y )
		{
			iRowStart = fioImg2.y - iWindowRows;
		}

		printf( "Row: %d\n", r );
		for( int c = 0; c < fioImg2.x; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fioImg2.x )
			{
				iColStart = fioImg2.x - iWindowCols;
			}

			float *pfWind2 = fioImg2.pfVectors + iRowStart*fioImg2.x + iColStart;
			DIAG_GMM &dgmm2 = ((DIAG_GMM*)fioGMM2.pfVectors)[r*fioGMM2.x + c];

			memset( pfJoint, 0, sizeof(float)*fioJoint.x*fioJoint.y );
			memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );
			memset( pfMarg2, 0, sizeof(float)*fioMarg2.x*fioMarg2.y );

			// Generate joint probability table

			int rr, cc; // Image window indicies
			int g1, g2;	// Marginal density indicies

			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					float fPix1 = pfWind1[rr*fioImg1.x + cc];
					float fPix2 = pfWind2[rr*fioImg2.x + cc];

					// Generate marginal probabilities of each class

					hfGmmProb( fPix1, pfM1, dgmm1 );
					hfGmmProb( fPix2, pfM2, dgmm2 );

					// Multiply marginals, add to joint

					for( g1 = 0; g1 < dgmm1.iClasses; g1++ )
					{
						for( g2 = 0; g2 < dgmm2.iClasses; g2++ )
						{
							pfJoint[g1*(dgmm1.iClasses) + g2] += pfM1[g1]*pfM2[g2];
						}
					}

					for( g1 = 0; g1 < dgmm1.iClasses; g1++ )
					{
						pfMarg1[g1] += pfM1[g1];
					}
					for( g2 = 0; g2 < dgmm2.iClasses; g2++ )
					{
						pfMarg2[g2] += pfM1[g2];
					}
				}
			}

			// Normalize distributions

			float fJointSum = 0;
			float fMargSum1 = 0;
			float fMargSum2 = 0;
			{
				for( g1 = 0; g1 < dgmm1.iClasses; g1++ )
				{
					for( g2 = 0; g2 < dgmm2.iClasses; g2++ )
					{
						fJointSum += pfJoint[g1*(dgmm1.iClasses) + g2];
					}
				}
				
				for( g1 = 0; g1 < dgmm1.iClasses; g1++ )
				{
					fMargSum1 += pfMarg1[g1];
				}
				for( g2 = 0; g2 < dgmm2.iClasses; g2++ )
				{
					fMargSum2 += pfMarg2[g2];
				}
			}

			// Calculate joint entropy

			float fMarg1Entropy = 0;
			for( g1 = 0; g1 < dgmm1.iClasses; g1++ )
			{
				float fProb = pfMarg1[g1] / fMargSum1;
				if( fProb > 0 )
				{
					fMarg1Entropy += (float)(fProb*log(fProb));
				}
			}
			//fMarg1Entropy /= -log( dgmm1.iClasses );

			float fMarg2Entropy = 0;
			for( g2 = 0; g2 < dgmm2.iClasses; g2++ )
			{
				float fProb = pfMarg2[g2] / fMargSum2;
				if( fProb > 0 )
				{
					fMarg2Entropy += (float)(fProb*log(fProb));
				}
			}
			//fMarg2Entropy /= -log( dgmm2.iClasses );

			float fJointEntropy = 0;
			for( g1 = 0; g1 < dgmm1.iClasses; g1++ )
			{
				for( g2 = 0; g2 < dgmm2.iClasses; g2++ )
				{
					float fProb = pfJoint[g1*(dgmm1.iClasses) + g2] / fJointSum;
					if( fProb > 0 )
					{
						fJointEntropy += (float)(fProb*log(fProb));
					}
				}
			}
			//fJointEntropy /= -log( dgmm1.iClasses*dgmm2.iClasses );

			fioMI.pfVectors[r*fioMI.x + c] = -(fMarg1Entropy + fMarg2Entropy - fJointEntropy);
			//fioMI.pfVectors[r*fioMI.x + c] = -fJointEntropy;

//			output_float( ppImgTemp, "joint_out.pgm" );
		}
	}

	fioDelete( fioMarg1 );
	fioDelete( fioMarg2 );
	fioDelete( fioJoint );

	return 1;
}

int
hfGenerateHistogramGMMsImageFloat(
							FEATUREIO &fioImg,
							FEATUREIO &fioGMMs,
							int iWindowRows,
							int iWindowCols,
							int iClasses
							)
{
	float *pfIntensities = new float[iWindowRows*iWindowCols];
	float *pfTempData = new float[iWindowRows*iWindowCols*MAX_GMM_CLASSES];
	assert( pfIntensities );
	assert( pfTempData );
	if( !pfTempData || !pfIntensities )
	{
		return 0;
	}

	for( int r = 0; r < fioImg.y; r++ )
	{
		printf( "quantize row %d\n", r );
		int iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fioImg.y )
		{
			iRowStart = fioImg.y - iWindowRows;
		}

		for( int c = 0; c < fioImg.x; c++ )
		{			
			int iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fioImg.x )
			{
				iColStart = fioImg.x - iWindowCols;
			}

			DIAG_GMM &dgmm = ((DIAG_GMM*)fioGMMs.pfVectors)[r*fioGMMs.x + c];
			dgmm.iClasses = iClasses;

			hfGenerateHistogramRangesGMMVectorFloat(
				fioImg, iRowStart, iColStart,
				iWindowRows, iWindowCols, dgmm,
				pfIntensities, pfTempData
			);
		}
	}

	delete [] pfTempData;
	delete [] pfIntensities;

	return 1;
}

#endif // #ifdef CHANGE_THIS_GMM_CODE

//
// hfGenerateHistogramRangesGaussianNormalizedVectorFloat()
//
// Normalizes intensities to a Gaussian distribution before
// quantizing.
//
void
hfGenerateHistogramRangesGaussianNormalizedVectorFloat(
						   FEATUREIO &fioImg,
						   int iRowStart,
						   int iColStart,
						   int iRows,
						   int iCols,
						   int iRanges,
						   float *pfRanges,
						   float *pfIntensities
						   )
{
	int r, c;
	
	float fSum = 0;
	float fSumSqr = 0;
	FILE *outfile = fopen( "intensities.txt", "wt" );
	for( r = 0; r < iRows; r++ )
	{
		float *pfRow = fioImg.pfVectors + (iRowStart + r)*fioImg.x;
		for( int c = 0; c < iCols; c++ )
		{
			float fValue = pfRow[iColStart+c];
			fSum	+= fValue;
			fSumSqr	+= fValue*fValue;
			fprintf( outfile, "%f\n", fValue );
		}
	}
	fclose( outfile );

	float fMean = fSum / (float)(iRows*iCols);
	float fVariance = (fSumSqr - fMean*fSum) / (float)(iRows*iCols - 1);
	float fStdev = sqrt( fVariance );
	float fStdevDiv = 1.0 / fStdev;

	// Normalize to zero mean an unit variance
	float fMin = fioImg.pfVectors[iRowStart*fioImg.x + iColStart];
	float fMax = fioImg.pfVectors[iRowStart*fioImg.x + iColStart];
	for( r = 0; r < iRows; r++ )
	{
		float *pfRow = fioImg.pfVectors + (iRowStart + r)*fioImg.x;
		for( int c = 0; c < iCols; c++ )
		{
			float fValue = pfRow[iColStart+c];
			pfIntensities[r*iCols + c] = (fValue - fMean)*fStdevDiv;
			if( fValue > fMax )
			{
				fMax = fValue;
			}
			if( fValue < fMin )
			{
				fMin = fValue;
			}
		}
	}

	float fBinSize = (fMax - fMin) / (float)(iRanges + 1);

	pfRanges[0] = fBinSize + fMin;
	for( r = 1; r < iRanges; r++ )
	{
		pfRanges[r] = fBinSize + pfRanges[r-1];
	}
}

//
// hfInitHistogramRangesUniformImageFloat()
// 
// Initializes histogram ranges uniformly over global image histogram.
//
int
hfInitHistogramRangesUniformImageFloat(
							FEATUREIO &fioImg,
							FEATUREIO &fioRanges
							)
{
	int r, c;
	float fMin = fioImg.pfVectors[0];
	float fMax = fioImg.pfVectors[0];
	for( r = 0; r < fioImg.y; r++ )
	{
		float *pfRow = fioImg.pfVectors + r*fioImg.x;
		for( int c = 0; c < fioImg.x; c++ )
		{
			float fValue = pfRow[c];
			if( fValue > fMax )
			{
				fMax = fValue;
			}
			if( fValue < fMin )
			{
				fMin = fValue;
			}
		}
	}

	int iRanges = fioRanges.iFeaturesPerVector;

	float fBinSize = (fMax - fMin) / (float)(iRanges + 1);

	for( r = 0; r < fioRanges.y; r++ )
	{
		float *pfRow = fioRanges.pfVectors + r*fioRanges.x*iRanges;
		for( int c = 0; c < fioRanges.x; c++ )
		{
			float *pfRanges = pfRow + c*iRanges;
			pfRanges[0] = fBinSize + fMin;
			for( int i = 1; i < iRanges; i++ )
			{
				pfRanges[i] = fBinSize + pfRanges[i-1];
			}
		}
	}

	return 1;
}

//
// hfInitHistogramRangesUniformImageFloat()
// 
// Initializes histogram ranges.
//
int
hfGenerateIntegralHistogramUniformImage(
							FEATUREIO &fioImg,
							FEATUREIO &fioIntHist
							)
{
	int r, c;
	float fMin = fioImg.pfVectors[0];
	float fMax = fioImg.pfVectors[0];
	for( r = 0; r < fioImg.y; r++ )
	{
		float *pfRow = fioImg.pfVectors + r*fioImg.x;
		for( int c = 0; c < fioImg.x; c++ )
		{
			float fValue = pfRow[c];
			if( fValue > fMax )
			{
				fMax = fValue;
			}
			if( fValue < fMin )
			{
				fMin = fValue;
			}
		}
	}

	int iRanges = fioIntHist.iFeaturesPerVector;

	float fBinSize = (fMax - fMin) / (float)(iRanges + 1);

	// Init first bin


	// Init first row

	for( r = 1; r < fioIntHist.y; r++ )
	{
	}

	// Init image

	for( r = 0; r < fioIntHist.y; r++ )
	{
		float *pfRow = fioIntHist.pfVectors + r*fioIntHist.x*iRanges;
		for( int c = 0; c < fioIntHist.x; c++ )
		{
			float *pfRanges = pfRow + c*iRanges;
			pfRanges[0] = fBinSize + fMin;
			for( int i = 1; i < iRanges; i++ )
			{
				pfRanges[i] = fBinSize + pfRanges[i-1];
			}
		}
	}

	return 1;
}

int
hfGenerateHistogramRangesImageFloat(
							FEATUREIO &fioImg,
							FEATUREIO &fioRanges,
							int iWindowRows,
							int iWindowCols,
							HISTOGRAM_QUANTIZATION_FUNCTION *funcQuantization
							)
{
	int iRanges = fioRanges.iFeaturesPerVector;

	float *pfTempData = new float[iWindowRows*iWindowCols];
	assert( pfTempData );
	if( !pfTempData )
	{
		return 0;
	}

	for( int r = 0; r < fioImg.y; r++ )
	{
		printf( "quantize row %d\n", r );
		int iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fioImg.y )
		{
			iRowStart = fioImg.y - iWindowRows;
		}

		for( int c = 0; c < fioImg.x; c++ )
		{			
			int iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fioImg.x )
			{
				iColStart = fioImg.x - iWindowCols;
			}

			funcQuantization(
				fioImg, iRowStart, iColStart,
				iWindowRows, iWindowCols, iRanges,
				fioRanges.pfVectors + (r*fioRanges.x + c)*fioRanges.iFeaturesPerVector,
				pfTempData
				);
		}
	}

	delete [] pfTempData;

	return 1;
}

//
// hfGenerateHistogram()
//
// Generates a histogram of a set of values, given ranges and bins.
//
void
hfGenerateHistogram(
			   float *pfValues,
			   int iValues,
			   float *pfRanges,
			   int iRanges,
			   float *pfBins
			   )
{
	memset( pfBins, 0, (iRanges+1)*sizeof(float) );
	for( int i = 0; i < iValues; i++ )
	{
		float fValue = pfValues[i];
		int iBin = hfHistogramBin( fValue, pfRanges, iRanges );
		pfBins[ iBin ]++;
	}
}

//
// hfHistogramBin()
//
// Returns the histogram bin index. There are iRanges+1 bins in total,
// since a range marks the border between bins.
//
int
hfHistogramBin(
			   float fValue,
			   float *pfRanges,
			   int iRanges
			   )
{
	if( iRanges == 0 || fValue < pfRanges[0] )
	{
		return 0;
	}
	for( int i = 1; i < iRanges; i++ )
	{
		if( fValue >= pfRanges[i-1] && fValue < pfRanges[i] )
		{
			return i;
		}
	}
	assert( fValue >= pfRanges[iRanges-1] );
	return iRanges;
}

int
hfGenerateJointEntropyLikelihoodImage(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fioImg1,
					   FEATUREIO &fioImg2,
					   FEATUREIO &fioRanges1,
					   FEATUREIO &fioRanges2,
					   FEATUREIO &fioMI
							)
{
	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 1;
	fioJoint.x = fioRanges1.iFeaturesPerVector + 1;
	fioJoint.y = fioRanges2.iFeaturesPerVector + 1;
	fioJoint.t = 1;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;
	PpImage ppImgTemp;
	fioimgInitReference( ppImgTemp, fioJoint );

	int iRowStart = iRow - iWindowRows/2;
	if( iRowStart < 0 )
	{
		iRowStart = 0;
	}
	else if( iRowStart + iWindowRows > fioImg1.y )
	{
		iRowStart = fioImg1.y - iWindowRows;
	}

	int iColStart = iCol - iWindowCols/2;
	if( iColStart < 0 )
	{
		iColStart = 0;
	}
	else if( iColStart + iWindowCols > fioImg1.x )
	{
		iColStart = fioImg1.x - iWindowCols;
	}

	float *pfWind1 = fioImg1.pfVectors + iRowStart*fioImg1.x + iColStart;
	float *pfRanges1 = fioRanges1.pfVectors + (iRow*fioRanges1.x + iCol)*fioRanges1.iFeaturesPerVector;

	for( int r = 0; r < fioImg2.y; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fioImg2.y )
		{
			iRowStart = fioImg2.y - iWindowRows;
		}

		printf( "Row: %d\n", r );
		for( int c = 0; c < fioImg2.x; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fioImg2.x )
			{
				iColStart = fioImg2.x - iWindowCols;
			}

			float *pfWind2 = fioImg2.pfVectors + iRowStart*fioImg2.x + iColStart;
			float *pfRanges2 = fioRanges2.pfVectors + (r*fioRanges2.x + c)*fioRanges2.iFeaturesPerVector;

			memset( pfJoint, 0, sizeof(float)*(fioRanges1.iFeaturesPerVector+1)*(fioRanges2.iFeaturesPerVector+1) );

			// Generate joint probability table

			int rr, cc;
			int iJointSum = 0;

			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					float fPix1 = pfWind1[rr*fioImg1.x + cc];
					float fPix2 = pfWind2[rr*fioImg2.x + cc];

					int iIndex1 = hfHistogramBin( fPix1, pfRanges1, fioRanges1.iFeaturesPerVector );
					int iIndex2 = hfHistogramBin( fPix2, pfRanges2, fioRanges2.iFeaturesPerVector );

					pfJoint[iIndex1*(fioRanges1.iFeaturesPerVector+1) + iIndex2]++;
					iJointSum++;
				}
			}

			// Calculate joint entropy

			float fJointEntropy = 0;

			for( rr = 0; rr < (fioRanges1.iFeaturesPerVector+1); rr++ )
			{
				for( cc = 0; cc < (fioRanges2.iFeaturesPerVector+1); cc++ )
				{
					float fProb =
						pfJoint[rr*(fioRanges2.iFeaturesPerVector+1) + cc] / (float)iJointSum;
					if( fProb > 0 )
					{
						fJointEntropy += (float)(fProb*log(fProb));
					}
				}
			}

			fioMI.pfVectors[r*fioMI.x + c] = fJointEntropy / log( (float)((fioRanges1.iFeaturesPerVector+1)*(fioRanges2.iFeaturesPerVector+1)) );

//			output_float( ppImgTemp, "joint_out.pgm" );
		}
	}

	fioDelete( fioJoint );

	return 1;
}

int
hfGenerateMILikelihoodImage(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fioImg1,
					   FEATUREIO &fioImg2,
					   FEATUREIO &fioRanges1,
					   FEATUREIO &fioRanges2,
					   FEATUREIO &fioMI,
					   int iInitNoiseLevel,	// The initial uncertainty added to
											// each entry of the joint.
					   int iSubStartRow,
					   int iSubStartCol,
					   int iSubStartRows,
					   int iSubStartCols
							)
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fioRanges1.iFeaturesPerVector + 1;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	FEATUREIO fioMarg2;
	fioMarg2.iFeaturesPerVector = 1;
	fioMarg2.z = 1;
	fioMarg2.x = fioRanges2.iFeaturesPerVector + 1;
	fioMarg2.y = 1;
	fioMarg2.t = 1;
	fioAllocate( fioMarg2 );
	float *pfMarg2 = fioMarg2.pfVectors;

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 1;
	fioJoint.x = fioMarg1.x;
	fioJoint.y = fioMarg2.x;
	fioJoint.t = 1;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	PpImage ppImgTemp;
	fioimgInitReference( ppImgTemp, fioJoint );

	PpImage ppImg1;
	//ppImg1.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );
	PpImage ppImg2;
	//ppImg2.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );

	int iRowStart = iRow - iWindowRows/2;
	if( iRowStart < 0 )
	{
		iRowStart = 0;
	}
	else if( iRowStart + iWindowRows > fioImg1.y )
	{
		iRowStart = fioImg1.y - iWindowRows;
	}

	int iColStart = iCol - iWindowCols/2;
	if( iColStart < 0 )
	{
		iColStart = 0;
	}
	else if( iColStart + iWindowCols > fioImg1.x )
	{
		iColStart = fioImg1.x - iWindowCols;
	}

	float *pfWind1 = fioImg1.pfVectors + iRowStart*fioImg1.x + iColStart;
	float *pfRanges1 = fioRanges1.pfVectors + (iRow*fioRanges1.x + iCol)*fioRanges1.iFeaturesPerVector;

	iSubStartRow  = ( iSubStartRow  < 0 ? 0         : iSubStartRow  );
	iSubStartCol  = ( iSubStartCol  < 0 ? 0         : iSubStartCol  );
	iSubStartRows = ( iSubStartRow + iSubStartRows > fioImg2.y ? fioImg2.y : iSubStartRow + iSubStartRows );
	iSubStartCols = ( iSubStartCol + iSubStartCols > fioImg2.x ? fioImg2.x : iSubStartCol + iSubStartCols );

	for( int r = iSubStartRow; r < iSubStartRows; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fioImg2.y )
		{
			iRowStart = fioImg2.y - iWindowRows;
		}

		printf( "Row: %d\n", r );
		for( int c = iSubStartCol; c < iSubStartCols; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fioImg2.x )
			{
				iColStart = fioImg2.x - iWindowCols;
			}

			float *pfWind2 = fioImg2.pfVectors + iRowStart*fioImg2.x + iColStart;
			float *pfRanges2 = fioRanges2.pfVectors + (r*fioRanges2.x + c)*fioRanges2.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			memset( pfJoint, 0, sizeof(float)*fioJoint.x*fioJoint.y );
			memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );
			memset( pfMarg2, 0, sizeof(float)*fioMarg2.x*fioMarg2.y );

			int rr, cc;

			// Add noise
			for( rr = 0; rr < fioMarg1.x; rr++ )
			{
				pfMarg1[rr] = iInitNoiseLevel*fioMarg1.x;
			}
			for( rr = 0; rr < fioMarg2.x; rr++ )
			{
				pfMarg2[rr] = iInitNoiseLevel*fioMarg2.x;
			}
			for( rr = 0; rr < (fioRanges1.iFeaturesPerVector+1); rr++ )
			{
				for( cc = 0; cc < (fioRanges2.iFeaturesPerVector+1); cc++ )
				{
					pfJoint[rr*(fioRanges2.iFeaturesPerVector+1) + cc]
						= iInitNoiseLevel;
				}
			}
			
			int iJointSum = fioMarg1.x*fioMarg2.x*iInitNoiseLevel;

			// Generate joint probability table

			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					float fPix1 = pfWind1[rr*fioImg1.x + cc];
					float fPix2 = pfWind2[rr*fioImg2.x + cc];

					int iIndex1 = hfHistogramBin( fPix1, pfRanges1, fioRanges1.iFeaturesPerVector );
					int iIndex2 = hfHistogramBin( fPix2, pfRanges2, fioRanges2.iFeaturesPerVector );

					//((float*)(ppImg1.ImageRow(rr)))[cc] = iIndex1;
					//((float*)(ppImg2.ImageRow(rr)))[cc] = iIndex2;

					pfJoint[iIndex1*(fioRanges1.iFeaturesPerVector+1) + iIndex2]++;
					pfMarg1[iIndex1]++;
					pfMarg2[iIndex2]++;

					iJointSum++;
				}
			}

			//output_float( ppImg1, "img1.pgm" );
			//output_float( ppImg2, "img2.pgm" );

			// Calculate entropies

			float fMarg1Entropy = 0;
			for( rr = 0; rr < fioMarg1.x; rr++ )
			{
				if( pfMarg1[rr] > 0 )
				{
					float fProb = pfMarg1[rr] / (float)iJointSum;
					fMarg1Entropy -= (float)(fProb*log(fProb));
				}
			}
			//fMarg1Entropy /= -log( fioMarg1.x );

			float fMarg2Entropy = 0;
			for( rr = 0; rr < fioMarg2.x; rr++ )
			{
				if( pfMarg2[rr] > 0 )
				{
					float fProb = pfMarg2[rr] / (float)iJointSum;
					fMarg2Entropy -= (float)(fProb*log(fProb));
				}
			}
			//fMarg2Entropy /= -log( fioMarg2.x );

			float fJointEntropy = 0;
			for( rr = 0; rr < (fioRanges1.iFeaturesPerVector+1); rr++ )
			{
				for( cc = 0; cc < (fioRanges2.iFeaturesPerVector+1); cc++ )
				{
					if( pfJoint[rr*(fioRanges2.iFeaturesPerVector+1) + cc] > 0 )
					{
						float fProb =
						pfJoint[rr*(fioRanges2.iFeaturesPerVector+1) + cc] / (float)iJointSum;
						fJointEntropy -= (float)(fProb*log(fProb));
					}
				}
			}

			//fJointEntropy /= -log( fioMarg1.x*fioMarg2.x );

			fioMI.pfVectors[r*fioMI.x + c] = fMarg1Entropy + fMarg2Entropy - fJointEntropy;
			// float fMinEnt = fMarg1Entropy < fMarg2Entropy ? fMarg1Entropy : fMarg2Entropy;
			//fioMI.pfVectors[r*fioMI.x + c] = fJointEntropy - fMinEnt;
			//fioMI.pfVectors[r*fioMI.x + c] = fJointEntropy;

//			output_float( ppImgTemp, "joint_out.pgm" );
		}
	}

	fioDelete( fioMarg1 );
	fioDelete( fioMarg2 );
	fioDelete( fioJoint );

	return 1;
}




int
hfGenerateHistogramEntropyImage(
					   FEATUREIO &fioImg2,
					   FEATUREIO &fioRanges2,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fioEntropy
//					   ,FEATUREIO &fioHistWeights
							)
{
	FEATUREIO fioMarg2;
	fioMarg2.iFeaturesPerVector = 1;
	fioMarg2.z = 1;
	fioMarg2.x = fioRanges2.iFeaturesPerVector + 1;
	fioMarg2.y = 1;
	fioMarg2.t = 1;
	fioAllocate( fioMarg2 );
	float *pfMarg2 = fioMarg2.pfVectors;

	for( int r = 0; r < fioImg2.y; r++ )
	{
		int iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fioImg2.y )
		{
			iRowStart = fioImg2.y - iWindowRows;
		}

		printf( "Row: %d\n", r );
		for( int c = 0; c < fioImg2.x; c++ )
		{			
			int iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fioImg2.x )
			{
				iColStart = fioImg2.x - iWindowCols;
			}

			float *pfWind2 = fioImg2.pfVectors + iRowStart*fioImg2.x + iColStart;
			float *pfRanges2 = fioRanges2.pfVectors + (r*fioRanges2.x + c)*fioRanges2.iFeaturesPerVector;
			//float *pfWeights = fioHistWeights.pfVectors + iRowStart*fioHistWeights.x + iColStart;

			memset( pfMarg2, 0, sizeof(float)*fioMarg2.x*fioMarg2.y );

			// Generate joint probability table

			int rr, cc;
			float fJointSum = 0.0f;

			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					float fPix2 = pfWind2[rr*fioImg2.x + cc];

					int iIndex2 = hfHistogramBin( fPix2, pfRanges2, fioRanges2.iFeaturesPerVector );

					//float fWeight = pfWeights[rr*fioHistWeights.x + cc];
					float fWeight = 1;

					pfMarg2[iIndex2] += fWeight;

					fJointSum += fWeight;
				}
			}

			if( fJointSum > 0.0f )
			{

				//FILE *outfile = fopen( "hist.txt", "wt" );
				float fMarg2Entropy = 0;
				for( rr = 0; rr < fioMarg2.x; rr++ )
				{
					//fprintf( outfile, "%f\n", pfMarg2[rr] );
					float fProb = pfMarg2[rr] / fJointSum;
					if( fProb > 0 )
					{
						fMarg2Entropy += (float)(fProb*log(fProb));
					}
				}
				//fclose( outfile );
				fioEntropy.pfVectors[r*fioEntropy.x + c] = -fMarg2Entropy;
			}
			else
			{
				fioEntropy.pfVectors[r*fioEntropy.x + c] = 0;
			}
		}
	}

	fioDelete( fioMarg2 );

	return 1;
}

int
hfGenerateOrientationIntensityEntropyImage(
					   FEATUREIO &fioImgPix,
					   FEATUREIO &fioImgOri,
					   FEATUREIO &fioRangesPix,
					   FEATUREIO &fioRangesOri,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fioEntropy,
					   FEATUREIO &fioHistWeights
							)
{
	// Allocate a 2D distribution: p(Intensity,Orientation)

	FEATUREIO fioDist;
	fioDist.iFeaturesPerVector = 1;
	fioDist.z = 1;
	fioDist.x = fioRangesPix.iFeaturesPerVector + 1;
	fioDist.y = fioRangesOri.iFeaturesPerVector + 1;
	fioDist.t = 1;
	fioAllocate( fioDist );
	float *pfDist = fioDist.pfVectors;

	for( int r = 0; r < fioImgPix.y; r++ )
	{
		int iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fioImgPix.y )
		{
			iRowStart = fioImgPix.y - iWindowRows;
		}

		printf( "Row: %d\n", r );
		for( int c = 0; c < fioImgPix.x; c++ )
		{			
			int iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fioImgPix.x )
			{
				iColStart = fioImgPix.x - iWindowCols;
			}

			float *pfImgWndPix = fioImgPix.pfVectors + iRowStart*fioImgPix.x + iColStart;
			float *pfRangesPix = fioRangesPix.pfVectors + (r*fioRangesPix.x + c)*fioRangesPix.iFeaturesPerVector;
			
			float *pfImgWndOri = fioImgOri.pfVectors + iRowStart*fioImgOri.x + iColStart;
			float *pfRangesOri = fioRangesOri.pfVectors + (r*fioRangesOri.x + c)*fioRangesOri.iFeaturesPerVector;

			float *pfWeights = fioHistWeights.pfVectors + iRowStart*fioHistWeights.x + iColStart;

			memset( pfDist, 0, sizeof(float)*fioDist.x*fioDist.y );

			// Generate joint probability table

			int rr, cc;
			float fJointSum = 0.0f;

			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					float fPix = pfImgWndPix[rr*fioImgPix.x + cc];
					int iIndexPix = hfHistogramBin( fPix, pfRangesPix, fioRangesPix.iFeaturesPerVector );
					
					float fOri = pfImgWndOri[rr*fioImgOri.x + cc];
					int iIndexOri = hfHistogramBin( fOri, pfRangesOri, fioRangesOri.iFeaturesPerVector );

					//assert( iIndexOri == 0 );

					//float fWeight = pfWeights[rr*fioHistWeights.x + cc];
					float fWeight = 1;

					pfDist[iIndexPix*fioDist.y + iIndexOri] += fWeight;
					fJointSum += fWeight;
				}
			}

			if( fJointSum > 0.0f )
			{
				//FILE *outfile = fopen( "hist.txt", "wt" );
				float fEntropy = 0;
				for( rr = 0; rr < fioDist.x*fioDist.y*fioDist.z; rr++ )
				{
					//fprintf( outfile, "%f\n", pfDist[rr] );
					float fProb = pfDist[rr] / fJointSum;
					if( fProb > 0 )
					{
						fEntropy -= (float)(fProb*log(fProb));
					}
				}
				//fclose( outfile );
				fioEntropy.pfVectors[r*fioEntropy.x + c] = fEntropy;
			}
			else
			{
				fioEntropy.pfVectors[r*fioEntropy.x + c] = 0;//log( fioDist.x*fioDist.y*fioDist.z );
			}
		}
	}

	fioDelete( fioDist );

	return 1;
}





//
//
//
//           fioImg1                     fioImg2
//      _____________________      _____________________
//     |   _____             |    |                     |
//     |  |     |            |    |                     |
//     |  |  *1 | iRowSize   |    |                     |
//     |  |     |            |    |                     |
//     |   -----             |    |   __________        |
//     |  iColSize           |    |  |          |       |
//     |                     |    |  |          |       |
//     |                     |    |  |    *2    | iRows |
//     |                     |    |  |          |       |
//     |                     |    |  |          |       |
//     |                     |    |   ----------        |
//     |                     |    |     iCols           |
//     |                     |    |                     |
//     |                     |    |                     |
//     ----------------------      ---------------------
//  
//          (iRowOffset, iColOffset) =  (*2) - (*1)
//  
//
//
// p(match) = exp( -[MaxMI - MI]*fMIDiv ) = exp( [MI - MaxMI]*fMIDiv )
//
//
//

float
hfGenerate_MI_EOL(
					   const int &iRowStart,	// Domain start
					   const int &iColStart,
					   const int &iRows,		// Domain extents
					   const int &iCols,

					   FEATUREIO &fioTemplate1,	// Template from fioImg1
					   FEATUREIO &fioImg2,		// Domain: fioImg2

					   FEATUREIO &fioRanges1,	// Template histogram ranges
					   FEATUREIO &fioRanges2,	// Domain histogram ranges

					   FEATUREIO &fioMarg1,			// Marginal for source template
					   FEATUREIO &fioMarg2,			// Marginal for domain template
					   FEATUREIO &fioJoint,			// Joint histogram for source, domain

					   const float &fMaxMI,		// Maximum mutual information
					   const float &fMIDiv,		// Varriance term for MI energy
					   const float *pfLogLookup	// Table lookup for function (i)log(i)
							)
{
	int &iWindowRows = fioTemplate1.y;
	int &iWindowCols = fioTemplate1.x;

	float *pfMarg1 = fioMarg1.pfVectors;
	float *pfMarg2 = fioMarg2.pfVectors;
	float *pfJoint = fioJoint.pfVectors;

	float *pfWind1 = fioTemplate1.pfVectors;
	float *pfRanges1 = fioRanges1.pfVectors;

	float fEntropySum = 0.0f;
	float fLikelihoodSum = 0.0f;
	float fEntropy = 0.0;

	//PpImage ppImgLike;
	//ppImgLike.Initialize( fioImg2.y, fioImg2.x, fioImg2.x*sizeof(float), 8*sizeof(float) );
	//memset( ppImgLike.ImageRow(0), 0, ppImgLike.Rows()*ppImgLike.Cols()*sizeof(float) );

	//PpImage ppImg1;
	//ppImg1.Initialize( fioTemplate1.y, fioTemplate1.x, fioTemplate1.x*sizeof(float), 8*sizeof(float) );
	//PpImage ppImg2;
	//ppImg2.Initialize( fioTemplate1.y, fioTemplate1.x, fioTemplate1.x*sizeof(float), 8*sizeof(float) );

	//
	// Assert that the template evaluated at domain positions will never exceed 
	// the domain image 2 size.
	//

	assert( iColStart - iWindowCols/2 >= 0 );
	assert( iColStart + iCols + iWindowCols/2 < fioImg2.x );

	assert( iRowStart - iWindowRows/2 >= 0 );
	assert( iRowStart + iRows + iWindowRows/2 < fioImg2.y );

	//
	// Loop over positions in image2 domain.
	//

	int r, c;

	float fNDiv = 1.0f/(float)(iWindowRows*iWindowCols);
	float flogN = (float)log((float)(iWindowRows*iWindowCols));

	for( r = iRowStart; r < iRowStart + iRows; r++ )
	{
		int r_off = r - iWindowRows/2;
		// printf( "Row: %d\n", r );
		for( int c = iColStart; c < iColStart + iCols; c++ )
		{			
			int c_off = c - iWindowCols/2;

			float *pfWind2 = fioImg2.pfVectors + r_off*fioImg2.x + c_off;
			float *pfRanges2 = fioRanges2.pfVectors + (r*fioRanges2.x + c)*fioRanges2.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			memset( pfJoint, 0, sizeof(float)*fioJoint.x*fioJoint.y );
			memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );
			memset( pfMarg2, 0, sizeof(float)*fioMarg2.x*fioMarg2.y );

			// Generate joint probability table

			int rr, cc;
			int iJointSum = 0;

			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					float fPix1 = pfWind1[rr*fioTemplate1.x + cc];
					float fPix2 = pfWind2[rr*fioImg2.x      + cc];

					int iIndex1 = hfHistogramBin( fPix1, pfRanges1, fioRanges1.iFeaturesPerVector );
					int iIndex2 = hfHistogramBin( fPix2, pfRanges2, fioRanges2.iFeaturesPerVector );

					//((float*)(ppImg1.ImageRow(rr)))[cc] = iIndex1;
					//((float*)(ppImg2.ImageRow(rr)))[cc] = iIndex2;

					pfJoint[iIndex1*(fioRanges1.iFeaturesPerVector+1) + iIndex2]++;
					pfMarg1[iIndex1]++;
					pfMarg2[iIndex2]++;

					iJointSum++;
				}
			}

			//output_float( ppImg1, "img1.pgm" );
			//output_float( ppImg2, "img2.pgm" );

			// Calculate entropies

			float fMarg1Entropy = 0;
			for( rr = 0; rr < fioMarg1.x; rr++ )
			{
				if( pfMarg1[rr] > 0 )
				{
					fMarg1Entropy += pfMarg1[rr]*log(pfMarg1[rr]);
				}
			}

			float fMarg2Entropy = 0;
			for( rr = 0; rr < fioMarg2.x; rr++ )
			{
				if( pfMarg2[rr] > 0 )
				{
					fMarg2Entropy += pfMarg2[rr]*log(pfMarg2[rr]);
				}
			}

			float fJointEntropy = 0;
			for( rr = 0; rr < (fioRanges1.iFeaturesPerVector+1); rr++ )
			{
				for( cc = 0; cc < (fioRanges2.iFeaturesPerVector+1); cc++ )
				{
					if( pfJoint[rr*(fioRanges2.iFeaturesPerVector+1) + cc] > 0 )
					{
						fJointEntropy += pfJoint[rr*(fioRanges2.iFeaturesPerVector+1) + cc]*log(pfJoint[rr*(fioRanges2.iFeaturesPerVector+1) + cc]);
					}
				}
			}

			float fMI = flogN + (-fMarg1Entropy-fMarg2Entropy+fJointEntropy)*fNDiv;
			float fResult = (fMaxMI - fMI)*fMIDiv;
			assert( fResult > 0.0f );
			float fExp = exp( -fResult );
			fLikelihoodSum += fExp;
			fEntropySum    += fExp*fResult;

			//((float*)(ppImgLike.ImageRow(r)))[c] = fExp;
		}
	}

	//static int iCount = 0;
	//char pcFileName[300];
	//sprintf( pcFileName, "like_%3.3d.pgm", iCount );
	//output_float( ppImgLike, pcFileName );

	// Calculate the entropy straighforward, see if we get the same result.

	//float fEntropyTest = 0;
	//for( r = 0; r < ppImgLike.Rows(); r++ )
	//{
	//	for( int c = 0; c < ppImgLike.Cols(); c++ )
	//	{
	//		((float*)(ppImgLike.ImageRow(r)))[c] /= fLikelihoodSum;
	//		float fProb = ((float*)(ppImgLike.ImageRow(r)))[c];
	//		if( fProb > 0.0f )
	//		{
	//			fEntropyTest -= fProb*log(fProb);
	//		}
	//	}
	//}

	fEntropy = log( fLikelihoodSum ) + fEntropySum / fLikelihoodSum;

	return fEntropy;
}

//
// Use lookup table to calculate logs - is this faster?
//
//
float
hfGenerate_MI_EOL_lookup(
					   const int &iRowStart,	// Domain start
					   const int &iColStart,
					   const int &iRows,		// Domain extents
					   const int &iCols,

					   FEATUREIO &fioTemplate1,	// Template from fioImg1
					   FEATUREIO &fioImg2,		// Domain: fioImg2

					   FEATUREIO &fioRanges1,	// Template histogram ranges
					   FEATUREIO &fioRanges2,	// Domain histogram ranges

					   FEATUREIO &fioMarg1,			// Marginal for source template
					   FEATUREIO &fioMarg2,			// Marginal for domain template
					   FEATUREIO &fioJoint,			// Joint histogram for source, domain

					   const float &fMaxMI,		// Maximum mutual information
					   const float &fMIDiv,		// Varriance term for MI energy
					   const float *pfLogLookup	// Table lookup for function (i)log(i)
							)
{
	int &iWindowRows = fioTemplate1.y;
	int &iWindowCols = fioTemplate1.x;

	float *pfMarg1 = fioMarg1.pfVectors;
	float *pfMarg2 = fioMarg2.pfVectors;
	float *pfJoint = fioJoint.pfVectors;

	float *pfWind1 = fioTemplate1.pfVectors;
	float *pfRanges1 = fioRanges1.pfVectors;

	float fEntropySum = 0.0f;
	float fLikelihoodSum = 0.0f;
	float fEntropy = 0.0;

	//PpImage ppImgLike;
	//ppImgLike.Initialize( fioImg2.y, fioImg2.x, fioImg2.x*sizeof(float), 8*sizeof(float) );
	//memset( ppImgLike.ImageRow(0), 0, ppImgLike.Rows()*ppImgLike.Cols()*sizeof(float) );

	//PpImage ppImg1;
	//ppImg1.Initialize( fioTemplate1.y, fioTemplate1.x, fioTemplate1.x*sizeof(float), 8*sizeof(float) );
	//PpImage ppImg2;
	//ppImg2.Initialize( fioTemplate1.y, fioTemplate1.x, fioTemplate1.x*sizeof(float), 8*sizeof(float) );

	//
	// Assert that the template evaluated at domain positions will never exceed 
	// the domain image 2 size.
	//

	assert( iColStart - iWindowCols/2 >= 0 );
	assert( iColStart + iCols + iWindowCols/2 < fioImg2.x );

	assert( iRowStart - iWindowRows/2 >= 0 );
	assert( iRowStart + iRows + iWindowRows/2 < fioImg2.y );

	//
	// Loop over positions in image2 domain.
	//

	int r, c;

	float fNDiv = 1.0f/(float)(iWindowRows*iWindowCols);
	float flogN = (float)log((float)(iWindowRows*iWindowCols));

	for( r = iRowStart; r < iRowStart + iRows; r++ )
	{
		int r_off = r - iWindowRows/2;
		// printf( "Row: %d\n", r );
		for( int c = iColStart; c < iColStart + iCols; c++ )
		{			
			int c_off = c - iWindowCols/2;

			float *pfWind2 = fioImg2.pfVectors + r_off*fioImg2.x + c_off;
			float *pfRanges2 = fioRanges2.pfVectors + (r*fioRanges2.x + c)*fioRanges2.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			memset( pfJoint, 0, sizeof(float)*fioJoint.x*fioJoint.y );
			memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );
			memset( pfMarg2, 0, sizeof(float)*fioMarg2.x*fioMarg2.y );

			// Generate joint probability table

			int rr, cc;
			int iJointSum = 0;

			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					float fPix1 = pfWind1[rr*fioTemplate1.x + cc];
					float fPix2 = pfWind2[rr*fioImg2.x      + cc];

					int iIndex1 = hfHistogramBin( fPix1, pfRanges1, fioRanges1.iFeaturesPerVector );
					int iIndex2 = hfHistogramBin( fPix2, pfRanges2, fioRanges2.iFeaturesPerVector );

					//((float*)(ppImg1.ImageRow(rr)))[cc] = iIndex1;
					//((float*)(ppImg2.ImageRow(rr)))[cc] = iIndex2;

					pfJoint[iIndex1*(fioRanges1.iFeaturesPerVector+1) + iIndex2]++;
					pfMarg1[iIndex1]++;
					pfMarg2[iIndex2]++;

					iJointSum++;
				}
			}

			//output_float( ppImg1, "img1.pgm" );
			//output_float( ppImg2, "img2.pgm" );

			// Calculate entropies

			float fMarg1Entropy = 0;
			for( rr = 0; rr < fioMarg1.x; rr++ )
			{
				fMarg1Entropy += pfLogLookup[ (int)pfMarg1[rr] ];
			}

			float fMarg2Entropy = 0;
			for( rr = 0; rr < fioMarg2.x; rr++ )
			{
				fMarg2Entropy += pfLogLookup[ (int)pfMarg2[rr] ];
			}

			float fJointEntropy = 0;
			for( rr = 0; rr < (fioRanges1.iFeaturesPerVector+1); rr++ )
			{
				for( cc = 0; cc < (fioRanges2.iFeaturesPerVector+1); cc++ )
				{
					fJointEntropy += pfLogLookup[ (int)pfJoint[rr*(fioRanges2.iFeaturesPerVector+1) + cc] ];
				}
			}

			float fMI = flogN + (-fMarg1Entropy-fMarg2Entropy+fJointEntropy)*fNDiv;
			float fResult = (fMaxMI - fMI)*fMIDiv;
			assert( fResult > 0.0f );
			float fExp = exp( -fResult );
			fLikelihoodSum += fExp;
			fEntropySum    += fExp*fResult;

			//((float*)(ppImgLike.ImageRow(r)))[c] = fExp;
		}
	}

	//static int iCount = 0;
	//char pcFileName[300];
	//sprintf( pcFileName, "like_%3.3d.pgm", iCount );
	//output_float( ppImgLike, pcFileName );

	// Calculate the entropy straighforward, see if we get the same result.

	//float fEntropyTest = 0;
	//for( r = 0; r < ppImgLike.Rows(); r++ )
	//{
	//	for( int c = 0; c < ppImgLike.Cols(); c++ )
	//	{
	//		((float*)(ppImgLike.ImageRow(r)))[c] /= fLikelihoodSum;
	//		float fProb = ((float*)(ppImgLike.ImageRow(r)))[c];
	//		if( fProb > 0.0f )
	//		{
	//			fEntropyTest -= fProb*log(fProb);
	//		}
	//	}
	//}

	fEntropy = log( fLikelihoodSum ) + fEntropySum / fLikelihoodSum;

	return fEntropy;
}

int
hfCalculate_eol_image_MI(
					   const int &iRowOffset,	// Offset of a pixel in fioImg1 to fioImg2
					   const int &iColOffset,
					   
					   const int &iRows,		// Domain size in fioImg2
					   const int &iCols,		//
					   
					   FEATUREIO &fioImg1,		// Image 1
					   FEATUREIO &fioImg2,		// Image 2

					   FEATUREIO &fioRanges1,	// Histogram ranges at locations in image 1
					   FEATUREIO &fioRanges2,	// Histogram ranges at locations in image 1

					   FEATUREIO &fioEOL,
					   
					   const int &iRowSize, // Size of template window
					   const int &iColSize, 
					   
					   const float &fMIVarrDiv,		// Varriance term for NCC energy

					   FEATUREIO &fioImgMask
						)
{	
	float fEntropyDivisor = 1.0f / log((float)(iRows*iCols));
	
	int iPercentDone = (fioImg1.x*fioImg1.y)/100;

	fioSet( fioEOL, 1.0f );

	// Allocate memory for distributions, template

	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fioRanges1.iFeaturesPerVector + 1;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	FEATUREIO fioMarg2;
	fioMarg2.iFeaturesPerVector = 1;
	fioMarg2.z = 1;
	fioMarg2.x = fioRanges2.iFeaturesPerVector + 1;
	fioMarg2.y = 1;
	fioMarg2.t = 1;
	fioAllocate( fioMarg2 );
	float *pfMarg2 = fioMarg2.pfVectors;

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 1;
	fioJoint.x = fioMarg1.x;
	fioJoint.y = fioMarg2.x;
	fioJoint.t = 1;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	FEATUREIO fioTemplate;
	fioTemplate.iFeaturesPerVector = 1;
	fioTemplate.z = 1;
	fioTemplate.x = iColSize;
	fioTemplate.y = iRowSize;
	fioTemplate.t = 1;
	fioAllocate( fioTemplate );

	FEATUREIO fioTemplateRanges;
	fioTemplateRanges.iFeaturesPerVector = fioRanges1.iFeaturesPerVector;
	fioTemplateRanges.z = 1;
	fioTemplateRanges.x = 1;
	fioTemplateRanges.y = 1;
	fioTemplateRanges.t = 1;
	fioAllocate( fioTemplateRanges );

	// Generate a lookup table for log calculations. We need
	// as many pixels as in the image template.

	float *pfLogLookup = new float[iColSize*iRowSize+1];
	pfLogLookup[0] = 0.0f;
	for( int i = 1; i < iColSize*iRowSize+1; i++ )
	{
		pfLogLookup[i] = (float)(((float)i)*log((float)i));
	}

	PpImage ppImgTemp;

	float fMaxMI = log((float)(fioRanges1.iFeaturesPerVector+1)) < log((float)(fioRanges2.iFeaturesPerVector+1)) ?
		log((float)(fioRanges1.iFeaturesPerVector+1)) : log((float)(fioRanges2.iFeaturesPerVector+1));

	//
	// Consider only template positions in Image1 that fully
	// fit within in the image.
	//
	// Adjust domain in Image2 so that there are exactly
	// iRows*iCols match locations.
	//
	// After this stage, the inner loop entropy calculation does
	// not have to worry about image boundaries.
	//

	float fMaxEnt = 0.0f;
	float fMinEnt = 10000.0;

	for( int y = iRowSize/2; y < fioImg1.y - iRowSize/2; y++ )
	{		
		int iRow2Start = y + iRowOffset - iRows/2;
		if( iRow2Start < iRowSize/2 )
		{
			iRow2Start = iRowSize/2;
		}
		else if( iRow2Start + iRows >= fioImg2.y - iRowSize/2 )
		{
			iRow2Start = fioImg2.y - iRows - iRowSize/2 - 1;
		}

		printf( "row: %2.2d, (max,min): (%f.%f)\n", y, fMaxEnt, fMinEnt );
		
		for( int x = iColSize/2; x < fioImg1.x - iColSize/2; x++ )
		{
			if( fioImgMask.pfVectors )
			{
				if( fioGetPixel( fioImgMask, x, y, 0 ) != 255 )
				{
					// 
					continue;
				}
			}

			int iCol2Start = x + iColOffset - iCols/2;
			if( iCol2Start < iColSize/2 )
			{
				iCol2Start = iColSize/2;
			}
			else if( iCol2Start + iCols >= fioImg2.x - iColSize/2 )
			{
				iCol2Start = fioImg2.x - iCols - iColSize/2 - 1;
			}

			// Generate template

			for( int yy = 0; yy < iRowSize; yy++ )
			{
				memcpy(
					fioTemplate.pfVectors + yy*fioTemplate.x,
					fioImg1.pfVectors + (yy+y-iRowSize/2)*fioImg1.x + (x - iColSize/2),
					iColSize*sizeof(float)
					);
			}

			//fioimgInitReference( ppImgTemp, fioTemplate );
			//output_float( ppImgTemp, "template.pgm" );

			// Generate template ranges

			memcpy(
				fioTemplateRanges.pfVectors,
				fioRanges1.pfVectors + (y*fioRanges1.x + x)*fioRanges1.iFeaturesPerVector,
				fioRanges1.iFeaturesPerVector*sizeof(float)
				);

			// Here we calculated the entropy of the MI calculation

			float fEntropy = 
				hfGenerate_MI_EOL_lookup(
				//hfGenerate_MI_EOL(
					iRow2Start, iCol2Start,	// Domain starting position in fioImg2
					iRows, iCols,			// Domain size in fioImg2

					fioTemplate,			// Template from fioImg1
					fioImg2,				// Domain: fioImg2

					fioTemplateRanges,		// Template histogram ranges
					fioRanges2,				// Domain histogram ranges
					
					//FEATUREIO &fioMI,

					   fioMarg1,			// Marginal for source template
					   fioMarg2,			// Marginal for domain template
					   fioJoint,			// Joint histogram for source, domain
					   fMaxMI,
					   fMIVarrDiv,		// Varriance term for NCC energy
					   pfLogLookup
							);

			// Normalize
			fEntropy *= fEntropyDivisor;
			
			//if( fEntropy < 0.0 || fEntropy > 1.0 ) { assert( 0 ); }
			fioEOL.pfVectors[y*fioEOL.x + x] = fEntropy;

			if( fEntropy > fMaxEnt )
			{
				fMaxEnt = fEntropy ;
			}
			if( fEntropy < fMinEnt )
			{
				fMinEnt = fEntropy;
			}
		}
	}

	delete [] pfLogLookup;
	fioDelete( fioMarg1 );
	fioDelete( fioMarg2 );
	fioDelete( fioJoint );
	fioDelete( fioTemplate );
	fioDelete( fioTemplateRanges );
	
	return 1;
}




//
// Use lookup table to calculate logs - is this faster?
//
//
float
hfGenerate_MI_EOL_lookup_scale(
					   const int &iRowStart,	// Domain start
					   const int &iColStart,
					   const int &iRows,		// Domain extents
					   const int &iCols,

					   FEATUREIO &fioTemplate1,	// Template from fioImg1
					   FEATUREIO &fioImg2,		// Domain: fioImg2

					   FEATUREIO &fioRanges1,	// Template histogram ranges
					   FEATUREIO &fioRanges2,	// Domain histogram ranges

					   FEATUREIO &fioMarg1,			// Marginal for source template
					   FEATUREIO &fioMarg2,			// Marginal for domain template
					   FEATUREIO &fioJoint,			// Joint histogram for source, domain

					   const float &fMaxMI,		// Maximum mutual information
					   const float &fMIDiv,		// Varriance term for MI energy
					   const float *pfLogLookup	// Table lookup for function (i)log(i)
							)
{
	int &iWindowRows = fioTemplate1.y;
	int &iWindowCols = fioTemplate1.x;

	float *pfMarg1[3];
	float *pfMarg2[3];
	float *pfJoint[3];

	pfMarg1[0] = fioMarg1.pfVectors + fioMarg1.x*fioMarg1.y*0;
	pfMarg1[1] = fioMarg1.pfVectors + fioMarg1.x*fioMarg1.y*1;
	pfMarg1[2] = fioMarg1.pfVectors + fioMarg1.x*fioMarg1.y*2;

	pfMarg2[0] = fioMarg2.pfVectors + fioMarg2.x*fioMarg2.y*0;
	pfMarg2[1] = fioMarg2.pfVectors + fioMarg2.x*fioMarg2.y*1;
	pfMarg2[2] = fioMarg2.pfVectors + fioMarg2.x*fioMarg2.y*2;

	pfJoint[0] = fioJoint.pfVectors + fioJoint.x*fioJoint.y*0;
	pfJoint[1] = fioJoint.pfVectors + fioJoint.x*fioJoint.y*1;
	pfJoint[2] = fioJoint.pfVectors + fioJoint.x*fioJoint.y*2;

	float *pfWind1 = fioTemplate1.pfVectors;
	float *pfRanges1 = fioRanges1.pfVectors;

	float fEntropySum = 0.0f;
	float fLikelihoodSum = 0.0f;
	float fEntropy = 0.0;

	//PpImage ppImgLike;
	//ppImgLike.Initialize( fioImg2.y, fioImg2.x, fioImg2.x*sizeof(float), 8*sizeof(float) );
	//memset( ppImgLike.ImageRow(0), 0, ppImgLike.Rows()*ppImgLike.Cols()*sizeof(float) );

	//PpImage ppImg1;
	//ppImg1.Initialize( fioTemplate1.y, fioTemplate1.x, fioTemplate1.x*sizeof(float), 8*sizeof(float) );
	//PpImage ppImg2;
	//ppImg2.Initialize( fioTemplate1.y, fioTemplate1.x, fioTemplate1.x*sizeof(float), 8*sizeof(float) );

	//
	// Assert that the template evaluated at domain positions will never exceed 
	// the domain image 2 size.
	//

	assert( iColStart - iWindowCols/2 >= 0 );
	assert( iColStart + iCols + iWindowCols/2 < fioImg2.x );

	assert( iRowStart - iWindowRows/2 >= 0 );
	assert( iRowStart + iRows + iWindowRows/2 < fioImg2.y );

	//
	// Loop over positions in image2 domain.
	//

	int r, c;

	float fNDiv[3];
	fNDiv[0] = 1.0f/(float)(15*15);
	fNDiv[1] = 1.0f/(float)(11*11);
	fNDiv[2] = 1.0f/(float)(5*5);

	float flogN[3];
	flogN[0] = (float)log((float)15*15);
	flogN[1] = (float)log((float)11*11);
	flogN[2] = (float)log((float)5*5);

	for( r = iRowStart; r < iRowStart + iRows; r++ )
	{
		int r_off = r - iWindowRows/2;
		// printf( "Row: %d\n", r );
		for( int c = iColStart; c < iColStart + iCols; c++ )
		{			
			int c_off = c - iWindowCols/2;

			float *pfWind2 = fioImg2.pfVectors + r_off*fioImg2.x + c_off;
			float *pfRanges2 = fioRanges2.pfVectors + (r*fioRanges2.x + c)*fioRanges2.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			memset( pfJoint[0], 0, sizeof(float)*fioJoint.x*fioJoint.y*fioJoint.z );
			memset( pfMarg1[0], 0, sizeof(float)*fioMarg1.x*fioMarg1.y*fioMarg1.z );
			memset( pfMarg2[0], 0, sizeof(float)*fioMarg2.x*fioMarg2.y*fioMarg2.z );

			// Generate joint probability table

			int rr, cc;
			int iJointSum = 0;

			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					float fPix1 = pfWind1[rr*fioTemplate1.x + cc];
					float fPix2 = pfWind2[rr*fioImg2.x      + cc];

					int iIndex1 = hfHistogramBin( fPix1, pfRanges1, fioRanges1.iFeaturesPerVector );
					int iIndex2 = hfHistogramBin( fPix2, pfRanges2, fioRanges2.iFeaturesPerVector );

					//((float*)(ppImg1.ImageRow(rr)))[cc] = fPix1;
					//((float*)(ppImg2.ImageRow(rr)))[cc] = fPix2;
					//((float*)(ppImg1.ImageRow(rr)))[cc] = iIndex1;
					//((float*)(ppImg2.ImageRow(rr)))[cc] = iIndex2;

					pfJoint[0][iIndex1*fioJoint.x + iIndex2]++;
					pfMarg1[0][iIndex1]++;
					pfMarg2[0][iIndex2]++;

					if( rr >= 2 && cc >= 2 && cc <= 12 && rr <= 12 )
					{
						pfJoint[1][iIndex1*fioJoint.x + iIndex2]++;
						pfMarg1[1][iIndex1]++;
						pfMarg2[1][iIndex2]++;
						if( rr >= 5 && cc >= 5 && cc <= 9 && rr <= 9 )
						{
							pfJoint[2][iIndex1*fioJoint.x + iIndex2]++;
							pfMarg1[2][iIndex1]++;
							pfMarg2[2][iIndex2]++;
						}
					}
				}
			}

			//output_float( ppImg1, "img1.pgm" );
			//output_float( ppImg2, "img2.pgm" );

			// Calculate entropies

			float fMarg1Entropy[3] = {0,0,0};
			for( rr = 0; rr < fioMarg1.x; rr++ )
			{
				fMarg1Entropy[0] += pfLogLookup[ (int)pfMarg1[0][rr] ];
				fMarg1Entropy[1] += pfLogLookup[ (int)pfMarg1[1][rr] ];
				fMarg1Entropy[2] += pfLogLookup[ (int)pfMarg1[2][rr] ];
			}

			float fMarg2Entropy[3] = {0,0,0};
			for( rr = 0; rr < fioMarg2.x; rr++ )
			{
				fMarg2Entropy[0] += pfLogLookup[ (int)pfMarg2[0][rr] ];
				fMarg2Entropy[1] += pfLogLookup[ (int)pfMarg2[1][rr] ];
				fMarg2Entropy[2] += pfLogLookup[ (int)pfMarg2[2][rr] ];

			}

			float sum[3] = {0,0,0};
			float fJointEntropy[3] = {0,0,0};
			for( rr = 0; rr < fioJoint.x*fioJoint.y; rr++ )
			{
				fJointEntropy[0] += pfLogLookup[ (int)pfJoint[0][rr] ];
				fJointEntropy[1] += pfLogLookup[ (int)pfJoint[1][rr] ];
				fJointEntropy[2] += pfLogLookup[ (int)pfJoint[2][rr] ];
				sum[0] +=pfJoint[0][rr];
				sum[1] +=pfJoint[1][rr];
				sum[2] +=pfJoint[2][rr];
			}

			float fMI0 = flogN[0] + (-fMarg1Entropy[0]-fMarg2Entropy[0]+fJointEntropy[0])*fNDiv[0];
			float fMI1 = flogN[1] + (-fMarg1Entropy[1]-fMarg2Entropy[1]+fJointEntropy[1])*fNDiv[1];
			float fMI2 = flogN[2] + (-fMarg1Entropy[2]-fMarg2Entropy[2]+fJointEntropy[2])*fNDiv[2];

			float fResult = (3*fMaxMI - fMI0 - fMI1- fMI2)*fMIDiv;
			assert( fResult > 0.0f );

			float fExp = exp( -fResult );
			fLikelihoodSum += fExp;
			fEntropySum    += fExp*fResult;

			//((float*)(ppImgLike.ImageRow(r)))[c] = fExp;
		}
	}

	//static int iCount = 0;
	//char pcFileName[300];
	//sprintf( pcFileName, "like_%3.3d.pgm", iCount );
	//output_float( ppImgLike, pcFileName );

	fEntropy = log( fLikelihoodSum ) + fEntropySum / fLikelihoodSum;

	return fEntropy;
}

int
hfCalculate_eol_image_MI_scale(
					   const int &iRowOffset,	// Offset of a pixel in fioImg1 to fioImg2
					   const int &iColOffset,
					   
					   const int &iRows,		// Domain size in fioImg2
					   const int &iCols,		//
					   
					   FEATUREIO &fioImg1,		// Image 1
					   FEATUREIO &fioImg2,		// Image 2

					   FEATUREIO &fioRanges1,	// Histogram ranges at locations in image 1
					   FEATUREIO &fioRanges2,	// Histogram ranges at locations in image 1

					   FEATUREIO &fioEOL,
					   
					   const int &iRowSize, // Size of template window
					   const int &iColSize, 
					   
					   const float &fMIVarrDiv,		// Varriance term for NCC energy

					   FEATUREIO &fioImgMask,
					   const int &iScales // The number of scales to calculate the MI
						)
{	
	float fEntropyDivisor = 1.0f / log((float)iRows*iCols);
	
	int iPercentDone = (fioImg1.x*fioImg1.y)/100;

	fioSet( fioEOL, 1.0f );

	// Allocate memory for distributions, template

	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = iScales;
	fioMarg1.x = fioRanges1.iFeaturesPerVector + 1;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	FEATUREIO fioMarg2;
	fioMarg2.iFeaturesPerVector = 1;
	fioMarg2.z = iScales;
	fioMarg2.x = fioRanges2.iFeaturesPerVector + 1;
	fioMarg2.y = 1;
	fioMarg2.t = 1;
	fioAllocate( fioMarg2 );
	float *pfMarg2 = fioMarg2.pfVectors;

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = iScales;
	fioJoint.x = fioMarg1.x;
	fioJoint.y = fioMarg2.x;
	fioJoint.t = 1;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	FEATUREIO fioTemplate;
	fioTemplate.iFeaturesPerVector = 1;
	fioTemplate.z = 1;
	fioTemplate.x = iColSize;
	fioTemplate.y = iRowSize;
	fioTemplate.t = 1;
	fioAllocate( fioTemplate );

	FEATUREIO fioTemplateRanges;
	fioTemplateRanges.iFeaturesPerVector = fioRanges1.iFeaturesPerVector;
	fioTemplateRanges.z = 1;
	fioTemplateRanges.x = 1;
	fioTemplateRanges.y = 1;
	fioTemplateRanges.t = 1;
	fioAllocate( fioTemplateRanges );

	// Generate a lookup table for log calculations. We need
	// as many pixels as in the image template.

	float *pfLogLookup = new float[iColSize*iRowSize+1];
	pfLogLookup[0] = 0.0f;
	for( int i = 1; i < iColSize*iRowSize+1; i++ )
	{
		pfLogLookup[i] = (float)(((float)i)*log((float)i));
	}

	PpImage ppImgTemp;

	float fMaxMI = log((float)fioRanges1.iFeaturesPerVector+1) < log((float)fioRanges2.iFeaturesPerVector+1) ?
		log((float)fioRanges1.iFeaturesPerVector+1) : log((float)fioRanges2.iFeaturesPerVector+1);

	//
	// Consider only template positions in Image1 that fully
	// fit within in the image.
	//
	// Adjust domain in Image2 so that there are exactly
	// iRows*iCols match locations.
	//
	// After this stage, the inner loop entropy calculation does
	// not have to worry about image boundaries.
	//

	float fMaxEnt = 0.0f;
	float fMinEnt = 10000.0;

	for( int y = iRowSize/2; y < fioImg1.y - iRowSize/2; y++ )
	{
		int iRow2Start = y + iRowOffset - iRows/2;
		if( iRow2Start < iRowSize/2 )
		{
			iRow2Start = iRowSize/2;
		}
		else if( iRow2Start + iRows >= fioImg2.y - iRowSize/2 )
		{
			iRow2Start = fioImg2.y - iRows - iRowSize/2 - 1;
		}

		printf( "row: %2.2d, (max,min): (%f.%f)\n", y, fMaxEnt, fMinEnt );
		
		for( int x = iColSize/2; x < fioImg1.x - iColSize/2; x++ )
		{
			if( fioImgMask.pfVectors )
			{
				if( fioGetPixel( fioImgMask, x, y, 0 ) != 255 )
				{
					// 
					continue;
				}
			}

			int iCol2Start = x + iColOffset - iCols/2;
			if( iCol2Start < iColSize/2 )
			{
				iCol2Start = iColSize/2;
			}
			else if( iCol2Start + iCols >= fioImg2.x - iColSize/2 )
			{
				iCol2Start = fioImg2.x - iCols - iColSize/2 - 1;
			}

			// Generate template

			for( int yy = 0; yy < iRowSize; yy++ )
			{
				memcpy(
					fioTemplate.pfVectors + yy*fioTemplate.x,
					fioImg1.pfVectors + (yy+y-iRowSize/2)*fioImg1.x + (x - iColSize/2),
					iColSize*sizeof(float)
					);
			}

			//fioimgInitReference( ppImgTemp, fioTemplate );
			//output_float( ppImgTemp, "template.pgm" );

			// Generate template ranges

			memcpy(
				fioTemplateRanges.pfVectors,
				fioRanges1.pfVectors + (y*fioRanges1.x + x)*fioRanges1.iFeaturesPerVector,
				fioRanges1.iFeaturesPerVector*sizeof(float)
				);

			// Here we calculated the entropy of the MI calculation

			float fEntropy = 
				hfGenerate_MI_EOL_lookup_scale(
				//hfGenerate_MI_EOL(
					iRow2Start, iCol2Start,	// Domain starting position in fioImg2
					iRows, iCols,			// Domain size in fioImg2

					fioTemplate,			// Template from fioImg1
					fioImg2,				// Domain: fioImg2

					fioTemplateRanges,		// Template histogram ranges
					fioRanges2,				// Domain histogram ranges
					
					//FEATUREIO &fioMI,

					   fioMarg1,			// Marginal for source template
					   fioMarg2,			// Marginal for domain template
					   fioJoint,			// Joint histogram for source, domain
					   fMaxMI,
					   fMIVarrDiv,		// Varriance term for NCC energy
					   pfLogLookup
							);

			// Normalize
			fEntropy *= fEntropyDivisor;
			
			//if( fEntropy < 0.0 || fEntropy > 1.0 ) { assert( 0 ); }
			fioEOL.pfVectors[y*fioEOL.x + x] = fEntropy;

			if( fEntropy > fMaxEnt )
			{
				fMaxEnt = fEntropy;
			}
			if( fEntropy < fMinEnt )
			{
				fMinEnt = fEntropy;
			}
		}
	}

	delete [] pfLogLookup;
	fioDelete( fioMarg1 );
	fioDelete( fioMarg2 );
	fioDelete( fioJoint );
	fioDelete( fioTemplate );
	fioDelete( fioTemplateRanges );
	
	return 1;
}

/////////////////////////////////////////////////////////////////////
//
// Class Probability Image - each pixel is a probability over a number
//	of classes.
//
/////////////////////////////////////////////////////////////////////

int
hfGenerateProbMILikelihoodImage(
								int *piCoord1,	// Center of box in fio1, xyzt, col/row/z/time
								int *piSize1,  // Size of box in fio1
								int *piCoord2,	// Center of box (search area) in fio2
								int *piSize2,	// Center of box (search area) in fio2
								FEATUREIO &fio1,
								FEATUREIO &fio2,
								FEATUREIO &fioMI
							)
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fio1.iFeaturesPerVector;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	FEATUREIO fioMarg2;
	fioMarg2.iFeaturesPerVector = 1;
	fioMarg2.z = 1;
	fioMarg2.x = fio2.iFeaturesPerVector;
	fioMarg2.y = 1;
	fioMarg2.t = 1;
	fioAllocate( fioMarg2 );
	float *pfMarg2 = fioMarg2.pfVectors;

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 1;
	fioJoint.x = fioMarg1.x;
	fioJoint.y = fioMarg2.x;
	fioJoint.t = fioMarg2.x;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	PpImage ppImgTemp;
	fioimgInitReference( ppImgTemp, fioJoint );

	PpImage ppImg1;
	//ppImg1.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );
	PpImage ppImg2;
	//ppImg2.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );

	//
	// x,y,z are the coordinates of box center location in fio2
	//
	for( int z = piCoord2[2]-piSize2[2]/2; z <= piCoord2[2]+piSize2[2]/2; z++ )
	{
		//
		// Check to see entire box fits in image 2,
		// if not then skip.
		//
		if( z - piSize1[2]/2 < 0 || z + piSize1[2]/2 >= fio2.z )
		{
			continue;
		}

		for( int y = piCoord2[1]-piSize2[1]/2; y <= piCoord2[1]+piSize2[1]/2; y++ )
		{
			if( y - piSize1[1]/2 < 0 || y + piSize1[1]/2 >= fio2.y )
			{
				continue;
			}

			//printf( "Row: %d\n", y );
			for( int x = piCoord2[0]-piSize2[0]/2; x <= piCoord2[0]+piSize2[0]/2; x++ )
			{
				if( x - piSize1[0]/2 < 0 || x + piSize1[0]/2 >= fio2.x )
				{
					continue;
				}

				int class1, class2;

				// Generation of probability tables could be done more efficiently

				memset( pfJoint, 0, sizeof(float)*fioJoint.x*fioJoint.y );
				memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );
				memset( pfMarg2, 0, sizeof(float)*fioMarg2.x*fioMarg2.y );

				// Generate joint probability table
				int xx, yy, zz;

				// Sum over corresponding box elements
				for( zz = 0; zz < piSize1[2]; zz++ )
				{
					for( yy = 0; yy < piSize1[1]; yy++ )
					{
						for( xx = 0; xx < piSize1[0]; xx++ )
						{
							// Sum over class probabilities (ouch, this is painfull)

							float *pfProbs1 = fioGetVector( fio1,
								piCoord1[0]-piSize1[0]/2+xx,
								piCoord1[1]-piSize1[1]/2+yy,
								piCoord1[2]-piSize1[2]/2+zz);
								//piCoord1[0]+piSize1[0]/2-xx,
								//piCoord1[1]-piSize1[1]/2+yy,
								//piCoord1[2]-piSize1[2]/2+zz);
							float *pfProbs2 = fioGetVector( fio2,
								x-piSize1[0]/2+xx,
								y-piSize1[1]/2+yy,
								z-piSize1[2]/2+zz);

							// Sum up maginal 1
							for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
							{
								pfMarg1[class1] += pfProbs1[class1];
							}

							// Sum up marginal 2
							for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
							{
								pfMarg2[class2] += pfProbs2[class2];
							}

							// Sum up joint, assume independence
							for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
							{
								for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
								{
									pfJoint[class2*fio1.iFeaturesPerVector + class1] +=
										pfProbs1[class1]*pfProbs2[class2];
								}
							}
						}
					}

					//output_float( ppImgTemp, "joint_intensity.pgm" );
					//output_float_to_text( ppImgTemp, "joint_intensity.txt" );

					//output_float( ppImg1, "img1.pgm" );
					//output_float( ppImg2, "img2.pgm" );

					// Normalize
					float fNorm;
					float fNormMult;

					// Normalize marginal 1
					fNorm = 0;
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						fNorm += pfMarg1[class1];
					}
					assert( fNorm > 0 );
					fNormMult = 1.0f/fNorm;
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						pfMarg1[class1] *= fNormMult;
					}

					// Normalize marginal 2
					fNorm = 0;
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						fNorm += pfMarg2[class2];
					}
					assert( fNorm > 0 );
					fNormMult = 1.0f/fNorm;
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						pfMarg2[class2] *= fNormMult;
					}

					// Normalize joint
					fNorm = 0;
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
						{
							fNorm += pfJoint[class2*fio1.iFeaturesPerVector + class1];
						}
					}
					assert( fNorm > 0 );
					fNormMult = 1.0f/fNorm;
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
						{
							pfJoint[class2*fio1.iFeaturesPerVector + class1] *= fNormMult;
						}
					}

					// Calculate entropies

					int rr, cc;

					float fProbSum;
					fProbSum = 0.0f;
					float fMarg1Entropy = 0;
					for( rr = 0; rr < fioMarg1.x; rr++ )
					{
						if( pfMarg1[rr] > 0 )
						{
							float fProb = pfMarg1[rr];
							fProbSum += fProb;
							fMarg1Entropy -= (float)(fProb*log(fProb));
						}
					}

					fProbSum = 0.0f;
					float fMarg2Entropy = 0;
					for( rr = 0; rr < fioMarg2.x; rr++ )
					{
						if( pfMarg2[rr] > 0 )
						{
							float fProb = pfMarg2[rr];
							fProbSum += fProb;
							fMarg2Entropy -= (float)(fProb*log(fProb));
						}
					}

					fProbSum = 0.0f;
					float fJointEntropy = 0;
					for( rr = 0; rr < fio2.iFeaturesPerVector; rr++ )
					{
						for( cc = 0; cc < fio1.iFeaturesPerVector; cc++ )
						{
							if( pfJoint[rr*fio1.iFeaturesPerVector + cc] > 0 )
							{
								float fProb = pfJoint[rr*fio1.iFeaturesPerVector + cc];
								fProbSum += fProb;
								fJointEntropy -= (float)(fProb*log(fProb));
							}
						}
					}

					float *pfMI = fioGetVector( fioMI, x, y, z );
					*pfMI = fMarg1Entropy + fMarg2Entropy - fJointEntropy;
				}
			}
		}
	}

	fioDelete( fioMarg1 );
	fioDelete( fioMarg2 );
	fioDelete( fioJoint );

	return 1;
}

int
hfGenerateLikelihoodImageCorrelation(
								int *piCoord1,	// Center of box in fio1, xyzt, col/row/z/time
								int *piSize1,  // Size of box in fio1
								int *piCoord2,	// Center of box (search area) in fio2
								int *piSize2,	// Center of box (search area) in fio2
								FEATUREIO &fio1,
								FEATUREIO &fio2,
								FEATUREIO &fioMI
							)
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fio1.iFeaturesPerVector;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	FEATUREIO fioMarg2;
	fioMarg2.iFeaturesPerVector = 1;
	fioMarg2.z = 1;
	fioMarg2.x = fio2.iFeaturesPerVector;
	fioMarg2.y = 1;
	fioMarg2.t = 1;
	fioAllocate( fioMarg2 );
	float *pfMarg2 = fioMarg2.pfVectors;

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 1;
	fioJoint.x = fioMarg1.x;
	fioJoint.y = fioMarg2.x;
	fioJoint.t = fioMarg2.x;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	PpImage ppImgTemp;
	fioimgInitReference( ppImgTemp, fioJoint );

	PpImage ppImg1;
	//ppImg1.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );
	PpImage ppImg2;
	//ppImg2.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );

	//
	// x,y,z are the coordinates of box center location in fio2
	//
	for( int z = piCoord2[2]-piSize2[2]/2; z <= piCoord2[2]+piSize2[2]/2; z++ )
	{
		//
		// Check to see entire box fits in image 2,
		// if not then skip.
		//
		if( z - piSize1[2]/2 < 0 || z + piSize1[2]/2 >= fio2.z )
		{
			continue;
		}

		for( int y = piCoord2[1]-piSize2[1]/2; y <= piCoord2[1]+piSize2[1]/2; y++ )
		{
			if( y - piSize1[1]/2 < 0 || y + piSize1[1]/2 >= fio2.y )
			{
				continue;
			}

			//printf( "Row: %d\n", y );
			for( int x = piCoord2[0]-piSize2[0]/2; x <= piCoord2[0]+piSize2[0]/2; x++ )
			{
				if( x - piSize1[0]/2 < 0 || x + piSize1[0]/2 >= fio2.x )
				{
					continue;
				}

				int class1, class2;
	
				float *pfProbs2Center = fioGetVector( fio2, x, y, z);
				if( pfProbs2Center[0] == 0 )
				{
					continue;
				}

				// Generation of probability tables could be done more efficiently

				memset( pfJoint, 0, sizeof(float)*fioJoint.x*fioJoint.y );
				memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );
				memset( pfMarg2, 0, sizeof(float)*fioMarg2.x*fioMarg2.y );

				// Generate joint probability table
				int xx, yy, zz;

				float fDiff = 0;

				float fSumSqr1 = 0;
				float fSumSqr2 = 0;

				// Sum over corresponding box elements
				for( zz = 0; zz < piSize1[2]; zz++ )
				{
					for( yy = 0; yy < piSize1[1]; yy++ )
					{
						for( xx = 0; xx < piSize1[0]; xx++ )
						{
							// Sum over class probabilities (ouch, this is painfull)

							float *pfProbs1 = fioGetVector( fio1,
								piCoord1[0]-piSize1[0]/2+xx,
								piCoord1[1]-piSize1[1]/2+yy,
								piCoord1[2]-piSize1[2]/2+zz);
							float *pfProbs2 = fioGetVector( fio2,
								x-piSize1[0]/2+xx,
								y-piSize1[1]/2+yy,
								z-piSize1[2]/2+zz);

							// Sum up product
							for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
							{
								fDiff +=  pfProbs1[class1]*pfProbs2[class1];
								fSumSqr1 += pfProbs1[class1]*pfProbs1[class1];
								fSumSqr2 += pfProbs2[class1]*pfProbs2[class1];
							}
						}
					}
					float *pfMI = fioGetVector( fioMI, x, y, z );
					if( fSumSqr1*fSumSqr2 > 0 )
					{
						// NCC
						*pfMI = fDiff / sqrt(fSumSqr1*fSumSqr2);
					}
					else
					{
						*pfMI = 0;
					}
				}
			}
		}
	}

	fioDelete( fioMarg1 );
	fioDelete( fioMarg2 );
	fioDelete( fioJoint );

	return 1;
}

int
hfGenerateLikelihoodImageSumAbsDiff(
								int *piCoord1,	// Center of box in fio1, xyzt, col/row/z/time
								int *piSize1,  // Size of box in fio1
								int *piCoord2,	// Center of box (search area) in fio2
								int *piSize2,	// Center of box (search area) in fio2
								FEATUREIO &fio1,
								FEATUREIO &fio2,
								FEATUREIO &fioMI
							)
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fio1.iFeaturesPerVector;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	FEATUREIO fioMarg2;
	fioMarg2.iFeaturesPerVector = 1;
	fioMarg2.z = 1;
	fioMarg2.x = fio2.iFeaturesPerVector;
	fioMarg2.y = 1;
	fioMarg2.t = 1;
	fioAllocate( fioMarg2 );
	float *pfMarg2 = fioMarg2.pfVectors;

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 1;
	fioJoint.x = fioMarg1.x;
	fioJoint.y = fioMarg2.x;
	fioJoint.t = fioMarg2.x;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	PpImage ppImgTemp;
	fioimgInitReference( ppImgTemp, fioJoint );

	PpImage ppImg1;
	//ppImg1.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );
	PpImage ppImg2;
	//ppImg2.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );

	//
	// x,y,z are the coordinates of box center location in fio2
	//
	for( int z = piCoord2[2]-piSize2[2]/2; z <= piCoord2[2]+piSize2[2]/2; z++ )
	{
		//
		// Check to see entire box fits in image 2,
		// if not then skip.
		//
		if( z - piSize1[2]/2 < 0 || z + piSize1[2]/2 >= fio2.z )
		{
			continue;
		}

		for( int y = piCoord2[1]-piSize2[1]/2; y <= piCoord2[1]+piSize2[1]/2; y++ )
		{
			if( y - piSize1[1]/2 < 0 || y + piSize1[1]/2 >= fio2.y )
			{
				continue;
			}

			//printf( "Row: %d\n", y );
			for( int x = piCoord2[0]-piSize2[0]/2; x <= piCoord2[0]+piSize2[0]/2; x++ )
			{
				if( x - piSize1[0]/2 < 0 || x + piSize1[0]/2 >= fio2.x )
				{
					continue;
				}

				int class1, class2;

				// Generation of probability tables could be done more efficiently

				memset( pfJoint, 0, sizeof(float)*fioJoint.x*fioJoint.y );
				memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );
				memset( pfMarg2, 0, sizeof(float)*fioMarg2.x*fioMarg2.y );

				// Generate joint probability table
				int xx, yy, zz;

				float fDiff = 0;

				// Sum over corresponding box elements
				for( zz = 0; zz < piSize1[2]; zz++ )
				{
					for( yy = 0; yy < piSize1[1]; yy++ )
					{
						for( xx = 0; xx < piSize1[0]; xx++ )
						{
							// Sum over class probabilities (ouch, this is painfull)

							float *pfProbs1 = fioGetVector( fio1,
								piCoord1[0]-piSize1[0]/2+xx,
								piCoord1[1]-piSize1[1]/2+yy,
								piCoord1[2]-piSize1[2]/2+zz);
							float *pfProbs2 = fioGetVector( fio2,
								x-piSize1[0]/2+xx,
								y-piSize1[1]/2+yy,
								z-piSize1[2]/2+zz);

							// Sum up maginal 1
							for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
							{
								fDiff +=  fabs( pfProbs1[class1] - pfProbs2[class1] );
							}

						}
					}
					float *pfMI = fioGetVector( fioMI, x, y, z );
					*pfMI = -fDiff;
				}
			}
		}
	}

	fioDelete( fioMarg1 );
	fioDelete( fioMarg2 );
	fioDelete( fioJoint );

	return 1;
}



int
hfGenerateProbMILikelihoodImage(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fio1,
					   FEATUREIO &fio2,
					   FEATUREIO &fioMI,
					   int iWindow
							)
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fio1.iFeaturesPerVector;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	FEATUREIO fioMarg2;
	fioMarg2.iFeaturesPerVector = 1;
	fioMarg2.z = 1;
	fioMarg2.x = fio2.iFeaturesPerVector;
	fioMarg2.y = 1;
	fioMarg2.t = 1;
	fioAllocate( fioMarg2 );
	float *pfMarg2 = fioMarg2.pfVectors;

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 1;
	fioJoint.x = fioMarg1.x;
	fioJoint.y = fioMarg2.x;
	fioJoint.t = fioMarg2.x;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	// Calculate conditional entropy H(I|X),
	// in order to test MI normalization via MI(I,X)
	FEATUREIO fioH_X1 = fio1;
	FEATUREIO fioH_X2 = fio2;
	fioH_X1.iFeaturesPerVector = 1;
	fioH_X2.iFeaturesPerVector = 1;
	fioAllocate( fioH_X1 );
	fioAllocate( fioH_X2 );
	float fNorm = 1.0f/(iWindowRows*iWindowCols);
	for( int i = 0; i < fioH_X1.x*fioH_X1.y; i++ )
	{
		float fH_X = computeEntropy( fio1.iFeaturesPerVector, fio1.pfVectors + i*fio1.iFeaturesPerVector );
		fioH_X1.pfVectors[i] = fH_X*fNorm;
	}
	for( int i = 0; i < fioH_X2.x*fioH_X2.y; i++ )
	{
		float fH_X = computeEntropy( fio2.iFeaturesPerVector, fio2.pfVectors + i*fio2.iFeaturesPerVector );
		fioH_X2.pfVectors[i] = fH_X*fNorm;
	}


	PpImage ppImgTemp;
	fioimgInitReference( ppImgTemp, fioJoint );

	PpImage ppImg1;
	//ppImg1.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );
	PpImage ppImg2;
	//ppImg2.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );

	int iRowStart = iRow - iWindowRows/2;
	if( iRowStart < 0 )
	{
		iRowStart = 0;
	}
	else if( iRowStart + iWindowRows > fio1.y )
	{
		iRowStart = fio1.y - iWindowRows;
	}

	int iColStart = iCol - iWindowCols/2;
	if( iColStart < 0 )
	{
		iColStart = 0;
	}
	else if( iColStart + iWindowCols > fio1.x )
	{
		iColStart = fio1.x - iWindowCols;
	}

	float *pfWind1 = fio1.pfVectors + (iRowStart*fio1.x + iColStart)*fio1.iFeaturesPerVector;	
	float *pfWindH_X1 = fioH_X1.pfVectors + (iRowStart*fioH_X1.x + iColStart)*fioH_X1.iFeaturesPerVector;

	float fH_X1;
	float fH_X2;

	for( int r = iWindowRows; r < fio2.y - iWindowRows; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fio2.y )
		{
			iRowStart = fio2.y - iWindowRows;
		}

		if( iWindow >= 0 )
		{
			if( abs(r-iRow) > iWindow )
			{
				continue;
			}
		}

		printf( "Row: %d\n", r );
		for( int c = iWindowCols; c < fio2.x - iWindowCols; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fio2.x )
			{
				iColStart = fio2.x - iWindowCols;
			}

			if( iWindow >= 0 )
			{
				if( abs(c-iCol) > iWindow )
				{
					continue;
				}
			}

			int class1, class2;

			float *pfWind2 = fio2.pfVectors + (iRowStart*fio2.x + iColStart)*fio2.iFeaturesPerVector;
			float *pfWindH_X2 = fioH_X2.pfVectors + (iRowStart*fioH_X2.x + iColStart)*fioH_X2.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			memset( pfJoint, 0, sizeof(float)*fioJoint.x*fioJoint.y );
			memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );
			memset( pfMarg2, 0, sizeof(float)*fioMarg2.x*fioMarg2.y );

			// Generate joint probability table
			int rr, cc;

			
			fH_X1=0;
			fH_X2=0;
			
			// Sum over location  alignments
			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					// Sum over class probabilities (ouch, this is painfull)

					float *pfProbs1 = pfWind1 + (rr*fio1.x + cc)*fio1.iFeaturesPerVector;
					float *pfProbs2 = pfWind2 + (rr*fio2.x + cc)*fio2.iFeaturesPerVector;

					// Sum up condition entropy
					fH_X1 += pfWindH_X1[rr*fioH_X1.x+cc];
					fH_X2 += pfWindH_X2[rr*fioH_X2.x+cc];

					// Sum up maginal 1
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						pfMarg1[class1] += pfProbs1[class1];
					}

					// Sum up marginal 2
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						pfMarg2[class2] += pfProbs2[class2];
					}

					// Sum up joint, assume independence
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
						{
							pfJoint[class2*fio1.iFeaturesPerVector + class1] +=
								pfProbs1[class1]*pfProbs2[class2];
						}
					}
				}
			}

			//output_float( ppImgTemp, "joint_intensity.pgm" );
			//output_float_to_text( ppImgTemp, "joint_intensity.txt" );

			//output_float( ppImg1, "img1.pgm" );
			//output_float( ppImg2, "img2.pgm" );

			// Normalize
			float fNorm;
			float fNormMult;

			// Normalize marginal 1
			fNorm = 0;
			for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
			{
				fNorm += pfMarg1[class1];
			}
			assert( fNorm > 0 );
			fNormMult = 1.0f/fNorm;
			for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
			{
				pfMarg1[class1] *= fNormMult;
			}

			// Normalize marginal 2
			fNorm = 0;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				fNorm += pfMarg2[class2];
			}
			assert( fNorm > 0 );
			fNormMult = 1.0f/fNorm;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				pfMarg2[class2] *= fNormMult;
			}

			// Normalize joint
			fNorm = 0;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
				{
					fNorm += pfJoint[class2*fio1.iFeaturesPerVector + class1];
				}
			}
			assert( fNorm > 0 );
			fNormMult = 1.0f/fNorm;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
				{
					pfJoint[class2*fio1.iFeaturesPerVector + class1] *= fNormMult;
				}
			}

			// Calculate entropies

			float fProbSum;

			fProbSum = 0.0f;
			float fMarg1Entropy = 0;
			for( rr = 0; rr < fioMarg1.x; rr++ )
			{
				if( pfMarg1[rr] > 0 )
				{
					float fProb = pfMarg1[rr];
					fProbSum += fProb;
					fMarg1Entropy -= (float)(fProb*log(fProb));
				}
			}

			fProbSum = 0.0f;
			float fMarg2Entropy = 0;
			for( rr = 0; rr < fioMarg2.x; rr++ )
			{
				if( pfMarg2[rr] > 0 )
				{
					float fProb = pfMarg2[rr];
					fProbSum += fProb;
					fMarg2Entropy -= (float)(fProb*log(fProb));
				}
			}

			fProbSum = 0.0f;
			float fJointEntropy = 0;
			for( rr = 0; rr < fio2.iFeaturesPerVector; rr++ )
			{
				for( cc = 0; cc < fio1.iFeaturesPerVector; cc++ )
				{
					if( pfJoint[rr*fio1.iFeaturesPerVector + cc] > 0 )
					{
						float fProb = pfJoint[rr*fio1.iFeaturesPerVector + cc];
						fProbSum += fProb;
						fJointEntropy -= (float)(fProb*log(fProb));
					}
				}
			}

			// Original MI - so far the best ...
			fioMI.pfVectors[r*fioMI.x + c] = (fMarg1Entropy + fMarg2Entropy - fJointEntropy);

			float fMII_X1 = fMarg1Entropy-fH_X1;
			float fMII_X2 = fMarg2Entropy-fH_X2;

			//assert( fMII_X1 + fMII_X2 > 0 );
			//assert( (fMarg1Entropy + fMarg2Entropy - fJointEntropy) <= (fMII_X1+fMII_X2) );

			// Normalize MI by MI(I,X): this should only do something for probabilistic images, lets see...
			//fioMI.pfVectors[r*fioMI.x + c] = (fMarg1Entropy + fMarg2Entropy - fJointEntropy)/ (fMII_X1+fMII_X2);

			// Normalized MI - makes very little difference, not sure if it improves or worsens
			//fioMI.pfVectors[r*fioMI.x + c] = (fMarg1Entropy + fMarg2Entropy - fJointEntropy)/(fMarg1Entropy + fMarg2Entropy);
		}
	}

	fioDelete( fioH_X1 );
	fioDelete( fioH_X2 );

	fioDelete( fioMarg1 );
	fioDelete( fioMarg2 );
	fioDelete( fioJoint );

	return 1;
}

//
// hfGenerateProbMILikelihoodImageHash(
//
//
//
//
int
hfGenerateProbMILikelihoodImageHash(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fio1, // Probabilibites
					   FEATUREIO &fio2,
					   FEATUREIO &fioIndex1, // Indices
					   FEATUREIO &fioIndex2, 
					   FEATUREIO &fioMI,
					   int iWindow
							)
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fio1.iFeaturesPerVector;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	FEATUREIO fioMarg2;
	fioMarg2.iFeaturesPerVector = 1;
	fioMarg2.z = 1;
	fioMarg2.x = fio2.iFeaturesPerVector;
	fioMarg2.y = 1;
	fioMarg2.t = 1;
	fioAllocate( fioMarg2 );
	float *pfMarg2 = fioMarg2.pfVectors;

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 1;
	fioJoint.x = fioMarg1.x;
	fioJoint.y = fioMarg2.x;
	fioJoint.t = fioMarg2.x;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	PpImage ppImgTemp;
	fioimgInitReference( ppImgTemp, fioJoint );

	PpImage ppImg1;
	//ppImg1.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );
	PpImage ppImg2;
	//ppImg2.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );

	int iRowStart = iRow - iWindowRows/2;
	if( iRowStart < 0 )
	{
		iRowStart = 0;
	}
	else if( iRowStart + iWindowRows > fio1.y )
	{
		iRowStart = fio1.y - iWindowRows;
	}

	int iColStart = iCol - iWindowCols/2;
	if( iColStart < 0 )
	{
		iColStart = 0;
	}
	else if( iColStart + iWindowCols > fio1.x )
	{
		iColStart = fio1.x - iWindowCols;
	}

	float *pfWind1 = fio1.pfVectors + (iRowStart*fio1.x + iColStart)*fio1.iFeaturesPerVector;

	for( int r = iWindowRows; r < fio2.y - iWindowRows; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fio2.y )
		{
			iRowStart = fio2.y - iWindowRows;
		}

		if( iWindow >= 0 )
		{
			if( abs(r-iRow) > iWindow )
			{
				continue;
			}
		}

		printf( "Row: %d\n", r );
		for( int c = iWindowCols; c < fio2.x - iWindowCols; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fio2.x )
			{
				iColStart = fio2.x - iWindowCols;
			}

			if( iWindow >= 0 )
			{
				if( abs(c-iCol) > iWindow )
				{
					continue;
				}
			}

			int class1, class2;

			float *pfWind2 = fio2.pfVectors + (iRowStart*fio2.x + iColStart)*fio2.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			memset( pfJoint, 0, sizeof(float)*fioJoint.x*fioJoint.y );
			memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );
			memset( pfMarg2, 0, sizeof(float)*fioMarg2.x*fioMarg2.y );

			// Generate joint probability table
			int rr, cc;
			
			// Sum over location  alignments
			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					// Sum over class probabilities (ouch, this is painfull)

					float *pfProbs1 = pfWind1 + (rr*fio1.x + cc)*fio1.iFeaturesPerVector;
					float *pfProbs2 = pfWind2 + (rr*fio2.x + cc)*fio2.iFeaturesPerVector;

					// Sum up maginal 1
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						pfMarg1[class1] += pfProbs1[class1];
					}

					// Sum up marginal 2
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						pfMarg2[class2] += pfProbs2[class2];
					}

					// Sum up joint, assume independence
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
						{
							pfJoint[class2*fio1.iFeaturesPerVector + class1] +=
								pfProbs1[class1]*pfProbs2[class2];
						}
					}
				}
			}

			//output_float( ppImgTemp, "joint_intensity.pgm" );
			//output_float_to_text( ppImgTemp, "joint_intensity.txt" );

			//output_float( ppImg1, "img1.pgm" );
			//output_float( ppImg2, "img2.pgm" );

			// Normalize
			float fNorm;
			float fNormMult;

			// Normalize marginal 1
			fNorm = 0;
			for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
			{
				fNorm += pfMarg1[class1];
			}
			assert( fNorm > 0 );
			fNormMult = 1.0f/fNorm;
			for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
			{
				pfMarg1[class1] *= fNormMult;
			}

			// Normalize marginal 2
			fNorm = 0;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				fNorm += pfMarg2[class2];
			}
			assert( fNorm > 0 );
			fNormMult = 1.0f/fNorm;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				pfMarg2[class2] *= fNormMult;
			}

			// Normalize joint
			fNorm = 0;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
				{
					fNorm += pfJoint[class2*fio1.iFeaturesPerVector + class1];
				}
			}
			assert( fNorm > 0 );
			fNormMult = 1.0f/fNorm;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
				{
					pfJoint[class2*fio1.iFeaturesPerVector + class1] *= fNormMult;
				}
			}

			// Calculate entropies

			float fProbSum;

			fProbSum = 0.0f;
			float fMarg1Entropy = 0;
			for( rr = 0; rr < fioMarg1.x; rr++ )
			{
				if( pfMarg1[rr] > 0 )
				{
					float fProb = pfMarg1[rr];
					fProbSum += fProb;
					fMarg1Entropy -= (float)(fProb*log(fProb));
				}
			}

			fProbSum = 0.0f;
			float fMarg2Entropy = 0;
			for( rr = 0; rr < fioMarg2.x; rr++ )
			{
				if( pfMarg2[rr] > 0 )
				{
					float fProb = pfMarg2[rr];
					fProbSum += fProb;
					fMarg2Entropy -= (float)(fProb*log(fProb));
				}
			}

			fProbSum = 0.0f;
			float fJointEntropy = 0;
			for( rr = 0; rr < fio2.iFeaturesPerVector; rr++ )
			{
				for( cc = 0; cc < fio1.iFeaturesPerVector; cc++ )
				{
					if( pfJoint[rr*fio1.iFeaturesPerVector + cc] > 0 )
					{
						float fProb = pfJoint[rr*fio1.iFeaturesPerVector + cc];
						fProbSum += fProb;
						fJointEntropy -= (float)(fProb*log(fProb));
					}
				}
			}

			fioMI.pfVectors[r*fioMI.x + c] = fMarg1Entropy + fMarg2Entropy - fJointEntropy;
		}
	}

	fioDelete( fioMarg1 );
	fioDelete( fioMarg2 );
	fioDelete( fioJoint );

	return 1;
}



int
hfGenerateProbDiffLikelihoodImage(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fio1,
					   FEATUREIO &fio2,
					   FEATUREIO &fioMI
							)
{
	int iRowStart = iRow - iWindowRows/2;
	if( iRowStart < 0 )
	{
		iRowStart = 0;
	}
	else if( iRowStart + iWindowRows > fio1.y )
	{
		iRowStart = fio1.y - iWindowRows;
	}

	int iColStart = iCol - iWindowCols/2;
	if( iColStart < 0 )
	{
		iColStart = 0;
	}
	else if( iColStart + iWindowCols > fio1.x )
	{
		iColStart = fio1.x - iWindowCols;
	}

	float *pfWind1 = fio1.pfVectors + (iRowStart*fio1.x + iColStart)*fio1.iFeaturesPerVector;

	float fMaxPossibleDiff = 2*iWindowCols*iWindowRows;
	double dSumAll = 0;
	for( int r = 0; r < fio2.y; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fio2.y )
		{
			iRowStart = fio2.y - iWindowRows;
		}

		printf( "Row: %d\n", r );
		for( int c = 0; c < fio2.x; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fio2.x )
			{
				iColStart = fio2.x - iWindowCols;
			}

			int class1, class2;

			float *pfWind2 = fio2.pfVectors + (iRowStart*fio2.x + iColStart)*fio2.iFeaturesPerVector;

			// Generate joint probability table
			int rr, cc;

			float fResult = 0;

			// Sum over location  alignments
			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					// Sum over class probabilities (ouch, this is painfull)

					float *pfProbs1 = pfWind1 + (rr*fio1.x + cc)*fio1.iFeaturesPerVector;
					float *pfProbs2 = pfWind2 + (rr*fio2.x + cc)*fio2.iFeaturesPerVector;

					// Sum up maginal 1
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						//fResult += abs( pfProbs1[class1] - pfProbs2[class1] );
						fResult += pfProbs1[class1]*pfProbs2[class1];
					}
				}
			}
			//fioMI.pfVectors[r*fioMI.x + c] = fMaxPossibleDiff - fResult;
			fioMI.pfVectors[r*fioMI.x + c] = fResult;
			dSumAll += fResult;
		}
	}

	// Remove average, to get mean centered image
	dSumAll /= (float)(fio2.y*fio2.x);
	for( int r = 0; r < fio2.y; r++ )
	{
		for( int c = 0; c < fio2.x; c++ )
		{
			fioMI.pfVectors[r*fioMI.x + c] -= dSumAll;
		}
	}


	return 1;
}



/////////////////////////////////////////////////////////////////////
//
// For CVPR 2010: uses a lookup table to compute pi log pi 
//
/////////////////////////////////////////////////////////////////////

#define LOOKUP_N 10000
float g_fPlogP[LOOKUP_N+1];		// Entropy look up table
float g_flogNFact[LOOKUP_N+1];	// Factorial look up table

int
hfGenerateProbMILikelihoodImageLookupTable(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fio1,
					   FEATUREIO &fio2,
					   FEATUREIO &fioMI
							)
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fio1.iFeaturesPerVector;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	FEATUREIO fioMarg2;
	fioMarg2.iFeaturesPerVector = 1;
	fioMarg2.z = 1;
	fioMarg2.x = fio2.iFeaturesPerVector;
	fioMarg2.y = 1;
	fioMarg2.t = 1;
	fioAllocate( fioMarg2 );
	float *pfMarg2 = fioMarg2.pfVectors;

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 1;
	fioJoint.x = fioMarg1.x;
	fioJoint.y = fioMarg2.x;
	fioJoint.t = fioMarg2.x;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	PpImage ppImgTemp;
	fioimgInitReference( ppImgTemp, fioJoint );

	// Init lookup tables
	for( int i = 0; i < LOOKUP_N+1; i++ )
	{
		if( i == 0 )
		{
			g_fPlogP[i] = 0;
			g_flogNFact[i] = 0;
		}
		else
		{
			float fProb = i / (float)LOOKUP_N;
			g_fPlogP[i] = -fProb*log(fProb);
			g_flogNFact[i] = g_flogNFact[i-1] + log((float)i);
		}
	}

	PpImage ppImg1;
	//ppImg1.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );
	PpImage ppImg2;
	//ppImg2.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );

	int iRowStart = iRow - iWindowRows/2;
	if( iRowStart < 0 )
	{
		iRowStart = 0;
	}
	else if( iRowStart + iWindowRows > fio1.y )
	{
		iRowStart = fio1.y - iWindowRows;
	}

	int iColStart = iCol - iWindowCols/2;
	if( iColStart < 0 )
	{
		iColStart = 0;
	}
	else if( iColStart + iWindowCols > fio1.x )
	{
		iColStart = fio1.x - iWindowCols;
	}

	float *pfWind1 = fio1.pfVectors + (iRowStart*fio1.x + iColStart)*fio1.iFeaturesPerVector;

	for( int r = 0; r < fio2.y; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fio2.y )
		{
			iRowStart = fio2.y - iWindowRows;
		}

		printf( "Row: %d\n", r );
		for( int c = 0; c < fio2.x; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fio2.x )
			{
				iColStart = fio2.x - iWindowCols;
			}

			int class1, class2;

			float *pfWind2 = fio2.pfVectors + (iRowStart*fio2.x + iColStart)*fio2.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			memset( pfJoint, 0, sizeof(float)*fioJoint.x*fioJoint.y );
			memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );
			memset( pfMarg2, 0, sizeof(float)*fioMarg2.x*fioMarg2.y );

			// Generate joint probability table
			int rr, cc;
			
			// Sum over location  alignments
			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					// Sum over class probabilities (ouch, this is painfull)

					float *pfProbs1 = pfWind1 + (rr*fio1.x + cc)*fio1.iFeaturesPerVector;
					float *pfProbs2 = pfWind2 + (rr*fio2.x + cc)*fio2.iFeaturesPerVector;

					// Sum up maginal 1
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						pfMarg1[class1] += pfProbs1[class1];
					}

					// Sum up marginal 2
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						pfMarg2[class2] += pfProbs2[class2];
					}

					// Sum up joint, assume independence
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
						{
							pfJoint[class2*fio1.iFeaturesPerVector + class1] +=
								pfProbs1[class1]*pfProbs2[class2];
						}
					}
				}
			}

			//output_float( ppImgTemp, "joint_intensity.pgm" );
			//output_float_to_text( ppImgTemp, "joint_intensity.txt" );

			//output_float( ppImg1, "img1.pgm" );
			//output_float( ppImg2, "img2.pgm" );

			// Normalize
			float fNorm;
			float fNormMult;

			// Normalize marginal 1
			fNorm = 0;
			for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
			{
				fNorm += pfMarg1[class1];
			}
			assert( fNorm > 0 );
			fNormMult = 1.0f/fNorm;
			for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
			{
				pfMarg1[class1] *= fNormMult;
			}

			// Normalize marginal 2
			fNorm = 0;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				fNorm += pfMarg2[class2];
			}
			assert( fNorm > 0 );
			fNormMult = 1.0f/fNorm;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				pfMarg2[class2] *= fNormMult;
			}

			// Normalize joint
			fNorm = 0;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
				{
					fNorm += pfJoint[class2*fio1.iFeaturesPerVector + class1];
				}
			}
			assert( fNorm > 0 );
			fNormMult = 1.0f/fNorm;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
				{
					pfJoint[class2*fio1.iFeaturesPerVector + class1] *= fNormMult;
				}
			}

			// Calculate entropies

			float fProbSum;

			fProbSum = 0.0f;
			float fMarg1Entropy = 0;
			float fMarg1Entropy2 = 0;
			for( rr = 0; rr < fioMarg1.x; rr++ )
			{
				int iIndex = pfMarg1[rr]*LOOKUP_N;
				fMarg1Entropy += g_fPlogP[iIndex];

				if( pfMarg1[rr] > 0 )
				{
					float fProb = pfMarg1[rr];
					fProbSum += fProb;
					fMarg1Entropy2 -= (float)(fProb*log(fProb));
				}
			}

			fProbSum = 0.0f;
			float fMarg2Entropy = 0;
			float fMarg2Entropy2 = 0;
			for( rr = 0; rr < fioMarg2.x; rr++ )
			{
				int iIndex = pfMarg2[rr]*LOOKUP_N;
				fMarg2Entropy += g_fPlogP[iIndex];

				if( pfMarg2[rr] > 0 )
				{
					float fProb = pfMarg2[rr];
					fProbSum += fProb;
					fMarg2Entropy2 -= (float)(fProb*log(fProb));
				}
			}

			fProbSum = 0.0f;
			float fJointEntropy = 0;
			float fJointEntropy2 = 0;
			for( rr = 0; rr < fio2.iFeaturesPerVector; rr++ )
			{
				for( cc = 0; cc < fio1.iFeaturesPerVector; cc++ )
				{
					int iIndex = pfJoint[rr*fio1.iFeaturesPerVector + cc]*LOOKUP_N;
					fJointEntropy += g_fPlogP[iIndex];

					if( pfJoint[rr*fio1.iFeaturesPerVector + cc] > 0 )
					{
						float fProb = pfJoint[rr*fio1.iFeaturesPerVector + cc];
						fProbSum += fProb;
						fJointEntropy2 -= (float)(fProb*log(fProb));
					}
				}
			}

			fioMI.pfVectors[r*fioMI.x + c] = fMarg1Entropy + fMarg2Entropy - fJointEntropy;
			//fioMI.pfVectors[r*fioMI.x + c] = fMarg1Entropy2 + fMarg2Entropy2 - fJointEntropy2;
		}
	}

	fioDelete( fioMarg1 );
	fioDelete( fioMarg2 );
	fioDelete( fioJoint );

	return 1;
}


int
hfGenerateProbMILikelihoodImageLookupTableHyperGeometric(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fio1,
					   FEATUREIO &fio2,
					   FEATUREIO &fioMI,
						float *pfPrior
							)
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fio1.iFeaturesPerVector;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	FEATUREIO fioMarg2;
	fioMarg2.iFeaturesPerVector = 1;
	fioMarg2.z = 1;
	fioMarg2.x = fio2.iFeaturesPerVector;
	fioMarg2.y = 1;
	fioMarg2.t = 1;
	fioAllocate( fioMarg2 );
	float *pfMarg2 = fioMarg2.pfVectors;

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 1;
	fioJoint.x = fioMarg1.x;
	fioJoint.y = fioMarg2.x;
	fioJoint.t = fioMarg2.x;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	PpImage ppImgTemp;
	fioimgInitReference( ppImgTemp, fioJoint );

	int iSize = iWindowRows*iWindowCols;

	// Init lookup tables
	for( int i = 0; i < LOOKUP_N+1; i++ )
	{
		if( i == 0 )
		{
			g_fPlogP[i] = 0;
			g_flogNFact[i] = 1;
		}
		else
		{
			float fProb = i / (float)LOOKUP_N;
			g_fPlogP[i] = -fProb*log(fProb);
			g_flogNFact[i] = g_flogNFact[i-1] + log((float)i);
		}
	}

	PpImage ppImg1;
	//ppImg1.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );
	PpImage ppImg2;
	//ppImg2.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );

	int iRowStart = iRow - iWindowRows/2;
	if( iRowStart < 0 )
	{
		iRowStart = 0;
	}
	else if( iRowStart + iWindowRows > fio1.y )
	{
		iRowStart = fio1.y - iWindowRows;
	}

	int iColStart = iCol - iWindowCols/2;
	if( iColStart < 0 )
	{
		iColStart = 0;
	}
	else if( iColStart + iWindowCols > fio1.x )
	{
		iColStart = fio1.x - iWindowCols;
	}

	float *pfWind1 = fio1.pfVectors + (iRowStart*fio1.x + iColStart)*fio1.iFeaturesPerVector;

	for( int r = 0; r < fio2.y; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fio2.y )
		{
			iRowStart = fio2.y - iWindowRows;
		}

		//printf( "Row: %d\n", r );
		for( int c = 0; c < fio2.x; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fio2.x )
			{
				iColStart = fio2.x - iWindowCols;
			}

			int class1, class2;

			float *pfWind2 = fio2.pfVectors + (iRowStart*fio2.x + iColStart)*fio2.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			if( pfPrior )
			{
				memcpy( pfJoint, pfPrior, sizeof(float)*fioJoint.x*fioJoint.y );
			}
			else
			{
				memset( pfJoint, 0, sizeof(float)*fioJoint.x*fioJoint.y );
			}
			//memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );
			//memset( pfMarg2, 0, sizeof(float)*fioMarg2.x*fioMarg2.y );

			// Generate joint probability table
			int rr, cc;
			
			// Sum over location  alignments
			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					// Sum over class probabilities (ouch, this is painfull)

					float *pfProbs1 = pfWind1 + (rr*fio1.x + cc)*fio1.iFeaturesPerVector;
					float *pfProbs2 = pfWind2 + (rr*fio2.x + cc)*fio2.iFeaturesPerVector;

					//// Sum up maginal 1
					//for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					//{
					//	pfMarg1[class1] += pfProbs1[class1];
					//}

					//// Sum up marginal 2
					//for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					//{
					//	pfMarg2[class2] += pfProbs2[class2];
					//}

					// Sum up joint, assume independence
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
						{
							pfJoint[class2*fio1.iFeaturesPerVector + class1] +=
								pfProbs1[class1]*pfProbs2[class2];
						}
					}
				}
			}
			memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );
			memset( pfMarg2, 0, sizeof(float)*fioMarg2.x*fioMarg2.y );

			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
				{
					pfMarg1[class1] += pfJoint[class2*fio1.iFeaturesPerVector + class1];
					pfMarg2[class2] += pfJoint[class2*fio1.iFeaturesPerVector + class1];
				}
			}


			//output_float( ppImgTemp, "joint_intensity.pgm" );
			//output_float_to_text( ppImgTemp, "joint_intensity.txt" );

			//output_float( ppImg1, "img1.pgm" );
			//output_float( ppImg2, "img2.pgm" );

			// Normalize
			float fNorm;
			float fNormMult;

			// Normalize marginal 1
			fNorm = 0;
			for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
			{
				fNorm += pfMarg1[class1];
			}
			assert( fNorm > 0 );
			fNormMult = 1.0f/fNorm;
			for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
			{
				pfMarg1[class1] *= fNormMult;
			}

			// Normalize marginal 2
			fNorm = 0;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				fNorm += pfMarg2[class2];
			}
			assert( fNorm > 0 );
			fNormMult = 1.0f/fNorm;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				pfMarg2[class2] *= fNormMult;
			}

			// Normalize joint
			fNorm = 0;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
				{
					fNorm += pfJoint[class2*fio1.iFeaturesPerVector + class1];
				}
			}
			assert( fNorm > 0 );
			fNormMult = 1.0f/fNorm;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
				{
					pfJoint[class2*fio1.iFeaturesPerVector + class1] *= fNormMult;
				}
			}

			// Calculate entropies

			float fProbSum;

			fProbSum = 0.0f;
			float fMarg1Entropy = 0;
			float fMarg1Entropy2 = 0;
			for( rr = 0; rr < fioMarg1.x; rr++ )
			{
				int iIndex = pfMarg1[rr]*LOOKUP_N;
 				fMarg1Entropy += g_fPlogP[iIndex];
				fMarg1Entropy2 += g_flogNFact[iIndex];

				//if( pfMarg1[rr] > 0 )
				//{
				//	float fProb = pfMarg1[rr];
				//	fProbSum += fProb;
				//	fMarg1Entropy2 -= (float)(fProb*log(fProb));
				//}
			}

			fProbSum = 0.0f;
			float fMarg2Entropy = 0;
			float fMarg2Entropy2 = 0;
			for( rr = 0; rr < fioMarg2.x; rr++ )
			{
				int iIndex = pfMarg2[rr]*LOOKUP_N;
				fMarg2Entropy += g_fPlogP[iIndex];
				fMarg2Entropy2 += g_flogNFact[iIndex];

				//if( pfMarg2[rr] > 0 )
				//{
				//	float fProb = pfMarg2[rr];
				//	fProbSum += fProb;
				//	fMarg2Entropy2 -= (float)(fProb*log(fProb));
				//}
			}

			fProbSum = 0.0f;
			float fJointEntropy = 0;
			float fJointEntropy2 = 0;
			for( rr = 0; rr < fio2.iFeaturesPerVector; rr++ )
			{
				for( cc = 0; cc < fio1.iFeaturesPerVector; cc++ )
				{
					int iIndex = pfJoint[rr*fio1.iFeaturesPerVector + cc]*LOOKUP_N;
					fJointEntropy += g_fPlogP[iIndex];
					fJointEntropy2 += g_flogNFact[iIndex];

					//if( pfJoint[rr*fio1.iFeaturesPerVector + cc] > 0 )
					//{
					//	float fProb = pfJoint[rr*fio1.iFeaturesPerVector + cc];
					//	fProbSum += fProb;
					//	fJointEntropy2 -= (float)(fProb*log(fProb));
					//}
				}
			}

			fioMI.pfVectors[r*fioMI.x + c] = fJointEntropy2 + g_flogNFact[iSize] - fMarg1Entropy2 - fMarg2Entropy2;
			//fioMI.pfVectors[r*fioMI.x + c] = fMarg1Entropy2 + fMarg2Entropy2 - fJointEntropy2 - g_flogNFact[LOOKUP_N];
			//fioMI.pfVectors[r*fioMI.x + c] = fMarg1Entropy + fMarg2Entropy - fJointEntropy;
			//fioMI.pfVectors[r*fioMI.x + c] = fMarg1Entropy + fMarg2Entropy - fJointEntropy;
		}
	}

	fioDelete( fioMarg1 );
	fioDelete( fioMarg2 );
	fioDelete( fioJoint );

	return 1;
}

int
hfGenerateProbMILikelihoodImageHyperGeometricIntergerCountsNonNormalized(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fio1,
					   FEATUREIO &fio2,
					   FEATUREIO &fioMI,
						float *pfPrior
							)
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fio1.iFeaturesPerVector;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	FEATUREIO fioMarg2;
	fioMarg2.iFeaturesPerVector = 1;
	fioMarg2.z = 1;
	fioMarg2.x = fio2.iFeaturesPerVector;
	fioMarg2.y = 1;
	fioMarg2.t = 1;
	fioAllocate( fioMarg2 );
	float *pfMarg2 = fioMarg2.pfVectors;

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 1;
	fioJoint.x = fioMarg1.x;
	fioJoint.y = fioMarg2.x;
	fioJoint.t = fioMarg2.x;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	PpImage ppImgTemp;
	fioimgInitReference( ppImgTemp, fioJoint );

	PpImage ppImg1;
	//ppImg1.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );
	PpImage ppImg2;
	//ppImg2.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );

	int iTotalSamples = iWindowRows*iWindowCols;

	// Init lookup tables
	for( int i = 0; i < LOOKUP_N+1; i++ )
	{
		if( i == 0 )
		{
			g_flogNFact[i] = 0;
		}
		else
		{
			g_flogNFact[i] = g_flogNFact[i-1] + log((float)i);
		}
	}

	int iRowStart = iRow - iWindowRows/2;
	if( iRowStart < 0 )
	{
		iRowStart = 0;
	}
	else if( iRowStart + iWindowRows > fio1.y )
	{
		iRowStart = fio1.y - iWindowRows;
	}

	int iColStart = iCol - iWindowCols/2;
	if( iColStart < 0 )
	{
		iColStart = 0;
	}
	else if( iColStart + iWindowCols > fio1.x )
	{
		iColStart = fio1.x - iWindowCols;
	}

	float *pfWind1 = fio1.pfVectors + (iRowStart*fio1.x + iColStart)*fio1.iFeaturesPerVector;

	for( int r = 0; r < fio2.y; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fio2.y )
		{
			iRowStart = fio2.y - iWindowRows;
		}

		//printf( "Row: %d\n", r );
		for( int c = 0; c < fio2.x; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fio2.x )
			{
				iColStart = fio2.x - iWindowCols;
			}

			int class1, class2;

			float *pfWind2 = fio2.pfVectors + (iRowStart*fio2.x + iColStart)*fio2.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			if( pfPrior )
			{
				memcpy( pfJoint, pfPrior, sizeof(float)*fioJoint.x*fioJoint.y );
			}
			else
			{
				memset( pfJoint, 0, sizeof(float)*fioJoint.x*fioJoint.y );
			}
			memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );
			memset( pfMarg2, 0, sizeof(float)*fioMarg2.x*fioMarg2.y );

			// Generate joint probability table
			int rr, cc;
			
			// Sum over location  alignments
			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					// Sum over class probabilities (ouch, this is painfull)

					float *pfProbs1 = pfWind1 + (rr*fio1.x + cc)*fio1.iFeaturesPerVector;
					float *pfProbs2 = pfWind2 + (rr*fio2.x + cc)*fio2.iFeaturesPerVector;

					// Find maginal 1
					int iIndex1=-1;
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						if( pfProbs1[class1] == 1 )
						{
							pfMarg1[class1]++;
							iIndex1 = class1;
							break;
						}
					}
					assert( iIndex1 >= 0 );

					// Find marginal 2
					int iIndex2=-1;
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						if( pfProbs2[class2] == 1 )
						{
							pfMarg2[class2]++;
							iIndex2 = class2;
							break;
						}
					}
					assert( iIndex2 >= 0 );

					pfJoint[iIndex2*fio1.iFeaturesPerVector + iIndex1]++;
				}
			}

			//output_float( ppImgTemp, "joint_intensity.pgm" );
			//output_float_to_text( ppImgTemp, "joint_intensity.txt" );

			//output_float( ppImg1, "img1.pgm" );
			//output_float( ppImg2, "img2.pgm" );

			// Calculate entropies

			float fMarg1Entropy = 0;
			for( rr = 0; rr < fioMarg1.x; rr++ )
			{
				int iCount = pfMarg1[rr];
				fMarg1Entropy += g_flogNFact[iCount];
			}

			float fMarg2Entropy = 0;
			for( rr = 0; rr < fioMarg2.x; rr++ )
			{
				int iCount = pfMarg2[rr];
				fMarg2Entropy += g_flogNFact[iCount];
			}

			float fJointEntropy = 0;
			for( rr = 0; rr < fio2.iFeaturesPerVector; rr++ )
			{
				for( cc = 0; cc < fio1.iFeaturesPerVector; cc++ )
				{
					int iCount = pfJoint[rr*fio1.iFeaturesPerVector + cc];
					fJointEntropy += g_flogNFact[iCount];
				}
			}

			fioMI.pfVectors[r*fioMI.x + c] = (fJointEntropy + g_flogNFact[iTotalSamples]) - (fMarg1Entropy + fMarg2Entropy);
		}
	}

	fioDelete( fioMarg1 );
	fioDelete( fioMarg2 );
	fioDelete( fioJoint );

	return 1;
}

float
hfCalculateRegionEntropyLookupTableHyperGeometric(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fio1,
					   FEATUREIO &fio2,
					   FEATUREIO &fioMI,
						float *pfPrior
							)
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fio1.iFeaturesPerVector;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	FEATUREIO fioMarg2;
	fioMarg2.iFeaturesPerVector = 1;
	fioMarg2.z = 1;
	fioMarg2.x = fio2.iFeaturesPerVector;
	fioMarg2.y = 1;
	fioMarg2.t = 1;
	fioAllocate( fioMarg2 );
	float *pfMarg2 = fioMarg2.pfVectors;

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 1;
	fioJoint.x = fioMarg1.x;
	fioJoint.y = fioMarg2.x;
	fioJoint.t = fioMarg2.x;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	PpImage ppImgTemp;
	fioimgInitReference( ppImgTemp, fioJoint );

	// Init lookup tables
	for( int i = 0; i < LOOKUP_N+1; i++ )
	{
		if( i == 0 )
		{
			g_fPlogP[i] = 0;
			g_flogNFact[i] = 0;
		}
		else
		{
			float fProb = i / (float)LOOKUP_N;
			g_fPlogP[i] = -fProb*log(fProb);
			g_flogNFact[i] = g_flogNFact[i-1] + log((float)i);
		}
	}

	PpImage ppImg1;
	//ppImg1.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );
	PpImage ppImg2;
	//ppImg2.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );

	int iRowStart = iRow - iWindowRows/2;
	if( iRowStart < 0 )
	{
		iRowStart = 0;
	}
	else if( iRowStart + iWindowRows > fio1.y )
	{
		iRowStart = fio1.y - iWindowRows;
	}

	int iColStart = iCol - iWindowCols/2;
	if( iColStart < 0 )
	{
		iColStart = 0;
	}
	else if( iColStart + iWindowCols > fio1.x )
	{
		iColStart = fio1.x - iWindowCols;
	}

	float *pfWind1 = fio1.pfVectors + (iRowStart*fio1.x + iColStart)*fio1.iFeaturesPerVector;

	for( int r = 0; r < fio2.y; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fio2.y )
		{
			iRowStart = fio2.y - iWindowRows;
		}

		//printf( "Row: %d\n", r );
		for( int c = 0; c < fio2.x; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fio2.x )
			{
				iColStart = fio2.x - iWindowCols;
			}

			int class1, class2;

			float *pfWind2 = fio2.pfVectors + (iRowStart*fio2.x + iColStart)*fio2.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			if( pfPrior )
			{
				memcpy( pfJoint, pfPrior, sizeof(float)*fioJoint.x*fioJoint.y );
			}
			else
			{
				memset( pfJoint, 0, sizeof(float)*fioJoint.x*fioJoint.y );
			}
			//memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );
			//memset( pfMarg2, 0, sizeof(float)*fioMarg2.x*fioMarg2.y );

			// Generate joint probability table
			int rr, cc;
			
			// Sum over location  alignments
			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					// Sum over class probabilities (ouch, this is painfull)

					float *pfProbs1 = pfWind1 + (rr*fio1.x + cc)*fio1.iFeaturesPerVector;
					float *pfProbs2 = pfWind2 + (rr*fio2.x + cc)*fio2.iFeaturesPerVector;

					//// Sum up maginal 1
					//for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					//{
					//	pfMarg1[class1] += pfProbs1[class1];
					//}

					//// Sum up marginal 2
					//for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					//{
					//	pfMarg2[class2] += pfProbs2[class2];
					//}

					// Sum up joint, assume independence
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
						{
							pfJoint[class2*fio1.iFeaturesPerVector + class1] +=
								pfProbs1[class1]*pfProbs2[class2];
						}
					}
				}
			}
			memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );
			memset( pfMarg2, 0, sizeof(float)*fioMarg2.x*fioMarg2.y );

			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
				{
					pfMarg1[class1] += pfJoint[class2*fio1.iFeaturesPerVector + class1];
					pfMarg2[class2] += pfJoint[class2*fio1.iFeaturesPerVector + class1];
				}
			}


			//output_float( ppImgTemp, "joint_intensity.pgm" );
			//output_float_to_text( ppImgTemp, "joint_intensity.txt" );

			//output_float( ppImg1, "img1.pgm" );
			//output_float( ppImg2, "img2.pgm" );

			// Normalize
			float fNorm;
			float fNormMult;

			// Normalize marginal 1
			fNorm = 0;
			for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
			{
				fNorm += pfMarg1[class1];
			}
			assert( fNorm > 0 );
			fNormMult = 1.0f/fNorm;
			for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
			{
				pfMarg1[class1] *= fNormMult;
			}

			// Normalize marginal 2
			fNorm = 0;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				fNorm += pfMarg2[class2];
			}
			assert( fNorm > 0 );
			fNormMult = 1.0f/fNorm;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				pfMarg2[class2] *= fNormMult;
			}

			// Normalize joint
			fNorm = 0;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
				{
					fNorm += pfJoint[class2*fio1.iFeaturesPerVector + class1];
				}
			}
			assert( fNorm > 0 );
			fNormMult = 1.0f/fNorm;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
				{
					pfJoint[class2*fio1.iFeaturesPerVector + class1] *= fNormMult;
				}
			}

			// Calculate entropies

			float fProbSum;

			fProbSum = 0.0f;
			float fMarg1Entropy = 0;
			float fMarg1Entropy2 = 0;
			for( rr = 0; rr < fioMarg1.x; rr++ )
			{
				int iIndex = pfMarg1[rr]*LOOKUP_N;
 				fMarg1Entropy += g_fPlogP[iIndex];
				fMarg1Entropy2 += g_flogNFact[iIndex];

				//if( pfMarg1[rr] > 0 )
				//{
				//	float fProb = pfMarg1[rr];
				//	fProbSum += fProb;
				//	fMarg1Entropy2 -= (float)(fProb*log(fProb));
				//}
			}

			fProbSum = 0.0f;
			float fMarg2Entropy = 0;
			float fMarg2Entropy2 = 0;
			for( rr = 0; rr < fioMarg2.x; rr++ )
			{
				int iIndex = pfMarg2[rr]*LOOKUP_N;
				fMarg2Entropy += g_fPlogP[iIndex];
				fMarg2Entropy2 += g_flogNFact[iIndex];

				//if( pfMarg2[rr] > 0 )
				//{
				//	float fProb = pfMarg2[rr];
				//	fProbSum += fProb;
				//	fMarg2Entropy2 -= (float)(fProb*log(fProb));
				//}
			}

			fProbSum = 0.0f;
			float fJointEntropy = 0;
			float fJointEntropy2 = 0;
			for( rr = 0; rr < fio2.iFeaturesPerVector; rr++ )
			{
				for( cc = 0; cc < fio1.iFeaturesPerVector; cc++ )
				{
					int iIndex = pfJoint[rr*fio1.iFeaturesPerVector + cc]*LOOKUP_N;
					fJointEntropy += g_fPlogP[iIndex];
					fJointEntropy2 += g_flogNFact[iIndex];

					//if( pfJoint[rr*fio1.iFeaturesPerVector + cc] > 0 )
					//{
					//	float fProb = pfJoint[rr*fio1.iFeaturesPerVector + cc];
					//	fProbSum += fProb;
					//	fJointEntropy2 -= (float)(fProb*log(fProb));
					//}
				}
			}

			//fioMI.pfVectors[r*fioMI.x + c] = fMarg1Entropy2 + fMarg2Entropy2 - fJointEntropy2 - g_flogNFact[LOOKUP_N];
			//fioMI.pfVectors[r*fioMI.x + c] = fMarg1Entropy + fMarg2Entropy - fJointEntropy;
			fioMI.pfVectors[r*fioMI.x + c] = fMarg1Entropy + fMarg2Entropy - fJointEntropy;

			
	fioDelete( fioMarg1 );
	fioDelete( fioMarg2 );
	fioDelete( fioJoint );

	return fMarg1Entropy;


		}
	}

	return -9999;
}

int
hfGenerateMILikelihoodImageLookupTable(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fioImg1,
					   FEATUREIO &fioImg2,
					   FEATUREIO &fioRanges1,
					   FEATUREIO &fioRanges2,
					   FEATUREIO &fioMI,
					   int iInitNoiseLevel,	// The initial uncertainty added to
											// each entry of the joint.
					   int iSubStartRow,
					   int iSubStartCol,
					   int iSubStartRows,
					   int iSubStartCols
							)
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fioRanges1.iFeaturesPerVector + 1;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	FEATUREIO fioMarg2;
	fioMarg2.iFeaturesPerVector = 1;
	fioMarg2.z = 1;
	fioMarg2.x = fioRanges2.iFeaturesPerVector + 1;
	fioMarg2.y = 1;
	fioMarg2.t = 1;
	fioAllocate( fioMarg2 );
	float *pfMarg2 = fioMarg2.pfVectors;

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 1;
	fioJoint.x = fioMarg1.x;
	fioJoint.y = fioMarg2.x;
	fioJoint.t = 1;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	int iJointSum = fioMarg1.x*fioMarg2.x*iInitNoiseLevel + iWindowRows*iWindowCols;

	// Init lookup tables
	for( int i = 0; i < LOOKUP_N+1; i++ )
	{
		if( i == 0 )
		{
			g_fPlogP[i] = 0;
			g_flogNFact[i] = 0;
		}
		else
		{
			// Here, unlike with probabilistic intensity classes, 
			float fProb = i / (float)iJointSum;
			g_fPlogP[i] = -fProb*log(fProb);
			g_flogNFact[i] = g_flogNFact[i-1] + log((float)i);
		}
	}

	PpImage ppImgTemp;
	fioimgInitReference( ppImgTemp, fioJoint );

	PpImage ppImg1;
	//ppImg1.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );
	PpImage ppImg2;
	//ppImg2.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );

	int iRowStart = iRow - iWindowRows/2;
	if( iRowStart < 0 )
	{
		iRowStart = 0;
	}
	else if( iRowStart + iWindowRows > fioImg1.y )
	{
		iRowStart = fioImg1.y - iWindowRows;
	}

	int iColStart = iCol - iWindowCols/2;
	if( iColStart < 0 )
	{
		iColStart = 0;
	}
	else if( iColStart + iWindowCols > fioImg1.x )
	{
		iColStart = fioImg1.x - iWindowCols;
	}

	float *pfWind1 = fioImg1.pfVectors + iRowStart*fioImg1.x + iColStart;
	float *pfRanges1 = fioRanges1.pfVectors + (iRow*fioRanges1.x + iCol)*fioRanges1.iFeaturesPerVector;

	iSubStartRow  = ( iSubStartRow  < 0 ? 0         : iSubStartRow  );
	iSubStartCol  = ( iSubStartCol  < 0 ? 0         : iSubStartCol  );
	iSubStartRows = ( iSubStartRow + iSubStartRows > fioImg2.y ? fioImg2.y : iSubStartRow + iSubStartRows );
	iSubStartCols = ( iSubStartCol + iSubStartCols > fioImg2.x ? fioImg2.x : iSubStartCol + iSubStartCols );

	for( int r = iSubStartRow; r < iSubStartRows; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fioImg2.y )
		{
			iRowStart = fioImg2.y - iWindowRows;
		}

		printf( "Row: %d\n", r );
		for( int c = iSubStartCol; c < iSubStartCols; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fioImg2.x )
			{
				iColStart = fioImg2.x - iWindowCols;
			}

			float *pfWind2 = fioImg2.pfVectors + iRowStart*fioImg2.x + iColStart;
			float *pfRanges2 = fioRanges2.pfVectors + (r*fioRanges2.x + c)*fioRanges2.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			memset( pfJoint, 0, sizeof(float)*fioJoint.x*fioJoint.y );
			memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );
			memset( pfMarg2, 0, sizeof(float)*fioMarg2.x*fioMarg2.y );

			int rr, cc;

			// Add noise
			for( rr = 0; rr < fioMarg1.x; rr++ )
			{
				pfMarg1[rr] = iInitNoiseLevel*fioMarg1.x;
			}
			for( rr = 0; rr < fioMarg2.x; rr++ )
			{
				pfMarg2[rr] = iInitNoiseLevel*fioMarg2.x;
			}
			for( rr = 0; rr < (fioRanges1.iFeaturesPerVector+1); rr++ )
			{
				for( cc = 0; cc < (fioRanges2.iFeaturesPerVector+1); cc++ )
				{
					pfJoint[rr*(fioRanges2.iFeaturesPerVector+1) + cc]
						= iInitNoiseLevel;
				}
			}

			// Generate joint probability table

			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					float fPix1 = pfWind1[rr*fioImg1.x + cc];
					float fPix2 = pfWind2[rr*fioImg2.x + cc];

					int iIndex1 = hfHistogramBin( fPix1, pfRanges1, fioRanges1.iFeaturesPerVector );
					int iIndex2 = hfHistogramBin( fPix2, pfRanges2, fioRanges2.iFeaturesPerVector );

					//((float*)(ppImg1.ImageRow(rr)))[cc] = iIndex1;
					//((float*)(ppImg2.ImageRow(rr)))[cc] = iIndex2;

					pfJoint[iIndex1*(fioRanges1.iFeaturesPerVector+1) + iIndex2]++;
					pfMarg1[iIndex1]++;
					pfMarg2[iIndex2]++;

					//iJointSum++;
				}
			}

			//output_float( ppImg1, "img1.pgm" );
			//output_float( ppImg2, "img2.pgm" );

			// Calculate entropies

			float fSum = 0;
			float fMarg1Entropy = 0;
			float fMarg1Entropy2 = 0;
			for( rr = 0; rr < fioMarg1.x; rr++ )
			{
				//if( pfMarg1[rr] > 0 )
				//{
				//	float fProb = pfMarg1[rr] / (float)iJointSum;
				//	fMarg1Entropy -= (float)(fProb*log(fProb));
				//}
				int iIndex = pfMarg1[rr];
				fMarg1Entropy += g_fPlogP[iIndex];
				fMarg1Entropy2 += g_flogNFact[iIndex];
				fSum += iIndex;
			}
			//fMarg1Entropy /= -log( fioMarg1.x );

			fSum = 0;
			float fMarg2Entropy = 0;
			float fMarg2Entropy2 = 0;
			for( rr = 0; rr < fioMarg2.x; rr++ )
			{
				//if( pfMarg2[rr] > 0 )
				//{
				//	float fProb = pfMarg2[rr] / (float)iJointSum;
				//	fMarg2Entropy -= (float)(fProb*log(fProb));
				//}
				
				int iIndex = pfMarg2[rr];
				fMarg2Entropy += g_fPlogP[iIndex];
				fMarg2Entropy2 += g_flogNFact[iIndex];
				fSum += iIndex;
			}
			//fMarg2Entropy /= -log( fioMarg2.x );

			fSum = 0;
			float fJointEntropy = 0;
			float fJointEntropy2 = 0;
			for( rr = 0; rr < (fioRanges1.iFeaturesPerVector+1); rr++ )
			{
				for( cc = 0; cc < (fioRanges2.iFeaturesPerVector+1); cc++ )
				{
					//if( pfJoint[rr*(fioRanges2.iFeaturesPerVector+1) + cc] > 0 )
					//{
					//	float fProb =
					//	pfJoint[rr*(fioRanges2.iFeaturesPerVector+1) + cc] / (float)iJointSum;
					//	fJointEntropy -= (float)(fProb*log(fProb));
					//}
					int iIndex = pfJoint[rr*(fioRanges2.iFeaturesPerVector+1) + cc];
					fJointEntropy += g_fPlogP[iIndex];
					fJointEntropy2 += g_flogNFact[iIndex];
					fSum += iIndex;
				}
			}

			//fioMI.pfVectors[r*fioMI.x + c] = fMarg1Entropy2 + fMarg2Entropy2 - fJointEntropy2 - g_flogNFact[iJointSum];
			fioMI.pfVectors[r*fioMI.x + c] = fMarg1Entropy + fMarg2Entropy - fJointEntropy;

			//			output_float( ppImgTemp, "joint_out.pgm" );
		}
	}

	fioDelete( fioMarg1 );
	fioDelete( fioMarg2 );
	fioDelete( fioJoint );

	return 1;
}



//
// hfGenerateProbMILikelihoodImageMasked()
//
// Same as hfGenerateProbMILikelihoodImage(), 
// but uses mask ppImgMask to keep/remove certain pixels.
//
int
hfGenerateProbMILikelihoodImageMasked(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fio1,
					   PpImage &ppImgMask, // Mask iWindowRows x iWindowCols
					   FEATUREIO &fio2,
					   FEATUREIO &fioMI
							)
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fio1.iFeaturesPerVector;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	FEATUREIO fioMarg2;
	fioMarg2.iFeaturesPerVector = 1;
	fioMarg2.z = 1;
	fioMarg2.x = fio2.iFeaturesPerVector;
	fioMarg2.y = 1;
	fioMarg2.t = 1;
	fioAllocate( fioMarg2 );
	float *pfMarg2 = fioMarg2.pfVectors;

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 1;
	fioJoint.x = fioMarg1.x;
	fioJoint.y = fioMarg2.x;
	fioJoint.t = fioMarg2.x;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	PpImage ppImgTemp;
	fioimgInitReference( ppImgTemp, fioJoint );

	PpImage ppImg1;
	//ppImg1.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );
	PpImage ppImg2;
	//ppImg2.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );

	int iRowStart = iRow - iWindowRows/2;
	if( iRowStart < 0 )
	{
		iRowStart = 0;
	}
	else if( iRowStart + iWindowRows > fio1.y )
	{
		iRowStart = fio1.y - iWindowRows;
	}

	int iColStart = iCol - iWindowCols/2;
	if( iColStart < 0 )
	{
		iColStart = 0;
	}
	else if( iColStart + iWindowCols > fio1.x )
	{
		iColStart = fio1.x - iWindowCols;
	}

	float *pfWind1 = fio1.pfVectors + (iRowStart*fio1.x + iColStart)*fio1.iFeaturesPerVector;

	for( int r = 0; r < fio2.y; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fio2.y )
		{
			iRowStart = fio2.y - iWindowRows;
		}

		printf( "Row: %d\n", r );
		for( int c = 0; c < fio2.x; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fio2.x )
			{
				iColStart = fio2.x - iWindowCols;
			}

			int class1, class2;

			float *pfWind2 = fio2.pfVectors + (iRowStart*fio2.x + iColStart)*fio2.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			memset( pfJoint, 0, sizeof(float)*fioJoint.x*fioJoint.y );
			memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );
			memset( pfMarg2, 0, sizeof(float)*fioMarg2.x*fioMarg2.y );

			// Generate joint probability table
			int rr, cc;
			
			// Sum over location  alignments
			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					// Sum over class probabilities (ouch, this is painfull)

					if( ppImgMask.ImageRow(rr)[cc] == 0 )
					{
						// Not in mask, don't process
						continue;
					}

					float *pfProbs1 = pfWind1 + (rr*fio1.x + cc)*fio1.iFeaturesPerVector;
					float *pfProbs2 = pfWind2 + (rr*fio2.x + cc)*fio2.iFeaturesPerVector;

					// Sum up maginal 1
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						pfMarg1[class1] += pfProbs1[class1];
					}

					// Sum up marginal 2
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						pfMarg2[class2] += pfProbs2[class2];
					}

					// Sum up joint, assume independence
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
						{
							pfJoint[class2*fio1.iFeaturesPerVector + class1] +=
								pfProbs1[class1]*pfProbs2[class2];
						}
					}
				}
			}

			//output_float( ppImgTemp, "joint_intensity.pgm" );
			//output_float_to_text( ppImgTemp, "joint_intensity.txt" );

			//output_float( ppImg1, "img1.pgm" );
			//output_float( ppImg2, "img2.pgm" );

			// Normalize
			float fNorm;
			float fNormMult;

			// Normalize marginal 1
			fNorm = 0;
			for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
			{
				fNorm += pfMarg1[class1];
			}
			assert( fNorm > 0 );
			fNormMult = 1.0f/fNorm;
			for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
			{
				pfMarg1[class1] *= fNormMult;
			}

			// Normalize marginal 2
			fNorm = 0;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				fNorm += pfMarg2[class2];
			}
			assert( fNorm > 0 );
			fNormMult = 1.0f/fNorm;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				pfMarg2[class2] *= fNormMult;
			}

			// Normalize joint
			fNorm = 0;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
				{
					fNorm += pfJoint[class2*fio1.iFeaturesPerVector + class1];
				}
			}
			assert( fNorm > 0 );
			fNormMult = 1.0f/fNorm;
			for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
			{
				for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
				{
					pfJoint[class2*fio1.iFeaturesPerVector + class1] *= fNormMult;
				}
			}

			// Calculate entropies

			float fProbSum;

			fProbSum = 0.0f;
			float fMarg1Entropy = 0;
			for( rr = 0; rr < fioMarg1.x; rr++ )
			{
				if( pfMarg1[rr] > 0 )
				{
					float fProb = pfMarg1[rr];
					fProbSum += fProb;
					fMarg1Entropy -= (float)(fProb*log(fProb));
				}
			}

			fProbSum = 0.0f;
			float fMarg2Entropy = 0;
			for( rr = 0; rr < fioMarg2.x; rr++ )
			{
				if( pfMarg2[rr] > 0 )
				{
					float fProb = pfMarg2[rr];
					fProbSum += fProb;
					fMarg2Entropy -= (float)(fProb*log(fProb));
				}
			}

			fProbSum = 0.0f;
			float fJointEntropy = 0;
			for( rr = 0; rr < fio2.iFeaturesPerVector; rr++ )
			{
				for( cc = 0; cc < fio1.iFeaturesPerVector; cc++ )
				{
					if( pfJoint[rr*fio1.iFeaturesPerVector + cc] > 0 )
					{
						float fProb = pfJoint[rr*fio1.iFeaturesPerVector + cc];
						fProbSum += fProb;
						fJointEntropy -= (float)(fProb*log(fProb));
					}
				}
			}

			fioMI.pfVectors[r*fioMI.x + c] = fMarg1Entropy + fMarg2Entropy - fJointEntropy;
		}
	}

	fioDelete( fioMarg1 );
	fioDelete( fioMarg2 );
	fioDelete( fioJoint );

	return 1;
}

//
// hfGenerateProbMI_ConditionalMarkov4_LikelihoodImage()
//
// Year: 2008
// Investigate entropy rate of aligned images as a measure of similarity
//
// Markov dependency between adjacent samples is assumed.
//
// In the case of independent and identically distributed samples, the
// entropy rate is equal to the joint entropy.
//
//
int
hfGenerateProbMI_ConditionalMarkov4_LikelihoodImage(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fio1,
					   FEATUREIO &fio2,
					   FEATUREIO &fioHRate
							)
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fio1.iFeaturesPerVector*fio2.iFeaturesPerVector; // Size is the joint
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	// Temporary vectors for time
	float *pfT1 = new float[fioMarg1.x];
	float *pfT2 = new float[fioMarg1.x];

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 1;
	fioJoint.x = fioMarg1.x;
	fioJoint.y = fioMarg1.x;
	fioJoint.t = 1;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	PpImage ppImgTemp;
	fioimgInitReference( ppImgTemp, fioJoint );

	PpImage ppImg1;
	//ppImg1.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );
	PpImage ppImg2;
	//ppImg2.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );

	int iRowStart = iRow - iWindowRows/2;
	if( iRowStart < 0 )
	{
		iRowStart = 0;
	}
	else if( iRowStart + iWindowRows > fio1.y )
	{
		iRowStart = fio1.y - iWindowRows;
	}

	int iColStart = iCol - iWindowCols/2;
	if( iColStart < 0 )
	{
		iColStart = 0;
	}
	else if( iColStart + iWindowCols > fio1.x )
	{
		iColStart = fio1.x - iWindowCols;
	}

	float *pfWind1 = fio1.pfVectors + (iRowStart*fio1.x + iColStart)*fio1.iFeaturesPerVector;

	for( int r = 0; r < fio2.y; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fio2.y )
		{
			iRowStart = fio2.y - iWindowRows;
		}

		printf( "Row: %d\n", r );
		for( int c = 0; c < fio2.x; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fio2.x )
			{
				iColStart = fio2.x - iWindowCols;
			}

			float *pfWind2 = fio2.pfVectors + (iRowStart*fio2.x + iColStart)*fio2.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			memset( pfJoint, 0, sizeof(float)*fioJoint.x*fioJoint.y );
			memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );

			// Generate joint probability table

			int rr, cc;
			int class1, class2;

			// Sum over location  alignments
			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					// Sum over class probabilities (ouch, this is painfull)

					float *pfProbs11 = pfWind1 + (rr*fio1.x + cc)*fio1.iFeaturesPerVector;
					float *pfProbs12 = pfWind2 + (rr*fio2.x + cc)*fio2.iFeaturesPerVector;
					//assert( pfProbs11[0]+pfProbs11[1]>0.99);
					//assert( pfProbs12[0]+pfProbs12[1]>0.99);

					// Generate state vector for time T1
					// Assume independent observations in image 1 and image 2
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
						{
							pfT1[class1*fio2.iFeaturesPerVector + class2] =
								pfProbs11[class1]*pfProbs12[class2];

							// Sum up marginal - is this indeed the stationary distribution??
							pfMarg1[class1*fio2.iFeaturesPerVector + class2] += 
								pfProbs11[class1]*pfProbs12[class2];
						}
					}

					//
					// Sum up joint for all possible transitions
					//
					// Consider right & down
					//

					// Right neighbour transition
					if( cc + 1 < iWindowCols )
					{
						float *pfProbs21 = pfWind1 + (rr*fio1.x + (cc+1) )*fio1.iFeaturesPerVector;
						float *pfProbs22 = pfWind2 + (rr*fio2.x + (cc+1) )*fio2.iFeaturesPerVector;

						// Generate state vector for time T2
						for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
						{
							for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
							{
								pfT2[class1*fio2.iFeaturesPerVector + class2] =
									pfProbs21[class1]*pfProbs22[class2];
							}
						}

						// Sum transition probabilities
						for( int class1 = 0; class1 < fioJoint.x; class1++ )
						{
							for( int class2 = 0; class2 < fioJoint.x; class2++ )
							{
								// Symetric transition prob
								pfJoint[class1*fioJoint.x + class2] += pfT1[class1]*pfT2[class2];
								pfJoint[class2*fioJoint.x + class1] += pfT1[class1]*pfT2[class2];
							}
						}
					}

					// Down neighbour transition
					if( rr + 1 < iWindowRows )
					{
						float *pfProbs21 = pfWind1 + ( (rr+1)*fio1.x + cc )*fio1.iFeaturesPerVector;
						float *pfProbs22 = pfWind2 + ( (rr+1)*fio1.x + cc )*fio2.iFeaturesPerVector;

						// Generate state vector for time T2
						for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
						{
							for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
							{
								pfT2[class1*fio2.iFeaturesPerVector + class2] =
									pfProbs21[class1]*pfProbs22[class2];
							}
						}

						// Sum transition probabilities
						for( int class1 = 0; class1 < fioJoint.x; class1++ )
						{
							for( int class2 = 0; class2 < fioJoint.x; class2++ )
							{
								// Symetric transition prob
								pfJoint[class1*fioJoint.x + class2] += pfT1[class1]*pfT2[class2];
								pfJoint[class2*fioJoint.x + class1] += pfT1[class1]*pfT2[class2];
							}
						}
					}
				}
			}

			//output_float( ppImgTemp, "joint_transition.pgm" );
			//output_float_to_text( ppImgTemp, "joint_transition.txt" );

			//output_float( ppImg1, "img1.pgm" );
			//output_float( ppImg2, "img2.pgm" );

			// Normalize rows of transition matrix
			for( int class1 = 0; class1 < fioJoint.x; class1++ )
			{
				float fNorm = 0;
				for( int class2 = 0; class2 < fioJoint.x; class2++ )
				{
					fNorm += pfJoint[class1*fioJoint.x + class2];
				}
				if( fNorm > 0 )
				{
					assert( fNorm > 0 );
					float fNormDiv = 1.0f/(float)fNorm;
					for( int class2 = 0; class2 < fioJoint.x; class2++ )
					{
						pfJoint[class1*fioJoint.x + class2] *= fNormDiv;
					}
				}
			}

			//output_float( ppImgTemp, "joint_transition_norm.pgm" );
			//output_float_to_text( ppImgTemp, "joint_transition_norm.txt" );

			// Normalize stationary distribution
			// Alternatively, calculate eigenvalues as below
			float fNorm = 0;
			for( int class1 = 0; class1 < fioMarg1.x; class1++ )
			{
				fNorm += pfMarg1[class1];
			}
			assert( fNorm > 0 );
			float fNormDiv = 1.0f/(float)fNorm;
			for( int class1 = 0; class1 < fioMarg1.x; class1++ )
			{
				pfMarg1[class1] *= fNormDiv;
			}

			if( 0 )
			{
				// Compute stationary matrix
				float w[4];
				float mat[4][4];
				float v[4][4];

				for( int class1 = 0; class1 < fioJoint.x; class1++ )
				{
					for( int class2 = 0; class2 < fioJoint.x; class2++ )
					{
						mat[class1][class2] = pfJoint[class1*fioJoint.x + class2];
					}
				}
				for( int class1 = 0; class1 < fioJoint.x; class1++ )
				{
					// Minus identity
					mat[class1][class1] -= 1.0f;
				}
				SingularValueDecomp<float, 4, 4>( mat, w, v );
				pfMarg1[0] = mat[0][0];
				pfMarg1[1] = mat[1][0];
				pfMarg1[2] = mat[2][0];
				pfMarg1[3] = mat[3][0];
				float stat_sum = pfMarg1[0]+pfMarg1[1]+pfMarg1[2]+pfMarg1[3];
				float stat_norm = 1.0f/stat_sum;
				pfMarg1[0] *= stat_norm;
				pfMarg1[1] *= stat_norm;
				pfMarg1[2] *= stat_norm;
				pfMarg1[3] *= stat_norm;
			}

			// Calculate information rate
			float fInformationRate = 0;
			for( rr = 0; rr < fioJoint.x; rr++ )
			{
				// Conditional probability
				float *pfJointCondProb = pfJoint + rr*fioJoint.x;

				// Calculate joint entropy
				float fJointEntropy = 0;
				float fProbSum = 0.0f;
				for( cc = 0; cc < fioJoint.x; cc++ )
				{
					float fProb = pfJointCondProb[cc];
					if( fProb > 0 )
					{
						fProbSum += fProb;
						fJointEntropy -= (float)(fProb*log(fProb));
					}
				}

				// Calculate marginal entropy 1
				float fMarg1Entropy = 0;
				fProbSum = 0;
				for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
				{
					float fProb = 0;
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						fProb += pfJointCondProb[class1*fio1.iFeaturesPerVector + class2];
					}
					if( fProb > 0 )
					{
						fProbSum += fProb;
						fMarg1Entropy -= (float)(fProb*log(fProb));
					}
				}

				// Calculate marginal entropy 2
				float fMarg2Entropy = 0;
				fProbSum = 0;
				for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
				{
					float fProb = 0;
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						fProb += pfJointCondProb[class1*fio1.iFeaturesPerVector + class2];
					}
					if( fProb > 0 )
					{
						fProbSum += fProb;
						fMarg2Entropy -= (float)(fProb*log(fProb));
					}
				}

				fInformationRate += pfMarg1[rr]*(fMarg1Entropy + fMarg2Entropy - fJointEntropy);
			}

			fioHRate.pfVectors[r*fioHRate.x + c] = fInformationRate;
		}
	}

	delete [] pfT1;
	delete [] pfT2;
	fioDelete( fioMarg1 );
	fioDelete( fioJoint );

	return 1;
}

int
hfGenerateProbMI_ConditionalMarkov1_LikelihoodImage(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fio1,
					   PpImage &ppImgN, // MRF neighbour indices in fio1
					   FEATUREIO &fio2,
					   FEATUREIO &fioHRate
							)
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fio1.iFeaturesPerVector*fio2.iFeaturesPerVector; // Size is the joint
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	// Temporary vectors for time
	float *pfT1 = new float[fioMarg1.x];
	float *pfT2 = new float[fioMarg1.x];

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 1;
	fioJoint.x = fioMarg1.x;
	fioJoint.y = fioMarg1.x;
	fioJoint.t = 1;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	PpImage ppImgTemp;
	fioimgInitReference( ppImgTemp, fioJoint );

	PpImage ppImg1;
	//ppImg1.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );
	PpImage ppImg2;
	//ppImg2.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );

	int iRowStart = iRow - iWindowRows/2;
	if( iRowStart < 0 )
	{
		iRowStart = 0;
	}
	else if( iRowStart + iWindowRows > fio1.y )
	{
		iRowStart = fio1.y - iWindowRows;
	}

	int iColStart = iCol - iWindowCols/2;
	if( iColStart < 0 )
	{
		iColStart = 0;
	}
	else if( iColStart + iWindowCols > fio1.x )
	{
		iColStart = fio1.x - iWindowCols;
	}

	float *pfWind1 = fio1.pfVectors + (iRowStart*fio1.x + iColStart)*fio1.iFeaturesPerVector;

	// Pointer to displacement to neighbours
	int *piN = ((int*)ppImgN.ImageRow(iRowStart)) + iColStart;
	piN = (int*)ppImgN.ImageRow(iRowStart);
	piN += iColStart;

	for( int r = 0; r < fio2.y; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fio2.y )
		{
			iRowStart = fio2.y - iWindowRows;
		}

		printf( "Row: %d\n", r );
		for( int c = 0; c < fio2.x; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fio2.x )
			{
				iColStart = fio2.x - iWindowCols;
			}

			float *pfWind2 = fio2.pfVectors + (iRowStart*fio2.x + iColStart)*fio2.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			memset( pfJoint, 0, sizeof(float)*fioJoint.x*fioJoint.y );
			memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );

			// Generate joint probability table

			int rr, cc;
			int class1, class2;

			// Sum over location  alignments
			for( rr = 1; rr < iWindowRows-1; rr++ )
			{
				for( cc = 1; cc < iWindowCols-1; cc++ )
				{
					// Sum over class probabilities (ouch, this is painfull)

					float *pfProbs11 = pfWind1 + (rr*fio1.x + cc)*fio1.iFeaturesPerVector;
					float *pfProbs12 = pfWind2 + (rr*fio2.x + cc)*fio2.iFeaturesPerVector;

					//assert( pfProbs11[0]+pfProbs11[1]>0.99);
					//assert( pfProbs12[0]+pfProbs12[1]>0.99);

					// Generate state vector for time T1
					// Assume independent observations in image 1 and image 2
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
						{
							pfT1[class1*fio2.iFeaturesPerVector + class2] =
								pfProbs11[class1]*pfProbs12[class2];

							// Sum up marginal - is this indeed the stationary distribution??
							pfMarg1[class1*fio2.iFeaturesPerVector + class2] += 
								pfProbs11[class1]*pfProbs12[class2];
						}
					}

					// Factor in neighbour - decode relative position
					int iIndexN = piN[rr*fio1.x + cc];
					signed char cYY = (iIndexN>>8)&0x000000FF;
					signed char cXX = (iIndexN>>0)&0x000000FF;

					float *pfProbs21 = pfWind1 + ((rr+cYY)*fio1.x + (cc+cXX))*fio1.iFeaturesPerVector;
					float *pfProbs22 = pfWind2 + ((rr+cYY)*fio2.x + (cc+cXX))*fio2.iFeaturesPerVector;

					// Generate state vector for time T2
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
						{
							pfT2[class1*fio2.iFeaturesPerVector + class2] =
								pfProbs21[class1]*pfProbs22[class2];
						}
					}

					// Sum transition probabilities
					for( int class1 = 0; class1 < fioJoint.x; class1++ )
					{
						for( int class2 = 0; class2 < fioJoint.x; class2++ )
						{
							// Symetric transition prob
							pfJoint[class1*fioJoint.x + class2] += pfT1[class1]*pfT2[class2];
							pfJoint[class2*fioJoint.x + class1] += pfT1[class1]*pfT2[class2];
						}
					}
				}
			}

			//output_float( ppImgTemp, "joint_transition.pgm" );
			//output_float_to_text( ppImgTemp, "joint_transition.txt" );

			//output_float( ppImg1, "img1.pgm" );
			//output_float( ppImg2, "img2.pgm" );

			// Normalize rows of transition matrix
			for( int class1 = 0; class1 < fioJoint.x; class1++ )
			{
				float fNorm = 0;
				for( int class2 = 0; class2 < fioJoint.x; class2++ )
				{
					fNorm += pfJoint[class1*fioJoint.x + class2];
				}
				if( fNorm > 0 )
				{
					assert( fNorm > 0 );
					float fNormDiv = 1.0f/(float)fNorm;
					for( int class2 = 0; class2 < fioJoint.x; class2++ )
					{
						pfJoint[class1*fioJoint.x + class2] *= fNormDiv;
					}
				}
			}

			//output_float( ppImgTemp, "joint_transition_norm.pgm" );
			//output_float_to_text( ppImgTemp, "joint_transition_norm.txt" );

			// Normalize stationary distribution
			// Alternatively, calculate eigenvalues as below
			float fNorm = 0;
			for( int class1 = 0; class1 < fioMarg1.x; class1++ )
			{
				fNorm += pfMarg1[class1];
			}
			assert( fNorm > 0 );
			float fNormDiv = 1.0f/(float)fNorm;
			for( int class1 = 0; class1 < fioMarg1.x; class1++ )
			{
				pfMarg1[class1] *= fNormDiv;
			}

			if( 1 )
			{
				// Compute stationary matrix
				float w[4];
				float mat[4][4];
				float v[4][4];

				for( int class1 = 0; class1 < fioJoint.x; class1++ )
				{
					for( int class2 = 0; class2 < fioJoint.x; class2++ )
					{
						mat[class1][class2] = pfJoint[class1*fioJoint.x + class2];
					}
				}
				for( int class1 = 0; class1 < fioJoint.x; class1++ )
				{
					// Minus identity
					mat[class1][class1] -= 1.0f;
				}
				SingularValueDecomp<float, 4, 4>( mat, w, v );
				pfMarg1[0] = mat[0][0];
				pfMarg1[1] = mat[1][0];
				pfMarg1[2] = mat[2][0];
				pfMarg1[3] = mat[3][0];
				float stat_sum = pfMarg1[0]+pfMarg1[1]+pfMarg1[2]+pfMarg1[3];
				float stat_norm = 1.0f/stat_sum;
				pfMarg1[0] *= stat_norm;
				pfMarg1[1] *= stat_norm;
				pfMarg1[2] *= stat_norm;
				pfMarg1[3] *= stat_norm;
			}

			// Calculate information rate
			float fInformationRate = 0;
			for( rr = 0; rr < fioJoint.x; rr++ )
			{
				// Conditional probability
				float *pfJointCondProb = pfJoint + rr*fioJoint.x;

				// Calculate joint entropy
				float fJointEntropy = 0;
				float fProbSum = 0.0f;
				for( cc = 0; cc < fioJoint.x; cc++ )
				{
					float fProb = pfJointCondProb[cc];
					if( fProb > 0 )
					{
						fProbSum += fProb;
						fJointEntropy -= (float)(fProb*log(fProb));
					}
				}

				// Calculate marginal entropy 1
				float fMarg1Entropy = 0;
				fProbSum = 0;
				for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
				{
					float fProb = 0;
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						fProb += pfJointCondProb[class1*fio1.iFeaturesPerVector + class2];
					}
					if( fProb > 0 )
					{
						fProbSum += fProb;
						fMarg1Entropy -= (float)(fProb*log(fProb));
					}
				}

				// Calculate marginal entropy 2
				float fMarg2Entropy = 0;
				fProbSum = 0;
				for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
				{
					float fProb = 0;
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						fProb += pfJointCondProb[class1*fio1.iFeaturesPerVector + class2];
					}
					if( fProb > 0 )
					{
						fProbSum += fProb;
						fMarg2Entropy -= (float)(fProb*log(fProb));
					}
				}

				fInformationRate += pfMarg1[rr]*(fMarg1Entropy + fMarg2Entropy - fJointEntropy);
			}

			if( fInformationRate < 0 )
			{
				fInformationRate = 0;
			}
			fioHRate.pfVectors[r*fioHRate.x + c] = fInformationRate;
		}
	}

	delete [] pfT1;
	delete [] pfT2;
	fioDelete( fioMarg1 );
	fioDelete( fioJoint );

	return 1;
}

int
hfGenerateProbMI_SpatialPattern_LikelihoodImage(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fio1,
					   PpImage &ppImgN, // Spatial pattern (binary, 0, 1 or (-1/255) for no sample)
					   FEATUREIO &fio2,
					   FEATUREIO &fioHRate
							)
{
	FEATUREIO fioMargZ;
	fioMargZ.iFeaturesPerVector = 1;
	fioMargZ.z = 1;
	fioMargZ.x = 2; // 2 binary sample patterns
	fioMargZ.y = 1;
	fioMargZ.t = 1;
	fioAllocate( fioMargZ );
	float *pfMargZ = fioMargZ.pfVectors;

	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fio1.iFeaturesPerVector; // Size is (marginal classes)x(2 binary sample patterns)
	fioMarg1.y = 2;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	FEATUREIO fioMarg2;
	fioMarg2.iFeaturesPerVector = 1;
	fioMarg2.z = 1;
	fioMarg2.x = fio2.iFeaturesPerVector; // Size is (marginal classes)x(2 binary sample patterns)
	fioMarg2.y = 2;
	fioMarg2.t = 1;
	fioAllocate( fioMarg2 );
	float *pfMarg2 = fioMarg2.pfVectors;

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 2; // 2 classes
	fioJoint.x = fio1.iFeaturesPerVector;
	fioJoint.y = fio2.iFeaturesPerVector;
	fioJoint.t = 1;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	PpImage ppImgTemp;
	fioimgInitReference( ppImgTemp, fioJoint );

	PpImage ppImg1;
	//ppImg1.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );
	PpImage ppImg2;
	//ppImg2.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );

	int iRowStart = iRow - iWindowRows/2;
	if( iRowStart < 0 )
	{
		iRowStart = 0;
	}
	else if( iRowStart + iWindowRows > fio1.y )
	{
		iRowStart = fio1.y - iWindowRows;
	}

	int iColStart = iCol - iWindowCols/2;
	if( iColStart < 0 )
	{
		iColStart = 0;
	}
	else if( iColStart + iWindowCols > fio1.x )
	{
		iColStart = fio1.x - iWindowCols;
	}

	float *pfWind1 = fio1.pfVectors + (iRowStart*fio1.x + iColStart)*fio1.iFeaturesPerVector;

	// Pointer to displacement to neighbours
	//int *piN = ((int*)ppImgN.ImageRow(iRowStart)) + iColStart;
	//piN = (int*)ppImgN.ImageRow(iRowStart);
	//piN += iColStart;

	for( int r = 0; r < fio2.y; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fio2.y )
		{
			iRowStart = fio2.y - iWindowRows;
		}

		printf( "Row: %d\n", r );
		for( int c = 0; c < fio2.x; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fio2.x )
			{
				iColStart = fio2.x - iWindowCols;
			}

			float *pfWind2 = fio2.pfVectors + (iRowStart*fio2.x + iColStart)*fio2.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			fioSet( fioJoint, 0 );
			fioSet( fioMarg1, 0 );
			fioSet( fioMarg2, 0 );
			fioSet( fioMargZ, 0 );

			// Generate joint probability table

			int rr, cc;
			int class1, class2;
			int iSpatialClass;

			// Sum over location  alignments
			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					// Sum over class probabilities (ouch, this is painfull)
					float *pfProbs1 = pfWind1 + (rr*fio1.x + cc)*fio1.iFeaturesPerVector;
					float *pfProbs2 = pfWind2 + (rr*fio2.x + cc)*fio2.iFeaturesPerVector;

					// Spatial label
					//
					//iSpatialClass = piN[rr*fio1.x + cc];
					iSpatialClass = ppImgN.ImageRow(rr)[cc];
					if( iSpatialClass != 0 && iSpatialClass != 1 )
					{
						// Don't process if not binary
						continue;
					}

					// Add spatial class sample
					fioMargZ.pfVectors[iSpatialClass]++;

					// Pointers to distributions conditional on spatial class
					float *pfM1 = fioMarg1.pfVectors + iSpatialClass*fioMarg1.x;
					float *pfM2 = fioMarg2.pfVectors + iSpatialClass*fioMarg2.x;
					float *pfJ  = fioJoint.pfVectors + iSpatialClass*fioJoint.x*fioJoint.y;

					// Sum up conditionals p(x|z) p(y|z) and p(x,y|z)
					// Assume independent observations in image 1 and image 2

					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						pfM1[class1] += pfProbs1[class1];
					}
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						pfM2[class2] += pfProbs2[class2];
					}
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
						{
							pfJ[class1*fioJoint.x + class2] += 
								pfProbs1[class1]*pfProbs2[class2];
						}
					}
				}
			}

			//output_float( ppImgTemp, "joint_transition.pgm" );
			//output_float_to_text( ppImgTemp, "joint_transition.txt" );

			//output_float( ppImg1, "img1.pgm" );
			//output_float( ppImg2, "img2.pgm" );


			//output_float( ppImgTemp, "joint_transition_norm.pgm" );
			//output_float_to_text( ppImgTemp, "joint_transition_norm.txt" );

			// Normalize conditional distributions according to space

			float fSpatialSum = 0;
			for( iSpatialClass = 0; iSpatialClass < 2; iSpatialClass++ )
			{
				fSpatialSum += fioMargZ.pfVectors[iSpatialClass];

				// Pointers to distributions conditional on spatial class
				float fNorm = 0;
				float fNormDiv;
				float *pfM1 = fioMarg1.pfVectors + iSpatialClass*fioMarg1.x;
				float *pfM2 = fioMarg2.pfVectors + iSpatialClass*fioMarg2.x;
				float *pfJ  = fioJoint.pfVectors + iSpatialClass*fioJoint.x*fioJoint.y;

				fNorm = 0;
				for( int class1 = 0; class1 < fioMarg1.x; class1++ )
				{
					fNorm += pfM1[class1];
				}
				if( fNorm > 0 )
				{
					fNormDiv = 1.0f/(float)fNorm;
					for( int class1 = 0; class1 < fioMarg1.x; class1++ )
					{
						pfM1[class1] *= fNormDiv;
					}
				}

				fNorm = 0;
				for( int class2 = 0; class2 < fioMarg2.x; class2++ )
				{
					fNorm += pfM2[class2];
				}
				if( fNorm > 0 )
				{
					assert( fNorm > 0 );
					fNormDiv = 1.0f/(float)fNorm;
					for( int class2 = 0; class2 < fioMarg2.x; class2++ )
					{
						pfM2[class2] *= fNormDiv;
					}
				}

				fNorm = 0;
				for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
				{
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						fNorm += pfJ[class1*fioJoint.x + class2];
					}
				}
				if( fNorm > 0 )
				{
					fNormDiv = 1.0f/(float)fNorm;
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
						{
							pfJ[class1*fioJoint.x + class2] *= fNormDiv;
						}
					}
				}
			}

			//output_float_to_text( ppImgTemp, "joint_transition_norm.txt" );

			// Calculate informations
			float fInformationRate = 0;
			assert( fSpatialSum > 0 );
			float fSpatialSumDiv = 1.0f/fSpatialSum;
			for( iSpatialClass = 0; iSpatialClass < 2; iSpatialClass++ )
			{
				// Conditional probability

				float *pfM1 = fioMarg1.pfVectors + iSpatialClass*fioMarg1.x;
				float *pfM2 = fioMarg2.pfVectors + iSpatialClass*fioMarg2.x;
				float *pfJ  = fioJoint.pfVectors + iSpatialClass*fioJoint.x*fioJoint.y;

				// Calculate joint entropy
				float fJointEntropy = 0;
				float fProbSum = 0.0f;
				for( cc = 0; cc < fioJoint.x*fioJoint.y; cc++ )
				{
					float fProb = pfJ[cc];
					if( fProb > 0 )
					{
						fProbSum += fProb;
						fJointEntropy -= (float)(fProb*log(fProb));
					}
				}

				// Calculate marginal entropy 1
				float fMarg1Entropy = 0;
				fProbSum = 0;
				for( int class1 = 0; class1 < fioMarg1.x; class1++ )
				{
					float fProb = pfM1[class1];
					if( fProb > 0 )
					{
						fProbSum += fProb;
						fMarg1Entropy -= (float)(fProb*log(fProb));
					}
				}

				// Calculate marginal entropy 2
				float fMarg2Entropy = 0;
				fProbSum = 0;
				for( int class2 = 0; class2 < fioMarg2.x; class2++ )
				{
					float fProb = pfM2[class2];
					if( fProb > 0 )
					{
						fProbSum += fProb;
						fMarg2Entropy -= (float)(fProb*log(fProb));
					}
				}

				float fSpatialProb = fSpatialSumDiv*fioMargZ.pfVectors[iSpatialClass];

				fInformationRate += fSpatialProb*(fMarg1Entropy + fMarg2Entropy - fJointEntropy);
			}

			if( fInformationRate < 0 )
			{
				fInformationRate = 0;
			}
			fioHRate.pfVectors[r*fioHRate.x + c] = fInformationRate;
		}
	}

	fioDelete( fioMargZ );
	fioDelete( fioMarg1 );
	fioDelete( fioMarg2 );
	fioDelete( fioJoint );

	return 1;
}

//
// hfGenerateProbHistogramEntropyImage()
//
// Generates entropy image probabilistically-labelled
// pixels. There is a problem - if individual pixels have
// a high entropy as to their identity, they are useless for
// registration - their labels mean nothing. That makes this particular
// entropy calculation useless in determining the informativeness of
// pixels.
//
// H(I)
// 
// Ideally, within a small window individual pixel labels have a low entropy,
// i.e. we are sure about individual pixel labels, but entropy of label
// given in this window is high.
//
int
hfGenerateProbHistogramEntropyImage(
									int iWindowRows,
									int iWindowCols,
									FEATUREIO &fio1,
									FEATUREIO &fioMI,
									PpImage &ppImgMask
									)
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fio1.iFeaturesPerVector;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	int iRowStart;
	int iColStart;

	for( int r = 0; r < fio1.y; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fio1.y )
		{
			iRowStart = fio1.y - iWindowRows;
		}

		printf( "Row: %d\n", r );
		for( int c = 0; c < fio1.x; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fio1.x )
			{
				iColStart = fio1.x - iWindowCols;
			}

			float *pfWind1 = fio1.pfVectors + (iRowStart*fio1.x + iColStart)*fio1.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );

			// Generate joint probability table ***Assuming image intensity is independent
			// of location, not necessarily valid***

			int rr, cc;
			int iJointSum = iWindowRows*iWindowCols;
			
			// Sum over location  alignments
			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{

					if( ppImgMask.ImageRow(rr)[cc] == 0 )
					{
						// Not in mask, don't process
						continue;
					}

					// Sum over class probabilities (ouch, this is painfull)
					float *pfProbs1 = pfWind1 + (rr*fio1.x + cc)*fio1.iFeaturesPerVector;
					
					// Sum up maginal 1
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						pfMarg1[class1] += pfProbs1[class1];
					}
				}
			}

			// Normalize
			float fNorm;
			float fNormMult;

			// Normalize marginal 1
			fNorm = 0;
			for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
			{
				fNorm += pfMarg1[class1];
			}
			assert( fNorm > 0 );
			fNormMult = 1.0f/fNorm;
			for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
			{
				pfMarg1[class1] *= fNormMult;
			}

			// Calculate entropies

			float fProbSum;

			fProbSum = 0.0f;
			float fMarg1Entropy = 0;
			for( rr = 0; rr < fioMarg1.x; rr++ )
			{
				if( pfMarg1[rr] > 0 )
				{
					float fProb = pfMarg1[rr];// / (float)iJointSum;
					fProbSum += fProb;
					fMarg1Entropy -= (float)(fProb*log(fProb));
				}
			}
			fioMI.pfVectors[r*fioMI.x + c] = fMarg1Entropy;
		}
	}

	fioDelete( fioMarg1 );

	return 1;
}


//
// hfGenerateProbHistogramEntropyImageLocation()
//
// This function returns the mutual information of
// image intensity and spatial location:
//    MI(I;X) = H(I) - H(I|X)
//
// This could be done equivalently by computing MI between
// 
//
// The hope is that this can provide a better meausure
// of local "goodness" for feature selection than H(I).
//
// For the moment, X is restricted to a binary random variable.
//
//int
//hfGenerateProbHistogramEntropyImageLocation__neverused(
//					   int iRow,
//					   int iCol,
//					   int iWindowRows,
//					   int iWindowCols,
//					   FEATUREIO &fio2,
//					   PpImage &ppImgN, // Spatial pattern (binary, 0, 1 or (-1/255) for no sample)
//					   FEATUREIO &fioHRate
//					   )
//{
//
//	FEATUREIO fioMargZ; // Z here is X, or spatial pattern region. For the moment, only 2 region labels are allowed.
//	fioMargZ.iFeaturesPerVector = 1;
//	fioMargZ.z = 1;
//	fioMargZ.x = 2; // 2 binary sample patterns
//	fioMargZ.y = 1;
//	fioMargZ.t = 1;
//	fioAllocate( fioMargZ );
//	float *pfMargZ = fioMargZ.pfVectors;
//
//	FEATUREIO fioMarg2; // Marg1 is actually the joint of intensity in fio2 and X
//	fioMarg2.iFeaturesPerVector = 1;
//	fioMarg2.z = 1;
//	fioMarg2.x = fio2.iFeaturesPerVector; // Size is (marginal classes)x(2 binary sample patterns)
//	fioMarg2.y = 2;
//	fioMarg2.t = 1;
//	fioAllocate( fioMarg2 );
//	float *pfMarg1 = fioMarg2.pfVectors;
//
//	PpImage ppImgTemp;
//	//fioimgInitReference( ppImgTemp, fioJoint );
//
//	PpImage ppImg1;
//	//ppImg1.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );
//	PpImage ppImg2;
//	//ppImg2.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );
//
//	int iRowStart = iRow - iWindowRows/2;
//	if( iRowStart < 0 )
//	{
//		iRowStart = 0;
//	}
//	else if( iRowStart + iWindowRows > fio2.y )
//	{
//		iRowStart = fio2.y - iWindowRows;
//	}
//
//	int iColStart = iCol - iWindowCols/2;
//	if( iColStart < 0 )
//	{
//		iColStart = 0;
//	}
//	else if( iColStart + iWindowCols > fio2.x )
//	{
//		iColStart = fio2.x - iWindowCols;
//	}
//
//	float *pfWind1 = fio2.pfVectors + (iRowStart*fio2.x + iColStart)*fio2.iFeaturesPerVector;
//
//	// Pointer to displacement to neighbours
//	//int *piN = ((int*)ppImgN.ImageRow(iRowStart)) + iColStart;
//	//piN = (int*)ppImgN.ImageRow(iRowStart);
//	//piN += iColStart;
//
//	for( int r = 0; r < fio2.y; r++ )
//	{
//		iRowStart = r - iWindowRows/2;
//		if( iRowStart < 0 )
//		{
//			iRowStart = 0;
//		}
//		else if( iRowStart + iWindowRows > fio2.y )
//		{
//			iRowStart = fio2.y - iWindowRows;
//		}
//
//		printf( "Row: %d\n", r );
//		for( int c = 0; c < fio2.x; c++ )
//		{			
//			iColStart = c - iWindowCols/2;
//			if( iColStart < 0 )
//			{
//				iColStart = 0;
//			}
//			else if( iColStart + iWindowCols > fio2.x )
//			{
//				iColStart = fio2.x - iWindowCols;
//			}
//
//			float *pfWind2 = fio2.pfVectors + (iRowStart*fio2.x + iColStart)*fio2.iFeaturesPerVector;
//
//			// Generation of probability tables could be done more efficiently
//
//			fioSet( fioMarg2, 0 );
//			fioSet( fioMargZ, 0 );
//
//			// Generate joint probability table
//
//			int rr, cc;
//			int class1, class2;
//			int iSpatialClass;
//
//			// Sum over location  alignments
//			for( rr = 0; rr < iWindowRows; rr++ )
//			{
//				for( cc = 0; cc < iWindowCols; cc++ )
//				{
//					// Sum over class probabilities (ouch, this is painfull)
//					float *pfProbs2 = pfWind2 + (rr*fio2.x + cc)*fio2.iFeaturesPerVector;
//
//					// Spatial label
//					//
//					//iSpatialClass = piN[rr*fio1.x + cc];
//					iSpatialClass = ppImgN.ImageRow(rr)[cc];
//					if( iSpatialClass != 0 && iSpatialClass != 1 )
//					{
//						// Don't process if not binary
//						continue;
//					}
//
//					// Add spatial class sample
//					fioMargZ.pfVectors[iSpatialClass]++;
//
//					// Pointers to distributions conditional on spatial class
//					float *pfM2 = fioMarg2.pfVectors + iSpatialClass*fioMarg2.x;
//
//					// Sum up conditionals p(x|z) p(y|z) and p(x,y|z)
//					// Assume independent observations in image 1 and image 2
//
//					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
//					{
//						pfM2[class2] += pfProbs2[class2];
//					}
//				}
//			}
//
//			//output_float( ppImgTemp, "joint_transition.pgm" );
//			//output_float_to_text( ppImgTemp, "joint_transition.txt" );
//
//			//output_float( ppImg1, "img1.pgm" );
//			//output_float( ppImg2, "img2.pgm" );
//
//
//			//output_float( ppImgTemp, "joint_transition_norm.pgm" );
//			//output_float_to_text( ppImgTemp, "joint_transition_norm.txt" );
//
//			// Normalize conditional distributions according to space
//
//			float fSpatialSum = 0;
//			for( iSpatialClass = 0; iSpatialClass < 2; iSpatialClass++ )
//			{
//				fSpatialSum += fioMargZ.pfVectors[iSpatialClass];
//
//				// Pointers to distributions conditional on spatial class
//				float fNorm = 0;
//				float fNormDiv;
//				float *pfM2 = fioMarg2.pfVectors + iSpatialClass*fioMarg2.x;
//
//				fNorm = 0;
//				for( int class2 = 0; class2 < fioMarg2.x; class2++ )
//				{
//					fNorm += pfM2[class2];
//				}
//				if( fNorm > 0 )
//				{
//					assert( fNorm > 0 );
//					fNormDiv = 1.0f/(float)fNorm;
//					for( int class2 = 0; class2 < fioMarg2.x; class2++ )
//					{
//						pfM2[class2] *= fNormDiv;
//					}
//				}
//			}
//
//			//output_float_to_text( ppImgTemp, "joint_transition_norm.txt" );
//
//			// Calculate informations
//			float fInformationRate = 0;
//			assert( fSpatialSum > 0 );
//			float fSpatialSumDiv = 1.0f/fSpatialSum;
//			for( iSpatialClass = 0; iSpatialClass < 2; iSpatialClass++ )
//			{
//				// Conditional probability
//				float *pfM2 = fioMarg2.pfVectors + iSpatialClass*fioMarg2.x;
//
//				// Calculate marginal entropy 2
//				float fMarg2Entropy = 0;
//				fProbSum = 0;
//				for( int class2 = 0; class2 < fioMarg2.x; class2++ )
//				{
//					float fProb = pfM2[class2];
//					if( fProb > 0 )
//					{
//						fProbSum += fProb;
//						fMarg2Entropy -= (float)(fProb*log(fProb));
//					}
//				}
//
//				float fSpatialProb = fSpatialSumDiv*fioMargZ.pfVectors[iSpatialClass];
//
//				fInformationRate += fSpatialProb*fMarg2Entropy;
//			}
//
//			if( fInformationRate < 0 )
//			{
//				fInformationRate = 0;
//			}
//			fioHRate.pfVectors[r*fioHRate.x + c] = fInformationRate;
//		}
//	}
//
//	fioDelete( fioMargZ );
//	fioDelete( fioMarg1 );
//	fioDelete( fioMarg2 );
//	fioDelete( fioJoint );
//
//	return 1;
//}


//
// hfGenerateProbHistogramLocationEntropyImage()
//
// Here we generate the entropy of pixel class given
// location. We do not assume pixel intensity is independent
// of location. Currently the probability of location p(x) is
// uniform.
//
//	H(I|X) = sum_x^N p(x) H(I|X=x)
//
int
hfGenerateProbHistogramLocationEntropyImage(
											int iWindowRows,
											int iWindowCols,
											FEATUREIO &fio1,
											FEATUREIO &fioMI
											)
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fio1.iFeaturesPerVector;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	int iRowStart;
	int iColStart;

	for( int r = 0; r < fio1.y; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fio1.y )
		{
			iRowStart = fio1.y - iWindowRows;
		}

		printf( "Row: %d\n", r );
		for( int c = 0; c < fio1.x; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fio1.x )
			{
				iColStart = fio1.x - iWindowCols;
			}

			float *pfWind1 = fio1.pfVectors + (iRowStart*fio1.x + iColStart)*fio1.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );

			// Generate joint probability table

			int rr, cc;
			int iJointSum = iWindowRows*iWindowCols;

			// Average entropy over location
			float fConditionalEntropySum = 0;

			// Sum over location  alignments
			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					// Sum over class probabilities (ouch, this is painfull)
					float *pfProbs1 = pfWind1 + (rr*fio1.x + cc)*fio1.iFeaturesPerVector;

					// Sum up maginal 1

					float fProbSum;
					fProbSum = 0.0f;
					float fPixelEntropy = 0;
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						// Sum up conditional entropy
						fProbSum += pfProbs1[class1];
						if( pfProbs1[class1] > 0 )
						{
							fPixelEntropy -= (float)(pfProbs1[class1]*log( pfProbs1[class1] ));
						}
						
						// Sum up marginal entropy
						pfMarg1[class1] += pfProbs1[class1];
					}

					fConditionalEntropySum += fPixelEntropy;
				}
			}

			// Calculate marginal entropy

			float fProbSum;
			fProbSum = 0.0f;
			float fMarg1Entropy = 0;
			for( rr = 0; rr < fioMarg1.x; rr++ )
			{
				if( pfMarg1[rr] > 0 )
				{
					float fProb = pfMarg1[rr] / (float)iJointSum;
					fProbSum += fProb;
					fMarg1Entropy -= (float)(fProb*log(fProb));
				}
			}
			
			fioMI.pfVectors[r*fioMI.x + c] = fMarg1Entropy - fConditionalEntropySum / ((float)iJointSum);

			//assert( fioMI.pfVectors[r*fioMI.x + c] >= 0 );

		}
	}

	fioDelete( fioMarg1 );

	return 1;
}


int
hfGenerateProbHistogramEntropyRateImage(
										int iWindowRows,
										int iWindowCols,
										FEATUREIO &fio1,
										FEATUREIO &fioMI
										)
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fio1.iFeaturesPerVector;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 1;
	fioJoint.x = fioMarg1.x;
	fioJoint.y = fioMarg1.x;
	fioJoint.t = 1;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	int iRowStart;
	int iColStart;

	for( int r = 0; r < fio1.y; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fio1.y )
		{
			iRowStart = fio1.y - iWindowRows;
		}

		printf( "Row: %d\n", r );
		for( int c = 0; c < fio1.x; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fio1.x )
			{
				iColStart = fio1.x - iWindowCols;
			}

			float *pfWind1 = fio1.pfVectors + (iRowStart*fio1.x + iColStart)*fio1.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y*fioMarg1.z );
			memset( pfJoint, 0, sizeof(float)*fioJoint.x*fioJoint.y*fioJoint.z );

			// Generate joint probability table

			int rr, cc;

			int iSum = 0;

			// Sum over location  alignments
			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					// Sum over class probabilities (ouch, this is painfull)
					float *pfProbs1 = pfWind1 + (rr*fio1.x + cc)*fio1.iFeaturesPerVector;
					
					// Sum up maginal 1
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						pfMarg1[class1] += pfProbs1[class1];
					}

					//
					// Sum up joint for all possible transitions
					//
					// Consider right & down
					//

					// Right neighbour transition
					if( cc + 1 < iWindowCols )
					{
						float *pfProbs2 = pfWind1 + (rr*fio1.x + (cc+1) )*fio1.iFeaturesPerVector;

						for( int class2 = 0; class2 < fio1.iFeaturesPerVector; class2++ )
						{
							for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
							{
								// Symetric transition prob
								pfJoint[class2*fioJoint.x + class1] +=
									pfProbs1[class1]*pfProbs2[class2];
								pfJoint[class1*fioJoint.x + class2] +=
									pfProbs1[class1]*pfProbs2[class2];
							}
						}
						iSum += 2;
					}

					// Down neighbour transition
					if( rr + 1 < iWindowRows )
					{
						float *pfProbs2 = pfWind1 + ( (rr+1)*fio1.x + cc)*fio1.iFeaturesPerVector;

						for( int class2 = 0; class2 < fio1.iFeaturesPerVector; class2++ )
						{
							for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
							{
								// Symetric transition prob
								pfJoint[class2*fioJoint.x + class1] +=
									pfProbs1[class1]*pfProbs2[class2];
								pfJoint[class1*fioJoint.x + class2] +=
									pfProbs1[class1]*pfProbs2[class2];
							}
						}
						iSum += 2;
					}
				}
			}

			int iSumMarg  = iWindowRows*iWindowCols;
			int iSumJoint = iSum; //2*(iWindowRows-1)*(iWindowCols-1);

			// Calculate entropies

			float fProbSumMarg;
			float fProbSumJoint;

			fProbSumMarg = 0.0f;
			fProbSumJoint = 0.0f;

			float fEntropyRate = 0;

			for( int class1 = 0; class1 < fioMarg1.x; class1++ )
			{
				// Loop over class1
				if( pfMarg1[class1] > 0 )
				{
					float fProbMarg = pfMarg1[class1] / (float)iSumMarg;
					fProbSumMarg += fProbMarg;

					// p(class2|class1=a) has to sum up to 1

					float fNorm = 0;
					
					for( int class2 = 0; class2 < fio1.iFeaturesPerVector; class2++ )
					{
						fNorm += pfJoint[class1*fioJoint.x + class2];
					}

					// Loop over transitions from class1 to class2
					for( int class2 = 0; class2 < fio1.iFeaturesPerVector; class2++ )
					{						
						if( pfJoint[class1*fioJoint.x + class2] > 0 )
						{
							float fProbJoint = pfJoint[class1*fioJoint.x + class2] / fNorm;
							fEntropyRate -= (float)(fProbMarg*fProbJoint*log(fProbJoint));
							fProbSumJoint += fProbJoint;
						}
					}
				}
			}

			//assert( fProbSumJoint == 1.0f );
			//assert( fProbSumMarg == 1.0f );

			fioMI.pfVectors[r*fioMI.x + c] = fEntropyRate;
		}
	}

	fioDelete( fioMarg1 );

	return 1;
}

/////////////////////////////////////////////////////////////////////
//
// Label Images - each pixel is a potentially a unique integer label,
//	not a probability over labels
//
/////////////////////////////////////////////////////////////////////

int
hfGenerateLabelMILikelihoodImage(
								int iRow,
								int iCol,
								int iWindowRows,
								int iWindowCols,
								FEATUREIO &fio1,	// Images of integers
								FEATUREIO &fio2,	// Images of integers
								FEATUREIO &fioMI
								)
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fio1.iFeaturesPerVector;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	FEATUREIO fioMarg2;
	fioMarg2.iFeaturesPerVector = 1;
	fioMarg2.z = 1;
	fioMarg2.x = fio2.iFeaturesPerVector;
	fioMarg2.y = 1;
	fioMarg2.t = 1;
	fioAllocate( fioMarg2 );
	float *pfMarg2 = fioMarg2.pfVectors;

	FEATUREIO fioJoint;
	fioJoint.iFeaturesPerVector = 1;
	fioJoint.z = 1;
	fioJoint.x = fioMarg1.x;
	fioJoint.y = fioMarg2.x;
	fioJoint.t = fioMarg2.t;
	fioAllocate( fioJoint );
	float *pfJoint = fioJoint.pfVectors;

	PpImage ppImgTemp;
	fioimgInitReference( ppImgTemp, fioJoint );

	PpImage ppImg1;
	//ppImg1.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );
	PpImage ppImg2;
	//ppImg2.Initialize( iWindowRows, iWindowCols, iWindowCols*sizeof(float), 8*sizeof(float) );

	int iRowStart = iRow - iWindowRows/2;
	if( iRowStart < 0 )
	{
		iRowStart = 0;
	}
	else if( iRowStart + iWindowRows > fio1.y )
	{
		iRowStart = fio1.y - iWindowRows;
	}

	int iColStart = iCol - iWindowCols/2;
	if( iColStart < 0 )
	{
		iColStart = 0;
	}
	else if( iColStart + iWindowCols > fio1.x )
	{
		iColStart = fio1.x - iWindowCols;
	}

	float *pfWind1 = fio1.pfVectors + (iRowStart*fio1.x + iColStart)*fio1.iFeaturesPerVector;

	for( int r = 0; r < fio2.y; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fio2.y )
		{
			iRowStart = fio2.y - iWindowRows;
		}

		printf( "Row: %d\n", r );
		for( int c = 0; c < fio2.x; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fio2.x )
			{
				iColStart = fio2.x - iWindowCols;
			}

			float *pfWind2 = fio2.pfVectors + (iRowStart*fio2.x + iColStart)*fio2.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			memset( pfJoint, 0, sizeof(float)*fioJoint.x*fioJoint.y );
			memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );
			memset( pfMarg2, 0, sizeof(float)*fioMarg2.x*fioMarg2.y );

			// Generate joint probability table

			int rr, cc;
			int iJointSum = iWindowRows*iWindowCols;
			
			// Sum over location  alignments
			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					// Sum over class probabilities (ouch, this is painfull)

					float *pfProbs1 = pfWind1 + (rr*fio1.x + cc)*fio1.iFeaturesPerVector;
					float *pfProbs2 = pfWind2 + (rr*fio2.x + cc)*fio2.iFeaturesPerVector;
					
					// Sum up maginal 1
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						pfMarg1[class1] += pfProbs1[class1];
					}

					// Sum up marginal 2
					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						pfMarg2[class2] += pfProbs2[class2];
					}

					// Sum up joint, assume independence

					for( int class2 = 0; class2 < fio2.iFeaturesPerVector; class2++ )
					{
						for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
						{
							pfJoint[class2*fio1.iFeaturesPerVector + class1] +=
								pfProbs1[class1]*pfProbs2[class2];
						}
					}
				}
			}

			//output_float( ppImg1, "img1.pgm" );
			//output_float( ppImg2, "img2.pgm" );

			// Calculate entropies

			float fProbSum;

			fProbSum = 0.0f;
			float fMarg1Entropy = 0;
			for( rr = 0; rr < fioMarg1.x; rr++ )
			{
				if( pfMarg1[rr] > 0 )
				{
					float fProb = pfMarg1[rr] / (float)iJointSum;
					fProbSum += fProb;
					fMarg1Entropy -= (float)(fProb*log(fProb));
				}
			}

			fProbSum = 0.0f;
			float fMarg2Entropy = 0;
			for( rr = 0; rr < fioMarg2.x; rr++ )
			{
				if( pfMarg2[rr] > 0 )
				{
					float fProb = pfMarg2[rr] / (float)iJointSum;
					fProbSum += fProb;
					fMarg2Entropy -= (float)(fProb*log(fProb));
				}
			}

			fProbSum = 0.0f;
			float fJointEntropy = 0;
			for( rr = 0; rr < fio2.iFeaturesPerVector; rr++ )
			{
				for( cc = 0; cc < fio1.iFeaturesPerVector; cc++ )
				{
					if( pfJoint[rr*fio1.iFeaturesPerVector + cc] > 0 )
					{
						float fProb = pfJoint[rr*fio1.iFeaturesPerVector + cc] / (float)iJointSum;
						fProbSum += fProb;
						fJointEntropy -= (float)(fProb*log(fProb));
					}
				}
			}

			fioMI.pfVectors[r*fioMI.x + c] = fMarg1Entropy + fMarg2Entropy - fJointEntropy;
		}
	}

	fioDelete( fioMarg1 );
	fioDelete( fioMarg2 );
	fioDelete( fioJoint );

	return 1;
}

//
//
//
//
int
hfGenerateLabelHistogramEntropyImage(
									int iWindowRows,
									int iWindowCols,
									FEATUREIO &fio1,
									FEATUREIO &fioMI
									)
{
	FEATUREIO fioMarg1;
	fioMarg1.iFeaturesPerVector = 1;
	fioMarg1.z = 1;
	fioMarg1.x = fio1.iFeaturesPerVector;
	fioMarg1.y = 1;
	fioMarg1.t = 1;
	fioAllocate( fioMarg1 );
	float *pfMarg1 = fioMarg1.pfVectors;

	int iRowStart;
	int iColStart;

	for( int r = 0; r < fio1.y; r++ )
	{
		iRowStart = r - iWindowRows/2;
		if( iRowStart < 0 )
		{
			iRowStart = 0;
		}
		else if( iRowStart + iWindowRows > fio1.y )
		{
			iRowStart = fio1.y - iWindowRows;
		}

		printf( "Row: %d\n", r );
		for( int c = 0; c < fio1.x; c++ )
		{			
			iColStart = c - iWindowCols/2;
			if( iColStart < 0 )
			{
				iColStart = 0;
			}
			else if( iColStart + iWindowCols > fio1.x )
			{
				iColStart = fio1.x - iWindowCols;
			}

			float *pfWind1 = fio1.pfVectors + (iRowStart*fio1.x + iColStart)*fio1.iFeaturesPerVector;

			// Generation of probability tables could be done more efficiently

			memset( pfMarg1, 0, sizeof(float)*fioMarg1.x*fioMarg1.y );

			// Generate joint probability table

			int rr, cc;
			int iJointSum = iWindowRows*iWindowCols;
			
			// Sum over location  alignments
			for( rr = 0; rr < iWindowRows; rr++ )
			{
				for( cc = 0; cc < iWindowCols; cc++ )
				{
					// Sum over class probabilities (ouch, this is painfull)
					float *pfProbs1 = pfWind1 + (rr*fio1.x + cc)*fio1.iFeaturesPerVector;
					
					// Sum up maginal 1
					for( int class1 = 0; class1 < fio1.iFeaturesPerVector; class1++ )
					{
						pfMarg1[class1] += pfProbs1[class1];
					}
				}
			}
			// Calculate entropies

			float fProbSum;

			fProbSum = 0.0f;
			float fMarg1Entropy = 0;
			for( rr = 0; rr < fioMarg1.x; rr++ )
			{
				if( pfMarg1[rr] > 0 )
				{
					float fProb = pfMarg1[rr] / (float)iJointSum;
					fProbSum += fProb;
					fMarg1Entropy -= (float)(fProb*log(fProb));
				}
			}
			fioMI.pfVectors[r*fioMI.x + c] = fMarg1Entropy;
		}
	}

	fioDelete( fioMarg1 );

	return 1;
}
