
#include <search.h>
#include <stdlib.h>
#include "DataPair.h"

static int
compare_DATA_PAIR_descending(
			   const void *p1, const void *p2
			   )
{
	DATA_PAIR *dp1 = (DATA_PAIR*)p1;
	DATA_PAIR *dp2 = (DATA_PAIR*)p2;

	if( dp1->iValue < dp2->iValue )
	{
		return 1;
	}
	else if( dp1->iValue > dp2->iValue )
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

static int
compare_DATA_PAIR_ascending(
			   const void *p1, const void *p2
			   )
{
	DATA_PAIR *dp1 = (DATA_PAIR*)p1;
	DATA_PAIR *dp2 = (DATA_PAIR*)p2;

	if( dp1->iValue > dp2->iValue )
	{
		return 1;
	}
	else if( dp1->iValue < dp2->iValue )
	{
		return -1;
	}
	else
	{
		return 0;
	}
}


int
dpSortAscending(
				DATA_PAIR *pdp,
				int iCount
			)
{
	qsort(
		pdp, iCount, sizeof(DATA_PAIR), compare_DATA_PAIR_ascending
		);
	return 0;
}

int
dpSortDescending(
				DATA_PAIR *pdp,
				int iCount
			)
{
	qsort(
		pdp, iCount, sizeof(DATA_PAIR), compare_DATA_PAIR_descending
		);
	return 0;
}

