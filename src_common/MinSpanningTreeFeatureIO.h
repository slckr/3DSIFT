
//
// File: MinSpanningTreeFeatureIO.(h/cpp)
// Desc: This code creates MSTs (minimum spanning trees) over latice of data, where
//	possible links are nearest neighbours in the latice. It is is therefore less
//  general than the MST code in MinSpanningTree.(h/cpp) where links can possibly exist
//  between any two data points.
//

#ifndef __MINSPANNINGTREEFEATUREIO_H__
#define __MINSPANNINGTREEFEATUREIO_H__

#include "MinSpanningTree.h"
#include "FeatureIO.h"

//
// computeNearestNeighbourForest()
//
// Computes nearest neighbour forest, a forest of trees who's links
// are nearest neighbours.
//
// The number of trees in the forest: N/2 <= Trees <= N-1
// The number of branches in the forest: N/2 <= Trees <= N-1
//
int
computeNearestNeighbourGraph(
							  FEATUREIO &fio,
							  FLOAT_VECTOR_DISTANCE_FUNCTION floatDist,
							  void			*pArg,
							  INDEXPAIR	*pipEdges	// Branches in Trees
							  );

//
// computeMinSpanningTreeFeatureIO()
//
// Computes the minumum spanning tree of a set of point vectors in a
// FEATUREIO structure. Makes the assumption that edges can only
// exist between neighbouring vectors in the FEATUREIO struct,
// either a 2D image or a 3D volume.
//
// O( Nlog(N) ), N is the number of pixels.
//
// Returns the result in array pipEdges - an array of COORD3DPAIR
// of size (iPointVectorCount - 1) containing index pairs of points
// in pPointVectors corresponding to edges in the minimum
// spanning tree.
//
int
computeMinSpanningTreeFeatureIO(
								FEATUREIO &fio,
								FLOAT_VECTOR_DISTANCE_FUNCTION floatDist,
								void			*pArg,
								INDEXPAIR	*pipEdges
								);

//
// createDendriteGraph()
//
// Resorts edges in descending order of edge weight.
//
// O( Nlog(N) ), N is the number of edges.
//
int
createDendriteGraph(
					FEATUREIO	&fio,
					FLOAT_VECTOR_DISTANCE_FUNCTION floatDist,
					INDEXPAIR	*pipEdges,
					int			iEdgeCount
					);



//
// FLOAT_TREE_DISTANCE_FUNCTION()
//
// Distance function between two trees on a latice.
//
typedef float FLOAT_TREE_DISTANCE_FUNCTION(
										   const int	iTree1,
										   const int	iTree2,
										   void			*pArg
										   );

//
// computeBlobsFeatureIO()
//
// Computes blobs using the same algorithm as computeMinSpanningTreeFeatureIO(),
// but does not merge trees if their distance is greater than fMaxAcceptableDist.
//
int
computeBlobsFeatureIO(
					  FEATUREIO &fio,
					  FLOAT_VECTOR_DISTANCE_FUNCTION floatDist,
					  float			fMaxAcceptableDist,
					  void			*pArg,
					  INDEXPAIR	*pipEdges
					  );

#endif
