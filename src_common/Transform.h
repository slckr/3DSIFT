

#ifndef __TRANSFORM_H__
#define __TRANSFORM_H__

#define MAX_NEIGHBOURS	10
#define MAX_FEATURES_PER_VECTOR 8

//
// A 2D point
//
typedef struct _POINT2D
{
	float fRow;
	float fCol;
} POINT2D;

//
// A pair of char
//
typedef char INTPAIR[2];

//
// A pair of char
//
typedef char RGBCHAR[3];

//
// The 2D transform unit
//
typedef struct _TRANS_STRUCT
{
	// Point location in image 1
	POINT2D	pt2dI1;
	
	// Point location in image 2
	POINT2D	pt2dI2;
	
	// Pointers to neighbours
	int piNeighbours[MAX_NEIGHBOURS];
	
	// The number of valid neighbours in ptsNeighbours (2-Cliques)
	int i2Cliques;
	
	// The number of valid neighbours in ptsNeighbours (3-Cliques)
	int i3Cliques;
	
	// 3-cliques
	INTPAIR			pip3Cliques[MAX_NEIGHBOURS];

	// 
	int bNoChange;
	
} TRANS_STRUCT;


#endif
