
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
#include <assert.h>
#include <math.h>


//
// ***NOTE*** this file not used anymore - 
//
//
//
//

#include "GaussianMask.h"
#include "convolution.h"
#include "PpImageFloatOutput.h"
#include "GaussianBlurFeatures.h"

#define PIXEL_VALUES 256
#define MAX_FEATURES_PER_PIXEL 50
#define PIXEL_SIMILARITY_VARRIANCE 400.0f

int
generateFeaturesGaussian(
						 FEATURE_STRUCT &fsStruct
							)
{
	PpImage	&ppImgIn = fsStruct.ppImgIn;
	int		&iBlurLevels = fsStruct.iFeaturesPerVector;
	float	&fSigma1 = fsStruct.fBlurSigma;
	int		&iSubSample = fsStruct.iSubSample;
	float *pfFeatures = generateFeaturesGaussian( ppImgIn, iBlurLevels, fSigma1, iSubSample );
	if( !pfFeatures )
	{
		return 0;
	}
	else
	{
		fsStruct.ppImgFeatures.InitializeSubImage(
			ppImgIn.Rows()/iSubSample, ppImgIn.Cols()/iSubSample,
			(ppImgIn.Cols()/iSubSample)*iBlurLevels*sizeof(float),
			iBlurLevels*sizeof(float)*8,
			(unsigned char*)pfFeatures
			);
		return 1;
	}
}

int
generateFeaturesGaussianEdge(
						 FEATURE_STRUCT &fsStruct
							)
{
	return 0;
}

//
// generateFeaturesGaussian()
//
// Returns an array of difference of multi-scale Gaussian blurring
// feature vectors.
//
// If ppImgIn is an RGB image, returns 3*iBlurLevels features per vector.
// If ppImgIn is greyscale, returns iBlurLevels features per vector.
//
float *
generateFeaturesGaussian(
							const PpImage	&ppImgIn,
							const int		&iBlurLevels,
							float			fSigma1,
							int				iSubSample
							)
{
	int iBytesPerPixel = ppImgIn.BitsPerPixel()/8;
	int iTotalFeaturesPerPixel = iBytesPerPixel*iBlurLevels;

	int iSubRows = (ppImgIn.Rows()/iSubSample);
	int iSubCols = (ppImgIn.Cols()/iSubSample);

	int iRows = ppImgIn.Rows();
	int iCols = ppImgIn.Cols();

	float *pfFeatures = new float[
		iSubRows
		*iSubCols
		*iTotalFeaturesPerPixel
		*iBytesPerPixel
		];

	if( !pfFeatures )
	{
		assert( pfFeatures );
		return 0;
	}

	// Images

	//PpImage ppImgGaussOut;
	PpImage ppImgGaussTemp;
	//ppImgGaussOut.Initialize( iSubRows, iSubCols, iSubCols*sizeof(float), sizeof(float)*8 );
	ppImgGaussTemp.Initialize( iCols, iRows, iRows*sizeof(float), sizeof(float)*8 );

	// Greyscale image for filtering - not subsampled

	PpImage ppImgTemp;
	ppImgTemp.Initialize( iRows, iCols, iCols, 8 );

	// Two filter template images

	PpImage ppImgGaussianMask2d;
	PpImage ppImgGaussianMask1d;

	// Generate levels of Gaussian blurring

	int iCurrFeature = 0;

	for( int iBlurLevel = 0; iBlurLevel < iBlurLevels; iBlurLevel++ )
	{
		// Initialize Gaussian filters

		int iTemplateSize = calculate_gaussian_filter_size( fSigma1, 0.01f );
		ppImgGaussianMask1d.Initialize( 1, iTemplateSize, iTemplateSize*sizeof(float), sizeof(float)*8 );

		generate_gaussian_filter1d( 
			ppImgGaussianMask1d, fSigma1, (float)(iTemplateSize/2)
			);

		for( int iByte = 0; iByte < iBytesPerPixel; iByte++ )
		{
			int r;
			int c;

			// Create temporary greyscale image with color layer
			// Full size - no subsampling yet

			for( r = 0; r < iRows; r++ )
			{
				unsigned char *puchImgInRow = ppImgIn.ImageRow(r);
				unsigned char *puchImgTempRow = ppImgTemp.ImageRow(r);
				for( int c = 0; c < iCols; c++ )
				{
					puchImgTempRow[c] =
						puchImgInRow[c*iBytesPerPixel + iByte];
				}
			}

			//ppImgTemp.WriteToFile( "color.pgm" );

			// Compute Gaussian organizing memory
			// Convolve on original image, no subsampling

			assert( ppImgTemp.Rows() == iRows );
			assert( ppImgTemp.Cols() == iCols );
			assert( ppImgGaussTemp.Rows() == iCols );
			assert( ppImgGaussTemp.Cols() == iRows );

			for( r = 0; r < iRows; r++ )
			{
				for( int c = 0; c < iCols; c++ )
				{
					float fPixel;
					
					convolve_zeroborder_char( ppImgTemp, ppImgGaussianMask1d,
						r, c, fPixel );

					if( fPixel != 0.0f )
					{
						fPixel = fPixel;
					}
					
					((float*)ppImgGaussTemp.ImageRow(c))[r] = fPixel;
				}
			}
			
			//output_float( ppImgGaussTemp, "gauss_out_1.pgm" );

			assert( ppImgTemp.Rows() == iRows );
			assert( ppImgTemp.Cols() == iCols );
			assert( ppImgGaussTemp.Rows() == iCols );
			assert( ppImgGaussTemp.Cols() == iRows );

			// Compute subsampled Gaussian

			for( r = 0; r < iSubCols; r++ )
			{
				for( int c = 0; c < iSubRows; c++ )
				{
					float fPixel;
					
					convolve_zeroborder_float( ppImgGaussTemp, ppImgGaussianMask1d,
						r*iSubSample, c*iSubSample, fPixel );
					
					//((float*)ppImgGaussOut.ImageRow(c))[r] = fPixel;

					pfFeatures[(c*iSubCols+r)*iTotalFeaturesPerPixel + iCurrFeature] = fPixel;
				}
			}

			//char pcFileName[200];
			//sprintf( pcFileName, "gauss_filter_%3.3f.pgm", fSigma1 );
			//output_float( ppImgGaussOut, pcFileName );

			// Next feature

			iCurrFeature++;
		}

		// Increase blur level by factor of two

		fSigma1 *= 2.0f;
	}

	return pfFeatures;
}

int
convertFeaturesDiffOfGauss(
			   float *pfFeatures,
			   int iFeaturesPerVector,
			   int iVectors
			   )
{
	for( int iVec = 0; iVec < iVectors; iVec++ )
	{
		float *pfFeature = pfFeatures + iVec*iFeaturesPerVector;
		for( int i = 0; i < iFeaturesPerVector - 1; i++ )
		{
			pfFeature[i] = pfFeature[i] - pfFeature[i+1];
		}
		pfFeature[iFeaturesPerVector - 1] = 0.0f;
	}
	return 1;
}

int
reverseFeatures(
			   float *pfFeatures,
			   int iFeaturesPerVector,
			   int iVectors
				)
{
	for( int iVec = 0; iVec < iVectors; iVec++ )
	{
		float *pfFeature = pfFeatures + iVec*iFeaturesPerVector;
		for( int i = 0; i < iFeaturesPerVector / 2; i++ )
		{
			float fTemp = pfFeature[i];
			pfFeature[i] = pfFeature[iFeaturesPerVector-1-i];
			pfFeature[iFeaturesPerVector-1-i] = fTemp;
		}
	}
	return 1;
}

//
// deleteFeatures()
//
// Deletes a feature vector array created by
// generateFeaturesGaussian()
//
int
deleteFeaturesGaussian(
			   float *pfFeatures
			   )
{
	if( !pfFeatures )
	{
		return 0;
	}

	delete [] pfFeatures;

	return 1;
}

