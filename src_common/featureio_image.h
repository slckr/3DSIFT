
#ifndef __FEATUREIO_IMAGE_H__
#define __FEATUREIO_IMAGE_H__

#include "FeatureIO.h"
#include "PpImage.h"

int
fioimgFeaturesToImageAll(
		   const FEATUREIO		&fio,
		   PpImage	&ppImg,
		   int bStretch = 0
		   );

int
fioimgFeaturesToImageOne(
		   const FEATUREIO		&fio,
		   PpImage	&ppImg,
		   int iFeature = 0
		   );

int
fioimgImageToFeaturesAll(
		   FEATUREIO		&fio,
		   const PpImage	&ppImg
		   );

int
fioimgImageToFeaturesOne(
		   FEATUREIO		&fio,
		   const PpImage	&ppImg,
		   const int		&iFeature = 0, // feature to put into fio
		   const int		&bInitFIO = 1	// initialize fio?
		   );

int
fioimgInitReference(
		   PpImage	&ppImg,
		   const FEATUREIO		&fio
					);

int
fioimgInitReferenceToImage(
		   const PpImage	&ppImg,
		   FEATUREIO		&fio
					);

int
fioimgRead(
		   char *pcFileName,
		   FEATUREIO &fio,
		   PpImage &ppImg
		   );


int
fioimgOutputVisualFeatureInVolume(
								  int x,
								  int y,
								  int z,
								  FEATUREIO &fioImg,
								char *pcFileNameBase,
								int bOneImage = 1,
								int bShowFeats = 1
					   );
#endif

