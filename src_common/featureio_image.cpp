
//
// What would it look like to see features in RGB,
// i.e. R->level 1 gaussian, G->level 2 gaussian, etc?
//

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include "PpImageFloatOutput.h"
#include "output_functions.h"
#include "featureio_image.h"

int
fioimgFeaturesToImageAll(
		   const FEATUREIO		&fio,
		   PpImage	&ppImg,
		   int bStretch
		   )
{
	if( fio.x != ppImg.Cols() || fio.y != ppImg.Rows() )
	{
		// Images must be of equal dimension
		return 0;
	}

	float fMin = 0;
	float fMax = 255;

	if( bStretch )
	{
		fMin = fio.pfVectors[0];
		fMax = fio.pfVectors[0];
		for( int i = 0; i < fio.x*fio.y*fio.z*fio.t*fio.iFeaturesPerVector; i++ )
		{
			if( fio.pfVectors[i] > fMax )
			{
				fMax = fio.pfVectors[i];
			}
			if( fio.pfVectors[i] < fMin )
			{
				fMin = fio.pfVectors[i];
			}
		}
	}
	float fRange = fMax-fMin;

	if( fio.iFeaturesPerVector == 1 )
	{
		if( ppImg.BitsPerPixel() == 8 )
		{
			for( int y = 0; y < fio.y; y++ )
			{
				unsigned char *pucRowImg = ppImg.ImageRow(y);
				float *pfRowFIO = fio.pfVectors + y*fio.x;
				for( int x = 0; x < fio.x; x++ )
				{
					pucRowImg[x] = (unsigned char)(255*(pfRowFIO[x]-fMin)/fRange);
				}
			}
		}
		else if( ppImg.BitsPerPixel() == 24 )
		{
			for( int y = 0; y < fio.y; y++ )
			{
				unsigned char *pucRowImg = ppImg.ImageRow(y);
				float *pfRowFIO = fio.pfVectors + y*fio.x*fio.iFeaturesPerVector;
				for( int x = 0; x < fio.x; x++ )
				{
					pucRowImg[x*3+0] = (unsigned char)(255*(pfRowFIO[x]-fMin)/fRange);
					pucRowImg[x*3+1] = (unsigned char)(255*(pfRowFIO[x]-fMin)/fRange);
					pucRowImg[x*3+2] = (unsigned char)(255*(pfRowFIO[x]-fMin)/fRange);
				}
			}
		}
		else if( ppImg.BitsPerPixel() == 32 )
		{
			int iZ = fio.z > 1 ? fio.z/2 : 0;
			for( int y = 0; y < fio.y; y++ )
			{
				float *pfRowImg = (float*)ppImg.ImageRow(y);
				//float *pfRowFIO = fio.pfVectors + y*fio.x*fio.iFeaturesPerVector;
				float *pfRowFIO = fioGetVector( fio, 0, y, iZ );
				for( int x = 0; x < fio.x; x++ )
				{
					pfRowImg[x] = pfRowFIO[x];
				}
			}
		}
	}
	else if( fio.iFeaturesPerVector == 3 && ppImg.BitsPerPixel() == 24 )
	{
		for( int y = 0; y < fio.y; y++ )
		{
			unsigned char *pucRowImg = ppImg.ImageRow(y);
			float *pfRowFIO = fio.pfVectors + y*fio.x*fio.iFeaturesPerVector;
			for( int x = 0; x < fio.x; x++ )
			{
				pucRowImg[x*fio.iFeaturesPerVector+0] = (unsigned char)(255*(pfRowFIO[3*x+0]-fMin)/fRange);
				pucRowImg[x*fio.iFeaturesPerVector+1] = (unsigned char)(255*(pfRowFIO[3*x+1]-fMin)/fRange);
				pucRowImg[x*fio.iFeaturesPerVector+2] = (unsigned char)(255*(pfRowFIO[3*x+2]-fMin)/fRange);
			}
		}
	}
	else
	{
		return 0;
	}

	return 1;
}

int
fioimgFeaturesToImageOne(
		   const FEATUREIO		&fio,
		   PpImage	&ppImg,
		   int iFeature
		   )
{
	if( fio.x != ppImg.Cols() || fio.y != ppImg.Rows() )
	{
		// Images must be of equal dimension
		return 0;
	}

	if( ppImg.BitsPerPixel() == 8 )
	{
		for( int y = 0; y < fio.y; y++ )
		{
			unsigned char *pucRowImg = ppImg.ImageRow(y);
			for( int x = 0; x < fio.x; x++ )
			{
				float *pfValue = fioGetVector( fio, x, y, 0 );
				pucRowImg[x] = (unsigned char)pfValue[iFeature];
			}
		}
	}
	else if( ppImg.BitsPerPixel() == 24 )
	{
		for( int y = 0; y < fio.y; y++ )
		{
			unsigned char *pucRowImg = ppImg.ImageRow(y);
			for( int x = 0; x < fio.x; x++ )
			{
				float *pfValue = fioGetVector( fio, x, y, 0 );
				pucRowImg[x*fio.iFeaturesPerVector+0] = (unsigned char)pfValue[iFeature];
				pucRowImg[x*fio.iFeaturesPerVector+1] = (unsigned char)pfValue[iFeature];
				pucRowImg[x*fio.iFeaturesPerVector+2] = (unsigned char)pfValue[iFeature];
			}
		}
	}
	else if( ppImg.BitsPerPixel() == 32 )
	{
		int iZ = fio.z > 1 ? fio.z/2 : 0;
		for( int y = 0; y < fio.y; y++ )
		{
			float *pfRowImg = (float*)ppImg.ImageRow(y);
			for( int x = 0; x < fio.x; x++ )
			{
				float *pfValue = fioGetVector( fio, x, y, iZ );
				pfRowImg[x] = (float)pfValue[iFeature];
 			}
		}
	}
	else
	{
		return 0;
	}
	return 1;
}

int
fioimgImageToFeaturesAll(
		   FEATUREIO		&fio,
		   const PpImage	&ppImg
		   )
{
	if( ppImg.Cols() == 0 || ppImg.Rows() == 0 )
	{
		return 0;
	}

	if( fio.x == ppImg.Cols() && fio.y == ppImg.Rows() &&
		(
		  ( fio.iFeaturesPerVector == 1 && ppImg.BitsPerPixel() == 8 )
		  ||
		  ( fio.iFeaturesPerVector == 3 && ppImg.BitsPerPixel() == 24 )
		  )
		  && fio.pfVectors != 0 )
	{
		// No need to allocate, images already of similar dimension and allocated
	}
	else
	{
		// Allocate
		if( ppImg.BitsPerPixel() == 8 )
		{
			// 8-bit greyscale
			fio.iFeaturesPerVector = 1;
		}
		else if( ppImg.BitsPerPixel() == 24 )
		{
			// 24-bit RGB
			fio.iFeaturesPerVector = 3;
		}
		else
		{
			// 32-bit floating point vector
			fio.iFeaturesPerVector = ppImg.BitsPerPixel() / (sizeof(float)*8);
		}

		fio.x = ppImg.Cols();
		fio.y = ppImg.Rows();
		fio.z = 1;
		fio.t = 1;
		if( !fioAllocate( fio ) )
		{
			return 0;
		}
	}

	if( ppImg.BitsPerPixel() == 8 )
	{
		for( int y = 0; y < fio.y; y++ )
		{
			unsigned char *pucRowImg = ppImg.ImageRow(y);
			float *pfRowFIO = fio.pfVectors + y*fio.x;
			for( int x = 0; x < fio.x; x++ )
			{
				pfRowFIO[x] = (float)pucRowImg[x];
			}
		}
	}
	else if( ppImg.BitsPerPixel() == 24 )
	{
		for( int y = 0; y < fio.y; y++ )
		{
			unsigned char *pucRowImg = ppImg.ImageRow(y);
			float *pfRowFIO = fio.pfVectors + y*fio.x*fio.iFeaturesPerVector;
			for( int x = 0; x < fio.x; x++ )
			{
				pfRowFIO[3*x+0] = (float)pucRowImg[x*fio.iFeaturesPerVector+0];
				pfRowFIO[3*x+1] = (float)pucRowImg[x*fio.iFeaturesPerVector+1];
				pfRowFIO[3*x+2] = (float)pucRowImg[x*fio.iFeaturesPerVector+2];
			}
		}
	}
	else
	{
		for( int y = 0; y < fio.y; y++ )
		{
			float *pfRowImg = (float*)ppImg.ImageRow(y);
			float *pfRowFIO = fio.pfVectors + y*fio.x;
			for( int x = 0; x < fio.x; x++ )
			{
				for( int k = 0; k < fio.iFeaturesPerVector; k++ )
				{
					pfRowFIO[x*fio.iFeaturesPerVector + k] = pfRowImg[x*fio.iFeaturesPerVector + k];
				}
			}
		}
	}

	return 1;
}

int
fioimgImageToFeaturesOne(
		   FEATUREIO		&fio,
		   const PpImage	&ppImg,
		   const int		&iFeature,
		   const int		&bInitFIO
		   )
{
	if( ppImg.Cols() == 0 || ppImg.Rows() == 0 )
	{
		return 0;
	}

	if( bInitFIO )
	{
		if( ppImg.BitsPerPixel() == 8 )
		{
			// 8-bit greyscale
			fio.iFeaturesPerVector = 1;
		}
		else if( ppImg.BitsPerPixel() == 24 )
		{
			// 24-bit RGB
			fio.iFeaturesPerVector = 1;
		}
		else
		{
			// 32-bit floating point vector
			fio.iFeaturesPerVector = ppImg.BitsPerPixel() / (sizeof(float)*8);
		}

		fio.x = ppImg.Cols();
		fio.y = ppImg.Rows();
		fio.z = 1;
		fio.t = 1;
		fio.pfMeans = 0;
		fio.pfVarrs = 0;
		fio.pfVectors = 0;
		if( !fioAllocate( fio ) )
		{
			return 0;
		}
	}
	else
	{
		assert( fio.x == ppImg.Cols() );
		assert( fio.y == ppImg.Rows() );
		assert( fio.z == 1 );
		assert( fio.t == 1 );
		assert( fio.pfMeans != 0 );
		assert( fio.pfVarrs != 0 );
		assert( fio.pfVectors != 0 );

		if(
			!( fio.x == ppImg.Cols() ) ||
			!( fio.y == ppImg.Rows() ) ||
			!( fio.z == 1 ) ||
			!( fio.t == 1 ) ||
			!( fio.pfMeans != 0 ) ||
			!( fio.pfVarrs != 0 ) ||
			!( fio.pfVectors != 0 )
			)
		{
			return 0;
		}
	}
	

	if( ppImg.BitsPerPixel() == 8 )
	{
		for( int y = 0; y < fio.y; y++ )
		{
			unsigned char *pucRowImg = ppImg.ImageRow(y);
			float *pfRowFIO = fio.pfVectors + y*fio.x;
			for( int x = 0; x < fio.x; x++ )
			{
				pfRowFIO[x] = (float)pucRowImg[x];
			}
		}
	}
	else if( ppImg.BitsPerPixel() == 24 )
	{
		assert( iFeature >= 0 && iFeature <= 2 );
		for( int y = 0; y < fio.y; y++ )
		{
			unsigned char *pucRowImg = ppImg.ImageRow(y);
			float *pfRowFIO = fio.pfVectors + y*fio.x;
			for( int x = 0; x < fio.x; x++ )
			{
				pfRowFIO[x] = (float)pucRowImg[3*x+iFeature];
			}
		}
	}
	else
	{
		assert( iFeature >= 0 && iFeature <= fio.iFeaturesPerVector );
		for( int y = 0; y < fio.y; y++ )
		{
			float *pfRowImg = (float*)ppImg.ImageRow(y);
			float *pfRowFIO = fio.pfVectors + y*fio.x;
			for( int x = 0; x < fio.x; x++ )
			{
				pfRowFIO[x] = pfRowImg[x*fio.iFeaturesPerVector + iFeature];
			}
		}
	}

	return 1;
}


int
fioimgInitReference(
		   PpImage	&ppImg,
		   const FEATUREIO		&fio
					)
{
	return ppImg.InitializeSubImage(
		fio.y, fio.x, fio.x*sizeof(float)*fio.iFeaturesPerVector,
		sizeof(float)*fio.iFeaturesPerVector*8, (unsigned char*)fio.pfVectors
		);
}

int
fioimgInitReferenceToImage(
		   const PpImage	&ppImg,
		   FEATUREIO		&fio
					)
{
	assert( ppImg.BitsPerPixel() == sizeof(float)*8 );

	fio.z = 1;
	fio.t = 1;
	fio.x = ppImg.Cols();
	fio.y = ppImg.Rows();
	fio.iFeaturesPerVector = 1;
	fio.pfMeans = 0;
	fio.pfVarrs = 0;

	if( ppImg.BitsPerPixel() == sizeof(float)*8 )
	{
        fio.pfVectors = (float*)ppImg.ImageRow(0);
		return 1;
	}
	else
	{
		fio.pfVectors = 0;
		return 0;
	}

}

int
fioimgRead(
		   char *pcFileName,
		   FEATUREIO &fio,
		   PpImage &ppImg
		   )
{
	if( !pcFileName )
	{
		return 0;
	}

	int iReturn;

	iReturn = fioRead( fio, pcFileName );
	if( iReturn  )
	{
		ppImg.Initialize( fio.y, fio.x, fio.x*sizeof(float), sizeof(float)*8 );
		return fioimgFeaturesToImageOne( fio, ppImg, 0 );
	}
	else if( ppImg.InitFromStream( pcFileName ) )
	{
		return fioimgImageToFeaturesAll( fio, ppImg );
	}

	return iReturn;
}

int
fioimgOutputVisualFeatureInVolume(
								  int x,
								  int y,
								  int z,
								  FEATUREIO &fioImg,
								char *pcFileNameBase,
								int bOneImage,
								int bShowFeats
					   )
{
	char pcFileName[400];
	PpImage ppImgOut;
	FEATUREIO fioTmp;

	int scale = 5;

	if( bOneImage )
	{
		PpImage ppImgOutSub;
		PpImage ppBigOut;

		ppBigOut.Initialize( fioImg.z+fioImg.x, fioImg.y+fioImg.x, (fioImg.y+fioImg.x)*sizeof(float), sizeof(float)*8 );
		if( bShowFeats )
		{
			sprintf( pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_circles.pgm", pcFileNameBase, (int)x, (int)y, (int)z );
		}
		else
		{
			sprintf( pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d.pgm", pcFileNameBase, (int)x, (int)y, (int)z );
		}
		for( int yy = 0; yy < ppBigOut.Rows(); yy++ )
		{
			for( int xx = 0; xx < ppBigOut.Cols(); xx++ )
			{
				((float*)ppBigOut.ImageRow(yy))[xx] =0;
			}
		}

		ppImgOutSub.InitializeSubImage(
			fioImg.z, fioImg.y,
			ppBigOut.RowInc(), ppBigOut.BitsPerPixel(),
			(unsigned char*)ppBigOut.ImageRow(0)
			);
		ppImgOut.Initialize( fioImg.z, fioImg.y, fioImg.y*sizeof(float), sizeof(float)*8 );
		fioFeatureSliceZY( fioImg, x, 0, (float*)ppImgOut.ImageRow(0) );
		fioimgInitReferenceToImage( ppImgOut, fioTmp ); fioNormalize( fioTmp, 255.0f );
		if( bShowFeats )
		{
				of_paint_white_circle( z, y, 2*scale, ppImgOut );
		}
		for( int yy = 0; yy < fioImg.z; yy++ )
		{
			for( int xx = 0; xx < fioImg.y; xx++ )
			{
				((float*)ppImgOutSub.ImageRow(yy))[xx] =
					((float*)ppImgOut.ImageRow(yy))[xx];
			}
		}
		//output_float( ppBigOut, pcFileName );

		ppImgOutSub.InitializeSubImage(
			fioImg.y, fioImg.x,
			ppBigOut.RowInc(), ppBigOut.BitsPerPixel(),
			(unsigned char*)ppBigOut.ImageRow( fioImg.z )
			);
		ppImgOut.Initialize( fioImg.y, fioImg.x, fioImg.x*sizeof(float), sizeof(float)*8 );
		fioFeatureSliceXY( fioImg,  z, 0, (float*)ppImgOut.ImageRow(0) );
		fioimgInitReferenceToImage( ppImgOut, fioTmp ); fioNormalize( fioTmp, 255.0f );
		if( bShowFeats )
		{
			of_paint_white_circle( y, x, 2*scale, ppImgOut );
		}
		for( int yy = 0; yy < fioImg.y; yy++ )
		{
			for( int xx = 0; xx < fioImg.x; xx++ )
			{
				((float*)ppImgOutSub.ImageRow(xx))[yy] =
					((float*)ppImgOut.ImageRow(yy))[fioImg.x-1-xx];
			}
		}
		//output_float( ppBigOut, pcFileName );

		ppImgOutSub.InitializeSubImage(
			fioImg.z, fioImg.x,
			ppBigOut.RowInc(), ppBigOut.BitsPerPixel(),
			(unsigned char*)((float*)ppBigOut.ImageRow(0) + fioImg.y)
			);
		ppImgOut.Initialize( fioImg.z, fioImg.x, fioImg.x*sizeof(float), sizeof(float)*8 );
		fioFeatureSliceZX( fioImg, y, 0, (float*)ppImgOut.ImageRow(0) );
		fioimgInitReferenceToImage( ppImgOut, fioTmp ); fioNormalize( fioTmp, 255.0f );
		if( bShowFeats )
		{
			of_paint_white_circle( z, x, 2*scale, ppImgOut );
		}
		for( int yy = 0; yy < fioImg.z; yy++ )
		{
			for( int xx = 0; xx < fioImg.x; xx++ )
			{
				((float*)ppImgOutSub.ImageRow(yy))[xx] =
					((float*)ppImgOut.ImageRow(yy))[xx];
			}
		}
		output_float( ppBigOut, pcFileName );
	}
	else
	{

		ppImgOut.Initialize( fioImg.z, fioImg.y, fioImg.y*sizeof(float), sizeof(float)*8 );
		fioFeatureSliceZY( fioImg, x, 0, (float*)ppImgOut.ImageRow(0) );
		fioimgInitReferenceToImage( ppImgOut, fioTmp ); fioNormalize( fioTmp, 255.0f );
		sprintf( pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_zy.pgm", pcFileNameBase, (int)x, (int)y, (int)z );
		if( bShowFeats )
			of_paint_white_circle( z, y, 2*scale, ppImgOut );
		output_float( ppImgOut, pcFileName );

		ppImgOut.Initialize( fioImg.y, fioImg.x, fioImg.x*sizeof(float), sizeof(float)*8 );
		fioFeatureSliceXY( fioImg,  z, 0, (float*)ppImgOut.ImageRow(0) );
		fioimgInitReferenceToImage( ppImgOut, fioTmp ); fioNormalize( fioTmp, 255.0f );
		sprintf( pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_xy.pgm", pcFileNameBase, (int)x, (int)y, (int)z );
		if( bShowFeats )
			of_paint_white_circle( y, x, 2*scale, ppImgOut );
		output_float( ppImgOut, pcFileName );

		ppImgOut.Initialize( fioImg.z, fioImg.x, fioImg.x*sizeof(float), sizeof(float)*8 );
		fioFeatureSliceZX( fioImg, y, 0, (float*)ppImgOut.ImageRow(0) );
		fioimgInitReferenceToImage( ppImgOut, fioTmp ); fioNormalize( fioTmp, 255.0f );
		sprintf( pcFileName, "%s_x%3.3d_y%3.3d_z%3.3d_zx.pgm", pcFileNameBase, (int)x, (int)y, (int)z );
		if( bShowFeats )
			of_paint_white_circle( z, x, 2*scale, ppImgOut );
		output_float( ppImgOut, pcFileName );
	}

	return 1;
}
