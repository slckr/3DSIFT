
#ifndef __FISHERSEXACTTEST_H__
#define __FISHERSEXACTTEST_H__

#pragma warning(disable:4786)

#include <vector>

using namespace std;


class FishersExactTest
{
public:

	FishersExactTest(
		)
	{
	}
	
	~FishersExactTest(
		)
	{
	}

	int
	Init( 
		int iTotalCounts
		);

	float
	OneSidedPValue(
		int a11,
		int a12,
		int a21,
		int a22
		);

	float
	ExactValue(
		int a11,
		int a12,
		int a21,
		int a22
		);

private:

	// Precompute the log of factorials
	vector< float > m_vfLogIntFactorial;
};

#endif
