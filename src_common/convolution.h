
#ifndef __CONVOLUTION_H__
#define __CONVOLUTION_H__

#include "PpImage.h"

//
// convolve_reflection()
//
// Convolves image ppImgIn with mask ppTmpIn with reflection at edges at point
// iImgRowCenter, iImgColCenter. Returns floating point result in fConvResult.
//
//	ppImgIn - input greyscale image, type char (8-bits per pixel)
//	ppTmpIn - input convolution mask image, type float (32-bits per pixel)
//	iImgRowCenter - center row position of convolution in ppImgIn
//	iImgColCenter - center col position of convolution in ppImgIn
//	fNormalization - normalization factor to divide convolution result.
//					if 0.0, ignored.
//	fConvResult - result of convolution
//
void
convolve_reflection_char(
	const PpImage &ppImg,		// char
	const PpImage &ppTmp,		// float
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  );
void
convolve_reflection_float(
	const PpImage &ppImg,		// float
	const PpImage &ppTmp,		// float
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  );
void
covariance_reflection_float(	
	const PpImage &ppImg,		// float
	const PpImage &ppTmp,		// float
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  );
void
ssd_reflection_float(	
	const PpImage &ppImg,		// float
	const PpImage &ppTmp,		// float
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  );
void
ssd_reflection_float_remove_mean(
	const PpImage &ppImg,
	const PpImage &ppTmp,
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  );

//
// convolve_zeroborder...()
//
// Performs convolution treating all pixels outside of the image border
// as having the value zero.
//
void
convolve_zeroborder_char(
	const PpImage &ppImg,// char
	const PpImage &ppTmp,// float
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  );
void
convolve_zeroborder_float(
	const PpImage &ppImg,// float
	const PpImage &ppTmp,// float
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  );
void
covariance_zeroborder_float(
	const PpImage &ppImg,// float
	const PpImage &ppTmp,// float
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  );
void
ssd_zeroborder_float(
	const PpImage &ppImg,// float
	const PpImage &ppTmp,// float
	const int &iImgRowCenter,
	const int &iImgColCenter,
	float &fConvResult
				  );

//
// convolve_image_reflection()
//
// Convolves image ppImgIn with mask ppTmpIn, puts result in ppImgOut.
//
//	ppImgIn - input greyscale image, type char (8-bits per pixel)
//	ppTmpIn - input convolution mask image, type integer (32-bits per pixel)
//  ppImgOut - output of convolution, type float (32-bits per pixel)
//
void
convolve_image_reflection_char(
				  const PpImage &ppImgIn,	// Input greyscale image
				  const PpImage &ppTmpIn,	// Input signed integer mask
				  PpImage		&ppImgOut	// Image of float
					);
void
convolve_image_reflection_float(
				  const PpImage &ppImgIn,	// Input greyscale image, float
				  const PpImage &ppTmpIn,	// Image filter float
				  PpImage		&ppImgOut	// Image of float
					);
void
convolve_image_zeroborder_float(
				  const PpImage &ppImgIn,	// Input greyscale image, float
				  const PpImage &ppTmpIn,	// Image filter float
				  PpImage		&ppImgOut	// Image of float
					);

//
// covariance_image_reflection_float()
//
// Computes the covarriance of image ppImgIn with mask ppTmpIn, puts result in ppImgOut.
//
// Sum(xi*yi) - Sum(xi)Sum(yi)/N
//
//	ppImgIn - input greyscale image, type char (8-bits per pixel)
//	ppTmpIn - input convolution mask image, type integer (32-bits per pixel)
//  ppImgOut - output of convolution, type float (32-bits per pixel)
//
void
covariance_image_reflection_float(
				  const PpImage &ppImgIn,	// Input float image
				  const PpImage &ppTmpIn,	// Input float mask
				  PpImage		&ppImgOut	// Output float image
					);
void
covariance_image_zeroborder_float(
				  const PpImage &ppImgIn,	// Input float image
				  const PpImage &ppTmpIn,	// Input float mask
				  PpImage		&ppImgOut	// Output float image
					);

//
// variance_image_zeroborder_float()
//
// Calculates the squared magnitudes of vectors at all locations of ppImgIn
// of size ppTmpIn. Used to normalize the covariance.
//
void
variance_image_zeroborder_float(
				  const PpImage &ppImgIn,	// Input float image
				  const PpImage &ppTmpIn,	// Input filter size
				  PpImage		&ppImgOut	// Output float image
					);

void
variance_image_reflection_float(
				  const PpImage &ppImgIn,	// Input float image
				  const PpImage &ppTmpIn,	// Input filter size
				  PpImage		&ppImgOut	// Output float image
					);

//
// normalized_covariance_image_reflection_float()
//
// Normalized covariance.
//
void
normalized_covariance_image_reflection_float(
				  const PpImage &ppImgIn,		// Input float image
				  const PpImage &ppTmpIn,		// Input float mask
				  const PpImage &ppImgVarrIn,	// Image of varriances for ppImgIn
				  const float	&fVarrTmp,		// Varriance of ppTempIn
				  PpImage		&ppImgOut		// Output float image
					);
void
normalized_covariance_image_zeroborder_float(
				  const PpImage &ppImgIn,		// Input float image
				  const PpImage &ppTmpIn,		// Input float mask
				  const PpImage &ppImgVarrIn,	// Image of varriances for ppImgIn
				  const float	&fVarrTmp,		// Varriance of ppTempIn
				  PpImage		&ppImgOut		// Output float image
					);

//
// ssd_image_reflection_float()
//
// Computes the covarriance of image ppImgIn with mask ppTmpIn, puts result in ppImgOut.
//
// Sum(xi*yi) - Sum(xi)Sum(yi)/N
//
//	ppImgIn - input greyscale image, type char (8-bits per pixel)
//	ppTmpIn - input convolution mask image, type integer (32-bits per pixel)
//  ppImgOut - output of convolution, type float (32-bits per pixel)
//
void
ssd_image_reflection_float(
				  const PpImage &ppImgIn,	// Input float image
				  const PpImage &ppTmpIn,	// Input float mask
				  PpImage		&ppImgOut	// Output float image
					);
void
ssd_image_zeroborder_float(
				  const PpImage &ppImgIn,	// Input float image
				  const PpImage &ppTmpIn,	// Input float mask
				  PpImage		&ppImgOut	// Output float image
					);

#endif
