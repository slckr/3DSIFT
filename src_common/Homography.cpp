
#include "Homography.h"

#include <math.h>

//using namespace std;


void
MatrixMultiply(
			   Matrix m1,
			   Matrix m2,
			   Matrix &result
			   )
{
	int i,j,k;
	double res;


	for(i = 0; i < 3; i++)
	{
		for(j = 0; j < 3; j++)
		{
			res = 0.0; 
			for(k = 0; k < 3; k++) 
			{
				res += (m1.array[i][k] * m2.array[k][j]);
			}
			result.array[i][j] = res;
		}
	}

}


//
// Compute the homography by decomposing to the unit square
// We can compute the inverse of a matrix as InvM=AdjunctM/detM. This implies mostly
// multiplications. Also since it is a projective transformation we can discard the 
// computation of det M
//
int
ComputeHomography4Points(
						 Point l[4],
						 Point r[4],
						 Matrix &H
						 )
{
	int i,j;
	Point cp[6],col[3],lig[3],pv[3];
	double det[6];
	Matrix H1inv,H2,tmp;

	cp[0].CrossProduct(r[2],r[3]);
	cp[1].CrossProduct(r[0],r[3]);
	cp[2].CrossProduct(r[2],r[0]);

	cp[3].CrossProduct(l[2],l[3]);
	cp[4].CrossProduct(l[0],l[3]);
	cp[5].CrossProduct(l[2],l[0]);


	det[0]=DotProduct(r[0],cp[0]);
	det[1]=DotProduct(r[1],cp[1]);
	det[2]=DotProduct(r[1],cp[2]);
	det[3]=DotProduct(l[0],cp[3]);
	det[4]=DotProduct(l[1],cp[4]);
	det[5]=DotProduct(l[1],cp[5]);

	col[0].ScalarMultiply(r[1],det[0]);
	col[1].ScalarMultiply(r[2],det[1]);
	col[2].ScalarMultiply(r[3],det[2]);

	pv[0].CrossProduct(l[2], l[3]);
	pv[1].CrossProduct(l[3], l[1]);
	pv[2].CrossProduct(l[1], l[2]);

	if ((det[0]!=0.0)&&(det[1]!=0.0)&&(det[2]!=0.0)\
		&&(det[3]!=0.0)&&(det[4]!=0.0)&&(det[5]!=0.0))
	{
		// Non-degenerate case
		lig[0].ScalarMultiply(pv[0],1.0/det[3]);
		lig[1].ScalarMultiply(pv[1],1.0/det[4]);
		lig[2].ScalarMultiply(pv[2],1.0/det[5]);


		H1inv.array[0][0]=lig[0].x;
		H1inv.array[0][1]=lig[0].y;
		H1inv.array[0][2]=lig[0].z;

		H1inv.array[1][0]=lig[1].x;
		H1inv.array[1][1]=lig[1].y;
		H1inv.array[1][2]=lig[1].z;

		H1inv.array[2][0]=lig[2].x;
		H1inv.array[2][1]=lig[2].y;
		H1inv.array[2][2]=lig[2].z;

		H2.array[0][0]=col[0].x;
		H2.array[1][0]=col[0].y;
		H2.array[2][0]=col[0].z;

		H2.array[0][1]=col[1].x;
		H2.array[1][1]=col[1].y;
		H2.array[2][1]=col[1].z;

		H2.array[0][2]=col[2].x;
		H2.array[1][2]=col[2].y;
		H2.array[2][2]=col[2].z;


		MatrixMultiply(H2,H1inv,tmp);

		// Normalize...

		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
				H.array[i][j]=(tmp.array[i][j]/tmp.array[2][2]);
		}
		return 1;
	}
	else 
	{ 
		// Degenerate case: return Id matrix.
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++) H.array[i][j]=((i==j)? 1.0 : 0.0);
		}
		return 0;
	}  
} 

void
MatrixPoint(
			Matrix &H,
			double x,
			double y,
			double &xi,
			double &yi
			)
{
	double xx,yy,zz;

	xx=H.array[0][0]*x+H.array[0][1]*y+H.array[0][2];
	yy=H.array[1][0]*x+H.array[1][1]*y+H.array[1][2];
	zz=H.array[2][0]*x+H.array[2][1]*y+H.array[2][2];


	xi=(int)(xx/zz+0.5);
	yi=(int)(yy/zz+0.5);
}



//int
//ComputeFundamentalMatrix8Points(
//						 Point *i1,
//						 Point *i2,
//						 Matrix &F,
//						 int iPoints
//								)
//{
//    CvMat* wpoints[2]={0,0};
//    CvMat* preFundMatr = 0;
//    CvMat* sqdists = 0;
//    CvMat* matrA = 0;
//    CvMat* matrU = 0;
//    CvMat* matrW = 0;
//    CvMat* matrV = 0;
//
//    int numFundMatrs = 0;
//
//    int numPoint;
//    numPoint = points1->cols;    
//
//    /* allocate data */
//    CV_CALL( wpoints[0] = cvCreateMat(2,numPoint,CV_64F) );
//    CV_CALL( wpoints[1] = cvCreateMat(2,numPoint,CV_64F) );
//    CV_CALL( matrA = cvCreateMat(numPoint,9,CV_64F) );
//    CV_CALL( preFundMatr = cvCreateMat(3,3,CV_64F) );
//    CV_CALL( matrU = cvCreateMat(numPoint,numPoint,CV_64F) );
//    CV_CALL( matrW = cvCreateMat(numPoint,9,CV_64F) );
//    CV_CALL( matrV = cvCreateMat(9,9,CV_64F) );
//    CV_CALL( sqdists = cvCreateMat(1,numPoint,CV_64F) );
//
//    /* Create working points with just x,y */
//
//    CvMat transfMatr[2];
//    double transfMatr1_dat[9];
//    double transfMatr2_dat[9];
//    transfMatr[0] = cvMat(3,3,CV_64F,transfMatr1_dat);
//    transfMatr[1] = cvMat(3,3,CV_64F,transfMatr2_dat);
//
//    {/* transform to x,y.  tranform point and compute transform matrixes */
//        icvMake2DPoints(points1,wpoints[0]);
//        icvMake2DPoints(points2,wpoints[1]);
//
//        icvNormalizeFundPoints( wpoints[0],&transfMatr[0]);
//        icvNormalizeFundPoints( wpoints[1],&transfMatr[1]);
//        
//        /* we have normalized working points wpoints[0] and wpoints[1] */
//        /* build matrix A from points coordinates */
//
//        int currPoint;
//        for( currPoint = 0; currPoint < numPoint; currPoint++ )
//        {
//            CvMat rowA;
//            double x1,y1;
//            double x2,y2;
//
//            x1 = cvmGet(wpoints[0],0,currPoint);
//            y1 = cvmGet(wpoints[0],1,currPoint);
//            x2 = cvmGet(wpoints[1],0,currPoint);
//            y2 = cvmGet(wpoints[1],1,currPoint);
//
//            cvGetRow(matrA,&rowA,currPoint);
//            rowA.data.db[0] = x1*x2;
//            rowA.data.db[1] = x1*y2;
//            rowA.data.db[2] = x1;
//
//            rowA.data.db[3] = y1*x2;
//            rowA.data.db[4] = y1*y2;
//            rowA.data.db[5] = y1;
//
//            rowA.data.db[6] = x2;
//            rowA.data.db[7] = y2;
//            rowA.data.db[8] = 1;
//        }
//    }
//
//    /* We have matrix A. Compute svd(A). We need last column of V */
//
//    
//    cvSVD( matrA, matrW, matrU, matrV, CV_SVD_V_T );/* get transposed matrix V */
//
//    /* Compute number of non zero eigen values */
//    int numEig;
//    numEig = 0;
//    {
//        int i;
//        for( i = 0; i < 8; i++ )
//        {
//            if( cvmGet(matrW,i,i) < 1e-8 )
//            {
//                break;
//            }
//        }
//
//        numEig = i;
//
//    }
//
//    if( numEig < 5 )
//    {/* Bad points */
//        numFundMatrs = 0;
//    }
//    else
//    {
//        numFundMatrs = 1;
//
//        /* copy last row of matrix V to precomputed fundamental matrix */
//
//        CvMat preF;
//        cvGetRow(matrV,&preF,8);
//        cvReshape(&preF,preFundMatr,1,3);
//
//        /* Apply singularity condition */
//        icvMakeFundamentalSingular(preFundMatr);
//    
//        /* Denormalize fundamental matrix */
//        /* Compute transformation for normalization */
//
//        CvMat wfundMatr;
//        double wfundMatr_dat[9];
//        wfundMatr = cvMat(3,3,CV_64F,wfundMatr_dat);
//    
//        {/*  Freal = T1'*Fpre*T2 */
//            double tmpMatr_dat[9];
//            double tmptransfMatr_dat[9];
//        
//            CvMat tmpMatr = cvMat(3,3,CV_64F,tmpMatr_dat);
//            CvMat tmptransfMatr = cvMat(3,3,CV_64F,tmptransfMatr_dat);
//
//            cvTranspose(&transfMatr[0],&tmptransfMatr);
//        
//            cvmMul(&tmptransfMatr,preFundMatr,&tmpMatr);
//            cvmMul(&tmpMatr,&transfMatr[1],&wfundMatr);
//        }
//
//        /* scale fundamental matrix */
//        double fsc;
//        fsc = cvmGet(&wfundMatr,2,2);
//        if( fabs(fsc) > 1.0e-8 )
//        {
//            cvmScale(&wfundMatr,&wfundMatr,1.0/fsc);
//        }
//    
//        /* copy result fundamental matrix */
//        cvConvert( &wfundMatr, fundMatr );
//
//    }
//    
//    cvReleaseMat(&matrU);
//    cvReleaseMat(&matrW);
//    cvReleaseMat(&matrV);
//    cvReleaseMat(&preFundMatr);
//    cvReleaseMat(&matrA);
//    cvReleaseMat(&wpoints[0]);
//    cvReleaseMat(&wpoints[1]);
//    cvReleaseMat(&sqdists);
//
//    return numFundMatrs;
//}
