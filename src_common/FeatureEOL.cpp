
#include <assert.h>
#include <math.h>

#include "Vector.h"
#include "FeatureEOL.h"

//
// calc_eol_pixel()
//
// Calculates the entropy of a discrete probability mass distribution,
// centered at mean pfFeatureVec, 
//
float
calc_eol_pixel_sums(
			   const int &iRowStart,
			   const int &iColStart,
			   const int &iRows,
			   const int &iCols,
			   const float *pfFeatureVec,
			   FEATUREIO &fio2
			   )
{
	float fEntropySum = 0.0f;
	float fLikelihoodSum = 0.0f;
	float fEntropy = 0.0;

	int iRowFinish = iRowStart + iRows;
	int iColFinish = iColStart + iCols;

	//FILE *outfile = fopen( "prob.txt", "wt" );

	for( int y = iRowStart; y < iRowFinish; y++ )
	{
		for( int x = iColStart; x < iColFinish; x++ )
		{
			float fResult =
				//vector_normalized_dot_product( pfFeatureVec, fio2.pfVectors + (y*fio2.x + x)*fio2.iFeaturesPerVector, fio2.iFeaturesPerVector );
				vector_sqr_distance_varr( pfFeatureVec, fio2.pfVectors + (y*fio2.x + x)*fio2.iFeaturesPerVector, fio2.iFeaturesPerVector, fio2.pfVarrs );

			fResult = fResult;

			float fExp = exp( -fResult );

			//fprintf( outfile, "%100.100f\t", fExp );

			fLikelihoodSum += fExp;
			fEntropySum    += fExp*fResult;
		}
		//fprintf( outfile, "\n" );
	}
	//fclose( outfile );
	fEntropy = log( fLikelihoodSum ) + fEntropySum / fLikelihoodSum;
	return fEntropy;
}

//
// calc_eol_pixel_sums_robust()
//
// Here the energy functional is a robust error norm, so instead of
//
//  p(Error) \propto exp( -Error^2 / Varr )
//
// we have:
//
//  p(Error) \propto Varr / (Error^2 + Varr)
//
//
//
float
calc_eol_pixel_sums_robust(
			   const int &iRowStart,
			   const int &iColStart,
			   const int &iRows,
			   const int &iCols,
			   const float *pfFeatureVec,
			   FEATUREIO &fio2,
			   const float &fRobustVarr
			   )
{
	float fEntropySum = 0.0f;
	float fLikelihoodSum = 0.0f;
	float fEntropy = 0.0;

	int iRowFinish = iRowStart + iRows;
	int iColFinish = iColStart + iCols;

	//FILE *outfile = fopen( "prob.txt", "wt" );

	for( int y = iRowStart; y < iRowFinish; y++ )
	{
		for( int x = iColStart; x < iColFinish; x++ )
		{
			float fResult =
				//vector_normalized_dot_product( pfFeatureVec, fio2.pfVectors + (y*fio2.x + x)*fio2.iFeaturesPerVector, fio2.iFeaturesPerVector );
				vector_sqr_distance_varr( pfFeatureVec, fio2.pfVectors + (y*fio2.x + x)*fio2.iFeaturesPerVector, fio2.iFeaturesPerVector, fio2.pfVarrs );

			float fExp = fRobustVarr / (fRobustVarr + fResult);

			fResult = log( fExp );

			//fprintf( outfile, "%100.100f\t", fExp );

			fLikelihoodSum += fExp;
			fEntropySum    += fExp*fResult;
		}
		//fprintf( outfile, "\n" );
	}
	//fclose( outfile );
	fEntropy = log( fLikelihoodSum ) + fEntropySum / fLikelihoodSum;
	return fEntropy;
}

int
calculate_eol_image(
					const int &iRowOffset,	// Offset of a pixel in fio1 to fio2
					const int &iColOffset,
					const int &iRows,		// Domain size in fio2
					const int &iCols,		//
					FEATUREIO &fio1,
					FEATUREIO &fio2,
					FEATUREIO &fioEOL
			  )
{
	float fEntropyDivisor = 1.0f / log((float)iRows*iCols);

	int iPercentDone = (fio1.x*fio1.y)/100;

	int iRowsDiv2=iRows/2, iColsDiv2=iCols/2;

	for( int y = 0; y < fio1.y; y++ )
	{
		int iRow2Start = y + iRowOffset - iRowsDiv2;
		if( iRow2Start < 0 )
		{
			iRow2Start = 0;
		}
		else if( iRow2Start + iRows > fio2.y )
		{
			iRow2Start = fio2.y - iRows;
		}

		for( int x = 0; x < fio1.x; x++ )
		{			
			int iCol2Start = x + iColOffset - iColsDiv2;
			if( iCol2Start < 0 )
			{
				iCol2Start = 0;
			}
			else if( iCol2Start + iCols > fio2.x )
			{
				iCol2Start = fio2.x - iCols;
			}

			float fEntropy =
					//calc_eol_pixel(
					calc_eol_pixel_sums(
				iRow2Start, iCol2Start, 
				iRows, iCols,
				fio1.pfVectors + (y*fio1.x + x)*fio1.iFeaturesPerVector,
				fio2
				);

			// Normalize
			fEntropy *= fEntropyDivisor;

			//if( fEntropy < 0.0 || fEntropy > 1.0 ) { assert( 0 ); }
			fioEOL.pfVectors[y*fioEOL.x + x] = fEntropy;
		}
	}

	return 1;
}

int
calculate_eol_image_robust(
					const int &iRowOffset,	// Offset of a pixel in fio1 to fio2
					const int &iColOffset,
					const int &iRows,		// Domain size in fio2
					const int &iCols,		//
					FEATUREIO &fio1,
					FEATUREIO &fio2,
					FEATUREIO &fioEOL,
					const float &fRobustVarr
			  )
{
	float fEntropyDivisor = 1.0f / log((float)iRows*iCols);

	int iPercentDone = (fio1.x*fio1.y)/100;

	int iRowsDiv2=iRows/2, iColsDiv2=iCols/2;

	for( int y = 0; y < fio1.y; y++ )
	{
		int iRow2Start = y + iRowOffset - iRowsDiv2;
		if( iRow2Start < 0 )
		{
			iRow2Start = 0;
		}
		else if( iRow2Start + iRows > fio2.y )
		{
			iRow2Start = fio2.y - iRows;
		}

		for( int x = 0; x < fio1.x; x++ )
		{			
			int iCol2Start = x + iColOffset - iColsDiv2;
			if( iCol2Start < 0 )
			{
				iCol2Start = 0;
			}
			else if( iCol2Start + iCols > fio2.x )
			{
				iCol2Start = fio2.x - iCols;
			}

			float fEntropy =
					//calc_eol_pixel(
					calc_eol_pixel_sums_robust(
				iRow2Start, iCol2Start, 
				iRows, iCols,
				fio1.pfVectors + (y*fio1.x + x)*fio1.iFeaturesPerVector,
				fio2,
				fRobustVarr
				);

			// Normalize
			fEntropy *= fEntropyDivisor;

			//if( fEntropy < 0.0 || fEntropy > 1.0 ) { assert( 0 ); }
			fioEOL.pfVectors[y*fioEOL.x + x] = fEntropy;
		}
	}

	return 1;
}
