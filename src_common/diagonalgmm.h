
#ifndef __DIAGONALGMM__
#define __DIAGONALGMM__

#include "dataset.h"

typedef struct _DIAG_GMM
{
	int iComponents;
	int iFeaturesPerVector;
	double *pfWeights;
	double *pfMeans;
	double *pfVarriances;
} DIAG_GMM;

int
dgmmInit(
		 DIAG_GMM	&dgmm,
		 int		iComponents,
		 int		iFeaturesPerVector
		 );

int
dgmmInitRandom(
		 DIAG_GMM	&dgmm,
		 DATASET	&ds,
		 int		iComponents
		 );

//
// dgmmInitDistance()
//
// Initialize dgmm components by choosing points the furthest away from 
// already instantiated components
//
//
int
dgmmInitDistance(
		 DIAG_GMM	&dgmm,
		 DATASET	&ds,
		 int		iComponents
		 );

//
// dgmmInitBinaryInOutComponents()
//
// Initialize a 2-component mixture, where both components
// are centered on approximately the same mean value, with
// a peaky "in" component and a broad "out" background
// component.
//
int
dgmmInitBinaryInOutComponents(
		 DIAG_GMM	&dgmm,
		 DATASET	&ds,
		 int		iComponents
		 );

//
// dgmmTrain()
//
// Trains a GMM with, diagonal covariance matricies.
//
int
dgmmTrain(
		 DIAG_GMM	&dgmm,
		 DATASET	&ds,
		 int		iIterations
		  );

int
dgmmTrainLog(
		 DIAG_GMM	&dgmm,
		 DATASET	&ds,
		 int		iIterations
		  );

//
// dgmmVectorProbabilityComponent()
//
// Returns the probability of the vector comming from the
// iComponent component.
//
int
dgmmVectorProbabilityComponent(
		 DIAG_GMM	&dgmm,
		 int		iComponent,
		 double		*pfVector,
		 double		*pfProb
		  );

//
// dgmmVectorProbability()
//
// Probability of a single data vector.
//
int
dgmmVectorProbability(
		 DIAG_GMM	&dgmm,
		 double		*pfVector,
		 double		*pfProb
		  );

//
// dgmmVectorProbability()
//
// Probability of a dataset of vectors. pfProbs is an 
// array filled with the individual vector probabilities.
// pfProbSum is a single value representing the sum of
// all vector probabilities.
//
int
dgmmDatasetProbability(
		 DIAG_GMM	&dgmm,
		 DATASET	&ds,
		 double		*pfProbs = 0,	// Pointer to an array of double, one for each dataset vector
		 double		*pfProbSum = 0	// Pointer to a single double
		  );

int
dgmmWrite(
		  char		*pcFileName,
		  DIAG_GMM	&dgmm
		  );

int
dgmmRead(
		  char		*pcFileName,
		  DIAG_GMM	&dgmm
		  );

int
dgmmDelete(
		 DIAG_GMM	&dgmm
		 );

#endif

