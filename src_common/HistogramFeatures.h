

#ifndef _HISTOGRAM_FEATURES_H__
#define _HISTOGRAM_FEATURES_H__

#include "FeatureIO.h"
#include "PpImage.h"
#include "diagonalgmm.h"

#define MAX_GMM_CLASSES 5

void
hfGenerateHistogramFeatureVectorChar(
									 PpImage &ppImg,
									 int iRowStart,
									 int iColStart,
									 int iRows,
									 int iCols,
									 int iBins,
									 float *pfFeatures
									 );

int
hfGenerateHistogramFeaturesChar(
								PpImage &ppImg,
								FEATUREIO &fioHist,
								int iWindowRows,
								int iWindowCols
								);

void
hfGenerateHistogramFeatureVectorFloat(
									  FEATUREIO &fioImg,
									  int iRowStart,
									  int iColStart,
									  int iRows,
									  int iCols,
									  int iBins,
									  float *pfFeatures
									  );

int
hfGenerateHistogramFeaturesFloat(
								 FEATUREIO &fioImg,
								 FEATUREIO &fioHist,
								 int iWindowRows,
								 int iWindowCols
								 );

void
hfGenerateHistogramRangesVectorFloat(
									 FEATUREIO &fioImg,
									 int iRowStart,
									 int iColStart,
									 int iRows,
									 int iCols,
									 int iRanges,
									 float *pfRanges,
									 float *pfIntensities
									 );

typedef void HISTOGRAM_QUANTIZATION_FUNCTION(
											 FEATUREIO &fioImg,
											 int iRowStart,
											 int iColStart,
											 int iRows,
											 int iCols,
											 int iRanges,
											 float *pfRanges,
											 float *pfIntensities
											 );


void
hfGenerateHistogramRangesUniformProbabilityVectorFloat(
													   FEATUREIO &fioImg,
													   int iRowStart,
													   int iColStart,
													   int iRows,
													   int iCols,
													   int iRanges,
													   float *pfRanges,
													   float *pfIntensities
													   );

void
hfGenerateHistogramRangesUniformIntensityVectorFloat(
													 FEATUREIO &fioImg,
													 int iRowStart,
													 int iColStart,
													 int iRows,
													 int iCols,
													 int iRanges,
													 float *pfRanges,
													 float *pfIntensities
													 );

void
hfGenerateHistogramRangesLloydMaxVectorFloat(
											 FEATUREIO &fioImg,
											 int iRowStart,
											 int iColStart,
											 int iRows,
											 int iCols,
											 int iRanges,
											 float *pfRanges,
											 float *pfIntensities
											 );

void
hfGenerateHistogramRangesGaussianNormalizedVectorFloat(
													   FEATUREIO &fioImg,
													   int iRowStart,
													   int iColStart,
													   int iRows,
													   int iCols,
													   int iRanges,
													   float *pfRanges,
													   float *pfIntensities
													   );

int
hfGenerateHistogramRangesImageFloat(
									FEATUREIO &fioImg,
									FEATUREIO &fioRanges,
									int iWindowRows,
									int iWindowCols,
									HISTOGRAM_QUANTIZATION_FUNCTION *funcQuantization =
									hfGenerateHistogramRangesUniformProbabilityVectorFloat
									);


//
// hfInitHistogramRangesUniformImageFloat()
// 
// Initializes histogram ranges uniformly over global image histogram.
//
int
hfInitHistogramRangesUniformImageFloat(
							FEATUREIO &fioImg,
							FEATUREIO &fioRanges
							);

//
// hfInitHistogramRangesOrientationFloat()
//
// This function is unneeded - use hfInitHistogramRangesUniformImageFloat()
//
// Initializes an orientation histogram range uniformly over a range of
// orientations. Orientations are edge orientations defined as:
//
//		dy = image[y+1,x] - image[y-1,x]
//		dx = image[y,x+1] - image[y,x-1]
//
//		orientation = atan2( dy, dx )
//
// Orientations range from -PI to PI.
//
//int hfInitHistogramRangesOrientationFloat( FEATUREIO &fioImg, FEATUREIO &fioRanges );

//
// hfGenerateHistogram()
//
// Generates a histogram of an array of values, given
// a set of ranges. The resulting histogram is stored
// in pfBins. There should be iRanges+1 bins in pfBins.
//
void
hfGenerateHistogram(
					float *pfValues,
					int iValues,
					float *pfRanges,
					int iRanges,
					float *pfBins
					);

//
// hfHistogramBin()
//
// Returns the histogram bin of value fValue given a set of ranges.
//
int
hfHistogramBin(
			   float fValue,
			   float *pfRanges,
			   int iRanges
			   );

//
// hfGenerateMILikelihoodImage()
//
// Calculates mutual information between two images. Generates
// histograms based on the ranges in fioRanges(1,2).
//
// iInitNoiseLevel is an initial estimate on the joint relationship
// between intensities in the two images. The larger the amount of noise,
// the more data samples are need to overcome it. This has the effect of 
// stabilizing MI for sparse histograms. 0 is the default value.
//
int
hfGenerateMILikelihoodImage(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fioImg1,
					   FEATUREIO &fioImg2,
					   FEATUREIO &fioRanges1,
					   FEATUREIO &fioRanges2,
					   FEATUREIO &fioMI,
					   int iInitNoiseLevel,	// The initial uncertainty added to
					   int iSubStartRow = -1,
					   int iSubStartCol = -1,
					   int iSubStartRows = -1,
					   int iSubStartCols = -1
					   );
int
hfGenerateMILikelihoodImageLookupTable(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fioImg1,
					   FEATUREIO &fioImg2,
					   FEATUREIO &fioRanges1,
					   FEATUREIO &fioRanges2,
					   FEATUREIO &fioMI,
					   int iInitNoiseLevel,	// The initial uncertainty added to
					   int iSubStartRow = -1,
					   int iSubStartCol = -1,
					   int iSubStartRows = -1,
					   int iSubStartCols = -1
					   );


//
// hfGenerateHistogramEntropyImage()
//
// Calculates the entropy at each point in fioImg2 based
// on the ranges specified in fioRanges2, puts the result
// in fioEntropy.
//
int
hfGenerateHistogramEntropyImage(
					   FEATUREIO &fioImg2,
					   FEATUREIO &fioRanges2,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fioEntropy
					   //,FEATUREIO &fioHistWeights
							);

//
// hfGenerateHistogramEntropyImage()
//
// Calculates the entropy at each point in fioImgPix, based on
// the intensities in fioImgPix and the orientations of edges. Weights
// are optionally applied to the orientations.
//
int
hfGenerateOrientationIntensityEntropyImage(
					   FEATUREIO &fioImgPix,
					   FEATUREIO &fioImgOri,
					   FEATUREIO &fioRangesPix,
					   FEATUREIO &fioRangesOri,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fioEntropy,
					   FEATUREIO &fioHistWeights
							);



//
// hfCalculate_eol_image_MI()
//
// Generates an EOL image for MI matching.
//
int
hfCalculate_eol_image_MI(
					   const int &iRowOffset,	// Offset of a pixel in fioImg1 to fioImg2
					   const int &iColOffset,
					   
					   const int &iRows,		// Domain size in fioImg2
					   const int &iCols,		//
					   
					   FEATUREIO &fioImg1,		// Image 1
					   FEATUREIO &fioImg2,		// Image 2

					   FEATUREIO &fioRanges1,	// Histogram ranges at locations in image 1
					   FEATUREIO &fioRanges2,	// Histogram ranges at locations in image 1

					   FEATUREIO &fioEOL,
					   
					   const int &iRowSize, // Size of template window
					   const int &iColSize, 
					   
					   const float &fMIVarrDiv,		// Varriance term for NCC energy

					   FEATUREIO &fioImgMask	// Mask, process pixel if mask(pixel) == 255
					   );



int
hfCalculate_eol_image_MI_scale(
					   const int &iRowOffset,	// Offset of a pixel in fioImg1 to fioImg2
					   const int &iColOffset,
					   
					   const int &iRows,		// Domain size in fioImg2
					   const int &iCols,		//
					   
					   FEATUREIO &fioImg1,		// Image 1
					   FEATUREIO &fioImg2,		// Image 2

					   FEATUREIO &fioRanges1,	// Histogram ranges at locations in image 1
					   FEATUREIO &fioRanges2,	// Histogram ranges at locations in image 1

					   FEATUREIO &fioEOL,
					   
					   const int &iRowSize, // Size of template window
					   const int &iColSize, 
					   
					   const float &fMIVarrDiv,		// Varriance term for NCC energy

					   FEATUREIO &fioImgMask,
					   const int &iScales // The number of scales to calculate the MI
						);

//
//
///////////////////////////////////////////////////
//
// Here are the probabilistic MI functions. If this doesn't
// work, I quit.
//
// Well, it works, sort of, so I won't quit just yet! Now I need
// a good probabilistic classification algorithm.
//

//
// hfGenerateProbMILikelihoodImage()
//
// Calculates the MI likeliood image from point iRow,iCol in
// fio1 to fio2, using a window size of iRowSize,iColSize.
//  * fio1 and fio2 are class conditional probabilities
// over abstract classes. Much more computationally intensive.
//
// 11:33 AM 10/14/2008: added likelihood weights
//
int
hfGenerateProbMILikelihoodImage(
								int iRow,
								int iCol,
								int iRowSize,
								int iColSize,
								FEATUREIO &fio1,
								FEATUREIO &fio2,
								FEATUREIO &fioMI,
								int iWindow = -1 // Optinal local search window size
								);


//
// hfGenerateProbMILikelihoodImage()
//
// Generalizes hfGenerateProbMILikelihoodImage() to 3D,
// hopefully more intiutive interface with optional search range in fio2.
//
//
//
int
hfGenerateProbMILikelihoodImage(
								int *piCoord1,	// Center of box in fio1, xyzt, col/row/z/time
								int *piSize1,  // Size of box in fio1
								int *piCoord2,	// Center of box (search area) in fio2
								int *piSize2,	// Center of box (search area) in fio2
								FEATUREIO &fio1,
								FEATUREIO &fio2,
								FEATUREIO &fioMI
							);

int
hfGenerateLikelihoodImageCorrelation(
								int *piCoord1,	// Center of box in fio1, xyzt, col/row/z/time
								int *piSize1,  // Size of box in fio1
								int *piCoord2,	// Center of box (search area) in fio2
								int *piSize2,	// Center of box (search area) in fio2
								FEATUREIO &fio1,
								FEATUREIO &fio2,
								FEATUREIO &fioMI
							);

int
hfGenerateLikelihoodImageSumAbsDiff(
								int *piCoord1,	// Center of box in fio1, xyzt, col/row/z/time
								int *piSize1,  // Size of box in fio1
								int *piCoord2,	// Center of box (search area) in fio2
								int *piSize2,	// Center of box (search area) in fio2
								FEATUREIO &fio1,
								FEATUREIO &fio2,
								FEATUREIO &fioMI
							);



//
//
//
// This is added strickly to see how well orientation histograms can 
// be matched without MI!
//
int
hfGenerateProbDiffLikelihoodImage(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fio1,
					   FEATUREIO &fio2,
					   FEATUREIO &fioMI
							);

int
hfGenerateProbMILikelihoodImageLookupTableHyperGeometric(
								int iRow,
								int iCol,
								int iRowSize,
								int iColSize,
								FEATUREIO &fio1,
								FEATUREIO &fio2,
								FEATUREIO &fioMI,
								float *pfPrior = 0
								);


int
hfGenerateProbMILikelihoodImageHyperGeometricIntergerCountsNonNormalized(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fio1,
					   FEATUREIO &fio2,
					   FEATUREIO &fioMI,
						float *pfPrior = 0
							);

float
hfCalculateRegionEntropyLookupTableHyperGeometric(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fio1,
					   FEATUREIO &fio2,
					   FEATUREIO &fioMI,
						float *pfPrior
							);

int
hfGenerateProbMILikelihoodImageLookupTable(
								int iRow,
								int iCol,
								int iRowSize,
								int iColSize,
								FEATUREIO &fio1,
								FEATUREIO &fio2,
								FEATUREIO &fioMI
								);

//
// hfGenerateProbMILikelihoodImageMasked()
//
// Same as hfGenerateProbMILikelihoodImage(), 
// but uses a mask ppImgMask to disregard certain pixels.
// All pixels flagged 0 in ppImgMask are disregarded.
//
int
hfGenerateProbMILikelihoodImageMasked(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fio1,
					   PpImage &ppImgMask, // Mask iWindowRows x iWindowCols
					   FEATUREIO &fio2,
					   FEATUREIO &fioMI
							);

//
// hfGenerateProbMI_ConditionalMarkov4_LikelihoodImage()
//
// Year: 2008
// Investigate entropy rate of aligned images as a measure of similarity
//
// Markov dependency between adjacent samples is assumed.
//
// In the case of independent and identically distributed samples, the
// entropy rate is equal to the joint entropy.
//
int
hfGenerateProbMI_ConditionalMarkov4_LikelihoodImage(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fio1,
					   FEATUREIO &fio2,
					   FEATUREIO &fioHRate
					   );

//
// hfGenerateProbMI_SpatialPattern_LikelihoodImage()
//
// Mutual information conditional on the binary spatial
// sampling pattern in ppImgN.
//
int
hfGenerateProbMI_SpatialPattern_LikelihoodImage(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fio1,
					   PpImage &ppImgN, // Spatial pattern (binary, 0, 1 or (-1/255) for no sample)
					   FEATUREIO &fio2,
					   FEATUREIO &fioHRate
							);

//
// hfGenerateProbMI_ConditionalMarkov1_LikelihoodImage()
//
// Mutual information conditional on neighbors specified in ppImgN.
//
int
hfGenerateProbMI_ConditionalMarkov1_LikelihoodImage(
					   int iRow,
					   int iCol,
					   int iWindowRows,
					   int iWindowCols,
					   FEATUREIO &fio1,
					   PpImage &ppImgN, // MRF neighbour indices in fio1
					   FEATUREIO &fio2,
					   FEATUREIO &fioHRate
					   );

//
//
//
//
int
hfGenerateProbHistogramEntropyImage(
									int iWindowRows,
									int iWindowCols,
									FEATUREIO &fio1,
									FEATUREIO &fioMI,
									PpImage &ppImgMask
									);


//
// hfGenerateProbHistogramLocationEntropyImage()
//
// Here we generate the entropy of pixel class given
// location. We do not assume pixel intensity is independent
// of location.
//
//	H(I|X) = sum_x^N p(x) H(I|X=x)
//
int
hfGenerateProbHistogramLocationEntropyImage(
									int iWindowRows,
									int iWindowCols,
									FEATUREIO &fio1,
									FEATUREIO &fioMI
									);


//
// hfGenerateProbHistogramEntropyRateImage()
//
// Calculates the entropy rate within windows
// over the image. The entropy rate treats image windows
// as a MRF, and calculates the average number of bits
// required to encode the window.
//
// H(S) = -Sum_i pi[ Sum_j p_ij log( p_ij ) ]
//
int
hfGenerateProbHistogramEntropyRateImage(
										int iWindowRows,
										int iWindowCols,
										FEATUREIO &fio1,
										FEATUREIO &fioMI
										);

//
//
///////////////////////////////////////////////////
//
// These functions calculate entropy and mutual information
// for images who's pixels are integer labels. In general,
// there can be up to NxN unique labels in an NxN image, 
// therefore a special means of calculating entropies is necessary.
//


//
// hfGenerateProbMILikelihoodImage()
//
// Calculates the MI likeliood image from point iRow,iCol in
// fio1 to fio2, using a window size of iRowSize,iColSize.
//  * fio1 and fio2 are class conditional probabilities
// over abstract classes. Much more computationally intensive.
//
//
int
hfGenerateLabelMILikelihoodImage(
								int iRow,
								int iCol,
								int iWindowRows,
								int iWindowCols,
								FEATUREIO &fio1,	// Images of integers
								FEATUREIO &fio2,	// Images of integers
								FEATUREIO &fioMI
								);

//
//
//
//
int
hfGenerateLabelHistogramEntropyImage(
									int iWindowRows,
									int iWindowCols,
									FEATUREIO &fio1,
									FEATUREIO &fioMI
									);


//
// This GMM stuff is built sort of add-hoc, the code in
// DiagonalGMM.(h/cpp) should be used instead of these
// definitions.
//

#ifdef CHANGE_THIS_GMM_CODE

typedef struct _DIAG_GMM
{
	int iClasses;
	float pfMeans[MAX_GMM_CLASSES];
	float pfVariances[MAX_GMM_CLASSES];
	float pfPriors[MAX_GMM_CLASSES];
} DIAG_GMM;

//
// hfGenerateHistogramRangesGMMVectorFloat()
//
// Generates a diagonal GMM based on data in a window.
//
void
hfGenerateHistogramRangesGMMVectorFloat(
										
										FEATUREIO &fioImg,
										int iRowStart,
										int iColStart,
										int iRows,
										int iCols,

										DIAG_GMM &dgmm,
										
										float *pfIntensities,    // size iRows*iCols
										float *pfTempArray		// size iClasses*iRows*iCols
										);
//
// hfGenerateMILikelihoodGMMImage()
//
// Calculates mutual information based on
// GMM histogram representations.
//
int
hfGenerateMILikelihoodGMMImage(
							   int iRow,
							   int iCol,
							   int iWindowRows,
							   int iWindowCols,
							   FEATUREIO &fioImg1,
							   FEATUREIO &fioImg2,
							   FEATUREIO &fioGMM1,
							   FEATUREIO &fioGMM2,
							   FEATUREIO &fioMI
							   );

int
hfGenerateHistogramGMMsImageFloat(
							FEATUREIO &fioImg,
							FEATUREIO &fioGMMs,
							int iWindowRows,
							int iWindowCols,
							int iClasses = MAX_GMM_CLASSES
							);
#endif


#endif
