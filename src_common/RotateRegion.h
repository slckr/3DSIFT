
#ifndef __ROTATEREGION_H__
#define __ROTATEREGION_H__

#include "PpImage.h"

//
// Usage:
//
//		Angles: adding a positive increment to the angle rotates the
//			image counter-clockwise. Angle 0 
//
//
//
//
//

//
// rotate_region()
//
// Rotates and magnifies in the row & col directions a region in an input
// image, puts the result in an output image.
//
void
rotate_region(
			  const PpImage &ppImg,	// Input image
			  PpImage &ppImgOut,	// Output image (dimensions iRows,iCols)
			  const float &angle,	// Rotation angle
			  const float &mag_row,	// Row magnification
			  const float &mag_col,	// Col magnification
			  const int iRow,		// Start row of region in ppImg 
			  const int iCol,		// Start col of region ppImg
			  const int iRows,		// Number of rows of region in ppImg
			  const int iCols		// Number of cols of region in ppImg
			  );

//
// rotate_region_circular_float()
//
// Rotates the circular region centered on pixel (iCenterRow,iCenterCol)
// in ppImgFloat, copies the result into ppImgOutFloat. The pixel
// at ppImgFloat(iCenterRow,iCenterCol) will correspond to 
// the pixel at ppImgOutFloat(iRadius+1,iRadius+1).
//
void
rotate_region_circular(
			  const PpImage &ppImg,	// Input image
			  PpImage &ppImgOut,	// Output image (dimensions iRows,iCols)
			  const float &angle,	// Rotation angle
			  const float &mag_row,	// Row magnification
			  const float &mag_col,	// Col magnification
			  const int iCenterRow,
			  const int iCenterCol,
			  const int iRadius
			  );

//
// rotate_region_float()
//
// Same thing, but with an image of float.
//
void
rotate_region_float(
			  const PpImage &ppImgFloat,
			  PpImage &ppImgOutFloat,
			  const float &angle,	// Rotation angle
			  const float &mag_row,	// Row magnification
			  const float &mag_col,	// Col magnification
			  const int iRow,
			  const int iCol,
			  const int iRows,
			  const int iCols,
			  float fCenterRow = -1,	// Center of rotation
	          float fCenterCol = -1		// Center of rotation
			  );


//
// rotate_region_circular_float()
//
// Rotates the circular region centered on pixel (iCenterRow,iCenterCol)
// in ppImgFloat, copies the result into ppImgOutFloat. The pixel
// at ppImgFloat(iCenterRow,iCenterCol) will correspond to 
// the pixel at ppImgOutFloat(iRadius+1,iRadius+1).
//
void
rotate_region_circular_float(
			  const PpImage &ppImgFloat,
			  PpImage &ppImgOutFloat,
			  const float &angle,	// Rotation angle
			  const float &mag_row,	// Row magnification
			  const float &mag_col,	// Col magnification
			  const int iCenterRow,
			  const int iCenterCol,
			  const int iRadius
			  );

//
// rotate_region_circular_float()
//
// Rotates the circular region centered on pixel (iCenterRow,iCenterCol)
// in ppImgFloat, copies the result into ppImgOutFloat. The pixel
// at ppImgFloat(iCenterRow,iCenterCol) will correspond to 
// the pixel at ppImgOutFloat(iRadius+1,iRadius+1).
//
void
rotate_region_circular(
			  const PpImage &ppImg,	// Input image
			  PpImage &ppImgOut,	// Output image (dimensions iRows,iCols)

			  // This is all relating to input image ppImg

			  const float &angle,	// Rotation angle
			  const float &mag_row,	// Row magnification
			  const float &mag_col,	// Col magnification
			  const int iCenterRow,
			  const int iCenterCol,
			  const int iRadius,

			  // This is the center of the output image ppImgOut
			  const int iOutCenterRow = 0,
			  const int iOutCenterCol = 0
			  );

void
rotate_region_circular_float_coord(
			  const PpImage &ppImg,	// Input image
			  PpImage &ppImgOut,	// Output image (dimensions iRows,iCols)
			  const float &angle,	// Rotation angle
			  const float &mag_row,	// Row magnification
			  const float &mag_col,	// Col magnification
			  const float fCenterRow,
			  const float fCenterCol,
			  // This is the center of the output image ppImgOut
			  const int iRadius,
			  const int iOutCenterRow,
			  const int iOutCenterCol
			  );

//
//
//
//
//
//

//
// coordsToSimilarity()
//
// Converts two (x,y) coordinates to similarity transform parameters.
//
void
coordsToSimilarity(
				   float x1, float y1, float x2, float y2,
				   float &x, float &y, float &angle, float &mag
				   );


void
similarity_transform_coord(
						   float x1, float y1,
						   float &x2, float &y2,
			  const float &angle,	// Rotation angle
			  const float &mag,	// Magnification
			  const float fCenterRow1,  // Center of rotation
			  const float fCenterCol1,  //     Image1
			  const float fCenterRow2,  // Center of rotation
			  const float fCenterCol2   //     Image2
			  );

//
// similarity_transform_image()
//
// Applies a similarity transform to map ppImg1 into
// the space of ppImg2. Both images are initialized.
// Pixels in ppImg2 falling outside the bounds of ppImg1
// are set to 0.
//
void
similarity_transform_image(
			  const PpImage &ppImg1,	// Input image
			  PpImage &ppImg2,	// Output image (dimensions iRows,iCols)
			  const float &angle,	// Rotation angle
			  const float &mag,	// Magnification
			  const float fCenterRow1,  // Center of rotation
			  const float fCenterCol1,  //     Image1
			  const float fCenterRow2,  // Center of rotation
			  const float fCenterCol2   //     Image2
			  );

// Same but for a floating point output image
void
similarity_transform_image_float(
			  const PpImage &ppImg1,	// Input image
			  PpImage &ppImg2,	// Output image (dimensions iRows,iCols) floating
			  const float &angle,	// Rotation angle
			  const float &mag,	// Magnification
			  const float fCenterRow1,  // Center of rotation
			  const float fCenterCol1,  //     Image1
			  const float fCenterRow2,  // Center of rotation
			  const float fCenterCol2   //     Image2
			  );


//
//
// Homography: Deformation of a 3D plane in 2D images from different viewpoints.
//
//

typedef double MATRIX3x3[3][3];
typedef double POINT3x1[3];

//
//
// coords_to_homography()
//
// Determine homographic transform parameters from four points.
//
int
coords_to_homography(
					 POINT3x1 i1[4],
					 POINT3x1 i2[4],
					 MATRIX3x3 &H
					 );

//
// homography_transform_image()
//
// Applies a homography transform to map ppImg1 into
// the space of ppImg2. Both images are initialized.
// Pixels in ppImg2 falling outside the bounds of ppImg1
// are set to 0.
//
void
homography_transform_image(
						   const PpImage &ppImg1,	// Input image
						   PpImage &ppImg2,	// Output image
						   MATRIX3x3 &H // Homography
						   );

#endif
