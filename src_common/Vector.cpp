
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "Vector.h"

float
vector_sqr_distance(
				 const float *pfVec1,
				 const float *pfVec2,
				 const int iVecLength
				 )
{
	float fSqrDiffSum = 0.0f;
	for( int i = 0; i < iVecLength; i++ )
	{
		float fDiff = pfVec1[i] - pfVec2[i];
		fSqrDiffSum += fDiff*fDiff;
	}
	return fSqrDiffSum;
}



float
vector_dot_product(
				 const float *pfVec1,
				 const float *pfVec2,
				 const int iVecLength
				 )
{
	float fSum = 0.0f;
	for( int i = 0; i < iVecLength; i++ )
	{
		fSum += pfVec1[i]*pfVec2[i];
	}
	return fSum;
}

float
vector_normalized_dot_product(
				 const float *pfVec1,
				 const float *pfVec2,
				 const int iVecLength
				 )
{
	return vector_dot_product(pfVec1,pfVec2,iVecLength) / sqrt( vector_dot_product(pfVec1,pfVec1,iVecLength)*vector_dot_product(pfVec2,pfVec2,iVecLength) );
}

float
vector_sqr_mag(
				 const float *pfVec1,
				 const int iVecLength
				 )
{
	return vector_dot_product( pfVec1, pfVec1, iVecLength );
}
