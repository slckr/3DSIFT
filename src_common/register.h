
#ifndef __REGISTER_H__
#define __REGISTER_H__

#include "Posterior.h"
#include "output_functions.h"
#include "FeatureIO.h"

typedef struct _PRIOR_STRUCT
{
	// Origin of variable

	int iVarX;
	int iVarY;
	int iVarZ;

	float fScale;

	// Expected square distances

	int iDistSqrX;	int iDistSqrY;	int iDistSqrZ; // not used

	int iDistSqr;
	int iDistQuad;
	float fDenominator;

	// 

} PRIOR_STRUCT;

//
// Some big number defines some maximum exponent value
// corresponding to some minumum probability value
//
#define MIN_EXPONENT -999.0e+20


typedef float CALCUATE_VALUE_FUNC(
								  LOCATION_VALUE_XYZ &lv,
								  FEATUREIO &fio
								  );

//
// regFindPeaks()
//
// Finds peaks in a FEATUREIO image, returns
//

int
regFindFEATUREIOPeaks(
					  LOCATION_VALUE_XYZ_ARRAY &lvaPeaks,
					  FEATUREIO &fio,
					  CALCUATE_VALUE_FUNC *pfunkValue = 0
					  );

//
// regFindFEATUREIOValleys()
//
// Finds valleys in a FEATUREIO image, returns
//

int
regFindFEATUREIOValleys(
					  LOCATION_VALUE_XYZ_ARRAY &lvaPeaks,
					  FEATUREIO &fio,
					  CALCUATE_VALUE_FUNC *pfunkValue = 0
					  );

//
// regFindLikelihoodPeaks()
//
// Finds likelihood peaks for a set of reference vectors. The likelihood peaks
// are returned as exponents.
//
int
regFindLikelihoodPeaks(
					   float *pfRefVectors,			// Array of reference vectors
					   LOCATION_VALUE_XYZ_COLLECTION &lvcPeaks,
					   FEATUREIO &fio
					   );

//
// regFindLikelihoods()
//
// Returns likelihoods as exponents.
//
int
regFindLikelihoodPeaks(
			   FEATUREIO &fio1,
			   FEATUREIO &fio2,
			   LOCATION_VALUE_XYZ_ARRAY &lvaVariables,	// Array of points in fio1
			   LOCATION_VALUE_XYZ_COLLECTION &lvcMarginals	// Array of likelihoods in fio2
			   );
#define regFindLikelihoods regFindLikelihoodPeaks

//
// regReadLikelihoodPeaks()
//
// Reads likelihood FEATUREIO files. The files are of the name:
// fprintf( name, pcFileName, 0 ) to fprintf( name, pcFileName, lvaVariables.iCount ).
// The files contain images of the likelihood exponent.
//
int
regReadLikelihoodPeaks(
				   char *pcFileNameBase,
				   LOCATION_VALUE_XYZ_ARRAY &lvaVariables,	// Array of points in fio1
				   LOCATION_VALUE_XYZ_COLLECTION &lvcMarginals	// Array of likelihoods in fio2
				   );
#define regReadLikelihoods regReadLikelihoodPeaks

//
// regReadLikelihoodRegions()
//
// Reads in likelihoods, but keeps a list of pixels in a region. The region
// is defined by a window of size (iXWindowSize,iYWindowSize,iZWindowSize),
// centered on the point origins lvaVariables.lvxOrigin.
//
//
int
regReadLikelihoodRegions(
				   char *pcFileNameBase,
				   LOCATION_VALUE_XYZ_ARRAY &lvaVariables,	// Array of points in fio1
				   LOCATION_VALUE_XYZ_COLLECTION &lvcMarginals,	// Array of likelihoods in fio2
				   int iXWindowSize,
				   int iYWindowSize,
				   int iZWindowSize,
				   FEATUREIO &fioTemp1,
				   FEATUREIO &fioTemp2
				   );

//
// regEnforceGeometricPrior()
//
// Enforces the geometric prior of psNeighbour on lvaVarrCurr. The prior
// is exponential, and exponents are added to lvaVarrCurr.
//
void
regEnforceGeometricPrior(
						LOCATION_VALUE_XYZ_ARRAY	&lvaVarrCurr,		// Current variable on which to enforce geometric prior
						LOCATION_VALUE_XYZ			&lvaNeighbourInstance,		// Current variable on which to enforce geometric prior
						PRIOR_STRUCT				&psNeighbour
						);

//
// regFindTransformMinProbCuttoff()
//
// This function is the same as find_transform_bruteforce(), but
// uses an alpha-beta tree search strategy. Uses the variable ordering
// as specified in piVariableOrdering. piVariableEvidence provides
// fixed evidence in the network, if piVariableEvidece[i] is non-negative, it
// specifies the fixed instantiation of variable piVariableOrdering[i].
//
float
regFindTransformMinProbCuttoff(
								int *piVariableOrdering,
								int *piVariableEvidence,
								LOCATION_VALUE_XYZ_COLLECTION &lvcLikes,
								PpImage &ppImgNeighbours,
								LOCATION_VALUE_XYZ_ARRAY &lvaBestSolution
								);

//
// regGreedySample()
//
// Performs a greedy sample, based on evidence, choosing instances
// according to maximum probability in the sampling order.
//
float
regGreedySample(
				int *piVariableOrdering,
				int *piVariableEvidence,
				LOCATION_VALUE_XYZ_COLLECTION &lvcLikes,
				PpImage &ppImgNeighbours,
				LOCATION_VALUE_XYZ_ARRAY &lvaSample
				);


//
// regGreedyAscentSample()
//
// Performs greedy ascent of the probability space, starting
// at the sample point. Variable order is not considered - each
// move is made such that the greatest increase in probability.
//
float
regGreedyAscentSample(
				int *piVariableOrdering,
				int *piVariableEvidence,
				LOCATION_VALUE_XYZ_COLLECTION &lvcLikes,
				PpImage &ppImgNeighbours,
				LOCATION_VALUE_XYZ_ARRAY &lvaSample
				);

//
// regSampleProbability()
//
// Returns the individual likelihood and prior probabilities for
// each point in lvaSample. Points in lvaSample
// correspond to variables in lvcLikes. lvcLikes contains the 
// likelihood and the prior information to calculate the probabiltiy.
// Likelihood and prior probabilities are filled into lvaLikes and lvaPrior.
//
float
regSampleProbability(
				   LOCATION_VALUE_XYZ_ARRAY			&lvaSample,
				   PpImage							&ppImgNeighbours,
				   LOCATION_VALUE_XYZ_COLLECTION	&lvcLikes,
				   LOCATION_VALUE_XYZ_ARRAY			&lvaLikes,
				   LOCATION_VALUE_XYZ_ARRAY			&lvaPrior
				   );

//
// regSampleProbability()
//
// Returns the probability of a sample. Points in lvaSample
// correspond to variables in lvcLikes. lvcLikes contains the 
// likelihood and the prior information to calculate the probabiltiy.
//
float
regSampleProbability(
				   LOCATION_VALUE_XYZ_ARRAY			&lvaSample,
				   PpImage							&ppImgNeighbours,
				   LOCATION_VALUE_XYZ_COLLECTION	&lvcLikes
				   );

//
// regSampleToIndices()
//
// Retrieves the indices of the samples of lvaSample in lvaInstances.
//
void
regSampleToIndices(
				   int *piIndices,
				   LOCATION_VALUE_XYZ_ARRAY &lvaSample,
				   LOCATION_VALUE_XYZ_COLLECTION &lvcInstances
				   );

//
// regIndicesToSample()
//
// Retrieves the samples in lvaInstances corresponding to the indices in
// piIndices.
//
void
regIndicesToSample(
				   int *piIndices,
				   LOCATION_VALUE_XYZ_ARRAY &lvaSample,
				   LOCATION_VALUE_XYZ_COLLECTION &lvcInstances
				   );

//
// regPosteriorProbability()
//
// Returns the posterior probabilities of a transform point given a set of
// fixed transform points.
//
int
regSampleProbability(
				   LOCATION_VALUE_XYZ_ARRAY			&lvaLikelihoods,// list of likelihood values for a new point
				   LOCATION_VALUE_XYZ_COLLECTION	&lvcFixedPoints		// list of fixed transform points
				   );

//
// regPosteriorProbability()
//
// Converts a list of likelihood values to their posterior probabilities,
// given a set of fixed transform points.
// Returns the index of the maximum posterior value in lvaLikelihoods.
//
int
regPosteriorProbability(
				   LOCATION_VALUE_XYZ_ARRAY			&lvaLikelihoods,	// list of likelihood values for a new point
				   LOCATION_VALUE_XYZ_COLLECTION	&lvcFixedPoints,	// list of fixed transform points
				   int bgFlag											// 1 for basal ganglion to full brain, 0 for
																		// same size matches
				   );


int
regCalculate_likelihood(
					 float *pfFeatures,
					 FEATUREIO &fioDomain,
					 FEATUREIO &fioLike
					 );

int
regCalculate_likelihood(
					 int iRow,
					 int iCol,
					 FEATUREIO &fio1,
					 FEATUREIO &fio2,
					 FEATUREIO &fioLike
					 );

int
regCalculate_likelihood_robust(
					 float *pfFeatures,
					 FEATUREIO &fioDomain,
					 FEATUREIO &fioLike,
					 const float &fRobustVarr
					 );

int
regCalculate_likelihood_robust(
					 int iRow,
					 int iCol,
					 FEATUREIO &fio1,
					 FEATUREIO &fio2,
					 FEATUREIO &fioLike,
					 const float &fRobustVarr
					 );

#endif
