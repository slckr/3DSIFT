#define _USE_MATH_DEFINES

#include <stdio.h>
#include <assert.h>
#include <functional>
#include "src_common.h"
#include "FeatureIOnifti.h"

//#include "AffineTransform3D.h"
#include <time.h>
//#include <sys/timeb.h>

#define INCLUDE_FLANN
#ifdef INCLUDE_FLANN
#include "flann/flann.h"
#endif

#include <algorithm>
#include <vector>
#include <map>
#include <cmath>
using namespace std;

int
removeZValue(
	vector<Feature3DInfo> &vecFeats
)
{
	int iRemoveCount = 0;
	for (int i = 0; i < vecFeats.size(); i++)
	{
		if (!(vecFeats[i].z > 400))
		{
			vecFeats.erase(vecFeats.begin() + i);
			i--;
			iRemoveCount++;
		}
	}
	return iRemoveCount;
}

int
removeNonReorientedFeatures(
	vector<Feature3DInfo> &vecFeats
)
{
	int iRemoveCount = 0;
	for (int i = 0; i < vecFeats.size(); i++)
	{
		if (!(vecFeats[i].m_uiInfo & INFO_FLAG_REORIENT))
		{
			vecFeats.erase(vecFeats.begin() + i);
			i--;
			iRemoveCount++;
		}
	}
	return iRemoveCount;
}

int
removeReorientedFeatures(
	vector<Feature3DInfo> &vecFeats
)
{
	int iRemoveCount = 0;
	for (int i = 0; i < vecFeats.size(); i++)
	{
		if ((vecFeats[i].m_uiInfo & INFO_FLAG_REORIENT))
		{
			vecFeats.erase(vecFeats.begin() + i);
			i--;
			iRemoveCount++;
		}
		else
		{
			memset(&vecFeats[i].ori[0][0], 0, sizeof(vecFeats[i].ori));
			vecFeats[i].ori[0][0] = 1;
			vecFeats[i].ori[1][1] = 1;
			vecFeats[i].ori[2][2] = 1;
		}
	}
	return iRemoveCount;
}

int
msComputeNearestNeighborDistanceRatioInfo(
	vector<Feature3DInfo> &vecFeats1,
	vector<Feature3DInfo> &vecFeats2,
	vector<int>		&vecMatchIndex12,
	vector<float>		&vecMatchDist12
)
{
	int iCompatible = 0;

	int iPCs = PC_ARRAY_SIZE;

	for (int i = 0; i < vecFeats1.size(); i++)
	{
		float fMinDist = vecFeats1[i].DistSqrPCs(vecFeats2[0], iPCs);
		int iMinDistIndex = 0;

		float fMinDist2 = vecFeats1[i].DistSqrPCs(vecFeats2[1], iPCs);
		int iMinDist2Index = 1;
		if (fMinDist2 < fMinDist)
		{
			float fTemp = fMinDist;
			fMinDist = fMinDist2;
			fMinDist2 = fTemp;
			iMinDistIndex = 1;
			iMinDist2Index = 0;
		}

		for (int j = 2; j < vecFeats2.size(); j++)
		{
			float fDist = vecFeats1[i].DistSqrPCs(vecFeats2[j], iPCs);
			if (fDist < fMinDist2)
			{
				// 1st and 2nd matches shoud not be compatible

				if (fDist < fMinDist)
				{
					if (!compatible_features(vecFeats2[j], vecFeats2[iMinDistIndex])) // Add this line for dense features
					{
						// Closest feature is not geometrically compatible with first
						// Shuffle down 1st and 2nd nearest distances
						fMinDist2 = fMinDist;
						iMinDist2Index = iMinDistIndex;
						fMinDist = fDist;
						iMinDistIndex = j;
					}
					else
					{
						// Closest feature is geometrically compatible with first
						// Replace only 1st feature, we have found a better instance of the same thing
						fMinDist = fDist;
						iMinDistIndex = j;
					}
				}
				else
				{
					if (!compatible_features(vecFeats2[j], vecFeats2[iMinDistIndex])) // Add this line for dense features
					{
						// Second closest feature is not geometrically compatible with first
						// Shuffle down 2nd nearest distance
						fMinDist2 = fDist;
						iMinDist2Index = j;
					}
					else
					{
						//printf( "worse \n" );
						// Second closest feature is geometrically compatible with first
						// It is a worse instance of the current closest feature, ignore it
					}
				}
			}
		}

		if (compatible_features(vecFeats2[iMinDist2Index], vecFeats2[iMinDistIndex]))
		{
			iCompatible++;
			//fMinDist = 1;
			//fMinDist2 = 10000;
		}

		vecMatchIndex12.push_back(iMinDistIndex);
		//vecMatchDist12.push_back( fMinDist );
		vecMatchDist12.push_back(fMinDist / fMinDist2);
	}
	return 0;
}

//
// October 2014 - 2ndNeighbor functions introduced to identify similar content in images, for duplicate feature detection.
// Return closest 2nd nearest neighbor. This is intended for matching an image to itself.
//
int
msComputeNearest2ndNeighbor(
	vector<Feature3DInfo> &vecFeats1,
	vector<Feature3DInfo> &vecFeats2,
	vector<int>		&vecMatchIndex12,
	vector<float>		&vecMatchDist12
)
{
	int iCompatible = 0;

	int iPCs = PC_ARRAY_SIZE;

	for (int i = 0; i < vecFeats1.size(); i++)
	{
		float fMinDist = vecFeats1[i].DistSqrPCs(vecFeats2[0], iPCs);
		int iMinDistIndex = 0;

		float fMinDist2 = vecFeats1[i].DistSqrPCs(vecFeats2[1], iPCs);
		int iMinDist2Index = 1;
		if (fMinDist2 < fMinDist)
		{
			float fTemp = fMinDist;
			fMinDist = fMinDist2;
			fMinDist2 = fTemp;
			iMinDistIndex = 1;
			iMinDist2Index = 0;
		}

		for (int j = 2; j < vecFeats2.size(); j++)
		{
			float fDist = vecFeats1[i].DistSqrPCs(vecFeats2[j], iPCs);
			if (fDist < fMinDist2)
			{
				// 1st and 2nd matches shoud not be compatible

				if (fDist < fMinDist)
				{
					if (!compatible_features(vecFeats2[j], vecFeats2[iMinDistIndex])) // Add this line for dense features
					{
						// Closest feature is not geometrically compatible with first
						// Shuffle down 1st and 2nd nearest distances
						fMinDist2 = fMinDist;
						iMinDist2Index = iMinDistIndex;
						fMinDist = fDist;
						iMinDistIndex = j;
					}
					else
					{
						// Closest feature is geometrically compatible with first
						// Replace only 1st feature, we have found a better instance of the same thing
						fMinDist = fDist;
						iMinDistIndex = j;
					}
				}
				else
				{
					if (!compatible_features(vecFeats2[j], vecFeats2[iMinDistIndex])) // Add this line for dense features
					{
						// Second closest feature is not geometrically compatible with first
						// Shuffle down 2nd nearest distance
						fMinDist2 = fDist;
						iMinDist2Index = j;
					}
					else
					{
						//printf( "worse \n" );
						// Second closest feature is geometrically compatible with first
						// It is a worse instance of the current closest feature, ignore it
					}
				}
			}
		}

		if (compatible_features(vecFeats2[iMinDist2Index], vecFeats2[iMinDistIndex]))
		{
			iCompatible++;
			//fMinDist = 1;
			//fMinDist2 = 10000;
		}

		vecMatchIndex12.push_back(iMinDist2Index);
		vecMatchDist12.push_back(fMinDist2);
	}
	return 0;
}

#ifdef INCLUDE_FLANN

typedef struct _msSearchStructure
{
	// FLANN parameters
	struct FLANNParameters g_flann_params;
	float g_speedup;
	flann_index_t g_index_id;
	int g_nn;

	// Size of vector 
	int iVectorSize;

	// Data vectors
	float *g_pf1; // New image feature
	float *g_pf2; // Data base of features
	int	*g_piFeatureFrequencies; // Frequency count array, one for each feature
	int g_iFeatCount; // Count of all features
	int *g_piFeatureLabels; // Label array, one for each feature, currently set to image index

							// Label vectors
	vector< float > vfLabelCounts; // A prior over all discrete label values, e.g. in training data

								   // Coordinates / Scale for each feature
	float *g_pfX;
	float *g_pfY;
	float *g_pfZ;
	float *g_pfS;

	int	*g_piFeatureL0; // quick label counter
	int	*g_piFeatureL1; // quick label counter

						// Index result
	int *g_piResult; // Result array for one search (input features x g_nn neighours)
	float *g_pfDist;  // Distance array for one search (input features x g_nn neighours)
	int g_iMaxImageFeatures; // Max size of array for one search

							 // Indices of features g_pf1
	int *g_piImgIndices; // Index array, one for each image, index mapping image to feature arrays
	int *g_piImgFeatureCounts; // Count array, one for each image, number of features per image
	int g_iImgCount;

	// file output
	FILE *outfile;

	// Labels
} msSearchStructure;


msSearchStructure g_SS;


//
// msInitNearestNeighborDistanceRatioInfoApproximate()
//
// Initialize with template / model
//
//
int
msInitNearestNeighborApproximate(
	vector<Feature3DInfo> &vecFeats2,
	int iMaxImage1Features // guess
)
{
	g_SS.g_flann_params = DEFAULT_FLANN_PARAMETERS;
	g_SS.g_flann_params.algorithm = FLANN_INDEX_KDTREE;
	g_SS.g_flann_params.trees = 8;
	g_SS.g_flann_params.log_level = FLANN_LOG_INFO;
	g_SS.g_flann_params.checks = 64;
	g_SS.g_nn = 2;

	g_SS.g_pf1 = new float[iMaxImage1Features*PC_ARRAY_SIZE];
	g_SS.g_pf2 = new float[vecFeats2.size()*PC_ARRAY_SIZE];

	g_SS.g_piResult = new int[iMaxImage1Features*g_SS.g_nn];
	g_SS.g_pfDist = new float[iMaxImage1Features*g_SS.g_nn];

	for (int i = 0; i < vecFeats2.size(); i++)
		memcpy(g_SS.g_pf2 + i*PC_ARRAY_SIZE, &(vecFeats2[i].m_pfPC[0]), sizeof(vecFeats2[i].m_pfPC));

	g_SS.g_index_id = flann_build_index(g_SS.g_pf2, vecFeats2.size(), PC_ARRAY_SIZE, &g_SS.g_speedup, &g_SS.g_flann_params);
	return 1;
}

//
// msInitNearestNeighborApproximate()
//
// Init for NN search for self
//
int
msNearestNeighborApproximateInit(
	vector< vector<Feature3DInfo> > &vvFeats,
	int iNeighbors,
	vector< int > &vLabels,
	float fGeometryWeight,
	int iImgSplit
)
{
	g_SS.iVectorSize = PC_ARRAY_SIZE;
	if (fGeometryWeight > 0)
	{
		// Add coordinates to vectors: add 
		g_SS.iVectorSize = PC_ARRAY_SIZE + 3;
	}

	// Tells where to split train / test data
	int iFeatureSplit = 0;

	printf("Descriptor size: %d\n", g_SS.iVectorSize);

	g_SS.g_flann_params = DEFAULT_FLANN_PARAMETERS;
	g_SS.g_flann_params.algorithm = FLANN_INDEX_KDTREE;
	g_SS.g_flann_params.trees = 8;
	g_SS.g_flann_params.log_level = FLANN_LOG_INFO;
	g_SS.g_flann_params.checks = 64;
	g_SS.g_flann_params.sorted = 1;
	g_SS.g_flann_params.cores = 1;
	g_SS.g_nn = iNeighbors;

	// Per-image info/arrays: feature indices and counts
	g_SS.g_iImgCount = vvFeats.size();
	g_SS.g_piImgIndices = new int[vvFeats.size()];
	g_SS.g_piImgFeatureCounts = new int[vvFeats.size()];
	g_SS.g_iFeatCount = 0;
	g_SS.g_iMaxImageFeatures = 0;
	for (int i = 0; i < vvFeats.size(); i++)
	{
		if (i == iImgSplit)
			iFeatureSplit = g_SS.g_iFeatCount;

		g_SS.g_piImgIndices[i] = g_SS.g_iFeatCount;
		g_SS.g_piImgFeatureCounts[i] = vvFeats[i].size();
		g_SS.g_iFeatCount += vvFeats[i].size();
		if (vvFeats[i].size() > g_SS.g_iMaxImageFeatures)
		{
			g_SS.g_iMaxImageFeatures = vvFeats[i].size();
		}

		while (vLabels[i] >= g_SS.vfLabelCounts.size())
		{
			// push back zero bins
			g_SS.vfLabelCounts.push_back(0);
		}
		// Add number of features associated with this label
		g_SS.vfLabelCounts[vLabels[i]] += vvFeats[i].size();
	}

	//
	float fPriorSum = 0;
	for (int i = 0; i < g_SS.vfLabelCounts.size(); i++)
	{
		// Add an extra sample to normalize distribution
		g_SS.vfLabelCounts[i]++;
		fPriorSum += g_SS.vfLabelCounts[i];
	}
	// Normalize label distribution - divide by total labels (images) in addition to prior (number of labels)
	for (int i = 0; i < g_SS.vfLabelCounts.size(); i++)
	{
		g_SS.vfLabelCounts[i] /= fPriorSum;
	}

	// Per-feature arrays: labels and descriptors
	g_SS.g_iFeatCount = g_SS.g_iFeatCount;
	g_SS.g_piFeatureLabels = new int[g_SS.g_iFeatCount];
	g_SS.g_pf2 = new float[g_SS.g_iFeatCount*g_SS.iVectorSize];  // Array to hold entire feature set
	g_SS.g_piFeatureFrequencies = new int[g_SS.g_iFeatCount];  // Array to hold frequency counts for each input feature
	g_SS.g_piFeatureL0 = new int[g_SS.g_iFeatCount]; // Quick label counters
	g_SS.g_piFeatureL1 = new int[g_SS.g_iFeatCount];
	memset(g_SS.g_piFeatureL0, 0, sizeof(int)*g_SS.g_iFeatCount);
	memset(g_SS.g_piFeatureL1, 0, sizeof(int)*g_SS.g_iFeatCount);

	g_SS.g_pfX = new float[g_SS.g_iFeatCount];
	g_SS.g_pfY = new float[g_SS.g_iFeatCount];
	g_SS.g_pfZ = new float[g_SS.g_iFeatCount];
	g_SS.g_pfS = new float[g_SS.g_iFeatCount];

	//memset( g_SS.g_piFeatureFrequencies, 0, sizeof(int)*iFeatCount );

	// Per-result arrays: indices and distances
	g_SS.g_piResult = new int[g_SS.g_iMaxImageFeatures*g_SS.g_nn];
	g_SS.g_pfDist = new float[g_SS.g_iMaxImageFeatures*g_SS.g_nn];
	g_SS.g_pf1 = NULL; //new float[g_SS.g_iMaxImageFeatures*g_SS.iVectorSize]; // Array to hold max features from one image


																	   // Copy descriptors into array
	int iFeatCount = 0;
	for (int i = 0; i < vvFeats.size(); i++)
	{
		for (int j = 0; j < vvFeats[i].size(); j++)
		{
			memcpy(g_SS.g_pf2 + iFeatCount*g_SS.iVectorSize, &(vvFeats[i][j].m_pfPC[0]), sizeof(vvFeats[i][j].m_pfPC));

			// This is where the feature info goes - set to zero for debugging
			if (fGeometryWeight > 0)
			{
				g_SS.g_pf2[iFeatCount*g_SS.iVectorSize + 0] = fGeometryWeight*vvFeats[i][j].x;/// vvFeats[i][j].scale;
				g_SS.g_pf2[iFeatCount*g_SS.iVectorSize + 1] = fGeometryWeight*vvFeats[i][j].y;// / vvFeats[i][j].scale;
				g_SS.g_pf2[iFeatCount*g_SS.iVectorSize + 2] = fGeometryWeight*vvFeats[i][j].z;// / vvFeats[i][j].scale;
				if (1)
				{
					g_SS.g_pf2[iFeatCount*g_SS.iVectorSize + 0] /= vvFeats[i][j].scale;
					g_SS.g_pf2[iFeatCount*g_SS.iVectorSize + 1] /= vvFeats[i][j].scale;
					g_SS.g_pf2[iFeatCount*g_SS.iVectorSize + 2] /= vvFeats[i][j].scale;
				}
			}

			//** add geometry weights here: small shift

			g_SS.g_piFeatureLabels[iFeatCount] = vLabels[i];
			g_SS.g_piFeatureFrequencies[iFeatCount] = 0; // Set to zero

			g_SS.g_pfX[iFeatCount] = vvFeats[i][j].x;
			g_SS.g_pfY[iFeatCount] = vvFeats[i][j].y;
			g_SS.g_pfZ[iFeatCount] = vvFeats[i][j].z;
			g_SS.g_pfS[iFeatCount] = vvFeats[i][j].scale;

			iFeatCount++;
		}
	}

	if (iImgSplit <= 0 || iFeatureSplit <= 0)
		iFeatureSplit = g_SS.g_iFeatCount;


	//g_SS.g_index_id = flann_build_index(g_SS.g_pf2, g_SS.g_iFeatCount, g_SS.iVectorSize, &g_SS.g_speedup, &g_SS.g_flann_params);
	g_SS.g_index_id = flann_build_index(g_SS.g_pf2, iFeatureSplit, g_SS.iVectorSize, &g_SS.g_speedup, &g_SS.g_flann_params);

	g_SS.outfile = fopen("report.all.txt", "wt");
	return 1;
}



int
msNearestNeighborApproximateDelete(
)
{
	if (g_SS.outfile) fclose(g_SS.outfile);
	flann_free_index(g_SS.g_index_id, &g_SS.g_flann_params);
	delete[] g_SS.g_piFeatureFrequencies;
	delete[] g_SS.g_pfDist;
	delete[] g_SS.g_piResult;
	//delete g_SS.g_pf1;
	delete[] g_SS.g_pf2;

	return 1;
}

//
// msNearestNeighborApproximateSearchSelf()
//
// Search stored database for own nearest neighbors
//
int
msNearestNeighborApproximateSearchSelf(
)
{
	vector< int > viImgCounts;
	viImgCounts.resize(g_SS.g_iImgCount, 0);

	printf("\n");

	for (int i = 0; i < g_SS.g_iImgCount; i++)
	{
		for (int j = 0; j < viImgCounts.size(); j++)
		{
			viImgCounts[j] = 0;
		}

		g_SS.g_pf1 = g_SS.g_pf2 + g_SS.g_piImgIndices[i] * g_SS.iVectorSize;
		flann_find_nearest_neighbors_index(g_SS.g_index_id, g_SS.g_pf1, g_SS.g_piImgFeatureCounts[i], g_SS.g_piResult, g_SS.g_pfDist, g_SS.g_nn, &g_SS.g_flann_params);

		for (int j = 0; j < g_SS.g_nn*g_SS.g_piImgFeatureCounts[i]; j++)
		{
			int iResult = g_SS.g_piResult[j];
			int iLabel = g_SS.g_piFeatureLabels[iResult];
			viImgCounts[iLabel]++;
		}

		for (int j = 0; j < viImgCounts.size(); j++)
		{
			printf("%d\t", viImgCounts[j]);
		}
		printf("\n");
	}

	return 1;
}

//
// msNearestNeighborApproximateSearchSelf()
//
// Search a specific image. This function is used in class
//
int
msNearestNeighborApproximateSearchSelf(
	int iImgIndex,
	vector< int > &vfImgCounts,  // Size: #images in database. Return value: # of times features match to database image N
	vector< float > &vfLabelCounts, // Size: #labels in database. Label counts large enough to fit all labels
	vector< float > &vfLabelLogLikelihood, // Size: #labels in database Log likelihood, large enough to fit all labels
	float** ppfMatchingVotes
)
{
	// Subtract this image label from label prior distribution (add it later)
	// This is important particularly labels with few samples, e.g. nearest neighbors
	int iImgLabel = g_SS.g_piFeatureLabels[g_SS.g_piImgIndices[iImgIndex]];
	//g_SS.vfLabelCounts[ iImgLabel ] -= 1.0f / (float)g_SS.g_piImgFeatureCounts[ iImgIndex ];
	float fImgFeaturesProb = g_SS.g_piImgFeatureCounts[iImgIndex] / ((float)(g_SS.g_iFeatCount + g_SS.vfLabelCounts.size()));
	g_SS.vfLabelCounts[iImgLabel] -= fImgFeaturesProb;

	// Crazy - here the prior is swamped by the number of subjects - for interpreting a new subject, it makes no difference.
	// Maybe this is how we learn. For example, if an animal records data from its sensors over 40 years of life. Assuming the 
	// computing facilities remain constant over time, it would take another 40 for the same amount of data
	// to enter the system and tune internal parameters. This seems to be an upper bound on the learning rate.
	//
	// First, consider a memory-based model of intelligence, where the system attempts to continuously predict future
	//	observations based on all observations up until the current time, in order to maximize the probability of
	//	achieving its goals. Here, intelligence measures the capacity of a system to predict future inputs from all
	//	all previous observations up to the present time. The MMI is a compelling model for several reasons, biologically
	//	the number of neurons in the brain of an animal is correlated with what could be described as intelligent behavior,
	//	auditory signaling, language, etc. Theoretically, simple nearest neighbor algorithms converge to within fractions
	//	of optimal Bayes error rates (Cover, Hart) as the amount of data stored tends to infinity. All parametric 
	//	models representing information with fewer bits than the incommong observations themselves, based on
	//	prior information. 
	//    
	// In an MMI, sensor data enters a system at a constant rate, and remains encoded in a neural substrate, where
	//	it is constantly compared to new inputs in order to predict future inputs.
	// 
	// In such a system, the amount of information learned can be quantified in terms of bits. For example, a memory
	//	of a 2x2 binary image requires a minimum of 2^4 bits to store, given no other sources of information.
	//
	// Sensors inputs have an input data rate, quantified by the amount of information observed per unit of time:
	//		Input data rate = dI / dt.
	//  
	// The amount of data observed from time 0 to current time t is the integral of this rate from 
	//		Data observed = D = \int_0^t dI / dt
	//  
	// As in an MMI, intelligence is based on a model of memory storage, D serves as an upper bound as to the
	//	amount of information learned over an interval of time, and thus the capacity of the system to predict
	//	future events.
	//
	// Thus the rate of learnig slows with time, 
	// Never eternal parameters.

	//float pfPriorProbsCT[5] = {0.76342983,	0.23657016}; // For MS data
	//float pfPriorProbsCT[5] = {0.17770, 0.20015, 0.19228, 0.21392, 0.21594}; // For COPD data
	//float pfPriorProbsCT[] = {0.331933564, 0.336318615, 0.331747821};
	//float pfPriorProbsCT[5] = {0.2,0.2,0.2,0.2,0.2}; 
	//float pfPriorProbsCT[5] = {0.179848168,	0.200687984,	0.193236422,	0.212957737,	0.213269689}; 
	float *pfPriorProbsCT = &(g_SS.vfLabelCounts[0]);

	printf("|%d|", g_SS.g_piFeatureLabels[iImgIndex]);
	// for( int j = 0; j < g_SS.vfLabelCounts.size(); j++ ) printf( "|%f|", pfPriorProbsCT[j] );

	for (int j = 0; j < vfImgCounts.size(); j++)
		vfImgCounts[j] = 0;
	for (int j = 0; j < vfLabelLogLikelihood.size(); j++)
		vfLabelLogLikelihood[j] = 0;

	int* piResult = new int[g_SS.g_iMaxImageFeatures*g_SS.g_nn];
	float* pfDist = new float[g_SS.g_iMaxImageFeatures*g_SS.g_nn];
	float* pf1 = g_SS.g_pf2 + g_SS.g_piImgIndices[iImgIndex] * g_SS.iVectorSize;
	
	//g_SS.g_pf1 = g_SS.g_pf2 + g_SS.g_piImgIndices[iImgIndex] * g_SS.iVectorSize;
	flann_find_nearest_neighbors_index(g_SS.g_index_id, pf1, g_SS.g_piImgFeatureCounts[iImgIndex], piResult, pfDist, g_SS.g_nn, &g_SS.g_flann_params);

	int iOutOfBounds = 0;

	// No deviation on location filter - if location is relevant (aligned subjects, e.g. brain),
	// then use concatenated geometry
	float fMaxDeviationLocation = 10000000;
	std::map<int, float> votedFeatures;
	for (int i = 0; i < g_SS.g_piImgFeatureCounts[iImgIndex]; i++)
	{
		// Initialize counts with Laplacian prior - actually Laplacian is no good, need actual prior
		for (int j = 0; j < vfLabelCounts.size(); j++)
			vfLabelCounts[j] = 1.0*pfPriorProbsCT[j];

		int iQueryFeatIndex = g_SS.g_piImgIndices[iImgIndex] + i;

		// Identify min distance to neighbor, use this as stdev in Gaussian weighting scheme
		float fMinDist = -1;
		std::vector< std::pair<int, float> > indexNN;
		std::vector< int > matchingLabels;
		for (int j = 0; j < g_SS.g_nn; j++)
		{
			int iResultFeatIndex = piResult[i*g_SS.g_nn + j];
			int iLabel = g_SS.g_piFeatureLabels[iResultFeatIndex];

			// Geometrical consistency check - not used, fMaxDeviationLocation set to infinity
			if (
				fabs(g_SS.g_pfX[iQueryFeatIndex] - g_SS.g_pfX[iResultFeatIndex]) < fMaxDeviationLocation &&
				fabs(g_SS.g_pfY[iQueryFeatIndex] - g_SS.g_pfY[iResultFeatIndex]) < fMaxDeviationLocation &&
				fabs(g_SS.g_pfZ[iQueryFeatIndex] - g_SS.g_pfZ[iResultFeatIndex]) < fMaxDeviationLocation &&
				1
				)
			{
				// Result index must not be from the query image
				if (iResultFeatIndex < g_SS.g_piImgIndices[iImgIndex] || iResultFeatIndex > g_SS.g_piImgIndices[iImgIndex] + g_SS.g_piImgFeatureCounts[iImgIndex])
				{
					// Only count features from other images
					if (indexNN.size() < g_SS.g_nn)
					{
						// Ensure feature only vote once per label
						if (std::find(matchingLabels.begin(), matchingLabels.end(), iLabel) == matchingLabels.end())
						{
							/* A vote does not already exists for this label */
							std::pair<int, float> tmp(piResult[i*g_SS.g_nn + j], pfDist[i*g_SS.g_nn + j]);
							indexNN.push_back(tmp);

							if (fMinDist == -1 || pfDist[i*g_SS.g_nn + j] < fMinDist)
							{
								if (pfDist[i*g_SS.g_nn + j] > 0)
								{
									// For duplicated scans, distance will be 0, we want first non-null minimum distance
									fMinDist = pfDist[i*g_SS.g_nn + j];
								}
							}

							matchingLabels.push_back(iLabel);
						}
					}
					else {
						break;
					}

					//vfLabelCounts[iLabel]++;
				}
			}
			else
			{
				// Should not get here with huge permissible deviation
				assert(0);
			}
		}


		// Normalize weights
		//float sum_weights = 0;
		std::vector<float> weights;
		for (int j = 0; j < indexNN.size(); ++j)
		{
			// TODO: Try normalized gaussian weight (to avoid big votes for feature with far NN)
			// Gaussian Weighting Scheme
			//float variance = fMinDist*fMinDist + 1;
			//float distance = indexNN[j].second;
			//float weight = (1 / std::sqrt(2 * M_PI * variance)) * exp(-(distance * distance) / (2 * variance));

			// Linear Weighting Scheme
			//float maxSqDist = 87360;
			//float weight = indexNN[j].second / maxSqDist;

			// Constant Weighting Scheme
			float weight = 1.0;

			// Normalize
			//weight /= g_SS.g_piImgFeatureCounts[iImgIndex];

			weights.push_back(weight);
			//sum_weights += weight;

		}

		/*
		for (int j = 0; j < weights.size(); ++j)
		{
		weights[j] /= sum_weights;
		}
		*/

		// Now add results based on min distance neighbor 
		//for( int j = 0; j < g_SS.g_nn; j++ )
		for (int j = 0; j < indexNN.size(); ++j)
		{
			//int iResultFeatIndex = g_SS.g_piResult[i*g_SS.g_nn+j];
			int iResultFeatIndex = indexNN[j].first;
			int iLabel = g_SS.g_piFeatureLabels[iResultFeatIndex];

			float fDx = (g_SS.g_pfX[iQueryFeatIndex] - g_SS.g_pfX[iResultFeatIndex]);
			float fDy = (g_SS.g_pfY[iQueryFeatIndex] - g_SS.g_pfY[iResultFeatIndex]);
			float fDz = (g_SS.g_pfZ[iQueryFeatIndex] - g_SS.g_pfZ[iResultFeatIndex]);
			float fDistGeom = fDx*fDx + fDy*fDy + fDz*fDz;
			if (fDistGeom > 0)
				fDistGeom = sqrt(fDistGeom);


			// Geometrical consistency check
			if (
				fabs(g_SS.g_pfX[iQueryFeatIndex] - g_SS.g_pfX[iResultFeatIndex]) < fMaxDeviationLocation &&
				fabs(g_SS.g_pfY[iQueryFeatIndex] - g_SS.g_pfY[iResultFeatIndex]) < fMaxDeviationLocation &&
				fabs(g_SS.g_pfZ[iQueryFeatIndex] - g_SS.g_pfZ[iResultFeatIndex]) < fMaxDeviationLocation &&
				1
				)
			{
				//
				if (iResultFeatIndex < g_SS.g_piImgIndices[iImgIndex] || iResultFeatIndex > g_SS.g_piImgIndices[iImgIndex] + g_SS.g_piImgFeatureCounts[iImgIndex])
				{
					// Only count features from other images
					float fDist = pfDist[i*g_SS.g_nn + j];
					float fExponent = fDist / (fMinDist + 1.0);
					// Gaussian weighting
					float fValue = exp(-fExponent*fExponent) / pfPriorProbsCT[iLabel];
					vfLabelCounts[iLabel] += fValue;          // Previously exp( -fExponent*fExponent )/ pfPriorProbsCT[iLabel];
															  // Simple KNN
															  //vfLabelCounts[iLabel] += 1.0f/ pfPriorProbsCT[iLabel];

															  //vfLabelCounts[iLabel] += (fMinDist*fMinDist/(fMinDist*fMinDist+fDist*fDist + 1)) / pfPriorProbsCT[iLabel];
															  //vfLabelCounts[iLabel]++;


															  // Output label (e.g. subject ID) and kernel value.
															  //fprintf( g_SS.outfile, "%d\t%d\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n", g_SS.g_piFeatureLabels[iQueryFeatIndex], iLabel, fValue, fDist, g_SS.g_pfX[iQueryFeatIndex], g_SS.g_pfY[iQueryFeatIndex], g_SS.g_pfZ[iQueryFeatIndex], g_SS.g_pfS[iQueryFeatIndex], g_SS.g_pfX[iResultFeatIndex], g_SS.g_pfY[iResultFeatIndex], g_SS.g_pfZ[iResultFeatIndex], g_SS.g_pfS[iResultFeatIndex]);
					//fprintf(g_SS.outfile, "%d\t%d\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n", g_SS.g_piFeatureLabels[iQueryFeatIndex], iLabel, weights[j], g_SS.g_pfX[iQueryFeatIndex], g_SS.g_pfY[iQueryFeatIndex], g_SS.g_pfZ[iQueryFeatIndex], g_SS.g_pfS[iQueryFeatIndex], g_SS.g_pfX[iResultFeatIndex], g_SS.g_pfY[iResultFeatIndex], g_SS.g_pfZ[iResultFeatIndex], g_SS.g_pfS[iResultFeatIndex]);

					// Multiple features from same subject cannot match to same feature in another subject (e.g. A1 -> B1, A2 -> B1)
					if (votedFeatures.find(iResultFeatIndex) != votedFeatures.end())
					{
						float previousVote = votedFeatures[iResultFeatIndex];

						// If this vote is better, delete previous one
						if (weights[j] > previousVote)
						{
							if (previousVote > 0)
								ppfMatchingVotes[g_SS.g_piFeatureLabels[iQueryFeatIndex]][iLabel] -= previousVote;
							ppfMatchingVotes[g_SS.g_piFeatureLabels[iQueryFeatIndex]][iLabel] += weights[j];
							votedFeatures[iResultFeatIndex] = weights[j];
						}
					}
					else
					{
						// Store votes
						ppfMatchingVotes[g_SS.g_piFeatureLabels[iQueryFeatIndex]][iLabel] += weights[j];
						votedFeatures.insert({ iResultFeatIndex, weights[j] });
					}

					vfImgCounts[iLabel]++;
				}
			}
			else
			{
				// Should not get here with huge permissible deviation
				assert(0);
			}
		}

		// Compute & accumulate log likelihood
		float fTotal = 0;
		for (int j = 0; j < vfLabelCounts.size(); j++)
		{
			// Divide here by prior probability of a sample
			//vfLabelCounts[j] /= pfPriorProbsCT[j];

			fTotal += vfLabelCounts[j];
		}
		// Note that normalization here makes no difference immediately
		for (int j = 0; j < vfLabelCounts.size(); j++)
		{
			vfLabelLogLikelihood[j] += log(vfLabelCounts[j] / (float)fTotal);
		}
	}

	// Add label back on
	//g_SS.vfLabelCounts[ iImgLabel ] += 1.0f / (float)g_SS.g_piImgFeatureCounts[ iImgIndex ];
	g_SS.vfLabelCounts[iImgLabel] += fImgFeaturesProb;

	delete[] piResult;
	delete[] pfDist;
	
	return 1;
}


//
//int
//msNearestNeighborApproximateSearchSelf(
//	int iImgIndex,
//	vector< int > &viImgCounts,  // Size: #images in database. Return value: # of times features match to database image N
//	vector< float > &vfLabelCounts, // Size: #labels in database. Label counts large enough to fit all labels
//	vector< float > &vfLabelLogLikelihood // Size: #labels in database Log likelihood, large enough to fit all labels
//)
//{
//
//	for( int j = 0; j < viImgCounts.size(); j++ )
//		viImgCounts[j] = 0;
//	for( int j = 0; j < vfLabelLogLikelihood.size(); j++ )
//		vfLabelLogLikelihood[j] = 0;
//
//	g_SS.g_pf1 = g_SS.g_pf2 + g_SS.g_piImgIndices[iImgIndex]*g_SS.iVectorSize;
//	flann_find_nearest_neighbors_index(g_SS.g_index_id, g_SS.g_pf1, g_SS.g_piImgFeatureCounts[iImgIndex], g_SS.g_piResult, g_SS.g_pfDist, g_SS.g_nn, &g_SS.g_flann_params);
//
//	int iOutOfBounds = 0;
//
//	float fMaxDeviationLocation = 1000;
//
//	for( int i = 0; i < g_SS.g_piImgFeatureCounts[iImgIndex]; i++ )
//	{
//		// Initialize counts with Laplacian prior
//		for( int j = 0; j < vfLabelCounts.size(); j++ )
//			vfLabelCounts[j] = 1;
//
//		int iQueryFeatIndex = g_SS.g_piImgIndices[iImgIndex] + i;
//
//		// Identify min distance to neighbor, use this as stdev in Gaussian weighting scheme
//		float fMinDist = -1;
//		for( int j = 0; j < g_SS.g_nn; j++ )
//		{
//			int iResultFeatIndex = g_SS.g_piResult[i*g_SS.g_nn+j];
//			int iLabel = g_SS.g_piFeatureLabels[iResultFeatIndex];
//
//			// Geometrical consistency check
//			if(
//				fabs( g_SS.g_pfX[iQueryFeatIndex]-g_SS.g_pfX[iResultFeatIndex] ) < fMaxDeviationLocation &&
//				fabs( g_SS.g_pfY[iQueryFeatIndex]-g_SS.g_pfY[iResultFeatIndex] ) < fMaxDeviationLocation &&
//				fabs( g_SS.g_pfZ[iQueryFeatIndex]-g_SS.g_pfZ[iResultFeatIndex] ) < fMaxDeviationLocation &&
//				1
//				)
//			{
//				// Result index must not be from the query image
//				if( iResultFeatIndex < g_SS.g_piImgIndices[iImgIndex] || iResultFeatIndex > g_SS.g_piImgIndices[iImgIndex] + g_SS.g_piImgFeatureCounts[iImgIndex] )
//				{
//					if( fMinDist == -1 || g_SS.g_pfDist[i*g_SS.g_nn+j] < fMinDist )
//					{
//						fMinDist = g_SS.g_pfDist[i*g_SS.g_nn+j];
//					}
//
//					// Only count features from other images
//					//vfLabelCounts[iLabel]++;
//				}
//			}
//		}
//
//		// Now add results based on min distance neighbor 
//		for( int j = 0; j < g_SS.g_nn; j++ )
//		{
//			int iResultFeatIndex = g_SS.g_piResult[i*g_SS.g_nn+j];
//			int iLabel = g_SS.g_piFeatureLabels[iResultFeatIndex];
//
//			// Geometrical consistency check
//			if(
//				fabs( g_SS.g_pfX[iQueryFeatIndex]-g_SS.g_pfX[iResultFeatIndex] ) < fMaxDeviationLocation &&
//				fabs( g_SS.g_pfY[iQueryFeatIndex]-g_SS.g_pfY[iResultFeatIndex] ) < fMaxDeviationLocation &&
//				fabs( g_SS.g_pfZ[iQueryFeatIndex]-g_SS.g_pfZ[iResultFeatIndex] ) < fMaxDeviationLocation &&
//				1
//				)
//			{
//				//
//				if( iResultFeatIndex < g_SS.g_piImgIndices[iImgIndex] || iResultFeatIndex > g_SS.g_piImgIndices[iImgIndex] + g_SS.g_piImgFeatureCounts[iImgIndex] )
//				{
//					// Only count features from other images
//					float fDist = g_SS.g_pfDist[i*g_SS.g_nn+j];
//					float fExponent = fDist/fMinDist;
//					vfLabelCounts[iLabel] += 10*exp( -fExponent*fExponent );
//					//vfLabelCounts[iLabel]++;
//
//					viImgCounts[iLabel]++;
//				}
//			}
//		}
//
//		// Compute & accumulate log likelihood
//		int iTotal = 0;
//		for( int j = 0; j < vfLabelCounts.size(); j++ )
//		{
//			iTotal += vfLabelCounts[j];
//		}
//		for( int j = 0; j < vfLabelCounts.size(); j++ )
//		{
//			vfLabelLogLikelihood[j] += log( vfLabelCounts[j]/(float)iTotal );
//		}
//
//	}
//
//	return 1;
//}

const bool
sortIntDescending(const int &i1, const int i2)
{
	return i2 < i1;
}


int
msNearestNeighborApproximateBarfInards(
)
{
	FILE *outfile = fopen("__msNearestNeighborApproximateBarfInards.txt", "wt");

	vector<int> viReformat;
	viReformat.resize(g_SS.g_iMaxImageFeatures, 0);

	for (int j = 0; j < g_SS.g_iImgCount; j++)
	{
		viReformat.resize(g_SS.g_piImgFeatureCounts[j]);

		for (int i = 0; i < g_SS.g_piImgFeatureCounts[j]; i++)
		{
			viReformat[i] = g_SS.g_piFeatureFrequencies[g_SS.g_piImgIndices[j] + i];
		}
		sort(viReformat.begin(), viReformat.begin() + g_SS.g_piImgFeatureCounts[j], greater<int>());

		for (int i = 0; i < g_SS.g_piImgFeatureCounts[j] && i < 1000; i++)
		{
			fprintf(outfile, "%d\t", viReformat[i]);
		}
		fprintf(outfile, "\n");
	}

	fclose(outfile);
	return 1;
}

int
msComputeNearestNeighborDistanceRatioInfoApproximate(
	vector<Feature3DInfo> &vecFeats1,
	vector<Feature3DInfo> &vecFeats2,
	vector<int>		&vecMatchIndex12,
	vector<float>		&vecMatchDist12
)
{
	//   flann_params = DEFAULT_FLANN_PARAMETERS;
	//   flann_params.algorithm = FLANN_INDEX_KDTREE;
	//   flann_params.trees = 8;
	//   flann_params.log_level = FLANN_LOG_INFO;
	//flann_params.checks = 64;
	//nn = 2;

	//float *pf1 = new float[vecFeats1.size()*PC_ARRAY_SIZE];
	//float *pf2 = new float[vecFeats2.size()*PC_ARRAY_SIZE];
	for (int i = 0; i < vecFeats1.size(); i++)
		memcpy(g_SS.g_pf1 + i*PC_ARRAY_SIZE, &(vecFeats1[i].m_pfPC[0]), sizeof(vecFeats1[i].m_pfPC));
	//for( int i = 0; i < vecFeats2.size(); i++ )
	//memcpy( pf2+i*PC_ARRAY_SIZE, &(vecFeats2[i].m_pfPC[0]), sizeof(vecFeats2[i].m_pfPC) );

	//int *piResult = new int[vecFeats1.size()*nn];
	//float *pfDist = new float[vecFeats1.size()*nn];

	//index_id = flann_build_index(pf2, vecFeats2.size(), PC_ARRAY_SIZE, &speedup, &flann_params);
	flann_find_nearest_neighbors_index(g_SS.g_index_id, g_SS.g_pf1, vecFeats1.size(), g_SS.g_piResult, g_SS.g_pfDist, g_SS.g_nn, &g_SS.g_flann_params);

	for (int i = 0; i < vecFeats1.size(); i++)
	{
		int iMinDistIndex = g_SS.g_piResult[i*g_SS.g_nn];
		float fMinDist = g_SS.g_pfDist[i*g_SS.g_nn];
		float fMinDist2 = g_SS.g_pfDist[i*g_SS.g_nn + 1];
		vecMatchIndex12.push_back(iMinDistIndex);
		if (fMinDist2 > 0)
		{
			vecMatchDist12.push_back(fMinDist / fMinDist2);
		}
		else
		{
			vecMatchDist12.push_back(1);
		}
	}
	//delete [] pfDist;
	//delete [] piResult;
	//delete [] pf2;
	//delete [] pf1;
	return 0;
}
int
msComputeNearestNeighborDistanceRatioInfoApproximate(
	float *pf1, float *pf2,
	int iSize1, int iSize2,
	int *piResult,
	float *pfDist
)
{
	struct FLANNParameters flann_params;
	float speedup;
	flann_index_t index_id;
	int nn = 2;
	flann_params = DEFAULT_FLANN_PARAMETERS;
	flann_params.algorithm = FLANN_INDEX_KDTREE;
	flann_params.trees = 8;
	flann_params.log_level = FLANN_LOG_INFO;
	flann_params.checks = 64;

	index_id = flann_build_index(pf2, iSize2, PC_ARRAY_SIZE, &speedup, &flann_params);

	flann_find_nearest_neighbors_index(index_id, pf1, iSize1, piResult, pfDist, nn, &flann_params);

	flann_free_index(index_id, &flann_params);

	return 0;
}


#endif

//
// msComputeNearestNeighborDistanceRatioInfoGeometryConstrained()
//
// Search for nearest neighbors, assume feats1 and feat2
// approximately aligned. The idea is correctly match features locally
// that are otherwise globally incorrect.
//
// Some local matches are very crappy and coincidental. So, if a closer
// random match can be found, don't consider.
//
int
msComputeNearestNeighborDistanceRatioInfoGeometryConstrained(
	vector<Feature3DInfo> &vecFeats1,
	vector<Feature3DInfo> &vecFeats2,
	vector<int>		&vecMatchIndex12,
	vector<float>		&vecMatchDist12,
	float fScaleDiffThreshold = 1.0f,
	float fShiftThreshold = 2.0f,
	float fCosineAngleThreshold = 0.7
)
{
	int iCompatible = 0;

	int iPCs = 64;

	for (int i = 0; i < vecFeats1.size(); i++)
	{
		float fMinDist = 9e30;
		int iMinDistIndex = -1;
		int j = 0;

		while (j < vecFeats2.size() && !compatible_features(vecFeats1[i], vecFeats2[j], fScaleDiffThreshold, fShiftThreshold, fCosineAngleThreshold))
		{
			j++;
		}

		if (j < vecFeats2.size())
		{
			fMinDist = vecFeats1[i].DistSqrPCs(vecFeats2[j], iPCs);
			iMinDistIndex = j++;
		}
		else
		{
			vecMatchIndex12.push_back(-1);
			vecMatchDist12.push_back(9e30);
			continue;
		}

		for (j; j < vecFeats2.size(); j++)
		{
			if (!compatible_features(vecFeats1[i], vecFeats2[j], fScaleDiffThreshold, fShiftThreshold, fCosineAngleThreshold))
			{
				continue;
			}

			float fDist = vecFeats1[i].DistSqrPCs(vecFeats2[j], iPCs);

			if (fDist < fMinDist)
			{
				fMinDist = fDist;
				iMinDistIndex = j;
			}
		}

		// At this point, we have the minimum distance match within geometrical constraints.
		// Let's see if another random feature correspondence is closer - if so, forget this match.

		for (int j = 0; j < vecFeats2.size() / 30; j++)
		{
			int iIndexRand = (rand()*vecFeats2.size()) / RAND_MAX;
			while (iIndexRand == iMinDistIndex || iIndexRand >= vecFeats2.size())
			{
				iIndexRand = (rand()*vecFeats2.size()) / RAND_MAX;
			}

			float fDist = vecFeats1[i].DistSqrPCs(vecFeats2[j], iPCs);

			if (fDist < fMinDist)
			{
				fMinDist = 9e30;
				iMinDistIndex = -1;
				break;
			}
		}

		vecMatchIndex12.push_back(iMinDistIndex);
		vecMatchDist12.push_back(fMinDist);
	}
	return 0;
}

int
msComputeNearestNeighborDistanceRatioInfoGeometryConstrainedOriginal(
	vector<Feature3DInfo> &vecFeats1,
	vector<Feature3DInfo> &vecFeats2,
	vector<int>		&vecMatchIndex12,
	vector<float>		&vecMatchDist12,
	float fScaleDiffThreshold = 1.0f,
	float fShiftThreshold = 1.0f,
	float fCosineAngleThreshold = 0.8
)
{
	int iCompatible = 0;

	int iPCs = 64;

	for (int i = 0; i < vecFeats1.size(); i++)
	{
		float fMinDist = 9e30;
		int iMinDistIndex = -1;
		int j = 0;

		while (j < vecFeats2.size() && !compatible_features(vecFeats1[i], vecFeats2[j], fScaleDiffThreshold, fShiftThreshold, fCosineAngleThreshold))
		{
			j++;
		}

		if (j < vecFeats2.size())
		{
			fMinDist = vecFeats1[i].DistSqrPCs(vecFeats2[j], iPCs);
			iMinDistIndex = j++;
		}
		else
		{
			vecMatchIndex12.push_back(-1);
			vecMatchDist12.push_back(9e30);
			continue;
		}

		for (j; j < vecFeats2.size(); j++)
		{
			if (!compatible_features(vecFeats1[i], vecFeats2[j], fScaleDiffThreshold, fShiftThreshold, fCosineAngleThreshold))
			{
				continue;
			}

			float fDist = vecFeats1[i].DistSqrPCs(vecFeats2[j], iPCs);

			if (fDist < fMinDist)
			{
				fMinDist = fDist;
				iMinDistIndex = j;
			}
		}

		vecMatchIndex12.push_back(iMinDistIndex);
		vecMatchDist12.push_back(fMinDist);
	}
	return 0;
}


bool pairIntFloatLeastToGreatest(const pair<int, float> &ii1, const pair<int, float> &ii2)
{
	return ii1.second < ii2.second;
}

int
getMinMaxDim(
	vector<Feature3DInfo> &vecImgFeats2,
	float *pfMin,
	float *pfMax
)
{
	pfMin[0] = pfMax[0] = vecImgFeats2[0].x;
	pfMin[1] = pfMax[1] = vecImgFeats2[0].y;
	pfMin[2] = pfMax[2] = vecImgFeats2[0].z;
	for (int i = 0; i < vecImgFeats2.size(); i++)
	{
		if (vecImgFeats2[i].x > pfMax[0]) pfMax[0] = vecImgFeats2[i].x;
		if (vecImgFeats2[i].x < pfMin[0]) pfMin[0] = vecImgFeats2[i].x;

		if (vecImgFeats2[i].y > pfMax[1]) pfMax[1] = vecImgFeats2[i].y;
		if (vecImgFeats2[i].y < pfMin[1]) pfMin[1] = vecImgFeats2[i].y;

		if (vecImgFeats2[i].z > pfMax[2]) pfMax[2] = vecImgFeats2[i].z;
		if (vecImgFeats2[i].z < pfMin[2]) pfMin[2] = vecImgFeats2[i].z;
	}
	return 0;
}

int
MatchKeys(
	vector<Feature3DInfo> &vecImgFeats1,
	vector<Feature3DInfo> &vecImgFeats2,
	vector<int>       &vecModelMatches,
	int				  iModelFeatsToConsider,
	char *pcFeatFile1,
	char *pcFeatFile2,
	vector<Feature3DInfo> *pvecImgFeats2Transformed, // Transformed features
	int		bMatchConstrainedGeometry, // If true, then consider only approximately correct feature matches

									   // Similarity transform parameters
	float *pfScale, // Scale change 2->1, unary
	float *pfRot,   // Rotation 2->1, 3x3 matrix
	float *pfTrans, // Translation 2->1, 1x3 matrix
	int bExpand
)
{
	vecModelMatches.resize(vecImgFeats1.size());

	FEATUREIO fio1;
	FEATUREIO fio2;
	char pcFileName[400];
	char *pch;
	if (pcFeatFile1)
	{
		strncpy(pcFileName, pcFeatFile1, sizeof(pcFileName));
		pch = strrchr(pcFileName, '.');
		sprintf(pch, ".hdr");
		if (fioReadNifti(fio1, pcFileName, 1) < 0)
		{
			sprintf(pch, ".nii");
			if (fioReadNifti(fio1, pcFileName, 1) < 0)
			{
				sprintf(pch, ".nii.gz");
				if (fioReadNifti(fio1, pcFileName, 1) < 0)
				{
					memset(&fio1, 0, sizeof(fio1));
				}
			}
		}
	}

	if (pcFeatFile2)
	{
		strncpy(pcFileName, pcFeatFile2, sizeof(pcFileName));
		pch = strrchr(pcFileName, '.');
		sprintf(pch, ".hdr");
		if (fioReadNifti(fio2, pcFileName, 1) < 0)
		{
			sprintf(pch, ".nii");
			if (fioReadNifti(fio2, pcFileName, 1) < 0)
			{
				sprintf(pch, ".nii.gz");
				if (fioReadNifti(fio2, pcFileName, 1) < 0)
				{
					memset(&fio2, 0, sizeof(fio2));
				}
			}
		}
		// 
		//fioFlipAxisY( fio2 );

		//fioReadRaw( fio1, pcFeatFile1 );
		//fioReadRaw( fio2, pcFeatFile2 );
	}


	if (iModelFeatsToConsider < 0)
	{
		iModelFeatsToConsider = vecImgFeats2.size();
	}


	vector<float> pfPoints0;
	vector<float> pfPoints1;
	vector<float> pfScales0;
	vector<float> pfScales1;
	vector<float> pfOris0;
	vector<float> pfOris1;

	vector<int> piInliers;
	int iMatchCount = 0;
	// Save indices
	vector<int> vecIndex0;
	vector<int> vecIndex1;

	// Generate vector of model features

	vector<int>		vecMatchIndex12;
	vector<float>	vecMatchDist12;
	if (bMatchConstrainedGeometry)
	{
		msComputeNearestNeighborDistanceRatioInfoGeometryConstrained(vecImgFeats2, vecImgFeats1, vecMatchIndex12, vecMatchDist12);
	}
	else
	{
		//if( g_SS.g_nn == -1 )
		//{
		//	msInitNearestNeighborDistanceRatioInfoApproximate(vecImgFeats1, 10*vecImgFeats1.size() );
		//}
		//msComputeNearestNeighborDistanceRatioInfoApproximate( vecImgFeats2, vecImgFeats1, vecMatchIndex12, vecMatchDist12 );
		msComputeNearestNeighborDistanceRatioInfo(vecImgFeats2, vecImgFeats1, vecMatchIndex12, vecMatchDist12);
	}

	vector< pair<int, float> > vecBestMatches;
	for (int i = 0; i < vecMatchIndex12.size(); i++)
	{
		if (vecMatchIndex12[i] >= 0)
		{
			vecBestMatches.push_back(pair<int, float>(i, vecMatchDist12[i]));
		}
	}
	sort(vecBestMatches.begin(), vecBestMatches.end(), pairIntFloatLeastToGreatest);

	for (int iImgFeat = 0; iImgFeat < vecImgFeats1.size(); iImgFeat++)
	{
		Feature3DInfo &featInput = vecImgFeats1[iImgFeat];

		// Flag as unfound...
		vecModelMatches[iImgFeat] = -1;
	}

	int iMaxMatches = 3000;

	vector< float > vecMatchProb;

	//map<float,int> mapModelXtoIndex;
	//map<float,int> mapModelXtoIndexModel;
	//
	//map<float,int> mapInputXtoIndex;
	//map<float,int> mapInputXtoIndexInput;

	map<float, int> mapMatchDuplicateFinder;
	map<float, int> mapMatchDuplicateFinderModel;

	for (int i = 0; i < vecBestMatches.size() && i < iMaxMatches; i++)
	{
		int iIndex = vecBestMatches[i].first;
		float fDist = vecBestMatches[i].second;
		if (fDist > 0.8)
		{
			//break;
		}

		Feature3DInfo &featModel = vecImgFeats2[iIndex];
		Feature3DInfo &featInput = vecImgFeats1[vecMatchIndex12[iIndex]];

		// Test this hash code - to avoid multiple matches between precisely the same locations
		map<float, int>::iterator itAmazingDupeFinder = mapMatchDuplicateFinder.find(featInput.x*featModel.x);
		if (itAmazingDupeFinder != mapMatchDuplicateFinder.end())
		{
			//printf( "Found something you both lost!\n" );
			//continue;
		}
		mapMatchDuplicateFinder.insert(pair<float, int>(featInput.x*featModel.x, vecMatchIndex12[iIndex]));
		mapMatchDuplicateFinderModel.insert(pair<float, int>(featInput.x*featModel.x, iIndex));


		// Should not allow features with same spatial location

		pfPoints0.push_back(featModel.x);
		pfPoints0.push_back(featModel.y);
		pfPoints0.push_back(featModel.z);



		pfPoints1.push_back(featInput.x);
		pfPoints1.push_back(featInput.y);
		pfPoints1.push_back(featInput.z);

		pfScales0.push_back(featModel.scale);
		pfScales1.push_back(featInput.scale);

		for (int o1 = 0; o1<3; o1++)
		{
			for (int o2 = 0; o2<3; o2++)
			{
				pfOris0.push_back(featModel.ori[o1][o2]);
				pfOris1.push_back(featInput.ori[o1][o2]);
			}
		}

		vecIndex0.push_back(iIndex);
		vecIndex1.push_back(vecMatchIndex12[iIndex]);

		// Save prior probability of this feature
		//    10-02-2015 (October 2, 2015)
		//     It seems we could adjust this threshold to reflect the relative
		//     probability of random feature correspondence, based on feature scale,
		//     the number of features at each scale-aprior and the probability of 
		//     of false matches.
		float fFeatsInThreshold = 1;//FeaturesWithinThreshold( vecOrderToFeat[ iIndex ] );
		vecMatchProb.push_back(fFeatsInThreshold);

		iMatchCount++;
	}

	// Now determine transform

	float pfMin[3];
	float pfMax[3];

	getMinMaxDim(vecImgFeats2, pfMin, pfMax);
	float pfModelCenter[3];
	pfModelCenter[0] = (pfMax[0] + pfMin[0]) / 2.0f;
	pfModelCenter[1] = (pfMax[1] + pfMin[1]) / 2.0f;
	pfModelCenter[2] = (pfMax[2] + pfMin[2]) / 2.0f;

	// Model center has no impact on hough transform here - it is only used to parameterize the ouput
	// similarity transform.
	//pfModelCenter[0] = 160.48;
	//pfModelCenter[1] = 147.4;
	//pfModelCenter[2] = 296.58;

	// Output parameters, the result of determine_similarity_transform_ransac()
	float rot[3 * 3];
	float pfModelCenter2[3];
	float fScaleDiff = 1;

	// Save inliers
	for (int iImgFeat = 0; iImgFeat < vecModelMatches.size(); iImgFeat++)
	{
		vecModelMatches[iImgFeat] = -1;
	}
	int iInliers = -1;

	vector< int > vecModelImgFeatCounts;
	vecModelImgFeatCounts.resize(vecImgFeats2.size(), 0);

	int iModelImgFeatMaxCount = 0;
	int iModelImgFeatMaxCountIndex = 0;
	if (iMatchCount <= 3)
	{
		// Not enough matches to determine a solution
		//FILE *outfile = fopen( "result.txt", "a+" );
		//fprintf( outfile, "%d\t%d\n", iMatchCount, 0 );
		//fclose( outfile );
		return iMatchCount;
	}
	piInliers.resize(iMatchCount, 0);
	iInliers =
		//determine_similarity_transform_ransac(
		//	&(pfPoints0[0]), &(pfPoints1[0]),
		//	&(pfScales0[0]), &(pfScales1[0]),
		//	&(vecMatchProb[0]),
		//	iMatchCount,  500, pfModelCenter,
		//	pfModelCenter2, rot, fScaleDiff, &(piInliers[0])
		//	);
		determine_similarity_transform_hough(
			&(pfPoints0[0]), &(pfPoints1[0]),
			&(pfScales0[0]), &(pfScales1[0]),
			&(pfOris0[0]), &(pfOris1[0]),
			&(vecMatchProb[0]),
			iMatchCount, 500, pfModelCenter,
			pfModelCenter2, rot, fScaleDiff, &(piInliers[0])
		);

	//
	// Try expanded / recursive hough transform, for deformable matching
	//
	// For lung images, seems to work quite well.
	// C:\downloads\data\james\images\ss10077P_INSP_STD_NJC_COPD.key    C:\downloads\data\james\images\ss10077P_EXP_STD_NJC_COPD.key

	int iExpandedInliers;
	if (bExpand)
	{
		iExpandedInliers =
			determine_similarity_transform_hough_expanded(
				&(pfPoints0[0]), &(pfPoints1[0]),
				&(pfScales0[0]), &(pfScales1[0]),
				&(pfOris0[0]), &(pfOris1[0]),
				&(vecMatchProb[0]),
				iMatchCount, 500, pfModelCenter,
				pfModelCenter2, rot, fScaleDiff, &(piInliers[0])
			);

		iInliers = iExpandedInliers;

		//int iExpandedInliersExtra = 
		//determine_similarity_transform_hough_expanded(
		//	&(pfPoints0[0]), &(pfPoints1[0]),
		//	&(pfScales0[0]), &(pfScales1[0]),
		//	&(pfOris0[0]), &(pfOris1[0]),
		//	&(vecMatchProb[0]),
		//	iMatchCount,  500, pfModelCenter,
		//	pfModelCenter2, rot, fScaleDiff, &(piInliers[0])
		//	);
	}

	// Save transform from image 2 to image 1
	if (pfScale)
		*pfScale = fScaleDiff;
	if (pfRot)
	{
		memcpy(pfRot, rot, sizeof(float) * 3 * 3);
	}
	if (pfTrans)
	{
		float pfStuff[3] = { 0,0,0 };
		similarity_transform_3point(
			pfStuff, pfTrans,
			pfModelCenter, pfModelCenter2,
			rot, fScaleDiff);
	}

	vector<float> pfPointsIn0;
	vector<float> pfPointsIn1;
	vector<float> pfScalesIn0;
	vector<float> pfScalesIn1;


	static int iCallNumber = -1;
	iCallNumber++;

	// Output inliers
	for (int i = 0; i < iMatchCount && 1; i++)
	{
		//piInliers[i]=1;
		if (piInliers[i])
		{
			// Save feature/model match
			if (vecModelMatches[vecIndex1[i]] != -1)
			{
				// A match has already been set. Check which one has the smallest distance.
				if (vecMatchDist12[vecModelMatches[vecIndex1[i]]] < vecMatchDist12[vecIndex0[i]])
				{
					// Decrease iMatchCount ? (Cannot be done here, or loop will be messed up)
					// Seems not to be used after
					continue;
				}
			}

			vecModelMatches[vecIndex1[i]] = vecIndex0[i];
			//continue;

			// Save inliers for later
			pfPointsIn0.push_back(pfPoints0[i * 3 + 0]);
			pfPointsIn0.push_back(pfPoints0[i * 3 + 1]);
			pfPointsIn0.push_back(pfPoints0[i * 3 + 2]);

			pfPointsIn1.push_back(pfPoints1[i * 3 + 0]);
			pfPointsIn1.push_back(pfPoints1[i * 3 + 1]);
			pfPointsIn1.push_back(pfPoints1[i * 3 + 2]);

			pfScalesIn0.push_back(pfScales0[i]);
			pfScalesIn1.push_back(pfScales1[i]);

			// Little hack here to print out just destination image
			//if( pcFeatFile2 && strstr( pcFeatFile2, "c." ) )
			if (pcFeatFile2) //&& strstr( pcFeatFile2, "sss3c" ) )
							 //if( pcFeatFile2 && (strstr( pcFeatFile2, "_ct" ) || strstr( pcFeatFile2, "_PD" )) )
			{
				Feature3DInfo &featModel = vecImgFeats2[vecIndex0[i]];
				sprintf(pcFileName, "mod%4.4d_subject%2.2d_img%4.4d_show", vecIndex1[i], iCallNumber, vecIndex0[i]);
				//featModel.OutputVisualFeatureInVolume( fio2, pcFileName, 1, 1 );
				sprintf(pcFileName, "mod%4.4d_subject%2.2d_img%4.4d_no", vecIndex1[i], iCallNumber, vecIndex0[i]);
				//featModel.OutputVisualFeatureInVolume( fio2, pcFileName, 0, 1 );
			}

			if (pcFeatFile1 && pcFeatFile2)
			{
				Feature3DInfo &featModel = vecImgFeats2[vecIndex0[i]];
				// featModel.y = -featModel.y; // Add this for visualizing features mirrored across the y axis
				sprintf(pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_3", i, vecIndex0[i], vecIndex1[i]);
				featModel.OutputVisualFeatureInVolume(fio2, pcFileName, 1, 1);

				Feature3DInfo &featInput = vecImgFeats1[vecIndex1[i]];
				sprintf(pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_2", i, vecIndex0[i], vecIndex1[i]);
				featInput.OutputVisualFeatureInVolume(fio1, pcFileName, 1, 1);

				FEATUREIO fioOut;
				//#define OUT_SIZE 101
				//				fioOut.x = OUT_SIZE;
				//				fioOut.y = OUT_SIZE;
				//				fioOut.z = OUT_SIZE;
				//				fioOut.iFeaturesPerVector = 1;
				//				fioOut.t = 1;
				//				fioAllocate( fioOut );
				//				float rotIn[9] = {1,0,0,0,1,0,0,0,1};
				//				float pfCenter1[3] = {OUT_SIZE/2, OUT_SIZE/2, OUT_SIZE/2};
				//				float pfCenter2 [3];
				//
				//				pfCenter2[0] = featModel.x;
				//				pfCenter2[1] = featModel.y;
				//				pfCenter2[2] = featModel.z;
				//				float fMinValue;
				//				fioFindMin( fio2, fMinValue );
				//				fioSet( fioOut, fMinValue );
				//				similarity_transform_image( fioOut, fio2, pfCenter1, pfCenter2, rotIn, featModel.scale/featInput.scale, fMinValue );
				//				//fioFadeGaussianWindow( fioOut, pfCenter1, 3*featInput.scale );
				//				sprintf( pcFileName, "img_%3.3d_mod%4.4d_img%4.4d_3", i, vecIndex0[i], vecIndex1[i] );
				//				fioWrite( fioOut, pcFileName );
				//
				//				pfCenter2[0] = featInput.x;
				//				pfCenter2[1] = featInput.y;
				//				pfCenter2[2] = featInput.z;
				//				fioFindMin( fio1, fMinValue );
				//				fioSet( fioOut, fMinValue );
				//				similarity_transform_image( fioOut, fio1, pfCenter1, pfCenter2, rotIn, 1, fMinValue );
				//				//fioFadeGaussianWindow( fioOut, pfCenter1, 3*featInput.scale );
				//				sprintf( pcFileName, "img_%3.3d_mod%4.4d_img%4.4d_2", i, vecIndex0[i], vecIndex1[i] );
				//				fioWrite( fioOut, pcFileName );
				//
				//
				//				//pfCenter1[0] = 0;
				//				//pfCenter1[1] = 0;
				//				//pfCenter1[2] = 0;
				//				//pfCenter2[0] = 0;
				//				//pfCenter2[1] = 0;
				//				//pfCenter2[2] = 0;
				//				//similarity_transform_image( fio1, fio1, pfCenter1, pfCenter2, rotIn, 1 );
				//
				//
				//				//pfCenter1[0] = featInput.x;
				//				//pfCenter1[1] = featInput.y;
				//				//pfCenter1[2] = featInput.z;
				//				//pfCenter2[0] = featModel.x;
				//				//pfCenter2[1] = featModel.y;
				//				//pfCenter2[2] = featModel.z;
				//				//fioFadeGaussianWindow( fio1, pfCenter1, 3*featInput.scale );
				//
				//				//sprintf( pcFileName, "img_%3.3d_mod%4.4d_img%4.4d_3", i, vecIndex0[i], vecIndex1[i] );
				//				//fioWrite( fio1, pcFileName );
				//
				//				//pfCenter1[0] = featInput.x;
				//				//pfCenter1[1] = featInput.y;
				//				//pfCenter1[2] = featInput.z;
				//				//pfCenter2[0] = featModel.x;
				//				//pfCenter2[1] = featModel.y;
				//				//pfCenter2[2] = featModel.z;
				//				//similarity_transform_image( fio1, fio2, pfCenter1, pfCenter2, rotIn, featModel.scale/featInput.scale );
				//				//fioFadeGaussianWindow( fio1, pfCenter1, 3*featInput.scale );
				//				//sprintf( pcFileName, "img_%3.3d_mod%4.4d_img%4.4d_2", i, vecIndex0[i], vecIndex1[i] );
				//				//fioWrite( fio1, pcFileName );




			}
		}
	}

	if (0 && pvecImgFeats2Transformed)
	{
		// Transform features from image 2 space to image 1 space
		vector<Feature3DInfo> &vecFeats2XFM = *pvecImgFeats2Transformed;
		vecFeats2XFM.resize(vecImgFeats2.size());
		for (int i = 0; i < vecImgFeats2.size(); i++)
		{
			vecFeats2XFM[i] = vecImgFeats2[i];
			vecFeats2XFM[i].SimilarityTransform(pfModelCenter, pfModelCenter2, rot, fScaleDiff);
		}
		msFeature3DVectorOutputText(vecFeats2XFM, "transformed.key");
	}

	//return iInliers;

	// Output entire transformed image
	// If this works the first time I'll eat my shorts...
	if (0 && pcFeatFile1 && pcFeatFile2 && fioDimensionsEqual(fio1, fio2))
	{
		FEATUREIO fioOut = fio1;
		fioAllocate(fioOut);

		//fioWrite( fioOut, "transformed_in" );

		// Invert transform, since
		similarity_transform_invert(pfModelCenter, pfModelCenter2, rot, fScaleDiff);
		similarity_transform_image(fioOut, fio2, pfModelCenter, pfModelCenter2, rot, fScaleDiff);

		//fioWrite( fio2, "transformed_fio2" );
		sprintf(pcFileName, "%s.hough", pcFeatFile2);
		fioWrite(fioOut, pcFileName);

		fioDelete(fioOut);
	}

	if (0 && iInliers >= 3)
	{
		float rotIn[3 * 3];
		float pfModelCenter2In[3];
		float fScaleDiffIn = 1;
		int iInliersRefined = refine_similarity_transform_ransac(
			&(pfPointsIn0[0]), &(pfPointsIn1[0]),
			&(pfScalesIn0[0]), &(pfScalesIn1[0]),
			&(vecMatchProb[0]),
			iInliers, 1000, pfModelCenter,
			pfModelCenter2In, rotIn, fScaleDiffIn, &(piInliers[0])
		);

		// Save transform from image 2 to image 1
		if (pfScale)
			*pfScale = fScaleDiffIn;
		if (pfRot)
		{
			memcpy(pfRot, rotIn, sizeof(float) * 3 * 3);
		}
		if (pfTrans)
		{
			float pfStuff[3] = { 0,0,0 };
			similarity_transform_3point(
				pfStuff, pfTrans,
				pfModelCenter, pfModelCenter2In,
				rotIn, fScaleDiffIn);
		}

		if (pcFeatFile1 && pcFeatFile2 && fioDimensionsEqual(fio1, fio2) && 0)
		{
			FEATUREIO fioOut = fio1;
			fioAllocate(fioOut);

			similarity_transform_invert(pfModelCenter, pfModelCenter2In, rotIn, fScaleDiffIn);

			similarity_transform_image(
				fioOut, fio2,
				pfModelCenter, pfModelCenter2In,
				rotIn, fScaleDiffIn);

			//fioWrite( fio2, "transformed_fio2" );
			sprintf(pcFileName, "%s.ransac", pcFeatFile2, iInliersRefined);

			fioWrite(fioOut, pcFileName);
			fioDelete(fioOut);
		}
	}

	if (pcFeatFile1)
	{
		fioDelete(fio1);
	}
	if (pcFeatFile2)
	{
		fioDelete(fio2);
	}

	//FILE *outfile = fopen( "result.txt", "a+" );
	//fprintf( outfile, "%d\t%d\n", iMatchCount, iInliers );
	//fclose( outfile );

	return iInliers;
}


//
// October 2014 - 2ndNeighbor functions introduced to identify similar content in images.
//
//
int
MatchKeysNearest2ndNeighbor(
	vector<Feature3DInfo> &vecImgFeats1,
	vector<Feature3DInfo> &vecImgFeats2,
	char *pcFeatFile1,
	char *pcFeatFile2
)
{
	vector<int>       vecModelMatches;

	vecModelMatches.resize(vecImgFeats1.size());

	FEATUREIO fio1;
	FEATUREIO fio2;
	char pcFileName[400];
	char *pch;
	if (pcFeatFile1)
	{
		strncpy(pcFileName, pcFeatFile1, sizeof(pcFileName));
		pch = strrchr(pcFileName, '.');
		sprintf(pch, ".hdr");
		if (fioReadNifti(fio1, pcFileName, 1) < 0)
		{
			sprintf(pch, ".nii");
			if (fioReadNifti(fio1, pcFileName, 1) < 0)
			{
				sprintf(pch, ".nii.gz");
				if (fioReadNifti(fio1, pcFileName, 1) < 0)
				{
					memset(&fio1, 0, sizeof(fio1));
				}
			}
		}
	}

	if (pcFeatFile2)
	{
		strncpy(pcFileName, pcFeatFile2, sizeof(pcFileName));
		pch = strrchr(pcFileName, '.');
		sprintf(pch, ".hdr");
		if (fioReadNifti(fio2, pcFileName, 1) < 0)
		{
			sprintf(pch, ".nii");
			if (fioReadNifti(fio2, pcFileName, 1) < 0)
			{
				sprintf(pch, ".nii.gz");
				if (fioReadNifti(fio2, pcFileName, 1) < 0)
				{
					memset(&fio2, 0, sizeof(fio2));
				}
			}
		}
		// 
		//fioFlipAxisY( fio2 );

		//fioReadRaw( fio1, pcFeatFile1 );
		//fioReadRaw( fio2, pcFeatFile2 );
	}


	vector<float> pfPoints0;
	vector<float> pfPoints1;
	vector<float> pfScales0;
	vector<float> pfScales1;
	vector<float> pfOris0;
	vector<float> pfOris1;

	vector<int> piInliers;
	int iMatchCount = 0;
	// Save indices
	vector<int> vecIndex0;
	vector<int> vecIndex1;

	// Generate vector of model features

	vector<int>		vecMatchIndex12;
	vector<float>	vecMatchDist12;
	msComputeNearest2ndNeighbor(vecImgFeats2, vecImgFeats1, vecMatchIndex12, vecMatchDist12);



	vector< pair<int, float> > vecBestMatches;
	for (int i = 0; i < vecMatchIndex12.size(); i++)
	{
		if (vecMatchIndex12[i] >= 0)
		{
			vecBestMatches.push_back(pair<int, float>(i, vecMatchDist12[i]));
		}
	}
	sort(vecBestMatches.begin(), vecBestMatches.end(), pairIntFloatLeastToGreatest);

	for (int iImgFeat = 0; iImgFeat < vecImgFeats1.size(); iImgFeat++)
	{
		Feature3DInfo &featInput = vecImgFeats1[iImgFeat];

		// Flag as unfound...
		vecModelMatches[iImgFeat] = -1;
	}

	int iMaxMatches = 1000;

	vector< float > vecMatchProb;

	map<float, int> mapMatchDuplicateFinder;
	map<float, int> mapMatchDuplicateFinderModel;

	for (int i = 0; i < vecBestMatches.size() && i < iMaxMatches; i++)
	{
		int iIndex = vecBestMatches[i].first;
		float fDist = vecBestMatches[i].second;
		if (fDist > 0.8)
		{
			//break;
		}

		Feature3DInfo &featModel = vecImgFeats2[iIndex];
		Feature3DInfo &featInput = vecImgFeats1[vecMatchIndex12[iIndex]];

		// Test this hash code - to avoid multiple matches between precisely the same locations
		map<float, int>::iterator itAmazingDupeFinder = mapMatchDuplicateFinder.find(featInput.x*featModel.x);
		if (itAmazingDupeFinder != mapMatchDuplicateFinder.end())
		{
			//printf( "Found something you both lost!\n" );
			//continue;
		}
		mapMatchDuplicateFinder.insert(pair<float, int>(featInput.x*featModel.x, vecMatchIndex12[iIndex]));
		mapMatchDuplicateFinderModel.insert(pair<float, int>(featInput.x*featModel.x, iIndex));


		// Should not allow features with same spatial location

		pfPoints0.push_back(featModel.x);
		pfPoints0.push_back(featModel.y);
		pfPoints0.push_back(featModel.z);

		pfPoints1.push_back(featInput.x);
		pfPoints1.push_back(featInput.y);
		pfPoints1.push_back(featInput.z);

		pfScales0.push_back(featModel.scale);
		pfScales1.push_back(featInput.scale);

		for (int o1 = 0; o1<3; o1++)
		{
			for (int o2 = 0; o2<3; o2++)
			{
				pfOris0.push_back(featModel.ori[o1][o2]);
				pfOris1.push_back(featInput.ori[o1][o2]);
			}
		}

		vecIndex0.push_back(iIndex);
		vecIndex1.push_back(vecMatchIndex12[iIndex]);

		// Save prior probability of this feature
		float fFeatsInThreshold = 1;//FeaturesWithinThreshold( vecOrderToFeat[ iIndex ] );
		vecMatchProb.push_back(fFeatsInThreshold);

		if (pcFeatFile1 && pcFeatFile2)
		{
			//Feature3DInfo &featModel = vecImgFeats2[ vecIndex0[i] ];
			// featModel.y = -featModel.y; // Add this for visualizing features mirrored across the y axis
			sprintf(pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_3", i, vecIndex0[i], vecIndex1[i]);
			featModel.OutputVisualFeatureInVolume(fio2, pcFileName, 1, 1);

			//Feature3DInfo &featInput = vecImgFeats1[ vecIndex1[i] ];
			sprintf(pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_2", i, vecIndex0[i], vecIndex1[i]);
			featInput.OutputVisualFeatureInVolume(fio1, pcFileName, 1, 1);
		}


		iMatchCount++;
	}

	return iMatchCount;
}




int
RefineMatchKeys(
	vector<Feature3DInfo> &vecImgFeats1,
	vector<Feature3DInfo> &vecImgFeats2,
	vector<int>       &vecModelMatches,
	int				  iModelFeatsToConsider,
	char *pcFeatFile1,
	char *pcFeatFile2,
	vector<Feature3DInfo> *pvecImgFeats2Transformed, // Transformed features
	int		bMatchConstrainedGeometry, // If true, then consider only approximately correct feature matches

									   // Similarity transform parameters
	float *pfScale, // Scale change 2->1, unary
	float *pfRot,   // Rotation 2->1, 3x3 matrix
	float *pfTrans // Translation 2->1, 1x3 matrix
)
{
	vecModelMatches.resize(vecImgFeats1.size());

	FEATUREIO fio1;
	FEATUREIO fio2;
	if (pcFeatFile1 && pcFeatFile2)
	{
		fioReadRaw(fio1, pcFeatFile1);
		fioReadRaw(fio2, pcFeatFile2);
	}

	char pcFileName[300];
	char *pch;

	if (iModelFeatsToConsider < 0)
	{
		iModelFeatsToConsider = vecImgFeats2.size();
	}


	vector<float> pfPoints0;
	vector<float> pfPoints1;
	vector<float> pfScales0;
	vector<float> pfScales1;
	vector<float> pfOris0;
	vector<float> pfOris1;

	vector<int> piInliers;
	int iMatchCount = 0;
	// Save indices
	vector<int> vecIndex0;
	vector<int> vecIndex1;

	// Generate vector of model features

	vector<int>		vecMatchIndex12;
	vector<float>	vecMatchDist12;
	if (bMatchConstrainedGeometry)
	{
		msComputeNearestNeighborDistanceRatioInfoGeometryConstrained(vecImgFeats2, vecImgFeats1, vecMatchIndex12, vecMatchDist12);
	}
	else
	{
		//msComputeNearestNeighborDistanceRatioInfoApproximate( vecImgFeats2, vecImgFeats1, vecMatchIndex12, vecMatchDist12 );
		msComputeNearestNeighborDistanceRatioInfo(vecImgFeats2, vecImgFeats1, vecMatchIndex12, vecMatchDist12);
	}

	vector< pair<int, float> > vecBestMatches;
	for (int i = 0; i < vecMatchIndex12.size(); i++)
	{
		if (vecMatchIndex12[i] >= 0)
		{
			vecBestMatches.push_back(pair<int, float>(i, vecMatchDist12[i]));
		}
	}
	sort(vecBestMatches.begin(), vecBestMatches.end(), pairIntFloatLeastToGreatest);

	for (int iImgFeat = 0; iImgFeat < vecImgFeats1.size(); iImgFeat++)
	{
		Feature3DInfo &featInput = vecImgFeats1[iImgFeat];

		// Flag as unfound...
		vecModelMatches[iImgFeat] = -1;
	}

	int iMaxMatches = 300;

	vector< float > vecMatchProb;
	map<float, int> mapModelXtoIndex;
	map<float, int> mapModelXtoIndexModel;
	for (int i = 0; i < vecBestMatches.size() && i < iMaxMatches; i++)
	{
		int iIndex = vecBestMatches[i].first;
		float fDist = vecBestMatches[i].second;
		if (fDist > 0.8)
		{
			//break;
		}

		Feature3DInfo &featModel = vecImgFeats2[iIndex];
		Feature3DInfo &featInput = vecImgFeats1[vecMatchIndex12[iIndex]];

		map<float, int>::iterator itXI = mapModelXtoIndex.find(featInput.x);
		if (itXI != mapModelXtoIndex.end())
		{
			int iPrevModelIndex = itXI->second;
			Feature3DInfo &featInputPrev = vecImgFeats1[iPrevModelIndex];
			if (
				featInputPrev.x == featInput.x &&
				featInputPrev.y == featInput.y &&
				featInputPrev.z == featInput.z)
			{
				// Match to same location/scale as another feature
				// skip
				itXI = mapModelXtoIndexModel.find(featInput.x);
				iPrevModelIndex = itXI->second;
				Feature3DInfo &featInputPrevModel = vecImgFeats2[iPrevModelIndex];
				if (
					featInputPrevModel.x == featModel.x &&
					featInputPrevModel.y == featModel.y &&
					featInputPrevModel.z == featModel.z)
				{
					continue;
				}
			}
		}
		// Insert into map
		mapModelXtoIndex.insert(pair<float, int>(featInput.x, vecMatchIndex12[iIndex]));
		mapModelXtoIndexModel.insert(pair<float, int>(featInput.x, iIndex));


		// Should not allow features with same spatial location

		pfPoints0.push_back(featModel.x);
		pfPoints0.push_back(featModel.y);
		pfPoints0.push_back(featModel.z);



		pfPoints1.push_back(featInput.x);
		pfPoints1.push_back(featInput.y);
		pfPoints1.push_back(featInput.z);

		pfScales0.push_back(featModel.scale);
		pfScales1.push_back(featInput.scale);

		for (int o1 = 0; o1<3; o1++)
		{
			for (int o2 = 0; o2<3; o2++)
			{
				pfOris0.push_back(featModel.ori[o1][o2]);
				pfOris1.push_back(featInput.ori[o1][o2]);
			}
		}

		vecIndex0.push_back(iIndex);
		vecIndex1.push_back(vecMatchIndex12[iIndex]);

		// Save prior probability of this feature
		float fFeatsInThreshold = 1;//FeaturesWithinThreshold( vecOrderToFeat[ iIndex ] );
		vecMatchProb.push_back(fFeatsInThreshold);

		iMatchCount++;
	}

	// Now determine transform

	float pfMin[3];
	float pfMax[3];

	getMinMaxDim(vecImgFeats2, pfMin, pfMax);
	float pfModelCenter[3];
	pfModelCenter[0] = (pfMax[0] + pfMin[0]) / 2.0f;
	pfModelCenter[1] = (pfMax[1] + pfMin[1]) / 2.0f;
	pfModelCenter[2] = (pfMax[2] + pfMin[2]) / 2.0f;

	// Output parameters, the result of determine_similarity_transform_ransac()
	float rot[3 * 3];
	float pfModelCenter2[3];
	float fScaleDiff = 1;

	// Save inliers
	for (int iImgFeat = 0; iImgFeat < vecModelMatches.size(); iImgFeat++)
	{
		vecModelMatches[iImgFeat] = -1;
	}
	int iInliers = -1;

	vector< int > vecModelImgFeatCounts;
	vecModelImgFeatCounts.resize(vecImgFeats2.size(), 0);

	int iModelImgFeatMaxCount = 0;
	int iModelImgFeatMaxCountIndex = 0;
	if (iMatchCount <= 3)
	{
		// Not enough matches to determine a solution
		//FILE *outfile = fopen( "result.txt", "a+" );
		//fprintf( outfile, "%d\t%d\n", iMatchCount, 0 );
		//fclose( outfile );
		return iMatchCount;
	}
	piInliers.resize(iMatchCount, 0);
	iInliers =
		//determine_similarity_transform_ransac(
		//	&(pfPoints0[0]), &(pfPoints1[0]),
		//	&(pfScales0[0]), &(pfScales1[0]),
		//	&(vecMatchProb[0]),
		//	iMatchCount,  500, pfModelCenter,
		//	pfModelCenter2, rot, fScaleDiff, &(piInliers[0])
		//	);
		determine_similarity_transform_hough(
			&(pfPoints0[0]), &(pfPoints1[0]),
			&(pfScales0[0]), &(pfScales1[0]),
			&(pfOris0[0]), &(pfOris1[0]),
			&(vecMatchProb[0]),
			iMatchCount, 500, pfModelCenter,
			pfModelCenter2, rot, fScaleDiff, &(piInliers[0])
		);

	// Save transform from image 2 to image 1
	if (pfScale)
		*pfScale = fScaleDiff;
	if (pfRot)
	{
		memcpy(pfRot, rot, sizeof(float) * 3 * 3);
	}
	if (pfTrans)
	{
		float pfStuff[3] = { 0,0,0 };
		similarity_transform_3point(
			pfStuff, pfTrans,
			pfModelCenter, pfModelCenter2,
			rot, fScaleDiff);
	}

	vector<float> pfPointsIn0;
	vector<float> pfPointsIn1;
	vector<float> pfScalesIn0;
	vector<float> pfScalesIn1;

	// Output inliers
	for (int i = 0; i < iMatchCount && 1; i++)
	{
		//piInliers[i]=1;
		if (piInliers[i])
		{
			// Save feature/model match
			vecModelMatches[vecIndex1[i]] = vecIndex0[i];

			// Save inliers for later
			pfPointsIn0.push_back(pfPoints0[i * 3 + 0]);
			pfPointsIn0.push_back(pfPoints0[i * 3 + 1]);
			pfPointsIn0.push_back(pfPoints0[i * 3 + 2]);

			pfPointsIn1.push_back(pfPoints1[i * 3 + 0]);
			pfPointsIn1.push_back(pfPoints1[i * 3 + 1]);
			pfPointsIn1.push_back(pfPoints1[i * 3 + 2]);

			pfScalesIn0.push_back(pfScales0[i]);
			pfScalesIn1.push_back(pfScales1[i]);

			if (pcFeatFile1 && pcFeatFile2)
			{
				Feature3DInfo &featModel = vecImgFeats2[vecIndex0[i]];
				sprintf(pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_3", i, vecIndex0[i], vecIndex1[i]);
				featModel.OutputVisualFeatureInVolume(fio2, pcFileName, 1, 1);

				Feature3DInfo &featInput = vecImgFeats1[vecIndex1[i]];
				sprintf(pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_2", i, vecIndex0[i], vecIndex1[i]);
				featInput.OutputVisualFeatureInVolume(fio1, pcFileName, 1, 1);

				FEATUREIO fioOut;
				//#define OUT_SIZE 101
				//				fioOut.x = OUT_SIZE;
				//				fioOut.y = OUT_SIZE;
				//				fioOut.z = OUT_SIZE;
				//				fioOut.iFeaturesPerVector = 1;
				//				fioOut.t = 1;
				//				fioAllocate( fioOut );
				//				float rotIn[9] = {1,0,0,0,1,0,0,0,1};
				//				float pfCenter1[3] = {OUT_SIZE/2, OUT_SIZE/2, OUT_SIZE/2};
				//				float pfCenter2 [3];
				//
				//				pfCenter2[0] = featModel.x;
				//				pfCenter2[1] = featModel.y;
				//				pfCenter2[2] = featModel.z;
				//				float fMinValue;
				//				fioFindMin( fio2, fMinValue );
				//				fioSet( fioOut, fMinValue );
				//				similarity_transform_image( fioOut, fio2, pfCenter1, pfCenter2, rotIn, featModel.scale/featInput.scale, fMinValue );
				//				//fioFadeGaussianWindow( fioOut, pfCenter1, 3*featInput.scale );
				//				sprintf( pcFileName, "img_%3.3d_mod%4.4d_img%4.4d_3", i, vecIndex0[i], vecIndex1[i] );
				//				fioWrite( fioOut, pcFileName );
				//
				//				pfCenter2[0] = featInput.x;
				//				pfCenter2[1] = featInput.y;
				//				pfCenter2[2] = featInput.z;
				//				fioFindMin( fio1, fMinValue );
				//				fioSet( fioOut, fMinValue );
				//				similarity_transform_image( fioOut, fio1, pfCenter1, pfCenter2, rotIn, 1, fMinValue );
				//				//fioFadeGaussianWindow( fioOut, pfCenter1, 3*featInput.scale );
				//				sprintf( pcFileName, "img_%3.3d_mod%4.4d_img%4.4d_2", i, vecIndex0[i], vecIndex1[i] );
				//				fioWrite( fioOut, pcFileName );
				//
				//
				//				//pfCenter1[0] = 0;
				//				//pfCenter1[1] = 0;
				//				//pfCenter1[2] = 0;
				//				//pfCenter2[0] = 0;
				//				//pfCenter2[1] = 0;
				//				//pfCenter2[2] = 0;
				//				//similarity_transform_image( fio1, fio1, pfCenter1, pfCenter2, rotIn, 1 );
				//
				//
				//				//pfCenter1[0] = featInput.x;
				//				//pfCenter1[1] = featInput.y;
				//				//pfCenter1[2] = featInput.z;
				//				//pfCenter2[0] = featModel.x;
				//				//pfCenter2[1] = featModel.y;
				//				//pfCenter2[2] = featModel.z;
				//				//fioFadeGaussianWindow( fio1, pfCenter1, 3*featInput.scale );
				//
				//				//sprintf( pcFileName, "img_%3.3d_mod%4.4d_img%4.4d_3", i, vecIndex0[i], vecIndex1[i] );
				//				//fioWrite( fio1, pcFileName );
				//
				//				//pfCenter1[0] = featInput.x;
				//				//pfCenter1[1] = featInput.y;
				//				//pfCenter1[2] = featInput.z;
				//				//pfCenter2[0] = featModel.x;
				//				//pfCenter2[1] = featModel.y;
				//				//pfCenter2[2] = featModel.z;
				//				//similarity_transform_image( fio1, fio2, pfCenter1, pfCenter2, rotIn, featModel.scale/featInput.scale );
				//				//fioFadeGaussianWindow( fio1, pfCenter1, 3*featInput.scale );
				//				//sprintf( pcFileName, "img_%3.3d_mod%4.4d_img%4.4d_2", i, vecIndex0[i], vecIndex1[i] );
				//				//fioWrite( fio1, pcFileName );




			}
		}
	}

	if (pvecImgFeats2Transformed)
	{
		// Transform features from image 2 space to image 1 space
		vector<Feature3DInfo> &vecFeats2XFM = *pvecImgFeats2Transformed;
		vecFeats2XFM.resize(vecImgFeats2.size());
		for (int i = 0; i < vecImgFeats2.size(); i++)
		{
			vecFeats2XFM[i] = vecImgFeats2[i];
			vecFeats2XFM[i].SimilarityTransform(pfModelCenter, pfModelCenter2, rot, fScaleDiff);
		}
		msFeature3DVectorOutputText(vecFeats2XFM, "transformed.key");
	}

	// Output entire transformed image
	// If this works the first time I'll eat my shorts...
	if (pcFeatFile1 && pcFeatFile2)
	{
		FEATUREIO fioOut = fio1;
		fioAllocate(fioOut);

		//fioWrite( fioOut, "transformed_in" );

		// Invert transform, since
		similarity_transform_invert(pfModelCenter, pfModelCenter2, rot, fScaleDiff);
		similarity_transform_image(fioOut, fio2, pfModelCenter, pfModelCenter2, rot, fScaleDiff);

		//fioWrite( fio2, "transformed_fio2" );
		sprintf(pcFileName, "%s.hough", pcFeatFile2);
		fioWrite(fioOut, pcFileName);

		fioDelete(fioOut);
	}

	if (iInliers >= 3)
	{
		float rotIn[3 * 3];
		float pfModelCenter2In[3];
		float fScaleDiffIn = 1;
		int iInliersRefined = refine_similarity_transform_ransac(
			&(pfPointsIn0[0]), &(pfPointsIn1[0]),
			&(pfScalesIn0[0]), &(pfScalesIn1[0]),
			&(vecMatchProb[0]),
			iInliers, 1000, pfModelCenter,
			pfModelCenter2In, rotIn, fScaleDiffIn, &(piInliers[0])
		);

		if (pcFeatFile1 && pcFeatFile2 && 1)
		{
			FEATUREIO fioOut = fio1;
			fioAllocate(fioOut);

			similarity_transform_invert(pfModelCenter, pfModelCenter2In, rotIn, fScaleDiffIn);

			similarity_transform_image(
				fioOut, fio2,
				pfModelCenter, pfModelCenter2In,
				rotIn, fScaleDiffIn);

			//fioWrite( fio2, "transformed_fio2" );
			sprintf(pcFileName, "%s.ransac", pcFeatFile2, iInliersRefined);

			fioWrite(fioOut, pcFileName);
			fioDelete(fioOut);
		}
	}

	if (pcFeatFile1 && pcFeatFile2)
	{
		fioDelete(fio1);
		fioDelete(fio2);
	}

	//FILE *outfile = fopen( "result.txt", "a+" );
	//fprintf( outfile, "%d\t%d\n", iMatchCount, iInliers );
	//fclose( outfile );

	return iInliers;
}


int
DetermineTransform(
	vector<Feature3DInfo> &vecImgFeats1,
	vector<Feature3DInfo> &vecImgFeats2,
	vector<int>       &vecModelMatches,

	// Similarity transform parameters
	float *pfScale, // Scale change 2->1, unary
	float *pfRot,   // Rotation 2->1, 3x3 matrix
	float *pfTrans, // Translation 2->1, 1x3 matrix
	char *pcFeatFile1,
	char *pcFeatFile2
)
{
	vector<float> pfPoints0;
	vector<float> pfPoints1;
	vector<float> pfScales0;
	vector<float> pfScales1;
	vector<float> pfOris0;
	vector<float> pfOris1;

	vector<int> piInliers;
	int iMatchCount = 0;
	// Save indices
	vector<int> vecIndex0;
	vector<int> vecIndex1;

	vector< float > vecMatchProb;
	map<float, int> mapModelXtoIndex;
	map<float, int> mapModelXtoIndexModel;
	int iNonZeroMatches = 0;
	for (int i = 0; i < vecModelMatches.size(); i++)
	{
		if (vecModelMatches[i] < 0)
		{
			continue;
		}
		iNonZeroMatches++;

		Feature3DInfo &featModel = vecImgFeats2[vecModelMatches[i]];
		Feature3DInfo &featInput = vecImgFeats1[i];

		map<float, int>::iterator itXI = mapModelXtoIndex.find(featInput.x);
		if (itXI != mapModelXtoIndex.end())
		{
			int iPrevModelIndex = itXI->second;
			Feature3DInfo &featInputPrev = vecImgFeats1[iPrevModelIndex];
			if (
				featInputPrev.x == featInput.x &&
				featInputPrev.y == featInput.y &&
				featInputPrev.z == featInput.z)
			{
				// Match to same location/scale as another feature
				// skip
				itXI = mapModelXtoIndexModel.find(featInput.x);
				iPrevModelIndex = itXI->second;
				Feature3DInfo &featInputPrevModel = vecImgFeats2[iPrevModelIndex];
				if (
					featInputPrevModel.x == featModel.x &&
					featInputPrevModel.y == featModel.y &&
					featInputPrevModel.z == featModel.z)
				{
					continue;
				}
			}
		}
		// Insert into map
		mapModelXtoIndex.insert(pair<float, int>(featInput.x, i));
		mapModelXtoIndexModel.insert(pair<float, int>(featInput.x, vecModelMatches[i]));


		// Should not allow features with same spatial location

		pfPoints0.push_back(featModel.x);
		pfPoints0.push_back(featModel.y);
		pfPoints0.push_back(featModel.z);



		pfPoints1.push_back(featInput.x);
		pfPoints1.push_back(featInput.y);
		pfPoints1.push_back(featInput.z);

		pfScales0.push_back(featModel.scale);
		pfScales1.push_back(featInput.scale);

		for (int o1 = 0; o1<3; o1++)
		{
			for (int o2 = 0; o2<3; o2++)
			{
				pfOris0.push_back(featModel.ori[o1][o2]);
				pfOris1.push_back(featInput.ori[o1][o2]);
			}
		}

		vecIndex0.push_back(vecModelMatches[i]);
		vecIndex1.push_back(i);

		// Save prior probability of this feature
		float fFeatsInThreshold = 1;//FeaturesWithinThreshold( vecOrderToFeat[ iIndex ] );
		vecMatchProb.push_back(fFeatsInThreshold);

		iMatchCount++;
	}

	// Now determine transform

	float pfMin[3];
	float pfMax[3];

	getMinMaxDim(vecImgFeats2, pfMin, pfMax);
	float pfModelCenter[3];
	pfModelCenter[0] = (pfMax[0] + pfMin[0]) / 2.0f;
	pfModelCenter[1] = (pfMax[1] + pfMin[1]) / 2.0f;
	pfModelCenter[2] = (pfMax[2] + pfMin[2]) / 2.0f;

	// Output parameters, the result of determine_similarity_transform_ransac()
	float rot[3 * 3];
	float pfModelCenter2[3];
	float fScaleDiff = 1;

	// Save inliers
	////for( int iImgFeat = 0; iImgFeat < vecModelMatches.size(); iImgFeat++ )
	////{
	////	vecModelMatches[iImgFeat] = -1;
	////}
	int iInliers = -1;

	vector< int > vecModelImgFeatCounts;
	vecModelImgFeatCounts.resize(vecImgFeats2.size(), 0);

	int iModelImgFeatMaxCount = 0;
	int iModelImgFeatMaxCountIndex = 0;
	if (iMatchCount <= 3)
	{
		// Not enough matches to determine a solution
		//FILE *outfile = fopen( "result.txt", "a+" );
		//fprintf( outfile, "%d\t%d\n", iMatchCount, 0 );
		//fclose( outfile );
		return iMatchCount;
	}
	piInliers.resize(iMatchCount, 0);
	iInliers =
		//determine_similarity_transform_ransac(
		//	&(pfPoints0[0]), &(pfPoints1[0]),
		//	&(pfScales0[0]), &(pfScales1[0]),
		//	&(vecMatchProb[0]),
		//	iMatchCount,  500, pfModelCenter,
		//	pfModelCenter2, rot, fScaleDiff, &(piInliers[0])
		//	);
		determine_similarity_transform_hough(
			&(pfPoints0[0]), &(pfPoints1[0]),
			&(pfScales0[0]), &(pfScales1[0]),
			&(pfOris0[0]), &(pfOris1[0]),
			&(vecMatchProb[0]),
			iMatchCount, 500, pfModelCenter,
			pfModelCenter2, rot, fScaleDiff, &(piInliers[0])
		);

	// Save transform from image 2 to image 1
	if (pfScale)
		*pfScale = fScaleDiff;
	if (pfRot)
	{
		memcpy(pfRot, rot, sizeof(float) * 3 * 3);
	}
	if (pfTrans)
	{
		float pfStuff[3] = { 0,0,0 };
		similarity_transform_3point(
			pfStuff, pfTrans,
			pfModelCenter, pfModelCenter2,
			rot, fScaleDiff);
	}

	vector<float> pfPointsIn0;
	vector<float> pfPointsIn1;
	vector<float> pfScalesIn0;
	vector<float> pfScalesIn1;


	// Output inliers
	for (int i = 0; i < iMatchCount && 1; i++)
	{
		//piInliers[i]=1;
		if (piInliers[i])
		{
			// Save feature/model match
			vecModelMatches[vecIndex1[i]] = vecIndex0[i];

			// Save inliers for later
			pfPointsIn0.push_back(pfPoints0[i * 3 + 0]);
			pfPointsIn0.push_back(pfPoints0[i * 3 + 1]);
			pfPointsIn0.push_back(pfPoints0[i * 3 + 2]);

			pfPointsIn1.push_back(pfPoints1[i * 3 + 0]);
			pfPointsIn1.push_back(pfPoints1[i * 3 + 1]);
			pfPointsIn1.push_back(pfPoints1[i * 3 + 2]);

			pfScalesIn0.push_back(pfScales0[i]);
			pfScalesIn1.push_back(pfScales1[i]);
		}
	}

	if (iInliers >= 3)
	{
		float rotIn[3 * 3];
		float pfModelCenter2In[3];
		float fScaleDiffIn = 1;
		int iInliersRefined = refine_similarity_transform_ransac(
			&(pfPointsIn0[0]), &(pfPointsIn1[0]),
			&(pfScalesIn0[0]), &(pfScalesIn1[0]),
			&(vecMatchProb[0]),
			iInliers, 1000, pfModelCenter,
			pfModelCenter2In, rotIn, fScaleDiffIn, &(piInliers[0])
		);

		// Save transform from image 2 to image 1
		if (pfScale)
			*pfScale = fScaleDiffIn;
		if (pfRot)
		{
			memcpy(pfRot, rotIn, sizeof(float) * 3 * 3);
		}
		if (pfTrans)
		{
			float pfStuff[3] = { 0,0,0 };
			similarity_transform_3point(
				pfStuff, pfTrans,
				pfModelCenter, pfModelCenter2In,
				rotIn, fScaleDiffIn);
		}

		if (pcFeatFile1)
		{
			FEATUREIO fio1;
			FEATUREIO fio2;
			fioReadRaw(fio1, pcFeatFile1);
			fioReadRaw(fio2, pcFeatFile2);

			FEATUREIO fioOut = fio1;
			fioAllocate(fioOut);

			similarity_transform_invert(pfModelCenter, pfModelCenter2In, rotIn, fScaleDiffIn);

			similarity_transform_image(
				fioOut, fio2,
				pfModelCenter, pfModelCenter2In,
				rotIn, fScaleDiffIn);

			char pcFileName[400];
			sprintf(pcFileName, "%s.ransac", pcFeatFile2);

			fioWrite(fioOut, pcFileName);
			fioDelete(fio1);
			fioDelete(fio2);
			fioDelete(fioOut);
		}

	}

	//FILE *outfile = fopen( "result.txt", "a+" );
	//fprintf( outfile, "%d\t%d\n", iMatchCount, iInliers );
	//fclose( outfile );

	return iInliers;
}


int
MatchKeysDeformationConstrained(
	vector<Feature3DInfo> &vecImgFeats1,
	vector<Feature3DInfo> &vecImgFeats2,
	vector<int>       &vecModelMatches,
	int				  iModelFeatsToConsider,
	char *pcFeatFile1,
	char *pcFeatFile2,
	FEATUREIO &fioDef
)
{
	vecModelMatches.resize(vecImgFeats1.size());

	FEATUREIO fio1;
	FEATUREIO fio2;
	char pcFileName[400];
	char *pch;
	if (pcFeatFile1)
	{
		strncpy(pcFileName, pcFeatFile1, sizeof(pcFileName));
		pch = strrchr(pcFileName, '.');
		sprintf(pch, ".hdr");
		if (fioReadNifti(fio1, pcFileName) < 0)
		{
			sprintf(pch, ".nii");
			if (fioReadNifti(fio1, pcFileName) < 0)
			{
				memset(&fio1, 0, sizeof(fio1));
			}
		}
	}

	if (pcFeatFile2)
	{
		strncpy(pcFileName, pcFeatFile2, sizeof(pcFileName));
		pch = strrchr(pcFileName, '.');
		sprintf(pch, ".hdr");
		if (fioReadNifti(fio2, pcFileName) < 0)
		{
			sprintf(pch, ".nii");
			if (fioReadNifti(fio2, pcFileName) < 0)
			{
				memset(&fio2, 0, sizeof(fio2));
			}
		}
		// 
		//fioFlipAxisY( fio2 );

		//fioReadRaw( fio1, pcFeatFile1 );
		//fioReadRaw( fio2, pcFeatFile2 );
	}


	if (iModelFeatsToConsider < 0)
	{
		iModelFeatsToConsider = vecImgFeats2.size();
	}


	vector<float> pfPoints0;
	vector<float> pfPoints1;
	vector<float> pfScales0;
	vector<float> pfScales1;
	vector<float> pfOris0;
	vector<float> pfOris1;

	vector<int> piInliers;
	int iMatchCount = 0;
	// Save indices
	vector<int> vecIndex0;
	vector<int> vecIndex1;

	// Generate vector of model features

	vector<int>		vecMatchIndex12;
	vector<float>	vecMatchDist12;
	msComputeNearestNeighborDistanceRatioInfo(vecImgFeats2, vecImgFeats1, vecMatchIndex12, vecMatchDist12);

	vector< pair<int, float> > vecBestMatches;
	for (int i = 0; i < vecMatchIndex12.size(); i++)
	{
		if (vecMatchIndex12[i] >= 0)
		{
			vecBestMatches.push_back(pair<int, float>(i, vecMatchDist12[i]));
		}
	}
	sort(vecBestMatches.begin(), vecBestMatches.end(), pairIntFloatLeastToGreatest);

	for (int iImgFeat = 0; iImgFeat < vecImgFeats1.size(); iImgFeat++)
	{
		Feature3DInfo &featInput = vecImgFeats1[iImgFeat];

		// Flag as unfound...
		vecModelMatches[iImgFeat] = -1;
	}

	int iMaxMatches = 100000;

	vector< float > vecMatchProb;

	//map<float,int> mapModelXtoIndex;
	//map<float,int> mapModelXtoIndexModel;
	//
	//map<float,int> mapInputXtoIndex;
	//map<float,int> mapInputXtoIndexInput;

	map<float, int> mapMatchDuplicateFinder;
	map<float, int> mapMatchDuplicateFinderModel;

	int iInlierCount = 0;

	for (int i = 0; i < vecBestMatches.size() && i < iMaxMatches; i++)
	{
		int iIndex = vecBestMatches[i].first;
		float fDist = vecBestMatches[i].second;
		if (fDist > 0.8)
		{
			//break;
		}
		Feature3DInfo &featModel = vecImgFeats2[iIndex];
		Feature3DInfo &featInput = vecImgFeats1[vecMatchIndex12[iIndex]];

		float pfDefXYZ_in[3];
		pfDefXYZ_in[0] = fioGetPixel(fioDef, featInput.x, featInput.y, featInput.z, 0);
		pfDefXYZ_in[1] = fioGetPixel(fioDef, featInput.x, featInput.y, featInput.z, 1);
		pfDefXYZ_in[2] = fioGetPixel(fioDef, featInput.x, featInput.y, featInput.z, 2);

		float pfDefXYZ[3];
		pfDefXYZ[0] = featModel.x;
		pfDefXYZ[1] = featModel.y;
		pfDefXYZ[2] = featModel.z;

		fDist = 0;
		for (int h = 0; h < 3; h++)
			fDist += (pfDefXYZ[h] - pfDefXYZ_in[h])*(pfDefXYZ[h] - pfDefXYZ_in[h]);
		if (fDist > 0)
			fDist = sqrt(fDist);

		if (fDist < 10)
		{
			iInlierCount++;
			sprintf(pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_3", i, 0, 0);
			featModel.OutputVisualFeatureInVolume(fio2, pcFileName, 1, 1);

			sprintf(pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_2", i, 0, 0);
			featInput.OutputVisualFeatureInVolume(fio1, pcFileName, 1, 1);

		}
	}

	return 1;
}

