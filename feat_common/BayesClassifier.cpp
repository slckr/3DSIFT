
#include <assert.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "BayesClassifier.h"

//
// estimate_distribution_counts()
//
// Learn predictors of the label.
//
// The label L is the feature iLabel located in row iLabel
// of ppImgCodesTraining.
//
// Image ppImgDistributions stores the counts. This image has
// the same number of rows as ppImgCodesTraining, and
// has 4 integer counts per row for the 4 possible events.
//
//
// Event counts for label/feature
//
//   \ | L=0 | L=1 |
// ----|------------
// F=0 |  0  |  1  |
// ----|------------
// F=1 |  2  |  3  |
// ----|------------
//
//	pfProbs[0] = piCounts[0]*fWeightLabelZer + iPriorSamples;
//	pfProbs[1] = piCounts[1]*fWeightLabelOne + iPriorSamples;
//	pfProbs[2] = piCounts[2]*fWeightLabelZer + iPriorSamples;
//	pfProbs[3] = piCounts[3]*fWeightLabelOne + iPriorSamples;
//


BayesClassifier::BayesClassifier()
{
	m_pbfdDistributions = 0;
	m_piCounts[0] = 0;
	m_piCounts[1] = 0;
}

BayesClassifier::~BayesClassifier()
{
	if( m_pbfdDistributions )
	{
		delete [] m_pbfdDistributions;
		m_pbfdDistributions = 0;
	}
	if( m_piCounts[0] )
	{
		delete [] m_piCounts[0];
		m_piCounts[0] = 0;
	}
	if( m_piCounts[1] )
	{
		delete [] m_piCounts[1];
		m_piCounts[1] = 0;
	}
}

void
BayesClassifier::LearnClassifier(
					   svm_problem &prob,
					   int iDirichletPrior
					   )
{
	// Free memory to start, if already allocated
	if( m_pbfdDistributions )
	{
		delete [] m_pbfdDistributions;
		m_pbfdDistributions = 0;
	}
	if( m_piCounts )
	{
		delete [] m_piCounts[0];
		delete [] m_piCounts[1];
		m_piCounts[0] = 0;
		m_piCounts[1] = 0;
	}

	m_iDirichletPrior = iDirichletPrior;

	assert( m_pbfdDistributions == 0 );
	assert( m_iDirichletPrior >= 0 );

	m_iFeatures = (prob.x[1]-2)->index + 1; // Note that the last feature in the vector has index -1, SVMLIB format
	assert( !m_pbfdDistributions );
	m_pbfdDistributions = new BAYESFEATUREDIST[m_iFeatures];
	assert( m_pbfdDistributions );
	assert( !m_piCounts[0] );
	assert( !m_piCounts[1] );
	m_piCounts[0] = new int[m_iFeatures];
	m_piCounts[1] = new int[m_iFeatures];
	memset( m_piCounts[0], 0, sizeof(int)*m_iFeatures );
	memset( m_piCounts[1], 0, sizeof(int)*m_iFeatures );
	assert( m_piCounts[0] );
	assert( m_piCounts[1] );

	// Counts
	m_iOnes = 0;
	m_iZeros = 0;
	m_fBiasTermOnes = 0;
	m_fBiasTermZeros = 0;

	int iFeat;

	// Zero classifier distributions,
	// count prior distribution of data.

	int iPosDistSum = 0;
	int iNegDistSum = 0;
	float *pfPosDist = new float[m_iFeatures];
	float *pfNegDist = new float[m_iFeatures];
	for( iFeat = 0; iFeat < m_iFeatures; iFeat++ )
	{
		m_pbfdDistributions[iFeat][0] = 0;
		m_pbfdDistributions[iFeat][1] = 0;
		m_pbfdDistributions[iFeat][2] = 0;
		m_pbfdDistributions[iFeat][3] = 0;
		pfPosDist[iFeat]=0;
		pfNegDist[iFeat]=0;
	}

	// Generate feature counts
	int iCode;
	for( iCode= 0; iCode < prob.l; iCode++ )
	{
		// Label
		int iLabel = -1;
		if( prob.y[iCode] == 1 )
		{
			iLabel = 1;
			m_iOnes++;
		}
		else if( prob.y[iCode] == -1 )
		{
			iLabel = 0;
			m_iZeros++;
		}
		assert( iLabel == 0 || iLabel == 1 );

		svm_node *snNodes = prob.x[iCode];
		iFeat = 0;
		while( snNodes[iFeat].index >= 0 )
		{
			int iValue = (int)(snNodes[iFeat].value);

			assert( iValue == 0 || iValue == 1 );

			m_pbfdDistributions[iFeat][2*iValue+iLabel] += 1;

			// Count positive feautre occurrences
			if( iValue == 1 )
			{
				m_piCounts[iLabel][iFeat] += 1;
			}

			if( iValue == 1 )
			{
				iPosDistSum++;
				pfPosDist[iFeat]++;
			}
			else if( iValue == 0 )
			{
				iNegDistSum++;
				pfNegDist[iFeat]++;
			}
			else
			{
				printf( "Error: strange label value!\n" );
				assert( 0 );
			}

			iFeat++;
		}
		assert( iFeat == m_iFeatures );
	}

	// Now add prior samples & generate distributions
	for( iFeat = 0; iFeat < m_iFeatures; iFeat++ )
	{
		float *pfProbs = m_pbfdDistributions[iFeat];

		int iTotalLabels = m_iOnes + m_iZeros;

		float fWeightLabelOne = (0.5f*iTotalLabels) / (float)(m_iOnes);
		float fWeightLabelZer = (0.5f*iTotalLabels) / (float)(m_iZeros);

		pfProbs[0] = pfProbs[0]*fWeightLabelZer + m_iDirichletPrior;
		pfProbs[1] = pfProbs[1]*fWeightLabelOne + m_iDirichletPrior;
		pfProbs[2] = pfProbs[2]*fWeightLabelZer + m_iDirichletPrior;
		pfProbs[3] = pfProbs[3]*fWeightLabelOne + m_iDirichletPrior;

		float fProbSum = pfProbs[0] + pfProbs[1] + pfProbs[2] + pfProbs[3];

		pfProbs[0] /= fProbSum;
		pfProbs[1] /= fProbSum;
		pfProbs[2] /= fProbSum;
		pfProbs[3] /= fProbSum;
	}

	// Now calcuate bias terms: the expected value of a log likelihood ratio give positive/negative feature occurrences
	
	double dExpectedLogZero = 0;
	double dExpectedLogOne = 0;
	for( iFeat = 0; iFeat < m_iFeatures; iFeat++ )
	{
		float *pfProbs = m_pbfdDistributions[iFeat];
	
		float fProbZer;
		float fProbOne;

		fProbZer = pfProbs[2];
		fProbOne = pfProbs[3];
		dExpectedLogOne  += pfPosDist[iFeat]*( log(fProbOne) - log(fProbZer) );

		fProbZer = pfProbs[0];
		fProbOne = pfProbs[1];
		dExpectedLogZero  += pfNegDist[iFeat]*( log(fProbOne) - log(fProbZer) );
	}

	m_fBiasTermOnes = (float)(dExpectedLogOne/(double)iPosDistSum);
	m_fBiasTermZeros = (float)(dExpectedLogZero/(double)iNegDistSum);

	// New bias term calculation: based on zeroing training set bias

	FILE *outfile = fopen( "bias_test.txt", "wt" );
	double dSumLogOneMinusLogZero = 0;
	for( iCode= 0; iCode < prob.l; iCode++ )
	{
		// Label
		int iLabel = ( prob.y[iCode] == 1 ? 1 : 0 );
		assert( iLabel == 0 || iLabel == 1 );

		svm_node *snNodes = prob.x[iCode];
		iFeat = 0;
		int iCodePosFeats = 0;
		double dLogOneMinusLogZero = 0;
		while( snNodes[iFeat].index >= 0 )
		{
			int iValue = (int)(snNodes[iFeat].value);
			assert( iValue == 0 || iValue == 1 );

			if( iValue==1 )
			{
				iCodePosFeats++;

				float *pfProbs = m_pbfdDistributions[iFeat];

				float fProbZer = pfProbs[2];
				float fProbOne = pfProbs[3];

				dLogOneMinusLogZero += pfPosDist[iFeat]*( log(fProbOne) - log(fProbZer) );
			}

			iFeat++;
		}
		fprintf( outfile, "%d\t%d\t%f\t%d\n", iCode, iLabel, dLogOneMinusLogZero, iCodePosFeats );
		dSumLogOneMinusLogZero += dLogOneMinusLogZero / (double)iCodePosFeats;
		assert( iFeat == m_iFeatures );
	}
	fclose( outfile );

	// Bias term is the average normalized sum
	//m_fBiasTerm = (float)(dSumLogOneMinusLogZero/(double)prob.l);

	delete [] pfPosDist;

}

double
BayesClassifier::Classify(
			  const struct svm_node *x,
			  double &dSum,
			  float &fLogProbZero,
			  float &fLogProbOne
			  )
{
	const svm_node *snNodes = x;
	int iFeat = 0;
	fLogProbZero = 0;
	fLogProbOne = 0;
	int iOnes = 0;
	int iZeros = 0;
	while( snNodes[iFeat].index >= 0 )
	{
		int iValue = (int)(snNodes[iFeat].value);

		assert( iValue == 0 || iValue == 1 );

		if( iValue == 1 ) // Incorporate geometrical modeling here
		{
			float *pfProbs = m_pbfdDistributions[iFeat];

			float fProbZer = pfProbs[2];
			float fProbOne = pfProbs[3];

			fLogProbOne += log(fProbOne);
			fLogProbZero += log(fProbZer);
			//if( fProbOne > fProbZer )
			//	fLogProbOne++;
			//else
			//	fLogProbZero++;

			iOnes++;
		}
		else
		{
			//float *pfProbs = m_pbfdDistributions[iFeat];

			//float fProbZer = pfProbs[0];
			//float fProbOne = pfProbs[1];

			//fLogProbOne += log(fProbOne);
			//fLogProbZero += log(fProbZer);

			iZeros++;
		}

		iFeat++;
	}
	assert( iFeat == m_iFeatures );

	//float fClass = fLogProbOne - fLogProbZero - (iOnes + iZeros)*m_fBiasTermOnes; // Gave amazing but unexplainable results for leave-one-out testing

	// Bias terms should 
	//float fClass = (fLogProbOne - fLogProbZero) - (iOnes*m_fBiasTermOnes + iZeros*m_fBiasTermZeros);

	float fClass = fLogProbOne - fLogProbZero; // Used in all published results

	//float fClass = (fLogProbOne - fLogProbZero)/(float)iOnes;
	//float fClass = (fLogProbOne - fLogProbZero)/(float)iOnes - m_fBiasTerm;

	dSum = fClass;

	if( fClass > 0 )
	{
		return 1;
	}
	else
	{
		return -1;
	}
}

double
BayesClassifier::ClassifyBoosting(
			  const struct svm_node *x,
			  double *pdCoefficients,
			  double &dSum,
			  float &fLogProbZero,
			  float &fLogProbOne
			  )
{
	const svm_node *snNodes = x;
	int iFeat = 0;
	fLogProbZero = 0;
	fLogProbOne = 0;
	int iOnes = 0;
	double dTotalSum = 0;
	dSum = 0;
	while( snNodes[iFeat].index >= 0 )
	{
		int iValue = (int)(snNodes[iFeat].value);

		assert( iValue == 0 || iValue == 1 );

		float *pfProbs = m_pbfdDistributions[iFeat];
		float fProbZer;
		float fProbOne;

		if( iValue == 1 )
		{
			fProbZer = pfProbs[2];
			fProbOne = pfProbs[3];
			iOnes++;
		}
		else
		{
			fProbZer = pfProbs[0];
			fProbOne = pfProbs[1];
		}

		// Boosting classifier is simply a linear sum of class 1 coefficients
		if( fProbOne >= fProbZer )
		{
			dSum += pdCoefficients[iFeat];
		}

		dTotalSum += pdCoefficients[iFeat];

		iFeat++;
	}
	assert( iFeat == m_iFeatures );

	if( dSum >= 0.5*dTotalSum )
	{
		return 1;
	}
	else
	{
		return -1;
	}
}

double
BayesClassifier::ClassifyWeights(
			  const struct svm_node *x,
			  const struct svm_node *w,
			  double &dSum,
			  float &fLogProbZero,
			  float &fLogProbOne
			  )
{
	const svm_node *snNodes = x;
	const svm_node *snWeights = w;
	int iFeat = 0;
	fLogProbZero = 0;
	fLogProbOne = 0;
	int iOnes = 0;
	while( snNodes[iFeat].index >= 0 )
	{
		int iValue = (int)(snNodes[iFeat].value);
		float fWeight = (float)(snWeights[iFeat].value);

		assert( iValue == 0 || iValue == 1 );

		if( iValue == 1 )
		{
			float *pfProbs = m_pbfdDistributions[iFeat];

			float fProbZer = (pfProbs[2]-1)*fWeight + 1;
			float fProbOne = (pfProbs[3]-1)*fWeight + 1;
			
			fLogProbOne += log(fProbOne);
			fLogProbZero += log(fProbZer);

			iOnes++;
		}
		else
		{
			//float *pfProbs = m_pbfdDistributions[iFeat];

			//float fProbZer = (pfProbs[0]-1)*fWeight + 1;
			//float fProbOne = (pfProbs[1]-1)*fWeight + 1;

			//fLogProbOne += log(fProbOne);
			//fLogProbZero += log(fProbZer);

			//iOnes++;
		}

		iFeat++;
	}
	assert( iFeat == m_iFeatures );

	///////float fClass = fLogProbOne - fLogProbZero - iOnes*m_fBiasTerm;
	float fClass = fLogProbOne - fLogProbZero;
	//float fClass = (fLogProbOne - fLogProbZero)/(float)iOnes;
	//float fClass = (fLogProbOne - fLogProbZero)/(float)iOnes - m_fBiasTerm;

	dSum = fClass;

	if( fClass > 0 )
	{
		return 1;
	}
	else
	{
		return -1;
	}
}


double
BayesClassifier::ClassifyRawSamples(
			  const struct svm_node *x,
			  double &dSum,
			  float &fLogProbZero,
			  float &fLogProbOne
			  )
{
	const svm_node *snNodes = x;
	int iFeat = 0;
	int iCountZero = 0;
	int iCountOne = 0;
	int iOnes = 0;

	// Get total counts
	int iTotalZeros = 0;
	int iTotalOnes = 0;
	for( int i = 0; i < m_iFeatures; i++ )
	{
		iTotalZeros += m_piCounts[0][i];
		iTotalOnes += m_piCounts[1][i];
	}

	while( snNodes[iFeat].index >= 0 )
	{
		int iValue = (int)(snNodes[iFeat].value);

		assert( iValue == 0 || iValue == 1 );

		if( iValue == 1 )
		{			
			//iCountZero += m_piCounts[0][iFeat];
			//iCountOne += m_piCounts[1][iFeat];

			iCountOne += (m_piCounts[1][iFeat]-m_piCounts[0][iFeat])*(m_piCounts[1][iFeat]+m_piCounts[0][iFeat]);

			iOnes++;
		}
		else
		{
			//float *pfProbs = m_pbfdDistributions[iFeat];

			//float fProbZer = pfProbs[0];
			//float fProbOne = pfProbs[1];

			//iCountZero += log(fProbZer);
			//iCountOne += log(fProbOne);

			//iOnes++;
		}

		iFeat++;
	}
	assert( iFeat == m_iFeatures );

	///////float fClass = iCountOne - iCountZero - iOnes*m_fBiasTerm;
	//float fClass = (iCountOne - iCountZero)/(float)iOnes;
	float fBias = 0;//(iTotalOnes - iTotalZeros)/(float)(iTotalOnes + iTotalZeros);
	float fClass = (iCountOne - iCountZero)*(1.0f-fBias);

	dSum = fClass;

	dSum = iCountOne;

	if( fClass > 0 )
	{
		return 1;
	}
	else
	{
		return -1;
	}
}


int
BayesClassifier::OutputCounts(
		char *pcFileName
	)
{
	FILE *outfile = fopen( pcFileName, "wt" );
	if( !outfile )
	{
		return -1;
	}
	for( int i =0; i < m_iFeatures; i++ )
	{
		fprintf( outfile, "%d\t%d\n", m_piCounts[0][i], m_piCounts[1][i] );
	}
	fclose( outfile );
	return 0;
}
