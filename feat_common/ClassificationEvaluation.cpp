
#include "ClassificationEvaluation.h"
#include <assert.h>

using namespace std;

#include <map>
#include <algorithm>

// Return whether first element is greater than the second
bool scoreGreater(
				  const ClassificationEvaluation::LabelScore &ls1,
				  const ClassificationEvaluation::LabelScore &ls2
				  )
{
   return ls1.m_fScore < ls2.m_fScore;
}


int
ClassificationEvaluation::Clear(
	  )
{
	m_vecLS.clear();
	return 0;
}

int
ClassificationEvaluation::AddLabelScore(
		int iLabel,
		float fScore,
		int iIndex
		)
{
	assert( iLabel == 0 || iLabel == 1 );

	m_vecLS.push_back( LabelScore( iLabel, fScore, iIndex ) );

	return 0;
}

float
ClassificationEvaluation::CalculateEER(
		FILE *outfile,
		float *pfThreshold,
		vector<int> *pvecCorrect,
		vector<int> *pvecIncorrect
		)
{
	if( m_vecLS.size() <= 2 )
	{
		return -1;
	}
	sort( m_vecLS.begin(), m_vecLS.end(), scoreGreater  );

	int piCounts[2] = {0,0};
	for( int i = 0; i < m_vecLS.size(); i++ )
	{
		piCounts[ m_vecLS[i].m_iLabel ]++;
	}

	vector< float > viAccum[2];

	viAccum[0].resize( m_vecLS.size()+2 );
	viAccum[1].resize( m_vecLS.size()+2 );

	// Start no 0s, all 1s
	viAccum[0][0] = (float)0;
	viAccum[1][0] = (float)piCounts[1];

	for( int i = 0; i < m_vecLS.size(); i++ )
	{
		LabelScore &ls = m_vecLS[i];

		if( m_vecLS[i].m_iLabel == 0 )
		{
			viAccum[0][i+1] = viAccum[0][i] + 1;
			viAccum[1][i+1] = viAccum[1][i];
		}
		else if( m_vecLS[i].m_iLabel == 1 )
		{
			viAccum[0][i+1] = viAccum[0][i];
			viAccum[1][i+1] = viAccum[1][i] - 1;
		}
		else
		{
			assert( 0 );
		}
	}
	// Finish with all 0s, no 1s
	viAccum[0][m_vecLS.size()+1] = (float)piCounts[1];
	viAccum[1][m_vecLS.size()+1] = (float)0;

	for( int i = 0; i < m_vecLS.size()+2; i++ )
	{
		viAccum[0][i] /= (float)piCounts[0];
		viAccum[1][i] /= (float)piCounts[1];
	}

	if( outfile )
	{
		for( int i = 0; i < m_vecLS.size(); i++ )
		{
			fprintf( outfile, "%d\t%d\t%f\t%f\t%f\n", m_vecLS[i].m_iIndex, m_vecLS[i].m_iLabel, m_vecLS[i].m_fScore, viAccum[0][i+1], viAccum[1][i+1] );
		}
	}

	//
	// Find equal error classification rate point
	// Stop just after the class rate for [1] is better than [0]
	//
	float fEER;
	int j = 0;
	while( viAccum[0][j] < viAccum[1][j] )
	{
		j++;
	}

	if( pvecCorrect && pvecIncorrect )
	{
		for( int iCount = 0; iCount < m_vecLS.size(); iCount++ )
		{
			if( iCount < j )
			{
				if( m_vecLS[iCount].m_iLabel == 0 )
				{
					(*pvecCorrect)[ m_vecLS[iCount].m_iIndex ]++;
				}
				else
				{
					(*pvecIncorrect)[ m_vecLS[iCount].m_iIndex ]++;
				}
			}
			else
			{
				if( m_vecLS[iCount].m_iLabel == 1 )
				{
					(*pvecCorrect)[ m_vecLS[iCount].m_iIndex ]++;
				}
				else
				{
					(*pvecIncorrect)[ m_vecLS[iCount].m_iIndex ]++;
				}
			}
		}
	}

	if( j == 0 || j == m_vecLS.size() )
	{
		// Nothing is classified
		fEER = 0.0f;
		if( pfThreshold ) *pfThreshold = -10000;
	}
	else
	{
		float p00 = viAccum[0][j-1];
		float p10 = viAccum[1][j-1];

		float p01 = viAccum[0][j];
		float p11 = viAccum[1][j];

		// Interpolate point
		float m0 = p01-p00;
		float m1 = p11-p10;

		assert( m1 != m0 );

		float pMid = (p00-p10)/(m1 - m0);
		fEER = pMid*m1 + p10;
		if( pfThreshold )
			*pfThreshold = pMid*(m_vecLS[j-1].m_fScore-m_vecLS[j-2].m_fScore) + m_vecLS[j-2].m_fScore;
	}

	return fEER;
}

int
ClassificationEvaluation::OptimalEERClassification(
		map<int,int> &mapClass
	)
{
	float fThresEER;
	CalculateEER( 0, &fThresEER );

	mapClass.clear();

	for( int i = 0; i < m_vecLS.size(); i++ )
	{
		if( m_vecLS[i].m_fScore <= fThresEER )
		{
			mapClass.insert( pair<int,int>( m_vecLS[i].m_iIndex, 0 ) );
		}
		else
		{
			mapClass.insert( pair<int,int>( m_vecLS[i].m_iIndex, 1 ) );
		}
	}

	return 0;
}

int
ClassificationEvaluation::WriteToFile(
	FILE *outfile
)
{
	if( !outfile )
	{
		return -1;
	}

	for( int i = 0; i < m_vecLS.size(); i++ )
	{
		fprintf( outfile, "%d\t%f\n", m_vecLS[i].m_iLabel, m_vecLS[i].m_fScore  );
	}
	return 0;
}

int
ClassificationEvaluation::ReadFromFile(
	FILE *infile
)
{
	
	if( !infile )
	{
		return -1;
	}

	Clear();

		int iLabel;
		float fScore;
		while( fscanf( infile, "%d\t%f\n", &iLabel, &fScore  ) == 2 )
		{
			AddLabelScore( iLabel, fScore );
		}

	return 0;
}
