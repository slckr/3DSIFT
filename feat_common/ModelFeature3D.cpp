
#include "ModelFeature3D.h"
#include <string.h>
#include <algorithm>

#ifdef OPEN_MP_
#include <omp.h>
#endif

#include "FishersExactTest.h"
#include "ScaleSpaceHashXYZ.h"

#include "ClassifyGeometrySXYZ.h"
#include "ClassificationEvaluation.h"

//#include "HashRank64.h"

#define PI 3.14159
#define MAX_AXIS_ROTATIONS_3D 24

// Global iteration count, for file output
// For decision tree file output
static int g_iIteration = -1;

int
InitHashTable(
			  ScaleSpaceXYZHash &ssh,
			  const vector<MODEL_FEATURE>	&vecFeats // Array of features
			  )
{

	/////// Build hash table
	float pfMin[3];
	float pfMax[3];
	float fScaleMin;
	float fScaleMax;
	//float pfIncPerScale[3] = { 0.5f, 0.5f, 0.5f };
	float pfIncPerScale[3] = { 1.0f, 1.0f, 1.0f };
	float fScaleInc = 1.5f;

	// Init min/max
	fScaleMin = vecFeats[0].scale;
	fScaleMax = vecFeats[0].scale;
	pfMin[0] = vecFeats[0].x;
	pfMin[1] = vecFeats[0].y;
	pfMin[2] = vecFeats[0].z;
	pfMax[0] = vecFeats[0].x;
	pfMax[1] = vecFeats[0].y;
	pfMax[2] = vecFeats[0].z;

	// Search for min/max
	for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	{
		const MODEL_FEATURE &feat = vecFeats[iFeat];

		if( feat.x < pfMin[0] ) pfMin[0] = feat.x;
		if( feat.y < pfMin[1] ) pfMin[1] = feat.y;
		if( feat.z < pfMin[2] ) pfMin[2] = feat.z;
		if( feat.x > pfMax[0] ) pfMax[0] = feat.x;
		if( feat.y > pfMax[1] ) pfMax[1] = feat.y;
		if( feat.z > pfMax[2] ) pfMax[2] = feat.z;

		if( feat.scale < fScaleMin  ) fScaleMin = feat.scale;
		if( feat.scale > fScaleMax  ) fScaleMax = feat.scale;
	}

	for( int i = 0; i < 3; i++ )
	{
		//if( pfMin[i] < 0 )
		//	pfMin[i] = 0;
		//if( pfMax[i] > 500 )
		//	pfMax[i] = 500;
	}

	// Init & fill scale space hash table
	ssh.Init( 3,
		(float*)pfMin,
		(float*)pfMax,
		(float*)pfIncPerScale,
		fScaleMin,
		fScaleMax,
		fScaleInc
		);
	for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	{
		const MODEL_FEATURE &feat = vecFeats[iFeat];
		float pfXYZ[3];
		pfXYZ[0] = feat.x;
		pfXYZ[1] = feat.y;
		pfXYZ[2] = feat.z;
		int bOutOfBounds = 0;
		for( int i = 0; i < 3; i++ )
		{
			if( pfXYZ[i] < pfMin[i]
			|| pfXYZ[i] > pfMax[i] )
			{
				bOutOfBounds = 1;
			}
		}
		if( !bOutOfBounds )
		{
			ssh.Insert( pfXYZ, feat.scale, iFeat );
		}
	}

	return 0;
}

static int
outputIntVector(
			 vector<int> &vec,
			 char *pcFileName
			 )
{
	FILE *outfile = fopen( pcFileName, "wt" );
	for( int i = 0; i < vec.size(); i++ )
	{
		fprintf( outfile, "%d\t%d\n", i, vec[i] );
	}
	fclose( outfile );
	return 1;
}


bool indexInfoDistSqrLeastToGreatest( const IndexInfo &ii1, const IndexInfo &ii2 )
{
	return ii1.DistSqr() < ii2.DistSqr();
}

int
ModelFeature3D::ReadDataInteger(
		char *pcFileName,
		TextFile &tfText,
		vector<int> &vecInt
	)
{
	if( tfText.Read( pcFileName ) != 0 )
	{
		return -1;
	}
	vecInt.clear();

	int iLabelCount = 0;
	int iLine = 0;
	while( iLabelCount < vecImgName.size() && iLine < tfText.Lines() )
	{
		int iLabel;
		int iReturn = sscanf( tfText.GetLine( iLine ), "%d", &iLabel );
		if( iReturn == 1 )
		{
			vecInt.push_back( iLabel );
			iLabelCount++;
		}
		iLine++;
	}
	return iLabelCount;
}

int
ModelFeature3D::ReadDataFloat(
		char *pcFileName,
		TextFile &tfText,
		vector<float> &vecFloat
	)
{
	if( tfText.Read( pcFileName ) != 0 )
	{
		return -1;
	}
	vecFloat.clear();

	int iLabelCount = 0;
	int iLine = 0;
	while( iLabelCount < vecImgName.size() && iLine < tfText.Lines() )
	{
		float fLabel;
		int iReturn = sscanf( tfText.GetLine( iLine ), "%f", &fLabel );
		if( iReturn == 1 )
		{
			vecFloat.push_back( fLabel );
			iLabelCount++;
		}
		iLine++;
	}
	return iLabelCount;
}


int
ModelFeature3D::ClassifyGenerateTrainingData(
		)
{
	if( m_prob.y )
	{
		delete [] m_prob.y;
		m_prob.y = 0;
	}
	if( m_prob.x )
	{
		if( m_prob.x[0] )
		{
			delete [] m_prob.x[0];
		}
		delete [] m_prob.x;
		m_prob.x = 0;
	}

	// Allocate labels
	m_prob.y = new double[vecImgLabels.size()];
	// Allocate feature pointers
	m_prob.x = new svm_node*[vecImgLabels.size()];
	// Allocate block of memory for all feature data
	m_prob.x[0] = new svm_node[vecImgLabels.size()*(vecOrderToFeat.size()+1)];

	// Set all indices, and set values to 0
	for( int iImg = 0; iImg < vecImgLabels.size(); iImg++ )
	{
		for( int iFeat = 0; iFeat < vecOrderToFeat.size(); iFeat++ )
		{
			m_prob.x[0][iImg*(vecOrderToFeat.size()+1) + iFeat].index = iFeat;
			m_prob.x[0][iImg*(vecOrderToFeat.size()+1) + iFeat].value = 0;
		}
		// Flag end of vector
		m_prob.x[0][iImg*(vecOrderToFeat.size()+1) + vecOrderToFeat.size()].index = -1;
		m_prob.x[0][iImg*(vecOrderToFeat.size()+1) + vecOrderToFeat.size()].value = -1;
	}

	//
	// This maps an image index to it's classifier data index. This
	// is important, because not all images have valid labels.
	//
	m_prob.l = 0;
	int *piImgToClassDataIndex = new int[vecImgLabels.size()];
	for( int iImg = 0; iImg < vecImgLabels.size(); iImg++ )
	{
		int iLabel = vecImgLabels[iImg];
		if( iLabel == 0 || iLabel == 1 )
		{
			// Label is valid, either 0 (normal control) or 1 (Parkinsons)
			m_prob.y[m_prob.l] = iLabel==0 ? -1 : 1;
			m_prob.x[m_prob.l] = m_prob.x[0] + m_prob.l*(vecOrderToFeat.size()+1);
			piImgToClassDataIndex[iImg] = m_prob.l;
			m_prob.l++;
		}
		else
		{
			// No valid classi
			piImgToClassDataIndex[iImg] = -1;
		}
	}

	// For each model feature, set
	int *piImages = new int[vecImgLabels.size()];
	for( int i = 0; i < vecOrderToFeat.size(); i++ )
	{
		int iFeat = vecOrderToFeat[i];

		int iCount = FeaturesWithinThreshold( iFeat, piImages );

		// Set if feature was found in image, if so, set in classifier data
		for( int iImg = 0; iImg < vecImgLabels.size(); iImg++ )
		{
			//
			// Here, considering only unique feature instances in learning,
			// classification is approximately equal or better
			//  3:15 PM 3/30/2009
			//
			if( piImages[iImg] != -1 ) // feature could have been found more than once in same image (-2, -3, etc.)
			//if( piImages[iImg] > -1 ) // feature was found only once in image iImg. 3:15 PM 3/30/2009
			{
				if( piImgToClassDataIndex[iImg] != -1 ) // image has a valid classification
				{
					// Feature was found in a valid training image, set label
					//!! this needs to be fixed !!
					int iDataRow = piImgToClassDataIndex[iImg];
					m_prob.x[iDataRow][i].value = 1;
				}
			}
		}
	}

	delete [] piImgToClassDataIndex;
	delete [] piImages;

	return 0;
}

int
ModelFeature3D::BuildClassifier(
		int iClassifierType, // 1: Bayesian, 2: Linear SVM, 3: RBF SVM
		float fParam1, // Bayesian: Dirichlet prior, SVM: C
		float fParam2 // Bayesian: NA, SVM: RBF
		)
{
	g_iIteration++;

	ClassifyGenerateTrainingData();

	//// Output in decision tree format
	//char pcFileName[400];
	//sprintf( pcFileName, "decision_tree%3.3d.label", g_iIteration );
	//FILE *outfile = fopen( pcFileName, "wt" );
	//fprintf( outfile, "decision\n" );
	//fclose( outfile );
	//sprintf( pcFileName, "decision_tree%3.3d.data", g_iIteration );
	//outfile = fopen( pcFileName, "wt" );
	//for( int iVec = 0; iVec < m_prob.l; iVec++ )
	//{
	//	int iResult = (int)m_prob.y[iVec] == 1 ? 1 : 0;
	//	fprintf( outfile, "%d", iResult );
	//	int iNode = 0;
	//	while( m_prob.x[iVec][iNode].index != -1 )
	//	{
	//		fprintf( outfile, ",%d", (int)m_prob.x[iVec][iNode].value ); 
	//		iNode++;
	//	}
	//	fprintf( outfile, "\n" ); 
	//}
	//fclose( outfile );

	// Delete old SVM model
	if( m_pmodel )
	{
		svm_destroy_model(m_pmodel);
		m_pmodel = 0;
	}

	// Build classifier
	switch( iClassifierType )
	{

	case ctBayesian: // Bayesian
		m_bayesClass.LearnClassifier( m_prob, (int)fParam1 );
		break;

	case ctSVM_Linear: // Linear SVM
		m_param.kernel_type = LINEAR;
		m_param.C = (double)fParam1;
		m_pmodel = svm_train(&m_prob,&m_param);
		break;

	case ctSVM_RBF: // RBF SVM
		m_param.kernel_type = RBF;
		m_param.C     = (double)fParam1;
		m_param.gamma = (double)fParam2;
		m_pmodel = svm_train(&m_prob,&m_param);
		break;

	case ctNearestNeighbor:
		BayesianNearnessBuild();
		break;

	case ctBayesianOverlap:
		BayesianOverlapBuild( (int)fParam1 );
		break;

	default:
		assert( 0 );
		return -1;
	}

	return 0;
}


int
ModelFeature3D::ClassifierDistBuild(
		int iDirichletPrior
	)
{
	vecOrderClassDistRatios.resize( vecOrderToFeat.size() );
	int piCounts[4];


	for( int i = 0; i < vecOrderToFeat.size(); i++ )
	{
//		FILE *outfile = fopen( "dist_classify.txt", "wt" );
		int iFeat = vecOrderToFeat[i];
		MODEL_FEATURE &feat = vecFeats[iFeat];

		memset( piCounts, 0, sizeof(piCounts) );
		piCounts[0] = 0;
		piCounts[1] = 0;
		piCounts[2] = 0;
		piCounts[3] = 0;

		for( int i1 = 0; i1 < vecFeatToIndexInfoCount[iFeat]; i1++ )
		{
			IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i1];
			int iFeat2 = II1.Index();
			// Cound valid matches
			int iImg2 = vecFeatToImgName[iFeat2];
			int iLabel2 = vecImgLabels[iImg2];

			float fThres = vecFeatAppearanceThresholds[iFeat];

			if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat] )
			{
				// Count labels within threshold
				if( iLabel2 >= 0 )
				{
					// Only count valid labels (0 or 1)
					piCounts[2+iLabel2]++;
				}

				float fRatio = (float)(piCounts[2+1]+iDirichletPrior)/(float)(piCounts[2+0]+iDirichletPrior);

				// Save ratio
				vecOrderClassDistRatios[i].push_back( log( fRatio ) );

				//fprintf( outfile, "%f\t%f\n", II1.DistSqr(), log( fRatio ) );
			}
			//else if( II1.DistSqrLessThanNeg() < 99999999 )
			//{
			//	// Count labels without threshold (geometrically consistent, yet bogus appearance feature
			//	vecOrderClassDistCounts[i][0+iLabel2]++;
			//	piCounts[0+iLabel2]++;
			//}
		}

		//fclose( outfile );
	}

	return 0;
}

int
ModelFeature3D::ClassifierDistClassify(
									   		vector<MODEL_FEATURE> &vecImgFeats,
		vector<int>       &vecModelMatches,
		double &dScore
		)
{
	//ClassifyGenerateTestingData( vecImgFeats, vecModelMatches, fScore, iFeatsUsed, iSamplesUsed, bReverse );

	float fLogRatioSum = 0;

	for( int iImgFeat = 0; iImgFeat < vecImgFeats.size(); iImgFeat++ )
	{
		int iModelFeatOrder = vecModelMatches[iImgFeat];
		if( iModelFeatOrder >= 0 )
		{
			int iModelFeat = vecOrderToFeat[iModelFeatOrder];
			float fDistSqrMatch = vecFeats[iModelFeat].MODEL_FEATURE_DIST( vecImgFeats[iImgFeat] );

			float fLogRatio = 0;

			for( int i1 = 0; i1 < vecFeatToIndexInfoCount[iModelFeat]; i1++ )
			{
				IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iModelFeat]+i1];
				if( II1.DistSqr() > vecFeatAppearanceThresholds[iModelFeat] )
				{
					// We should never get here
					assert( 0 );
				}
				if( II1.DistSqr() < fDistSqrMatch )
				{
					// Save most recent ratio below or equal to the match distance
					fLogRatio = vecOrderClassDistRatios[iModelFeatOrder][i1];
				}
				else
				{ 					// Were done with this feature
					break;
				}
			}

			// Add sum
			fLogRatioSum += fLogRatio;
		}
	}

	dScore = fLogRatioSum;

	if( fLogRatioSum > 0 )
	{
		return 1;
	}
	else
	{
		return -1;
	}

}

static void
outputDPFeatures(
				 char *pcFileName,
				 ModelFeature3D &mf,
				 DATA_PAIR *pdp,
				 int iCount
				 )
{
	FILE *outfile = fopen( pcFileName, "wt" );
	if( !outfile )
	{
		return;
	}
	for( int i = 0; i < iCount; i++ )
	{
		int iFeat = pdp[i].iIndex;
		MODEL_FEATURE &feat = mf.vecFeats[iFeat];
		fprintf( outfile, "%f\t%f\t%f\t%f\n", feat.x, feat.y, feat.z, feat.scale );
	}
	fclose( outfile );
}


static bool pairIntFloatLeastToGreatest( const pair<int,float> &ii1, const pair<int,float> &ii2 )
{
	return ii1.second < ii2.second;
}

int
ModelFeature3D::SortOrderSignificance(
									  )
{
	vector< pair<int,float> > vecifPair;
	vecifPair.resize( vecOrderToFeat.size() );

	FILE *outfile = fopen( "__sig.txt", "wt" );
	FishersExactTest fet;
	fet.Init( vecImgLabels.size() );
	int piLabelCounts[2];
	memset( piLabelCounts, 0, sizeof(piLabelCounts) );
	for( int i = 0; i < vecImgLabels.size(); i++ )
	{
		if( vecImgLabels[i] == 0 )
		{
			piLabelCounts[0]++;
		}
		else if( vecImgLabels[i] == 1 )
		{
			piLabelCounts[1]++;
		}
	}
	for( int i = 0; i < vecOrderToFeat.size(); i++ )
	{
		int iFeat = vecOrderToFeat[i];
		MODEL_FEATURE &feat = vecFeats[iFeat];

		float fSig = fet.OneSidedPValue(
			m_bayesClass.m_piCounts[0][i], m_bayesClass.m_piCounts[1][i],
			piLabelCounts[0] - m_bayesClass.m_piCounts[0][i], piLabelCounts[1] - m_bayesClass.m_piCounts[1][i] );

		fprintf( outfile, "%d\t%f\n", iFeat, fSig );

		vecifPair[i].first = iFeat;
		vecifPair[i].second = fSig;
	}
	fclose( outfile );

	// Sort features according to significance
	sort( vecifPair.begin(), vecifPair.end(), pairIntFloatLeastToGreatest );

	// Reset order
	for( int i = 0; i < vecOrderToFeat.size(); i++ )
	{
		vecOrderToFeat[i] = vecifPair[i].first;
	}

	return 0;
}


int
ModelFeature3D::OutputClassifierInfo(
		char *pcFileName
	)
{
	FishersExactTest fet;
	fet.Init( vecImgLabels.size() );
	int piLabelCounts[2];
	memset( piLabelCounts, 0, sizeof(piLabelCounts) );
	for( int i = 0; i < vecImgLabels.size(); i++ )
	{
		if( vecImgLabels[i] == 0 )
		{
			piLabelCounts[0]++;
		}
		else if( vecImgLabels[i] == 1 )
		{
			piLabelCounts[1]++;
		}
	}

	FILE *outfile = fopen( pcFileName, "wt" );
	fprintf( outfile, "index\tzeros\tones\tx\ty\tz\tscale\tp-value\n", m_bayesClass.m_iOnes, m_bayesClass.m_iZeros );
	for( int i = 0; i < vecOrderToFeat.size(); i++ )
	{
		int iFeat = vecOrderToFeat[i];
		MODEL_FEATURE &feat = vecFeats[iFeat];

		float fSig = fet.OneSidedPValue(
			m_bayesClass.m_piCounts[0][i], m_bayesClass.m_piCounts[1][i],
			piLabelCounts[0] - m_bayesClass.m_piCounts[0][i], piLabelCounts[1] - m_bayesClass.m_piCounts[1][i] );

		fprintf( outfile, "%d\t%d\t%d\t%f\t%f\t%f\t%f\t%f\n", iFeat, m_bayesClass.m_piCounts[0][i], m_bayesClass.m_piCounts[1][i],
			
			feat.x, feat.y, feat.z, feat.scale,

			fSig
			
			);
	}
	fclose( outfile );


	//
	// This code outputs features for display with openGL
	//
	DATA_PAIR *pdpNC = new DATA_PAIR[vecOrderToFeat.size()];
	DATA_PAIR *pdpAD = new DATA_PAIR[vecOrderToFeat.size()];
	int iCountNC = 0;
	int iCountAD = 0;
	for( int i = 0; i < vecOrderToFeat.size(); i++ )
	{
		int iFeat = vecOrderToFeat[i];
		float fSig = fet.OneSidedPValue(
			m_bayesClass.m_piCounts[0][i], m_bayesClass.m_piCounts[1][i],
			piLabelCounts[0] - m_bayesClass.m_piCounts[0][i], piLabelCounts[1] - m_bayesClass.m_piCounts[1][i] );

		if( m_bayesClass.m_piCounts[1][i] - m_bayesClass.m_piCounts[0][i] > 0 )
		{
			// AD			
			pdpAD[iCountAD].iIndex = iFeat;
			pdpAD[iCountAD].iValue = fSig*10000;
			iCountAD++;
		}
		else
		{
			// NC
			pdpNC[iCountNC].iIndex = iFeat;
			pdpNC[iCountNC].iValue = fSig*10000;
			iCountNC++;
		}
		
	}
	dpSortAscending( pdpNC, iCountNC );
	dpSortAscending( pdpAD, iCountAD );

	outputDPFeatures( "feat_list_NC.txt", *this, pdpNC, iCountNC );
	outputDPFeatures( "feat_list_AD.txt", *this, pdpAD, iCountAD );

	delete [] pdpNC;
	delete [] pdpAD;

	return 1;
}


int
ModelFeature3D::OutputFeatureInVolume(
		int iFeat,
		char *pcFileName
	)
{
	MODEL_FEATURE &feat = vecFeats[iFeat];
	int iImg = vecFeatToImgName[iFeat];
	char *pcImgName = vecImgName[ iImg ];

	FEATUREIO fio;
	if( fioRead( fio, pcImgName ) == 0 )
	{
		return 0;
	}

	feat.OutputVisualFeatureInVolume( fio, pcFileName, 1 );

	fioDelete( fio );
	return 1;
}

int
ModelFeature3D::OutputFeatureStatsVsDist(
		int iFeat,
		char *pcFileName,
		int iDirichletPrior
		)
{
	int piCounts[4];

	FILE *outfile = fopen( pcFileName, "wt" );
	if( !outfile )
	{
		return -1;
	}
	MODEL_FEATURE &feat = vecFeats[iFeat];

	memset( piCounts, 0, sizeof(piCounts) );
	piCounts[0] = 0;
	piCounts[1] = 0;
	piCounts[2] = 0;
	piCounts[3] = 0;

	for( int i1 = 0; i1 < vecFeatToIndexInfoCount[iFeat]; i1++ )
	{
		IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i1];
		int iFeat2 = II1.Index();
		// Cound valid matches
		int iImg2 = vecFeatToImgName[iFeat2];
		int iLabel2 = vecImgLabels[iImg2];

		float fThres = vecFeatAppearanceThresholds[iFeat];

		//if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat] )
		if( II1.DistSqrLessThanNeg() < 99999999 )
		{
			// Count labels within threshold
			if( iLabel2 >= 0 )
			{
				// Only count valid labels (0 or 1)
				piCounts[2+iLabel2]++;
			}

			float fRatio = (float)(piCounts[2+1]+iDirichletPrior)/(float)(piCounts[2+0]+iDirichletPrior);

			fprintf( outfile, "%f\t%d\t%d\t%d\t%f\t%d\t%d\n", II1.DistSqr(), II1.DistSqrLessThanNeg(), piCounts[2+1], piCounts[2+0], log( fRatio ), (II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat]), iImg2 );
		}
		//else if( II1.DistSqrLessThanNeg() < 99999999 )
		//{
		//	// Count labels without threshold (geometrically consistent, yet bogus appearance feature
		//	vecOrderClassDistCounts[i][0+iLabel2]++;
		//	piCounts[0+iLabel2]++;
		//}
	}

	fclose( outfile );
	return 0;
}

int
ModelFeature3D::IdentifyOverlappingFeatures(
		int iFeat1,
		int iFeat2,
		vector<int> *pvecOverlapFeatIndices
	)
{
	int iOverlapCount = 0;
	for( int i1 = 0; i1 < vecFeatToIndexInfoCount[iFeat1]; i1++ )
	{
		IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat1]+i1];
		if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat1] )
		{
			for( int i2 = 0; i2 < vecFeatToIndexInfoCount[iFeat2]; i2++ )
			{
				IndexInfo &II2 = vecIndexInfo[vecFeatToIndexInfo[iFeat2]+i2];
				if( II2.DistSqr() <= vecFeatAppearanceThresholds[iFeat2] )
				{
					if( II1.Index() == II2.Index() )
					{
						iOverlapCount++;
						if( pvecOverlapFeatIndices )
						{
							pvecOverlapFeatIndices->push_back( II1.Index() );
						}
					}
				}
				else
				{
					break;
				}
			}
		}
	
		else
		{
			break;
		}
	}
	return iOverlapCount;
}

int
ModelFeature3D::IdentifyOverlappingFeatures(
	)
{
	int  iOverlapCountTotal = 0;
	vector< vector<int> > vecvecintOverlapping;
	vecvecintOverlapping.resize( vecOrderToFeat.size() );
	for( int i = 0; i < vecOrderToFeat.size(); i++ )
	{
		int iFeat1 = vecOrderToFeat[i];
		for( int j = i+1; j < vecOrderToFeat.size(); j++ )
		{
			int iFeat2 = vecOrderToFeat[j];
			int iOverlapCount = IdentifyOverlappingFeatures( iFeat1, iFeat2 );
			if( iOverlapCount > 0 )
			{
				iOverlapCountTotal++;
				// Save overlappping feature indices
				vecvecintOverlapping[i].push_back( iFeat2 );
				vecvecintOverlapping[j].push_back( iFeat1 );
			}
		}
	}

	FILE *outfile = fopen( "overlapping_features.txt", "wt" );
	for( int i = 0; i < vecvecintOverlapping.size(); i++ )
	{
		fprintf( outfile, "%d:\t", vecOrderToFeat[i] );
		for( int j = 0; j < vecvecintOverlapping[i].size(); j++ )
		{
			fprintf( outfile, "%d\t", vecvecintOverlapping[i][j] );
		}
		fprintf( outfile, "\n" );
	}
	fclose( outfile );

	return iOverlapCountTotal;
}

int
ModelFeature3D::IdentifyFeature1ContainsFeature2(
		int iFeat1,
		int iFeat2
	)
{
	for( int i1 = 0; i1 < vecFeatToIndexInfoCount[iFeat1]; i1++ )
	{
		IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat1]+i1];
		if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat1] )
		{
			if( II1.Index() == iFeat2 )
			{
				return 1;
			}
		}
		else
		{
			break;
		}
	}
	return 0;
}

int
ModelFeature3D::IdentifyGeometricallyConsistentFeatures(
	)
{
	int  iContainsCount = 0;
	vector< vector<int> > vecvecintOverlapping;
	vecvecintOverlapping.resize( vecOrderToFeat.size() );
	for( int i = 0; i < vecOrderToFeat.size(); i++ )
	{
		int iFeat1 = vecOrderToFeat[i];
		MODEL_FEATURE &feat1 = vecFeats[iFeat1];
		for( int j = 0; j < vecOrderToFeat.size(); j++ )
		{
			if( i == j )
			{
				continue;
			}

			int iFeat2 = vecOrderToFeat[j];
			MODEL_FEATURE &feat2 = vecFeats[iFeat2];

			int bFeat1Feat2Compatible = compatible_features( feat1, feat2 );
			if( bFeat1Feat2Compatible )
			{
				if( vecvecintOverlapping[j].size() == 0 )
				{
					iContainsCount++;
					//char pcFileName[400];
					//int iCount1 = FeaturesWithinThreshold( iFeat1 );
					//int iCount2 = FeaturesWithinThreshold( iFeat2 );
					//int iCommon = CommonFeatureSample( iFeat1, iFeat2 );
					//sprintf( pcFileName, "feat%5.5d_count%3.3d", iFeat1, iCount1 ); OutputFeatureInVolume( iFeat1, pcFileName );
					//sprintf( pcFileName, "feat%5.5d_count%3.3d", iFeat2, iCount2 ); OutputFeatureInVolume( iFeat2, pcFileName );
				}

				char pcFileName[300];
				//sprintf( pcFileName, "feat%5.5d_%5.5d_1", iFeat1, iFeat2 );
				//OutputFeatureInVolume( iFeat1, pcFileName );
				//sprintf( pcFileName, "feat%5.5d_%5.5d_2", iFeat1, iFeat2 );
				//OutputFeatureInVolume( iFeat2, pcFileName );
				//int iCount1 = FeaturesWithinThreshold( iFeat1 );
				//int iCount2 = FeaturesWithinThreshold( iFeat2 );

				// For iFeat2, save list of features which contain it
				vecvecintOverlapping[j].push_back( iFeat1 );
			}
		}
	}

	FILE *outfile = fopen( "contains_.txt", "wt" );
	for( int i = 0; i < vecvecintOverlapping.size(); i++ )
	{
		int iContained = vecvecintOverlapping[i].size() > 0 ? 1 : 0;
		fprintf( outfile, "%d\t%d\t", iContained, vecvecintOverlapping[i].size() );
		for( int j =0; j < vecvecintOverlapping[i].size(); j++ )
		{
			fprintf( outfile, "%d\t", i, vecvecintOverlapping[i][j] );
		}
		fprintf( outfile, "\n" );
	}
	fclose( outfile );

	for( int i = vecvecintOverlapping.size()-1; i >= 0; i-- )
	{
		if( vecvecintOverlapping[i].size() > 0 )
		{
			// Erase invalid feature
			int iFeat = vecOrderToFeat[i];
			vecFeatValid[iFeat] = 0;
			vecOrderToFeat.erase( vecOrderToFeat.begin() + i );
		}
	}

	return iContainsCount;
}


int
ModelFeature3D::IdentifyFeature1ContainsFeature2(
	)
{
	int  iContainsCount = 0;
	vector< vector<int> > vecvecintOverlapping;
	vecvecintOverlapping.resize( vecOrderToFeat.size() );
	for( int i = 0; i < vecOrderToFeat.size(); i++ )
	{
		int iFeat1 = vecOrderToFeat[i];
		for( int j = 0; j < vecOrderToFeat.size(); j++ )
		{
			if( i == j )
			{
				continue;
			}

			int iFeat2 = vecOrderToFeat[j];
			int bFeat1ContainsFeat2 = IdentifyFeature1ContainsFeature2( iFeat1, iFeat2 );
			if( bFeat1ContainsFeat2 )
			{
				if( vecvecintOverlapping[j].size() == 0 )
				{
					iContainsCount++;
					//char pcFileName[400];
					//int iCount1 = FeaturesWithinThreshold( iFeat1 );
					//int iCount2 = FeaturesWithinThreshold( iFeat2 );
					//int iCommon = CommonFeatureSample( iFeat1, iFeat2 );
					//sprintf( pcFileName, "feat%5.5d_count%3.3d", iFeat1, iCount1 ); OutputFeatureInVolume( iFeat1, pcFileName );
					//sprintf( pcFileName, "feat%5.5d_count%3.3d", iFeat2, iCount2 ); OutputFeatureInVolume( iFeat2, pcFileName );
				}

				// For iFeat2, save list of features which contain it
				vecvecintOverlapping[j].push_back( iFeat1 );
			}
		}
	}

	FILE *outfile = fopen( "contains_.txt", "wt" );
	for( int i = 0; i < vecvecintOverlapping.size(); i++ )
	{
		int iContained = vecvecintOverlapping[i].size() > 0 ? 1 : 0;
		fprintf( outfile, "%d\t%d\t", iContained, vecvecintOverlapping[i].size() );
		for( int j =0; j < vecvecintOverlapping[i].size(); j++ )
		{
			fprintf( outfile, "%d\t", i, vecvecintOverlapping[i][j] );
		}
		fprintf( outfile, "\n" );
	}
	fclose( outfile );

	for( int i = vecvecintOverlapping.size()-1; i >= 0; i-- )
	{
		if( vecvecintOverlapping[i].size() > 0 )
		{
			// Erase invalid feature
			int iFeat = vecOrderToFeat[i];
			vecFeatValid[iFeat] = 0;
			vecOrderToFeat.erase( vecOrderToFeat.begin() + i );
		}
	}

	return iContainsCount;
}

int
ModelFeature3D::CommonFeatureSample(
		int iFeat1,
		int iFeat2
	)
{
	int iCommonSamples = 0;
	for( int ii1 = vecFeatToIndexInfo[iFeat1]; ii1 < vecFeatToIndexInfo[iFeat1] + vecFeatToIndexInfoCount[iFeat1]; ii1++ )
	{
		IndexInfo &II1 = vecIndexInfo[ii1];
		if( II1.DistSqr() > vecFeatAppearanceThresholds[iFeat1] )
		{
			break;
		}

		for( int ii2 = vecFeatToIndexInfo[iFeat2]; ii2 < vecFeatToIndexInfo[iFeat2] + vecFeatToIndexInfoCount[iFeat2]; ii2++ )
		{
			IndexInfo &II2 = vecIndexInfo[ii2];
			if( II2.DistSqr() > vecFeatAppearanceThresholds[iFeat2] )
			{
				break;
			}

			if( II1.Index() == II2.Index() )
			{
				iCommonSamples++;
				//return II1.Index();
			}
		}
	}
	return iCommonSamples;
}

int
ModelFeature3D::InspectModelFeatureOverlap(
	)
{
	int *piImages = new int[1000];

	vector<int> veciCounts;
	
	veciCounts.resize( vecOrderToFeat.size() );
	for( int i = 0; i < vecOrderToFeat.size(); i++ )
	{
		veciCounts[i] = 0;
	}

	for( int i = 0; i < vecOrderToFeat.size(); i++ )
	{
		int iFeat1 = vecOrderToFeat[i];
		MODEL_FEATURE &feat1 = vecFeats[iFeat1];

		// Loop over condensed array of features
		for( int j = i+1; j < vecOrderToFeat.size(); j++ ) // Forward
		{
			int iFeat2 = vecOrderToFeat[j];
			MODEL_FEATURE &feat2 = vecFeats[iFeat2];

			int iCommonSamples = CommonFeatureSample( iFeat1, iFeat2 );
			if( iCommonSamples > 0 )
			{
				veciCounts[i] += iCommonSamples;
				veciCounts[j] += iCommonSamples;

				int iCount1 = FeaturesWithinThreshold( iFeat1, piImages );
				int iCount2 = FeaturesWithinThreshold( iFeat2, piImages );

				printf( "F1: %d, F2: %d, Shared: %d\n", iCount1, iCount2, iCommonSamples );
			}
		}
	}

	FILE *outfile = fopen( "feat_overlap.txt", "wt" );
	for( int i = 0; i < vecOrderToFeat.size(); i++ )
	{
		fprintf( outfile, "%d\t%d\n", i, veciCounts[i] );
	}
	fclose( outfile );

	delete piImages;

	return 1;
}

int
ModelFeature3D::ClassifyGenerateTestingData(
		vector<MODEL_FEATURE> &vecImgFeats,
		vector<int>       &vecModelMatches,
		float			   &fScore,
		int					&iFeatsUsed,
		int					&iSamplesUsed,
		int					bReverse
		)
{
	if( m_prob_test)
	{
		delete [] m_prob_test;
		m_prob_test =0;
	}
	if(  m_prob_test_weights )
	{
		delete [] m_prob_test_weights;
		m_prob_test_weights=0;
	}
	m_prob_test = new svm_node[(vecOrderToFeat.size()+1)];
	m_prob_test_weights = new svm_node[(vecOrderToFeat.size()+1)];
	for( int i = 0; i < vecOrderToFeat.size(); i++ )
	{
		m_prob_test[i].index = i;
		m_prob_test[i].value = 0;
		m_prob_test_weights[i].index = 1;
		m_prob_test_weights[i].value = 1;
	}
	// Set last node to -1, as per SVMLIB format
	m_prob_test[vecOrderToFeat.size()].index = -1;
	m_prob_test[vecOrderToFeat.size()].value = -1;
	m_prob_test_weights[vecOrderToFeat.size()].index = -1;
	m_prob_test_weights[vecOrderToFeat.size()].value = -1;

	vecModelMatches.clear();
	vecModelMatches.resize( vecImgFeats.size() );

	int iSamples = 0;
	int iFeatsFound = 0;

	FILE *outfileF = fopen( "multiple_matches_fwd.txt", "wt" );
	FILE *outfileB = fopen( "multiple_matches_bwd.txt", "wt" );

	vector<int> veciModelFeatureMatches;
	veciModelFeatureMatches.resize( vecOrderToFeat.size(), 0 );

	// This is a big hak just for output
	int iOut = 0;
	int iImgFeatStartIndex = 0;
	for( iImgFeatStartIndex = 0; iImgFeatStartIndex < vecFeats.size(); iImgFeatStartIndex++ )
	{
		if( vecFeatToImgName[iImgFeatStartIndex] == m_prob_test_image )
		{
			break;
		}
	}


	for( int iImgFeat = 0; iImgFeat < vecImgFeats.size(); iImgFeat++ )
	{
		MODEL_FEATURE &featInput = vecImgFeats[iImgFeat];

		// Flag as unfound...
		vecModelMatches[iImgFeat] = -1;

		int iImageFeatureToModelFeatureMatch = 0;

		// Save model matches for inspection
		vector<int> vecMM;

		// Try keeping the nearest feature instead of the most common...
		float fNearestModelFeatDist;
		int   iNearestModelFeatIndex = -1;
		int	  iFirstModelFeatIndex = -1;

		// Loop over condensed array of features
		for( int i = 0; i < vecOrderToFeat.size(); i++ ) // Forward
		//for( int i = vecOrderToFeat.size()-1; i >= 0; i-- ) // Backward
		{
			int iFeat = vecOrderToFeat[i];
			if( bReverse )
			{
				iFeat = vecOrderToFeat[vecOrderToFeat.size()-i-1];
				assert( iFeat >= 0 );
				assert( iFeat < vecOrderToFeat.size() );
			}
			MODEL_FEATURE &featModel = vecFeats[iFeat];
			if( compatible_features( featModel, featInput )
				&&
				featModel.MODEL_FEATURE_DIST( featInput ) <= vecFeatAppearanceThresholds[iFeat]
				)
			{
				if( iOut )
				{
					char pcFileName[400];
					sprintf( pcFileName, "out\\feat_%4.4d_img", iImgFeat );
					OutputFeatureInVolume( iImgFeatStartIndex+iImgFeat, pcFileName );

					sprintf( pcFileName, "out\\feat_%4.4d_mod_order_%4.4d_zer%2.2d_one%2.2d", iImgFeat, i, m_bayesClass.m_piCounts[0][i], m_bayesClass.m_piCounts[1][i]  );
					OutputFeatureInVolume( iFeat, pcFileName );
				}

				fprintf( outfileF, "%d\t%d\t%d\n", iImgFeat, i, iFeat );
				iImageFeatureToModelFeatureMatch++; // Same image feature, count model feature matches
				veciModelFeatureMatches[i]++; // Same model feature, count image feature matches.
				vecMM.push_back( iFeat );

				// Save nearest match
				if( iNearestModelFeatIndex == -1 )
				{
					fNearestModelFeatDist = featModel.MODEL_FEATURE_DIST( featInput );
					iNearestModelFeatIndex = i;
					iFirstModelFeatIndex = i;
				}
				else if( featModel.MODEL_FEATURE_DIST( featInput ) < fNearestModelFeatDist )
				{
					fNearestModelFeatDist = featModel.MODEL_FEATURE_DIST( featInput );
					iNearestModelFeatIndex = i;
				}

				//if( vecModelMatches[iImgFeat] != -1 )
				//{
				//	// This image feature has already been matched, error
				//	//vecModelMatches[iImgFeat] = -2;
				//	//m_prob_test[i].value = 0;
				//}
				//else
				//{
				//	// Set to model feature.
				//	vecModelMatches[iImgFeat] = i;
				//	m_prob_test[i].value = 1;
				//	assert( m_prob_test[i].index == i );

				//	// Set weight - linear function of distance
				//	// This didn't have a big impact, not currently used
				//	m_prob_test_weights[i].value = 1 - (featModel.MODEL_FEATURE_DIST( featInput ) / vecFeatAppearanceThresholds[iFeat]);

				//	iFeatsFound++;
				//}
			}
		}

		if( iFirstModelFeatIndex > -1 )
		{
			int iDecisionIndex = iFirstModelFeatIndex; // When only ones are used in Bayesian classification, this works best
			//int iDecisionIndex = iNearestModelFeatIndex; //

			// Set to model feature.
			vecModelMatches[iImgFeat] = iDecisionIndex;
			m_prob_test[iDecisionIndex].value = 1;
			assert( m_prob_test[iDecisionIndex].index == iDecisionIndex );

			// Count samples
			int iCount = FeaturesWithinThreshold( vecOrderToFeat[iDecisionIndex] );

			iFeatsFound++;
			iSamples += iCount;
		}

		int iCount1;
		int iCount2;
		int iCommonSamples;

		if( iImageFeatureToModelFeatureMatch > 1 )
		{
			printf( "Same image feature, multiple model features, Img: %d\t, Matches: %d\n", iImgFeat, iImageFeatureToModelFeatureMatch );

			// iCount1 = FeaturesWithinThreshold( vecMM[0] );
			// iCount2 = FeaturesWithinThreshold( vecMM[1] );
			// iCommonSamples = CommonFeatureSample( vecMM[0], vecMM[1] );
			//for( int iI = 0; iI < vecMM.size(); iI++ )
			//{
			//	char pcFileName[400];
			//	sprintf( pcFileName, "feat_number%2.2d_index%4.4d", iI, vecMM[iI] );
			//	OutputFeatureInVolume( vecMM[iI],  pcFileName );
			// VisualizeVarriance( vecMM[iI] );
			//}
		}
	}

	fclose( outfileF );
	fclose( outfileB );

	FILE *outfile = fopen( "modelfeaturematches.txt", "wt" );
	for( int i = 0; i <veciModelFeatureMatches.size(); i++ )
	{
		fprintf( outfile, "%d\t%f\n", veciModelFeatureMatches[i], m_prob_test[i].value );
		//veciModelFeatureMatches[i] = 0;
	}
	fclose( outfile );

	printf( "Feats found: %d, Samples: %d\n", iFeatsFound, iSamples );

	iFeatsUsed = iFeatsFound;
	iSamplesUsed = iSamples;

	return 0;
}

int
ModelFeature3D::BayesianNearnessBuild(
	)
{
	// Count the number of model feature instances for each image
	vecModelFeatsPerImage.resize( vecImgLabels.size() );
	for( int i = 0; i < vecModelFeatsPerImage.size(); i++ )
	{
		vecModelFeatsPerImage[i] = 0;
	}
	for( int i=0; i < vecOrderToFeat.size(); i++ )
	{
		int iFeat = vecOrderToFeat[i];
		for( int i1 = 0; i1 < vecFeatToIndexInfoCount[iFeat]; i1++ )
		{
			IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i1];

			float fThres = vecFeatAppearanceThresholds[iFeat];

			if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat] )
			{
				int iInfo = II1.Info();
				int iFeat2 = II1.Index();
				int iImg2 = vecFeatToImgName[iFeat2];
				int iLabel2 = vecImgLabels[iImg2];

				if( iLabel2 != -1 )
				{
					vecModelFeatsPerImage[iImg2]++;
				}
			}
			else
			{
				break;
			}
		}
	}

	char pcFileName[400];
	sprintf( pcFileName, "iter%3.3d_model_features_per_image.txt", g_iIteration );
	outputIntVector( vecModelFeatsPerImage, pcFileName );

	return -1;
}

int
ModelFeature3D::BayesianNearnessClassify(
		vector<MODEL_FEATURE> &vecImgFeats,
		vector<int>       &vecModelMatches,
		double			&dScore
	)
{
	vector<float> vecImgVotes; // Vector of vote counts for each image
	vecImgVotes.resize( vecImgName.size(), 0.0f );

	vector<float> vecClassVotes; // Vector of vote counts for each class/label
	vecClassVotes.resize( 2, 0 );

	int iMatches = 0;

	vector< IndexInfo > veciiMatches;

	for( int iImgFeat = 0; iImgFeat < vecImgFeats.size(); iImgFeat++ )
	{
		MODEL_FEATURE &featImg = vecImgFeats[iImgFeat];
		veciiMatches.clear();
		if( vecModelMatches[iImgFeat] >= 0 )
		{
			// This image feature was matched to some model feature, find the closest one

			float fNearestDist = 9.99e30;
			int iFeatNearest = -1;

			for( int i = 0; i < vecOrderToFeat.size(); i++ )
			{
				int iFeat = vecOrderToFeat[i];
				MODEL_FEATURE &featModel = vecFeats[iFeat];
				// If geometrically compatible, then compare appearance
				if( compatible_features( featModel, featImg ) )
				{
					// Go through all samples for this feature
					for( int i1 = 0; i1 < vecFeatToIndexInfoCount[iFeat]; i1++ )
					{
						IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i1];

						if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat] )
						{
							int iInfo = II1.Info();
							int iFeat2 = II1.Index();
							int iImg2 = vecFeatToImgName[iFeat2];
							int iLabel2 = vecImgLabels[iImg2];

							// Only test distance if label is valid
							if( iLabel2 != -1 )
							{
								MODEL_FEATURE &featModel2 = vecFeats[iFeat2];

								float fDist = featModel2.MODEL_FEATURE_DIST( featImg );
								if( iFeatNearest == -1 || fDist < fNearestDist )
								{
									fNearestDist = fDist;
									iFeatNearest = iFeat2;
								}

								// Save match index/distance for K-nearest neighbors
								IndexInfo ii;
								ii.SetIndex( iFeat2 );
								ii.SetDistSqr( fDist );
								veciiMatches.push_back( ii );
							}
							else
							{
								continue;
							}
						}
						else
						{
							break;
						}
					}
				}
			}

			// Add votes associated with K nearest neighbors
			sort( veciiMatches.begin(), veciiMatches.end(), indexInfoDistSqrLeastToGreatest );
			int piLocalLabelCounts[2];
			memset( piLocalLabelCounts, 0, sizeof(piLocalLabelCounts) );
			for( int i = 0; i < 7 && i < veciiMatches.size(); i++ )
			{
				int iImgNearest = vecFeatToImgName[ veciiMatches[i].Index() ];
				int iLabelNearest = vecImgLabels[iImgNearest];

				piLocalLabelCounts[iLabelNearest]++;

				vecImgVotes[iImgNearest]++;
				vecClassVotes[iLabelNearest]++;
			}

			////
			//if( piLocalLabelCounts[1] > piLocalLabelCounts[0] )
			//{
			//	vecImgVotes[1]++;
			//	vecClassVotes[1]++;
			//}
			//else
			//{
			//	vecImgVotes[0]++;
			//	vecClassVotes[0]++;
			//}

			// Update votes for this feature
			//assert( iFeatNearest != -1 );
			//int iImgNearest = vecFeatToImgName[iFeatNearest];
			//int iLabelNearest = vecImgLabels[iImgNearest];

			//vecImgVotes[iImgNearest]++;
			//vecClassVotes[iLabelNearest]++;
			iMatches++;
		}
	}

	// First try simple nearest neighbor voting
	dScore = vecClassVotes[1] - vecClassVotes[0];

	//float fMaxVotes = -1;
	//int iMaxVoteImg = -1;
	//for( int i = 0; i < vecImgVotes.size(); i++ )
	//{
	//	float fVotesImg = (float)vecImgVotes[i]/(float)vecModelFeatsPerImage[i];
	//	if( iMaxVoteImg == -1 || fVotesImg > fMaxVotes )
	//	{
	//		iMaxVoteImg = i;
	//		fMaxVotes = fVotesImg;
	//	}
	//}
	//if( vecImgLabels[iMaxVoteImg] == 1 )
	//{
	//	dScore = fMaxVotes;
	//}
	//else if( vecImgLabels[iMaxVoteImg] == 0 )
	//{
	//	dScore = -fMaxVotes;
	//}
	//else
	//{
	//	printf( "Invalid Votes!!!\n" );
	//}

	return 0;
}

int
ModelFeature3D::BayesianOverlapBuild(
									 int iDirichletPrior
	)
{
	// Allocate volumes to the size of OASIS data
	if( !m_fioOnes.pfVectors )
	{
		m_fioOnes.x = 176;
		m_fioOnes.y = 208;
		m_fioOnes.z = 176;
		m_fioOnes.t = 1;
		m_fioOnes.iFeaturesPerVector = 1;
		fioAllocate( m_fioOnes );
	}
	if( !m_fioZeros.pfVectors )
	{
		m_fioZeros.x = 176;
		m_fioZeros.y = 208;
		m_fioZeros.z = 176;
		m_fioZeros.t = 1;
		m_fioZeros.iFeaturesPerVector = 1;
		fioAllocate( m_fioZeros );
	}

	// Must build classifier to get counts
	BuildClassifier( ModelFeature3D::ctBayesian, iDirichletPrior );

	return 0;
}

int
ModelFeature3D::BayesianOverlapClassify(
		vector<MODEL_FEATURE> &vecImgFeats,
		vector<int>       &vecModelMatches,
		double			&dScore
	)
{
	// Set volumes to zero
	fioSet( m_fioOnes, 0 );
	fioSet( m_fioZeros, 0 );

	for( int iImgFeat = 0; iImgFeat < vecImgFeats.size(); iImgFeat++ )
	{
		MODEL_FEATURE &featImg = vecImgFeats[iImgFeat];
		if( vecModelMatches[iImgFeat] >= 0 )
		{
			// Retreive model feature 
			int iFeat = vecOrderToFeat[ vecModelMatches[iImgFeat] ];
			MODEL_FEATURE &featMod = vecFeats[iFeat];

			// Get likelihood ratio, dirichlet regularize by 3
			float fProbOne = m_bayesClass.m_piCounts[1][vecModelMatches[iImgFeat]] + 3;
			float fProbZer = m_bayesClass.m_piCounts[0][vecModelMatches[iImgFeat]] + 3;

			//float *pfProbs = m_bayesClass.m_pbfdDistributions[ vecModelMatches[iImgFeat] ];
			//float fProbZer = pfProbs[2];
			//float fProbOne = pfProbs[3];

			fProbZer = log( fProbZer );
			fProbOne = log( fProbOne );

			// Divide by volume
			float fRadius = 2*featMod.scale;
			float fVolume = 4.0f*PI*fRadius*fRadius*fRadius/3.0f;
			fProbOne /= fVolume;
			fProbZer /= fVolume;

			// Paint now into FIO
			int iStart_z = featMod.z-fRadius >= 0 ? featMod.z-fRadius : 0;
			for( int z = iStart_z; z < featMod.z+fRadius && z < m_fioOnes.z; z++ )
			{
				int iStart_y = featMod.y-fRadius >= 0 ? featMod.y-fRadius : 0;
				for( int y = iStart_y; y < featMod.y+fRadius && y < m_fioOnes.y; y++ )
				{
					int iStart_x = featMod.x-fRadius >= 0 ? featMod.x-fRadius : 0;
					for( int x = iStart_x; x < featMod.x+fRadius && x < m_fioOnes.x; x++ )
					{
						// Check to see if we fall into ratius
						float dx = x-featMod.x;
						float dy = y-featMod.y;
						float dz = z-featMod.z;
						if( dx*dx + dy*dy + dz*dz <= fRadius*fRadius )
						{
							// Save most highest log count for each class
							float *pfVector;

							pfVector = fioGetVector( m_fioOnes, x, y, z );
							if( pfVector[0] < fProbOne )
							{
								pfVector[0] = fProbOne;
							}
							
							pfVector = fioGetVector( m_fioZeros, x, y, z );
							if( pfVector[0] < fProbZer )
							{
								pfVector[0] = fProbZer;
							}
						}

					}
				}
			}

		}
	}

	// Now sum out volume
	float fSumZer = 0;
	float fSumOne = 0;

	for( int z = 0; z < m_fioOnes.z; z++ )
	{
		for( int y = 0; y < m_fioOnes.y; y++ )
		{
			for( int x = 0; x < m_fioOnes.x; x++ )
			{
				float *pfVector;
				pfVector = fioGetVector( m_fioOnes, x, y, z );
				fSumOne += pfVector[0];
				pfVector = fioGetVector( m_fioZeros, x, y, z );
				fSumZer += pfVector[0];
			}
		}
	}

	// Calculate score
	dScore = fSumOne - fSumZer;

	return 0;
}

int
ModelFeature3D::Classify(
	    int iClassifierType,
		vector<MODEL_FEATURE> &vecImgFeats,
		vector<int>       &vecModelMatches,
		float			   &fScore,
		int					&iFeatsUsed,
		int					&iSamplesUsed,
		int					bReverse
	)
{
	ClassifyGenerateTestingData( vecImgFeats, vecModelMatches, fScore, iFeatsUsed, iSamplesUsed, bReverse );

	//// Append testing data
	//char pcFileName[400];
	//sprintf( pcFileName, "decision_tree%3.3d.data", g_iIteration );
	//FILE *outfile = fopen( pcFileName, "a+" );	
	//int iResult = (int)m_prob_test_label == 1 ? 1 : 0;
	//fprintf( outfile, "%d", iResult );
	//int iNode = 0;
	//while( m_prob_test[iNode].index != -1 )
	//{
	//	fprintf( outfile, ",%d", (int)m_prob_test[iNode].value ); 
	//	iNode++;
	//}
	//fprintf( outfile, "\n" ); 
	//fclose( outfile );

	double dSum;
	float fLogProbZero;
	float fLogProbOne;
	int iClass;

	double *pdCoefficients;
	int i;

	switch( iClassifierType )
	{
	case ctBayesianBoosting: // do the same as bayesian...
	case ctBayesian: // Bayesian
		iClass = m_bayesClass.Classify( m_prob_test, dSum, fLogProbZero, fLogProbOne);
		// iClass = m_bayesClass.ClassifyWeights( m_prob_test, m_prob_test_weights, dSum, fLogProbZero, fLogProbOne);
		//
		// This is crap, don't use
		// iClass = m_bayesClass.ClassifyRawSamples( m_prob_test, dSum, fLogProbZero, fLogProbOne);
		//
		break;

	case ctSVM_Linear:
	case ctSVM_RBF:
		iClass = (int)svm_predict( m_pmodel, m_prob_test, &dSum );
		break;

	case ctBayesianDist:
		iClass = ClassifierDistClassify( vecImgFeats, vecModelMatches, dSum );
		break;

	case ctNearestNeighbor:
		iClass = BayesianNearnessClassify( vecImgFeats, vecModelMatches, dSum );
		break;

	case ctBayesianOverlap:
		iClass = BayesianOverlapClassify( vecImgFeats, vecModelMatches, dSum );
		break;

	//case ctBayesianBoosting:
	//	// Set coefficients into array, call boosting classifier...if this works I'll eat my shorts
	//	pdCoefficients = new double[m_vecBoostingCoefficients.size()];
	//	for( i = 0; i < m_vecBoostingCoefficients.size(); i++ )
	//	{
	//		pdCoefficients[i] = m_vecBoostingCoefficients[i];
	//	}
	//	iClass = m_bayesClass.ClassifyBoosting( m_prob_test, pdCoefficients, dSum, fLogProbZero, fLogProbOne);
	//	delete [] pdCoefficients;
	//	break;

	default:
		assert( 0 );
		return -1;
		break;
	}

	// Convert to friendly class
	if( iClass == -1 )
	{
		iClass = 0;
	}
	// Store score
	fScore = (float)dSum;

	// Return classification
	return iClass;
}

int
ModelFeature3D::MeanVarrInit(
	)
{
	vecOrderMean.resize( vecOrderToFeat.size() );
	vecOrderVarr.resize( vecOrderToFeat.size() );

	if( vecOrderMean.size() == vecOrderToFeat.size()
		&& vecOrderVarr.size() == vecOrderToFeat.size() )
	{
		for( int i =0; i < vecOrderMean.size(); i++ )
		{
			vecOrderMean[i].ZeroData();
			vecOrderVarr[i].ZeroData();
		}
		return 0;
	}
	else
	{
		return -1;
	}
}

//int
//ModelFeature3D::MeanVarrCompute(
//	)
//{
//	if( MeanVarrInit() != 0 )
//	{
//		return -1;
//	}
//
//	Feature3D featMeanZero;
//	featMeanZero.ZeroData();
//
//	vector<int> vecPrevImageCount;
//	vecPrevImageCount.resize( vecImgName.size(), 0 );
//
//	//
//	// Look at stats for duplicates of the same feature
//	//
//	//int piLabels[2][2];
//	//float pfScales[2];
//	//int piCounts[2];
//
//	int iDupes;
//	int iSamples;
//
//	for( int i=0; i < vecOrderToFeat.size(); i++ )
//	{
//		int iFeat = vecOrderToFeat[i];
//
//		Feature3D &featMean = vecOrderMean[i];
//		Feature3D &featVarr = vecOrderVarr[i];
//
//		featMean.ZeroData();
//		featVarr.ZeroData();
//
//		//FILE *outfile = fopen( "feat000.txt", "wt" );
//
//		//for( int j = 0; j < vecPrevImageCount.size(); j++ )
//		//{
//		//	vecPrevImageCount[j] = 0;
//		//}
//		//for( int j = 0; j < 2; j++ )
//		//{
//		//	pfScales[j] = 0;
//		//	piCounts[j] = 0;
//		//	piLabels[j][0] = 0;
//		//	piLabels[j][1] = 0;
//		//}
//
//		iSamples = 0;
//		iDupes = 0;
//		for( int i1 = 0; i1 < vecFeatToIndexInfoCount[iFeat]; i1++ )
//		{
//			IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i1];
//
//			float fThres = vecFeatAppearanceThresholds[iFeat];
//
//			if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat] )
//			{
//				int iInfo = II1.Info();
//				int iFeat2 = II1.Index();
//				int iImg2 = vecFeatToImgName[iFeat2];
//				int iLabel2 = vecImgLabels[iImg2];
//
//				if( iLabel2 == -1 )
//				{
//					// Feature comes from an invalidated label, don't include
//					continue;
//				}
//				assert( iLabel2 == 0 || iLabel2 == 1 );
//
//				Feature3D featOther;
//				featOther = vecFeats[iFeat2];
//				//featOther.InitFromFeature3DShort( vecFeats[iFeat2] );
//				featMean.AccumulateData( featOther ); // sum_i x_i
//				featVarr.AccumulateDataSqr( featOther, featMeanZero ); // sum_i x_i*x_i
//				iSamples++;
//
//			//	char pcFileName[400];
//			//	sprintf( pcFileName, "img%3.3d_index%3.3d_feat%5.5d", iImg2, i1, iFeat2 );
//			//	OutputFeatureInVolume( iFeat2, pcFileName );	
//			//	fprintf( outfile, "%f\n", featOther.data_zyx[0][0][0] );
//
//			//	if( vecPrevImageCount[ iImg2 ] == 0 )
//			//	{
//			//		if( iLabel2 == 0 || iLabel2 == 1 )
//			//		{
//			//			piCounts[0] += 1;
//			//			piLabels[0][iLabel2] += 1;
//			//			pfScales[0] += log( featOther.scale );
//			//		}
//			//	}
//			//	else
//			//	{
//			//		iDupes++;
//			//		if( vecPrevImageCount[ iImg2 ] == 1 )
//			//		{
//			//			if( iLabel2 == 0 || iLabel2 == 1 )
//			//			{
//			//				piCounts[1] += 1;
//			//				piLabels[1][iLabel2] += 1;
//			//				pfScales[1] += log( featOther.scale );
//			//			}
//			//		}
//			//		if( vecPrevImageCount[ iImg2 ] > 2 )
//			//		{
//			//			printf( "Greater: %d\n", vecPrevImageCount[ iImg2 ] );
//			//			OutputFeatureInVolume( iFeat2, pcFileName );
//			//		}
//			//	}
//			//	vecPrevImageCount[ iImg2 ]++;
//
//			}
//			//else
//			//{
//			//	pfScales[0] /= (float)(piCounts[0]);
//			//	pfScales[1] /= (float)(piCounts[1]);
//			//	break;
//			//}
//		}
//		//fclose( outfile );
//		featMean.CalculateMeanVarr( featMean, featVarr, iSamples );
//	}
//
//	return 0;
//}

//int
//ModelFeature3D::RelearnFeatsMahalanobisDist(
//	)
//{
//	// For each learned feature, use estimated Mahalanobis distance to re-calculate
//	// feature stats, etc.
//
//	//
//	// The number of geometrically consistent matches will not change, only the appearance...
//	//
//
//	vector< unsigned char > vecucGroundTruth;
//
//	vecucGroundTruth.resize( vecFeats.size() );
//
//	for( int i = 0; i < vecOrderToFeat.size(); i++ )
//	{
//		int iFeat = vecOrderToFeat[i];
//
//		Feature3D &featMean = vecOrderMean[i];
//		Feature3D &featVarr = vecOrderVarr[i];
//
//		MODEL_FEATURE &feat = vecFeats[iFeat];
//
//
//
//		// Zero group truth flags
//		for( int iFeat2 = 0; iFeat2 < vecFeats.size(); iFeat2++ )
//		{
//			vecucGroundTruth[iFeat2] = 0;
//		}
//
//		FILE *outfile;
//		char pcFileName[400];
//		sprintf( pcFileName, "idx%3.3d_feat%6.6d.txt", i, iFeat );
//		outfile = fopen( pcFileName, "wt" );
//		for( int i1 = 0; i1 < vecFeatToIndexInfoCount[iFeat]; i1++ )
//		{
//			IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i1];
//			int iFeat2 = II1.Index();
//			int iImg2 = vecFeatToImgName[iFeat2];
//			int iLabel2 = vecImgLabels[iImg2];
//
//			MODEL_FEATURE &feat2 = vecFeats[ iFeat2 ];
//
//			float fThres = vecFeatAppearanceThresholds[iFeat];
//
//			float fDistEuc = feat.MODEL_FEATURE_DIST( feat2 );
//			float fDistMah = feat.DistSqr( feat2, featVarr );
//
//			fprintf( outfile, "%d\t%d\t%f\t%f\t%d\n", i1, iLabel2, fDistEuc, fDistMah, II1.DistSqrLessThanNeg() );
//
//			if( II1.DistSqrLessThanNeg() != 99999999 )
//			{
//				// Save new ground truth distance for valid IndexInfo
//				II1.SetDistSqr( fDistMah );
//				vecucGroundTruth[iFeat2] = 1;
//			}
//		}
//		fclose( outfile );
//
//		// Re-sort according to Mahalanobis distance
//		sort( vecIndexInfo.begin() + vecFeatToIndexInfo[iFeat],
//			vecIndexInfo.begin() + vecFeatToIndexInfo[iFeat] + vecFeatToIndexInfoCount[iFeat],
//			indexInfoDistSqrLeastToGreatest
//			);
//
//		sprintf( pcFileName, "index%3.3d_feat%5.5d", i, iFeat );
//		OutputFeatureInVolume( iFeat, pcFileName );
//
//		for( int iFeat2 = 0; iFeat2 < vecFeats.size(); iFeat2++ )
//		{
//			const MODEL_FEATURE &feat2 = vecFeats[iFeat2];
//
//			// Features must be of the same label but not of the same image
//			if( vecFeatToImgName[iFeat] != vecFeatToImgName[iFeat2] && vecImgLabels[vecFeatToImgName[iFeat2]] != -1 )
//			{
//				if( vecucGroundTruth[iFeat2] == 0 ) // Not a ground truth match
//				{
//					float fDistMah = feat.DistSqr( feat2, featVarr );
//					for( int i = 0; i < vecFeatToIndexInfoCount[iFeat]; i++ )
//					{
//						IndexInfo &II = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i];
//						if( fDistMah < II.DistSqr() )
//						{
//							II.SetDistSqrLessThanNeg( II.DistSqrLessThanNeg() + 1 );
//							break;
//						}
//					}
//				}
//			}
//		}
//
//		sprintf( pcFileName, "idx%3.3d_feat%6.6d_mah.txt", i, iFeat );
//		outfile = fopen( pcFileName, "wt" );
//		for( int i1 = 0; i1 < vecFeatToIndexInfoCount[iFeat]; i1++ )
//		{
//			IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i1];
//			int iFeat2 = II1.Index();			
//			int iImg2 = vecFeatToImgName[iFeat2];
//			int iLabel2 = vecImgLabels[iImg2];
//
//			MODEL_FEATURE &feat2 = vecFeats[ iFeat2 ];
//
//			float fThres = vecFeatAppearanceThresholds[iFeat];
//
//			float fDistEuc = feat.DistSqr( feat2 );
//			float fDistMah = feat.DistSqr( feat2, featVarr );
//
//			fprintf( outfile, "%d\t%d\t%f\t%f\t%d\n", i1, iLabel2, fDistEuc, fDistMah, II1.DistSqrLessThanNeg() );
//		}
//		fclose( outfile );
//	}
//
//	return 0;
//}

int
ModelFeature3D::Remove2ndImageMatches(
	)
{
	// Keep track of multiple non-mirror matches to the same
	// images - bad, especially for cortex.
	int *piImageLabels = new int[vecImgName.size()]; 
	assert( piImageLabels );
	if( !piImageLabels )
	{
		return -1;
	}

	for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	{
		// Retreive label for this feature
		MODEL_FEATURE &feat = vecFeats[iFeat];
		int iFeatImg = vecFeatToImgName[iFeat];
		//int iFeatLabel = vecImgLabels[iFeatImg];

		memset( piImageLabels, 0, sizeof(int)*vecImgName.size() );
		int iDuplicateImages = 0;

		//FILE *outfile = fopen( "write_reorder.txt", "wt" );
		//for( int i = 0; i < vecFeatToIndexInfoCount[iFeat]; i++ )
		//{
		//	IndexInfo &ii = vecIndexInfo[ vecFeatToIndexInfo[iFeat] + i ];

		//	int iInfo = ii.Info();
		//	int iFeat2 = ii.Index();
		//	int iFeatImg2 = vecFeatToImgName[iFeat2];

		//	fprintf( outfile, "%6.6d\t%3.3d\timg%3.3d\t%.0f\t", iFeat2, ii.DistSqrLessThanNeg(), iFeatImg2, ii.DistSqr() );

		//	int iIndexImg = vecFeatToImgName[ iFeat2 ];
		//	int iBit = 1<<iInfo;
		//	if( (piImageLabels[iIndexImg] & iBit) != 0 )
		//	{	
		//		fprintf( outfile, "1\n" );
		//	}
		//	else
		//	{
		//		fprintf( outfile, "0\n" );
		//	}
		//	piImageLabels[iIndexImg] |= iBit;
		//}
		//fclose( outfile );


		//memset( piImageLabels, 0, sizeof(int)*vecImgName.size() );
		//iDuplicateImages = 0;

		// Start at index 1 (index 0 is the feature itself)

		int iCount__ = vecFeatToIndexInfoCount[iFeat];
		int iIndex__ = vecFeatToIndexInfo[iFeat];

		for( int i = 1; i < vecFeatToIndexInfoCount[iFeat]; i++ )
		{
			IndexInfo &ii = vecIndexInfo[ vecFeatToIndexInfo[iFeat] + i ];

			int iInfo = ii.Info();
			int iFeat2 = ii.Index();
			int iFeatImg2 = vecFeatToImgName[iFeat2];

			int iIndexImg = vecFeatToImgName[ iFeat2 ];
			int iBit = 1<<iInfo;
			if( (piImageLabels[iIndexImg] & iBit) != 0 )
			{
				// Duplicate match: consider as a negative match
				iDuplicateImages++;

				// Propagate false matches + 1 (for this feature) to next feature in line
				if( i+1 < vecFeatToIndexInfoCount[iFeat] )
				{
					IndexInfo &iiNext = vecIndexInfo[ vecFeatToIndexInfo[iFeat] + i + 1];
					iiNext.SetDistSqrLessThanNeg( iiNext.DistSqrLessThanNeg() + ii.DistSqrLessThanNeg() + 1 );
				}

				// Set to extremely large number of false matches and distance
				ii.SetDistSqrLessThanNeg( 99999999 );
				ii.SetDistSqr( 32767.0*32767.0*32767.0 );
				
			}
			piImageLabels[iIndexImg] |= iBit;
		}

		if( iDuplicateImages > 0 )
		{
			// Re-sort according to distance
			sort( vecIndexInfo.begin() + vecFeatToIndexInfo[iFeat],
				vecIndexInfo.begin() + vecFeatToIndexInfo[iFeat] + vecFeatToIndexInfoCount[iFeat],
				indexInfoDistSqrLeastToGreatest
				);
		}

		//memset( piImageLabels, 0, sizeof(int)*vecImgName.size() );
		//iDuplicateImages = 0;

		//outfile = fopen( "write_reorder_after.txt", "wt" );
		//// Start at index 1 (index 0 is the feature itself)
		//for( int i = 0; i < vecFeatToIndexInfoCount[iFeat]; i++ )
		//{
		//	IndexInfo &ii = vecIndexInfo[ vecFeatToIndexInfo[iFeat] + i ];

		//	int iInfo = ii.Info();
		//	int iFeat2 = ii.Index();
		//	int iFeatImg2 = vecFeatToImgName[iFeat2];

		//	fprintf( outfile, "%6.6d\t%3.3d\timg%3.3d\t%.0f\t", iFeat2, ii.DistSqrLessThanNeg(), iFeatImg2, ii.DistSqr() );

		//	int iIndexImg = vecFeatToImgName[ iFeat2 ];
		//	int iBit = 1<<iInfo;
		//	if( (piImageLabels[iIndexImg] & iBit) != 0 )
		//	{	
		//		fprintf( outfile, "1\n" );
		//	}
		//	else
		//	{
		//		fprintf( outfile, "0\n" );
		//	}
		//	piImageLabels[iIndexImg] |= iBit;
		//}
		//fclose( outfile );
	}

	delete [] piImageLabels;

	return 0;
}

int
ModelFeature3D::AddRandomNegatives(
		int iNegativeCount
	)
{
	// Generate vector of random negatives
	vector< MODEL_FEATURE > vecNegatives;
	vecNegatives.resize( iNegativeCount );
	for( int iFeat = 0; iFeat < iNegativeCount; iFeat++ )
	{
		Feature3D feat;
		feat.RandomAppearance();
		feat.NormalizeData();
		feat.OutputVisualFeature( "random" );
		vecNegatives[iFeat].InitFromFeature3D( feat );
	}

	for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	{
		MODEL_FEATURE &feat = vecFeats[iFeat];

		if( vecFeatValid[iFeat] )
		{
			for( int iFeat2 = 0; iFeat2 < vecNegatives.size(); iFeat2++ )
			{
				const MODEL_FEATURE &feat2 = vecNegatives[iFeat2];

				float fSqrDist = feat.MODEL_FEATURE_DIST( feat2 );
				for( int i = 0; i < vecFeatToIndexInfoCount[iFeat]; i++ )
				{
					IndexInfo &II = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i];
					if( fSqrDist < II.DistSqr() )
					{
						II.SetDistSqrLessThanNeg( II.DistSqrLessThanNeg() + 1 );
						break;
					}
				}
			}
		}
	}

	return 1;
}

int
ModelFeature3D::OutputNearestNeighbors(
		int iFeat,
		char *pcFileNameBase,
		int iNNCount,
		int bMirror
	)
{
	const MODEL_FEATURE &feat = vecFeats[iFeat];

	vector< pair<int,float> > vecIndexDist;
	for( int iFeat2 = 0; iFeat2 < vecFeats.size(); iFeat2++ )
	{
		const MODEL_FEATURE &feat2 = vecFeats[iFeat2];
		float fSqrDist = feat.MODEL_FEATURE_DIST( feat2 );

		vecIndexDist.push_back( pair<int,float>( iFeat2, fSqrDist ) );

		if( bMirror )
		{
			// Encode mirror as a negative feature index
			//fSqrDist = feat.DistSqrFlipX( feat2 );
			fSqrDist = 1000000;
			vecIndexDist.push_back( pair<int,float>( -iFeat2, fSqrDist ) );
		}
	}

	sort( vecIndexDist.begin(), vecIndexDist.end(), pairIntFloatLeastToGreatest );

	for( int i = 0; i < vecIndexDist.size() && i < iNNCount; i++ )
	{
		int iFeat2 = vecIndexDist[i].first;
		float fDist = vecIndexDist[i].second;

		int bMirrored = 0;
		if( iFeat < 0 )
		{
			bMirrored = 1;
		}

		MODEL_FEATURE &feat2 = vecFeats[iFeat2];

		int bCompatible = 0;
		if( compatible_features( feat, feat2 ) )
		{
			bCompatible = 1;
		}

		char pcFileName[400];
		sprintf( pcFileName,
			"%s_feat%5.5d_matchindex%5.5d_match%d_mirror%d_compatible%d.pgm",
			pcFileNameBase, iFeat, i, iFeat, bMirrored, bCompatible );

		int iImg2 = vecFeatToImgName[iFeat2];
		char *pcImgName = vecImgName[ iImg2 ];

		FEATUREIO fio;
		if( fioRead( fio, pcImgName ) )
		{
			feat2.OutputVisualFeatureInVolume( fio, pcFileName, 1 );
			fioDelete( fio );
		}
		
		sprintf( pcFileName,
			"%s_feat%5.5d_matchindex%5.5d_intensities.pgm",
			pcFileNameBase, iFeat, i, iFeat, bMirrored, bCompatible );

		MODEL_FEATURE feat2_3d;
		feat2_3d = feat2;
		//feat2_3d.InitFromFeature3DShort( feat2 );
		feat2_3d.OutputVisualFeature( pcFileName );
	}

	return 1;
}

//int
//ModelFeature3D::VisualizeVarriance(
//						int iFeat
//	)
//{
//	Feature3D featMean;
//	Feature3D featVarr;
//	featMean.ZeroData();
//	featVarr.ZeroData();
//
//	featMean = vecFeats[iFeat];
//	//featMean.InitFromFeature3DShort( vecFeats[iFeat] );
//
//	int iDuplicateImages = 0;
//	int iSum = 0;
//	for( int i1 = 1; i1 < vecFeatToIndexInfoCount[iFeat]; i1++ )
//	{
//		IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i1];
//
//		float fThres = vecFeatAppearanceThresholds[iFeat];
//
//		if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat] )
//		{
//				int iInfo = II1.Info();
//				int iFeat2 = II1.Index();
//
//				Feature3D featOther;
//				featOther = vecFeats[iFeat2];
//				//featOther.InitFromFeature3DShort( vecFeats[iFeat2] );
//				featVarr.AccumulateDataSqr( featOther, featMean );
//		}
//		else
//		{
//			break;
//		}
//	}
//
//	featMean.OutputVisualFeature( "mean" );
//	featVarr.OutputVisualFeature( "varr" );
//
//	return 0;
//}

int
ModelFeature3D::FeaturesWithinThreshold(
						int iFeat,
						int *piImages
	)
{
	if( piImages != 0 )
	{
		memset( piImages, 0, vecImgName.size()*sizeof(int) );
	}

	//unsigned char piDupes[500];
	//memset( piDupes, 0,  sizeof(piDupes) );

	int iDuplicateImages = 0;
	int iSum = 0;
	for( int i1 = 1; i1 < vecFeatToIndexInfoCount[iFeat]; i1++ )
	{
		IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i1];

		float fThres = vecFeatAppearanceThresholds[iFeat];

		if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat] )
		{
			if( piImages != 0 )
			{
				int iInfo = II1.Info();
				int iFeat2 = II1.Index();
				int iIndexImg = vecFeatToImgName[iFeat2];
				int iBit = 1<<iInfo;
				if( (piImages[iIndexImg] & iBit) != 0 )
				{
					// Duplicate match, do not count
					iDuplicateImages++;
					//piDupes[i1]++;
				}
				else
				{
					iSum++;
				}
				piImages[iIndexImg] |= iBit;
			}
			else
			{
				iSum++;
			}
		}
		else
		{
			//piDupes[i1] = 5555555;
			break;
		}
	}

	// Here just fill out the images
	if( piImages != 0 )
	{
		memset( piImages, -1, vecImgName.size()*sizeof(int) );
		// count self
		for( int i1 = 0; i1 < vecFeatToIndexInfoCount[iFeat]; i1++ )
		{
			IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i1];

			if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat] )
			{
				int iFeat2 = II1.Index();
				if( piImages != 0 )
				{
					if( piImages[ vecFeatToImgName[iFeat2] ] == -1 )
					{
						// Save index of model feature
						piImages[ vecFeatToImgName[iFeat2] ] = II1.Index();
					}
					// Do not report duplicates, see how this affects things... 1:12 PM 3/30/2009
					else if( piImages[ vecFeatToImgName[iFeat2] ] >= 0 )
					{
						// set to -2, 2 matches to the same image
						piImages[ vecFeatToImgName[iFeat2] ] = -2;
					}
					else
					{
						// count negative matches to the same image
						piImages[ vecFeatToImgName[iFeat2] ]--;
					}
				}
			}
			else
			{
				break;
			}
		}
	}
	return iSum;
}

int
ModelFeature3D::ValidFeaturesWithinThreshold(
						int iFeat,
						int *piImages
	)
{
	if( piImages != 0 )
	{
		memset( piImages, 0, vecImgName.size()*sizeof(int) );
	}

	//unsigned char piDupes[500];
	//memset( piDupes, 0,  sizeof(piDupes) );

	int iDuplicateImages = 0;
	int iSum = 0;
	for( int i1 = 1; i1 < vecFeatToIndexInfoCount[iFeat]; i1++ )
	{
		IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i1];
		if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat] )
		{
			// Only count valid features
			int iFeat2 = II1.Index();
			if( vecFeatValid[iFeat2] != 0 )
			{
				if( piImages != 0 )
				{
					int iInfo = II1.Info();
					int iIndexImg = vecFeatToImgName[iFeat2];
					int iBit = 1<<iInfo;
					if( (piImages[iIndexImg] & iBit) != 0 )
					{
						// Duplicate match, do not count
						iDuplicateImages++;
						//piDupes[i1]++;
					}
					else
					{
						iSum++;
					}
					piImages[iIndexImg] |= iBit;
				}
				else
				{
					iSum++;
				}
			}
		}
		else
		{
			//piDupes[i1] = 5555555;
			break;
		}
	}

	// Here just fill out the images
	if( piImages != 0 )
	{
		memset( piImages, -1, vecImgName.size()*sizeof(int) );
		// count self
		for( int i1 = 0; i1 < vecFeatToIndexInfoCount[iFeat]; i1++ )
		{
			IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i1];

			if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat] )
			{
				int iFeat2 = II1.Index();
				if( piImages != 0 )
				{
					if( piImages[ vecFeatToImgName[iFeat2] ] == -1 )
					{
						// Save index of model feature
						piImages[ vecFeatToImgName[iFeat2] ] = II1.Index();
					}
				}
			}
			else
			{
				break;
			}
		}
	}
	return iSum;
}



//
// FeaturesWithinThreshold()
//
// Return the number of valid features within threshold.
//
int
FeaturesWithinThreshold(
						int iFeat,
						vector<char*> &vecImgName, // Array of image names
						vector<int> &vecFeatToImgName, // Maps each Feat to its image name
						vector<MODEL_FEATURE> &vecFeats, // Array of features
						vector<IndexInfo> &vecIndexInfo, // IndexInfo
						vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
						vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
						vector<float> &vecFeatAppearanceThresholds, // For each Feat, appearance thresholds

						int *piImages
	)
{
	if( piImages != 0 )
	{
		memset( piImages, 0, vecImgName.size()*sizeof(int) );
	}

	//unsigned char piDupes[500];
	//memset( piDupes, 0,  sizeof(piDupes) );

	int iDuplicateImages = 0;
	int iSum = 0;
	for( int i1 = 1; i1 < vecFeatToIndexInfoCount[iFeat]; i1++ )
	{
		IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i1];

		float fThres = vecFeatAppearanceThresholds[iFeat];

		if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat] )
		{
			if( piImages != 0 )
			{
				int iInfo = II1.Info();
				int iFeat2 = II1.Index();
				int iIndexImg = vecFeatToImgName[iFeat2];
				int iBit = 1<<iInfo;
				if( (piImages[iIndexImg] & iBit) != 0 )
				{
					// Duplicate match, do not count
					iDuplicateImages++;
					//piDupes[i1]++;
				}
				else
				{
					iSum++;
				}
				piImages[iIndexImg] |= iBit;
			}
			else
			{
				iSum++;
			}
		}
		else
		{
			//piDupes[i1] = 5555555;
			break;
		}
	}

	// Here just fill out the images
	if( piImages != 0 )
	{
		memset( piImages, -1, vecImgName.size()*sizeof(int) );
		// count self
		for( int i1 = 0; i1 < vecFeatToIndexInfoCount[iFeat]; i1++ )
		{
			IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i1];

			if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat] )
			{
				int iFeat2 = II1.Index();
				if( piImages != 0 )
				{
					if( piImages[ vecFeatToImgName[iFeat2] ] == -1 )
					{
						// Save index of model feature
						piImages[ vecFeatToImgName[iFeat2] ] = II1.Index();
					}
					// Do not report duplicates, see how this affects things... 1:12 PM 3/30/2009
					else if( piImages[ vecFeatToImgName[iFeat2] ] >= 0 )
					{
						// set to -2, 2 matches to the same image
						piImages[ vecFeatToImgName[iFeat2] ] = -2;
					}
					else
					{
						// count negative matches to the same image
						piImages[ vecFeatToImgName[iFeat2] ]--;
					}
				}
			}
			else
			{
				break;
			}
		}
	}
	return iSum;
}

int
ValidFeaturesWithinThreshold(
						int iFeat,
						vector<char*> &vecImgName, // Array of image names
						vector<int> &vecFeatToImgName, // Maps each Feat to its image name
						vector<MODEL_FEATURE> &vecFeats, // Array of features
						vector<IndexInfo> &vecIndexInfo, // IndexInfo
						vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
						vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
						vector<float> &vecFeatAppearanceThresholds, // For each Feat, appearance thresholds
						vector<int> &vecFeatValid, // For each Feat, valid flags

						int *piImages
	)
{
	if( piImages != 0 )
	{
		memset( piImages, 0, vecImgName.size()*sizeof(int) );
	}

	//unsigned char piDupes[500];
	//memset( piDupes, 0,  sizeof(piDupes) );

	int iDuplicateImages = 0;
	int iSum = 0;
	for( int i1 = 1; i1 < vecFeatToIndexInfoCount[iFeat]; i1++ )
	{
		IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i1];
		if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat] )
		{
			// Only count valid features
			int iFeat2 = II1.Index();
			if( vecFeatValid[iFeat2] != 0 )
			{
				if( piImages != 0 )
				{
					int iInfo = II1.Info();
					int iIndexImg = vecFeatToImgName[iFeat2];
					int iBit = 1<<iInfo;
					if( (piImages[iIndexImg] & iBit) != 0 )
					{
						// Duplicate match, do not count
						iDuplicateImages++;
						//piDupes[i1]++;
					}
					else
					{
						iSum++;
					}
					piImages[iIndexImg] |= iBit;
				}
				else
				{
					iSum++;
				}
			}
		}
		else
		{
			//piDupes[i1] = 5555555;
			break;
		}
	}

	// Here just fill out the images
	if( piImages != 0 )
	{
		memset( piImages, -1, vecImgName.size()*sizeof(int) );
		// count self
		for( int i1 = 0; i1 < vecFeatToIndexInfoCount[iFeat]; i1++ )
		{
			IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i1];

			if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat] )
			{
				// Only count valid features: Added after NeuroImage2009
				// This condition was not noticed in leave-one-out testing where there were few invalid features.
				// It was noticed 
				int iFeat2 = II1.Index();
				if( vecFeatValid[iFeat2] != 0 )
				{
					if( piImages[ vecFeatToImgName[iFeat2] ] == -1 )
					{
						// Save index of model feature
						piImages[ vecFeatToImgName[iFeat2] ] = II1.Index();
					}
				}
			}
			else
			{
				break;
			}
		}
	}
	return iSum;
}

int
ValidFeaturesWithinThreshold_labels(
						int iFeat,
						vector<char*> &vecImgName, // Array of image names
						vector<int> &vecFeatToImgName, // Maps each Feat to its image name
						vector<MODEL_FEATURE> &vecFeats, // Array of features
						vector<IndexInfo> &vecIndexInfo, // IndexInfo
						vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
						vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
						vector<float> &vecFeatAppearanceThresholds, // For each Feat, appearance thresholds
						vector<int> &vecFeatValid, // For each Feat, valid flags

						vector<int> &vecImgLabels,

						int *piImages
	)
{
	if( !piImages )
	{
		// Need an image array
		return -1;
	}
	int iSamples = ValidFeaturesWithinThreshold( iFeat,
						vecImgName, vecFeatToImgName, vecFeats,
						vecIndexInfo, vecFeatToIndexInfo, vecFeatToIndexInfoCount,
						vecFeatAppearanceThresholds, vecFeatValid,
						piImages);

	int iImg = vecFeatToImgName[iFeat];
	int iLabel = vecImgLabels[iImg];

	// Go through and count only feature occurrences with the same label as iFeat.
	int iSum = 0;
	for( int i = 0; i < vecImgName.size(); i++ )
	{
		if( piImages[i] > 0 )
		{
			int iOtherLabel = vecImgLabels[i];
			if( iLabel == iOtherLabel )
			{
				iSum++;
			}
		}
	}
	iSum--; // Remove self
	if( iSum > iSamples ) // iSamples adds 1
	{
		iSamples = ValidFeaturesWithinThreshold( iFeat,
						vecImgName, vecFeatToImgName, vecFeats,
						vecIndexInfo, vecFeatToIndexInfo, vecFeatToIndexInfoCount,
						vecFeatAppearanceThresholds, vecFeatValid,
						piImages);
	}
	else if( iSum == iSamples && iSum > 2 )
	{
		iSamples = ValidFeaturesWithinThreshold( iFeat,
						vecImgName, vecFeatToImgName, vecFeats,
						vecIndexInfo, vecFeatToIndexInfo, vecFeatToIndexInfoCount,
						vecFeatAppearanceThresholds, vecFeatValid,
						piImages);
	}

	assert( iSamples >= iSum );

	return iSum;
}

int
ModelFeature3D::WriteFeatureHistogram(
		char *pcFileName
	)
{
	int *piImages = new int[1000];
	FILE *infile = fopen( pcFileName, "wt" );
	if( infile )
	{
		for( int i = 0; i < vecOrderToFeat.size(); i++ )
		{
			int iFeat = vecOrderToFeat[i];

			int iCount = FeaturesWithinThreshold( iFeat, piImages );

			fprintf( infile, "%d\t%d\t%d\n", i, iFeat, iCount );
		}
		fclose( infile );
		return 0;
	}
	else
	{
		return -1;
	}
	delete [] piImages;
}


int
ModelFeature3D::SetValidFlag(
			int iFlag // flag
		)
{
	int iCount = 0;
	assert( iFlag == 0 || iFlag == 1 );
	for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	{
		vecFeatValid[iFeat] = iFlag;
		iCount++;
	}
	return iCount;
}

int
ModelFeature3D::SetValidFlagImage( 
		int iImg,
		int iFlag
		)
{
	int iCount = 0;
	assert( iFlag == 0 || iFlag == 1 );
	for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	{
		if( vecFeatToImgName[iFeat] == iImg )
		{
			vecFeatValid[iFeat] = iFlag;
			iCount++;
		}
	}
	return iCount;
}


int
ModelFeature3D::GetFeatsImage( 
		int iImg,
		vector<MODEL_FEATURE> &vecImgFeats
		)
{
	vecImgFeats.clear();
	for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	{
		if( vecFeatToImgName[iFeat] == iImg )
		{
			MODEL_FEATURE &feat = vecFeats[iFeat];
			vecImgFeats.push_back( vecFeats[iFeat] );
		}
	}
	return vecImgFeats.size();
}

int
sort_DATA_PAIR(
			   const void *p1, const void *p2
			   )
{
	DATA_PAIR *dp1 = (DATA_PAIR*)p1;
	DATA_PAIR *dp2 = (DATA_PAIR*)p2;

	if( dp1->iValue < dp2->iValue )
	{
		return 1;
	}
	else if( dp1->iValue > dp2->iValue )
	{
		return -1;
	}
	else
	{
		return 0;
	}
}

int
ModelFeature3D::AlignImages( 
							int iIterations
		)
{
	for( int iTarget = 0; iTarget < vecImgName.size(); iTarget++ )
	{
	}

	for( int iIt = 0; iIt < iIterations; iIt++ )
	{
		vector<MODEL_FEATURE> vecFeats1;
		vector<MODEL_FEATURE> vecFeats2;

		for( int iTarget = 0; iTarget < vecImgName.size(); iTarget++ )
		{
			GetFeatsImage( iTarget, vecFeats1 );
			for( int iSource = 0; iSource < vecImgName.size(); iSource++ )
			{
				if( iSource == iTarget )
				{
					continue;
				}
				GetFeatsImage( iSource, vecFeats2 );
			}
		}
	}
	return -1;
}

#ifdef OPEN_MP_

int
GenerateIndexInfoParallel(
	const vector<char*> &vecImgName, // Array of image names
	const vector<int> &vecFeatToImgName, // Maps each Feat to its image name
	const vector<MODEL_FEATURE> &vecFeats,
	vector<IndexInfo> &vecIndexInfo, // IndexInfo
	vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	int iPCs, // number of principal components to consider, if 0 then use original data
	int iMaxNegatives // Maximum negative features to consider. If -1, then consider all features in vecFeats
)
{
	vecFeatToIndexInfo.clear();
	vecFeatToIndexInfoCount.clear();

	vecFeatToIndexInfo.resize( vecFeats.size() );
	vecFeatToIndexInfoCount.resize( vecFeats.size() );

	// Create vector of IndexInfo vectors, one for each processor
	vector< vector<IndexInfo>  > vecvecIIs;
	vector<int> vecFeatStarts;
	vector<int> vecFeatEnds;

	// Init a hash table for all processes 
	ScaleSpaceXYZHash sshData;
	InitHashTable( sshData, vecFeats );

#define PROCESSORS_N 4

	int iChunk = vecFeats.size() / PROCESSORS_N;	
	for( int i = 0; i < PROCESSORS_N; i++ )
	{
		vector<IndexInfo> vecIITmp;
		vecIITmp.clear();
		vecvecIIs.push_back( vecIITmp );
		vecFeatStarts.push_back(i*iChunk);
		if( i == (PROCESSORS_N - 1) )
		{
			vecFeatEnds.push_back( vecFeats.size()-1 );
		}
		else
		{
			vecFeatEnds.push_back( (i+1)*iChunk-1 );
		}
	}

	// Generate 4 parallel IndexInfo vectors
	omp_set_num_threads(PROCESSORS_N);
	int i;
	printf( "Numthreads: %d\n", omp_get_num_threads() );
	//#pragma omp parallel private(i) 
	{
		//printf( "Numthreads: %d\n", omp_get_num_threads() );
		#pragma omp parallel for
		for( i = 0; i < PROCESSORS_N; i++ )
		{
			printf( "Current thread: %d\n", i );
			GenerateIndexInfoSubSection( vecImgName, vecFeatToImgName, vecFeats,
					vecvecIIs[i], // 
					vecFeatToIndexInfo,
					vecFeatToIndexInfoCount,
					&sshData,
					vecFeatStarts[i],
					vecFeatEnds[i],
					iPCs,
					iMaxNegatives
					);
		}
	}

	printf( "Merging..." );

	// Merge IndexInfo vectors

	vecIndexInfo.clear();
	for( int i = 0; i < PROCESSORS_N; i++ )
	{
		// Save current start position in IndexInfo vector
		int iStart = vecIndexInfo.size();

		//printf( "VecSize: %d, concatenating vector: %d, size: %d\n", iStart, i, vecvecIIs[i].size() );

		vector<IndexInfo> &vecII = vecvecIIs[i];

		// Concatenate vectors
		for( int iII = 0; iII < vecII.size(); iII++ )
		{
			IndexInfo &II = vecII[iII];
			vecIndexInfo.push_back( II );
		}

		// Update pointers from feature to IndexInfo
		for( int iFeat = vecFeatStarts[i]; iFeat <= vecFeatEnds[i]; iFeat++ )
		{
			vecFeatToIndexInfo[iFeat] += iStart;
		}
	}

	return 0;
}

#endif // OPEN_MP_

int
GenerateIndexInfoSubSection(
	const vector<char*> &vecImgName, // Array of image names
	const vector<int> &vecFeatToImgName, // Maps each Feat to its image name
	const vector<MODEL_FEATURE> &vecFeats,
	vector<IndexInfo> &vecIndexInfo, // IndexInfo
	vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	ScaleSpaceXYZHash *pssh, // Hash table, internal 
	int iFeatStart,	// Index of first feature in vecFeats to process
	int iFeatEnd, // Index of last feature in vecFeats to process
	int iPCs, // number of principal components to consider, if 0 then use original data
	int iMaxNegatives // Maximum negative features to consider. If -1, then consider all features in vecFeats
)
{
	int iFeat;

	vecIndexInfo.clear();

	if( vecFeatToIndexInfo.size() != vecFeats.size() )
	{
		assert( 0 );
		return -1;
	}
	if( vecFeatToIndexInfoCount.size() != vecFeats.size() )
	{
		assert( 0 );
		return -1;
	}

	if( iFeatEnd == -1 )
	{
		// Set to the last vector feature
		iFeatEnd = vecFeats.size()-1;
	}

	if( iMaxNegatives < 0 )
	{
		iMaxNegatives = vecFeats.size();
	}

	// Init a hash table if necessary
	ScaleSpaceXYZHash sshData;
	if( !pssh )
	{
		InitHashTable( sshData, vecFeats );
		pssh = &sshData;
	}
	
	// Ke
typedef int MATCH_INFO[2];
	MATCH_INFO *vecFeatureInstancesPerImage = new MATCH_INFO[vecImgName.size()];

	for( iFeat = iFeatStart; iFeat <= iFeatEnd; iFeat++ )
	{
		//printf( "Feature %d of %d", iFeat, vecFeats.size() );
		printf( "Feature %d of %d\n", iFeat - iFeatStart, iFeatEnd - iFeatStart );
		const MODEL_FEATURE &feat = vecFeats[iFeat];

		vecFeatToIndexInfo[iFeat] = vecIndexInfo.size();
		vecFeatToIndexInfoCount[iFeat] = 0;

		// Add feature to it's own list
		vecIndexInfo.push_back( IndexInfo( iFeat, 0, 0.0f ) );
		vecFeatToIndexInfoCount[iFeat]++;

		// Determine all geometrically consistent matches using hash table: much quicker
		int piNInc[3];
		float pfXYZ[3];
		pfXYZ[0] = feat.x;
		pfXYZ[1] = feat.y;
		pfXYZ[2] = feat.z;
#define INCMAG 2
		for( int iS = -INCMAG; iS <= INCMAG; iS++ )
		{
			for( piNInc[0] = -INCMAG; piNInc[0] <= INCMAG; piNInc[0]++ )
			{
				for( piNInc[1] = -INCMAG; piNInc[1] <= INCMAG; piNInc[1]++ )
				{
					for( piNInc[2] = -INCMAG; piNInc[2] <= INCMAG; piNInc[2]++ )
					{
						ScaleSpaceXYZBin *pssb = pssh->GetNeighborBin( pfXYZ, feat.scale, iS,  piNInc );
						if( pssb )
						{
							for( ScaleSpaceXYZBin::iterator it = pssb->begin(); it != pssb->end(); it++ )
							{
								int iFeat2 = *it;
								const MODEL_FEATURE &feat2 = vecFeats[iFeat2];
								if(
									vecFeatToImgName[iFeat] != vecFeatToImgName[iFeat2]
									&& compatible_features( feat, feat2 )
										)
								{
									float fSqrDist;
									if( iPCs > 0 )
									{
										if( iPCs < PC_ARRAY_SIZE )
											fSqrDist = feat.DistSqrPCs( feat2, iPCs );
										else
											fSqrDist = feat.MODEL_FEATURE_DIST( feat2, iPCs );
									}
									else
									{
										fSqrDist = feat.MODEL_FEATURE_DIST( feat2 );
									}

									vecIndexInfo.push_back( IndexInfo( iFeat2, 0, fSqrDist ) );
									vecFeatToIndexInfoCount[iFeat]++;
									vecFeatureInstancesPerImage[ vecFeatToImgName[iFeat2] ][0]++;
								}
							}
						}
					}
				}
			}
		}

		// Sort according to distance
		sort( vecIndexInfo.begin() + vecFeatToIndexInfo[iFeat],
			vecIndexInfo.begin() + vecFeatToIndexInfo[iFeat] + vecFeatToIndexInfoCount[iFeat],
			indexInfoDistSqrLeastToGreatest
			);

		// Here, should keep only one match per image
		// This is already accounted for in likelihood ratio estimation tho
		//for( int i = 0; i < feat.m_iIndexInfoCount; i++ )
		//{
		//	IndexInfo &II = vecIndexInfo[feat.m_iIndexInfoIndex+i];
		//	int iIndex = II.Index();
		//	int iM = II.Info();
		//	const MODEL_FEATURE &feat2 = GetModelPoint(iIndex);
		//	if( vecFeatureInstancesPerImage[feat2.m_iImageIndex][iM] > 0 )
		//	{
		//		// Keep feature, flag zero
		//		vecFeatureInstancesPerImage[feat2.m_iImageIndex][iM] = 0;
		//	}
		//	else
		//	{
		//		// Remove feature
		//		vecIndexInfo.erase( vecIndexInfo.begin() + feat.m_iIndexInfoIndex+i );
		//		feat.m_iIndexInfoCount--;
		//		feat.m_HC.m_fTotalPositives--;
		//		i--;
		//	}
		//}

		//
		// Compute false matches for this feature
		// ** this should be put back in
		//
		for( int iFeat2 = 0; iFeat2 < vecFeats.size() && iFeat2 < iMaxNegatives; iFeat2++ )
		{
			const MODEL_FEATURE &feat2 = vecFeats[iFeat2];

			// Features must be of the same label but not of the same image
			if( vecFeatToImgName[iFeat] != vecFeatToImgName[iFeat2] )
			{
				if( !compatible_features( feat, feat2 ) )
				{
					float fSqrDist;
					if( iPCs > 0 )
					{
						if( iPCs < PC_ARRAY_SIZE )
							fSqrDist = feat.DistSqrPCs( feat2, iPCs );
						else
							fSqrDist = feat.MODEL_FEATURE_DIST( feat2, iPCs );
					}
					else
					{
						fSqrDist = feat.MODEL_FEATURE_DIST( feat2 );
					}

					for( int i = 0; i < vecFeatToIndexInfoCount[iFeat]; i++ )
					{
						IndexInfo &II = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i];
						if( fSqrDist < II.DistSqr() )
						{
							II.SetDistSqrLessThanNeg( II.DistSqrLessThanNeg() + 1 );
							break;
						}
					}
				}
			}
		}

		//printf( ", II: %d of %d\n", vecFeatToIndexInfoCount[iFeat], vecFeatToIndexInfo[iFeat] );
	}
	delete [] vecFeatureInstancesPerImage;
	return 0;
}

int
GenerateIndexInfo(
	const vector<char*> &vecImgName, // Array of image names
	const vector<int> &vecFeatToImgName, // Maps each Feat to its image name
	const vector<MODEL_FEATURE> &vecFeats,
	vector<IndexInfo> &vecIndexInfo, // IndexInfo
	vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int> &vecFeatToIndexInfoCount,  // For each Feat, the number of IndexInfos
	int iPCs, // Unused
	int iMaxNegatives // Maximum negative features to consider. If -1, then consider all features in vecFeats
		)
{
	vecFeatToIndexInfo.resize( vecFeats.size() );
	vecFeatToIndexInfoCount.resize( vecFeats.size() );

	int iFeatStart = 0;
	int iFeatEnd = vecFeats.size()-1;

	return GenerateIndexInfoSubSection( 
		vecImgName, vecFeatToImgName, vecFeats, vecIndexInfo, vecFeatToIndexInfo, 
		vecFeatToIndexInfoCount, 0, iFeatStart, iFeatEnd, iPCs, iMaxNegatives );
}

int
SetAppearanceThresholds_f_given_s(
	vector<char*> &vecImgName, // Array of image names
	vector<int> &vecFeatToImgName, // Maps each Feat to its image name
	vector<MODEL_FEATURE> &vecFeats, // Array of features
	vector<IndexInfo> &vecIndexInfo, // IndexInfo
	vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	vector<int> &vecFeatValid, // For each Feat, valid flags

	vector<float> &vecFeatAppearanceThresholds, // For each Feat, threshold
	float fMinLikelihoodRatio,
	int bSetMaxLikelhoodRatio
	)
{
	if( vecFeatValid.size() != vecFeats.size() )
	{
		assert( vecFeatAppearanceThresholds.size() == 0 );
		vecFeatValid.resize( vecFeats.size(), 1 );
	}
	if( vecFeatAppearanceThresholds.size() != vecFeats.size() )
	{
		// Allocate thresholds, set ot zero
		assert( vecFeatAppearanceThresholds.size() == 0 );
		vecFeatAppearanceThresholds.resize( vecFeats.size(), 0.0f );
	}
	int iValidThresholds = 0;

	// Keep track of multiple non-mirror matches to the same
	// images - bad, especially for cortex.
	int *piImageLabels = new int[vecImgName.size()]; 
	assert( piImageLabels );
	if( !piImageLabels )
	{
		return -1;
	}

	for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	{
		if( vecFeatValid[iFeat] == 0 )
		{
			// Don't consider invalid features
			vecFeatAppearanceThresholds[iFeat] = 0.0f;
			continue;
		}

		MODEL_FEATURE &feat = vecFeats[iFeat];

		// Set total positives and total negatives
		float fTotalPositives = vecImgName.size();
		float fTotalNegatives = 0;
		for( int i = 1; i < vecFeatToIndexInfoCount[iFeat]; i++ )
		{
			IndexInfo &ii = vecIndexInfo[ vecFeatToIndexInfo[iFeat] + i ];
			fTotalNegatives += ii.DistSqrLessThanNeg();
		}

#define ACCUM_PRIOR_POS 1
#define ACCUM_PRIOR_NEG 1

		int bThresholdSet = 0;
		float fThresLikePos = 0;
		float fThresLikeNeg = 0;
		float fThresDistSqr = -1;

		// Add a prior of 1, so there are no zero probs
		float fAccumNeg = ACCUM_PRIOR_NEG;

		memset( piImageLabels, 0, sizeof(int)*vecImgName.size() );
		int iDuplicateImages = 0;

		FILE *outfile;

		float fMaxRatio = 0;
		float fMaxPos = 0;
		float fMaxNeg = 0;
		float fMaxThresDistSqr = -1;

		// Start at index 1 (index 0 is the feature itself)
		for( int i = 1; i < vecFeatToIndexInfoCount[iFeat]; i++ )
		{
			IndexInfo &ii = vecIndexInfo[ vecFeatToIndexInfo[iFeat] + i ];

			int iInfo = ii.Info();
			int iFeat2 = ii.Index();

			if( vecFeatValid[iFeat2] == 0 )
			{
				// Don't consider invalid features in positive count
				// Just maintain negative count
				fAccumNeg += ii.DistSqrLessThanNeg();
				continue;
			}

			int iIndexImg = vecFeatToImgName[ iFeat2 ];
			int iBit = 1<<iInfo;
			if( (piImageLabels[iIndexImg] & iBit) != 0 )
			{
				// Duplicate match
				iDuplicateImages++;
			}
			piImageLabels[iIndexImg] |= iBit;

			fAccumNeg += ii.DistSqrLessThanNeg();
			float fAccumPos = i - iDuplicateImages + ACCUM_PRIOR_POS; // 'i' is the number of positive examples
	
			float fLikePos = fAccumPos;// / fTotalPositives; // 11:41 AM 11/28/2007 (why consider totals?)
			float fLikeNeg = fAccumNeg;// / fTotalNegatives; // 11:41 AM 11/28/2007 (this was changed very near the end of my doc)

			if( fLikePos/fLikeNeg >= fMinLikelihoodRatio )
			{
				bThresholdSet = 1;
				fThresDistSqr = ii.DistSqr();
				fThresLikePos = fLikePos;
				fThresLikeNeg = fLikeNeg;

				// Added in 2008
				// Consider keeping the max likelihood ratio?
				if( fLikePos/fLikeNeg > fMaxRatio )
				{
					fMaxRatio = fLikePos/fLikeNeg;
					fMaxPos = fLikePos;
					fMaxNeg = fLikeNeg;
					fMaxThresDistSqr = ii.DistSqr();
				}
			}
		}

		if( bThresholdSet )
		{
			iValidThresholds++;
			if( bSetMaxLikelhoodRatio )
			{
				vecFeatAppearanceThresholds[iFeat] = fMaxThresDistSqr;
			}
			else
			{
				vecFeatAppearanceThresholds[iFeat] = fThresDistSqr;
			}
		}
		else
		{
			vecFeatAppearanceThresholds[iFeat] = 0.0f;
		}
	}

	delete [] piImageLabels;

	return iValidThresholds;
}



//
// FlagInvalidFeatures()
//
// This function clusters features
//
int
FlagInvalidFeatures(
	vector<char*>		&vecImgName, // Array of image names
	vector<int>			&vecFeatToImgName, // Maps each Feat to its image name
	vector<MODEL_FEATURE>	&vecFeats, // Array of features
	vector<IndexInfo>	&vecIndexInfo, // IndexInfo
	vector<int>			&vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int>			&vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	vector<int>			&vecFeatValid,	// For each Feat, is this feature valid?
	vector<float>		&vecFeatAppearanceThresholds, // For each Feat, appearance thresholds

	vector<int>			&vecOrderToFeat,	// Feature indices, sorted from most to least frequent
	int iMinMatches
	//FeatureInstanceCallback *pfiCallback
)
{
	// A peak feature is one which contains as many or more positive matches
	// within it's threshold than any of it's positive matches.
	// Keep the peak feature, discard all features corresponding to it's 
	// positive matches.

	if( vecFeatValid.size() < vecFeats.size() )
	{
		// Vector of valid flags must be zero, flag all as valid
		assert( vecFeatValid.size() == 0 );
		vecFeatValid.resize( vecFeats.size(), 1 );
	}

	int *piFeatsToRemove = new int[vecFeats.size()];
	assert( piFeatsToRemove );

	int *piImageFlags = new int[vecImgName.size()];
	assert( piImageFlags );

	DATA_PAIR *pdpFeats = new DATA_PAIR[vecFeats.size()];
	assert( pdpFeats );

	if( piFeatsToRemove == 0 || piImageFlags == 0 || pdpFeats == 0 )
	{
		printf( "Error: FlagInvalidFeatures() - could not allocate memory.\n" );
		return -1;
	}

	memset( piFeatsToRemove, 0,       sizeof(int)*vecFeats.size() );
	memset( pdpFeats,        0, sizeof(DATA_PAIR)*vecFeats.size() );

	//printf( "FlagInvalidFeatures(): allocated memory.\n" );

	int iRemovers = 0;

	int iF1Samples;
	int iF2Samples;

	for( int iFeat1 = 0; iFeat1 < vecFeats.size(); iFeat1++ )
	{
		int bPeakFeat = 0;
		iF2Samples = 0;

		if( vecFeatValid[iFeat1] == 0 )
		{
			// Remove all features flagged as invalid
			bPeakFeat = 0;
			iF2Samples = 0;
		}
		else
		{
			bPeakFeat = 1;
			iF2Samples = 0;

			MODEL_FEATURE &feat1 = vecFeats[iFeat1];
			iF1Samples = ::ValidFeaturesWithinThreshold( iFeat1,
				vecImgName, vecFeatToImgName, vecFeats,
				vecIndexInfo, vecFeatToIndexInfo, vecFeatToIndexInfoCount,
				vecFeatAppearanceThresholds, vecFeatValid, piImageFlags);

			// Test all features within threshold of Feat1. If one of the
			// features within the threshold has more features within it's
			// threshold, then Feat1 is not a peak features
			for( int i1 = 1; i1 < vecFeatToIndexInfoCount[iFeat1] && bPeakFeat; i1++ )
			{
				IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat1]+i1];

				if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat1] )
				{
					// II1 is a valid instance of feat1
					int iFeat2 = II1.Index();
					 
					iF2Samples = ValidFeaturesWithinThreshold( iFeat2,
						vecImgName, vecFeatToImgName, vecFeats,
						vecIndexInfo, vecFeatToIndexInfo, vecFeatToIndexInfoCount,
						vecFeatAppearanceThresholds, vecFeatValid,
						piImageFlags);

					int iF2SamplesOriginal = FeaturesWithinThreshold( iFeat2,
						vecImgName, vecFeatToImgName, vecFeats,
						vecIndexInfo, vecFeatToIndexInfo, vecFeatToIndexInfoCount,
						vecFeatAppearanceThresholds, piImageFlags);

					//
					// 4:10 AM 11/21/2008 
					// Hmm - according my phd code here: 
					//     if( iF2Samples > iF1Samples )
					// if there was a tight cluster of features which were all instances
					// of each other, then  (iF2Samples == iF1Samples) and I was not
					// pruning any of them!!
					//
					// Trouble here is, what do with a tight cluster of features
					// which has exactly equal numbers? Obviously I should keep one,
					// let's say the first one. How to test if they're the same??
					//
					//

					if( iF2Samples > iF1Samples )
					{
						bPeakFeat = 0;
					}
					else if( iF2Samples == iF1Samples )
					{
						// Arbitrarily keep the feature with the highest index
						// This keeps one features from a tight cluster,
						// which makes detection more computationally efficient but appears to 
						// reduce the quality of the scores?
						if( II1.Index() < iFeat1 )
						{
							bPeakFeat = 0;
						}
					}
				}
				else
				{
					break;
				}
			}
		}

		if( !bPeakFeat || iF1Samples == 0 )
		{
			iRemovers++;
			piFeatsToRemove[iFeat1] = 0;
		}
		else
		{
			if( iMinMatches == 0 )
			{
				iF1Samples++;
			}
			piFeatsToRemove[iFeat1] = iF1Samples;
		}
	}

	//printf( "FlagInvalidFeatures(): scanned all features\n" );

	//FILE *outfile = fopen( "feats.txt", "wt" );
	//for( int iFeat1 = 0; iFeat1 < vecFeats.size(); iFeat1++ )
	//{
	//	fprintf( outfile, "%d\t%d\n", iFeat1, piFeatsToRemove[iFeat1] );
	//}
	//fclose( outfile );

	//printf( "FlagInvalidFeatures(): outputted feats.txt\n" );

	int iValidFeatures = 0;
	// Here we remove all features flagged
	for( int iFeat1 = 0; iFeat1 < vecFeats.size(); iFeat1++ )
	{
		pdpFeats[iFeat1].iIndex = iFeat1;
		pdpFeats[iFeat1].iValue = piFeatsToRemove[iFeat1];
		if( piFeatsToRemove[iFeat1] < iMinMatches )
		{
			// Flag redundant features as invalid
			vecFeatValid[iFeat1] = 0;
		}
		else
		{
			// Flag valid feature
			vecFeatValid[iFeat1] = 1;
			iValidFeatures++;
		}
	}

	// Sort in descending order of occurrence frequency
	qsort( pdpFeats, vecFeats.size(), sizeof(DATA_PAIR), sort_DATA_PAIR );

	vecOrderToFeat.clear();

	// Process all non-zero features in descending order
	for( int iFeat1 = 0; pdpFeats[iFeat1].iValue > 0; iFeat1++ )
	{
		if( vecFeatValid[pdpFeats[iFeat1].iIndex] > 0 )
		{
			// Store order information
			vecOrderToFeat.push_back( pdpFeats[iFeat1].iIndex );

			char pcFile[200];
			sprintf( pcFile, "feat_%4.4d_matches_%3.3d_", pdpFeats[iFeat1].iIndex, pdpFeats[iFeat1].iValue );
			MODEL_FEATURE &feat1 = vecFeats[ pdpFeats[iFeat1].iIndex ];
			//iF1Samples = FeaturesWithinThreshold(feat1,piImageFlags);
			//OutputModelPointAndInstances( pdpFeats[iFeat1].iIndex, pcFile, 5 );
			//if( pfiCallback )
			//{
			//	pfiCallback->Callback( pdpFeats[iFeat1].iIndex, iFeat1, pdpFeats[iFeat1].iValue );
			//}
		}
	}

	//printf( "FlagInvalidFeatures(): done, freeing memory\n" );

	//printf( "Valid features: %d\n", iValidFeatures );

	// The result of this code block is strickly for output/fun
	delete [] piImageFlags;
	delete [] pdpFeats;
	delete [] piFeatsToRemove;

	return 0;
}

int
SetAppearanceThresholds_f_given_s_labels(
	vector<char*> &vecImgName, // Array of image names
	vector<int> &vecFeatToImgName, // Maps each Feat to its image name
	vector<MODEL_FEATURE> &vecFeats, // Array of features
	vector<IndexInfo> &vecIndexInfo, // IndexInfo
	vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	vector<int> &vecFeatValid, // For each Feat, valid flags

	vector<int> &vecImgLabels, // For each image, a label

	vector<float> &vecFeatAppearanceThresholds, // For each Feat, threshold
	float fMinLikelihoodRatio,
	int bSetMaxLikelhoodRatio
	)
{
	if( vecFeatValid.size() != vecFeats.size() )
	{
		assert( vecFeatAppearanceThresholds.size() == 0 );
		vecFeatValid.resize( vecFeats.size(), 0 );
	}
	if( vecFeatAppearanceThresholds.size() != vecFeats.size() )
	{
		// Allocate thresholds, set ot zero
		assert( vecFeatAppearanceThresholds.size() == 0 );
		vecFeatAppearanceThresholds.resize( vecFeats.size(), 0.0f );
	}
	int iValidThresholds = 0;

	// Keep track of multiple non-mirror matches to the same
	// images - bad, especially for cortex.
	int *piImageLabels = new int[vecImgName.size()]; 
	assert( piImageLabels );
	if( !piImageLabels )
	{
		return -1;
	}

	for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	{
		if( vecFeatValid[iFeat] == 0 )
		{
			// Don't consider invalid features
			vecFeatAppearanceThresholds[iFeat] = 0.0f;
			continue;
		}

		// Retreive label for this feature
		MODEL_FEATURE &feat = vecFeats[iFeat];
		int iFeatImg = vecFeatToImgName[iFeat];
		int iFeatLabel = vecImgLabels[iFeatImg];

		// Set total positives and total negatives
		//float fTotalPositives = vecImgName.size();
		//float fTotalNegatives = 0;
		//for( int i = 1; i < vecFeatToIndexInfoCount[iFeat]; i++ )
		//{
		//	IndexInfo &ii = vecIndexInfo[ vecFeatToIndexInfo[iFeat] + i ];
		//	fTotalNegatives += ii.DistSqrLessThanNeg();
		//}

#define ACCUM_PRIOR_POS 1
#define ACCUM_PRIOR_NEG 1

		int bThresholdSet = 0;
		float fThresLikePos = 0;
		float fThresLikeNeg = 0;
		float fThresDistSqr = -1;

		// Add a prior of 1, so there are no zero probs
		float fAccumNeg = ACCUM_PRIOR_NEG;

		memset( piImageLabels, 0, sizeof(int)*vecImgName.size() );
		int iDuplicateImages = 0;

		FILE *outfile;

		float fMaxRatio = 0;
		float fMaxPos = 0;
		float fMaxNeg = 0;
		float fMaxThresDistSqr = -1;

		// Start at index 1 (index 0 is the feature itself)
		for( int i = 1; i < vecFeatToIndexInfoCount[iFeat]; i++ )
		{
			IndexInfo &ii = vecIndexInfo[ vecFeatToIndexInfo[iFeat] + i ];

			int iInfo = ii.Info();
			int iFeat2 = ii.Index();

			if( vecFeatValid[iFeat2] == 0 )
			{
				// Don't consider invalid features in positive count
				// Just maintain negative count
				fAccumNeg += ii.DistSqrLessThanNeg();
				continue;
			}

			// Get class label for this feature
			int iFeat2Img = vecFeatToImgName[iFeat2];
			int iFeat2Label = vecImgLabels[iFeat2Img];
			if( iFeat2Label != iFeatLabel )
			{
				// Consider opposite labels as a negative example
				// and maintain negative count
				fAccumNeg += ii.DistSqrLessThanNeg() + 1;
				continue;
			}

			int iIndexImg = vecFeatToImgName[ iFeat2 ];
			int iBit = 1<<iInfo;
			if( (piImageLabels[iIndexImg] & iBit) != 0 )
			{
				// Duplicate match: consider as a negative match
				// 3:59 PM 7/28/2009
				// This improves AD/NC classification by a small amount
				// 
				iDuplicateImages++;
				fAccumNeg += ii.DistSqrLessThanNeg();
				continue;
			}
			piImageLabels[iIndexImg] |= iBit;

			fAccumNeg += ii.DistSqrLessThanNeg();
			float fAccumPos = i - iDuplicateImages + ACCUM_PRIOR_POS; // 'i' is the number of positive examples
	
			float fLikePos = fAccumPos;// / fTotalPositives; // 11:41 AM 11/28/2007 (why consider totals?)
			float fLikeNeg = fAccumNeg;// / fTotalNegatives; // 11:41 AM 11/28/2007 (this was changed very near the end of my doc)

			if( fLikePos/fLikeNeg >= fMinLikelihoodRatio )
			{
				bThresholdSet = 1;
				fThresDistSqr = ii.DistSqr();
				fThresLikePos = fLikePos;
				fThresLikeNeg = fLikeNeg;

				// Added in 2008
				// Consider keeping the max likelihood ratio?
				if( fLikePos/fLikeNeg > fMaxRatio )
				{
					fMaxRatio = fLikePos/fLikeNeg;
					fMaxPos = fLikePos;
					fMaxNeg = fLikeNeg;
					fMaxThresDistSqr = ii.DistSqr();
				}
			}
		}

		if( bThresholdSet )
		{
			iValidThresholds++;
			if( bSetMaxLikelhoodRatio )
			{
				vecFeatAppearanceThresholds[iFeat] = fMaxThresDistSqr;
			}
			else
			{
				vecFeatAppearanceThresholds[iFeat] = fThresDistSqr;
			}
		}
		else
		{
			vecFeatAppearanceThresholds[iFeat] = 0.0f;
		}
	}

	delete [] piImageLabels;

	return iValidThresholds;
}

int
SetAppearanceThresholds_f_given_s_labels_new(
	vector<char*> &vecImgName, // Array of image names
	vector<int> &vecFeatToImgName, // Maps each Feat to its image name
	vector<MODEL_FEATURE> &vecFeats, // Array of features
	vector<IndexInfo> &vecIndexInfo, // IndexInfo
	vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	vector<int> &vecFeatValid, // For each Feat, valid flags

	vector<int> &vecImgLabels, // For each image, a label

	vector<float> &vecFeatAppearanceThresholds, // For each Feat, threshold
	float fMinLikelihoodRatio,
	int bSetMaxLikelhoodRatio
	)
{
	if( vecFeatValid.size() != vecFeats.size() )
	{
		assert( vecFeatAppearanceThresholds.size() == 0 );
		vecFeatValid.resize( vecFeats.size(), 0 );
	}
	if( vecFeatAppearanceThresholds.size() != vecFeats.size() )
	{
		// Allocate thresholds, set ot zero
		assert( vecFeatAppearanceThresholds.size() == 0 );
		vecFeatAppearanceThresholds.resize( vecFeats.size(), 0.0f );
	}
	int iValidThresholds = 0;

	// Keep track of multiple non-mirror matches to the same
	// images - bad, especially for cortex.
	int *piImageLabels = new int[vecImgName.size()]; 
	assert( piImageLabels );
	if( !piImageLabels )
	{
		return -1;
	}

	for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	{
		if( vecFeatValid[iFeat] == 0 )
		{
			// Don't consider invalid features
			vecFeatAppearanceThresholds[iFeat] = 0.0f;
			continue;
		}

		// Retreive label for this feature
		MODEL_FEATURE &feat = vecFeats[iFeat];
		int iFeatImg = vecFeatToImgName[iFeat];
		int iFeatLabel = vecImgLabels[iFeatImg];

#define ACCUM_PRIOR_POS 1
#define ACCUM_PRIOR_NEG 1

		int bThresholdSet = 0;
		float fThresLikePos = 0;
		float fThresLikeNeg = 0;
		float fThresDistSqr = -1;

		// Add a prior of 1, so there are no zero probs
		float fAccumNeg = ACCUM_PRIOR_NEG;

		memset( piImageLabels, 0, sizeof(int)*vecImgName.size() );
		int iDuplicateImages = 0;

		float fMaxRatio = 0;
		float fMaxPos = 0;
		float fMaxNeg = 0;
		float fMaxThresDistSqr = -1;

		// Keep track of the number of classes
		int piAccumClassCount[2]; // 0: opposite, 1: same
		memset( piAccumClassCount, 0, sizeof(piAccumClassCount) );
		piAccumClassCount[1] = 1; // Start with self - same

		// Start at index 1 (index 0 is the feature itself)
		for( int i = 1; i < vecFeatToIndexInfoCount[iFeat]; i++ )
		{
			IndexInfo &ii = vecIndexInfo[ vecFeatToIndexInfo[iFeat] + i ];
			if( ii.DistSqrLessThanNeg() >= 99999999 )
			{
				// Code to break out - we've reached 2nd nearest neighbour in same image
				break;
			}

			//if( piAccumClassCount[1] < piAccumClassCount[0] )
			//{
			//	// Break out if opposite labels are more than same.
			//	// This didn't work at all....
			//	break;
			//}

			int iInfo = ii.Info();
			int iFeat2 = ii.Index();

			if( vecFeatValid[iFeat2] == 0 )
			{
				// Don't consider invalid features in positive count
				// Just maintain negative count
				fAccumNeg += ii.DistSqrLessThanNeg();
				continue;
			}

			// Get class label for this feature
			int iFeat2Img = vecFeatToImgName[iFeat2];
			int iFeat2Label = vecImgLabels[iFeat2Img];
			if( iFeat2Label != iFeatLabel )
			{
				// Consider opposite labels as a negative example
				// and maintain negative count
				// fAccumNeg += ii.DistSqrLessThanNeg() + 1;
				// continue;
				piAccumClassCount[0] += 1; // Increment different label count
			}
			else
			{
				piAccumClassCount[1] += 1; // Increment same label count
			}

			int iIndexImg = vecFeatToImgName[ iFeat2 ];
			int iBit = 1<<iInfo;
			if( (piImageLabels[iIndexImg] & iBit) != 0 )
			{
				// Duplicate match: consider as a negative match
				// 3:59 PM 7/28/2009
				// This improves AD/NC classification by a small amount
				// 
				iDuplicateImages++;
				fAccumNeg += ii.DistSqrLessThanNeg();
				continue;
			}
			piImageLabels[iIndexImg] |= iBit;

			fAccumNeg += ii.DistSqrLessThanNeg();
			float fAccumPos = i - iDuplicateImages + ACCUM_PRIOR_POS; // 'i' is the number of positive examples

			float fClassRate = ((float)piAccumClassCount[1]+1.0f)/((float)piAccumClassCount[0]+1.0f);
	
			float fLikePos = fAccumPos;// / fTotalPositives; // 11:41 AM 11/28/2007 (why consider totals?)
			float fLikeNeg = fAccumNeg;// / fTotalNegatives; // 11:41 AM 11/28/2007 (this was changed very near the end of my doc)

			if( fLikePos*fClassRate/fLikeNeg >= fMinLikelihoodRatio )
			{
				bThresholdSet = 1;
				fThresDistSqr = ii.DistSqr();
				fThresLikePos = fLikePos;
				fThresLikeNeg = fLikeNeg;

				// Added in 2008
				// Consider keeping the max likelihood ratio?
				if( fLikePos/fLikeNeg > fMaxRatio )
				{
					fMaxRatio = fLikePos/fLikeNeg;
					fMaxPos = fLikePos;
					fMaxNeg = fLikeNeg;
					fMaxThresDistSqr = ii.DistSqr();
				}
			}
		}

		bSetMaxLikelhoodRatio = 1;

		if( bThresholdSet )
		{
			iValidThresholds++;
			if( bSetMaxLikelhoodRatio )
			{
				vecFeatAppearanceThresholds[iFeat] = fMaxThresDistSqr;
			}
			else
			{
				vecFeatAppearanceThresholds[iFeat] = fThresDistSqr;
			}
		}
		else
		{
			vecFeatAppearanceThresholds[iFeat] = 0.0f;
		}
	}

	delete [] piImageLabels;

	return iValidThresholds;
}


int
SetAppearanceThresholds_f_given_s_labels_new2(
	vector<char*> &vecImgName, // Array of image names
	vector<int> &vecFeatToImgName, // Maps each Feat to its image name
	vector<MODEL_FEATURE> &vecFeats, // Array of features
	vector<IndexInfo> &vecIndexInfo, // IndexInfo
	vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	vector<int> &vecFeatValid, // For each Feat, valid flags

	vector<int> &vecImgLabels, // For each image, a label

	vector<float> &vecFeatAppearanceThresholds, // For each Feat, threshold
	float fMinLikelihoodRatio,
	int bSetMaxLikelhoodRatio
	)
{
	if( vecFeatValid.size() != vecFeats.size() )
	{
		assert( vecFeatAppearanceThresholds.size() == 0 );
		vecFeatValid.resize( vecFeats.size(), 0 );
	}
	if( vecFeatAppearanceThresholds.size() != vecFeats.size() )
	{
		// Allocate thresholds, set ot zero
		assert( vecFeatAppearanceThresholds.size() == 0 );
		vecFeatAppearanceThresholds.resize( vecFeats.size(), 0.0f );
	}
	int iValidThresholds = 0;

	// Keep track of multiple non-mirror matches to the same
	// images - bad, especially for cortex.
	int *piImageLabels = new int[vecImgName.size()]; 
	assert( piImageLabels );
	if( !piImageLabels )
	{
		return -1;
	}

	vector< float > vecRatios;
	vector< float > vecDists;

	for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	{
		if( vecFeatValid[iFeat] == 0 )
		{
			// Don't consider invalid features
			vecFeatAppearanceThresholds[iFeat] = 0.0f;
			continue;
		}

		// Retreive label for this feature
		MODEL_FEATURE &feat = vecFeats[iFeat];
		int iFeatImg = vecFeatToImgName[iFeat];
		int iFeatLabel = vecImgLabels[iFeatImg];

		// Set total positives and total negatives
		//float fTotalPositives = vecImgName.size();
		//float fTotalNegatives = 0;
		//for( int i = 1; i < vecFeatToIndexInfoCount[iFeat]; i++ )
		//{
		//	IndexInfo &ii = vecIndexInfo[ vecFeatToIndexInfo[iFeat] + i ];
		//	fTotalNegatives += ii.DistSqrLessThanNeg();
		//}

#define ACCUM_PRIOR_POS 1
#define ACCUM_PRIOR_NEG 1

		int bThresholdSet = 0;
		float fThresLikePos = 0;
		float fThresLikeNeg = 0;
		float fThresDistSqr = -1;
		int iRatioII = -1;

		// Add a prior of 1, so there are no zero probs
		float fAccumNeg = ACCUM_PRIOR_NEG;
		float fAccumPos = ACCUM_PRIOR_POS; // Start with one positive count for this feature itself

		memset( piImageLabels, 0, sizeof(int)*vecImgName.size() );
		int iDuplicateImages = 0;

		FILE *outfile;

		float fMaxRatio = 0;
		float fMaxPos = 0;
		float fMaxNeg = 0;
		float fMaxThresDistSqr = -1;
		int iMaxRatioII = -1;

		vecRatios.clear();
		vecDists.clear();

		int iOppositeLabels = 0;
		int iInvalidFeats = 0;

		// Keep track of the number of classes
		int piAccumClassCount[2]; // 0: opposite, 1: same
		memset( piAccumClassCount, 0, sizeof(piAccumClassCount) );
		piAccumClassCount[1] = 1; // Start with self - same

		// Identify peak
		for( int i = 1; i < vecFeatToIndexInfoCount[iFeat]; i++ )
		{
			IndexInfo &ii = vecIndexInfo[ vecFeatToIndexInfo[iFeat] + i ];

			int iInfo = ii.Info();
			int iFeat2 = ii.Index();

			if( vecFeatValid[iFeat2] == 0 )
			{
				// Don't consider invalid features in positive count
				// Just maintain negative count
				iInvalidFeats++;
				fAccumNeg += ii.DistSqrLessThanNeg();
			}
			else
			{
				int iIndexImg = vecFeatToImgName[ iFeat2 ];
				int iBit = 1<<iInfo;
				if( (piImageLabels[iIndexImg] & iBit) != 0 )
				{
					// Duplicate match: consider as a negative match
					// 3:59 PM 7/28/2009
					// This improves AD/NC classification by a small amount
					// 
					iDuplicateImages++;
					fAccumNeg += 1;
					fAccumNeg += ii.DistSqrLessThanNeg();
				}
				else
				{
					// Get class label for this feature
					int iFeat2Img = vecFeatToImgName[iFeat2];
					int iFeat2Label = vecImgLabels[iFeat2Img];
					if( iFeat2Label != iFeatLabel )
					{
						// Consider opposite labels as a negative example
						// and maintain negative count
						iOppositeLabels++;
						fAccumNeg += 1;
						//fAccumPos += 1; // Consider as the same feature
						fAccumNeg += ii.DistSqrLessThanNeg();
						piAccumClassCount[0] += 1; // Increment different label count
					}
					else
					{
						// This sample is valid, of same label, increment positive count
						fAccumPos += 1;
						fAccumNeg += ii.DistSqrLessThanNeg();
						piAccumClassCount[1] += 1; // Increment same label count
					}
				}
				piImageLabels[iIndexImg] |= iBit;
			}
	
			float fLikePos = fAccumPos;// / fTotalPositives; // 11:41 AM 11/28/2007 (why consider totals?)
			float fLikeNeg = fAccumNeg;// / fTotalNegatives; // 11:41 AM 11/28/2007 (this was changed very near the end of my doc)

			float fClassRate = ((float)piAccumClassCount[1]+1.0f)/((float)piAccumClassCount[0]+1.0f);

			float fRatio = fLikePos/fLikeNeg;

			// Save ratios
			vecRatios.push_back( fRatio );
			vecDists.push_back( ii.DistSqr() );

			if( fRatio >= fMinLikelihoodRatio )
			{
				bThresholdSet = 1;
				fThresDistSqr = ii.DistSqr();
				fThresLikePos = fLikePos;
				fThresLikeNeg = fLikeNeg;
				iRatioII = i-1;

				// Added in 2008
				// Consider keeping the max likelihood ratio?
				if( fRatio > fMaxRatio )
				{
					fMaxRatio = fRatio;
					fMaxPos = fLikePos;
					fMaxNeg = fLikeNeg;
					fMaxThresDistSqr = ii.DistSqr();
					iMaxRatioII = i-1;
				}
			}
		}

		if( bThresholdSet )
		{
			// Find first dip below fMinLikelihoodRatio after max
			float fThresDistSqrFirstDip = vecRatios[iMaxRatioII];
			for( int i = iMaxRatioII; i < vecRatios.size(); i++ )
			{
				float fNextThresh = vecRatios[i];
				if( vecRatios[i] >= fMinLikelihoodRatio )
				{
					fThresDistSqrFirstDip = vecDists[i];
				}
				else
				{					
					break;
				}
			}

			if( fThresDistSqrFirstDip != fThresDistSqr )
			{
				//fThresDistSqr = fThresDistSqrFirstDip;
			}

			iValidThresholds++;
			if( bSetMaxLikelhoodRatio )
			{
				vecFeatAppearanceThresholds[iFeat] = fMaxThresDistSqr;
			}
			else
			{
				vecFeatAppearanceThresholds[iFeat] = fThresDistSqr;
			}
		}
		else
		{
			vecFeatAppearanceThresholds[iFeat] = 0.0f;
		}
	}

	delete [] piImageLabels;

	return iValidThresholds;
}

int
SetAppearanceThresholds_f_given_s_fixed(
	vector<char*> &vecImgName, // Array of image names
	vector<int> &vecFeatToImgName, // Maps each Feat to its image name
	vector<MODEL_FEATURE> &vecFeats, // Array of features
	vector<IndexInfo> &vecIndexInfo, // IndexInfo
	vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	vector<int> &vecFeatValid, // For each Feat, valid flags

	vector<int> &vecImgLabels, // For each image, a label

	vector<float> &vecFeatAppearanceThresholds, // For each Feat, threshold
	float fMinLikelihoodRatio,
	int bSetMaxLikelhoodRatio
	)
{
	if( vecFeatAppearanceThresholds.size() != vecFeats.size() )
	{
		// Allocate thresholds, set ot zero
		assert( vecFeatAppearanceThresholds.size() == 0 );
		vecFeatAppearanceThresholds.resize( vecFeats.size(), 0.0f );
	}
	int iValidThresholds = 0;
	for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	{
		// Set to a quarter of the maximum distance squared
		float fDist = (32767.0*0.5);
		vecFeatAppearanceThresholds[iFeat] = fDist*fDist;
	}
	return 1;
}



int
SetAppearanceThresholds_f_given_s_lowesterror(
	vector<char*> &vecImgName, // Array of image names
	vector<int> &vecFeatToImgName, // Maps each Feat to its image name
	vector<MODEL_FEATURE> &vecFeats, // Array of features
	vector<IndexInfo> &vecIndexInfo, // IndexInfo
	vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	vector<int> &vecFeatValid, // For each Feat, valid flags

	vector<int> &vecImgLabels, // For each image, a label

	vector<float> &vecFeatAppearanceThresholds, // For each Feat, threshold
	float fMinLikelihoodRatio,
	int bSetMaxLikelhoodRatio
	)
{
	if( vecFeatAppearanceThresholds.size() != vecFeats.size() )
	{
		// Allocate thresholds, set ot zero
		assert( vecFeatAppearanceThresholds.size() == 0 );
		vecFeatAppearanceThresholds.resize( vecFeats.size(), 0.0f );
	}

	int iValidThresholds = 0;
	
	// Keep track of multiple non-mirror matches to the same
	// images - bad, especially for cortex.
	int *piImageLabels = new int[vecImgName.size()]; 
	assert( piImageLabels );
	if( !piImageLabels )
	{
		return -1;
	}

	ClassifyGeometrySXYZ cg;

	for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	{
		if( vecFeatValid[iFeat] == 0 )
		{
			// Don't consider invalid features
			vecFeatAppearanceThresholds[iFeat] = -1.0f;
			continue;
		}
		else
		{			
			memset( piImageLabels, 0, sizeof(int)*vecImgName.size() );
			int iDuplicateImages = 0;

			cg.Clear();

			// Retreive label for this feature
			MODEL_FEATURE &feat = vecFeats[iFeat];
			int iFeatImg = vecFeatToImgName[iFeat];
			int iFeatLabel = vecImgLabels[iFeatImg];

			// Add samples to classifier
			for( int i = 0; i < vecFeatToIndexInfoCount[iFeat]; i++ )
			{
				IndexInfo &ii = vecIndexInfo[ vecFeatToIndexInfo[iFeat] + i ];

				int iInfo = ii.Info();
				int iFeat2 = ii.Index();
				MODEL_FEATURE &feat2 = vecFeats[iFeat2];
				int iFeat2Img = vecFeatToImgName[iFeat2];
				int iLabel2 = vecImgLabels[iFeat2Img];

				if( vecFeatValid[iFeat2] == 0 )
				{
					// Don't consider invalid features
				}
				else
				{
					int iIndexImg = vecFeatToImgName[ iFeat2 ];
					int iBit = 1<<iInfo;
					if( (piImageLabels[iIndexImg] & iBit) != 0 )
					{
						// Duplicate match: do not consider this feature
						iDuplicateImages++;
					}
					else
					{
						// Get class label for this feature
						cg.AddSample( iLabel2, ii.DistSqr(), feat2.x, feat2.y, feat2.z );
					}
					piImageLabels[iIndexImg] |= iBit;
				}
			}
			vecFeatAppearanceThresholds[iFeat] = cg.ComputeOptimalClassificationThreshold();
		}
	}

	delete [] piImageLabels;

	return 1;
}


//
// FlagInvalidFeatures_labels()
//
// This function clusters features, those with opposite labels are considered negative examples.
//
int
FlagInvalidFeatures_labels(
	vector<char*>		&vecImgName, // Array of image names
	vector<int>			&vecFeatToImgName, // Maps each Feat to its image name
	vector<MODEL_FEATURE>	&vecFeats, // Array of features
	vector<IndexInfo>	&vecIndexInfo, // IndexInfo
	vector<int>			&vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int>			&vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	vector<int>			&vecFeatValid,	// For each Feat, is this feature valid?
	vector<float>		&vecFeatAppearanceThresholds, // For each Feat, appearance thresholds

	vector<int> &vecImgLabels, // For each image, a label

	vector<int>			&vecOrderToFeat,	// Feature indices, sorted from most to least frequent
	int iMinMatches
	//FeatureInstanceCallback *pfiCallback
)
{
	// A peak feature is one which contains as many or more positive matches
	// within it's threshold than any of it's positive matches.
	// Keep the peak feature, discard all features corresponding to it's 
	// positive matches.

	if( vecFeatValid.size() < vecFeats.size() )
	{
		// Vector of valid flags must be zero, flag all as valid
		assert( vecFeatValid.size() == 0 );
		vecFeatValid.resize( vecFeats.size(), 1 );
	}

	int *piFeatsToRemove = new int[vecFeats.size()];
	assert( piFeatsToRemove );

	int *piImageFlags = new int[vecImgName.size()];
	assert( piImageFlags );

	DATA_PAIR *pdpFeats = new DATA_PAIR[vecFeats.size()];
	assert( pdpFeats );

	if( piFeatsToRemove == 0 || piImageFlags == 0 || pdpFeats == 0 )
	{
		printf( "Error: FlagInvalidFeatures() - could not allocate memory.\n" );
		return -1;
	}

	memset( piFeatsToRemove, 0,       sizeof(int)*vecFeats.size() );
	memset( pdpFeats,        0, sizeof(DATA_PAIR)*vecFeats.size() );

	//printf( "FlagInvalidFeatures(): allocated memory.\n" );

	int iRemovers = 0;

	int iF1Samples;
	int iF2Samples;

	for( int iFeat1 = 0; iFeat1 < vecFeats.size(); iFeat1++ )
	{
		int bPeakFeat = 0;
		iF2Samples = 0;

		if( vecFeatValid[iFeat1] == 0 )
		{
			// Remove all features flagged as invalid
			bPeakFeat = 0;
			iF2Samples = 0;
		}
		else
		{
			bPeakFeat = 1;
			iF2Samples = 0;

			MODEL_FEATURE &feat1 = vecFeats[iFeat1];
			iF1Samples = ValidFeaturesWithinThreshold_labels( iFeat1,
				vecImgName, vecFeatToImgName, vecFeats,
				vecIndexInfo, vecFeatToIndexInfo, vecFeatToIndexInfoCount,
				vecFeatAppearanceThresholds, vecFeatValid,
				vecImgLabels,
				piImageFlags);

			// Test all features within threshold of Feat1. If one of the
			// features within the threshold has more features within it's
			// threshold, then Feat1 is not a peak features
			for( int i1 = 1; i1 < vecFeatToIndexInfoCount[iFeat1] && bPeakFeat; i1++ )
			{
				IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat1]+i1];

				if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat1] )
				{
					// II1 is a valid instance of feat1
					int iFeat2 = II1.Index();
					 
					iF2Samples = ValidFeaturesWithinThreshold_labels( iFeat2,
						vecImgName, vecFeatToImgName, vecFeats,
						vecIndexInfo, vecFeatToIndexInfo, vecFeatToIndexInfoCount,
						vecFeatAppearanceThresholds, vecFeatValid,
						vecImgLabels,
						piImageFlags);

					//
					// 4:10 AM 11/21/2008 
					// Hmm - according my phd code here: 
					//     if( iF2Samples > iF1Samples )
					// if there was a tight cluster of features which were all instances
					// of each other, then  (iF2Samples == iF1Samples) and I was not
					// pruning any of them!!
					//
					// Trouble here is, what do with a tight cluster of features
					// which has exactly equal numbers? Obviously I should keep one,
					// let's say the first one. How to test if they're the same??
					//
					//

					if( iF2Samples > iF1Samples )
					{
						bPeakFeat = 0;
					}
					else if( iF2Samples == iF1Samples )
					{
						// Arbitrarily keep the feature with the highest index
						// This keeps one features from a tight cluster,
						// which makes detection more computationally efficient but appears to 
						// reduce the quality of the scores?
						if( II1.Index() < iFeat1 )
						{
							bPeakFeat = 0;
						}
					}
				}
				else
				{
					break;
				}
			}
		}

		if( !bPeakFeat || iF1Samples == 0 )
		{
			iRemovers++;
			piFeatsToRemove[iFeat1] = 0;
		}
		else
		{
			piFeatsToRemove[iFeat1] = iF1Samples;
		}
	}

	//printf( "FlagInvalidFeatures(): scanned all features\n" );

	//FILE *outfile = fopen( "feats.txt", "wt" );
	//for( int iFeat1 = 0; iFeat1 < vecFeats.size(); iFeat1++ )
	//{
	//	fprintf( outfile, "%d\t%d\n", iFeat1, piFeatsToRemove[iFeat1] );
	//}
	//fclose( outfile );

	//printf( "FlagInvalidFeatures(): outputted feats.txt\n" );

	int iValidFeatures = 0;
	// Here we remove all features flagged
	for( int iFeat1 = 0; iFeat1 < vecFeats.size(); iFeat1++ )
	{
		pdpFeats[iFeat1].iIndex = iFeat1;
		pdpFeats[iFeat1].iValue = piFeatsToRemove[iFeat1];
		if( piFeatsToRemove[iFeat1] < iMinMatches )
		{
			// Flag redundant features as invalid
			vecFeatValid[iFeat1] = 0;
		}
		else
		{
			// Flag valid feature
			vecFeatValid[iFeat1] = 1;
			iValidFeatures++;
		}
	}

	// Sort in descending order of occurrence frequency
	qsort( pdpFeats, vecFeats.size(), sizeof(DATA_PAIR), sort_DATA_PAIR );

	vecOrderToFeat.clear();

	// Process all non-zero features in descending order
	for( int iFeat1 = 0; pdpFeats[iFeat1].iValue > 0; iFeat1++ )
	{
		if( vecFeatValid[pdpFeats[iFeat1].iIndex] > 0 )
		{
			// Store order information
			vecOrderToFeat.push_back( pdpFeats[iFeat1].iIndex );

			char pcFile[200];
			sprintf( pcFile, "feat_%4.4d_matches_%3.3d_", pdpFeats[iFeat1].iIndex, pdpFeats[iFeat1].iValue );
			MODEL_FEATURE &feat1 = vecFeats[ pdpFeats[iFeat1].iIndex ];
			//iF1Samples = FeaturesWithinThreshold(feat1,piImageFlags);
			//OutputModelPointAndInstances( pdpFeats[iFeat1].iIndex, pcFile, 5 );
			//if( pfiCallback )
			//{
			//	pfiCallback->Callback( pdpFeats[iFeat1].iIndex, iFeat1, pdpFeats[iFeat1].iValue );
			//}
		}
	}

	//printf( "FlagInvalidFeatures(): done, freeing memory\n" );

	//printf( "Valid features: %d\n", iValidFeatures );

	// The result of this code block is strickly for output/fun
	delete [] piImageFlags;
	delete [] pdpFeats;
	delete [] piFeatsToRemove;

	return 0;
}

template<class TYPE> int
vector_write_binary(
				 FILE *outfile, //"wb"
				 vector<TYPE> &vec
					 )
{
	int iReturn = 0;
	for( int i = 0; i < vec.size(); i++ )
	{
		TYPE &data = vec[i];
		iReturn += fwrite( &data, sizeof(TYPE), 1, outfile );
	}
	return iReturn;
}

int
vector_write_model_feature_binary(
				 FILE *outfile, //"wb"
				 vector<MODEL_FEATURE> &vec,
				 int bAppearance = 0
					 )
{
	int iReturn = 0;
	iReturn = sizeof(MODEL_FEATURE);
	for( int i = 0; i < vec.size(); i++ )
	{
		MODEL_FEATURE &data = vec[i];

		//iReturn += fwrite( &data, sizeof(TYPE), 1, outfile );

		int iBytes = 0;
		iBytes += fwrite( &data.m_uiInfo,		sizeof(data.m_uiInfo), 1, outfile );
		iBytes += fwrite( &data.x,		sizeof(data.x), 1, outfile );
		iBytes += fwrite( &data.y,		sizeof(data.y), 1, outfile );
		iBytes += fwrite( &data.z,		sizeof(data.z), 1, outfile );
		iBytes += fwrite( &data.scale, sizeof(data.scale), 1, outfile );
		iBytes += fwrite( &data.ori,	sizeof(data.ori), 1, outfile );
		iBytes += fwrite( &data.eigs,	sizeof(data.eigs), 1, outfile );
		iBytes += fwrite( &data.m_pfPC,	sizeof(data.m_pfPC), 1, outfile );
		if( bAppearance )
		{
			//iBytes += fwrite( &data.data_zyx,	sizeof(data.data_zyx), 1, outfile );
		}

		iBytes++;

	}
	return iReturn;
}

template<class TYPE> int
vector_read_binary(
				 FILE *infile, //"rb"
				 vector<TYPE> &vec
					 )
{
	int iReturn = 0;
	for( int i = 0; i < vec.size(); i++ )
	{
		TYPE &data = vec[i];
		iReturn += fread( &data, sizeof(TYPE), 1, infile );		
	}
	return iReturn;
}


//int
//ModelFeature3D::ReplaceFeatures(
//		char *pcFileName
//	)
//{
//	ModelFeature3D model_other;
//	
//	// Must be able to open other file
//	if( model_other.Read( pcFileName ) != 0 )
//	{
//		return -1;
//	}
//
//	// Size of feature vectors must be identical
//	if( model_other.vecFeats.size() != vecFeats.size() )
//	{
//		return -1;
//	}
//
//	for( int iFeat = 0; iFeat < model_other.vecFeats.size(); iFeat++ )
//	{
//		Feature3D &feat = vecFeats[iFeat];
//		Feature3D &feat_other = model_other.vecFeats[iFeat];
//
//		if( feat.x != feat_other.x ||
//			feat.y != feat_other.y ||
//			feat.z != feat_other.z ||
//			feat.scale != feat_other.scale )
//		{
//			// Big error
//			return -100;
//		}
//
//		// Copy data
//		for( int z = 0; z < MODEL_FEATURE::FEATURE_3D_DIM; z++ )
//		{
//			for( int y = 0; y < MODEL_FEATURE::FEATURE_3D_DIM; y++ )
//			{
//				for( int x = 0; x < MODEL_FEATURE::FEATURE_3D_DIM; x++ )
//				{
//					feat.data_zyx[z][y][x] = feat_other.data_zyx[z][y][x];
//				}
//			}
//		}
//	}
//
//	return 0;
//}

int
ModelFeature3D::Read(
		char *pcFileName
	)
{
	FILE *infile;
	char pcFName[400];

	// Output array sizes
	sprintf( pcFName, "%s.info", pcFileName );
	infile = fopen( pcFName, "rt" );
	if( !infile )
	{
		return -1;
	}

	int iSize;
	fscanf( infile, "%d\n", &iSize ); vecFeatToImgName.resize(iSize);
	fscanf( infile, "%d\n", &iSize ); vecFeatToIndexInfo.resize(iSize);
	fscanf( infile, "%d\n", &iSize ); vecFeatToIndexInfoCount.resize(iSize);
	fscanf( infile, "%d\n", &iSize ); vecFeatValid.resize(iSize);
	fscanf( infile, "%d\n", &iSize ); vecOrderToFeat.resize(iSize);
	fscanf( infile, "%d\n", &iSize ); vecIndexInfo.resize(iSize);
	fscanf( infile, "%d\n", &iSize ); vecFeatAppearanceThresholds.resize(iSize);
	fscanf( infile, "%d\n", &iSize ); vecFeats.resize(iSize);
	// Read in atlas space dimensions
	int iGotDim;
	fscanf( infile, "%d\n", &m_piModelImageSizeXYZ[0] );
	fscanf( infile, "%d\n", &m_piModelImageSizeXYZ[1] );
	iGotDim = fscanf( infile, "%d\n", &m_piModelImageSizeXYZ[2] );
	if( iGotDim <= 0 )
	{
		m_piModelImageSizeXYZ[0] = 100;
		m_piModelImageSizeXYZ[1] = 100;
		m_piModelImageSizeXYZ[2] = 100;
	}
	fclose( infile );

	//vector<Feature3D> vecTestFloat;
	//vecTestFloat.resize(iSize);

	sprintf( pcFName, "%s.data", pcFileName );
	infile = fopen( pcFName, "rb" );
	if( infile )
	{
		vector_read_binary<int>( infile, vecFeatToImgName ); // int
		vector_read_binary<int>( infile, vecFeatToIndexInfo ); // int
		vector_read_binary<int>( infile, vecFeatToIndexInfoCount ); // int
		vector_read_binary<int>( infile, vecFeatValid ); // int
		vector_read_binary<int>( infile, vecOrderToFeat ); // int
		vector_read_binary<IndexInfo>( infile, vecIndexInfo ); // IndexInfo
		vector_read_binary<float>( infile, vecFeatAppearanceThresholds ); // float
		vector_read_binary<MODEL_FEATURE>( infile, vecFeats ); // MODEL_FEATURE
		//vector_read_binary<Feature3D>( infile, vecTestFloat ); // MODEL_FEATURE

			int iSizeF3D__000 = 5392;// (info+data), no longer supported


		fclose( infile );
	}
	else
	{
		return -1;
	}

	sprintf( pcFName, "%s.images", pcFileName );
	if( tfNames.Read( pcFName ) == 0 )
	{
		for( int i = 0; i < tfNames.Lines(); i++ )
		{
			// Each image file name should be greater than 5?
			if( strlen( tfNames.GetLine(i) ) > 5 )
			{
				char *pch = tfNames.GetLine(i);
				vecImgName.push_back( pch );
			}
		}
	}
	else
	{
		return -2;
	}

	// Attempt to read in a label files
	sprintf( pcFName, "%s.labels", pcFileName );
	ReadDataInteger( pcFName, tfLabels, vecImgLabels );

	sprintf( pcFName, "%s.age", pcFileName );
	ReadDataFloat( pcFName, tfAge, vecImgAge );

	sprintf( pcFName, "%s.size", pcFileName );
	ReadDataFloat( pcFName, tfSize, vecImgSize );

	sprintf( pcFName, "%s.sex", pcFileName );
	ReadDataInteger( pcFName, tfSex, vecImgSex );

	return 0;
}

int
ModelFeature3D::Write(
		char *pcFileName
	)
{
	FILE *outfile;
	char pcFName[400];

	// Output array sizes
	sprintf( pcFName, "%s.info", pcFileName );
	outfile = fopen( pcFName, "wt" );
	fprintf( outfile, "%d\n", vecFeatToImgName.size() );
	fprintf( outfile, "%d\n", vecFeatToIndexInfo.size() );
	fprintf( outfile, "%d\n", vecFeatToIndexInfoCount.size() );
	fprintf( outfile, "%d\n", vecFeatValid.size() );
	fprintf( outfile, "%d\n", vecOrderToFeat.size() );
	fprintf( outfile, "%d\n", vecIndexInfo.size() );
	fprintf( outfile, "%d\n", vecFeatAppearanceThresholds.size() );
	fprintf( outfile, "%d\n", vecFeats.size() );

	// Write outatlas space dimensions
	fprintf( outfile, "%d\n", m_piModelImageSizeXYZ[0] );
	fprintf( outfile, "%d\n", m_piModelImageSizeXYZ[1] );
	fprintf( outfile, "%d\n", m_piModelImageSizeXYZ[2] );
	fclose( outfile );

	// Output arrays
	sprintf( pcFName, "%s.data", pcFileName );
	outfile = fopen( pcFName, "wb" );
	if( outfile )
	{
		vector_write_binary<int>( outfile, vecFeatToImgName ); // int
		vector_write_binary<int>( outfile, vecFeatToIndexInfo ); // int
		vector_write_binary<int>( outfile, vecFeatToIndexInfoCount ); // int
		vector_write_binary<int>( outfile, vecFeatValid ); // int
		vector_write_binary<int>( outfile, vecOrderToFeat ); // int
		vector_write_binary<IndexInfo>( outfile, vecIndexInfo ); // IndexInfo
		vector_write_binary<float>( outfile, vecFeatAppearanceThresholds ); // float
		//vector_write_binary<MODEL_FEATURE>( outfile, vecFeats ); // MODEL_FEATURE
		vector_write_model_feature_binary(outfile, vecFeats ); // MODEL_FEATURE
		fclose( outfile );
	}
	else
	{
		return -1;
	}

	// Output image names
	sprintf( pcFName, "%s.images", pcFileName );
	outfile = fopen( pcFName, "wt" );
	if( outfile )
	{
		for( int i = 0; i < vecImgName.size(); i++ )
		{
			fprintf( outfile, "%s\n", vecImgName[i] );
		}
		fclose( outfile );
	}
	else
	{
		return -2;
	}

	return 0;
}

int
ModelFeature3D::ScrambledLabels(
		vector<int> &vecImgLabelsScrambled
	)
{
	vecImgLabelsScrambled.resize( vecImgLabels.size() );
	for( int i = 0; i < vecImgLabels.size(); i++ )
	{
		vecImgLabelsScrambled[i] = vecImgLabels[i];
	}

	// Go through entire array 10 times, scramble	
	for( int i = 0; i < 10; i++ )
	{
		for( int iI1 = 0; iI1 < vecImgLabelsScrambled.size(); iI1++ )
		{
			int iI2 = iI1;
			while( iI1 == iI2 )
			{
				iI2 = (rand()*vecImgLabelsScrambled.size())/(RAND_MAX+1.0);
			}
			assert( iI2 < vecImgLabelsScrambled.size() );
			int iTemp = vecImgLabelsScrambled[iI1];
			vecImgLabelsScrambled[iI1] = vecImgLabelsScrambled[iI2];
			vecImgLabelsScrambled[iI2] = iTemp;
		}
	}
	return 0;
}

int
ModelFeature3D::InvalidateRandomImageTestSet(
		int *piLabelCounts,
		int iLabels,
		int *piPreviousLabels
	)
{
	// Make a list of 
	for( int iLabel = 0; iLabel < iLabels; iLabel++ )
	{
		// Save all images bearing label iLabel
		vector<int> vecSingleLabelImages;
		for( int iImg = 0; iImg < vecImgLabels.size(); iImg++ )
		{
			if( vecImgLabels[iImg] == iLabel )
			{
				if( piPreviousLabels )
				{
					if( piPreviousLabels[iImg] == iLabel )
					{
						vecSingleLabelImages.push_back( iImg );
					}
				}
				else
				{
					vecSingleLabelImages.push_back( iImg );
				}
			}
		}

		int iSize = vecSingleLabelImages.size();

		if( vecSingleLabelImages.size() == 0 )
		{
			// Did not find any labels
			return -1;
		}

		// Now go through and select (piLabelCounts[iLabel]) random images as test images
		for( int iImg = 0; iImg < piLabelCounts[iLabel] && vecSingleLabelImages.size()>0; iImg++ )
		{
			int iRandImgIndex = (rand()*vecSingleLabelImages.size())/(RAND_MAX+1.0);
			assert( iRandImgIndex >= 0 );
			assert( iRandImgIndex < vecSingleLabelImages.size() );
			vecImgLabels[ vecSingleLabelImages[iRandImgIndex] ] = -1; // Invalidate
			vecSingleLabelImages.erase( vecSingleLabelImages.begin() + iRandImgIndex ); // Erase index from selection
		}
	}

	return 0;
}

int
ModelFeature3D::SelectTrainingSetAgeRange(
		int iSubject,
		int iTrainingSubjects
	)
{
	if( vecImgAge.size() < vecImgLabels.size() )
	{
		// Something wrong
		return -1;
	}

	if( iTrainingSubjects > vecImgLabels.size() )
	{
		iTrainingSubjects = vecImgLabels.size();
	}
	vector< pair<int,float> > vecAgeSubject;
	for( int i =0; i < vecImgName.size(); i++ )
	{
		vecAgeSubject.push_back( pair<int,float>( i, vecImgAge[i] ) );
	}
	sort( vecAgeSubject.begin(), vecAgeSubject.end(), pairIntFloatLeastToGreatest );

	// Find rank of iSubject
	int iSubjectRank = -1;
	for( int i =0; i < vecImgName.size() && iSubjectRank == -1; i++ )
	{
		if( vecAgeSubject[i].first == iSubject )
		{
			iSubjectRank = i;
		}
	}

	//
	int iTrainingRankStart = -1;
	int iTrainingRankEnd   = -1;
	if( iSubjectRank - (iTrainingSubjects/2) < 0 )
	{
		iTrainingRankStart = 0;
		iTrainingRankEnd = iTrainingRankStart + iTrainingSubjects;
	}
	else if( iSubjectRank + (iTrainingSubjects/2) >= vecImgName.size() )
	{
		iTrainingRankEnd = vecImgName.size() - 1;
		iTrainingRankStart = iTrainingRankEnd - iTrainingSubjects;
	}
	else
	{
		iTrainingRankStart = iSubjectRank - (iTrainingSubjects/2);
		iTrainingRankEnd = iTrainingRankStart + iTrainingSubjects;
	}

	for( int i =0; i < vecImgName.size(); i++ )
	{
		if( i < iTrainingRankStart || i > iTrainingRankEnd )
		{
			// Invalidate all features/labels associated with this image/subject
			SetValidFlagImage( vecAgeSubject[i].first, 0 );
			vecImgLabels[vecAgeSubject[i].first] = -1;
		}
	}
	return 1;
}

int
ModelFeature3D::BoostFeatures(
	int iBoostedFeatureCount,
	vector< int > *pvecOrderFeatsDisregard
)
{
	float *pfImgWeights = new float[vecImgName.size()]; // Boosted value of a training image (distribution)
	float *pfCurrClassScore = new float[vecImgName.size()]; // Current classification score based on all previously selected features
															// log likelihood ratio.
	float *pfNewClassScore = new float[vecImgName.size()]; // Log likelihood for individual features
	float *pfBestNewClassScore = new float[vecImgName.size()]; // Log likelihood for individual features
	int *piImgOccur = new int[vecImgName.size()];	// Occurrence of a feature
	for( int iImg = 0; iImg < vecImgName.size(); iImg++ )
	{
		pfImgWeights[iImg] = 1.0f/(float)vecImgName.size(); // all images weighted equally
		piImgOccur[iImg] = 0; // no feature occurrences
		pfCurrClassScore[iImg] = 0; // log(1/1)
		pfNewClassScore[iImg] = 0;
		pfBestNewClassScore[iImg] = 0;
	}

	// The ordered feature vector should be specified
	assert( vecOrderToFeat.size() > iBoostedFeatureCount );
	int iValidFeatCount = vecOrderToFeat.size();
	if( iValidFeatCount == 0
		|| iValidFeatCount < iBoostedFeatureCount )
	{
		return -1;
	}

	// Allocate arrays for valid & boosted features

	vector< pair<int,float> > pdpValidFeats;
	vector< pair<int,float> > pdpBoostedFeats;

	pdpValidFeats.resize( iValidFeatCount, pair<int,float>(0,0) );
	pdpBoostedFeats.resize( iBoostedFeatureCount, pair<int,float>(0,0) );

	// Initialize valid feature array
	for( int i = 0; i < iValidFeatCount; i++ )
	{
			int iFeat = vecOrderToFeat[i];
			pdpValidFeats[i].first = iFeat;
			pdpValidFeats[i].second = 0;
			
		if( pvecOrderFeatsDisregard )
		{
			if( (*pvecOrderFeatsDisregard)[i] != 0 )
			{			
				pdpValidFeats[i].first = -1; // Disregard this feature from boostinng
			}
		}
	}

	FILE *outfile = fopen( "weights.txt", "wt" );
	fclose( outfile );

	int iBoostedFeatIndexPrev = -1000;
	float fBoostedFeatScorePrev = -1000;

	m_vecBoostingCoefficients.clear();

	int iBoostedFeat;
	for( iBoostedFeat = 0; iBoostedFeat < iBoostedFeatureCount; iBoostedFeat++ )
	{
		// Recalculate weights (normalize)
		float fSum = 0;
		for( int iImg = 0; iImg < vecImgName.size(); iImg++ )
		{
			if( vecImgLabels[iImg] >= 0 )
			{
				fSum += pfImgWeights[iImg];
			}
		}
		outfile = fopen( "weights.txt", "a++" );
		fprintf( outfile, "%d\t%f\t", iBoostedFeatIndexPrev, fBoostedFeatScorePrev );
		for( int iImg = 0; iImg < vecImgName.size(); iImg++ )
		{
			if( vecImgLabels[iImg] >= 0 )
			{
				pfImgWeights[iImg] /= fSum;
				//fprintf( outfile, "%d\t%f\t", iImg, pfImgWeights[iImg] );
				fprintf( outfile, "%f\t", pfImgWeights[iImg] );
			}
		}
		fprintf( outfile, "\n" );
		fclose( outfile );

		// Classifier
		ClassificationEvaluation ceClassification;

		// Save best valid feature
		int iBestValidFeat = -1;
		float fBestValidFeatScore = 100000;
		float fBestValidFeatThreshold = 100000;

		// Re-evaluate all remaining features
		for( int iValidFeat = 0; iValidFeat < iValidFeatCount; iValidFeat++ )
		{
			// Reset value
			pdpValidFeats[iValidFeat].second = 0;

			ceClassification.Clear();

			if( pdpValidFeats[iValidFeat].first >= 0 ) // This is a little check, feature is invalid/already used if negative
			{
				// This feature is still a candiate, recalculate it's worth
				int iFeatIndex = pdpValidFeats[iValidFeat].first;
				MODEL_FEATURE &feat = vecFeats[iFeatIndex];

				// Obtain label for this feature
				int iFeatImg = vecFeatToImgName[iFeatIndex];
				int iFeatLabel = vecImgLabels[iFeatImg];

				if( vecImgLabels[iFeatImg] < 0 )
				{
					printf( "Invalid Label!\n" );
					getchar();
				}

				// Identify occurrences in training data
				memset( piImgOccur, 0, sizeof(int)*vecImgName.size() );
				int iFeatOccurrences = 0;
				for( int iII = 0; iII < vecFeatToIndexInfoCount[iFeatIndex]; iII++ )
				{
					IndexInfo &II = vecIndexInfo[vecFeatToIndexInfo[iFeatIndex]+iII];
					MODEL_FEATURE &featMatched = vecFeats[II.Index()];

					// Obtain label for matched feature
					int iFeatMatchedImg = vecFeatToImgName[II.Index()];
					int iFeatMatchedLabel = vecImgLabels[iFeatMatchedImg];

					if( vecImgLabels[iFeatMatchedImg] < 0 )
					{
						continue;
					}

					if( iFeatMatchedImg == iFeatImg )
					{
						// Don't let an image classify itself - generalization!
						continue;
					}

					if( II.DistSqr() <= vecFeatAppearanceThresholds[iFeatIndex] )
					{
						piImgOccur[ iFeatMatchedImg ] = 1;
					}
				}

				// Generate new classification score for each image based on this feature
				float *pfProbs = m_bayesClass.m_pbfdDistributions[ iValidFeat ];
				float fRatioFeat0 = log( pfProbs[1] / pfProbs[0] );
				float fRatioFeat1 = log( pfProbs[3] / pfProbs[2] );
				for( int iImg = 0; iImg < vecImgName.size(); iImg++ )
				{
					if( vecImgLabels[iImg] < 0 )
					{
						if( piImgOccur[iImg] != 0 )
						{
							printf( "This image should not be counted!\n" );
							getchar();
						}
						continue;
					}
					if( piImgOccur[iImg] == 0 )
					{
						pfNewClassScore[iImg] = pfCurrClassScore[iImg] + fRatioFeat0;
					}
					else
					{
						pfNewClassScore[iImg] = pfCurrClassScore[iImg] + fRatioFeat1;
					}
					ceClassification.AddLabelScore( vecImgLabels[iImg], pfNewClassScore[iImg], iImg );
				}

				// Calculate optimal threshold
				float fThreshold;
				float fEER = ceClassification.CalculateEER( 0, &fThreshold );

				// Score feature based on weights
				float fFeatScoreError = 0;
				for( int iImg = 0; iImg < vecImgName.size(); iImg++ )
				{
					if( vecImgLabels[iImg] < 0 )
					{
						if( pfNewClassScore[iImg] != 0 )
						{
							printf( "This image should not be classified!\n" );
							getchar();
						}
						continue;
					}
					int iImgLabel = vecImgLabels[iImg];
					if( (iImgLabel == 0 && pfNewClassScore[iImg] <= fThreshold)
						||
						(iImgLabel == 1 && pfNewClassScore[iImg] > fThreshold)
						)
					{
						// Correctly classified
					}
					else
					{
						// Incorrectly classified, sum weight
						fFeatScoreError += pfImgWeights[iImg];
					}
				}
				pdpValidFeats[iValidFeat].second = fFeatScoreError;

				// Save best feature, threshold, new classification score for all images
				if( iBestValidFeat == -1 || fFeatScoreError < fBestValidFeatScore )
				{
					iBestValidFeat = iValidFeat;
					fBestValidFeatScore = fFeatScoreError;
					fBestValidFeatThreshold = fThreshold;
					for( int iImg = 0; iImg < vecImgName.size(); iImg++ )
					{
						if( vecImgLabels[iImg] < 0 )
						{
							if( pfNewClassScore[iImg] != 0 )
							{
								printf( "This image should not be classified!\n" );
								getchar();
							}
							continue;
						}
						pfBestNewClassScore[iImg] = pfNewClassScore[iImg];
					}
				}
			}
		}

		if( fBoostedFeatScorePrev == pdpValidFeats[iBestValidFeat].second
			|| fBestValidFeatScore >= 0.5 )
		{
			// Further boosting does not help, time to get out...
			break;
		}

		iBoostedFeatIndexPrev = pdpValidFeats[iBestValidFeat].first;
		fBoostedFeatScorePrev = pdpValidFeats[iBestValidFeat].second;

		if( pvecOrderFeatsDisregard )
		{
			// Save features to disregard for next round of boosting
			(*pvecOrderFeatsDisregard)[iBestValidFeat] = 1;
		}

		// Set best feature as the next boosted feature
		assert( pdpValidFeats[iBestValidFeat].first >= 0 );
		pdpBoostedFeats[iBoostedFeat].first = pdpValidFeats[iBestValidFeat].first;
		pdpBoostedFeats[iBoostedFeat].second = pdpValidFeats[iBestValidFeat].second;

		// Save current classification score based on the boosted feature
		for( int iImg = 0; iImg < vecImgName.size(); iImg++ )
		{
			if( vecImgLabels[iImg] < 0 )
			{
				if( pfNewClassScore[iImg] != 0 )
				{
					printf( "This image should not be classified!\n" );
					getchar();
				}
				continue;
			}
			pfCurrClassScore[iImg] = pfBestNewClassScore[iImg];
		}

		// Remove boosted feature from consideration
		pdpValidFeats[iBestValidFeat].first = -1;

		// Save error coefficient
		if( fBestValidFeatScore >= 1.0f )
		{
			printf( "Error: best valid error greater than zero!\n" );
		}
		float fBeta = (fBestValidFeatScore) / (1.0f-fBestValidFeatScore);
		float fLogBetaInv = log( 1.0f / fBeta );
		m_vecBoostingCoefficients.push_back( fLogBetaInv );

		// Update weights: decrease weight if image is correctly classified
		for( int iImg = 0; iImg < vecImgName.size(); iImg++ )
		{
			if( vecImgLabels[iImg] < 0 )
			{
				if( pfNewClassScore[iImg] != 0 )
				{
					printf( "This image should not be classified!\n" );
					getchar();
				}
				continue;
			}
			int iImgLabel = vecImgLabels[iImg];
			if( (iImgLabel == 0 && pfCurrClassScore[iImg] <= fBestValidFeatThreshold)
				||
				(iImgLabel == 1 && pfCurrClassScore[iImg] > fBestValidFeatThreshold)
				)
			{
				// Correctly classified, decrease weight
				pfImgWeights[iImg] *= fBeta;
			}
		}
	}

	// Now remove all valid flags except for boosted features
	// Have to reset the order flag too...
	for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	{
		vecFeatValid[iFeat] = 0;
	}

	// We may not have been able to find as many boosted features as desired...
	if( iBoostedFeat != iBoostedFeatureCount )
	{
		iBoostedFeatureCount = iBoostedFeat;
	}

	// Set boosted feature validity flags
	vecOrderToFeat.resize(iBoostedFeatureCount);
	for( int iBoostedFeat = 0; iBoostedFeat < iBoostedFeatureCount; iBoostedFeat++ )
	{
		vecFeatValid[pdpBoostedFeats[iBoostedFeat].first] = 1;
		vecOrderToFeat[iBoostedFeat] = pdpBoostedFeats[iBoostedFeat].first;
	}

	outfile = fopen( "positive_boost_features.txt", "wt" );
	for( int iBoostedFeat = 0; iBoostedFeat < iBoostedFeatureCount; iBoostedFeat++ )
	{
		int iCountCorrect = pdpBoostedFeats[iBoostedFeat].second;
		int iCountError = pdpBoostedFeats[iBoostedFeat].second;
		fprintf( outfile, "%d\t%d\t%d\n", pdpBoostedFeats[iBoostedFeat].first, iCountCorrect, iCountError );
	}
	fclose( outfile );

	delete [] pfImgWeights;
	delete [] pfCurrClassScore;
	delete [] pfNewClassScore;
	delete [] pfBestNewClassScore;
	delete [] piImgOccur;

	return 0;
}


int
ModelFeature3D::BoostFeaturesRegistration(
	int iBoostedFeatureCount,
	vector< int > *pvecOrderFeatsDisregard
)
{
	float *pfImgWeights = new float[vecImgName.size()]; // Boosted value of a training image (distribution)
	float *pfCurrClassScore = new float[vecImgName.size()]; // Current classification score based on all previously selected features
															// log likelihood ratio.
	float *pfNewClassScore = new float[vecImgName.size()]; // Log likelihood for individual features
	float *pfBestNewClassScore = new float[vecImgName.size()]; // Log likelihood for individual features
	int *piImgOccur = new int[vecImgName.size()];	// Occurrence of a feature
	int iValidImages = 0;
	for( int iImg = 0; iImg < vecImgName.size(); iImg++ )
	{
		pfImgWeights[iImg] = 1.0f/(float)vecImgName.size(); // all images weighted equally
		piImgOccur[iImg] = 0; // no feature occurrences
		pfCurrClassScore[iImg] = 0; // log(1/1)
		pfNewClassScore[iImg] = 0;
		pfBestNewClassScore[iImg] = 0;
		if( vecImgLabels[iImg] > 0 )
		{
			iValidImages++; // Count number of valid training images
		}
	}


	// The ordered feature vector should be specified
	assert( vecOrderToFeat.size() > iBoostedFeatureCount );
	int iValidFeatCount = vecOrderToFeat.size();
	if( iValidFeatCount == 0
		|| iValidFeatCount < iBoostedFeatureCount )
	{
		return -1;
	}

	// Allocate arrays for valid & boosted features

	vector< pair<int,float> > pdpValidFeats;
	vector< pair<int,float> > pdpBoostedFeats;

	pdpValidFeats.resize( iValidFeatCount, pair<int,float>(0,0) );
	pdpBoostedFeats.resize( iBoostedFeatureCount, pair<int,float>(0,0) );

	// Initialize valid feature array
	for( int i = 0; i < iValidFeatCount; i++ )
	{
			int iFeat = vecOrderToFeat[i];
			pdpValidFeats[i].first = iFeat;
			pdpValidFeats[i].second = 0;
			
		if( pvecOrderFeatsDisregard )
		{
			if( (*pvecOrderFeatsDisregard)[i] != 0 )
			{			
				pdpValidFeats[i].first = -1; // Disregard this feature from boostinng
			}
		}
	}

	FILE *outfile = fopen( "weights.txt", "wt" );
	fclose( outfile );

	int iBoostedFeatIndexPrev = -1000;
	float fBoostedFeatScorePrev = -1000;

	m_vecBoostingCoefficients.clear();

	int iBoostedFeat;
	for( iBoostedFeat = 0; iBoostedFeat < iBoostedFeatureCount; iBoostedFeat++ )
	{
		// Recalculate weights (normalize)
		float fSum = 0;
		for( int iImg = 0; iImg < vecImgName.size(); iImg++ )
		{
			if( vecImgLabels[iImg] >= 0 )
			{
				fSum += pfImgWeights[iImg];
			}
		}
		outfile = fopen( "weights.txt", "a++" );
		fprintf( outfile, "%d\t%f\t", iBoostedFeatIndexPrev, fBoostedFeatScorePrev );
		for( int iImg = 0; iImg < vecImgName.size(); iImg++ )
		{
			if( vecImgLabels[iImg] >= 0 )
			{
				pfImgWeights[iImg] /= fSum;
				//fprintf( outfile, "%d\t%f\t", iImg, pfImgWeights[iImg] );
				fprintf( outfile, "%f\t", pfImgWeights[iImg] );
			}
		}
		fprintf( outfile, "\n" );
		fclose( outfile );

		// Classifier
		ClassificationEvaluation ceClassification;

		// Save best valid feature
		int iBestValidFeat = -1;
		float fBestValidFeatScore = 100000;
		float fBestValidFeatThreshold = 100000;

		// Re-evaluate all remaining features
		for( int iValidFeat = 0; iValidFeat < iValidFeatCount; iValidFeat++ )
		{
			// Reset value
			pdpValidFeats[iValidFeat].second = 0;

			ceClassification.Clear();

			if( pdpValidFeats[iValidFeat].first >= 0 ) // This is a little check, feature is invalid/already used if negative
			{
				// This feature is still a candiate, recalculate it's worth
				int iFeatIndex = pdpValidFeats[iValidFeat].first;
				MODEL_FEATURE &feat = vecFeats[iFeatIndex];

				// Obtain label for this feature
				int iFeatImg = vecFeatToImgName[iFeatIndex];
				int iFeatLabel = vecImgLabels[iFeatImg];

				if( vecImgLabels[iFeatImg] < 0 )
				{
					printf( "Invalid Label!\n" );
					getchar();
				}

				// Identify occurrences in training data
				memset( piImgOccur, 0, sizeof(int)*vecImgName.size() );
				int iFeatOccurrences = 0;
				for( int iII = 0; iII < vecFeatToIndexInfoCount[iFeatIndex]; iII++ )
				{
					IndexInfo &II = vecIndexInfo[vecFeatToIndexInfo[iFeatIndex]+iII];
					MODEL_FEATURE &featMatched = vecFeats[II.Index()];

					// Obtain label for matched feature
					int iFeatMatchedImg = vecFeatToImgName[II.Index()];
					int iFeatMatchedLabel = vecImgLabels[iFeatMatchedImg];

					if( vecImgLabels[iFeatMatchedImg] < 0 )
					{
						continue;
					}

					if( iFeatMatchedImg == iFeatImg )
					{
						// Don't let an image classify itself - generalization!
						//continue;
					}

					if( II.DistSqr() <= vecFeatAppearanceThresholds[iFeatIndex] )
					{
						piImgOccur[ iFeatMatchedImg ] = 1;
					}
				}

				// Score feature based on weights
				float fFeatScoreError = 0;
				for( int iImg = 0; iImg < vecImgName.size(); iImg++ )
				{
					if( piImgOccur[iImg] )
					{
						// Correctly classified
					}
					else
					{
						// Incorrectly classified, sum weight
						fFeatScoreError += pfImgWeights[iImg];
					}
				}
				pdpValidFeats[iValidFeat].second = fFeatScoreError;

				// Save best feature, threshold, new classification score for all images
				if( iBestValidFeat == -1 || fFeatScoreError < fBestValidFeatScore )
				{
					iBestValidFeat = iValidFeat;
					fBestValidFeatScore = fFeatScoreError;
					for( int iImg = 0; iImg < vecImgName.size(); iImg++ )
					{
						if( vecImgLabels[iImg] < 0 )
						{
							if( pfNewClassScore[iImg] != 0 )
							{
								printf( "This image should not be classified!\n" );
								getchar();
							}
							continue;
						}
						else
						{
							// Image correctly classified if feature occurs
							if( piImgOccur[ iImg ] )
							{
								pfBestNewClassScore[iImg] = 1.0f;
							}
							else
							{
								pfBestNewClassScore[iImg] = 0.0f;
							}
						}
					}
				}
			}
		}

		if( fBoostedFeatScorePrev == pdpValidFeats[iBestValidFeat].second
			//|| fBestValidFeatScore >= 0.5
			)
		{
			// Further boosting does not help, time to get out...
			break;
		}

		iBoostedFeatIndexPrev = pdpValidFeats[iBestValidFeat].first;
		fBoostedFeatScorePrev = pdpValidFeats[iBestValidFeat].second;

		if( pvecOrderFeatsDisregard )
		{
			// Save features to disregard for next round of boosting
			(*pvecOrderFeatsDisregard)[iBestValidFeat] = 1;
		}

		// Set best feature as the next boosted feature
		assert( pdpValidFeats[iBestValidFeat].first >= 0 );
		pdpBoostedFeats[iBoostedFeat].first = pdpValidFeats[iBestValidFeat].first;
		pdpBoostedFeats[iBoostedFeat].second = pdpValidFeats[iBestValidFeat].second;

		// Remove boosted feature from consideration
		pdpValidFeats[iBestValidFeat].first = -1;

		// Save error coefficient
		if( fBestValidFeatScore >= 1.0f )
		{
			printf( "Error: best valid error greater than zero!\n" );
		}
		float fBeta = (fBestValidFeatScore) / (1.0f-fBestValidFeatScore);
		float fLogBetaInv = log( 1.0f / fBeta );
		m_vecBoostingCoefficients.push_back( fLogBetaInv );

		// Update weights: decrease weight if image is correctly classified
		for( int iImg = 0; iImg < vecImgName.size(); iImg++ )
		{
			if( vecImgLabels[iImg] < 0 )
			{
				if( pfNewClassScore[iImg] != 0 )
				{
					printf( "This image should not be classified!\n" );
					getchar();
				}
				continue;
			}
			else if( pfBestNewClassScore[iImg] > 0.0f )
			{
				// Correctly classified, decrease weight
				pfImgWeights[iImg] *= 0.5;//fBeta;
			}
		}
	}

	// Now remove all valid flags except for boosted features
	// Have to reset the order flag too...
	for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	{
		vecFeatValid[iFeat] = 0;
	}

	// We may not have been able to find as many boosted features as desired...
	if( iBoostedFeat != iBoostedFeatureCount )
	{
		iBoostedFeatureCount = iBoostedFeat;
	}

	// Set boosted feature validity flags
	vecOrderToFeat.resize(iBoostedFeatureCount);
	for( int iBoostedFeat = 0; iBoostedFeat < iBoostedFeatureCount; iBoostedFeat++ )
	{
		vecFeatValid[pdpBoostedFeats[iBoostedFeat].first] = 1;
		vecOrderToFeat[iBoostedFeat] = pdpBoostedFeats[iBoostedFeat].first;
	}

	outfile = fopen( "positive_boost_features.txt", "wt" );
	for( int iBoostedFeat = 0; iBoostedFeat < iBoostedFeatureCount; iBoostedFeat++ )
	{
		int iCountCorrect = pdpBoostedFeats[iBoostedFeat].second;
		int iCountError = pdpBoostedFeats[iBoostedFeat].second;
		fprintf( outfile, "%d\t%d\t%d\n", pdpBoostedFeats[iBoostedFeat].first, iCountCorrect, iCountError );
	}
	fclose( outfile );

	delete [] pfImgWeights;
	delete [] pfCurrClassScore;
	delete [] pfNewClassScore;
	delete [] pfBestNewClassScore;
	delete [] piImgOccur;

	return 0;
}

int
BoostFeaturesOriginal(
	vector<char*>		&vecImgName, // Array of image names
	vector<int>			&vecFeatToImgName, // Maps each Feat to its image name
	vector<MODEL_FEATURE>	&vecFeats, // Array of features
	vector<IndexInfo>	&vecIndexInfo, // IndexInfo
	vector<int>			&vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int>			&vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	vector<int>			&vecFeatValid,	// For each Feat, is this feature valid?
	vector<float>		&vecFeatAppearanceThresholds, // For each Feat, appearance thresholds
	vector<int>			&vecImgLabels,
	vector<int>			&vecOrderToFeat,	// Feature indices, sorted from most to least frequent
	int iBoostedFeatureCount
)
{
	float *pfImgValue = new float[vecImgName.size()]; // Boosted value of a training image (distribution)
	int *piImgOccur = new int[vecImgName.size()];	// Occurrence of a feature
	for( int iImg = 0; iImg < vecImgName.size(); iImg++ )
	{
		pfImgValue[iImg] = 1.0f/(float)vecImgName.size(); // all images weighted equally
		piImgOccur[iImg] = 0; // no feature occurrences
	}

	// Create an array of valid features
	int iValidFeatCount = 0;
	for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	{
		if( vecFeatValid[iFeat] == 1 )
		{
			iValidFeatCount++;
		}
	}

	// *** Fix for a moment
	assert( vecOrderToFeat.size() > iBoostedFeatureCount );
	iValidFeatCount = vecOrderToFeat.size();

	// Allocate arrays for valid & boosted features
	DATA_PAIR *pdpValidFeats = new DATA_PAIR[ iValidFeatCount ];
	DATA_PAIR *pdpBoostedFeats = new DATA_PAIR[ iBoostedFeatureCount ];
	memset( pdpValidFeats, 0, sizeof(DATA_PAIR)*iValidFeatCount );
	memset( pdpBoostedFeats, 0, sizeof(DATA_PAIR)*iBoostedFeatureCount );

	// Initialize valid feature array
	//iValidFeatCount = 0;
	//for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	//{
	//	if( vecFeatValid[iFeat] == 1 )
	//	{
	//		pdpValidFeats[iValidFeatCount].iIndex = iFeat;
	//		pdpValidFeats[iValidFeatCount].iValue = 0;
	//		iValidFeatCount++;
	//	}
	//}
	for( int i = 0; i < iValidFeatCount; i++ )
	{
		int iFeat = vecOrderToFeat[i];
		pdpValidFeats[i].iIndex = iFeat;
		pdpValidFeats[i].iValue = 0;
	}

	//
	// For informativeness, simply consider co-occurence in positive 
	// examples.
	// Let's try a version of boosting: just consider positive examples
	//
	
	FILE *outfile = fopen( "weights.txt", "wt" );
	fclose( outfile );

	// At most, one boosted feature per training image 
	//for( int iBoostedFeat = 0; iBoostedFeat < vecImgName.size(); iBoostedFeat++ )
	for( int iBoostedFeat = 0; iBoostedFeat < iBoostedFeatureCount; iBoostedFeat++ )
	{
		// Recalculate weights (normalize)
		float fSum = 0;
		for( int iImg = 0; iImg < vecImgName.size(); iImg++ )
		{
			fSum += pfImgValue[iImg];
		}

		//FILE *outfile = fopen( "weights.txt", "a++" );
		for( int iImg = 0; iImg < vecImgName.size(); iImg++ )
		{
			//pfImgValue[iImg] /= fSum;
			//fprintf( outfile, "%f\t", pfImgValue[iImg] );
		}
		//fprintf( outfile, "\n" );
		fclose( outfile );

		// Re-evaluate all remaining features
		for( int iValidFeat = 0; iValidFeat < iValidFeatCount; iValidFeat++ )
		{
			// Reset value
			pdpValidFeats[iValidFeat].iValue = 0;

			if( pdpValidFeats[iValidFeat].iIndex >= 0 )
			{
				// This feature is still a candiate, recalculate it's worth
				int iFeatIndex = pdpValidFeats[iValidFeat].iIndex;
				MODEL_FEATURE &feat = vecFeats[iFeatIndex];

				// Obtain label for this feature
				int iFeatImg = vecFeatToImgName[iFeatIndex];
				int iFeatLabel = vecImgLabels[iFeatImg];

				// Identify occurrences in training data
				memset( piImgOccur, 0, sizeof(int)*vecImgName.size() );
				int iFeatOccurrences = 0;
				for( int iII = 0; iII < vecFeatToIndexInfoCount[iFeatIndex]; iII++ )
				{
					IndexInfo &II = vecIndexInfo[vecFeatToIndexInfo[iFeatIndex]+iII];
					
					MODEL_FEATURE &featMatched = vecFeats[II.Index()];

					// Obtain label for matched feature
					int iFeatMatchedImg = vecFeatToImgName[II.Index()];
					int iFeatMatchedLabel = vecImgLabels[iFeatMatchedImg];

					if( II.DistSqr() <= vecFeatAppearanceThresholds[iFeatIndex] )
					{
						if( piImgOccur[ vecFeatToImgName[II.Index()] ] == 0 )
						{
							// Count only once
							iFeatOccurrences++;
						}
						if( iFeatLabel == iFeatMatchedLabel )
						{
							piImgOccur[ vecFeatToImgName[II.Index()] ] = 1;
						}
						else
						{
							piImgOccur[ vecFeatToImgName[II.Index()] ] = -1;
						}
					}
				}

				// Score feature based on weights
				float fFeatScoreTotal = 0;
				float fFeatScoreError = 0;
				for( int iImg = 0; iImg < vecImgName.size(); iImg++ )
				{
					//fFeatScore += piImgOccur[iImg]*pfImgValue[iImg];
					if( piImgOccur[iImg] != 0 )
					{
						fFeatScoreTotal += pfImgValue[iImg];
						if( piImgOccur[iImg] == -1 )
						{
							// Generate the error rate
							fFeatScoreError += pfImgValue[iImg];
						}
					}
				}

				// Normalize error according the number number of occurrences
				// Add an extra term 1.0f to favor features which occurr often.
				float fFeatErrorRate = (fFeatScoreError + 0.0001f) / (fFeatScoreTotal + 0.0001f);

				// Feature score always between -1 and 1
				assert( fFeatErrorRate >= 0.0f );
				assert( fFeatErrorRate <  1.0f );
				//pdpValidFeats[iValidFeat].iValue = 0x0FFFFFFF*fFeatScore;
				// Save correct rate = 1 - error rate.
				pdpValidFeats[iValidFeat].iValue = 0x0FFFFFFF*(1.0f-fFeatErrorRate);
			}
		}

		// Determine maximal feature
		dpSortDescending( pdpValidFeats, iValidFeatCount );

		// Save maximal boosted feature
		assert( pdpValidFeats[0].iIndex >= 0 );
		pdpBoostedFeats[iBoostedFeat].iIndex = pdpValidFeats[0].iIndex;
		pdpBoostedFeats[iBoostedFeat].iValue = pdpValidFeats[0].iValue;
		float fBestScore = (float)(pdpValidFeats[0].iValue) / (float)0x0FFFFFFF;

		// Remove boosted feature from consideration
		pdpValidFeats[0].iIndex = -1;

		// Re-calculate occurrences for this feature
		int iBoostedFeatIndex = pdpBoostedFeats[iBoostedFeat].iIndex;
		MODEL_FEATURE &feat = vecFeats[iBoostedFeatIndex];
		// Obtain label for this feature
		int iBoostedFeatImg = vecFeatToImgName[iBoostedFeatIndex];
		int iBoostedFeatLabel = vecImgLabels[iBoostedFeatImg];
		memset( piImgOccur, 0, sizeof(int)*vecImgName.size() );

		int iCountCorrect = 1; // Original feature is correct
		int iCountError = 0;
		for( int iII = 0; iII < vecFeatToIndexInfoCount[iBoostedFeatIndex]; iII++ )
		{
			IndexInfo &II = vecIndexInfo[vecFeatToIndexInfo[iBoostedFeatIndex]+iII];
			MODEL_FEATURE &featMatched = vecFeats[II.Index()];
			// Obtain label for matched feature
			int iFeatMatchedImg = vecFeatToImgName[II.Index()];
			int iFeatMatchedLabel = vecImgLabels[iFeatMatchedImg];
			
			if( II.DistSqr() <= vecFeatAppearanceThresholds[iBoostedFeatIndex] )
			{
				if( iBoostedFeatLabel == iFeatMatchedLabel )
				{
					piImgOccur[ vecFeatToImgName[II.Index()] ] = 1;
					iCountCorrect++;
				}
				else
				{
					piImgOccur[ vecFeatToImgName[II.Index()] ] = -1;
					iCountError++;
				}
			}
		}

		// Save pos/neg count for later output
		pdpBoostedFeats[iBoostedFeat].iValue = iCountCorrect*1000 + iCountError;

		//this->OutputModelPointAndInstances( 
		//	pdpBoostedFeats[iBoostedFeat].iIndex, 
		//	"boosted", 5 );

		// Re-calculate weights based on boosted feature
		//   - either multiply or divide image weights by 2 depending on whether they've 
		// been incorrectly or correctly classified.
		for( int iImg = 0; iImg < vecImgName.size(); iImg++ )
		{
			pfImgValue[iImg] *= exp( -0.5*piImgOccur[iImg] );
			//if( piImgOccur[iImg] == 1 )
			//{
			//	pfImgValue[iImg] /= 2.0f;
			//}
			//else if( piImgOccur[iImg] == -1 )
			//{
			//	pfImgValue[iImg] *= 2.0f;
			//}
		}
	}

	// Now remove all valid flags except for boosted features
	// Have to reset the order flag too...
	for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	{
		vecFeatValid[iFeat] = 0;
	}

	// Set boosted feature validity flags
	vecOrderToFeat.resize(iBoostedFeatureCount);
	for( int iBoostedFeat = 0; iBoostedFeat < iBoostedFeatureCount; iBoostedFeat++ )
	{
		vecFeatValid[pdpBoostedFeats[iBoostedFeat].iIndex] = 1;
		vecOrderToFeat[iBoostedFeat] = pdpBoostedFeats[iBoostedFeat].iIndex;
	}

	outfile = fopen( "positive_boost_features.txt", "wt" );
	for( int iBoostedFeat = 0; iBoostedFeat < iBoostedFeatureCount; iBoostedFeat++ )
	{
		int iCountCorrect = pdpBoostedFeats[iBoostedFeat].iValue / 1000;
		int iCountError = pdpBoostedFeats[iBoostedFeat].iValue % 1000;
		fprintf( outfile, "%d\t%d\t%d\n", pdpBoostedFeats[iBoostedFeat].iIndex, iCountCorrect, iCountError );
	}
	fclose( outfile );

	delete [] pdpBoostedFeats;
	delete [] pdpValidFeats;
	delete [] pfImgValue;
	delete [] piImgOccur;

	return 0;
}


int
EnforceROI(
	vector<MODEL_FEATURE>	&vecFeats, // Array of features
	vector<int>			&vecFeatValid,	// For each Feat, is this feature valid?
	vector<int>			&vecOrderToFeat,	// Feature indices, sorted from most to least frequent
	int x1, int x2,
	int y1, int y2,
	int z1, int z2
)
{
	int iRemoveCount = 0;
	for( int i = 0; i < vecOrderToFeat.size(); i++ )
	{
		int iFeat = vecOrderToFeat[i];
		MODEL_FEATURE &feat = vecFeats[iFeat];
		if( feat.x < x1 || feat.x > x2 
			|| feat.y < y1 || feat.y > y2
			|| feat.z < z1 || feat.z > z2 )
		{
			// Invalidate
			vecFeatValid[iFeat] = 0;
			// Erase
			vector<int>::iterator it = vecOrderToFeat.begin() + i;
			vecOrderToFeat.erase( it );
			// Decrement i
			i--;
			iRemoveCount++;
		}
	}
	return iRemoveCount;
}

// 
// generate_axis_rotations()
//
// generate all possible axis rotations.
//

void
axis3D_code_to_axis(
			 int iCode,
			 int &iAxis, // 0, 1 or 2
			 int &iSign // -1 or 1
			 )
{
	iAxis = iCode % 3;
	iSign = iCode < 3 ? 1 : -1;
}

void
axis3D_code_to_vector(
			 int iCode,
			 int *piVec
			 )
{
	int iAxis;
	int iSign;
	axis3D_code_to_axis( iCode, iAxis, iSign );
	piVec[0]=piVec[1]=piVec[2]=0;
	piVec[iAxis]=iSign;
}

void
axis3D_vector_to_code(
			 int *piVec,
			 int &iCode
			 )
{
	int iAxis;
	for( iAxis = 0; iAxis < 3; iAxis++ )
	{
		if( piVec[iAxis] != 0 )
		{

			if( piVec[iAxis] == 1 )
			{
				iCode=iAxis;
			}
			else if( piVec[iAxis] == -1 )
			{
				iCode=iAxis+3;
			}
			else
			{
				// Invalid
				iCode = -1;
				assert( 0 );
			}
		}
	}
}

void
axis3D_generate_axis_rotations(
						int *piRotations,
						int &iCount
						)
{
	iCount = 0;
	for( int iAx0 = 0; iAx0 < 6; iAx0++ )
	{
		int piV0[3];
		axis3D_code_to_vector( iAx0, piV0 );
		for( int iAx1 = 0; iAx1 < 6; iAx1++ )
		{
			if( (iAx1 % 3) == (iAx0 % 3) )
			{
				continue;
			}
			int piV1[3];
			axis3D_code_to_vector( iAx1, piV1 );

			// Find vector cross product
			int piV2[3];
			piV2[0] =  piV0[1]*piV1[2] - piV0[2]*piV1[1];
			piV2[1] = -piV0[0]*piV1[2] + piV0[2]*piV1[0];
			piV2[2] =  piV0[0]*piV1[1] - piV0[1]*piV1[0];

			// Convert to code
			int iAx2;
			axis3D_vector_to_code( piV2, iAx2 );

			// Save
			piRotations[3*iCount+0]=iAx0;
			piRotations[3*iCount+1]=iAx1;
			piRotations[3*iCount+2]=iAx2;
			iCount++;
		}
	}
}


void
msCalculatePC_SVD_rotation_coeffs(
				   vector< vector< vector<float> > >  &vecPCArr,
		int					iPCs=PC_ARRAY_SIZE // number of principal components to use, should be max
			   )
{
	iPCs = 16;//vecPCArr[0].size();
	int image_size = iPCs*iPCs;
	double *pdCov = new double[image_size*image_size];
	double *pdEig = new double[image_size];
	double *pdVec = new double[image_size*image_size];

	double **pdCovPt = new double*[image_size];
	double **pdVecPt = new double*[image_size];
	for( int i = 0; i < image_size; i++ )
	{
		pdCovPt[i] = &pdCov[i*image_size];
		pdVecPt[i] = &pdVec[i*image_size];
		for( int j = 0; j < image_size; j++ )
		{
			pdCovPt[i][j] = 0;
			pdVecPt[i][j] = 0;
		}
	}

	// Compute mean image
	double *pdMean = new double[image_size];
	for( int i =0; i < image_size; i++ )
	{
		pdMean[i] = 0;
	}
	for( int iFeat = 0; iFeat < vecPCArr.size(); iFeat++ )
	{
		for( int i = 0; i < iPCs; i++ )
		{
			for( int j = 0; j < iPCs; j++ )
			{
				pdMean[i*iPCs+j] += vecPCArr[iFeat][i][j];
			}
		}
	}
	for( int i =0; i < image_size; i++ )
	{
		pdMean[i] /= (float)vecPCArr.size();
	}

	// Accumulate covariance matrix
	int iSamples = 0;
	for( int iFeat = 0; iFeat < vecPCArr.size(); iFeat++ )
	{
		// Subtract mean
		for( int i = 0; i < iPCs; i++ )
		{
			for( int j = 0; j < iPCs; j++ )
			{
				vecPCArr[iFeat][i][j] -= pdMean[i*iPCs+j];
			}
		}

		for( int i = 0; i < iPCs; i++ )
		{
			for( int j = 0; j < iPCs; j++ )
			{
				for( int ii = 0; ii < iPCs; ii++ )
				{
					for( int jj = 0; jj < iPCs; jj++ )
					{
						pdCovPt[i*iPCs+j][ii*iPCs+jj]
								+= vecPCArr[iFeat][i][j]*vecPCArr[iFeat][ii][jj];
					}
				}
			}
		}
		iSamples++;
	}
	
	// Normalize number of samples
	for( int i =0; i < image_size; i++ )
	{
		for( int j=0; j < image_size; j++ )
		{
			pdCovPt[i][j] /= (double)iSamples;
		}
	}

	// Determine principal components
	svdcmp_iterations( pdCovPt, image_size, image_size, pdEig, pdVecPt, 1000 );

	reorder_descending( image_size, image_size, pdEig, pdVecPt );

	// Print out 1st 20 components
	for( int iFeat = 0;  iFeat < 50; iFeat++ )
	{
		//Feature3D feat;
		//feat.x=feat.y=feat.z=feat.scale=0;
		//for( int i =0; i < image_size; i++ )
		//{
		//	feat.data_zyx[0][0][i] = pdVecPt[i][iFeat];
		//}

		//// Save eigen value
		//feat.eigs[0] = pdEig[iFeat];

		//vecFeatPCs.push_back( feat );

		char pcFName[400];
		sprintf( pcFName, "pc%3.3d_%5.5f.pgm", iFeat, pdEig[iFeat] );
		//feat.OutputVisualFeature( pcFName );
	}

	delete [] pdMean;
	delete [] pdCov;
	delete [] pdEig;
	delete [] pdVec;
	delete [] pdCovPt;
	delete [] pdVecPt;
}

void
pctable_GenerateRotationLookup(
		vector<Feature3D> &vecPCs, // principal components
		vector< vector< vector<float> > >  &vecPCArr,
		int					iPCs=PC_ARRAY_SIZE // number of principal components to use, should be max
				  )
{
	int piRotations[MAX_AXIS_ROTATIONS_3D][3];
	memset( &(piRotations[0][0]), 0, sizeof(piRotations) );
	int iRotationCount=-1;
	axis3D_generate_axis_rotations( (int *)&(piRotations[0][0]), iRotationCount );

	// Save PCs
	vector<Feature3D> vecPCsTmp;
	vecPCsTmp.resize( vecPCs.size() );

	//for( int i = 0; i < vecPCs.size(); i++ ) // Forward
	//{
	//	Feature3D &featPC = vecPCs[i];
	//	Feature3D &featTmp = vecPCsTmp[i];
	//	featTmp = featPC;
	//}

	float ori[3][3];
	for( int iRot = 0; iRot < MAX_AXIS_ROTATIONS_3D; iRot++ )
	{
		// Apply orientation
		for( int i = 0; i < 3; i++ )
		{
			for( int j = 0; j < 3; j++ )
			{
				ori[i][j]=0;
			}
		}

		for( int i = 0; i < 3; i++ )
		{
			int iIndex = piRotations[iRot][i] % 3;
			int iValue = piRotations[iRot][i] < 3 ? 1 : -1;
			ori[ iIndex ][i]= iValue;
		}

		// For each rotation, generate the coefficients required to 
		// represent the rotated PC

		for( int iPC = 0; iPC < iPCs; iPC++ ) // Forward
		{
			Feature3D &featPC = vecPCs[iPC];
			Feature3D featTmp;

			// Copy PC to temporary feature
			featTmp = featPC;
			memcpy( &(featTmp.data_zyx[0][0]), &(featPC.data_zyx[0][0]), sizeof(featPC.data_zyx) );

			// Rotate
			featTmp.RotateData( (float*)&(ori[0][0]) );

			// Project to principal coponent basis
			featTmp.ProjectToPCs( vecPCs, iPCs );

			// Save
			for( int iPC2 = 0; iPC2 < iPCs; iPC2++ )
			{
				vecPCArr[iRot][iPC][iPC2] = featTmp.m_pfPC[iPC2];
			}
		}
	}

	// Exploratory function to estimate principal components of
	// rotation matrices - it doesn't appear that there is any linear PC structure.
	// Investigate PCs in rotation matrices
	//msCalculatePC_SVD_rotation_coeffs( vecPCArr,  26 );

}

//
// pctable_LookupPCs()
//
// Lookup coefficients for PC rotated according to iRot.
//
void
pctable_LookupPCs(
			   Feature3D &featIn,
			   Feature3D &featRotated,
			   vector< vector< vector<float> > >  &vecPCArr,
			   int iRot
			   )
{

	// Accumulate PCs for each rotated component
	featRotated = featIn;
	for( int i = 0; i < PC_ARRAY_SIZE; i++ )
	{
		featRotated.m_pfPC[i] = 0;
	}

	for( int iPC = 0; iPC < PC_ARRAY_SIZE; iPC++ )
	{
		for( int iPC2 = 0; iPC2 < PC_ARRAY_SIZE; iPC2++ )
		{
			featRotated.m_pfPC[iPC2] += featIn.m_pfPC[iPC] * vecPCArr[iRot][iPC][iPC2];
		}
	}
}

int
ModelFeature3D::FitToImageSearchRotationsPCTable(
		vector<Feature3D> &vecImgFeats,
		vector<int>       &vecModelMatches,
		int				  iModelFeatsToConsider,
		char *pcFeatFile,
		int					iPCs, // number of principal components to use
		vector<Feature3D> *pvecPCs // principal components
)
{
	// Encode rotations: 6 unique axis permutations 
	int piRotations[MAX_AXIS_ROTATIONS_3D][3];
	int iRotationCount=-1;
	axis3D_generate_axis_rotations( (int *)&(piRotations[0][0]), iRotationCount );

	// Generate a lookup table for projections of rotated principal components
	vector< vector< vector<float> > > vecRotatedPC;
	vecRotatedPC.resize( MAX_AXIS_ROTATIONS_3D );
	for( int iRot = 0; iRot < MAX_AXIS_ROTATIONS_3D; iRot++ )
	{
		vecRotatedPC[iRot].resize( PC_ARRAY_SIZE );
		for( int iPC = 0; iPC < PC_ARRAY_SIZE; iPC++ )
		{
			vecRotatedPC[iRot][iPC].resize( PC_ARRAY_SIZE );
		}
	}
	pctable_GenerateRotationLookup( *pvecPCs, vecRotatedPC );

	// Save image feats
	vector<Feature3D> vecFeatsBackup;
	vecFeatsBackup.resize( vecImgFeats.size() );

	// Project all input image features to PCs
	// Ideally this would be done externally and all we work with are principal components

	for( int i = 0; i < vecImgFeats.size(); i++ ) // Forward
	{
		Feature3D &featImg = vecImgFeats[i];
		Feature3D &featTmp = vecFeatsBackup[i];
		// Copy local feature geometry, this will not change
		featTmp = featImg;

		assert( iPCs > 0 );
		assert( pvecPCs );
		featImg.ProjectToPCs( *pvecPCs );

	}

	int iMaxInliers = -1;
	int iMaxRot = -1;


	for( int iRot = 0; iRot < MAX_AXIS_ROTATIONS_3D; iRot++ )
	{
		// Generate rotated principal components
		for( int i = 0; i < vecImgFeats.size(); i++ )
		{
			Feature3D &featImg = vecImgFeats[i];
			Feature3D &featTmp = vecFeatsBackup[i];
			// Copy to backup array
			pctable_LookupPCs( featImg, featTmp, vecRotatedPC,iRot );
		}

		// Fit to image
		int iInliers;
		iInliers = FitToImage( vecFeatsBackup,vecModelMatches,iModelFeatsToConsider, 0, iPCs );//pcFeatFile);
		if( iInliers > iMaxInliers )
		{
			// Save max info
			iMaxRot = iRot;
			iMaxInliers = iInliers;
		}
	}

	// Fit best solution again to image
	for( int i = 0; i < vecImgFeats.size(); i++ )
	{
		Feature3D &featImg = vecImgFeats[i];
		Feature3D &featTmp = vecFeatsBackup[i];

		// Copy to backup array
		pctable_LookupPCs( featImg, featTmp, vecRotatedPC, iMaxRot );
	}

	// Fit to image
	int iInliers;
	iInliers = FitToImage( vecFeatsBackup,vecModelMatches,iModelFeatsToConsider,pcFeatFile, iPCs);

	return iInliers;
}


int
ModelFeature3D::FitToImageSearchRotations(
		vector<Feature3D> &vecImgFeats,
		vector<int>       &vecModelMatches,
		int				  iModelFeatsToConsider,
		char *pcFeatFile,
		int					iPCs, // number of principal components to use
		vector<Feature3D> *pvecPCs // principal components
)
{
	// Encode rotations: 6 unique axis permutations 
	int piRotations[MAX_AXIS_ROTATIONS_3D][3];
	int iRotationCount=-1;
	axis3D_generate_axis_rotations( (int *)&(piRotations[0][0]), iRotationCount );

//#define TEST_LOOKUP
#ifdef TEST_LOOKUP
	// This code to test the PC/rotation lookup table along side the standard rotate/PC
	// 
	// Generate a lookup table for projections of rotated principal components
	vector< vector< vector<float> > > vecRotatedPC;
	vecRotatedPC.resize( MAX_AXIS_ROTATIONS_3D );
	for( int iRot = 0; iRot < MAX_AXIS_ROTATIONS_3D; iRot++ )
	{
		vecRotatedPC[iRot].resize( PC_ARRAY_SIZE );
		for( int iPC = 0; iPC < PC_ARRAY_SIZE; iPC++ )
		{
			vecRotatedPC[iRot][iPC].resize( PC_ARRAY_SIZE );
		}
	}
	pctable_GenerateRotationLookup( *pvecPCs, vecRotatedPC );
#endif

	int iMaxInliers = -1;
	int iMaxRot = -1;
	float max_ori[3][3];
	float ori[3][3];

	// Save image feats
	vector<Feature3D> vecFeatsBackup;
	vecFeatsBackup.resize( vecImgFeats.size() );

	for( int i = 0; i < vecImgFeats.size(); i++ ) // Forward
	{
		Feature3D &featImg = vecImgFeats[i];
		// Project to principal coponents
		if( iPCs > 0 )
		{
			featImg.ProjectToPCs( *pvecPCs );
		}
		Feature3D &featTmp = vecFeatsBackup[i];
		featTmp = featImg;
	}

	for( int iRot = 0; iRot < MAX_AXIS_ROTATIONS_3D; iRot++ )
	{
		// Apply orientation
		for( int i = 0; i < 3; i++ )
		{
			for( int j = 0; j < 3; j++ )
			{
				ori[i][j]=0;
			}
		}

		for( int i = 0; i < 3; i++ )
		{
			int iIndex = piRotations[iRot][i] % 3;
			int iValue = piRotations[iRot][i] < 3 ? 1 : -1;
			ori[ iIndex ][i]= iValue;
		}

		for( int i = 0; i < vecImgFeats.size(); i++ ) // Forward
		{
			Feature3D &featImg = vecImgFeats[i];
			Feature3D &featTmp = vecFeatsBackup[i];

			// Copy to backup array
			featTmp = featImg;
			//memcpy( &(featTmp.data_zyx[0][0]), &(featImg.data_zyx[0][0]), sizeof(featImg.data_zyx) );

			// Rotate
			featTmp.RotateData( (float*)&(ori[0][0]) );

			// Project to principal coponents
			if( iPCs > 0 )
			{
				featTmp.ProjectToPCs( *pvecPCs, iPCs );
			}

#ifdef TEST_LOOKUP
			// Test lookup table
			//Feature3D feat3DCheck;
			//feat3DCheck = featImg;

			//pctable_LookupPCs( featImg, feat3DCheck, vecRotatedPC,iRot );
			pctable_LookupPCs( featImg, featTmp, vecRotatedPC,iRot );
#endif
		}

		// Fit to image
		int iInliers;
		iInliers = FitToImage( vecFeatsBackup,vecModelMatches,iModelFeatsToConsider, 0, iPCs );//pcFeatFile);
		if( iInliers > iMaxInliers )
		{
			// Save max info
			iMaxRot = iRot;
			iMaxInliers = iInliers;
			for( int i = 0; i < 3; i++ )
			{
				for( int j = 0; j < 3; j++ )
				{
					max_ori[i][j] = ori[i][j];
				}
			}
		}
	}

	// Fit best solution again to image

	for( int i = 0; i < vecImgFeats.size(); i++ )
	{
		Feature3D &featImg = vecImgFeats[i];
		Feature3D &featTmp = vecFeatsBackup[i];
		// Copy to backup array
		memcpy( &(featTmp.data_zyx[0][0]), &(featImg.data_zyx[0][0]), sizeof(featImg.data_zyx) );
		// Rotate
		featTmp.RotateData( (float*)&(max_ori[0][0]) );
		// Project to principal coponents
		if( iPCs > 0 )
		{
			featTmp.ProjectToPCs( *pvecPCs, iPCs );
		}
	}

	// Fit to image
	int iInliers;
	iInliers = FitToImage( vecFeatsBackup,vecModelMatches,iModelFeatsToConsider,pcFeatFile, iPCs);

	return 1;
}

int
ModelFeature3D::FitToImageSearchRotationsRotateModel(
		vector<Feature3D> &vecImgFeats,
		vector<int>       &vecModelMatches,
		int				  iModelFeatsToConsider,
		char *pcFeatFile,
		int					iPCs // principal components
)
#ifndef MODEL_FEATURE_INFO
{
	// Encode rotations: 6 unique axis permutations 

	int piRotations[MAX_AXIS_ROTATIONS_3D][3];
	int iRotationCount=-1;
	axis3D_generate_axis_rotations( (int *)&(piRotations[0][0]), iRotationCount );

	int iMaxInliers = -1;
	int iMaxRot = -1;
	float max_ori[3][3];
	float ori[3][3];

	// Save model feats
	vector<MODEL_FEATURE> vecFeatsBackup;
	vecFeatsBackup.resize(iModelFeatsToConsider);
	for( int i = 0; i < vecOrderToFeat.size() && i < iModelFeatsToConsider; i++ ) // Forward
	{
		int iFeat = vecOrderToFeat[i];
		MODEL_FEATURE &featModel = vecFeats[iFeat]; 
		memcpy( &(vecFeatsBackup[i].data_zyx[0][0]), &(featModel.data_zyx[0][0]), sizeof(featModel.data_zyx) );
	}

	for( int iRot = 0; iRot < MAX_AXIS_ROTATIONS_3D; iRot++ )
	{
		// Apply orientation
		for( int i = 0; i < 3; i++ )
		{
			for( int j = 0; j < 3; j++ )
			{
				ori[i][j]=0;
			}
		}

		for( int i = 0; i < 3; i++ )
		{
			int iIndex = piRotations[iRot][i] % 3;
			int iValue = piRotations[iRot][i] < 3 ? 1 : -1;
			ori[ iIndex ][i]= iValue;
		}

		for( int i = 0; i < vecOrderToFeat.size() && i < iModelFeatsToConsider; i++ ) // Forward
		{
			int iFeat = vecOrderToFeat[i];
			MODEL_FEATURE &featModel = vecFeats[iFeat];

			// Copy backup feat
			memcpy( &(featModel.data_zyx[0][0]), &(vecFeatsBackup[i].data_zyx[0][0]), sizeof(featModel.data_zyx) );
			// Rotate
			featModel.RotateData( (float*)&(ori[0][0]) );
		}

		// Fit to image
		int iInliers;
		iInliers = FitToImage( vecImgFeats,vecModelMatches,iModelFeatsToConsider, 0 );//pcFeatFile);
		if( iInliers > iMaxInliers )
		{
			// Save max info
			iMaxRot = iRot;
			iMaxInliers = iInliers;
			for( int i = 0; i < 3; i++ )
			{
				for( int j = 0; j < 3; j++ )
				{
					max_ori[i][j] = ori[i][j];
				}
			}
		}
	}

	// Fit best solution again to image

	for( int i = 0; i < vecOrderToFeat.size() && i < iModelFeatsToConsider; i++ ) // Forward
	{
		int iFeat = vecOrderToFeat[i];
		MODEL_FEATURE &featModel = vecFeats[iFeat];

		// Copy backup feat
		memcpy( &(featModel.data_zyx[0][0]), &(vecFeatsBackup[i].data_zyx[0][0]), sizeof(featModel.data_zyx) );
		// Rotate
		featModel.RotateData( (float*)&(max_ori[0][0]) );
	}

	// Fit to image
	int iInliers;
	iInliers = FitToImage( vecImgFeats,vecModelMatches,iModelFeatsToConsider,pcFeatFile);

	return 1;
}
#else
{
	return -1;
}
#endif 

int
ModelFeature3D::FitToImageRotationInvariant(
		vector<Feature3D> &vecImgFeats,
		vector<int>       &vecModelMatches,
		int				  iModelFeatsToConsider,
		char *pcFeatFile
)
{
	int iMaxInliers = -1;

	// Fit to image
	int iInliers;
	//iInliers = FitToImage( vecImgFeats,vecModelMatches,iModelFeatsToConsider,pcFeatFile, 64);

	iInliers = FitToImageNN( vecImgFeats,vecModelMatches,iModelFeatsToConsider,pcFeatFile, 64);

	return iInliers;
}

int
ModelFeature3D::FitToImage(
		vector<Feature3D> &vecImgFeats,
		vector<int>       &vecModelMatches,
		int				  iModelFeatsToConsider,
		char *pcFeatFile,
		int iPCs
		)
{
	vector<int> veciModelFeatureMatches;
	veciModelFeatureMatches.resize( vecOrderToFeat.size(), 0 );
	vecModelMatches.resize(vecImgFeats.size());

	FEATUREIO fio2;
	char pcFileName[300];
	char *pch;
	if( pcFeatFile )
	{
		sprintf(pcFileName, "%s", pcFeatFile );
		if( pch = strrchr( pcFileName, '.' ) )
		{
			*pch = 0;
		}
		fioRead( fio2, pcFileName );
	}

	if( iModelFeatsToConsider < 0 )
	{
		iModelFeatsToConsider = vecOrderToFeat.size();
	}

	
	vector<float> pfPoints0;
	vector<float> pfPoints1;
	vector<float> pfScales0;
	vector<float> pfScales1;
	vector<int> piInliers;
	vector< float > vecMatchProb;
	int iMatchCount = 0;
	// Save indices
	vector<int> vecIndex0;
	vector<int> vecIndex1;


	for( int iImgFeat = 0; iImgFeat < vecImgFeats.size(); iImgFeat++ )
	{
		MODEL_FEATURE &featInput = vecImgFeats[iImgFeat];

		// Flag as unfound...
		vecModelMatches[iImgFeat] = -1;

		int iImageFeatureToModelFeatureMatch = 0;

		// Save model matches for inspection
		vector<int> vecMM;

		// Try keeping the nearest feature instead of the most common...
		float fNearestModelFeatDist;
		int   iNearestModelFeatIndex = -1;
		int	  iFirstModelFeatIndex = -1;

		// Loop over condensed array of features
		//for( int i = 0; i < vecOrderToFeat.size(); i++ ) // Forward
		for( int i = 0; i < vecOrderToFeat.size() && i < iModelFeatsToConsider; i++ ) // Forward
		{
			int iFeat = vecOrderToFeat[i];
			MODEL_FEATURE &featModel = vecFeats[iFeat];
			float fThres = vecFeatAppearanceThresholds[iFeat];
			float fDistSqr;
			if( iPCs > 0 )
			{
				fDistSqr = featModel.DistSqrPCs( featInput, iPCs );
			}
			else
			{
				fDistSqr = featModel.MODEL_FEATURE_DIST( featInput );
			}
			if(
				//compatible_features( featModel, featInput ) &&
				//featModel.MODEL_FEATURE_DIST( featInput ) <= vecFeatAppearanceTh[iFeat]
				fDistSqr <= vecFeatAppearanceThresholds[iFeat]
			)
			{
				iImageFeatureToModelFeatureMatch++; // Same image feature, count model feature matches
				veciModelFeatureMatches[i]++; // Same model feature, count image feature matches.
				vecMM.push_back( iFeat );

				pfPoints0.push_back(featModel.x);
				pfPoints0.push_back(featModel.y);
				pfPoints0.push_back(featModel.z);

				pfPoints1.push_back(featInput.x);
				pfPoints1.push_back(featInput.y);
				pfPoints1.push_back(featInput.z);

				pfScales0.push_back( featModel.scale );
				pfScales1.push_back( featInput.scale );

				vecIndex0.push_back(iFeat);
				vecIndex1.push_back(iImgFeat);

				vecMatchProb.push_back( 1 );


				iMatchCount++;

				// Save nearest match
				if( iNearestModelFeatIndex == -1 )
				{
					fNearestModelFeatDist = featModel.MODEL_FEATURE_DIST( featInput );
					iNearestModelFeatIndex = i;
					iFirstModelFeatIndex = i;
				}
				else if( featModel.MODEL_FEATURE_DIST( featInput ) < fNearestModelFeatDist )
				{
					fNearestModelFeatDist = featModel.MODEL_FEATURE_DIST( featInput );
					iNearestModelFeatIndex = i;
				}
			}
		}

		if( iFirstModelFeatIndex > -1 )
		{
			//int iDecisionIndex = iFirstModelFeatIndex; // When only ones are used in Bayesian classification, this works best
			int iDecisionIndex = iNearestModelFeatIndex; // Try this for registration

			// Set to model feature.
			vecModelMatches[iImgFeat] = iDecisionIndex;
			//m_prob_test[iDecisionIndex].value = 1;
			//assert( m_prob_test[iDecisionIndex].index == iDecisionIndex );

			// Count samples
			int iCount = FeaturesWithinThreshold( vecOrderToFeat[iDecisionIndex] );

			// Count matches
			//iMatchCount++;

		}
	}

	// Now determine transform

	float pfModelCenter[3];
	pfModelCenter[0] = m_piModelImageSizeXYZ[0]/2.0f;
	pfModelCenter[1] = m_piModelImageSizeXYZ[1]/2.0f;
	pfModelCenter[2] = m_piModelImageSizeXYZ[2]/2.0f;

	// Output parameters, the result of determine_similarity_transform_ransac()
	float rot[3*3];
	float pfModelCenter2[3];
	float fScaleDiff=1;

	// Save inliers
	for( int iImgFeat = 0; iImgFeat < vecModelMatches.size(); iImgFeat++ )
	{
		vecModelMatches[iImgFeat] = -1;
	}
	int iInliers = -1;
	if( iMatchCount >= 3 )
	{
		piInliers.resize(iMatchCount,0);
		iInliers = 
		determine_similarity_transform_ransac(
			&(pfPoints0[0]), &(pfPoints1[0]),
			&(pfScales0[0]), &(pfScales1[0]),
			&(vecMatchProb[0]),
			iMatchCount,  100, pfModelCenter,
			pfModelCenter2, rot, fScaleDiff, &(piInliers[0])
			);

		// Create a mask
		//FEATUREIO fioMaskModel;
		//FEATUREIO fioMaskImage;

		//if( pcFeatFile )
		//{
		//	fioMaskModel = fioMaskImage = fio2;

		//	fioRead( fioMaskModel, vecImgName[0] );

		//	//fioAllocate( fioMaskModel );
		//	fioAllocate( fioMaskImage );
		//	fioSet( fioMaskModel, 0 );
		//	fioSet( fioMaskImage, 0 );
		//}

		// Output inliers
		for( int i = 0; i < iMatchCount; i++ )
		{
			//piInliers[i]=1;
			if( piInliers[i] && pcFeatFile )
			{
				sprintf( pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_1", i, vecIndex0[i], vecIndex1[i] );
				OutputFeatureInVolume( vecIndex0[i], pcFileName );
				MODEL_FEATURE &featInput = vecImgFeats[ vecIndex1[i] ];
				sprintf( pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_2", i, vecIndex0[i], vecIndex1[i] );
				featInput.OutputVisualFeatureInVolume( fio2, pcFileName, 1, 1 );

				//vecFeats[ vecIndex0[i] ].PaintVisualFeatureInVolume( fioMaskModel, 1 );
				//featInput.PaintVisualFeatureInVolume( fioMaskImage, 1 );

				// Save feature/model match
				vecModelMatches[ vecIndex1[i] ] = vecIndex0[i];
			}
		}

		//if( pcFeatFile )
		//{
		//	fioWrite( fioMaskModel, "resultMaskModel" );
		//	fioWrite( fioMaskImage, "resultMaskImage" );
		//	fioDelete( fioMaskModel );
		//	fioDelete( fioMaskImage );
		//}

		// Output entire transformed image
		// If this works the first time I'll eat my shorts...
		if( pcFeatFile )
		{
			FEATUREIO fioOut;

			fioOut.t = 1;
			fioOut.x = m_piModelImageSizeXYZ[0];
			fioOut.y = m_piModelImageSizeXYZ[1];
			fioOut.z = m_piModelImageSizeXYZ[2];
			fioOut.iFeaturesPerVector = 1;
			fioOut.pfMeans = fioOut.pfVarrs = fioOut.pfVectors = 0;

			fioAllocate( fioOut );

			//fioWrite( fioOut, "transformed_in" );

			similarity_transform_image(
				fioOut, fio2,
				pfModelCenter, pfModelCenter2,
				rot, fScaleDiff );

			//fioWrite( fio2, "transformed_fio2" );
			sprintf(pcFileName, "%s.talairach_inliers%3.3d", pcFeatFile, iInliers );
			fioWrite( fioOut, pcFileName );

			fioDelete( fioOut );
		}
	}

	FILE *outfile = fopen( "result.txt", "a+" );
	fprintf( outfile, "%d\t%d\n", iMatchCount, iInliers );
	fclose( outfile );


	//FILE *outfile = fopen( "modelfeaturematches.txt", "wt" );
	//for( int i = 0; i <veciModelFeatureMatches.size(); i++ )
	//{
	//	fprintf( outfile, "%d\t%f\n", veciModelFeatureMatches[i], m_prob_test[i].value );
	//	//veciModelFeatureMatches[i] = 0;
	//}
	//fclose( outfile );

	if( pcFeatFile )
	{
		fioDelete( fio2 );
	}

	return iInliers;
}

int
ModelFeature3D::FitToImageNN(
		vector<Feature3D> &vecImgFeats,
		vector<int>       &vecModelMatches,
		int				  iModelFeatsToConsider,
		char *pcFeatFile,
		int iPCs
		)
{
	vector<int> veciModelFeatureMatches;
	veciModelFeatureMatches.resize( vecOrderToFeat.size(), 0 );
	vecModelMatches.resize(vecImgFeats.size());

	FEATUREIO fio2;
	char pcFileName[300];
	char *pch;
	if( pcFeatFile )
	{
		sprintf(pcFileName, "%s", pcFeatFile );
		if( pch = strrchr( pcFileName, '.' ) )
		{
			*pch = 0;
		}
		if( pch = strstr( pcFileName, ".txt.trunc" ) )
		{
			*pch = 0;
		}
		fioRead( fio2, pcFileName );
		float fMax, fMin;
		fioFindMax( fio2, fMax );
		fioFindMin( fio2, fMin );
		//fioRead( fio2, "C:\\downloads\\data\\tmi-data\\bin\\oasis-avg100.txt" );
	}

	if( iModelFeatsToConsider < 0 )
	{
		iModelFeatsToConsider = vecOrderToFeat.size();
	}

	
	vector<float> pfPoints0;
	vector<float> pfPoints1;
	vector<float> pfScales0;
	vector<float> pfScales1;
	vector<float> pfOris0;
	vector<float> pfOris1;

	vector<int> piInliers;
	int iMatchCount = 0;
	// Save indices
	vector<int> vecIndex0;
	vector<int> vecIndex1;

	// Generate vector of model features
	vector<Feature3D> vecFeatsModel;
	for( int i = 0; i < vecOrderToFeat.size() && i < iModelFeatsToConsider; i++ ) // Forward
	{
			int iFeat = vecOrderToFeat[i];
			Feature3D featModel;
			featModel.InitFromFeature3DInfo( vecFeats[iFeat] );
			vecFeatsModel.push_back(featModel);
	}

	vector<int>		vecMatchIndex12;
	vector<float>	vecMatchDist12;
	msComputeNearestNeighborDistanceRatio( vecFeatsModel, vecImgFeats, vecMatchIndex12, vecMatchDist12, 64 );

	vector< pair<int,float> > vecBestMatches;
	for( int i = 0; i < vecMatchIndex12.size(); i++ )
	{
		vecBestMatches.push_back( pair<int,float>(i,vecMatchDist12[i]));
	}
	sort( vecBestMatches.begin(), vecBestMatches.end(), pairIntFloatLeastToGreatest );

	for( int iImgFeat = 0; iImgFeat < vecImgFeats.size(); iImgFeat++ )
	{
		MODEL_FEATURE &featInput = vecImgFeats[iImgFeat];

		// Flag as unfound...
		vecModelMatches[iImgFeat] = -1;
	}

	vector< float > vecMatchProb;

	map<float,int> mapModelXtoIndex;
	map<float,int> mapModelXtoIndexModel;
	for( int i = 0; i < vecBestMatches.size() && i < 300; i++ )
	{
		int iIndex = vecBestMatches[i].first;
		float fDist = vecBestMatches[i].second;
		if( fDist > 0.8 )
		{
			//break;
		}

		Feature3DInfo &featModel = vecFeatsModel[iIndex];
		Feature3DInfo &featInput = vecImgFeats[ vecMatchIndex12[iIndex] ];

		map<float,int>::iterator itXI = mapModelXtoIndex.find( featInput.x );
		if( itXI != mapModelXtoIndex.end() )
		{
			int iPrevModelIndex = itXI->second;
			Feature3DInfo &featInputPrev = vecImgFeats[iPrevModelIndex];
			if(
				featInputPrev.x == featInput.x && 
				featInputPrev.y == featInput.y && 
				featInputPrev.z == featInput.z )
			{
				// Match to same location/scale as another feature
				// skip
				itXI = mapModelXtoIndexModel.find( featInput.x );
				iPrevModelIndex = itXI->second;
				Feature3DInfo &featInputPrevModel = vecFeatsModel[iPrevModelIndex];
				if(
					featInputPrevModel.x == featModel.x && 
					featInputPrevModel.y == featModel.y && 
					featInputPrevModel.z == featModel.z )
				{
					continue;
				}
			}
		}
		// Insert into map
		mapModelXtoIndex.insert( pair<float,int>(featInput.x, vecMatchIndex12[iIndex]) );
		mapModelXtoIndexModel.insert( pair<float,int>(featInput.x, iIndex) );


		// Should not allow features with same spatial location

		pfPoints0.push_back(featModel.x);
		pfPoints0.push_back(featModel.y);
		pfPoints0.push_back(featModel.z);



		pfPoints1.push_back(featInput.x);
		pfPoints1.push_back(featInput.y);
		pfPoints1.push_back(featInput.z);

		pfScales0.push_back( featModel.scale );
		pfScales1.push_back( featInput.scale );

		for( int o1=0;o1<3;o1++)
		{
			for( int o2=0;o2<3;o2++ )
			{
				pfOris0.push_back( featModel.ori[o1][o2] );
				pfOris1.push_back( featInput.ori[o1][o2] );
			}
		}

		vecIndex0.push_back( iIndex );
		vecIndex1.push_back( vecMatchIndex12[iIndex] );

		// Save prior probability of this feature
		float fFeatsInThreshold = 1;//FeaturesWithinThreshold( vecOrderToFeat[ iIndex ] );
		vecMatchProb.push_back( fFeatsInThreshold );

		iMatchCount++;
	}

	// Now determine transform

	float pfModelCenter[3];
	pfModelCenter[0] = m_piModelImageSizeXYZ[0]/2.0f;
	pfModelCenter[1] = m_piModelImageSizeXYZ[1]/2.0f;
	pfModelCenter[2] = m_piModelImageSizeXYZ[2]/2.0f;

	// Output parameters, the result of determine_similarity_transform_ransac()
	float rot[3*3];
	float pfModelCenter2[3];
	float fScaleDiff=1;

	// Save inliers
	for( int iImgFeat = 0; iImgFeat < vecModelMatches.size(); iImgFeat++ )
	{
		vecModelMatches[iImgFeat] = -1;
	}
	int iInliers = -1;

	vector< int > vecModelImgFeatCounts;
	vecModelImgFeatCounts.resize( vecImgName.size(), 0 );
	int iModelImgFeatMaxCount = 0;
	int iModelImgFeatMaxCountIndex = 0;
	if( iMatchCount >= 3 )
	{
		piInliers.resize(iMatchCount,0);
		iInliers = 
		//determine_similarity_transform_ransac(
		//	&(pfPoints0[0]), &(pfPoints1[0]),
		//	&(pfScales0[0]), &(pfScales1[0]),
		//	&(vecMatchProb[0]),
		//	iMatchCount,  500, pfModelCenter,
		//	pfModelCenter2, rot, fScaleDiff, &(piInliers[0])
		//	);
		determine_similarity_transform_hough(
			&(pfPoints0[0]), &(pfPoints1[0]),
			&(pfScales0[0]), &(pfScales1[0]),
			&(pfOris0[0]), &(pfOris1[0]),
			&(vecMatchProb[0]),
			iMatchCount,  500, pfModelCenter,
			pfModelCenter2, rot, fScaleDiff, &(piInliers[0])
			);

		vector<float> pfPointsIn0;
		vector<float> pfPointsIn1;
		vector<float> pfScalesIn0;
		vector<float> pfScalesIn1;

		FEATUREIO fio3;
		fioRead( fio3, "C:\\downloads\\data\\tmi-data\\bin\\oasis-avg100.txt" );

		// Output inliers
		for( int i = 0; i < iMatchCount; i++ )
		{
			//piInliers[i]=1;
			if( piInliers[i] && pcFeatFile )
			{
				sprintf( pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_1", i, vecIndex0[i], vecIndex1[i] );
//				OutputFeatureInVolume( vecOrderToFeat[ vecIndex0[i] ], pcFileName );

				//MODEL_FEATURE &featModel = vecFeats[ vecOrderToFeat[ vecIndex0[i] ] ];
				//sprintf( pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_3", i, vecIndex0[i], vecIndex1[i] );
				//featModel.OutputVisualFeatureInVolume( fio3, pcFileName, 1, 1 );

				//MODEL_FEATURE &featModel = vecFeatsModel[ vecIndex0[i] ];
				//featModel.OutputVisualFeatureInVolume( fio2, pcFileName, 1, 1 );

				MODEL_FEATURE &featInput = vecImgFeats[ vecIndex1[i] ];
				sprintf( pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_2", i, vecIndex0[i], vecIndex1[i] );
//				featInput.OutputVisualFeatureInVolume( fio2, pcFileName, 1, 1 );

				//vecFeats[ vecIndex0[i] ].PaintVisualFeatureInVolume( fioMaskModel, 1 );
				//featInput.PaintVisualFeatureInVolume( fioMaskImage, 1 );

				// Save feature/model match
				vecModelMatches[ vecIndex1[i] ] = vecIndex0[i];

				// Save inliers for later
				pfPointsIn0.push_back(pfPoints0[i*3+0]);
				pfPointsIn0.push_back(pfPoints0[i*3+1]);
				pfPointsIn0.push_back(pfPoints0[i*3+2]);

				pfPointsIn1.push_back(pfPoints1[i*3+0]);
				pfPointsIn1.push_back(pfPoints1[i*3+1]);
				pfPointsIn1.push_back(pfPoints1[i*3+2]);

				pfScalesIn0.push_back( pfScales0[i] );
				pfScalesIn1.push_back( pfScales1[i] );

				// Count number of inliers per model feature
				int iModelFeatIndex = vecOrderToFeat[ vecIndex0[i] ];
				for( int i1 = 0; i1 < vecFeatToIndexInfoCount[iModelFeatIndex]; i1++ )
				{
					IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iModelFeatIndex]+i1];
					int iFeat2 = II1.Index();
					int iImg2 = vecFeatToImgName[iFeat2];

					float fThres = vecFeatAppearanceThresholds[iModelFeatIndex];

					if( II1.DistSqr() <= vecFeatAppearanceThresholds[iModelFeatIndex] )
					{
						vecModelImgFeatCounts[iImg2]++;

						// Save model image with most inliers
						if( vecModelImgFeatCounts[iImg2] > iModelImgFeatMaxCount )
						{
							iModelImgFeatMaxCountIndex = iImg2;
							iModelImgFeatMaxCount = vecModelImgFeatCounts[iImg2];
						}
					}
				}
			}
		}

		// Now save points in image with most inliers
		vector<float> pfPointsInImg0;
		vector<float> pfPointsInImg1;
		vector<float> pfScalesInImg0;
		vector<float> pfScalesInImg1;
		for( int i = 0; i < iMatchCount; i++ )
		{
			if( piInliers[i] && pcFeatFile )
			{
				// Count number of inliers per model feature
				int iModelFeatIndex = vecOrderToFeat[ vecIndex0[i] ];
				for( int i1 = 0; i1 < vecFeatToIndexInfoCount[iModelFeatIndex]; i1++ )
				{
					IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iModelFeatIndex]+i1];
					int iFeat2 = II1.Index();
					int iImg2 = vecFeatToImgName[iFeat2];

					float fThres = vecFeatAppearanceThresholds[iModelFeatIndex];

					if( II1.DistSqr() <= vecFeatAppearanceThresholds[iModelFeatIndex]
						&& iImg2 == iModelImgFeatMaxCountIndex 
						)
					{
						// Save inliers for later
						pfPointsInImg0.push_back(pfPoints0[i*3+0]);
						pfPointsInImg0.push_back(pfPoints0[i*3+1]);
						pfPointsInImg0.push_back(pfPoints0[i*3+2]);

						pfPointsInImg1.push_back(pfPoints1[i*3+0]);
						pfPointsInImg1.push_back(pfPoints1[i*3+1]);
						pfPointsInImg1.push_back(pfPoints1[i*3+2]);

						pfScalesInImg0.push_back( pfScales0[i] );
						pfScalesInImg1.push_back( pfScales1[i] );
					}
				}
			}
		}

		//if( pcFeatFile )
		//{
		//	fioWrite( fioMaskModel, "resultMaskModel" );
		//	fioWrite( fioMaskImage, "resultMaskImage" );
		//	fioDelete( fioMaskModel );
		//	fioDelete( fioMaskImage );
		//}

		// Output entire transformed image
		// If this works the first time I'll eat my shorts...
		if( pcFeatFile )
		{
			FEATUREIO fioOut;

			fioOut.t = 1;
			fioOut.x = m_piModelImageSizeXYZ[0];
			fioOut.y = m_piModelImageSizeXYZ[1];
			fioOut.z = m_piModelImageSizeXYZ[2];
			fioOut.iFeaturesPerVector = 1;
			fioOut.pfMeans = fioOut.pfVarrs = fioOut.pfVectors = 0;

			fioAllocate( fioOut );

			//fioWrite( fioOut, "transformed_in" );

			similarity_transform_image(
				fioOut, fio2,
				pfModelCenter, pfModelCenter2,
				rot, fScaleDiff );

			//fioWrite( fio2, "transformed_fio2" );
			sprintf(pcFileName, "%s.talairach_inliers%3.3d", pcFeatFile, iInliers );
			fioWrite( fioOut, pcFileName );

			// Here, perform correlation between average atlas and resampled image
			vector<float> pfPointsIn1New;
			vector<float> pfScalesIn1New;
			pfPointsIn1New.resize(pfPointsIn1.size());
			pfScalesIn1New.resize(pfScalesIn1.size());

			similarity_transform_feature_list(
				pfModelCenter, pfModelCenter2,
				rot, fScaleDiff ,
				&(pfPointsIn1New[0]), &(pfPointsIn1[0]),
					&(pfScalesIn1New[0]), &(pfScalesIn1[0]),
					iInliers );

			printf( "Warning ... refine_feature_matches() has be rewritten, test this code!!\n" );
			refine_feature_matches( fio3, fioOut,
				&(pfPointsIn0[0]), &(pfPointsIn1New[0]),
					&(pfScalesIn0[0]), &(pfScalesIn1New[0]),
					iInliers, 11
					   );

			// Determine new scale factor

			if( iInliers >= 3 )
			{
				float rotIn[3*3];
				float pfModelCenter2In[3];
				float fScaleDiffIn=1;
				int iInliersRefined = refine_similarity_transform_ransac(
					&(pfPointsIn0[0]), &(pfPointsIn1[0]),
					&(pfScalesIn0[0]), &(pfScalesIn1[0]),
					&(vecMatchProb[0]),
					iInliers, 1000, pfModelCenter,
					pfModelCenter2In, rotIn, fScaleDiffIn, &(piInliers[0])
					);

				similarity_transform_image(
					fioOut, fio2,
					pfModelCenter, pfModelCenter2In,
					rotIn, fScaleDiffIn );
	
				//fioWrite( fio2, "transformed_fio2" );
				sprintf(pcFileName, "%s.talairach_inliers_ref%3.3d", pcFeatFile, iInliersRefined );

				fioWrite( fioOut, pcFileName );
			}
			
			// Try transform with features in a single model image
			if( iModelImgFeatMaxCount >= 3 )
			{
				float rotIn[3*3];
				float pfModelCenter2In[3];
				float fScaleDiffIn=1;
				int iInliersRefined = refine_similarity_transform_ransac(
					&(pfPointsInImg0[0]), &(pfPointsInImg1[0]),
					&(pfScalesInImg0[0]), &(pfScalesInImg1[0]),
					&(vecMatchProb[0]),
					iModelImgFeatMaxCount, 1000, pfModelCenter,
					pfModelCenter2In, rotIn, fScaleDiffIn, &(piInliers[0])
					);

				similarity_transform_image(
					fioOut, fio2,
					pfModelCenter, pfModelCenter2In,
					rotIn, fScaleDiffIn );

				//fioWrite( fio2, "transformed_fio2" );
				sprintf(pcFileName, "%s.talairach_inliers_ref_oneimage%3.3d", pcFeatFile, iInliersRefined );

				fioWrite( fioOut, pcFileName );
			}

			fioDelete( fioOut );
		}

	}

	FILE *outfile = fopen( "result.txt", "a+" );
	fprintf( outfile, "%d\t%d\n", iMatchCount, iInliers );
	fclose( outfile );




	//FILE *outfile = fopen( "modelfeaturematches.txt", "wt" );
	//for( int i = 0; i <veciModelFeatureMatches.size(); i++ )
	//{
	//	fprintf( outfile, "%d\t%f\n", veciModelFeatureMatches[i], m_prob_test[i].value );
	//	//veciModelFeatureMatches[i] = 0;
	//}
	//fclose( outfile );

	if( pcFeatFile )
	{
		fioDelete( fio2 );
	}

	return iInliers;
}


//int
//ModelFeature3D::FitToImageOneMatchPerFeature(
//		vector<Feature3D> &vecImgFeats,
//		vector<int>       &vecModelMatches,
//		int				  iModelFeatsToConsider,
//		char *pcFeatFile
//		)
//{
//	vector<int> veciModelFeatureMatches;
//	veciModelFeatureMatches.resize( vecOrderToFeat.size(), 0 );
//	vecModelMatches.resize(vecImgFeats.size());
//
//	FEATUREIO fio2;
//	char pcFileName[300];
//	char *pch;
//	if( pcFeatFile )
//	{
//		sprintf(pcFileName, "%s", pcFeatFile );
//		if( pch = strrchr( pcFileName, '.' ) )
//		{
//			*pch = 0;
//		}
//		fioRead( fio2, pcFileName );
//	}
//
//	if( iModelFeatsToConsider < 0 )
//	{
//		iModelFeatsToConsider = vecOrderToFeat.size();
//	}
//
//	int iMatchCount = 0;
//
//	for( int iImgFeat = 0; iImgFeat < vecImgFeats.size(); iImgFeat++ )
//	{
//		MODEL_FEATURE &featInput = vecImgFeats[iImgFeat];
//
//		// Flag as unfound...
//		vecModelMatches[iImgFeat] = -1;
//
//		int iImageFeatureToModelFeatureMatch = 0;
//
//		// Save model matches for inspection
//		vector<int> vecMM;
//
//		// Try keeping the nearest feature instead of the most common...
//		float fNearestModelFeatDist;
//		int   iNearestModelFeatIndex = -1;
//		int	  iFirstModelFeatIndex = -1;
//
//		// Loop over condensed array of features
//		//for( int i = 0; i < vecOrderToFeat.size(); i++ ) // Forward
//		for( int i = 0; i < vecOrderToFeat.size() && i < iModelFeatsToConsider; i++ ) // Forward
//		{
//			int iFeat = vecOrderToFeat[i];
//			MODEL_FEATURE &featModel = vecFeats[iFeat];
//			if(
//				//compatible_features( featModel, featInput ) &&
//				featModel.MODEL_FEATURE_DIST( featInput ) <= vecFeatAppearanceThresholds[iFeat]
//			)
//			{
//				iImageFeatureToModelFeatureMatch++; // Same image feature, count model feature matches
//				veciModelFeatureMatches[i]++; // Same model feature, count image feature matches.
//				vecMM.push_back( iFeat );
//
//				// Save nearest match
//				if( iNearestModelFeatIndex == -1 )
//				{
//					fNearestModelFeatDist = featModel.MODEL_FEATURE_DIST( featInput );
//					iNearestModelFeatIndex = i;
//					iFirstModelFeatIndex = i;
//				}
//				else if( featModel.MODEL_FEATURE_DIST( featInput ) < fNearestModelFeatDist )
//				{
//					fNearestModelFeatDist = featModel.MODEL_FEATURE_DIST( featInput );
//					iNearestModelFeatIndex = i;
//				}
//			}
//		}
//
//		if( iFirstModelFeatIndex > -1 )
//		{
//			//int iDecisionIndex = iFirstModelFeatIndex; // When only ones are used in Bayesian classification, this works best
//			int iDecisionIndex = iNearestModelFeatIndex; // Try this for registration
//
//			// Set to model feature.
//			vecModelMatches[iImgFeat] = iDecisionIndex;
//			//m_prob_test[iDecisionIndex].value = 1;
//			//assert( m_prob_test[iDecisionIndex].index == iDecisionIndex );
//
//			// Count samples
//			int iCount = FeaturesWithinThreshold( vecOrderToFeat[iDecisionIndex] );
//
//			// Count matches
//			iMatchCount++;
//
//		}
//	}
//
//	float *pfPoints0 = new float[iMatchCount*3];
//	float *pfPoints1 = new float[iMatchCount*3];
//	float *pfScales0 = new float[iMatchCount];
//	float *pfScales1 = new float[iMatchCount];
//	int	*piInliers = new int[iMatchCount];
//
//	// Save indices
//	vector<int> vecIndex0;
//	vector<int> vecIndex1;
//
//	int iCurrMatch = 0;
//	for( int iImgFeat = 0; iImgFeat < vecModelMatches.size(); iImgFeat++ )
//	{
//		if( vecModelMatches[iImgFeat] > -1 )
//		{
//			iCurrMatch;
//			
//			MODEL_FEATURE &featInput = vecImgFeats[iImgFeat];
//
//			int iFeatOrderIndex = vecModelMatches[iImgFeat];
//			int iModFeat = vecOrderToFeat[iFeatOrderIndex];
//			MODEL_FEATURE &featModel = vecFeats[iModFeat];
//
//			pfPoints0[iCurrMatch*3+0] = featModel.x;
//			pfPoints0[iCurrMatch*3+1] = featModel.y;
//			pfPoints0[iCurrMatch*3+2] = featModel.z;
//
//			pfPoints1[iCurrMatch*3+0] = featInput.x;
//			pfPoints1[iCurrMatch*3+1] = featInput.y;
//			pfPoints1[iCurrMatch*3+2] = featInput.z;
//
//			pfScales0[iCurrMatch] = featModel.scale;
//			pfScales1[iCurrMatch] = featInput.scale;
//
//			vecIndex0.push_back(iModFeat);
//			vecIndex1.push_back(iImgFeat);
//
//			iCurrMatch++;
//		}
//	}
//
//	// Now determine transform
//
//	float pfModelCenter[3];
//	pfModelCenter[0] = m_piModelImageSizeXYZ[0]/2.0f;
//	pfModelCenter[1] = m_piModelImageSizeXYZ[1]/2.0f;
//	pfModelCenter[2] = m_piModelImageSizeXYZ[2]/2.0f;
//
//	float rot0[3*3];
//	float rot1[3*3];
//	float fScaleDiff=1;
//
//	// Save inliers
//	for( int iImgFeat = 0; iImgFeat < vecModelMatches.size(); iImgFeat++ )
//	{
//		vecModelMatches[iImgFeat] = -1;
//	}
//	int iInliers = -1;
//	if( iMatchCount >= 3 )
//	{
//		iInliers = 
//		determine_similarity_transform_ransac(
//			pfPoints0, pfPoints1,
//			pfScales0, pfScales1,
//			iMatchCount,  100, pfModelCenter,
//			rot0, rot1, fScaleDiff, piInliers
//			);
//
//		// Output inliers
//		for( int i = 0; i < iMatchCount; i++ )
//		{
//			if( piInliers[i] && pcFeatFile )
//			{
//				sprintf( pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_1", i, vecIndex0[i], vecIndex1[i] );
//				OutputFeatureInVolume( vecIndex0[i], pcFileName );
//				MODEL_FEATURE &featInput = vecImgFeats[ vecIndex1[i] ];
//				sprintf( pcFileName, "feat_%3.3d_mod%4.4d_img%4.4d_2", i, vecIndex0[i], vecIndex1[i] );
//				featInput.OutputVisualFeatureInVolume( fio2, pcFileName, 1, 1 );
//
//				// Save feature/model match
//				vecModelMatches[ vecIndex1[i] ] = vecIndex0[i];
//			}
//		}
//	}
//
//	//FILE *outfile = fopen( "modelfeaturematches.txt", "wt" );
//	//for( int i = 0; i <veciModelFeatureMatches.size(); i++ )
//	//{
//	//	fprintf( outfile, "%d\t%f\n", veciModelFeatureMatches[i], m_prob_test[i].value );
//	//	//veciModelFeatureMatches[i] = 0;
//	//}
//	//fclose( outfile );
//
//	if( pcFeatFile )
//	{
//		fioDelete( fio2 );
//	}
//	delete [] pfPoints0;
//	delete [] pfPoints1;
//	delete [] pfScales0;
//	delete [] pfScales1;
//	delete [] piInliers;
//
//	return iInliers;
//}


int
ModelFeature3D::DetermineOrientation(
									 )
{
	//// Orient all features

	for( int i = 0; i < vecOrderToFeat.size(); i++ )
	{
		int iFeat = vecOrderToFeat[i];
	//for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	//{
		int iCode = iFeat;
		//vecFeats[iFeat].DetermineOrientationCubeIntensity( iCode, (float*)&(vecFeats[iFeat].ori[0][0]) );
		//vecFeats[iFeat].DetermineOrientationCube( iCode, (float*)&(vecFeats[iFeat].ori[0][0]) );
	}
	return 1;

	for( int i = 0; i < vecOrderToFeat.size(); i++ )
	{
		int iFeat = vecOrderToFeat[i];
		for( int i1 = 0; i1 < vecFeatToIndexInfoCount[iFeat]; i1++ )
		{
			IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i1];
			int iFeat2 = II1.Index();
			MODEL_FEATURE &feat = vecFeats[iFeat2];

			char pcNewImageName[400];

			if( i1 == 0 )
			{
				FEATUREIO fio;
				int iImg = vecFeatToImgName[iFeat];
				if( fioRead( fio, vecImgName[iImg] ) )
				{
					sprintf( pcNewImageName, "feat%4.4d_img%4.4d_index%6.6d_vol", i, iImg, i1 );
					feat.OutputVisualFeatureInVolume( fio, pcNewImageName, 1 );
					fioDelete( fio );
				}
			}

			int iImg2 = vecFeatToImgName[iFeat2];

			float fThres = vecFeatAppearanceThresholds[iFeat];

			if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat] && i1 < 5 )
			{
				int iCode = i*100+i1;
				//feat.DetermineOrientationCubeIntensity( iCode, (float*)&(feat.ori[0][0]) );

				sprintf( pcNewImageName, "feat%4.4d_img%4.4d_index%6.6d", i, iImg2, i1 );
				//feat.OutputVisualFeature( pcNewImageName );
			}
		}
	}

	return 1;
}

int
ModelFeature3D::FitToImageBlackAndWhite(
		vector<Feature3D> &vecImgFeats,
		vector<int>       &vecModelMatches,
		int				  iModelFeatsToConsider,
		char *pcFeatFile
		)
{
	FILE *outfile = fopen( "points_image.txt", "wt" );
	for( int i = 0; i < vecImgFeats.size(); i++ )
	{
		fprintf( outfile, "%f\n", vecImgFeats[i].scale );
	}
	fclose( outfile );

	outfile = fopen( "points_model.txt", "wt" );
	for( int i = 0; i < vecOrderToFeat.size() && i < iModelFeatsToConsider; i++ )
	{
		int iFeat = vecOrderToFeat[i];
		MODEL_FEATURE &feat = vecFeats[iFeat];
		fprintf( outfile, "%f\n", vecFeats[iFeat].scale );
	}
	fclose( outfile );

	// Generate inter-model-feature distance map
	vector< vector<float> > vvDistMap;
	vvDistMap.resize( iModelFeatsToConsider*iModelFeatsToConsider );
	for( int i = 0; i < vecOrderToFeat.size() && i < iModelFeatsToConsider; i++ )
	{
		int iFeat = vecOrderToFeat[i];
		MODEL_FEATURE &feat = vecFeats[iFeat];
		for( int j = 0; j < vecOrderToFeat.size() && j < iModelFeatsToConsider; j++)
		{
		}
	}

	// Open FEATUREIO for output
	FEATUREIO fio2;
	char pcFileName[400];
	char *pch;
	if( pcFeatFile )
	{
		sprintf(pcFileName, "%s", pcFeatFile );
		if( pch = strrchr( pcFileName, '.' ) )
		{
			*pch = 0;
		}
		fioRead( fio2, pcFileName );
	}


	// Level 1
	// Possible constraints:
	//
	//  - agreement of real-world image size (e.g. mm). This is often known in advance.
	//  - agreement in appearance polarity: black or white - this should hold for same modality, but can be relaxed for multimodal.
	//

	int iCandidateSol = 0;

	for( int p11 = 0; p11 < iModelFeatsToConsider; p11++ )
	{
		int iFeat11 = vecOrderToFeat[p11];
		MODEL_FEATURE &feat11 = vecFeats[iFeat11];

		for( int p21 = 0; p21 < vecImgFeats.size(); p21++ )
		{
			int iFeat21 = p21;
			MODEL_FEATURE &feat21 = vecImgFeats[iFeat21];

			float fLogScaleDiff_11_21 = log( feat11.scale ) - log( feat21.scale );

			// Level 2
			// Possible constraints:
			//
			// c2: agreement of relative feature sizes, distances
			//
			for( int p12 = p11+1; p12 < iModelFeatsToConsider; p12++ )
			{
				int iFeat12 = vecOrderToFeat[p12];
				MODEL_FEATURE &feat12 = vecFeats[iFeat12];

				float fLogDist_11_12 = log( feat11.MODEL_FEATURE_DIST( feat12 ) );

				for( int p22 = 0; p22 < vecImgFeats.size(); p22++ )
				{
					int iFeat22 = p22;
					MODEL_FEATURE &feat22 = vecImgFeats[iFeat22];

					float fLogScaleDiff_12_22 = log( feat12.scale ) - log( feat22.scale );

					float fLogDist_21_22 = log( feat21.MODEL_FEATURE_DIST( feat22 ) );
					float fLogDistDiff_1_2 = fLogDist_11_12 - fLogDist_21_22;

					// Here we have 3 distance/size ratios: they should all be approximately the equal.
					// Thresholds are set to 1.5, ideally distributions over these values could be learned.

					if(
						fabs( fLogScaleDiff_11_21 - fLogScaleDiff_12_22 ) > 1.5f ||
						fabs( fLogScaleDiff_11_21 - fLogDistDiff_1_2 ) > 1.5f ||
						fabs( fLogScaleDiff_12_22 - fLogDistDiff_1_2 ) > 1.5f )
					{
						continue;
					}

					// Here we should have a nice pair of image features - let's see how they look

					//sprintf( pcFileName, "feat_%3.3d_mod%3.3d_img%3.3d_1", iCandidateSol, p11, p21 );
					//OutputFeatureInVolume( iFeat11, pcFileName );
					//sprintf( pcFileName, "feat_%3.3d_mod%3.3d_img%3.3d_2", iCandidateSol, p11, p21 );
					//feat21.OutputVisualFeatureInVolume( fio2, pcFileName, 1, 1 );

					//sprintf( pcFileName, "feat_%3.3d_mod%3.3d_img%3.3d_1", iCandidateSol, p12, p22 );
					//OutputFeatureInVolume( iFeat12, pcFileName );
					//sprintf( pcFileName, "feat_%3.3d_mod%3.3d_img%3.3d_2", iCandidateSol, p12, p22 );
					//feat22.OutputVisualFeatureInVolume( fio2, pcFileName, 1, 1 );

					iCandidateSol++;


					// Level 3
					// Possible constraints:
					//
					// c2: agreement of relative feature sizes, distances
					//
					for( int p13 = p12+1; p13 < iModelFeatsToConsider; p13++ )
					{


						for( int p23 = 0; p23 < vecImgFeats.size(); p23++ )
						{
						}
					}
				}
			}
		}
	}
	return 1;
}

//int
//countMatchingBits(
//				 __int64 *pvecCodeList0,
//				 __int64 *pvecCodeList1,
//				 int	*piMatch
//				 )
//{
//
//	for( int i = 0; i < 64; i++ )
//	{
//		int iMatchCount = 0;
//		__int64 iAnd = pvecCodeList0[i] & pvecCodeList1[i];
//		for( int j = 0; j < 64; j++ )
//		{
//			__int64 iBitAnd = 1;
//			iBitAnd <<= j;
//			__int64 iBitAndAnd = iAnd & iBitAnd;
//			if( iBitAndAnd )
//			{
//				iMatchCount++;
//			}
//		}
//		piMatch[i] = iMatchCount;
//	}
//	return 0;
//}

int
ModelFeature3D::LoopThroughFeaturesForFun(
		)
{
//	HashRank64 hf;
//	hf.Init( vecFeats );
//
//	hf.StoreFeatures( vecFeats, vecFeats.size()/8 );
//
//	__int64 vecCodeList0[64];
//	__int64 vecCodeList1[64];
//	int vecMatch[64];
//	
//	//vecCodeList0.resize(64);
//	//vecCodeList1.resize(64);
//
//	for( int i = 0; i < vecOrderToFeat.size(); i++ )
//	{
////		FILE *outfile = fopen( "dist_classify.txt", "wt" );
//		int iFeat = vecOrderToFeat[i];
//		MODEL_FEATURE &feat = vecFeats[iFeat];
//
//		//hf.GenerateCodeList( feat.m_pfPC, vecCodeList0 );
//
//		for( int i1 = 0; i1 < vecFeatToIndexInfoCount[iFeat]; i1++ )
//		{
//			IndexInfo &II1 = vecIndexInfo[vecFeatToIndexInfo[iFeat]+i1];
//			int iFeat2 = II1.Index();
//			int iImg2 = vecFeatToImgName[iFeat2];
//
//			float fThres = vecFeatAppearanceThresholds[iFeat];
//
//			if( II1.DistSqr() <= vecFeatAppearanceThresholds[iFeat] )
//			{
//				MODEL_FEATURE &feat2 = vecFeats[iFeat2];
//				hf.GenerateCodeList( feat2.m_pfPC, vecCodeList1 );
//				countMatchingBits( vecCodeList0, vecCodeList1, vecMatch );
//
//				if( iFeat2 > vecFeats.size()/8 )
//				{
//					// Test nearest neighbors
//					
//					// See if nearest neighbors are comparable
//					// Sera
//					int iNearestIndex = -1;
//					float fNearestDist = 999999e20;
//					for( int k = 0; k < vecFeats.size()/8; k++ )
//					{
//						float fDist = feat2.DistSqrPCs( vecFeats[k] );
//						if( fDist < fNearestDist )
//						{
//							iNearestIndex = k;
//							fNearestDist = fDist;
//						}
//					}
//
//					int iNearestIndexHash = iNearestIndex;
//					hf.FindFeatureData( feat2.m_pfPC, iNearestIndexHash );
//
//				}
//			}
//		}
//
//		//fclose( outfile );
//	}
	return 1;
}

static float
peakFunction(
			 LOCATION_VALUE_XYZ &lv,
			 FEATUREIO &fio
			 )
{
	return 1;
}


int
ModelFeature3D::CinchUpFeaturesToAtlas(
		FEATUREIO	&fioAtlas,
		int iSearchRadius
	)
{
	FEATUREIO fioImg;
	FEATUREIO fioLike1;

	fioLike1 = fioAtlas;
	fioAllocate( fioLike1 );
	for( int i = 0; i < vecOrderToFeat.size(); i++ )
	{
		int iFeat = vecOrderToFeat[i];
		int iImg = vecFeatToImgName[iFeat];
		if( fioRead( fioImg, vecImgName[iImg] ) <= 0 )
		{
			continue;
		}

		Feature3DInfo &feat = vecFeats[iFeat];

		int piCoord1[3];	
		int piSize1[3];
		int piCoord2[3];
		int piSize2[3];
		fioSet( fioLike1, 0 );

		piCoord1[0]	= feat.x;			piCoord1[1] = feat.y;			piCoord1[2] = feat.z;
		piSize1[0]	= 2*feat.scale;		piSize1[1] = 2*feat.scale;			piSize1[2] = 2*feat.scale;

		piCoord2[0]	= feat.x;			piCoord2[1] = feat.y;			piCoord2[2] = feat.z;
		piSize2[0]	= iSearchRadius;	piSize2[1] = iSearchRadius;		piSize2[2] = iSearchRadius;

		hfGenerateLikelihoodImageCorrelation(
		//hfGenerateProbMILikelihoodImage(
			piCoord1,	// Center of box in fio1, xyzt, col/row/z/time
			piSize1,  // Size of box in fio1
			piCoord2,	// Center of box (search area) in fio2
			piSize2,	// Center of box (search area) in fio2

			fioImg,
			fioAtlas, fioLike1
			);

		// Write out latest peaks
		LOCATION_VALUE_XYZ_ARRAY lvaPeaks;
		memset( &lvaPeaks, 0, sizeof(lvaPeaks) );
		regFindFEATUREIOPeaks( lvaPeaks, fioLike1, peakFunction );
		lvaPeaks.plvz = new LOCATION_VALUE_XYZ[lvaPeaks.iCount];
		regFindFEATUREIOPeaks( lvaPeaks, fioLike1, peakFunction );
		//sprintf( pcFileName, "peaks%3.3d.txt", i );
		lvSortHighLow( lvaPeaks );

		char pcFileName[400];

		sprintf( pcFileName, "feat%3.3d_img", i );
		feat.OutputVisualFeatureInVolume( fioImg, pcFileName, 1 );
		
		sprintf( pcFileName, "feat%3.3d_atlas", i );
		feat.OutputVisualFeatureInVolume( fioAtlas, pcFileName, 1 );

		float fx;
		float fy;
		float fz;

		get_max_3D( lvaPeaks.plvz[0].x, lvaPeaks.plvz[0].y, lvaPeaks.plvz[0].z, fioLike1, fx, fy, fz, 1 );

		feat.x = fx;
		feat.y = fy;
		feat.z = fz;

		sprintf( pcFileName, "feat%3.3d_atlas_reg", i );
		feat.OutputVisualFeatureInVolume( fioAtlas, pcFileName, 1 );
		
		sprintf( pcFileName, "feat%3.3d_atlas_likelihood", i );
		feat.OutputVisualFeatureInVolume( fioLike1, pcFileName, 1 );

		fioDelete( fioImg );
	}

	return 1;
}

