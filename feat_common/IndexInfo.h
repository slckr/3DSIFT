
#ifndef __INDEXINFO_H__
#define __INDEXINFO_H__

#include <assert.h>

class IndexInfo
{
public:
	IndexInfo()
	{
		m_iIndex = 0;
		m_fDistSqr = 0;
		m_iDistSqrLessThanNeg = 0;
	};
	IndexInfo(int iIndex, int iInfo, float fDistSqr)
	{
		SetIndex(iIndex);
		SetInfo(iInfo); // iInfo=1 means a mirror match
		//m_iIndex = iIndex;
		//m_iIndex |= (iInfo<<31);
		m_fDistSqr = fDistSqr;
		m_iDistSqrLessThanNeg = 0;
	};
	~IndexInfo() {};

	int
	ToFile( FILE *outfile )
	{
		int iReturnMe = 
			fprintf(
			outfile,
			"%d\t%f\t%d\t",
			m_iIndex,
			m_fDistSqr,
			m_iDistSqrLessThanNeg
			);
		
		fprintf( outfile, "\n" );
		return iReturnMe;
	}

	int
	FromFile( FILE *infile )
	{
		int iReturnMe = 
			fscanf(
			infile,
			"%d\t%f\t%d\t",
			&m_iIndex,
			&m_fDistSqr,
			&m_iDistSqrLessThanNeg
			);

		//int iIndex, iInfo;
		//int iC1, iC2;
		//int iReturnMe = 
		//	fscanf(
		//	infile, "%d\t%d\t%f\t%d\t%d\t%d\t",
		//	&iIndex, &iInfo,
		//	&m_fDistSqr,
		//	&iC1,
		//	&m_iDistSqrLessThanNeg,
		//	&iC2
		//	);
		//SetIndex( iIndex );
		//SetInfo( iInfo );
		//assert( iReturnMe == 6 );

		if( iReturnMe == EOF )
		{
			return 0;
		}

		assert( iReturnMe == 3 );

		return iReturnMe;
	}

	int ValidIndex()
	{
		return ((m_iIndex & 0x7FFFFFFF) != 0x7FFFFFFF);
	}

	int Index()
	{
		return m_iIndex & 0x7FFFFFFF;
	}
	int Info()
	{
		return (m_iIndex>>31) & 0x1;
	}
	int DistSqrLessThanNeg()
	{
		return m_iDistSqrLessThanNeg;
	}

	float DistSqr() const
	{
		return m_fDistSqr;
	}

	void SetIndex( int iIndex )
	{
		// Keep high order bit as info
		m_iIndex = (iIndex & 0x7FFFFFFF) | (m_iIndex & 0x80000000);
	}

	void SetInfo( int iInfo )
	{
		// Set info into higher order bit: info=1 means a mirror match
		m_iIndex = (m_iIndex & 0x7FFFFFFF) | ((iInfo<<31) & 0x80000000);
	}

	void SetDistSqrLessThanNeg( int iDSLTS )
	{
		m_iDistSqrLessThanNeg = iDSLTS;
	}

	void SetDistSqr( float fDS )
	{
		m_fDistSqr = fDS;
	}

private:
	int m_iIndex;	// Feature index in m_vecFeatures
	float m_fDistSqr;	// Squared distance of this feature with match
	int	m_iDistSqrLessThanNeg; // Number of negative examples with NCC greater than this
};


class IndexInfoOriginalBig
{
public:
	IndexInfoOriginalBig() {};
	IndexInfoOriginalBig(int iIndex, int iInfo, float fDistSqr)
	{
		m_iIndex = iIndex; m_iInfo = iInfo;
		m_fDistSqr = fDistSqr;
		m_iDistSqrLessThanPos = 0;
		m_iDistSqrLessThanNeg = 0;
		m_iDistSqrLessThanTotal = 0;
	};
	~IndexInfoOriginalBig() {};

	int
	ToFile( FILE *outfile )
	{
		int iReturnMe = 
			fprintf(
			outfile, "%d\t%d\t%f\t%d\t%d\t%d\t",
			m_iIndex, m_iInfo,
			m_fDistSqr,
			m_iDistSqrLessThanPos,
			m_iDistSqrLessThanNeg,
			m_iDistSqrLessThanTotal
			);
		
		fprintf( outfile, "\n" );
		return iReturnMe;
	}

	int
	FromFile( FILE *infile )
	{
		int iReturnMe = 
			fscanf(
			infile, "%d\t%d\t%f\t%d\t%d\t%d\t",
			&m_iIndex, &m_iInfo,
			&m_fDistSqr,
			&m_iDistSqrLessThanPos,
			&m_iDistSqrLessThanNeg,
			&m_iDistSqrLessThanTotal
			);

		if( iReturnMe == EOF )
		{
			return 0;
		}

		assert( iReturnMe == 6 );

		return iReturnMe;
	}

	int m_iIndex;	// Feature index in m_vecFeatures
	int m_iInfo;	// Match info relating to feature

	float m_fDistSqr;	// Squared distance of this feature with match

	int	m_iDistSqrLessThanPos; // Number of positive examples with NCC greater than this
	int	m_iDistSqrLessThanNeg; // Number of negative examples with NCC greater than this
	int	m_iDistSqrLessThanTotal; // Number of neg & pos examples with NCC greater than this
};

#endif
