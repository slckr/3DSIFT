
#ifndef __CLASSIFYGEOMETRYSXYZ_H__
#define __CLASSIFYGEOMETRYSXYZ_H__

#include <vector>

using namespace std;

class ClassifyGeometrySXYZ
{
public:

	ClassifyGeometrySXYZ();

	~ClassifyGeometrySXYZ();

	int
	Clear();

	int
	AddSample(
		int label, // 0 or 1
		float scale,
		float x,
		float y,
		float z
		);

	//
	// ComputeOptimalClassificationThreshold()
	//
	// Returns the optimal classification threshold of feature scale
	// based on added samples.
	//
	float
	ComputeOptimalClassificationThreshold(
		int iDirichletPrior = 1
	);

	//
	// ClassifySample()
	//
	// Return log probabilities of sample being 0 or 1
	//
	int
	ClassifySample(
		float fscale,
		float x,
		float y,
		float z,
		float &fLogPLabel0,
		float &fLogPLabel1
		);

	int
	WriteToFile(
		char *pcFileName
	);

	int
	ReadFromFile(
		char *pcFileName
	);


	class SXYZ
	{
	public:

		SXYZ()
		{
		};

		SXYZ( int iL, float fS, float fX, float fY, float fZ )
		{
			label=iL;
			scale=fS;
			x=fX;
			y=fY;
			z=fZ;
		}

		int   label;
		float scale;
		float x;
		float y;
		float z;
	};

private:

	SXYZ m_sxyzThres; // Thresholds for classification

	float m_pfProbs[2][2]; // Prob[0,1] before threshold, Prob [0,1] after threshold.

	vector< SXYZ > m_vecSXYZ;

};

#endif

