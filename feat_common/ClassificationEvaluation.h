
#ifndef __CLASSIFICATIONEVALUATION_H__
#define __CLASSIFICATIONEVALUATION_H__

#include <stdio.h>

#pragma warning(disable:4786)

#include <vector>
#include <map>

using namespace std;

class ClassificationEvaluation
{
public:
	class LabelScore
	{
	public:
		LabelScore() {};
		LabelScore( int iLabel, float fScore, int iIndex ) {
			m_iLabel = iLabel;
			m_fScore = fScore;
			m_iIndex = iIndex;
		};
		~LabelScore() {};
		int m_iIndex; // Data index (image, etc)
		int m_iLabel;
		float m_fScore;
	};

	ClassificationEvaluation() {};

	~ClassificationEvaluation() {};

	int
	Clear();

	int
	AddLabelScore(
		int iLabel,
		float fScore,
		int iIndex = -1
		);

	//
	// CalculateEER()
	//
	// Calculate the equal error classification rate point for currently stored set of LabelScore pairs.
	//
	float
	CalculateEER(
		FILE *outfile = 0,
		float *fThreshold = 0, 
		vector<int> *pvecIndexCorrect = 0,
		vector<int> *pvecIndexIncorrect = 0
		);

	int
	WriteToFile(
		FILE *outfile
	);
	int
	ReadFromFile(
		FILE *infile
	);

	//
	// OptimalEERClassification()
	//
	// Returns a map of (image index, class 1/0) based on the EER threshold.
	//
	int
	OptimalEERClassification(
		map<int,int> &mapClass
	);

private:

	vector< LabelScore > m_vecLS;
};


#endif
