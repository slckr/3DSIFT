#ifndef _LIBSVM_H
#define _LIBSVM_H

#ifdef __cplusplus
extern "C" {
#endif

struct svm_node
{
	int index;
	double value;
};

struct svm_problem
{
	int l;
	double *y;
	struct svm_node **x;
};
#if 0
enum { C_SVC, NU_SVC, ONE_CLASS, EPSILON_SVR, NU_SVR };	/* svm_type */
#endif
enum { C_SVC, C_SVCq, R2, R2q, NU_SVR };	/* svm_type */
enum { LINEAR, POLY, RBF, SIGMOID };	/* kernel_type */

struct svm_parameter
{
	int svm_type;
	int kernel_type;
	double degree;	// for poly
	double gamma;	// for poly/rbf/sigmoid
	double coef0;	// for poly/sigmoid

	// these are for training only
	double cache_size; // in MB
	double eps;	// stopping criteria
	double C;	// for C_SVC, EPSILON_SVR and NU_SVR
	int nr_weight;		// for C_SVC
	int *weight_label;	// for C_SVC
	double* weight;		// for C_SVC
	double nu;	// for NU_SVC, ONE_CLASS, and NU_SVR
	double p;	// for EPSILON_SVR
	int shrinking;	// use the shrinking heuristics
	int cal_partial;	// calculate the partial derivatives
};

//
// svm_model
//
struct svm_model
{
	struct svm_parameter param;	// parameter
	int nr_class;		// number of classes, = 2 in regression/one class svm
	int l;			// total #SV
	struct svm_node **SV;		// SVs (SV[l])
	double **sv_coef;	// coefficients for SVs in decision functions (sv_coef[n-1][l])
	double *rho;		// constants in decision functions (rho[n*(n-1)/2])
	double obj, wsq, cxi, rsq;	// should be arrays for multiclass, but just values for now

	// for classification only

	int *label;		// label of each class (label[n])
	int *nSV;		// number of SVs for each class (nSV[n])
				// nSV[0] + nSV[1] + ... + nSV[n-1] = l
	int *nBSV;
	// XXX
	int free_sv;		// 1 if svm_model is created by svm_load_model
				// 0 if svm_model is created by svm_train
//	double partial;		// 1/2 \partial\|w\|^2 / \partial\sigma^2 or \partial R^2 / \partial C
};

struct svm_model *svm_train(const struct svm_problem *prob,
			    const struct svm_parameter *param);

int svm_save_model(const char *model_file_name, const struct svm_model *model);

struct svm_model *svm_load_model(const char *model_file_name);

double svm_predict(const svm_model *model, const svm_node *x, double *pdsum = 0 );

double svm_calculate_partial(const struct svm_model *model);

void svm_destroy_model(struct svm_model *model);

#ifdef __cplusplus
}
#endif

#endif /* _LIBSVM_H */
