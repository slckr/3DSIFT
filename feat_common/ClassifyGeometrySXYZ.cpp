
#include "ClassifyGeometrySXYZ.h"
#include <algorithm>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

ClassifyGeometrySXYZ::ClassifyGeometrySXYZ()
{
}

ClassifyGeometrySXYZ::~ClassifyGeometrySXYZ()
{
}

int
ClassifyGeometrySXYZ::Clear(
							)
{
	m_vecSXYZ.clear();
	return 0;
}

int
ClassifyGeometrySXYZ::AddSample(
		int label, // 0 or 1
		float scale,
		float x,
		float y,
		float z
		)
{
	m_vecSXYZ.push_back(
		SXYZ( label, scale, x, y, z )		
		);

	return 0;
};

bool pairSXYZLeastToGreatestScale( const ClassifyGeometrySXYZ::SXYZ &ii1, const ClassifyGeometrySXYZ::SXYZ &ii2 )
{
	return ii1.scale < ii2.scale;
}

float
ClassifyGeometrySXYZ::ComputeOptimalClassificationThreshold(
	int iDirichletPrior
	)
{
	sort( m_vecSXYZ.begin(), m_vecSXYZ.end(), pairSXYZLeastToGreatestScale );

	int i;
	int piCounts[2] = {0,0};
	for( i = 0; i < m_vecSXYZ.size(); i++ )
	{
		piCounts[ m_vecSXYZ[i].label ]++;
	}

	int iTotalCount = piCounts[0]+piCounts[1]+4*iDirichletPrior;

	// Init contingency table of counts
	int piCTable[2][2] = { {0,0},{0,0} };

	// All counts are initially classified as greater than threshold
	piCTable[1][0] = piCounts[0];
	piCTable[1][1] = piCounts[1];

	// Find the threshold with the lowest total classification error

	// Set nominal error rate (for guessing the most frequent label)
	float fLowestErr;
	if( piCTable[1][0] < piCTable[1][1] )
	{
		fLowestErr = (piCTable[1][0]+iDirichletPrior)/(float)iTotalCount;
	}
	else
	{
		fLowestErr = (piCTable[1][1]+iDirichletPrior)/(float)iTotalCount;
	}
	float fOriginalErr = fLowestErr;
	float fLowestErrThreshold = m_vecSXYZ[0].scale - 1.0f;
	// p(Class|below threshold)  (normalize later)
	m_pfProbs[0][0] = piCTable[0][0];
	m_pfProbs[0][1] = piCTable[0][1];
	// p(Class|above threshold)  (normalize later)
	m_pfProbs[1][0] = piCTable[1][0];
	m_pfProbs[1][1] = piCTable[1][1];

	i = 0;

static int iIter__ = 0;
	//char pcFileName[200];
	//sprintf( pcFileName, "out\\sort%3.3d_zclass.txt", iIter__ );
	//FILE *outfile = fopen( pcFileName, "wt" );
	//iIter__++;
	//fprintf( outfile, "%f\t%f\n", fLowestErr, 0 );

	while( i < m_vecSXYZ.size() )
	{
		SXYZ &sxyz = m_vecSXYZ[i];
		// Scroll through all samples whose scale is equal to (or less than) the current threshold
		float fThreshold = m_vecSXYZ[i].scale;
		while(i < m_vecSXYZ.size() && m_vecSXYZ[i].scale <= fThreshold )
		{
			sxyz = m_vecSXYZ[i];
			// Remove a label from greater-than bin
			piCTable[1][m_vecSXYZ[i].label]--;
			// Place in less-than bin
			piCTable[0][m_vecSXYZ[i].label]++;
			assert( m_vecSXYZ[i].label == 1 || m_vecSXYZ[i].label == 0 );
			i++;
		}

		// Compute absolute classification error
		float fErr0 = (piCTable[0][1]+piCTable[1][0]+2*iDirichletPrior)/(float)iTotalCount;
		float fErr1 = (piCTable[0][0]+piCTable[1][1]+2*iDirichletPrior)/(float)iTotalCount;
		float fErrMin = ( fErr0 < fErr1 ? fErr0 : fErr1 );

		//fprintf( outfile, "%f\t%f\n", fErrMin, fThreshold );

		// If lower than nominal error, save threshold and contingency table counts (normalize later)
		if( fErrMin < fLowestErr )
		{
			fLowestErr = fErrMin;
			fLowestErrThreshold = fThreshold;

			// Classification rates below threshold
			m_pfProbs[0][0] = piCTable[0][0];
			m_pfProbs[0][1] = piCTable[0][1];

			// Classification rates above threshold
			m_pfProbs[1][0] = piCTable[1][0];
			m_pfProbs[1][1] = piCTable[1][1];
		}
	}

	//fclose( outfile );

	//if( outfile )
	//{
	//	for( int i = 0; i < m_vecSXYZ.size(); i++ )
	//	{
	//		fprintf( outfile, "%d\t%d\t%f\t%f\t%f\n", m_vecSXYZ[i].m_iIndex, m_vecSXYZ[i].label, m_vecSXYZ[i].m_fScore, viAccum[0][i+1], viAccum[1][i+1] );
	//	}
	//}

	// Save scale threshold
	m_sxyzThres.scale = fLowestErrThreshold;

	return fLowestErrThreshold;

	float fDirichletPrior = 1.0;
	// Save probabilities (with prior estimate)
	m_pfProbs[0][0] += fDirichletPrior;
	m_pfProbs[0][1] += fDirichletPrior;
	m_pfProbs[1][0] += fDirichletPrior;
	m_pfProbs[1][1] += fDirichletPrior;

	float fProbSum0 = m_pfProbs[0][0] + m_pfProbs[0][1];
	assert( fProbSum0 > 0 );

	float fProbSum1 = m_pfProbs[1][0] + m_pfProbs[1][1];
	assert( fProbSum1 > 0 );

	m_pfProbs[0][0] /= fProbSum0;
	m_pfProbs[0][1] /= fProbSum0;
	m_pfProbs[1][0] /= fProbSum1;
	m_pfProbs[1][1] /= fProbSum1;
	
	//m_pfProbs[0][0] = log( m_pfProbs[0][0] );
	//m_pfProbs[0][1] = log( m_pfProbs[0][1] );
	//m_pfProbs[1][0] = log( m_pfProbs[1][0] );
	//m_pfProbs[1][1] = log( m_pfProbs[1][1] );

	//if( j == 0 || j == m_vecSXYZ.size() )
	//{
	//	// Nothing is classified
	//	fEER = 0.0f;
	//	fThreshold = -10000;
	//}
	//else
	//{
	//	float p00 = viAccum[0][j-1];
	//	float p10 = viAccum[1][j-1];

	//	float p01 = viAccum[0][j];
	//	float p11 = viAccum[1][j];

	//	// Interpolate point
	//	float m0 = p01-p00;
	//	float m1 = p11-p10;

	//	assert( m1 != m0 );

	//	float pMid = (p00-p10)/(m1 - m0);
	//	fEER = pMid*m1 + p10;
	//	fThreshold = pMid*(m_vecSXYZ[j-1].scale-m_vecSXYZ[j-2].scale) + m_vecSXYZ[j-2].scale;
	//}

	return -1;
}

int
ClassifyGeometrySXYZ::ClassifySample(
		float fscale,
		float x,
		float y,
		float z,
		float &fLogPLabel0,
		float &fLogPLabel1
		)
{
	if( fscale < m_sxyzThres.scale )
	{
		fLogPLabel0 = m_pfProbs[0][0];
		fLogPLabel1 = m_pfProbs[0][1];
	}
	else
	{
		fLogPLabel0 = m_pfProbs[1][0];
		fLogPLabel1 = m_pfProbs[1][1];
	}
	return 0;
}

int
ClassifyGeometrySXYZ::WriteToFile(
		char *pcFileName
	)
{
	FILE *outfile = fopen( pcFileName, "wt" );
	if( !outfile )
	{
		return -1;
	}

	int iResult;

	SXYZ &sxyz = m_sxyzThres;
	iResult = fprintf( outfile, "%d\t%f\t%f\t%f\t%f\n", sxyz.label, sxyz.scale, sxyz.x, sxyz.y, sxyz.z ); 
	//if( iResult != 5 )
	//{
	//	assert( 0 );
	//	fclose( outfile );
	//	return -1;
	//}

	iResult = fprintf( outfile, "%f\t%f\t%f\t%f\n",
		m_pfProbs[0][0],
		m_pfProbs[0][1],
		m_pfProbs[1][0],
		m_pfProbs[1][1]
		);
	//if( iResult != 4 )
	//{
	//	assert( 0 );
	//	fclose( outfile );
	//	return -1;
	//}

	for( int i = 0; i < m_vecSXYZ.size(); i++ )
	{
		sxyz = m_vecSXYZ[i];
		iResult = fprintf( outfile, "%d\t%f\t%f\t%f\t%f\n", sxyz.label, sxyz.scale, sxyz.x, sxyz.y, sxyz.z ); 
		//if( iResult != 5 )
		//{
		//	assert( 0 );
		//	fclose( outfile );
		//	return -1;
		//}
	}

	fclose( outfile );

	return 0;
}


int
ClassifyGeometrySXYZ::ReadFromFile(
		char *pcFileName
	)
{
	FILE *outfile = fopen( pcFileName, "rt" );
	if( !outfile )
	{
		return -1;
	}

	int iResult;

	SXYZ &sxyz = m_sxyzThres;
	iResult = fscanf( outfile, "%d\t%f\t%f\t%f\t%f\n", &sxyz.label, &sxyz.scale, &sxyz.x, &sxyz.y, &sxyz.z ); 
	if( iResult != 5 )
	{
		assert( 0 );
		fclose( outfile );
		return -1;
	}

	iResult = fscanf( outfile, "%f\t%f\t%f\t%f\n",
		&m_pfProbs[0][0],
		&m_pfProbs[0][1],
		&m_pfProbs[1][0],
		&m_pfProbs[1][1]
		);
	if( iResult != 4 )
	{
		assert( 0 );
		fclose( outfile );
		return -1;
	}

	while( fscanf( outfile, "%d\t%f\t%f\t%f\t%f\n", &sxyz.label, &sxyz.scale, &sxyz.x, &sxyz.y, &sxyz.z ) == 5 )
	{
		m_vecSXYZ.push_back( sxyz );
	}

	fclose( outfile );

	return 0;
}



