
#ifndef __BAYESCLASSIFIER_H__
#define __BAYESCLASSIFIER_H__

#include "svm.h"

typedef float BAYESFEATUREDIST[4];

class BayesClassifier
{
public:

	BayesClassifier();

	~BayesClassifier();

	//
	// bayes_learn_classifier()
	//
	// Learn a Bayesian classifier of the form p(f|label=1)/p(f|label=-1), for
	// each feature/element in the svm_problem.
	//
	// Memory is allocated here for bcClassifier.pbfdDistributions:
	//   new BAYESFEATUREDIST[prob.l] 
	//
	void
	LearnClassifier(
		svm_problem &prob,
		int iDirichletPrior = 1
		);

	//
	// Classify()
	//
	// Classify and return the label value for svm_node x based
	// on the Bayesian classifier.
	//
	double
	Classify(
		const struct svm_node *x,
		double &dSum,
		float &fLogProbZero,
		float &fLogProbOne
		);

	//
	// ClassifyBoosting
	//
	// A classify according the boosting method.
	//
	double
	ClassifyBoosting(
		const struct svm_node *x,
		double *pdCoefficients,
		double &dSum,
		float &fLogProbZero,
		float &fLogProbOne
		);

	//
	// ClassifyWeights()
	//
	// Same as above, except a weight 0 <= w <= 1 is applied
	// For w = 1, ClassifyWeights() = Classify()
	// For w = 0, ClassifyWeights() = 1
	// 
	//
	double
	ClassifyWeights(
			  const struct svm_node *x,
			  const struct svm_node *w,
			  double &dSum,
			  float &fLogProbZero,
			  float &fLogProbOne
			  );

	//
	// ClassifyRawSamples()
	//
	// Simply return the number of raw samples.
	//
	double
	ClassifyRawSamples(
		const struct svm_node *x,
		double &dSum,
		float &fLogProbZero,
		float &fLogProbOne
		);

	int
	OutputCounts(
		char *pcFileName
	);

public:
	int m_iOnes;
	int m_iZeros;
	int m_iFeatures;
	BAYESFEATUREDIST *m_pbfdDistributions;
	int					*m_piCounts[2];      // raw counts for positive feature occurrences
	float m_fBiasTermOnes;
	float m_fBiasTermZeros;
	int m_iDirichletPrior;

};



#endif
