
#ifndef __MODELFEATURE3D_H__
#define __MODELFEATURE3D_H__

#include "src_common.h"
#include "TextFile.h"
#include "IndexInfo.h"
#include "BayesClassifier.h"

//#define MODEL_FEATURE Feature3DChar
//#define MODEL_FEATURE Feature3DShort
//#define MODEL_FEATURE Feature3D
#define MODEL_FEATURE_INFO 1
#define MODEL_FEATURE Feature3DInfo
#define MODEL_FEATURE_DIST DistSqrPCs

class ScaleSpaceXYZHash;

class ModelFeature3D
{
public:

	typedef enum
	{
		ctBayesian=1,
		ctSVM_Linear=2,
		ctSVM_RBF=3,
		ctBayesianDist=4,
		ctNearestNeighbor=5,
		ctBayesianOverlap=6,
		ctBayesianBoosting=7
	} ClassifierType;

	ModelFeature3D()
	{
		m_prob.y = 0;
		m_prob.x = 0;

		m_param.svm_type = C_SVC;
		m_param.kernel_type = RBF;
		m_param.degree = 3;
		m_param.gamma = 0;	// 1/k
		m_param.coef0 = 0;
		m_param.nu = 0.5;
		m_param.cache_size = 40;
		m_param.C = 1;
		m_param.eps = 1e-3;
		m_param.p = 0.1;
		m_param.shrinking = 1;
		m_param.cal_partial = 0;
		m_param.nr_weight = 0;
		m_param.weight_label = NULL;
		m_param.weight = NULL;

		m_pmodel = 0;
		m_prob_test =0;
		m_prob_test_weights=0;

		m_fioOnes.x = m_fioOnes.y = m_fioOnes.z = 0; m_fioOnes.pfVectors = 0;
		m_fioZeros.x = m_fioZeros.y = m_fioZeros.z = 0; m_fioZeros.pfVectors = 0;
	};

	~ModelFeature3D()
	{
		if( m_prob.y )
		{
			delete [] m_prob.y;
			m_prob.y = 0;
		}
		if( m_prob.x )
		{
			if( m_prob.x[0] )
			{
				delete [] m_prob.x[0];
			}
			delete [] m_prob.x;
			m_prob.x = 0;
		}
		if( m_pmodel )
		{
			svm_destroy_model( m_pmodel );
		}
		if( m_prob_test)
		{
			delete [] m_prob_test;
			m_prob_test =0;
		}
		if(  m_prob_test_weights )
		{
			delete [] m_prob_test_weights;
			m_prob_test_weights=0;
		}
			
		if( m_fioOnes.pfVectors )
		{
			fioDelete( m_fioOnes );
		}
		if( !m_fioZeros.pfVectors )
		{
			fioDelete( m_fioZeros );
		}
	};

	//
	// ReplaceFeatures()
	//
	// This function reads in another model trained on precisely the same features,
	// and replaces the current features (particularly their appearances) with those
	// of the other model.
	//
	int
	ReplaceFeatures(
		char *pcFileName
	);

	int
	Read(
		char *pcFileName
	);

	int
	Write(
		char *pcFileName
	);

	int
	WriteFeatureHistogram(
		char *pcFileName
	);

	//
	// ReadData(Integer/Float)()
	//
	// Reads data from a text file, each line of text file pcFileName
	// contains an integer/float, corresponding to the label for the image file
	// at the same index. Used to read in labels/age/sex.
	//
	int
	ReadDataInteger(
		char *pcFileName,
		TextFile &tfText,
		vector<int> &vecInt
	);
	int
	ReadDataFloat(
		char *pcFileName,
		TextFile &tfText,
		vector<float> &vecFloat
	);

	//
	// BuildClassifier()
	//
	// Build classifier based on learned model and labels
	// Sev
	//
	int
	BuildClassifier(
		int iClassifierType = 1, // 1: Bayesian, 2: Linear SVM, 3: RBF SVM
		float fParam1 = 1, // Bayesian: Dirichlet prior, SVM: C
		float fParam2 = 1 // Bayesian: NA, SVM: gamma
	);
	
	//
	// BuildClassifierDist()
	//
	// Here we build a classifier, where the Bayes ratio is dependent on the distance
	// of the image feature to the model feature. For each model feature, we generate Bayes
	// ratio as a function of distance.
	//
	int
	ClassifierDistBuild(
		int iDirichletPrior
	);
	int
	ClassifierDistClassify(
									   		vector<MODEL_FEATURE> &vecImgFeats,
		vector<int>       &vecModelMatches,
		double			&dScore
	);

	//
	// ClassifyBayesianNearness()
	//
	// Nearest neighbour classification - classify using k-nearest neighbours, where a neighbour proximity is
	// defined by the the training image that shares the most features with this image.
	//
	int
	BayesianNearnessBuild(
	);
	int
	BayesianNearnessClassify(
		vector<MODEL_FEATURE> &vecImgFeats,
		vector<int>       &vecModelMatches,
		double			&dScore
	);
	
	//
	// BayesianOverlap classifier
	//
	// Consider spatial overlap like statistical dependence. 
	//
	int
	BayesianOverlapBuild(
		int iDirichletPrior
	);
	int
	BayesianOverlapClassify(
		vector<MODEL_FEATURE> &vecImgFeats,
		vector<int>       &vecModelMatches,
		double			&dScore
	);

	//
	// InspectModelFeatureOverlap()
	//
	// This function is to inspect model feature overlap...is this a problem?
	//
	int
	InspectModelFeatureOverlap(
	);

	//
	// Classify()
	//
	// Fit a vector of image features to model features,
	// output classification.
	//
	//    return value: the hard binary classification  (0 or 1)
	//
	//    vecModelMatches: maps one-to-one with vecImgFeats, 
	// and is set to the index (ordered index) of
	// the model feature that matched to the corresponding
	// image feature.
	//
	//    fScore: the score of the classifier
	//
	int
	Classify(
		int iClassifierType, // 1: Bayesian, 2: Linear SVM, 3: RBF SVM
		vector<MODEL_FEATURE> &vecImgFeats,
		vector<int>       &vecModelMatches,
		float			   &fScore,
		int					&iFeatsUsed,
		int					&iSamplesUsed,
		int					bReverse = 0 // For debugging....
	);

	//
	// SetValidFlag()
	//
	// Set the validate flag for all features
	//
	int
	SetValidFlag(
			int iFlag // flag
		);

	//
	// SetValidFlagImage()
	//
	// Set the validate flag for all features from image iImg.
	int
	SetValidFlagImage( 
		int iImg,
		int iFlag
		);

	//
	// GetFeatsImage()
	//
	// Get a vector of all features associated with image iImg.
	//
	int
	GetFeatsImage( 
		int iImg,
		vector<MODEL_FEATURE> &vecImgFeats
		);

	//
	// AlignImages()
	//
	// Align all internal images.
	// Each image is thus going to have a transform from origin to reference frame.
	//
	int
	AlignImages(
		int iIterations
		);

	//
	// Returns an array of scrambled labels.
	//
	int
	ScrambledLabels(
		vector<int> &vecImgLabelsScrambled
	);

	//
	// SelectTrainingSetAgeRange()
	//
	// Selects a set of training subjects equally distributed in age about central subject iSubject.
	//
	int
	SelectTrainingSetAgeRange(
		int iSubject,
		int iTrainingSubjects
	);

	//
	// InvalidateTestSet()
	//
	// Invalidate test set, for NeuroImage AD/NC classification trials.
	//
	// piLabelCounts: an array of the number of images of each label to invalidate (small)
	// iLabels: the number of labels/elements in piLabelCounts.
	// piPreviousLabels: array of previously invalidated labels. If piPreviousLabels[iImg] != vecImgLabels[iImg], then
	//   do not consider invalidating this image.
	//  Typically, piPreviousLabels[iImg] = -1 means iImg is in a test set
	//			   piPreviousLabels[iImg] = -2 means iImg is in a validation set
	//
	int
	InvalidateRandomImageTestSet(
		int *piLabelCounts,
		int iLabels,
		int *piPreviousLabels = 0// Optional list of previous labels to avoid (for k-fold testing)
	);

	//
	// OutputClassifierInfo()
	//
	// Output information related to classifier.
	//
	int
	OutputClassifierInfo(
		char *pcFileName
	);

	//
	// OutputFeatureStatsVsDist()
	//
	// Output classification ratio vs. appearance distance.
	//
	int
	OutputFeatureStatsVsDist(
		int iFeat,
		char *pcFileName,
		int iDirichletPrior = 1
	);

	//
	// OutputFeatureInVolume()
	//
	// Output the feature in volume.
	//
	int
	OutputFeatureInVolume(
		int iFeat,
		char *pcFileName
	);

	//
	// BoostFeatures()
	//
	// To be called after the model and Bayesian classifier have been built.
	// Reduces the set of model features to a set of boosted features.
	//
	int
	BoostFeatures(
		int iBoostedFeatureCount,
		vector< int > *pvecOrderFeatsDisregard = 0 // A vector of features to disregard from boosting (if 1)
		);

	//
	// BoostFeaturesRegistration()
	//
	// Boosts features for registration. In terms of the classification function, a feature correctly
	// identified/registered/present in an image is considered 1, unpresent is -1.
	//
	int
	BoostFeaturesRegistration(
		int iBoostedFeatureCount,
		vector< int > *pvecOrderFeatsDisregard = 0 // A vector of features to disregard from boosting (if 1)
		);
	
	//
	// FitToImageSearchRotationsPCTable()
	//
	// Fit model to features in a new image, try all 24 axis rotations in 3D
	// in order to capture global rotation.
	//
	int
	FitToImageSearchRotationsPCTable(
		vector<Feature3D> &vecImgFeats,
		vector<int>       &vecModelMatches,
		int				  iModelFeatsToConsider=-1, // Model features consider, if -1, use all (slow)
		char *pcFeatFile = 0, // Output file name
		int					iPCs=0, // number of principal components to use
		vector<Feature3D> *pvecPCs = 0 // principal components
		);


	//
	// FitToImageSearchRotations()
	//
	// Fit model to features in a new image, try all 24 axis rotations in 3D
	// in order to capture global rotation.
	//
	int
	FitToImageSearchRotations(
		vector<Feature3D> &vecImgFeats,
		vector<int>       &vecModelMatches,
		int				  iModelFeatsToConsider=-1, // Model features consider, if -1, use all (slow)
		char *pcFeatFile = 0, // Output file name
		int					iPCs=0, // number of principal components to use
		vector<Feature3D> *pvecPCs = 0 // principal components
		);

	int
	FitToImageSearchRotationsRotateModel(
		vector<Feature3D> &vecImgFeats,
		vector<int>       &vecModelMatches,
		int				  iModelFeatsToConsider=-1, // Model features consider, if -1, use all (slow)
		char *pcFeatFile = 0, // Output file name
		int					iPCs=0 // principal components
		);

	//
	// FitToImageRotationInvariant()
	//
	// Fit model to features in a new image, model & image features must be rotation
	// invariant.
	//
	int
	FitToImageRotationInvariant(
		vector<Feature3D> &vecImgFeats,
		vector<int>       &vecModelMatches,
		int				  iModelFeatsToConsider=-1, // Model features consider, if -1, use all (slow)
		char *pcFeatFile = 0 // Output file name
		);

	//
	// FitToImage()
	//
	// Fit model to features in a new image.
	//
	int
	FitToImage(
		vector<Feature3D> &vecImgFeats,
		vector<int>       &vecModelMatches,
		int				  iModelFeatsToConsider=-1, // Model features consider, if -1, use all (slow)
		char *pcFeatFile = 0, // Output file name
		int iPCs = 0 // principal components to use
		);

	//
	// FitToImageNN(), considers only nearest neighbors.
	//
	int
	FitToImageNN(
		vector<Feature3D> &vecImgFeats,
		vector<int>       &vecModelMatches,
		int				  iModelFeatsToConsider=-1, // Model features consider, if -1, use all (slow)
		char *pcFeatFile = 0, // Output file name
		int iPCs = 0 // principal components to use
		);

	//
	// FitToImageBlackAndWhite()
	//
	// Try a search over all orientations/scales/translations
	// based on most common model features and simple DoG peak/valley similarity (rotation invariant)
	//
	int
	FitToImageBlackAndWhite(
		vector<Feature3D> &vecImgFeats,
		vector<int>       &vecModelMatches,
		int				  iModelFeatsToConsider,
		char *pcFeatFile = 0 // Output file name
		);

	//
	// DetermineOrientation()
	//
	// Determine orientation of features.
	//
	int
	DetermineOrientation(
	);

public:

	int
	VisualizeVarriance(
						int iFeat
	);

	//
	// IdentifyFeature1ContainsFeature2()
	//
	// If iFeat1 contains iFeat2, return 1, else return 0
	//
	int 
	IdentifyFeature1ContainsFeature2(
		int iFeat1,
		int iFeat2
	);
	//
	int 
	IdentifyFeature1ContainsFeature2(
	);

	// Identifies geometricall consistent, and thus possibly statistically dependent, features.
	int
	IdentifyGeometricallyConsistentFeatures(
		);

	//
	// IdentifyOverlappingFeatures()
	//
	// Return the number of overlapping feature samples, i.e. feature samples shared by model features iFeat1 and iFeat2.
	// Optionally return a list of overlapping feature sample indices shared by model features Feat1 and Feat2.
	//
	int
	IdentifyOverlappingFeatures(
		int iFeat1,
		int iFeat2,
		vector<int> *pvecOverlapFeatIndices = 0 
	);
	// IdentifyOverlappingFeatures()
	//
	// Identify all model features which overlap/share samples.
	// Then figure out what to do with them - merge?
	int
	IdentifyOverlappingFeatures(
	);

	//
	// SortOrderSignificance()
	//
	// Sort according to order of statistical significance.
	// This can only be called after the classifier has been built.
	//
	int
	SortOrderSignificance(
									  );

	//
	// CommonFeatureSample()
	//
	// Returns the index of the first feature sampled shared between two model features, -1 otherwise.
	//
	int
	CommonFeatureSample(
		int iFeat1,
		int iFeat2
	);

	//
	// ComputeMeanVarr()
	//
	// Computes the mean and variance for all model features, i.e. features in vecOrderToFeat.
	// Used in Mahalanobis distance.
	//
	int
	MeanVarrInit(
	);
	int
	MeanVarrCompute(
	);
	int
	RelearnFeatsMahalanobisDist(
	);

	//
	// Remove2ndImageMatches()
	//
	// Treat 2nd nearest neighbour matches for each feature in another image as false matches.
	// Under the hypothesis that features are unique.
	// This will deal with the phenomenon of double feature matches.
	//
	int
	Remove2ndImageMatches(
	);

	//
	// AddRandomNegatives()
	//
	// This function adds iNegativeCount random negative appearance samples all valid model features.
	// Negative appearance samples are generated using the rand() function.
	//
	int
	AddRandomNegatives(
		int iNegativeCount
	);

	//
	// OutputNearestNeighbors()
	//
	// Output the iNNCount nearest neighbors of iFeat.
	//
	//
	int
	OutputNearestNeighbors(
		int iFeat,
		char *pcFileNameBase,
		int iNNCount = 5,
		int bMirror = 0
	);

	//
	// CinchUpFeaturesToAtlas()
	//
	// This function operates after learning has generated a set of model features,
	// and an average atlas or reference image is available.
	// Correlation of image intensities between the feature in the original image of extraction
	// and the reference image is used to cast all model features into a single, reference image.
	//
	int
	CinchUpFeaturesToAtlas(
		FEATUREIO	&fioAtlas,
				int iSearchRadius = 11

	);

private:

	// 
	// ClassifyGenerateTrainingData()
	//
	// Helper function for classification, prepares data in SVMLIB formate
	//
	int
	ClassifyGenerateTrainingData(
		);

	// 
	// ClassifyGenerateTestingData()
	//
	// Helper function for classification, prepares data in SVMLIB formate
	//
	int
	ClassifyGenerateTestingData(
			vector<MODEL_FEATURE> &vecImgFeats,
		vector<int>       &vecModelMatches,
		float			   &fScore,
		int					&iFeatsUsed,
		int					&iSamplesUsed,
		int					bReverse
		);

public:

	int
	FeaturesWithinThreshold(
						int iFeat,
						int *piImages = 0
	);
	int
	ValidFeaturesWithinThreshold(
						int iFeat,
						int *piImages = 0
	);

	//
	// LoopThroughFeaturesForFun()
	//
	// Sandbox function to investigate features.
	//
	int
	LoopThroughFeaturesForFun(
	);

public:

	vector<char*>		vecImgName; // Array of image names
	vector<int>			vecImgLabels; // Array of image labels - one per image name, typically the variable of interest, e.g. disease
	vector<float>		vecImgAge; // Array of image age labels, years
	vector<float>		vecImgSize; // Array of image size labels, relative floating poitn values
	vector<int>			vecImgSex; // Array of image sex labels: 0=female, 1=male

	vector<IndexInfo>	vecIndexInfo; // IndexInfo

	vector<MODEL_FEATURE>	vecFeats; // Array of features
	vector<int>			vecFeatToImgName; // Maps each Feat to its image name
	vector<int>			vecFeatToIndexInfo; // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int>			vecFeatToIndexInfoCount; // For each Feat, the number of IndexInfos
	vector<float>		vecFeatAppearanceThresholds; // For each Feat, appearance thresholds
	vector<int>			vecFeatValid;	// For each Feat; is this feature valid?

	vector<int>			vecOrderToFeat;	// Model feature indices in vecFeats, sorted from most to least frequent
	vector<Feature3D>	vecOrderMean; // Model feature mean
	vector<Feature3D>	vecOrderVarr; // Model feature variance

	vector<int>			vecModelFeatsPerImage; // Count of model features for each image

	//vector< vector<int> > vecOrderClassDistCounts;
	vector< vector<float> > vecOrderClassDistRatios;

	TextFile			tfNames;	// Storage for reading in names
	TextFile			tfLabels;	// Storage for reading in labels
	TextFile			tfAge;	// Storage for reading in age
	TextFile			tfSex;	// Storage for reading in sex
	TextFile			tfSize;	// Storage for reading in sex

	// Structures related to classification
	struct svm_problem	m_prob;
	double				m_prob_test_label;
	int					m_prob_test_image; 
	svm_node			*m_prob_test;
	svm_node			*m_prob_test_weights;
	BayesClassifier		m_bayesClass; // Bayesian classifer
	struct svm_parameter m_param; // SVM classifier
	struct svm_model	 *m_pmodel;

	vector< float >		m_vecBoostingCoefficients; // For a boosted Bayesian classifier

	// For handling feature overlap
	FEATUREIO	m_fioOnes;
	FEATUREIO	m_fioZeros;

	// Dimensions of model, from training images.
	int m_piModelImageSizeXYZ[3];
};

//
// FeaturesWithinThreshold()
//
// Return the number of valid features within threshold.
//
int
FeaturesWithinThreshold(
						int iFeat,
						vector<char*> &vecImgName, // Array of image names
						vector<int> &vecFeatToImgName, // Maps each Feat to its image name
						vector<MODEL_FEATURE> &vecFeats, // Array of features
						vector<IndexInfo> &vecIndexInfo, // IndexInfo
						vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
						vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
						vector<float> &vecFeatAppearanceThresholds, // For each Feat, appearance thresholds

						int *piImages = 0
	);

//
// ValidFeaturesWithinThreshold()
//
// Return the number of features within threshold flagged as valid.
//
int
ValidFeaturesWithinThreshold(
						int iFeat,
						vector<char*> &vecImgName, // Array of image names
						vector<int> &vecFeatToImgName, // Maps each Feat to its image name
						vector<MODEL_FEATURE> &vecFeats, // Array of features
						vector<IndexInfo> &vecIndexInfo, // IndexInfo
						vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
						vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
						vector<float> &vecFeatAppearanceThresholds, // For each Feat, appearance thresholds
						vector<int> &vecFeatValid, // For each Feat, valid flags

						int *piImages
	);

//
// GenerateIndexInfo()
//
// Generate IndexInfo array. This makes a call to GenerateIndexInfoSubSection().
//
int
GenerateIndexInfo(
	const vector<char*> &vecImgName, // Array of image names
	const vector<int> &vecFeatToImgName, // Maps each Feat to its image name
	const vector<MODEL_FEATURE> &vecFeats,
	vector<IndexInfo> &vecIndexInfo, // IndexInfo
	vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int> &vecFeatToIndexInfoCount,  // For each Feat, the number of IndexInfos
	int iPCs = 0, // Unused anyway
	int iMaxNegatives = -1 // Maximum negative features to consider. If -1, then consider all features in vecFeats
);

//
// GenerateIndexInfoSubSection()
//
// This is a parallelizable version of GenerateIndexInfo()
// All arrays except for vecIndexInfo() must be allocated, their lengths won't change.
//
// Preconditions:
//   vecFeatToIndexInfo.size() == vecFeats.size()
//   vecFeatToIndexInfoCount.size() == vecFeats.size()
//
// Processing:
//   From vecFeats[iFeatStart] to vecFeats[iFeatEnd]
//
int
GenerateIndexInfoSubSection(
	const vector<char*> &vecImgName, // Array of image names
	const vector<int> &vecFeatToImgName, // Maps each Feat to its image name
	const vector<MODEL_FEATURE> &vecFeats,
	vector<IndexInfo> &vecIndexInfo, // IndexInfo
	vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo, vecFeatToIndexInfo.size() == vecFeats.size()
	vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	ScaleSpaceXYZHash *pssh = 0, // Hash table, internal 
	int iFeatStart = 0,	// Index of first feature in vecFeats to process
	int iFeatEnd = -1, // Index of last feature in vecFeats to process
	int iPCs = 0, // number of principal components to consider, if 0 then use original data
	int iMaxNegatives = -1 // Maximum negative features to consider. If -1, then consider all features in vecFeats
);

int
GenerateIndexInfoParallel(
	const vector<char*> &vecImgName, // Array of image names
	const vector<int> &vecFeatToImgName, // Maps each Feat to its image name
	const vector<MODEL_FEATURE> &vecFeats,
	vector<IndexInfo> &vecIndexInfo, // IndexInfo
	vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int> &vecFeatToIndexInfoCount,  // For each Feat, the number of IndexInfos
	int iPCs = 0, // number of principal components to consider, if 0 then use original data
	int iMaxNegatives = -1 // Maximum negative features to consider. If -1, then consider all features in vecFeats
);


//
// SetAppearanceThresholds_f_given_s()
//
// Set appearance threshold to maximize the likelihood ratio.
//
int
SetAppearanceThresholds_f_given_s(
	vector<char*> &vecImgName, // Array of image names
	vector<int> &vecFeatToImgName, // Maps each Feat to its image name
	vector<MODEL_FEATURE> &vecFeats, // Array of features
	vector<IndexInfo> &vecIndexInfo, // IndexInfo
	vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	vector<int> &vecFeatValid, // For each Feat, valid flags

	vector<float> &vecFeatAppearanceThresholds, // For each Feat, threshold
	float fMinLikelihoodRatio, // The 
	int bSetMaxLikelhoodRatio = 0 // If 1, set threshold to maximize likelihood ratio
									// if 0, set threshold to last likelihood ratio which was above fMinLikelihoodRatio.
	);

//
// FlagInvalidFeatures()
//
// This function clusters features
int
FlagInvalidFeatures(
	vector<char*>		&vecImgName, // Array of image names
	vector<int>			&vecFeatToImgName, // Maps each Feat to its image name
	vector<MODEL_FEATURE>	&vecFeats, // Array of features
	vector<IndexInfo>	&vecIndexInfo, // IndexInfo
	vector<int>			&vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int>			&vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	vector<int>			&vecFeatValid,	// For each Feat, is this feature valid?
	vector<float>		&vecFeatAppearanceThresholds, // For each Feat, appearance thresholds

	vector<int>			&vecOrderToFeat,	// Feature indices, sorted from most to least frequent
	int iMinMatches
	//FeatureInstanceCallback *pfiCallback
);

//
// SetAppearanceThresholds_f_given_s_labels()
//
// Set appearance threshold to maximize the likelihood ratio, except here
// consider image labels. All features labelled differently from the current
// feature are considered negative examples.
//
int
SetAppearanceThresholds_f_given_s_labels(
	vector<char*> &vecImgName, // Array of image names
	vector<int> &vecFeatToImgName, // Maps each Feat to its image name
	vector<MODEL_FEATURE> &vecFeats, // Array of features
	vector<IndexInfo> &vecIndexInfo, // IndexInfo
	vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	vector<int> &vecFeatValid, // For each Feat, valid flags

	vector<int> &vecImgLabels, // For each image, a label

	vector<float> &vecFeatAppearanceThresholds, // For each Feat, threshold
	float fMinLikelihoodRatio,
	int bSetMaxLikelhoodRatio
	);

//
// SetAppearanceThresholds_f_given_s_labels_new()
// 10:27 AM 8/6/2009
//
// Here I experiment with a few new ways of definining features.
//  1) Consider features whose class relationship is strictly monotonic
//  2) Consider widening the appearance threshold to include more examples?
//
int
SetAppearanceThresholds_f_given_s_labels_new(
	vector<char*> &vecImgName, // Array of image names
	vector<int> &vecFeatToImgName, // Maps each Feat to its image name
	vector<MODEL_FEATURE> &vecFeats, // Array of features
	vector<IndexInfo> &vecIndexInfo, // IndexInfo
	vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	vector<int> &vecFeatValid, // For each Feat, valid flags

	vector<int> &vecImgLabels, // For each image, a label

	vector<float> &vecFeatAppearanceThresholds, // For each Feat, threshold
	float fMinLikelihoodRatio,
	int bSetMaxLikelhoodRatio
	);

//
// SetAppearanceThresholds_f_given_s_labels_new2()
//
// Experiment with defining threshold - first time ratio dips below fMinLikeRatio after ratio peaks
//            *
//  *        * *
// *  *    *     *   *
//------*---------*-*-*----------------------
//                 *   *
int
SetAppearanceThresholds_f_given_s_labels_new2(
	vector<char*> &vecImgName, // Array of image names
	vector<int> &vecFeatToImgName, // Maps each Feat to its image name
	vector<MODEL_FEATURE> &vecFeats, // Array of features
	vector<IndexInfo> &vecIndexInfo, // IndexInfo
	vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	vector<int> &vecFeatValid, // For each Feat, valid flags

	vector<int> &vecImgLabels, // For each image, a label

	vector<float> &vecFeatAppearanceThresholds, // For each Feat, threshold
	float fMinLikelihoodRatio,
	int bSetMaxLikelhoodRatio
	);

//
// SetAppearanceThresholds_f_given_s_fixed()
//
// Set fixed threshold for all features.
// This was done to test leave-one-out classification.
//
int
SetAppearanceThresholds_f_given_s_fixed(
	vector<char*> &vecImgName, // Array of image names
	vector<int> &vecFeatToImgName, // Maps each Feat to its image name
	vector<MODEL_FEATURE> &vecFeats, // Array of features
	vector<IndexInfo> &vecIndexInfo, // IndexInfo
	vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	vector<int> &vecFeatValid, // For each Feat, valid flags

	vector<int> &vecImgLabels, // For each image, a label

	vector<float> &vecFeatAppearanceThresholds, // For each Feat, threshold
	float fMinLikelihoodRatio,
	int bSetMaxLikelhoodRatio
	);

//
// SetAppearanceThresholds_f_given_s_lowesterror()
//
// Calculate the error of classification strictly based
// on geometrically consistent features.
//
int
SetAppearanceThresholds_f_given_s_lowesterror(
	vector<char*> &vecImgName, // Array of image names
	vector<int> &vecFeatToImgName, // Maps each Feat to its image name
	vector<MODEL_FEATURE> &vecFeats, // Array of features
	vector<IndexInfo> &vecIndexInfo, // IndexInfo
	vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	vector<int> &vecFeatValid, // For each Feat, valid flags

	vector<int> &vecImgLabels, // For each image, a label

	vector<float> &vecFeatAppearanceThresholds, // For each Feat, threshold
	float fMinLikelihoodRatio,
	int bSetMaxLikelhoodRatio
	);

//
// FlagInvalidFeatures()
//
// This function clusters features. Features with labels different
// to the current feature are considered as being from a different cluster.
//
int
FlagInvalidFeatures_labels(
	vector<char*>		&vecImgName, // Array of image names
	vector<int>			&vecFeatToImgName, // Maps each Feat to its image name
	vector<MODEL_FEATURE>	&vecFeats, // Array of features
	vector<IndexInfo>	&vecIndexInfo, // IndexInfo
	vector<int>			&vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int>			&vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	vector<int>			&vecFeatValid,	// For each Feat, is this feature valid?
	vector<float>		&vecFeatAppearanceThresholds, // For each Feat, appearance thresholds

	vector<int>			&vecImgLabels,

	vector<int>			&vecOrderToFeat,	// Feature indices, sorted from most to least frequent
	int iMinMatches
	//FeatureInstanceCallback *pfiCallback
);

//
//
//
//
int
ValidFeaturesWithinThreshold_labels(
						int iFeat,
						vector<char*> &vecImgName, // Array of image names
						vector<int> &vecFeatToImgName, // Maps each Feat to its image name
						vector<MODEL_FEATURE> &vecFeats, // Array of features
						vector<IndexInfo> &vecIndexInfo, // IndexInfo
						vector<int> &vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
						vector<int> &vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
						vector<float> &vecFeatAppearanceThresholds, // For each Feat, appearance thresholds
						vector<int> &vecFeatValid, // For each Feat, valid flags

						vector<int> &vecImgLabels,

						int *piImages
	);



//
// BoostFeatures()
//
// Use boosting to reduce features to a minimal set for optimal linear classification.
//
int
BoostFeatures(
	vector<char*>		&vecImgName, // Array of image names
	vector<int>			&vecFeatToImgName, // Maps each Feat to its image name
	vector<MODEL_FEATURE>	&vecFeats, // Array of features
	vector<IndexInfo>	&vecIndexInfo, // IndexInfo
	vector<int>			&vecFeatToIndexInfo, // Maps each Feat to its IndexInfo in vecIndexInfo
	vector<int>			&vecFeatToIndexInfoCount, // For each Feat, the number of IndexInfos
	vector<int>			&vecFeatValid,	// For each Feat, is this feature valid?
	vector<float>		&vecFeatAppearanceThresholds, // For each Feat, appearance thresholds

	vector<int>			&vecImgLabels,

	vector<int>			&vecOrderToFeat,	// Feature indices, sorted from most to least frequent
	int iBoostedFeatureCount // Number of boosted features to identify
);



//
// EnforceROI()
//
// Invalidate & remove all features outside of the ROI
//
int
EnforceROI(
	vector<MODEL_FEATURE>	&vecFeats, // Array of features
	vector<int>			&vecFeatValid,	// For each Feat, is this feature valid?
	vector<int>			&vecOrderToFeat,	// Feature indices, sorted from most to least frequent
	int x1, int x2,
	int y1, int y2,
	int z1, int z2
);

//
// estimate_similarity_transform3D()
//
//
//
int
estimate_similarity_transform3D(
								vector<float> &vecP0,
								vector<float> &vecP1
					   );

// __MODELFEATURE3D_H__

#endif 
