

#include <stdio.h>
#include <assert.h>
#include "src_common.h"
#include "nifti1_io.h"


//#include "AffineTransform3D.h"
//int
//affine_transform_image(
//						   FEATUREIO &fio0,
//						   FEATUREIO &fio1,
//						   	at_mat44 *ptrans_affine
//					   )
//{
//	for( int zz = 0; zz < fio0.z; zz++ )
//	{
//		for( int yy = 0; yy < fio0.y; yy++ )
//		{
//			for( int xx = 0; xx < fio0.x; xx++ )
//			{
//				float pfP0[3];
//				pfP0[0] = xx;
//				pfP0[1] = yy;
//				pfP0[2] = zz;
//
//				float pfP1[3];
//				reg_mat44_mul(ptrans_affine, pfP0, pfP1);
//
//				// Get pixel - trilinear interpolation requires adding 0.5 apparently
//				float fPix1 = fioGetPixelTrilinearInterp(
//					fio1,
//					pfP1[0] + 0.5f,
//					pfP1[1] + 0.5f,
//					pfP1[2] + 0.5f );
//
//				float *pfVec0 = fioGetVector( fio0, xx, yy, zz );
//				*pfVec0 = fPix1;
//			}
//		}
//	}
//	return 1;
//}


template <class NewTYPE, class DTYPE>
void reg_changeDatatype1(nifti_image *image)
{
	// the initial array is saved and freeed
	DTYPE *initialValue = (DTYPE *)malloc(image->nvox*sizeof(DTYPE));
	memcpy(initialValue, image->data, image->nvox*sizeof(DTYPE));
	
	// the new array is allocated and then filled
	if(sizeof(NewTYPE)==sizeof(unsigned char)) image->datatype = NIFTI_TYPE_UINT8;
	else if(sizeof(NewTYPE)==sizeof(float)) image->datatype = NIFTI_TYPE_FLOAT32;
	else if(sizeof(NewTYPE)==sizeof(double)) image->datatype = NIFTI_TYPE_FLOAT64;
	else{
		printf("err\treg_changeDatatype\tOnly change to unsigned char, float or double are supported\n");
		free(initialValue);
		return;
	}
	free(image->data);
	image->nbyper = sizeof(NewTYPE);
	image->data = (void *)calloc(image->nvox,sizeof(NewTYPE));
	NewTYPE *dataPtr = static_cast<NewTYPE *>(image->data);
	for(unsigned int i=0; i<image->nvox; i++)
		dataPtr[i] = (NewTYPE)(initialValue[i]);

	free(initialValue);
	return;
}

template <class NewTYPE>
void reg_changeDatatype(nifti_image *image)
{
	switch(image->datatype){
		case NIFTI_TYPE_UINT8:
			reg_changeDatatype1<NewTYPE,unsigned char>(image);
			break;
		case NIFTI_TYPE_INT8:
			reg_changeDatatype1<NewTYPE,char>(image);
			break;
		case NIFTI_TYPE_UINT16:
			reg_changeDatatype1<NewTYPE,unsigned short>(image);
			break;
		case NIFTI_TYPE_INT16:
			reg_changeDatatype1<NewTYPE,short>(image);
			break;
		case NIFTI_TYPE_UINT32:
			reg_changeDatatype1<NewTYPE,unsigned int>(image);
			break;
		case NIFTI_TYPE_INT32:
			reg_changeDatatype1<NewTYPE,int>(image);
			break;
		case NIFTI_TYPE_FLOAT32:
			reg_changeDatatype1<NewTYPE,float>(image);
			break;
		case NIFTI_TYPE_FLOAT64:
			reg_changeDatatype1<NewTYPE,double>(image);
			break;
		default:
			printf("err\treg_changeDatatype\tThe initial image data type is not supported\n");
			return;
	}
}

//
// fioReadNifti()
//
// Read nifty, optionally convert to isotropic.
//
int
fioReadNifti(
			 FEATUREIO &fioIn,
			 char *pcFileName,
			 int bIsotropic = 0,
			 int iRescale = 0,
			 nifti_image *returnHeader = 0
			 )
{
	/* Read the target and source images */
	nifti_image *targetHeader = nifti_image_read(pcFileName,true);
	if(targetHeader == NULL )
	{
		return -1;
	}

	if( targetHeader->data == NULL )
	{
		return -2;
	}

	reg_changeDatatype<float>(targetHeader);

	FEATUREIO fioTmp;
	memset( &fioTmp, 0, sizeof(FEATUREIO) );
	memset( &fioIn, 0, sizeof(FEATUREIO) );

	fioTmp.x = targetHeader->nx;
	fioTmp.y = targetHeader->ny;
	fioTmp.z = targetHeader->nz;
	fioTmp.t = targetHeader->nt > 0 ? targetHeader->nt : 1;
	fioTmp.iFeaturesPerVector = 1;
	fioTmp.pfVectors = (float*)targetHeader->data;

	if( bIsotropic &&
		(targetHeader->dx != targetHeader->dy
			|| targetHeader->dy != targetHeader->dz
				|| targetHeader->dx != targetHeader->dz)
				)
	{
		float fMinSize = targetHeader->dx;
		if( targetHeader->dy < fMinSize )
			fMinSize = targetHeader->dy;
		if( targetHeader->dz < fMinSize )
			fMinSize = targetHeader->dz;

		fioIn.x = targetHeader->nx*targetHeader->dx / fMinSize;
		fioIn.y = targetHeader->ny*targetHeader->dy / fMinSize;
		fioIn.z = targetHeader->nz*targetHeader->dz / fMinSize;
		fioIn.t = targetHeader->nt > 0 ? targetHeader->nt : 1;
		fioIn.iFeaturesPerVector = 1;
		if( !fioAllocate( fioIn ) )
		{
			// Not enough memory to resample :(
			return -3;
		}

		// Determine which matrix to use - pick the non-zero one
		//float fSumQ = fabs(targetHeader->qto_ijk.m[0][0])+fabs(targetHeader->qto_ijk.m[0][2])+fabs(targetHeader->qto_ijk.m[0][2])+fabs(targetHeader->qto_ijk.m[0][3]);
		//float fSumS = fabs(targetHeader->sto_ijk.m[0][0])+fabs(targetHeader->sto_ijk.m[0][2])+fabs(targetHeader->sto_ijk.m[0][2])+fabs(targetHeader->sto_ijk.m[0][3]);
		//int bEqualMatrices = (memcmp( &(targetHeader->qto_ijk.m[0][0]), &(targetHeader->sto_ijk.m[0][0]), sizeof(targetHeader->qto_ijk.m) ) == 0);

		float x,y,z;

		// Generate multiplication factors for rescaling/upscaling
		// These factors shrink isotropic coordinates down to anisotropic.
		float pfRescaleFactorsIJK[3];
		pfRescaleFactorsIJK[0] = fMinSize / targetHeader->dx;
		pfRescaleFactorsIJK[1] = fMinSize / targetHeader->dy;
		pfRescaleFactorsIJK[2] = fMinSize / targetHeader->dz;

		// This code is just to test the transforms
		Feature3DInfo feat;
		feat.x = 10; feat.y = 20; feat.z = 30;
		feat.scale = 5;
		feat.SimilarityTransform( &targetHeader->qto_xyz.m[0][0] );
		feat.SimilarityTransform( &targetHeader->qto_ijk.m[0][0] );

		// Apply rescale factors ijk, one per factor per column.
		// These will be used to compute feature coordinates in xyz
		// Note: scaling factors operate on ijk voxel coordinates, so 
		// apply one rescaling/normalization factor per transform matrix column.
		// Essentially, we rescale the direction cosine angles.
		for( int i = 0; i < 3; i++ )
		{
			for( int j = 0; j < 3; j++ )
			{
				targetHeader->qto_xyz.m[i][j] *= pfRescaleFactorsIJK[j];
				if( targetHeader->sform_code > 0 )
					targetHeader->sto_xyz.m[i][j] *= pfRescaleFactorsIJK[j];
			}
		}

		// Compute inverses
		targetHeader->qto_ijk = nifti_mat44_inverse( targetHeader->qto_xyz );
		if( targetHeader->sform_code > 0 )
			targetHeader->sto_ijk = nifti_mat44_inverse( targetHeader->sto_xyz );

		feat.SimilarityTransform( &targetHeader->qto_xyz.m[0][0] );
		feat.SimilarityTransform( &targetHeader->qto_ijk.m[0][0] );

		for( int z = 0; z < fioIn.z; z++ )
		{
			for( int y = 0; y < fioIn.y; y++ )
			{
				for( int x = 0; x < fioIn.x; x++ )
				{
					float pXYZ[3];

					// * This is the original code, I believe it is slightly
					// incorrect, the 0.5 should be applied after multiplication/division
					//float fPixel = fioGetPixelTrilinearInterp( fioTmp,
					//		(x*fMinSize+0.5)/targetHeader->dx,
					//		(y*fMinSize+0.5)/targetHeader->dy,
					//		(z*fMinSize+0.5)/targetHeader->dz
					//		);					

					float fPixel = fioGetPixelTrilinearInterp( fioTmp,
							(x*pfRescaleFactorsIJK[0]+0.5),
							(y*pfRescaleFactorsIJK[1]+0.5),
							(z*pfRescaleFactorsIJK[2]+0.5)
							);

					float *pfDestPix = fioGetVector( fioIn, x, y, z );
					*pfDestPix = fPixel;
				}
			}
		}

		// Reset changed fields
		targetHeader->dx = fMinSize;
		targetHeader->dy = fMinSize;
		targetHeader->dz = fMinSize;

		//fioWrite( fioIn, "out-iso2"  );

	}
	else
	{
		// Allocate & copy image to new data structure
		// (old will be deleted ... )
		fioIn = fioTmp;
		fioAllocate( fioIn );
		fioCopy( fioIn, fioTmp );
	}

	// Save return target header information
	if( returnHeader != 0 )
		memcpy( returnHeader, targetHeader, sizeof(nifti_image) );

	nifti_image_free( targetHeader );
	return 1;
}

int
fioInvertIntensity(
				   FEATUREIO &fio
				   )
{
	float fMin, fMax;
	fioFindMinMax( fio, fMin, fMax );
	int iFeats = fio.x*fio.y*fio.z*fio.t*fio.iFeaturesPerVector;
	for( int i = 0; i < iFeats; i++ )
	{
		fio.pfVectors[i] = fMin + (fMax-fio.pfVectors[i]);
	}
	return 1;
}



int
fioOutputHistogram(
		FEATUREIO &fio,
			 float fMin,			// Min value
			 float fMax,
			 int iBins,
			 char *pcFileName
				   )
{
	vector< int > vecHist;
	vecHist.resize( iBins, 0 );
	float fRange = fMax-fMin;
	assert( fRange > 0 );
	for( int z = 0; z < fio.z; z++ )
	{
		int iZIndex = z*fio.y*fio.x*fio.iFeaturesPerVector;
		for( int y = 0; y < fio.y; y++ )
		{
			int iYIndex = iZIndex + y*fio.x*fio.iFeaturesPerVector;
			for( int x = 0; x < fio.x; x++ )
			{
				int iXIndex = iYIndex + x*fio.iFeaturesPerVector;
				for( int k = 0; k < fio.iFeaturesPerVector; k++ )
				{
					float fValue = fio.pfVectors[iXIndex + k];
					int iBin = ((fValue-fMin)/(fRange+1.0))*iBins;
					vecHist[iBin]++;
				}
			}
		}
	}

	FILE *outfile =fopen( pcFileName, "wt" );
	for( int i = 0; i < vecHist.size(); i++ )
	{
		fprintf( outfile, "%d\t%d\n", i, vecHist[i] );
	}
	fclose( outfile );

	return 0;
}

int
fioThreshold(
			 FEATUREIO &fio,
			 float fThresh
			 )
{
	PpImage ppImg;
	ppImg.Initialize( fio.y, fio.x, fio.x, 8 );

	for( int y = 0; y < fio.y; y++ )
	{
		unsigned char *pucRow = ppImg.ImageRow( y );
		for( int x = 0; x < fio.x; x++ )
		{
			float fValue = fioGetPixel( fio, x, y );
			if( fValue > fThresh )
			{
				pucRow[x] = 255;
			}
			else
			{
				pucRow[x] = 0;
			}
		}
	}

	int iThreshold = fThresh;
	char pcFileName[300];
	sprintf( pcFileName, "output\\thres%5.5d.pgm", iThreshold );
	ppImg.WriteToFile( pcFileName );

	return 0;
}


int
fioTestThresholds(
			 FEATUREIO &fio
				  )
{
	FILE *outfile = fopen( "stuff", "wt" );
	fprintf( outfile, "dude\n" );
	fclose( outfile );
	for( int i = 1500; i < 3000; i += 10 )
	{
		fioThreshold( fio, i );
	}
	return 1;
}

int
fioTestThresholds(
			 FEATUREIO &fio,
			 FEATUREIO &fioMask
				  )
{
	FILE *outfile = fopen( "stuff", "wt" );
	fprintf( outfile, "dude\n" );
	fclose( outfile );
	for( int i = 1500; i < 3000; i += 10 )
	{
		fioThreshold( fio, i );
	}
	return 1;
}


//
// C:\downloads\data\oasis\example\OAS1_0002_MR1_mpr_n4_anon_111_t88_gfc.bin C:\downloads\data\oasis\example\OAS1_0002_MR1_mpr_n4_anon_111_t88_gfc.key
// C:\downloads\data\oasis\example\OAS1_0003_MR1_mpr_n4_anon_111_t88_gfc.bin C:\downloads\data\oasis\example\OAS1_0003_MR1_mpr_n4_anon_111_t88_gfc.key
// C:\downloads\data\oasis\example\tp1_rs256.img C:\downloads\data\oasis\example\tp1_rs256.key
// E:\code\Windows\head-neck\ct1-1rig-crop.key E:\code\Windows\head-neck\ct2-2rig-crop.key out.txt
// E:\code\Windows\head-neck\ct2-2rig-crop.hdr E:\code\Windows\head-neck\ct2-2rig-crop2.key
// -2 C:\downloads\data\styner\test_data\11048_092309_combined_result.hdr C:\downloads\data\styner\test_data\11048_092309_combined_result.txt 
// -2 C:\downloads\data\styner\test_data\11053_0816_combined_result.hdr C:\downloads\data\styner\test_data\11053_0816_combined_result.txt
// -2 C:\downloads\data\styner\combined_feature\normal\11053_0816_combined_result.img stuff.key
// C:\downloads\data\oasis\repeatability\OAS1_0001_MR1\RAW\OAS1_0001_MR1_mpr-1_anon.hdr out.key
//
// C:\downloads\data\UCL\IXI\IXI-T1\IXI109-Guys-0732-T1.nii.gz C:\downloads\data\UCL\IXI\IXI-T1\IXI109-Guys-0732-T1.key
//
// Marc Modat: Register prone/supine images of the same person.
// C:\downloads\data\marc\P_halved.nii.gz C:\downloads\data\marc\P_halved.key
//
//
// Andry - prostate
// -2- C:\downloads\data\andriy\ProstateRegistrationValidation\Images\sCase1-t2ax.hdr C:\downloads\data\andriy\ProstateRegistrationValidation\Images\sCase1-t2ax.key 
//
// Testing world coordinates:
// -w C:\downloads\data\RIRE\tests\extract-original\patient_101_mr_T1.nii C:\downloads\data\RIRE\tests\extract-original\patient_101_mr_T1.key
//
int
print_options(
			  )
{
		printf( "Volumetric local feature extraction v1.1\n");
		printf( "Usage: %s [options] <input image> <output features>\n", "featExtract" );
		printf( "  <input image>: nifti (.nii,.hdr,.nii.gz).\n" );// or raw input volume (IEEE 32-bit float, little endian).\n" );
		printf( "  <output features>: output file with features.\n" );
		printf( " [options]\n" );
		printf( "  -w         : output feature geometry in world coordinates, NIFTI qto_xyz matrix (default is voxel units).\n" );
		printf( "  -2+        : double input image size.\n" );
		printf( "  -2-        : halve input image size.\n" );
		//printf( "  -text      : output text keypoint file (default binary).\n" );
		//printf( "  -e <value> : edge threshold, lower values result in fewer but more distinct features (default 140).\n" );
		//printf( "  -i         : image converted to isotropic prior to extraction (default no conversion).\n" );
		return 1;
}

//
// -w C:\downloads\data\MNI\bite-us\group2\01\01_mr_tal.nii C:\downloads\data\MNI\bite-us\group2\01\01_mr_tal.key
// 

int
main(
	 int argc,
	 char **argv
)
{
	// Write the code to test the scale selection stuff

	FEATUREIO	fioIn;
	PpImage		ppImgIn;

	if( argc < 3 )
	{
		print_options();
		return -1;
	}
	int iArg = 1;
	int bDoubleImageSize = 0;
	int bOutputText = 1;
	int bWorldCoordinates = 0;
	int bIsotropicProcessing = 0;
	int iFlipCoord = 0; // Flip a coordinate system: x=1,y=2,z=3
	float fEigThres = 140;
	int bMultiModal = 0;
	while( argv[iArg][0] == '-' )
	{
		switch( argv[iArg][1] )
		{
			// Double initial image resolution option
		case '2':
			// Option to double initial image size, get 8 times as many features...
			bDoubleImageSize = 1;
			if( argv[iArg][2] == '-' )
			{
				// Halve image resolution
				bDoubleImageSize = -1;
			}
			iArg++;
			break;

		case 'b': case 'B':
			// Option to output binary
			bOutputText = 0;
			iArg++;
			break;

		case 'm': case 'M':
			// Option to extract multi-modal features
			bMultiModal = 1;
			iArg++;
			break;

		case 'q': case 'Q':
			// Option to output world coordinates
			// Performs isotropic extraction, otherwise world coordinates
			// doesn't make sense with isotropic scale invariant features
			// Same as options 'w' 'W' below
		case 'w': case 'W':
			// Option to output world coordinates
			// Performs isotropic extraction, otherwise world coordinates
			// doesn't make sense
			bWorldCoordinates = 1;
			bIsotropicProcessing = 1;
			if( argv[iArg][2] == 's' || argv[iArg][2] == 'S' )
			{
				// Optionally use the nifti sform coordinate system, if available 
				bWorldCoordinates = 2;
			}
			iArg++;
			break;

		case 'e': case 'E':
			// Eigenvalue threshold
			iArg++;
			fEigThres = atof( argv[iArg] );
			if( fEigThres <= 0 )
			{
				printf( "Error: invalid eigenvalue threshold %f\n", fEigThres );
				return -1;
			}
			iArg++;
			break;

		case 'i': case 'I':
			bIsotropicProcessing = 1;
			iArg++;
			break;

		case 'f': case 'F':
			// Flip coordinate system
			iFlipCoord = 1;
			iArg++;
			break;

		default:
			printf( "Error: unknown command line argument: %s\n", argv[iArg] );
			print_options();
			return -1;
			break;
		}
	}
	printf( "Extracting features: %s\n", argv[iArg] );

	nifti_image returnHeader;
	if( fioReadNifti( fioIn, argv[iArg], bIsotropicProcessing, 0, &returnHeader ) < 0 )
	{
		//if( fioReadRaw( fioIn, argv[iArg] ) < 0 )
		{
			printf( "Error: could not read input file: %s\n", argv[iArg] );
			return -1;
		}
	}

	//PpImage ppImgTmp;
	//ppImgTmp.Initialize( fioIn.y, fioIn.x, fioIn.x*sizeof(float), sizeof(float)*8 );
	//fioFeatureSliceXY( fioIn, fioIn.z/2, 0, (float*)ppImgTmp.ImageRow(0) );
	//output_float( ppImgTmp, "midslice1_raw.pgm" );

	// Define initial image pixel size
	float fInitialBlurScale = 1.0f;
	if( bDoubleImageSize != 0 )
	{
		if( bDoubleImageSize == 1 )
		{
			// Performing image doubling halves the size of pixels
			fioDoubleSize( fioIn );
			fInitialBlurScale *= 0.5;
		}
		else if( bDoubleImageSize == -1 )
		{
			// Reduce image size, initial pi
			FEATUREIO fioTmp = fioIn;
			fioIn.x /= 2;
			fioIn.y /= 2;
			fioIn.z /= 2;
			fioAllocate( fioIn );
			fioSubSample2DCenterPixel( fioTmp, fioIn );
			fioDelete( fioTmp );
			//fInitialBlurScale *= ;
			//fioWrite( fioIn, "subsample" );
		}
	}

	if( iFlipCoord )
	{
		// Test mirroring right/left hemispheres in OASIS
		//fioWrite( fioIn, "_original" );
		fioFlipAxisX(fioIn );
		//fioWrite( fioIn, "_flipped" );
	}

	//
	// Simple extraction & visualization
	// For binary distribution

	if( fioIn.z <= 1 )
	{
 		printf( "Could not read volume: %s\n", argv[iArg] );
		return -1;
	}

	//FEATUREIO fioG1, fioTemp, fioG2;
	//fioG1 = fioIn;
	//fioTemp = fioIn;
	//fioG2 = fioIn;
	//fioAllocate( fioTemp );
	//fioAllocate( fioG2 );
	//gb3d_blur3d( fioG1, fioTemp, fioG2, 2.3, 0.01 );
	//fioWrite( fioG1, "original" );
	//fioWrite( fioG2, "original-vyv5-2.3" );

	printf( "Input image: i=%d j=%d k=%d\n", fioIn.x, fioIn.y, fioIn.z );

	vector<Feature3D> vecFeats3D;
	int iReturn;
	try
	{
		// to fix here!!
		// Something is broken here, extraction returns way too many features. Release version might work.
		//iReturn = msGeneratePyramidDOG3D( fioIn, vecFeats3D, fInitialBlurScale, 0, 0, fEigThres );
		iReturn = msGeneratePyramidDOG3D_efficient( fioIn, vecFeats3D, fInitialBlurScale, 0, 0, fEigThres );
		if( bMultiModal )
		{
			printf( "\n" );
			fioInvertIntensity( fioIn );
			iReturn = msGeneratePyramidDOG3D( fioIn, vecFeats3D, fInitialBlurScale, 0, 0, fEigThres );
		}
	}
	catch (...)
	{
		printf( "Error: could not extract features, insufficient memory.\n" );
		return -1;
	}
	if( iReturn != 1 )
	{
		printf( "Error: could not extract features, insufficient memory.\n" );
		return -1;
	}

	// Prepare scaling factor
	float fSizeFactor = 1;
	if( bDoubleImageSize > 0 )
		fSizeFactor /= 2;
	else if( bDoubleImageSize < 0 )
		fSizeFactor *= 2;

	// Prepare transform to world coordinates
	// Scale should be isotropic in matrix gto_xyz
	float pfScale[3];
	float fScaleSum = 0;
	float pfRot3x3[3][3];
	mat44 m_current;

	if( bWorldCoordinates )
	{
		if( !bIsotropicProcessing )
		{
			printf( "Error: world coordinate output with anisotropic extraction.\n" );
			return -1;
		}
		// Feature orientation vectors need to be rotated to xyz
		// Points need transformed
		// Scale needs to be adjusted

		mat44 *pm = &returnHeader.qto_xyz;
		if( bWorldCoordinates == 2 )
		{
			if( returnHeader.sform_code > 0 )
			{
				pm = &returnHeader.sto_xyz;
			}
			else
			{
				printf( "Error: sform_code <= 0, output to qto_xyz instead of sto_xyz" );
			}
		}

		memcpy( &m_current.m, pm->m, sizeof(mat44) );

		for( int i = 0; i < 3; i++ )
		{
			// Figure out scaling - should be isotropic
			pfScale[i] = vec3D_mag( &pm->m[i][0] );
			fScaleSum += pfScale[i];

			// Create rotation matrix - should be able to rescale
			memcpy( &pfRot3x3[i][0], &pm->m[i][0], 3*sizeof(float) );
			vec3D_norm_3d( &pfRot3x3[i][0] );
		}
		fScaleSum /= 3;
	}

	for( int iFeat = 0; iFeat < vecFeats3D.size(); iFeat++ )
	{
		Feature3DInfo &featHere = vecFeats3D[iFeat];
		vecFeats3D[iFeat].NormalizeData();
		msResampleFeaturesGradientOrientationHistogram(vecFeats3D[iFeat]);
		//msResampleFeaturesGradientOrientationHistogram2(vecFeats3D[iFeat]);
		//msResampleFeaturesRotationInvariantHistogram(vecFeats3D[iFeat]);
		vecFeats3D[iFeat].NormalizeDataRankedPCs();

		// Incorporate scale/size factor on pixel ijk coordinates
		vecFeats3D[iFeat].x *= fSizeFactor;
		vecFeats3D[iFeat].y *= fSizeFactor;
		vecFeats3D[iFeat].z *= fSizeFactor;
		vecFeats3D[iFeat].scale *= fSizeFactor;

		if( bWorldCoordinates )
		{
			if( !bIsotropicProcessing )
			{
				printf( "Error: world coordinate output with anisotropic extraction.\n" );
				return -1;
			}

			// Convert feature coordinates
			float pfIJK[4];
			float pfXYZ[4];
			pfIJK[0] = vecFeats3D[iFeat].x;
			pfIJK[1] = vecFeats3D[iFeat].y;
			pfIJK[2] = vecFeats3D[iFeat].z;
			pfIJK[3] = 1;
			mult_4x4_vector(&m_current.m[0][0], pfIJK, pfXYZ );
			vecFeats3D[iFeat].x = pfXYZ[0];
			vecFeats3D[iFeat].y = pfXYZ[1];
			vecFeats3D[iFeat].z = pfXYZ[2];

			// Convert scale
			vecFeats3D[iFeat].scale *= fScaleSum;

			// Rotate orientation vectors 
			// Feature ori matrix contains direction cosine vectors in columns.
			// Rotation should multiply ori from the left hand side:
			//	vecFeats3D[iFeat].ori = pfRot3x3 * vecFeats3D[iFeat].ori
			float pfOri[3][3];
			float pfOriOut[3][3];
			invert_3x3<float,float>( vecFeats3D[iFeat].ori, pfOri );
			mult_3x3_matrix<float,float>( pfRot3x3, pfOri, pfOriOut );
			invert_3x3<float,float>( pfOriOut, vecFeats3D[iFeat].ori );
		}
	}

	// Now convert

	char *ppcComments[3];
	char pcComment1[200];
	char pcComment2[200];
	char pcComment3[400];
	sprintf( pcComment1, "Extraction Voxel Resolution (ijk) : %d %d %d", fioIn.x, fioIn.y, fioIn.z );
	sprintf( pcComment2, "Extraction Voxel Size (mm)  (ijk) : %f %f %f", 1.0f*returnHeader.dx, 1.0f*returnHeader.dy, 1.0f*returnHeader.dz );
	if( bWorldCoordinates )
	{
		if( bWorldCoordinates == 1 )
		{
			sprintf( pcComment3, "Feature Coordinate Space: millimeters (qto_xyz) : %f %f %f %f %f %f %f %f %f %f %f %f 0.0 0.0 0.0 1.0",
				1.0f*m_current.m[0][0],1.0f*m_current.m[0][1],1.0f*m_current.m[0][2],1.0f*m_current.m[0][3],
				1.0f*m_current.m[1][0],1.0f*m_current.m[1][1],1.0f*m_current.m[1][2],1.0f*m_current.m[1][3],
				1.0f*m_current.m[2][0],1.0f*m_current.m[2][1],1.0f*m_current.m[2][2],1.0f*m_current.m[2][3] );
		}
		else if( bWorldCoordinates == 2 )
		{
			sprintf( pcComment3, "Feature Coordinate Space: millimeters (sto_xyz) : %f %f %f %f %f %f %f %f %f %f %f %f 0.0 0.0 0.0 1.0",
				1.0f*m_current.m[0][0],1.0f*m_current.m[0][1],1.0f*m_current.m[0][2],1.0f*m_current.m[0][3],
				1.0f*m_current.m[1][0],1.0f*m_current.m[1][1],1.0f*m_current.m[1][2],1.0f*m_current.m[1][3],
				1.0f*m_current.m[2][0],1.0f*m_current.m[2][1],1.0f*m_current.m[2][2],1.0f*m_current.m[2][3] );
		}
	}
	else
	{
		sprintf( pcComment3, "Feature Coordinate Space: voxels: 1.0 0.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0 0.0 0.0 0.0 0.0 1.0" );
	}

	ppcComments[0] = pcComment1;
	ppcComments[1] = pcComment2;
	ppcComments[2] = pcComment3;
	if( bOutputText )
	{
		msFeature3DVectorOutputText( vecFeats3D, argv[iArg+1], fEigThres, 3, ppcComments );
	}
	else
	{
		msFeature3DVectorOutputBin( vecFeats3D, argv[iArg+1], fEigThres );
	}

	printf( "\nDone.\n" );

	return 0;
}



int
main_test(
	 int argc,
	 char **argv
)
{
	// Write the code to test the scale selection stuff

	FEATUREIO	fioIn;
	FEATUREIO	fioMask;
	FEATUREIO	fioBrain;
	PpImage		ppImgIn;

	int bIsotropicProcessing = 0;

	nifti_image returnHeader;
	if( fioReadNifti( fioIn, argv[1], bIsotropicProcessing, 0, &returnHeader ) < 0 )
	{
		//if( fioReadRaw( fioIn, argv[iArg] ) < 0 )
		{
			printf( "Error: could not read input file: %s\n", argv[1] );
			return -1;
		}
	}

	nifti_image returnHeaderMask;
	if( fioReadNifti( fioMask, argv[2], bIsotropicProcessing, 0, &returnHeaderMask ) < 0 )
	{
		//if( fioReadRaw( fioIn, argv[iArg] ) < 0 )
		{
			printf( "Error: could not read input file: %s\n", argv[2] );
			return -1;
		}
	}

	//nifti_image returnHeaderBrain;
	//if( fioReadNifti( fioBrain, argv[3], bIsotropicProcessing, 0, &returnHeaderBrain ) < 0 )
	//{
	//	//if( fioReadRaw( fioIn, argv[iArg] ) < 0 )
	//	{
	//		printf( "Error: could not read input file: %s\n", argv[3] );
	//		return -1;
	//	}
	//}

	fioMultiply( fioIn, fioMask, fioIn );	

	int ix, iy, iz;
	float fValue;
	fioFindMax( fioIn, ix, iy, iz, fValue );

	printf( "Max: %f\tLesion: %f\n", fValue, fioGetPixel( fioMask, ix, iy, iz ) );

	Feature3DInfo feat;
	feat.x=ix;
	feat.y=iy;
	feat.z=iz;
	feat.scale = 10;
	feat.OutputVisualFeatureInVolume( fioIn, argv[1], 1, 1 );

	return 1;
	
	float fmin, fmax;
	fioFindMinMax( fioIn, fmin, fmax );

	FEATUREIO fioXY;
	fioXY = fioIn;
	fioXY.z = 1;
	fioXY.pfVectors = fioGetVector( fioIn, 0, 0, feat.z );
	fioOutputHistogram( fioXY, 0, 3000, 500, "histogram03.txt" );

	fioTestThresholds(fioXY);
	return 1;
}

