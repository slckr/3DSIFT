
#include <stdio.h>
#include <stdlib.h>
#include <vector>

#include "FeatureIOnifti.h"
#include "featMatchUtilities.h"
#include "src_common.h"
#include "nifti1_io.h"

//#include "AffineTransform3D.h"

#include "svdcmp.h"
#include "DoubleArray.h"

#define PI 3.14159265359

using namespace std;


int
readPointFile4(
			  char *pcFileName,
			  std::vector<float> &vecFeats
			  )
{
	FILE *infile = fopen( pcFileName, "rt" );
	if( !infile )
		return -1;

	char pcBuffer[1000];
	float x,y,z,scale;
	while( fgets( pcBuffer, sizeof(pcBuffer), infile ) )
	{
		// Look for preceding tab
		char *pch = strchr( pcBuffer, '\t' );
		if( !pch )
		{
			continue;
		}
		if( sscanf( pch, "\t%f\t%f\t%f\t%f", &x, &y, &z, &scale ) == 4 )
		{
			vecFeats.push_back(x);
			vecFeats.push_back(y);
			vecFeats.push_back(z);
			vecFeats.push_back(scale);
		}
	}
	fclose( infile );

	return vecFeats.size()/4;
}

int
readPointFile3(
			  char *pcFileName,
			  std::vector<float> &vecFeats
			  )
{
	FILE *infile = fopen( pcFileName, "rt" );
	if( !infile )
		return -1;

	char pcBuffer[1000];
	float x,y,z,scale=0;
	while( fgets( pcBuffer, sizeof(pcBuffer), infile ) )
	{
		// Look for preceding tab
		char *pch = strchr( pcBuffer, '\t' );
		if( !pch )
		{
			continue;
		}
		if( sscanf( pch, "\t%f\t%f\t%f\t%f", &x, &y, &z ) == 3 )
		{
			vecFeats.push_back(x);
			vecFeats.push_back(y);
			vecFeats.push_back(z);
			vecFeats.push_back(scale);
		}
	}
	fclose( infile );

	return vecFeats.size()/3;
}

//
// writePointFile4()
//
// 
//
int
writePointFile4(
			  char *pcFileNameOut,
			  std::vector<float> &vecFeats,
			  char *pcFileNameIn = 0
			  )
{
	FILE *infile;
	FILE *outfile;

	if( pcFileNameIn && (infile = fopen( pcFileNameIn, "rt" ) ) )
	{
		char pcBuffer[1000];
		float x,y,z,scale;
		while( fgets( pcBuffer, sizeof(pcBuffer), infile ) )
		{
			// Look for preceding tab
			char *pch = strchr( pcBuffer, '\t' );
			if( !pch )
			{
				continue;
			}
			if( sscanf( pch, "\t%f\t%f\t%f", &x, &y, &z, &scale ) == 4 )
			{
				vecFeats.push_back(x);
				vecFeats.push_back(y);
				vecFeats.push_back(z);
				vecFeats.push_back(scale);
			}
		}
		fclose( infile );
	}

	return vecFeats.size()/4;
}


float
peakFunction(
			 LOCATION_VALUE_XYZ &lv,
			 FEATUREIO &fio
			 )
{
	return 1;
}

int
outputMatches(
						 std::vector<float> &vecFeats1,
						 std::vector<float> &vecFeats2,
						 char *pcImage1,
						 char *pcImage2
						 )
{

	FEATUREIO fio1;
	FEATUREIO fio2;
	FEATUREIO fio2like;
	char pcFileName[400];
	char *pch;
	nifti_image headerImage2;
	if( fioReadNifti( fio1, pcImage1, 1 ) < 0 )
	{
		printf( "Error: could not read nifti image file %s\n", pcImage1 );
		return -1;
	}

	if( fioReadNifti( fio2, pcImage2, 1 ) < 0 )
	{
		printf( "Error: could not read nifti image file %s\n", pcImage2 );
		return -1;
	}

	for( int i = 0; i < vecFeats1.size()/4; i++ )
	{
		Feature3DInfo feat1;
		Feature3DInfo feat2;

		int piCoord1[3];	
		int piSize1[3];
		int piCoord2[3];
		int piSize2[3];
		fioSet( fio2like, 0 );

		float fScale = vecFeats1[4*i+3];
		float fSearchSize = fScale;

		piCoord1[0]	= vecFeats1[4*i+0];			piCoord1[1] = vecFeats1[4*i+1];			piCoord1[2] = vecFeats1[4*i+2];
		piSize1[0]	= fScale;		piSize1[1] = fScale;			piSize1[2] = fScale;

		piCoord2[0]	= vecFeats2[4*i+0];			piCoord2[1] = vecFeats2[4*i+1];			piCoord2[2] = vecFeats2[4*i+2];
		piSize2[0]	= fSearchSize;	piSize2[1] = fSearchSize;		piSize2[2] = fSearchSize;



		char pcFileName[300];
		sprintf( pcFileName, "feature%3.3d_1", i );
		feat1.x = piCoord1[0];feat1.y = piCoord1[1];feat1.z = piCoord1[2];feat1.scale=vecFeats1[4*i+3];
		feat1.OutputVisualFeatureInVolume( fio1, pcFileName, 1 );

		sprintf( pcFileName, "feature%3.3d_2", i );
		feat2.x = piCoord2[0];feat2.y = piCoord2[1];feat2.z = piCoord2[2];feat2.scale=vecFeats2[4*i+3];
		feat2.OutputVisualFeatureInVolume( fio2, pcFileName, 1 );
	}
	return 1;
}


int
refineMatches(
						 std::vector<float> &vecFeats1,
						 std::vector<float> &vecFeats2,
						 char *pcImage1,
						 char *pcImage2
						 )
{

	FEATUREIO fio1;
	FEATUREIO fio2;
	FEATUREIO fio2like;
	char pcFileName[400];
	char *pch;
	nifti_image headerImage2;
	if( fioReadNifti( fio1, pcImage1, 1 ) < 0 )
	{
		printf( "Error: could not read nifti image file %s\n", pcImage1 );
		return -1;
	}

	if( fioReadNifti( fio2, pcImage2, 1 ) < 0 )
	{
		printf( "Error: could not read nifti image file %s\n", pcImage2 );
		return -1;
	}

	if( fioReadNifti( fio2like, pcImage2, 1, 0, &headerImage2 ) < 0 )
	{
		printf( "Error: could not read nifti image file %s\n", pcImage2 );
		return -1;
	}

	headerImage2.data = fio2like.pfVectors;
	fioWriteNifti( fio2like, (void*)&headerImage2, "out.hdr" );

	fioSet( fio2like, 0.7 );

	for( int i = 0; i < vecFeats1.size()/4; i++ )
	{
		int piCoord1[3];	
		int piSize1[3];
		int piCoord2[3];
		int piSize2[3];
		fioSet( fio2like, 0 );

		float fScale = vecFeats1[4*i+3];
		float fSearchSize = fScale;

		piCoord1[0]	= vecFeats1[4*i+0];			piCoord1[1] = vecFeats1[4*i+1];			piCoord1[2] = vecFeats1[4*i+2];
		piSize1[0]	= fScale;		piSize1[1] = fScale;			piSize1[2] = fScale;

		piCoord2[0]	= vecFeats2[4*i+0];			piCoord2[1] = vecFeats2[4*i+1];			piCoord2[2] = vecFeats2[4*i+2];
		piSize2[0]	= fSearchSize;	piSize2[1] = fSearchSize;		piSize2[2] = fSearchSize;

		for( int j = 0; j < 3; j++ )
		{
			piSize1[j] = 4*piSize1[j]+1;
			piSize2[j] = 2*piSize2[j]+1;
		}

		// *** Test autocorrelation ***
		//hfGenerateLikelihoodImageCorrelation(
		////hfGenerateProbMILikelihoodImage(
		//	piCoord2,	// Center of box in fio1, xyzt, col/row/z/time
		//	piSize1,  // Size of box in fio1
		//	piCoord2,	// Center of box (search area) in fio2
		//	piSize2,	// Center of box (search area) in fio2

		//	fio2,
		//	fio2, fio2like
		//	);

				hfGenerateLikelihoodImageCorrelation(
		//hfGenerateProbMILikelihoodImage(
			piCoord1,	// Center of box in fio1, xyzt, col/row/z/time
			piSize1,  // Size of box in fio1
			piCoord2,	// Center of box (search area) in fio2
			piSize2,	// Center of box (search area) in fio2

			fio1,
			fio2, fio2like
			);

		// Write out peaks in current image

		LOCATION_VALUE_XYZ_ARRAY lvaPeaks;
		memset( &lvaPeaks, 0, sizeof(lvaPeaks) );
		regFindFEATUREIOPeaks( lvaPeaks, fio2like, peakFunction );
		lvaPeaks.plvz = new LOCATION_VALUE_XYZ[lvaPeaks.iCount];
		regFindFEATUREIOPeaks( lvaPeaks, fio2like, peakFunction );
		lvSortHighLow( lvaPeaks );

		//fioWriteNifti( fio2like, (void*)&headerImage2, "out.hdr" );

		Feature3DInfo feat1;
		Feature3DInfo feat2;
		Feature3DInfo feat2out;

		char pcFileName[300];
		sprintf( pcFileName, "feature%3.3d_1", i );
		feat1.x = piCoord1[0];feat1.y = piCoord1[1];feat1.z = piCoord1[2];feat1.scale=vecFeats1[4*i+3];
		feat1.OutputVisualFeatureInVolume( fio1, pcFileName, 1 );
		feat2.x = piCoord2[0];feat2.y = piCoord2[1];feat2.z = piCoord2[2];feat2.scale=vecFeats2[4*i+3];
		sprintf( pcFileName, "feature%3.3d_2", i );
		feat2.OutputVisualFeatureInVolume( fio2, pcFileName, 1 );
		sprintf( pcFileName, "feature%3.3d_2l", i );
		feat2.OutputVisualFeatureInVolume( fio2like, pcFileName, 1 );

		feat2.x = lvaPeaks.plvz[0].x;feat2.y = lvaPeaks.plvz[0].y;feat2.z = lvaPeaks.plvz[0].z;feat2.scale=vecFeats2[4*i+3];
		sprintf( pcFileName, "feature%3.3d_2_refined", i );
		feat2.OutputVisualFeatureInVolume( fio2, pcFileName, 1 );



		//#define OUT_SIZE 101
		//				fioOut.x = OUT_SIZE;
		//				fioOut.y = OUT_SIZE;
		//				fioOut.z = OUT_SIZE;
		//				fioOut.iFeaturesPerVector = 1;
		//				fioOut.t = 1;
		//				fioAllocate( fioOut );
		//				float rotIn[9] = {1,0,0,0,1,0,0,0,1};
		//				float pfCenter1[3] = {OUT_SIZE/2, OUT_SIZE/2, OUT_SIZE/2};
		//				float pfCenter2 [3];
		//
		//				pfCenter2[0] = featModel.x;
		//				pfCenter2[1] = featModel.y;
		//				pfCenter2[2] = featModel.z;
		//				float fMinValue;
		//				fioFindMin( fio2, fMinValue );
		//				fioSet( fioOut, fMinValue );
		//				similarity_transform_image( fioOut, fio2, pfCenter1, pfCenter2, rotIn, featModel.scale/featInput.scale, fMinValue );
		//				//fioFadeGaussianWindow( fioOut, pfCenter1, 3*featInput.scale );
		//				sprintf( pcFileName, "img_%3.3d_mod%4.4d_img%4.4d_3", i, vecIndex0[i], vecIndex1[i] );
		//				fioWrite( fioOut, pcFileName );
		//
		//				pfCenter2[0] = featInput.x;
		//				pfCenter2[1] = featInput.y;
		//				pfCenter2[2] = featInput.z;
		//				fioFindMin( fio1, fMinValue );
		//				fioSet( fioOut, fMinValue );
		//				similarity_transform_image( fioOut, fio1, pfCenter1, pfCenter2, rotIn, 1, fMinValue );
		//				//fioFadeGaussianWindow( fioOut, pfCenter1, 3*featInput.scale );
		//				sprintf( pcFileName, "img_%3.3d_mod%4.4d_img%4.4d_2", i, vecIndex0[i], vecIndex1[i] );
		//				fioWrite( fioOut, pcFileName );
		//
		//
		//				//pfCenter1[0] = 0;
		//				//pfCenter1[1] = 0;
		//				//pfCenter1[2] = 0;
		//				//pfCenter2[0] = 0;
		//				//pfCenter2[1] = 0;
		//				//pfCenter2[2] = 0;
		//				//similarity_transform_image( fio1, fio1, pfCenter1, pfCenter2, rotIn, 1 );
		//
		//
		//				//pfCenter1[0] = featInput.x;
		//				//pfCenter1[1] = featInput.y;
		//				//pfCenter1[2] = featInput.z;
		//				//pfCenter2[0] = featModel.x;
		//				//pfCenter2[1] = featModel.y;
		//				//pfCenter2[2] = featModel.z;
		//				//fioFadeGaussianWindow( fio1, pfCenter1, 3*featInput.scale );
		//
		//				//sprintf( pcFileName, "img_%3.3d_mod%4.4d_img%4.4d_3", i, vecIndex0[i], vecIndex1[i] );
		//				//fioWrite( fio1, pcFileName );
		//
		//				//pfCenter1[0] = featInput.x;
		//				//pfCenter1[1] = featInput.y;
		//				//pfCenter1[2] = featInput.z;
		//				//pfCenter2[0] = featModel.x;
		//				//pfCenter2[1] = featModel.y;
		//				//pfCenter2[2] = featModel.z;
		//				//similarity_transform_image( fio1, fio2, pfCenter1, pfCenter2, rotIn, featModel.scale/featInput.scale );
		//				//fioFadeGaussianWindow( fio1, pfCenter1, 3*featInput.scale );
		//				//sprintf( pcFileName, "img_%3.3d_mod%4.4d_img%4.4d_2", i, vecIndex0[i], vecIndex1[i] );
		//				//fioWrite( fio1, pcFileName );

	}
	return 1;
}



double
rbfGaussian3D(
			  float *pfV1,
			  float *pfV2,
			  float fVariance,
			  float fRegularizationFactor=1
			)
{
	double fDistSqr = 0;
	for( int i = 0; i < 3; i++ )
	{
		fDistSqr += (pfV1[i]-pfV2[i])*(pfV1[i]-pfV2[i]);
	}
	double fValue = exp( -fDistSqr / (2.0f*fVariance) );

	// Compute square root of determinant of covariance matrix
	//  (independent identically distributed variance in X Y Z)
	double fDenominator = sqrt( fVariance*fVariance*fVariance );

	// Compute PI factor 
	// (2*PI)^(3/2)
#define TWO_PI_EXP_1_5 15.74960994572241
	fDenominator *= TWO_PI_EXP_1_5;

	fValue = fRegularizationFactor*fValue / fDenominator;
	
	return fValue;
}

double
rbfExponential3D(
			  float *pfV1,
			  float *pfV2,
			  float fVariance
			)
{
	double fDistSqr = 0;
	for( int i = 0; i < 3; i++ )
	{
		fDistSqr += (pfV1[i]-pfV2[i])*(pfV1[i]-pfV2[i]);
	}
	//double fDist = fDistSqr <= 0 ? 0 : sqrt( fDistSqr );
	//double fValue = fDist / fVariance;
	double fValue = exp( -fDistSqr / fVariance );

	return fValue;
}

//
// rbfComputeMatrix()
//
// Compute RBF matrix
//
//
// iParam
//	0	:	fixed kernel variance,		no additional regularization
//	1	:	variable kernel variance,	no additional regularization
//	2	:	fixed kernel variance,		additional regularization
//	3	:	variable kernel variance,	additional regularization
//
int
rbfComputeMatrix(
				 std::vector<float> &vecFeats1,
				 std::vector<float> &vecFeats2,
				double **pppdX,
			  int iCols,
			  int iRows,
			  int iParams = 0
				 )
{
	assert( iRows == vecFeats1.size()/4 );
	assert( iCols == vecFeats2.size()/4 );
	assert( vecFeats2.size() == vecFeats1.size() );
	for( int i = 0; i < vecFeats1.size()/4; i++ )
	{
		float *pfV1 = &vecFeats1[4*i+0];
		for( int j = 0; j < vecFeats2.size()/4; j++ )
		{
			float *pfV2 = &vecFeats2[4*j+0];
			//pppdX[i][j] = rbfGaussian3D( pfV1, pfV2, 1000.0f );
			//pppdX[i][j] = rand() / (RAND_MAX+1.0); //rbfGaussian3D( pfV1, pfV2, pfV1[3]*pfV1[3] );
			//	pppdX[i][j] = rbfGaussian3D( pfV1, pfV2, 1000, 20000) ;

			if( iParams & 0x01 )
			{
				pppdX[i][j] = rbfGaussian3D( pfV1, pfV2, 100*pfV2[3]*pfV2[3], 1e10 );
			}
			else
			{
				// Consider uniform variance across points
				pppdX[i][j] = rbfGaussian3D( pfV1, pfV2, 1000, 1e10 );
			}
			
			if( iParams & 0x02 )
			{
				// Add variance term as in Rasmussen 2006
				pppdX[i][j] += pfV1[3]*pfV2[3];
			}
		}
	}
	return 1;
}


int
rbfNormalizeRow(
				double *pdX,
				int iCols
				)
{
	double dSum = 0;
	for( int i = 0; i < iCols; i++ )
	{
		dSum += pdX[i];
	}
	double dNorm = 1.0 / dSum;
	for( int i = 0; i < iCols; i++ )
	{
		pdX[i] *= dNorm;
	}
	dSum = 0;
	for( int i = 0; i < iCols; i++ )
	{
		dSum += pdX[i];
	}
	return 1;
}

int
rbfComputeMatrixRBF_Exact(
				 std::vector<float> &vecFeats1,
				 std::vector<float> &vecFeats2,
				double **pppdX,
			  int iCols,
			  int iRows,
			  int iParams,
			  float fVarianceFactor = 100,
			  float fPostMultiplier =1
				 )
{
	assert( iRows == vecFeats1.size()/4 );
	assert( iCols == vecFeats2.size()/4 );
	//assert( vecFeats2.size() == vecFeats1.size() );
	for( int i = 0; i < vecFeats1.size()/4; i++ )
	{
		float *pfV1 = &vecFeats1[4*i+0];
		for( int j = 0; j < vecFeats2.size()/4; j++ )
		{
			float *pfV2 = &vecFeats2[4*j+0];
			//pppdX[i][j] = rbfGaussian3D( pfV1, pfV2, 1000.0f );
			//pppdX[i][j] = rand() / (RAND_MAX+1.0); //rbfGaussian3D( pfV1, pfV2, pfV1[3]*pfV1[3] );
			//	pppdX[i][j] = rbfGaussian3D( pfV1, pfV2, 1000, 20000) ;

			if( iParams & 0x01 )
			{
				// Note: variance of vector 2 only used - this is used in training & testing/prediction
				// In the case of prediction, scale is junk/invalid. 
				// Might it be interesting to predict scale?
				//pppdX[i][j] = rbfGaussian3D( pfV1, pfV2, fVarianceFactor*pfV2[3]*pfV2[3]/10.0f, fPostMultiplier );
				pppdX[i][j] = fPostMultiplier*rbfGaussian3D( pfV1, pfV2, fVarianceFactor*pfV2[3]*pfV2[3] );
				if( i == j )
					pppdX[i][j] += 0;
			}
			else
			{
				// Consider uniform variance across points
				//pppdX[i][j] = rbfGaussian3D( pfV1, pfV2, fVarianceFactor, fPostMultiplier );
				pppdX[i][j] = fPostMultiplier*rbfGaussian3D( pfV1, pfV2, fVarianceFactor );
				if( i == j )
					pppdX[i][j] += 0;
			}
			//pppdX[i][j] = -log( pppdX[i][j] );
		}
		rbfNormalizeRow( pppdX[i], iCols );
	}
	return 1;
}

//
//
//
//
//
int
rbfComputeResponse(
				 std::vector<float> &vecFeats1,
				 std::vector<float> &vecFeats2,
				double **pppdY,
			  int iCols,
			  int iRows
				 )
{
	assert( iCols == 3 );
	assert( iRows == vecFeats1.size()/4 );
	assert( vecFeats2.size() == vecFeats1.size() );
	for( int i = 0; i < vecFeats1.size()/4; i++ )
	{
		float *pfV1 = &vecFeats1[4*i+0];
		float *pfV2 = &vecFeats2[4*i+0];
		for( int j = 0; j < iCols; j++ )
		{
			pppdY[i][j] = pfV1[j]-pfV2[j];
		}
	}
	return 1;
}

//
// rbfEstimate()
// 
// Inputs are vectors of feature vectors, of length 4.
//
int
rbfEstimate(
			std::vector<float> &vecFeats1,
			std::vector<float> &vecFeats2
			)
{
	int iRowsM_X = vecFeats1.size()/4;
	int iColsN_X = vecFeats1.size()/4;
	double **ppdDataX;
	char pcFormatX[10];

	int iRowsM_Y = vecFeats1.size()/4;
	int iColsN_Y = 3;
	double **ppdDataY;
	double **ppdDataY_predicted;
	char pcFormatY[10];

	//
	// Y = X B
	//

	// Known data - RBF kernel between landmarks G(xi,xj)
	createArray( &ppdDataX, iColsN_X, iRowsM_X );

	// Known response - displacements between landmarks [x,y,z]
	createArray( &ppdDataY, iColsN_Y, iRowsM_Y );

	// Predicted response - displacements between landmarks [x,y,z]
	createArray( &ppdDataY_predicted, iColsN_Y, iRowsM_Y );

	// Unknown coefficients - same dimensions as Y
	double **ppdDataB;
	createArray( &ppdDataB, iColsN_Y, iColsN_X );

	// Pseudoinverse - with inverted dimensions from X
	double **pdDataInv;
	createArray( &pdDataInv, iRowsM_X, iColsN_X );

	rbfComputeResponse( vecFeats1, vecFeats2, ppdDataY, iColsN_Y, iRowsM_Y );

	for( int iParams = 0; iParams < 4; iParams++ )
	{
		// Should be able to reverse feats1/2
		// Ideally, features in image 1 align along the columns of the matrix
		//rbfComputeMatrix( vecFeats1, vecFeats2, ppdDataX, iColsN_X, iRowsM_X, iParams );
		rbfComputeMatrixRBF_Exact( vecFeats2, vecFeats1, ppdDataX, iColsN_X, iRowsM_X, iParams );

		writeArrayText( "stuff", ppdDataX[0], iColsN_X, iRowsM_X, "float" );

		mossPseudoInverse((double*)pdDataInv[0], (double*)ppdDataX[0], iRowsM_X, iColsN_X);

		//{
		//	// Test here to ensure that pseudoinverse works
		//	writeArrayText( "_pseudoinv", pdDataInv[0], iRowsM_X, iColsN_X, "float" );

		//	double **ppdDataXCopy;
		//	createArray( &ppdDataXCopy, iColsN_X, iRowsM_X );

		//	// Multiply original matrix with pseudoinverse - should equal the identity matrix
		//	multiplyArrays( ppdDataXCopy,
		//		pdDataInv, iRowsM_X, iColsN_X,
		//		ppdDataX, iColsN_X, iRowsM_X );

		//	// The product here should be the identity matrix - and it is!!
		//	writeArrayText( "_product_identity", ppdDataXCopy[0], iRowsM_X, iColsN_X, "float" );
		//}

		// Compute coefficients
		multiplyArrays( ppdDataB,
			pdDataInv, iRowsM_X, iColsN_X,
			ppdDataY, iColsN_Y, iRowsM_Y );

		{
			// See how well we can reproduce input data??

			writeArrayText( "array_Y", ppdDataY[0], iColsN_Y, iRowsM_Y, "float" );

			// Multiply original matrix with pseudoinverse - should equal the identity matrix
			multiplyArrays( ppdDataY_predicted
				, ppdDataX, iRowsM_X, iColsN_X
				, ppdDataB, iColsN_Y, iRowsM_Y
				);

			// The product here should be the identity matrix - and it is!!
			writeArrayText( "array_Y_comp", ppdDataY_predicted[0], iColsN_Y, iRowsM_Y, "float" );
		}
	}

	return 1;
}

int
rbfEstimateTest(
			std::vector<float> &vecFeats1,	// Original: 4 features per vector [x y z scale]
			std::vector<float> &vecFeats2,
			std::vector<float> &vecFeatsGT1, // Ground truth: 4 features per vector [x y z junk]
			std::vector<float> &vecFeatsGT2 // Ground truth
			)
{
	int iRowsM_X = vecFeats1.size()/4;
	int iColsN_X = vecFeats1.size()/4;
	double **ppdDataX;
	char pcFormatX[10];

	int iRowsM_Y = vecFeats1.size()/4;
	int iColsN_Y = 3;
	double **ppdDataY;
	double **ppdDataY_predicted;
	char pcFormatY[10];

	//
	// Y = X B
	//

	printf( "Compute first response feats1 %d feats2 %d\n", vecFeats1.size(), vecFeats2.size() );

	// Known data - RBF kernel between landmarks G(xi,xj)
	createArray( &ppdDataX, iColsN_X, iRowsM_X );

	// Known and predicted responses - displacements between landmarks [x,y,z]

	createArray( &ppdDataY, iColsN_Y, iRowsM_Y );
	rbfComputeResponse( vecFeats1, vecFeats2, ppdDataY, iColsN_Y, iRowsM_Y );
	createArray( &ppdDataY_predicted, iColsN_Y, iRowsM_Y );

	// Unknown coefficients - same dimensions as Y
	double **ppdDataB;
	createArray( &ppdDataB, iColsN_Y, iColsN_X );

	// Pseudoinverse - with inverted dimensions from X
	double **pdDataInv;
	createArray( &pdDataInv, iRowsM_X, iColsN_X );


	//
	// Generate test data matrices
	//

	int iRowsM_Ytest = vecFeatsGT1.size()/4;
	int iColsN_Ytest = 3;
	double **ppdDataYtest;
	double **ppdDataYtest_predicted;

	// Testing response: ground truth vs predicted
	createArray( &ppdDataYtest, iColsN_Ytest, iRowsM_Ytest );
	rbfComputeResponse( vecFeatsGT1, vecFeatsGT2, ppdDataYtest, iColsN_Ytest, iRowsM_Ytest );
	createArray( &ppdDataYtest_predicted, iColsN_Ytest, iRowsM_Ytest );

	//
	// Mixing matrix	
	//
	// Rows:	the dimension of testing data
	// Columns:	the dimension of training data
	//
	int iRowsM_Xtest = vecFeatsGT1.size()/4;
	int iColsN_Xtest = vecFeats1.size()/4;
	double **ppdDataXtest;
	createArray( &ppdDataXtest, iColsN_Xtest, iRowsM_Xtest );

	float fVarianceFactor = 100;
	float fPostMultiplier =1;

	for( int iParams = 0; iParams < 1; iParams++ )
	{
		//
		// Training: compute unknown weights matrix B
		//

		// Compute training matrix - put Feats1 in columns place
		rbfComputeMatrixRBF_Exact( vecFeats1, vecFeats1, ppdDataX, iColsN_X, iRowsM_X, iParams,fVarianceFactor,fPostMultiplier );
		mossPseudoInverse((double*)pdDataInv[0], (double*)ppdDataX[0], iRowsM_X, iColsN_X);

		writeArrayText( "X_train", ppdDataX[0], iColsN_X, iRowsM_X, "float" );

		// Compute coefficients from training data
		multiplyArrays( ppdDataB,
			pdDataInv, iRowsM_X, iColsN_X, ppdDataY, iColsN_Y, iRowsM_Y );

		//
		// Testing 1: evaluate on training data, should be exact
		//

		// See how well we can reproduce input data??

		writeArrayText( "ppdDataY", ppdDataY[0], iColsN_Y, iRowsM_Y, "float" );

		// Need to create a new matrix/input vector with new data

		multiplyArrays( ppdDataY_predicted
				, ppdDataX, iRowsM_X, iColsN_X
				, ppdDataB, iColsN_Y, iRowsM_Y
				);

		// Predicted values should match input ... 
		writeArrayText( "ppdDataY_predicted", ppdDataY_predicted[0], iColsN_Y, iRowsM_Y, "float" );

		//
		// Testing 2: evaluate on testing data, should be close
		//

		rbfComputeMatrixRBF_Exact( vecFeatsGT1, vecFeats1, ppdDataXtest, iColsN_Xtest, iRowsM_Xtest, iParams,fVarianceFactor,fPostMultiplier );		

		writeArrayText( "X_test", ppdDataXtest[0], iColsN_Xtest, iRowsM_Xtest, "float" );

		writeArrayText( "ppdDataYtest", ppdDataYtest[0], iColsN_Ytest, iRowsM_Ytest, "float" );

		multiplyArrays( ppdDataYtest_predicted
				, ppdDataXtest, iColsN_Xtest, iRowsM_Xtest
				, ppdDataB, iColsN_Y, iRowsM_Y
				);
		writeArrayText( "ppdDataYtest_predicted", ppdDataYtest_predicted[0], iColsN_Ytest, iRowsM_Ytest, "float" );

		
		//differenceArrays( 
	}

	return 1;
}


vector<float>
rbfAverage4(
			vector<float> &feats
			)
{
	vector<float> vecAverage;
	vecAverage.resize(4,0);
	for( int i = 0; i < feats.size()/4; i++ )
	{
		for( int j = 0; j < 4; j++ )
		{
			vecAverage[j] += feats[4*i+j];
		}
	}
	float fSize = 1.0/(feats.size()/4.0);
	for( int j = 0; j < 4; j++ )
	{
		vecAverage[j] *= fSize;
	}
	return vecAverage;
}

int
rbfSubtractVector4(	   
			vector<float> &feats,
			vector<float> &vec
			)
{
	for( int i = 0; i < feats.size()/4; i++ )
	{
		for( int j = 0; j < 4; j++ )
		{
			feats[4*i+j] -= vec[j];
		}
	}
	return 1;
}

int
rbfAddVector4(	   
			vector<float> &feats,
			vector<float> &vec
			)
{
	for( int i = 0; i < feats.size()/4; i++ )
	{
		for( int j = 0; j < 4; j++ )
		{
			feats[4*i+j] += vec[j];
		}
	}
	return 1;
}

double
rbfEuclideanDist(
			  float *pfV1,
			  float *pfV2
			)
{
	double fDistSqr = 0;
	for( int i = 0; i < 3; i++ )
	{
		fDistSqr += (pfV1[i]-pfV2[i])*(pfV1[i]-pfV2[i]);
	}
	if( fDistSqr > 0 )
		return sqrt( fDistSqr );
	else
		return 0;
}


//
// rbfEstimate2()
// 
// Inputs are vectors of feature vectors, of length 4.
//
// Use kernel regression for deformable interpolation. Interpolate a single point.
//
int
rbfEstimatePoint(
			std::vector<float> &vecFeats1,
			std::vector<float> &vecFeats2,
			std::vector<float> &vX1,
			std::vector<float> &vX2,
			std::vector<double> *vecWeights = 0
			)
{
	int iFeatureCount = vecFeats1.size()/4;

	vX2[0] = 0;
	vX2[1] = 0;
	vX2[2] = 0;
	vX2[3] = 0;

	// Normalization sum
	float fNormSum = 0;

	// Compute displacement vectors for kernels
	std::vector<float> vecFeatDiff;
	vecFeatDiff.resize( vecFeats1.size() );
	for( int i = 0; i < vecFeatDiff.size(); i++ )
	{
		vecFeatDiff[i] = vecFeats2[i]-vecFeats1[i];
	}

	// Compute normalization factors for kernels
	std::vector<float> vecFeatNorms;
	vecFeatNorms.resize( vecFeats1.size()/4 );
	for( int i = 0; i < vecFeatNorms.size(); i++ )
	{
		// Keep track of 3D standard deviation factor
		// associated with kernel
		float fStdev = vecFeats1[4*i+3];
		float fFactor = fStdev*fStdev*fStdev;
		vecFeatNorms[i] = 1.0f/fFactor;
	}

	// Find nearest neighbor
	int iClosestIndex = -1;
	float fClosestDist = 0;
	for( int i = 0; i < iFeatureCount; i++ )
	{
		float fDist = rbfEuclideanDist(&(vX1[0]), &(vecFeats1[4*i]));
		if( iClosestIndex < 0 || fDist < fClosestDist )
		{
			iClosestIndex = i;
			fClosestDist = fDist;
		}
	}
	

	for( int i = 0; i < iFeatureCount; i++ )
	{
		float fStdev = vecFeats1[4*i+3];
		double fWeight = rbfExponential3D( &(vX1[0]), &(vecFeats1[4*i]), 2*fClosestDist*fStdev );
		fWeight *= vecFeatNorms[i];

		if( vecWeights )
		{
			(*vecWeights)[i] = fWeight;
		}

		// Compute weighted sum
		for( int j = 0; j < 4; j++ )
		{
			vX2[j] += vecFeatDiff[4*i+j]*fWeight;
		}
		fNormSum += fWeight;
	}

	// Compute weighted sum
	for( int j = 0; j < 4; j++ )
	{
		vX2[j] /= fNormSum;
	}

	return 1;
}

int
rbfEstimateVolume(
			std::vector<float> &vecFeats1,
			std::vector<float> &vecFeats2,
			std::vector<float> &vecPoint1,
			std::vector<float> &vecPointOut
			)
{
	int iRowsM_X = vecFeats1.size()/4;
	int iColsN_X = vecFeats1.size()/4;

	vecPointOut[0] = 0;
	vecPointOut[1] = 0;
	vecPointOut[2] = 0;

	for( int iFeat = 0; iFeat < iRowsM_X; iFeat++ )
	{
	}

	return 1;
}





//
// testRBFPoint()
//
// Do leave-one-out testing and estimation for all points.
//	predict/estimate validity of matches.
//
//
int
testRBFPoints(
			 std::vector<float> &vecFeats1,
			 std::vector<float> &vecFeats2
			 )
{

	FILE *outfile = fopen( "rbf_test.txt", "wt" );

	for( int i = 0; i < vecFeats1.size()/4; i++ )
	{
		std::vector<float> vecFeats1_;
		std::vector<float> vecFeats2_;
		std::vector<float> vTest1;
		std::vector<float> vTest2;
		std::vector<float> vD12;

		for( int j = 0; j < vecFeats1.size()/4; j++ )
		{
			for( int k = 0; k < 4; k++ )
			{
				if( j != i )
				{
					// Save to kernel estimation lists
					vecFeats1_.push_back( vecFeats1[4*j+k] );
					vecFeats2_.push_back( vecFeats2[4*j+k] );
				}
				else
				{
					// Save test point
					vTest1.push_back( vecFeats1[4*j+k] );
					vTest2.push_back( vecFeats2[4*j+k] );
					vD12.push_back( vecFeats2[4*j+k] );
				}
			}
		}

		// Save weight vector just for visualization
		std::vector<double> vecWeights;
		vecWeights.resize( vecFeats1_.size()/4 );
		rbfEstimatePoint(vecFeats1_, vecFeats2_, vTest1, vD12, &vecWeights );

		std::vector<float> vTest2_check;
		for( int k = 0; k < 4; k++ )
		{
			vTest2_check.push_back( vTest1[k]+vD12[k] );
			fprintf( outfile, "%f\t", vTest2_check[k] );
		}
		for( int k = 0; k < 4; k++ )
		{
			fprintf( outfile, "%f\t", vTest2[k] );
		}
		fprintf( outfile, "|\t" );

		// Print out weight vector
		for( int j = 0; j < vecFeats1.size()/4; j++ )
		{
			if( j < i )
			{
				fprintf( outfile, "%f\t", vecWeights[j] );
			}
			else if( j > i )
			{
				fprintf( outfile, "%f\t", vecWeights[j-1] );
			}
			else
			{
				fprintf( outfile, "%f\t", 0 );
			}
		}
		fprintf( outfile, "\n" );

	}
	fclose( outfile );
	return 1;
}









//
// C:\downloads\data\Gurman\debugging_2014\features\01-010-009-JGC_TLC_YC.key.matches.img1.txt C:\downloads\data\Gurman\debugging_2014\features\01-010-009-JGC_TLC_YC.key.matches.img2.txt transform.txt
// C:\downloads\data\Gurman\debugging_2014\features\01-007-008-JDP_TLC_D.key.matches.img1.txt C:\downloads\data\Gurman\debugging_2014\features\01-007-008-JDP_TLC_D.key.matches.img2.txt
//

int
main(
	 int argc,
	 char **argv
	 )
{
	if( argc != 4  ) //&& argc != 5 && argc != 7 )
	{
		printf( "featMatchRefine - estimate deformation from correspondences.\n" );
		printf( "Usage 1: featMatchRefine <3D points1> <3D points2> <3D  points test in> <image2> \n" );
		printf( "featMatchRefine - refines local feature correspondences.\n" );
		printf( "Usage 2: featMatchRefine <3D points1> <3D points2> <image1> <image2> \n" );
		printf( "	<3D points1> <3D points2>: lists of 4 coordinates per line (x y z scale), tab separated, preceded by a tab\n" );
		printf( "	<image 1> <image 2>: names of image 1 and image 2, nifti file format\n" );
		printf( "	Output: file with refined locations <3D points2>.refined.txt.\n" );
		return -1;
	}

	vector< float > feats1, feats2, feats3;

	int iFeats1 = readPointFile4( argv[1], feats1 );
	int iFeats2 = readPointFile4( argv[2], feats2 );
	int iFeats3 = readPointFile4( argv[3], feats3 );
	if( iFeats1 != iFeats2 )
	{
		printf( "Error: uneven number of training features: %d %d", iFeats1, iFeats2 );
		return -1;
	}

	if( argc == 4 )
	{
		printf( "testing exact estimate\n" );
		rbfEstimateTest( feats1, feats2, feats3, feats3 );
		return 1;
	}

	//
	// Correlation-based refinement
	//refineMatches( feats1, feats2, argv[3], argv[4] );
	//
	outputMatches(feats1, feats2, argv[3], argv[4] );

	//
	// Radial basis function estimate
	//rbfEstimate(feats1, feats2);

	//
	// Test
	//
	vector<float> vec1;
	vector<float> vec2;
	for( int i = 0; i < 4; i++ )
	{
		vec1.push_back( feats1[i] );
		vec2.push_back( feats2[i] );
	}
	rbfEstimatePoint(feats1, feats2, vec1, vec2);

	// Test collection of points.
	testRBFPoints(feats1, feats2);

	vector< float > featsGT1, featsGT2;
	int iFeatsGroundTruth1 = readPointFile3( argv[5], featsGT1 );
	int iFeatsGroundTruth2 = readPointFile3( argv[6], featsGT2 );
	if( iFeatsGroundTruth1 != iFeatsGroundTruth2 )
	{
		printf( "Error: uneven number of testing features: %d %d", iFeatsGroundTruth1, iFeatsGroundTruth2 );
		return -1;
	}

	//vector<float> vMean1 = rbfAverage4( feats1 );
	//vector<float> vMean2 = rbfAverage4( feats2 );
	//vector<float> vMeanGT1 = rbfAverage4( featsGT1 );
	//vector<float> vMeanGT2 = rbfAverage4( featsGT2 );

	//rbfSubtractVector4( feats1, vMean1 );
	//rbfSubtractVector4( feats2, vMean2 );
	//rbfSubtractVector4( featsGT1, vMean2 );
	//rbfSubtractVector4( featsGT2, vMean2 );

	//
	//vector<float> vMean11 = rbfAverage4( feats1 );
	//vector<float> vMean21 = rbfAverage4( feats2 );
	//vector<float> vMeanGT11 = rbfAverage4( featsGT1 );
	//vector<float> vMeanGT21 = rbfAverage4( featsGT2 );

	rbfEstimateTest(feats1, feats2, featsGT1, featsGT2);

	char pcFileName[400];
	sprintf( pcFileName, "%s.refined.txt", argv[4] );

	writePointFile4( pcFileName, feats2, argv[2] );

	return 0;

}
