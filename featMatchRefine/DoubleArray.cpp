
#include "DoubleArray.h"

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <math.h>

//
// readArray()
//
// Read in an array.
//
int
readArray(
		  char *pcFileNameBase,
		  double ***pppdX,
		  int &iCols,
		  int &iRows,
		  char *pcFormat
		  )
{
	FILE *infile;
	char pcFileName[300];
	int iReturn;
	
	*pppdX = 0;

	// Open header
	sprintf( pcFileName, "%s.txt", pcFileNameBase );
	infile = fopen( pcFileName, "rt" );
	if( !infile )
	{
		return -1;
	}
	iReturn = fscanf( infile, "cols:\t%d\n", &iCols );
	iReturn = fscanf( infile, "rows:\t%d\n", &iRows );
	iReturn = fscanf( infile, "format:\t%s\n", pcFormat );
	fclose( infile );

	// Open data
	sprintf( pcFileName, "%s.bin", pcFileNameBase );
	infile = fopen( pcFileName, "rb" );
	if( !infile )
	{
		return -1;
	}
	double *pdDataX = new double[iRows*iCols];
	if( !pdDataX )
	{
		fclose( infile );
		return -1;
	}
	if( pcFormat[0] == 'i' )
	{
		for( int i = 0; i < iRows*iCols; i++ )
		{
			int iData;
			fread( &iData, sizeof(int), 1, infile );
			pdDataX[i] = (double)iData;
		}
	}
	else if( pcFormat[0] == 'f' )
	{
		for( int i = 0; i < iRows*iCols; i++ )
		{
			double dData;
			fread( &dData, sizeof(double), 1, infile );
			pdDataX[i] = (double)dData;
		}
	}
	fclose( infile );

	double **pdX = new double*[iRows];
	for( int i = 0; i < iRows; i++ )
	{
		pdX[i] = &pdDataX[i*iCols];
	}

	*pppdX = pdX;

	return 0;
}

int
writeArray(
		  char *pcFileNameBase,
		  double *ppdX,
		  int iCols,
		  int iRows,
		  char *pcFormat
		  )
{
	FILE *infile;
	char pcFileName[300];
	int iReturn;

	// Open header
	sprintf( pcFileName, "%s.txt", pcFileNameBase );
	infile = fopen( pcFileName, "wt" );
	if( !infile )
	{
		return -1;
	}
	iReturn = fprintf( infile, "cols:\t%d\n", iCols );
	iReturn = fprintf( infile, "rows:\t%d\n", iRows );
	iReturn = fprintf( infile, "format:\t%s\n", pcFormat );
	fclose( infile );

	// Open data
	sprintf( pcFileName, "%s.bin", pcFileNameBase );
	infile = fopen( pcFileName, "wb" );
	if( !infile )
	{
		return -1;
	}

	if( pcFormat[0] == 'i' )
	{
		for( int i = 0; i < iRows*iCols; i++ )
		{
			int iData = ppdX[i];
			fwrite( &iData, sizeof(int), 1, infile );
		}
	}
	else if( pcFormat[0] == 'f' )
	{
		for( int i = 0; i < iRows*iCols; i++ )
		{
			double dData = ppdX[i];
			fwrite( &dData, sizeof(double), 1, infile );
		}
	}
	fclose( infile );

	return 0;
}

int
writeArrayText(
		  char *pcFileNameBase,
		  double *ppdX,
		  int iCols,
		  int iRows,
		  char *pcFormat
		  )
{
	FILE *infile;
	char pcFileName[300];
	int iReturn;

	// Open header
	sprintf( pcFileName, "%s.txt", pcFileNameBase );
	infile = fopen( pcFileName, "wt" );
	if( !infile )
	{
		return -1;
	}
	iReturn = fprintf( infile, "cols:\t%d\n", iCols );
	iReturn = fprintf( infile, "rows:\t%d\n", iRows );
	iReturn = fprintf( infile, "format:\t%s\n", pcFormat );

	for( int i = 0; i < iRows; i++ )
	{
		for( int j = 0; j < iCols; j++ )
		{
			double dData = ppdX[i*iCols+j];
			fprintf( infile, "%f\t", dData );
		}
		fprintf( infile, "\n" );
	}
	fclose( infile );

	return 0;
}


int
createArray(
		  double ***pppdX,
		  int iCols,
		  int iRows
		  )
{
	*pppdX = 0;

	double *pdDataX = new double[iRows*iCols];
	if( !pdDataX )
	{
		return -1;
	}

	double **pdX = new double*[iRows];
	for( int i = 0; i < iRows; i++ )
	{
		pdX[i] = &pdDataX[i*iCols];
	}

	*pppdX = pdX;

	return 1;

}

int
setArrayIdentity(
		  double **ppdX,
		  int iCols,
		  int iRows
		  )
{
	for( int i = 0; i < iRows*iCols; i++ )
	{
		ppdX[0][i] = 0;
	}

	for( int i = 0; i < iRows && i < iCols; i++ )
	{
		ppdX[i][i] = 1;
	}

	return 1;

}

int
deleteArray(
		  double **ppdX
		  )
{
	if( ppdX )
	{
		delete [] ppdX[0];
		delete [] ppdX;
	}
	return 1;
}

//
// concatArrays()
//
// Merge two arrays. Must have same number of rows. Columns are concatenated.
// Result stored in first array.
//
int
concatArrays(
		  double ***pppdX,
		  int &iCols,
		  int &iRows,

		  double **pppdX1,
		  int iCols1,
		  int iRows1,
		  
		  double **pppdX2,
		  int iCols2,
		  int iRows2
		  )
{
	if( iRows2 != iRows1 )
	{
		return -1;
	}

	if( !pppdX1 || !pppdX2 )
	{
		return -2;
	}

	iCols = iCols1 + iCols2;
	iRows = iRows1;

	if( createArray( pppdX, iCols, iRows ) < 0 )
	{
		return -3;
	}

	// Copy data
	for( int i = 0; i < iRows; i++ )
	{
		double *pdRow = (*pppdX)[i];
		for( int j = 0; j < iCols1; j++ )
		{
			pdRow[j] = pppdX1[i][j];
		}
		for( int j = 0; j < iCols2; j++ )
		{
			pdRow[iCols1 + j] = pppdX2[i][j];
		}
	}


	return 1;
}

//
// X = X1 + X2
//
int
addArrays(
		  double **pppdX,
		  // X is size iRows1, iCols2

		  double **pppdX1,
		  int iCols1,
		  int iRows1,
		  
		  double **pppdX2,
		  int iCols2,
		  int iRows2
		  )
{	
	assert( iCols1 == iCols2 ); // Dimensions must match
	assert( iRows1 == iRows2 ); // Dimensions must match

	// Compute coefficients
	for( int i = 0; i < iRows1; i++ )
	{
		for( int j = 0; j < iCols2; j++ )
		{
			pppdX[i][j] = pppdX1[i][j] + pppdX2[i][j];
		}
	}
	return 1;
}


//
// X = X1*X2
//
int
multiplyArrays(
		  double **pppdX,
		  // X is size iRows1, iCols2

		  double **pppdX1,
		  int iCols1,
		  int iRows1,
		  
		  double **pppdX2,
		  int iCols2,
		  int iRows2
		  )
{	
	assert( iCols1 == iRows2 ); // This is the common dimension

	// Compute coefficients
	for( int i = 0; i < iRows1; i++ )
	{
		for( int j = 0; j < iCols2; j++ )
		{
			pppdX[i][j] = 0;
			for( int k = 0; k < iCols1; k++ )
			{
				pppdX[i][j] += pppdX1[i][k]*pppdX2[k][j];
			}
		}
	}
	return 1;
}
