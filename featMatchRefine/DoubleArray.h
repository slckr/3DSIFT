

#ifndef __DOUBLEARRAY_H__
#define __DOUBLEARRAY_H__

//
// readArray()
//
// Read in an array.
// Contains a header file and data.
// Header file is a text file with the following 3 line format.
// Note 3d line can be "format:	float" or "format:	int"
//
//	cols:	512
//	rows:	34456
//	format:	int
//
int
readArray(
		  char *pcFileNameBase,
		  double ***pppdX,
		  int &iCols,
		  int &iRows,
		  char *pcFormat
		  );

int
writeArray(
		  char *pcFileNameBase,
		  double *ppdX,
		  int iCols,
		  int iRows,
		  char *pcFormat
		  );

int
writeArrayText(
		  char *pcFileNameBase,
		  double *ppdX,
		  int iCols,
		  int iRows,
		  char *pcFormat
		  );

int
createArray(
		  double ***pppdX,
		  int iCols,
		  int iRows
		  );

int
deleteArray(
		  double **ppdX
		  );

//
// concatArrays()
//
// Merge two arrays. Must have same number of rows. Columns are concatenated.
// Result stored in first array.
//
int
concatArrays(
		  double ***pppdX,
		  int &iCols,
		  int &iRows,

		  double **pppdX1,
		  int iCols1,
		  int iRows1,
		  
		  double **pppdX2,
		  int iCols2,
		  int iRows2
		  );

//
// X = X1*X2
//
int
multiplyArrays(
		  double **pppdX,
		  // X is size iRows1, iCols2

		  double **pppdX1,
		  int iCols1,
		  int iRows1,
		  
		  double **pppdX2,
		  int iCols2,
		  int iRows2
		  );

#endif
