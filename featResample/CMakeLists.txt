cmake_minimum_required(VERSION 3.0)

project(featResample)

include_directories(${NIFTI_INCLUDE_DIR} ${FLANN_INCLUDE_DIR})
link_directories(${NIFTI_LIBRARY_DIR} ${ZLIB_LIBRARY_DIR})

add_executable(${PROJECT_NAME} featResample.cpp)
target_link_libraries(${PROJECT_NAME} src_common feat_common niftiio znz zlib)
install(TARGETS featResample RUNTIME DESTINATION ${CMAKE_INSTALL_PREFIX})
