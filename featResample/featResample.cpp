

#include "featMatchUtilities.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "src_common.h"
#include "nifti1_io.h"



template <class NewTYPE, class DTYPE>
void reg_changeDatatype1(nifti_image *image)
{
	// the initial array is saved and freeed
	DTYPE *initialValue = (DTYPE *)malloc(image->nvox*sizeof(DTYPE));
	memcpy(initialValue, image->data, image->nvox*sizeof(DTYPE));
	
	// the new array is allocated and then filled
	if(sizeof(NewTYPE)==sizeof(unsigned char)) image->datatype = NIFTI_TYPE_UINT8;
	else if(sizeof(NewTYPE)==sizeof(float)) image->datatype = NIFTI_TYPE_FLOAT32;
	else if(sizeof(NewTYPE)==sizeof(double)) image->datatype = NIFTI_TYPE_FLOAT64;
	else{
		printf("err\treg_changeDatatype\tOnly change to unsigned char, float or double are supported\n");
		free(initialValue);
		return;
	}
	free(image->data);
	image->nbyper = sizeof(NewTYPE);
	image->data = (void *)calloc(image->nvox,sizeof(NewTYPE));
	NewTYPE *dataPtr = static_cast<NewTYPE *>(image->data);
	for(unsigned int i=0; i<image->nvox; i++)
		dataPtr[i] = (NewTYPE)(initialValue[i]);

	free(initialValue);
	return;
}

template <class NewTYPE>
void reg_changeDatatype(nifti_image *image)
{
	switch(image->datatype){
		case NIFTI_TYPE_UINT8:
			reg_changeDatatype1<NewTYPE,unsigned char>(image);
			break;
		case NIFTI_TYPE_INT8:
			reg_changeDatatype1<NewTYPE,char>(image);
			break;
		case NIFTI_TYPE_UINT16:
			reg_changeDatatype1<NewTYPE,unsigned short>(image);
			break;
		case NIFTI_TYPE_INT16:
			reg_changeDatatype1<NewTYPE,short>(image);
			break;
		case NIFTI_TYPE_UINT32:
			reg_changeDatatype1<NewTYPE,unsigned int>(image);
			break;
		case NIFTI_TYPE_INT32:
			reg_changeDatatype1<NewTYPE,int>(image);
			break;
		case NIFTI_TYPE_FLOAT32:
			reg_changeDatatype1<NewTYPE,float>(image);
			break;
		case NIFTI_TYPE_FLOAT64:
			reg_changeDatatype1<NewTYPE,double>(image);
			break;
		default:
			printf("err\treg_changeDatatype\tThe initial image data type is not supported\n");
			return;
	}
}

int
fioReadNifti(
			 FEATUREIO &fioIn,
			 char *pcFileName,
			 nifti_image **pnHeader
			 )
{
	/* Read the target and source images */
	nifti_image *targetHeader = nifti_image_read(pcFileName,true);
	if(targetHeader == NULL )
	{
		return -1;
	}

	if( targetHeader->data == NULL )
	{
		return -2;
	}

	//nifti_image_write( targetHeader );

	//char *pcf = strstr( targetHeader->fname, ".nii" );
	//sprintf( pcf-1, "f.hdr" );
	//char *pci = strstr( targetHeader->iname, ".nii" );
	//sprintf( pci-1, "f.img" );

	//targetHeader->nifti_type = 2;
	//nifti_image_write( targetHeader );

	reg_changeDatatype<float>(targetHeader);

	//sprintf( pcf-1, "g.hdr" );
	//sprintf( pci-1, "g.img" );

	//nifti_image_write( targetHeader );

	memset( &fioIn, 0, sizeof(FEATUREIO) );

	fioIn.x = targetHeader->nx;
	fioIn.y = targetHeader->ny;
	fioIn.z = targetHeader->nz;
	fioIn.t = targetHeader->nt;
	fioIn.iFeaturesPerVector = 1;
	fioIn.pfVectors = (float*)targetHeader->data;

	*pnHeader = targetHeader;

	return 1;
}


//
// fioReadNifti()
//
// Read nifty, optionally convert to isotropic.
//
int
fioReadNifti(
			 FEATUREIO &fioIn,
			 char *pcFileName,
			 int bIsotropic = 0,
			 int iRescale = 0,
			nifti_image **pnHeader = 0
			 )
{
	/* Read the target and source images */
	nifti_image *targetHeader = nifti_image_read(pcFileName,true);
	if(targetHeader == NULL )
	{
		return -1;
	}

	float adfa;
	int check = sizeof(adfa);

	if( targetHeader->data == NULL )
	{
		return -2;
	}

	reg_changeDatatype<float>(targetHeader);

	FEATUREIO fioTmp;
	memset( &fioTmp, 0, sizeof(FEATUREIO) );
	memset( &fioIn, 0, sizeof(FEATUREIO) );

	fioTmp.x = targetHeader->nx;
	fioTmp.y = targetHeader->ny;
	fioTmp.z = targetHeader->nz;
	fioTmp.t = 1;//targetHeader->nt;
	fioTmp.iFeaturesPerVector = 1;
	fioTmp.pfVectors = (float*)targetHeader->data;

	if( bIsotropic &&
		(targetHeader->dx != targetHeader->dy
			|| targetHeader->dy != targetHeader->dz
				|| targetHeader->dx != targetHeader->dz)
				)
	{
		float fMinSize = targetHeader->dx;
		if( targetHeader->dy < fMinSize )
			fMinSize = targetHeader->dy;
		if( targetHeader->dz < fMinSize )
			fMinSize = targetHeader->dz;

		fioIn.x = targetHeader->nx*targetHeader->dx / fMinSize;
		fioIn.y = targetHeader->ny*targetHeader->dy / fMinSize;
		fioIn.z = targetHeader->nz*targetHeader->dz / fMinSize;
		fioIn.t = 1;
		fioIn.iFeaturesPerVector = 1;
		if( !fioAllocate( fioIn ) )
		{
			// Not enough memory to resample :(
			return -3;
		}

		for( int z = 0; z < fioIn.z; z++ )
		{
			for( int y = 0; y < fioIn.y; y++ )
			{
				for( int x = 0; x < fioIn.x; x++ )
				{
					float fPixel = fioGetPixelTrilinearInterp( fioTmp,
							(x*fMinSize+0.5)/targetHeader->dx,
							(y*fMinSize+0.5)/targetHeader->dy,
							(z*fMinSize+0.5)/targetHeader->dz
							);
					float *pfDestPix = fioGetVector( fioIn, x, y, z );
					*pfDestPix = fPixel;
				}
			}
		}
		fioIn.x = targetHeader->nx*targetHeader->dx / fMinSize;
		fioIn.y = targetHeader->ny*targetHeader->dy / fMinSize;
		fioIn.z = targetHeader->nz*targetHeader->dz / fMinSize;
		fioIn.t = 1;

		targetHeader->dim[1] = targetHeader->nx = fioIn.x;
		targetHeader->dim[2] = targetHeader->ny = fioIn.y;
		targetHeader->dim[3] = targetHeader->nz = fioIn.z;
		targetHeader->data = fioGetVector( fioIn, 0,0,0 );
		targetHeader->pixdim[1] = targetHeader->dx = fMinSize;
		targetHeader->pixdim[2] = targetHeader->dy = fMinSize;
		targetHeader->pixdim[3] = targetHeader->dz = fMinSize;
		targetHeader->nvox = fioIn.x*fioIn.y*fioIn.z;

		//fioWrite( fioIn, "out-iso"  );

		//nifti_image_free( targetHeader );
	}
	else
	{
		fioIn = fioTmp;
	}

	*pnHeader = targetHeader;

	return 1;
}


int
fioWriteNifti(
			 FEATUREIO &fioIn,
			 nifti_image *targetHeader,
			 char *pcFileName
			 )
{
	char pcFileNameImg[400];
	char pcFileNameHdr[400];

	targetHeader->iname = pcFileNameImg;
	targetHeader->fname = pcFileNameHdr;
	sprintf( pcFileNameImg, "%s", pcFileName );
	sprintf( pcFileNameHdr, "%s", pcFileName );
	char *pch = strstr( pcFileNameImg, ".img" );
	if( pch )
	{
		pch = strstr( pcFileNameHdr, ".img" );
		pch[1] = 'h';pch[2] = 'd';pch[3] = 'r';
		//targetHeader->nifti_type = 2;
	}
	else if( pch = strstr( pcFileNameImg, ".hdr" ) )
	{
		pch = strstr( pcFileNameImg, ".hdr" );
		pch[1] = 'i';pch[2] = 'm';pch[3] = 'g';
		//targetHeader->nifti_type = 2;
	}
	else if( pch = strstr( pcFileNameImg, ".nii" ) )
	{
		targetHeader->iname = 0;
		//targetHeader->nifti_type = 1;
	}
	
	targetHeader->nifti_type = 0;

	nifti_image_write( targetHeader );
	//nifti_image_write_hdr_img( targetHeader, 1, "wb" );

	return 0;
}

//
//
// featCrop()
//
// Crop an image to a smaller volume. Centers image within smaller volume.
// Arg1: Name of destination image, used for sizing
// Arg2: Input image name
// Arg3: Ouput image name
//
// C:\downloads\data\RIRE\back\test_patient_002\mr_T1\simage-small.hdr C:\downloads\data\RIRE\back\test_patient_002\mr_T1\simage.hdr C:\downloads\data\RIRE\back\test_patient_002\mr_T1\simage-mt-out.hdr
//
int
featCrop(
	 int argc,
	 char **argv
		 )
{
	nifti_image *targetHeader;
	nifti_image *sourceHeader;
	FEATUREIO fioTarget;
	FEATUREIO fioSource;
	int iArg = 1;
	if( fioReadNifti( fioTarget, argv[iArg], &targetHeader ) < 0 )
	{
			printf( "Error: could not read input file: %s\n", argv[iArg] );
			return -1;
	}
	iArg++;

	if( fioReadNifti( fioSource, argv[iArg], &sourceHeader ) < 0 )
	{
			printf( "Error: could not read input file: %s\n", argv[iArg] );
			return -1;
	}
	iArg++;

	TransformSimilarity ts;
	ts.m_pfTrans[0] = -(fioTarget.x - fioSource.x)/2.0f;
	ts.m_pfTrans[1] = -(fioTarget.y - fioSource.y)/2.0f;
	ts.m_pfTrans[2] = -(fioTarget.z - fioSource.z)/2.0f;
	
	float pfZero[3] = {0,0,0};
	//similarity_transform_invert( pfZero, ts.m_pfTrans, ts.m_ppfRot[0], ts.m_fScale );
	similarity_transform_image( fioTarget, fioSource, pfZero, ts.m_pfTrans, ts.m_ppfRot[0], ts.m_fScale );

	fioWriteNifti( fioTarget, targetHeader, argv[iArg] );
}

//
// featResampleIsotropic()
//
// Resample and output image as isotropic resolution.
// Two arguments: <input file name> <output file name>
//
int
featResampleIsotropic(
	 int argc,
	 char **argv
		 )
{
	nifti_image *targetHeader;
	nifti_image *sourceHeader;
	FEATUREIO fioTarget;
	FEATUREIO fioSource;
	int bIsotropicProcessing = 1;
	int iArg = 1;
	if( fioReadNifti( fioTarget, argv[iArg], bIsotropicProcessing, 0, &targetHeader ) < 0 )
	{
			printf( "Error: could not read input file: %s\n", argv[iArg] );
			return -1;
	}
	iArg++;

	fioWriteNifti( fioTarget, targetHeader, argv[iArg] );
	return 1;
}

//
// featSubsample()
//
// Resample and output image as isotropic resolution.
// Two arguments: <input file name> <output file name>
//
int
featSubsample(
	 int argc,
	 char **argv
		 )
{
	nifti_image *targetHeader;
	nifti_image *sourceHeader;
	FEATUREIO fioTarget;
	FEATUREIO fioSource;
	int bIsotropicProcessing = 1;
	int iArg = 1;
	if( fioReadNifti( fioTarget, argv[iArg], bIsotropicProcessing, 0, &targetHeader ) < 0 )
	{
			printf( "Error: could not read input file: %s\n", argv[iArg] );
			return -1;
	}
	iArg++;

	// Sumbsample
	FEATUREIO fioTmp = fioTarget;
	fioTarget.x /= 2;
	fioTarget.y /= 2;
	fioTarget.z /= 2;
	fioAllocate( fioTarget );
	fioSubSample2DCenterPixel( fioTmp, fioTarget );
	fioDelete( fioTmp );


	// Readjust nifti header

	targetHeader->dim[1] = targetHeader->nx = fioTarget.x;
	targetHeader->dim[2] = targetHeader->ny = fioTarget.y;
	targetHeader->dim[3] = targetHeader->nz = fioTarget.z;
	targetHeader->data = fioGetVector( fioTarget, 0,0,0 );
	targetHeader->pixdim[1] = targetHeader->dx = 2*targetHeader->dx;
	targetHeader->pixdim[2] = targetHeader->dy = 2*targetHeader->dy;
	targetHeader->pixdim[3] = targetHeader->dz = 2*targetHeader->dz;
	targetHeader->nvox = fioTarget.x*fioTarget.y*fioTarget.z;


	fioWriteNifti( fioTarget, targetHeader, argv[iArg] );
	return 1;
}

//
// resampleTextFile()
//
// Resample and output coordinates in a text file according a transform, then output.
//
int
resampleTextFile(
				 char *pcFileIn,
				 char *pcFileTransform,
				 char *pcFileOut
				 )
{
	TransformSimilarity ts;
	vector<Feature3DInfo> vecFeats;

	float pfZero[3] = {0,0,0};

	int bRunTranslations = 0; // Input a list of translations, to center about a landmark
	int bRunMirrorYAxis = 1; // Mirror X axis, to double training set of symmetric objects (Gurman Lung)

	//if( msFeature3DVectorInputBin( vecFeats, pcFileIn, 140 ) > )
	if( msFeature3DVectorInputText( vecFeats, pcFileIn, 140 ) >= 0 )
	{
		if( bRunTranslations )
		{
			// This code was strictly to translate data to center label locations
			// example command line:
			//  text \trainingFeatures00.txt \trainingLabels00.txt  out.txt
			// 
			//ts.Identity();
			FILE *infile = fopen( pcFileTransform, "rt" );
			if( !infile  )
			{
				return -1;
			}
			
			vector<Feature3DInfo> vecFeatsSave;
			vecFeatsSave = vecFeats;

			// Treat input point list instead as a bunch of translations
			int iPoint = 0;
			float pfP0[3];
			while( fscanf( infile, "%f %f %f\n", &pfP0[0], &pfP0[1], &pfP0[2] ) == 3 )
			{
				// Translate to 150 - Gurman's data
				ts.m_pfTrans[0] = 150 - pfP0[0];
				ts.m_pfTrans[1] = 150 - pfP0[1];
				ts.m_pfTrans[2] = 150 - pfP0[2];

				vecFeats = vecFeatsSave;

				// Open, transform and write feature file
				for( int i = 0; i < vecFeats.size(); i++ )
				{
					vecFeats[i].SimilarityTransform( pfZero, &ts.m_pfTrans[0], ts.m_ppfRot[0], ts.m_fScale );
				}

				char pcFileName[400];
				sprintf( pcFileName, "%s.%1.1d.txt", pcFileOut, iPoint );

				msFeature3DVectorOutputText( vecFeats, pcFileName );

				iPoint++;
			}

			fclose( infile );
			return 0; 
		}
		else if( bRunMirrorYAxis )
		{
			// Mirror about point 150 y axis, transform is unimportant
			// example command line:
			//  text \trainingFeatures00.txt junk-unimportant-argument  out.txt

			for( int k = 0; k < vecFeats.size(); k++ )
			{
				msMirrorYFeaturesGradientOrientationHistogram( vecFeats[k] );
				vecFeats[k].y = 150-vecFeats[k].y;
				vecFeats[k].ori[0][1] = -vecFeats[k].ori[0][1];
				vecFeats[k].ori[1][1] = -vecFeats[k].ori[1][1];
				vecFeats[k].ori[2][2] = -vecFeats[k].ori[2][2];
				vecFeats[k].ori[2][0] = -vecFeats[k].ori[2][0];
			}
			return msFeature3DVectorOutputText( vecFeats, pcFileOut );
		}
	}
	else
	{
		// Open, transform and write text file

		if( ts.ReadMatrix( pcFileTransform ) != 1 )
		{
			return -1;
		}

		FILE *infile = fopen( pcFileIn, "rt" );
		FILE *outfile = fopen( pcFileOut, "wt" );
		if( !infile || !outfile )
		{
			return -1;
		}

		float pfP0[3];
		while( fscanf( infile, "%f %f %f\n", &pfP0[0], &pfP0[1], &pfP0[2] ) == 3 )
		{
			float pfP1[3];

			// Determine location of point P1 in fio1
			similarity_transform_3point(
					pfP0, pfP1,
					pfZero, ts.m_pfTrans,
					ts.m_ppfRot[0], ts.m_fScale
				   );
			fprintf( outfile, "%f\t%f\t%f\n", pfP1[0], pfP1[1], pfP1[2] );
		}
		fclose( infile );
		fclose( outfile );
	}
	return 0;
}

//
// C:\downloads\data\styner\combined_feature\normal\11063_082009_combined_result.hdr C:\downloads\data\styner\combined_feature\normal\11053_0816_combined_result_rot.hdr C:\downloads\data\styner\combined_feature\normal\11053_0816_combined_result_rot.key.trans-inverse.txt out.hdr
// E:\code\data\11063_082009_combined_result.hdr E:\code\data\11053_0816_combined_result_rot.hdr E:\code\data\11053_0816_combined_result_rot.key.trans-inverse.txt out.hdr
//
// C:\downloads\data\RIRE\back\test_patient_002\ssimage-CT-s.hdr C:\downloads\data\RIRE\back\test_patient_002\ssimage-CT.hdr C:\downloads\data\RIRE\back\test_patient_002\ssimage-CT-out.hdr
// C:\downloads\data\RIRE\back\test_patient_002\ssimage-CT-s.hdr C:\downloads\data\RIRE\back\test_patient_002\simage-T1.hdr C:\downloads\data\RIRE\back\test_patient_002\ssimage-T1-out.hdr
// C:\downloads\data\RIRE\back\test_patient_002\ct\simage-small.hdr C:\downloads\data\RIRE\back\test_patient_002\ct\simage.hdr C:\downloads\data\RIRE\back\test_patient_002\ct\simage-ct-out.hdr
//
// C:\downloads\data\RIRE\image\patient_105_mr_MP-RAGE_iso.nii C:\downloads\data\RIRE\image\patient_105_ct_iso.nii C:\downloads\data\RIRE\image\modal-multi3\pass20\patient_105_ct_iso.key.trans-inverse.txt resampled.hdr
// C:\downloads\data\RIRE\image\patient_105_mr_MP-RAGE_iso.nii C:\downloads\data\RIRE\image\patient_101_ct_iso.nii C:\downloads\data\RIRE\image\multi-modal\pass20\patient_101_ct_iso.key.trans-inverse.txt resampled_101.nii
// C:\downloads\data\RIRE\image\patient_105_mr_MP-RAGE_iso.nii C:\downloads\data\RIRE\image\patient_101_ct_iso.nii C:\downloads\data\RIRE\image\multi-modal\pass20\patient_101_ct_iso.key.trans.txt resampled.nii
//
// C:\downloads\data\andriy\temp100.hdr C:\downloads\data\andriy\10.hdr C:\downloads\data\andriy\identity-trans.txt out.hdr
//
int
main(
	 int argc,
	 char **argv
)
{
	// featCrop( argc, argv ); return 1;
	// featResampleIsotropic( argc, argv ); return 1;
	//featSubsample( argc, argv ); return 1;

	FEATUREIO	fioIn;
	PpImage		ppImgIn;

	if( argc != 5 )
	{
		printf( "Volumetric image resample v1.1\n");
		printf( "Usage: %s <target image> <source image> <linear transform> <output image>\n", "featResample" );
		printf( "  <target/source/output image>: nifti (.nii,.hdr).\n" );
		printf( "  <linear transform>: input text file 4x4 matrix.\n" );
		return -1;
	}
	int iArg = 1;
	int bDoubleImageSize = 0;
	while( argv[iArg][0] == '-' )
	{
		switch( argv[iArg][1] )
		{
			// Double initial image resolution option
		case '2':
			// Option to double initial image size, get 8 times as many features...
			bDoubleImageSize = 1;
			if( argv[iArg][2] == '-' )
			{
				// Halve image resolution
				bDoubleImageSize = -1;
			}
			iArg++;
			break;
		default:
			printf( "Error: unknown command line argument: %s\n", argv[iArg] );
			return -1;
			break;
		}
	}

	// This is the option to input/output a text file with coordinates.
	if( strcmp( argv[iArg], "text" ) == 0 )
	{
		printf( "Resampling text files.\n" );
		return resampleTextFile( argv[iArg+1], argv[iArg+2], argv[iArg+3] );
	}

	nifti_image *targetHeader;
	nifti_image *sourceHeader;
	FEATUREIO fioTarget;
	FEATUREIO fioSource;
	if( fioReadNifti( fioTarget, argv[iArg], &targetHeader ) < 0 )
	{
			printf( "Error: could not read input file: %s\n", argv[iArg] );
			return -1;
	}
	iArg++;

	// For flip twin brain axis
	//fioFlipAxisX( fioTarget );
	//fioWriteNifti( fioTarget, targetHeader, argv[iArg] );
	//return 1;

	if( fioReadNifti( fioSource, argv[iArg], &sourceHeader ) < 0 )
	{
			printf( "Error: could not read input file: %s\n", argv[iArg] );
			return -1;
	}
	iArg++;

	//targetHeader->iname_offset = 0;
	//fioWriteNifti( fioTarget, targetHeader, "test1.hdr" );
	//sourceHeader->ndim = 4;
	//sourceHeader->dim[0] = 4;
	//fioWriteNifti( fioSource, sourceHeader, "test2.hdr" );
	//fioWriteNifti( fioSource, sourceHeader, "test2.nii" );
	//fioReadNifti( fioTarget, "test.hdr", &targetHeader );
	//fioWriteNifti( fioTarget, targetHeader, "test.hdr" );

	TransformSimilarity ts;
	ts.ReadMatrix( argv[iArg] );
	iArg++;

	float pfZero[3] = {0,0,0};
	//similarity_transform_invert( pfZero, ts.m_pfTrans, ts.m_ppfRot[0], ts.m_fScale );
	similarity_transform_image( fioTarget, fioSource, pfZero, ts.m_pfTrans, ts.m_ppfRot[0], ts.m_fScale );

	if( strstr( argv[iArg], ".pgm" ) )
	{
		Feature3DInfo feat;
		feat.x = fioTarget.x/2;
		feat.y = fioTarget.y/2;
		feat.z = fioTarget.z/2;
		feat.OutputVisualFeatureInVolume(  fioTarget, argv[iArg] );
	}
	else
	{
		fioWriteNifti( fioTarget, targetHeader, argv[iArg] );
	}

	return 0;
}
