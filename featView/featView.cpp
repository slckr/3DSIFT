

#include <stdio.h>
#include <assert.h>
#include "src_common.h"
#include "nifti1_io.h"

template <class NewTYPE, class DTYPE>
void reg_changeDatatype1(nifti_image *image)
{
	// the initial array is saved and freeed
	DTYPE *initialValue = (DTYPE *)malloc(image->nvox*sizeof(DTYPE));
	memcpy(initialValue, image->data, image->nvox*sizeof(DTYPE));
	
	// the new array is allocated and then filled
	if(sizeof(NewTYPE)==sizeof(unsigned char)) image->datatype = NIFTI_TYPE_UINT8;
	else if(sizeof(NewTYPE)==sizeof(float)) image->datatype = NIFTI_TYPE_FLOAT32;
	else if(sizeof(NewTYPE)==sizeof(double)) image->datatype = NIFTI_TYPE_FLOAT64;
	else{
		printf("err\treg_changeDatatype\tOnly change to unsigned char, float or double are supported\n");
		free(initialValue);
		return;
	}
	free(image->data);
	image->nbyper = sizeof(NewTYPE);
	image->data = (void *)calloc(image->nvox,sizeof(NewTYPE));
	NewTYPE *dataPtr = static_cast<NewTYPE *>(image->data);
	for(unsigned int i=0; i<image->nvox; i++)
		dataPtr[i] = (NewTYPE)(initialValue[i]);

	free(initialValue);
	return;
}

template <class NewTYPE>
void reg_changeDatatype(nifti_image *image)
{
	switch(image->datatype){
		case NIFTI_TYPE_UINT8:
			reg_changeDatatype1<NewTYPE,unsigned char>(image);
			break;
		case NIFTI_TYPE_INT8:
			reg_changeDatatype1<NewTYPE,char>(image);
			break;
		case NIFTI_TYPE_UINT16:
			reg_changeDatatype1<NewTYPE,unsigned short>(image);
			break;
		case NIFTI_TYPE_INT16:
			reg_changeDatatype1<NewTYPE,short>(image);
			break;
		case NIFTI_TYPE_UINT32:
			reg_changeDatatype1<NewTYPE,unsigned int>(image);
			break;
		case NIFTI_TYPE_INT32:
			reg_changeDatatype1<NewTYPE,int>(image);
			break;
		case NIFTI_TYPE_FLOAT32:
			reg_changeDatatype1<NewTYPE,float>(image);
			break;
		case NIFTI_TYPE_FLOAT64:
			reg_changeDatatype1<NewTYPE,double>(image);
			break;
		default:
			printf("err\treg_changeDatatype\tThe initial image data type is not supported\n");
			return;
	}
}

//
// fioReadNifti()
//
// Read nifty, optionally convert to isotropic.
//
int
fioReadNifti(
			 FEATUREIO &fioIn,
			 char *pcFileName,
			 int bIsotropic = 0,
			 int iRescale = 0,
			 nifti_image *returnHeader = 0
			 )
{
	/* Read the target and source images */
	nifti_image *targetHeader = nifti_image_read(pcFileName,true);
	if(targetHeader == NULL )
	{
		return -1;
	}

	if( targetHeader->data == NULL )
	{
		return -2;
	}

	reg_changeDatatype<float>(targetHeader);

	FEATUREIO fioTmp;
	memset( &fioTmp, 0, sizeof(FEATUREIO) );
	memset( &fioIn, 0, sizeof(FEATUREIO) );

	fioTmp.x = targetHeader->nx;
	fioTmp.y = targetHeader->ny;
	fioTmp.z = targetHeader->nz;
	fioTmp.t = targetHeader->nt > 0 ? targetHeader->nt : 1;
	fioTmp.iFeaturesPerVector = 1;
	fioTmp.pfVectors = (float*)targetHeader->data;

	if( bIsotropic &&
		(targetHeader->dx != targetHeader->dy
			|| targetHeader->dy != targetHeader->dz
				|| targetHeader->dx != targetHeader->dz)
				)
	{
		float fMinSize = targetHeader->dx;
		if( targetHeader->dy < fMinSize )
			fMinSize = targetHeader->dy;
		if( targetHeader->dz < fMinSize )
			fMinSize = targetHeader->dz;

		fioIn.x = targetHeader->nx*targetHeader->dx / fMinSize;
		fioIn.y = targetHeader->ny*targetHeader->dy / fMinSize;
		fioIn.z = targetHeader->nz*targetHeader->dz / fMinSize;
		fioIn.t = targetHeader->nt > 0 ? targetHeader->nt : 1;
		fioIn.iFeaturesPerVector = 1;
		if( !fioAllocate( fioIn ) )
		{
			// Not enough memory to resample :(
			return -3;
		}

		// Determine which matrix to use - pick the non-zero one
		//float fSumQ = fabs(targetHeader->qto_ijk.m[0][0])+fabs(targetHeader->qto_ijk.m[0][2])+fabs(targetHeader->qto_ijk.m[0][2])+fabs(targetHeader->qto_ijk.m[0][3]);
		//float fSumS = fabs(targetHeader->sto_ijk.m[0][0])+fabs(targetHeader->sto_ijk.m[0][2])+fabs(targetHeader->sto_ijk.m[0][2])+fabs(targetHeader->sto_ijk.m[0][3]);
		//int bEqualMatrices = (memcmp( &(targetHeader->qto_ijk.m[0][0]), &(targetHeader->sto_ijk.m[0][0]), sizeof(targetHeader->qto_ijk.m) ) == 0);

		float x,y,z;

		// Generate multiplication factors for rescaling/upscaling
		// These factors shrink isotropic coordinates down to anisotropic.
		float pfRescaleFactorsIJK[3];
		pfRescaleFactorsIJK[0] = fMinSize / targetHeader->dx;
		pfRescaleFactorsIJK[1] = fMinSize / targetHeader->dy;
		pfRescaleFactorsIJK[2] = fMinSize / targetHeader->dz;

		// Apply rescale factors ijk, one per factor per column.
		// These will be used to compute feature coordinates in xyz
		// Note: scaling factors operate on ijk voxel coordinates, so 
		// apply one rescaling/normalization factor per transform matrix column.
		// Essentially, we rescale the direction cosine angles.
		for( int i = 0; i < 3; i++ )
		{
			for( int j = 0; j < 3; j++ )
			{
				targetHeader->qto_xyz.m[i][j] *= pfRescaleFactorsIJK[j];
				if( targetHeader->sform_code > 0 )
					targetHeader->sto_xyz.m[i][j] *= pfRescaleFactorsIJK[j];
			}
		}

		// Compute inverses
		targetHeader->qto_ijk = nifti_mat44_inverse( targetHeader->qto_xyz );
		if( targetHeader->sform_code > 0 )
			targetHeader->sto_ijk = nifti_mat44_inverse( targetHeader->sto_xyz );

		for( int z = 0; z < fioIn.z; z++ )
		{
			for( int y = 0; y < fioIn.y; y++ )
			{
				for( int x = 0; x < fioIn.x; x++ )
				{
					float pXYZ[3];

					// * This is the original code, I believe it is slightly
					// incorrect, the 0.5 should be applied after multiplication/division
					//float fPixel = fioGetPixelTrilinearInterp( fioTmp,
					//		(x*fMinSize+0.5)/targetHeader->dx,
					//		(y*fMinSize+0.5)/targetHeader->dy,
					//		(z*fMinSize+0.5)/targetHeader->dz
					//		);					

					float fPixel = fioGetPixelTrilinearInterp( fioTmp,
							(x*pfRescaleFactorsIJK[0]+0.5),
							(y*pfRescaleFactorsIJK[1]+0.5),
							(z*pfRescaleFactorsIJK[2]+0.5)
							);

					float *pfDestPix = fioGetVector( fioIn, x, y, z );
					*pfDestPix = fPixel;
				}
			}
		}

		// Reset changed fields
		targetHeader->dx = fMinSize;
		targetHeader->dy = fMinSize;
		targetHeader->dz = fMinSize;

		//fioWrite( fioIn, "out-iso2"  );

	}
	else
	{
		// Allocate & copy image to new data structure
		// (old will be deleted ... )
		fioIn = fioTmp;
		fioAllocate( fioIn );
		fioCopy( fioIn, fioTmp );
	}

	// Save return target header information
	if( returnHeader != 0 )
		memcpy( returnHeader, targetHeader, sizeof(nifti_image) );

	nifti_image_free( targetHeader );
	return 1;
}

//
//int
//fioReadNifti(
//			 FEATUREIO &fioIn,
//			 char *pcFileName
//			 )
//{
//	/* Read the target and source images */
//	nifti_image *targetHeader = nifti_image_read(pcFileName,true);
//	if(targetHeader == NULL )
//	{
//		return -1;
//	}
//
//	if( targetHeader->data == NULL )
//	{
//		return -2;
//	}
//
//	reg_changeDatatype<float>(targetHeader);
//
//	memset( &fioIn, 0, sizeof(FEATUREIO) );
//
//	fioIn.x = targetHeader->nx;
//	fioIn.y = targetHeader->ny;
//	fioIn.z = targetHeader->nz;
//	fioIn.t = targetHeader->nt;
//	fioIn.iFeaturesPerVector = 1;
//	fioIn.pfVectors = (float*)targetHeader->data;
//
//	return 1;
//}

//
// readInputTextCoordinates()
//
// Originally a less restrictive format for reading in 3D points + scale.
// Now
//
int
readInputTextCoordinates(
						 vector<char*> &vecImgNames,
						 vector<Feature3DInfo> &vecFeats3D,
						 vector<char*> &vecCodes,
						 TextFile &tfInput
						 )
{

	Feature3DInfo feat;
	vecFeats3D.clear();

	int iIndex = 0;

	for( int i = 0; i < tfInput.Lines(); i++ )
	{
		// Create a list of pointers to tab-separated fields
		char *ppcFields[7];
		ppcFields[0] = tfInput.GetLine( i );
		if( ppcFields[0][0] == '#' )
		{
			// Comment line
			continue;
		}
		int j;
		for( j = 1; j < 7; j++ )
		{
			ppcFields[j] = strchr( ppcFields[j-1], '\t' );
			if( !ppcFields[j] )
			{
				break;
			}
			// Set tab to null
			ppcFields[j][0] = 0;
			ppcFields[j]++;
		}
		if( j < 6 )
		{
			// Insufficient input
			continue;
		}

		feat.x = atof( ppcFields[1] );
		feat.y = atof( ppcFields[2] );
		feat.z = atof( ppcFields[3] );
		feat.scale = atof( ppcFields[4] );


		vecImgNames.push_back( ppcFields[0] );
		vecFeats3D.push_back( feat );
		vecCodes.push_back( ppcFields[5] );		

		iIndex++;
	}


	return vecFeats3D.size();
}

//
// readInputTextCoordinates()
//
// Less restrictive format for reading in 3D points + scale
//
int
readInputTextCoordinates(
				  vector<Feature3DInfo> &vecFeats3D,
				  char *pcFileName
						 )
{
	FILE *infile = fopen( pcFileName, "rt" );
	if( !infile )
	{
		return -1;
	}
	Feature3DInfo feat;
	vecFeats3D.clear();

	char *pcBuffer = new char[2000];

	int iIndex = 0;

	while( fgets( pcBuffer, 2000, infile ) )
	{
		if( sscanf( pcBuffer, "%f\t%f\t%f\t%f", &feat.x, &feat.y, &feat.z, &feat.scale ) == 4 )
		{
			// 5th field unavailable ... set to 0
			feat.m_uiInfo = 0;
			vecFeats3D.push_back( feat );
			iIndex++;
		}
	}

	delete [] pcBuffer;
	fclose( infile );

	return vecFeats3D.size();
}

void
usage(
	  )
{
		// Three modes of uage
		printf( "Volumetric Feature Visualization v1.1\n");
		printf( "Usage 1: %s [options] <input image> <x>  <y>  <z>  <scale> <output_code>\n", "featView" );
		printf( "  <input image>: nifti (.nii,.hdr).\n" );
		printf( "  <x>  <y>  <z>  <scale>: four numbers, feature information.\n" );
		printf( "  <output_code>: name code appended to output image.\n" );
		printf( "Usage 2: %s [options] <input image> <feature list> <output_code> \n", "featView" );
		printf( "  <input image>: nifti (.nii,.hdr).\n" );
		printf( "  <feature list>: text file, each line contains: <x>  <y>  <z>  <scale>.\n" );
		printf( "  <output_code>: name code appended to output image.\n" );
		printf( "Usage 3: %s [options] <command_file>\n", "featView" );
		printf( "  <command_file>: text file, each line contains: <input image> <x>  <y>  <z>  <scale> <output_code>.\n" );
		printf( "Options: <options>\n" );
		printf( "  -qto_xyz : feature geometry in millimeter units using NIFTI qto_xyz transform\n" );
		printf( "  -sto_xyz : feature geometry in millimeter units using NIFTI sto_xyz transform (currently unsupported)\n" );
		printf( "  -voxel   : feature geometry in voxel units\n" );
}


//
int
main(
	 int argc,
	 char **argv
)
{
	// Write the code to test the scale selection stuff

	FEATUREIO	fioIn;
	PpImage		ppImgIn;

	memset( &fioIn, 0, sizeof(fioIn) );

	if( argc < 2 )
	{
		usage();
		return -1;
	}

	int iArg = 1;
	int bDoubleImageSize = 0;
	int bVoxels = 1;
	int bqto_xyz = 0;
	while( argv[iArg][0] == '-' )
	{
		switch( argv[iArg][1] )
		{
		case 'v': case 'V':
			bVoxels = 1;
			bqto_xyz = 0;
			iArg++;
			break;
			
		case 'q': case 'Q':
			bVoxels = 0;
			bqto_xyz = 1;
			iArg++;
			break;

		default:
			printf( "Error: unknown command line argument: %s\n", argv[iArg] );
			return -1;
			break;
		}
	}

		// Three modes of uage
	if( (argc-iArg) != 1 && (argc-iArg) != 3 && (argc-iArg) != 6 )
	{
		usage();
		return -1;
	}


	TextFile tfInput;
	vector<Feature3DInfo> vecFeats;
	vector<char*> vecImgNames;
	vector<char*> vecCodes;

	if( (argc-iArg)  == 1 )
	{
		printf( "Reading features: %s\n", argv[iArg] );
		if( tfInput.Read( argv[iArg] ) < 0 )
		{
			printf( "Could not open input file: %s\n", argv[iArg] );
			return -1;
		}
		//if( msFeature3DVectorInputText( vecFeats, argv[iArg], 144 ) < 0 )
		if( readInputTextCoordinates( vecImgNames, vecFeats, vecCodes, tfInput ) < 0 )
		{
			printf( "Error: could not read input file: %s\n", argv[iArg] );
			return -1;
		}
		iArg++;
	}
	else if( (argc-iArg)  == 3 )
	{
		readInputTextCoordinates( vecFeats, argv[iArg+1] );
		if( vecFeats.size() == 0 )
		{
			printf( "Error: could not read/identify features in input file: %s\n", argv[iArg+1] );
		}
		vecImgNames.resize( vecFeats.size() );
		vecCodes.resize( vecFeats.size() );
		for( int i = 0; i < vecFeats.size(); i++ )
		{
			vecImgNames[i] = argv[iArg];
			vecCodes[i] = argv[iArg+2];
		}
	}
	else if( (argc-iArg)  == 6 )
	{
		vecImgNames.push_back( argv[iArg++] );
		vecFeats.resize(1);
		vecFeats[0].x = atof( argv[iArg++] );
		vecFeats[0].y = atof( argv[iArg++] );
		vecFeats[0].z = atof( argv[iArg++] );
		vecFeats[0].scale = atof( argv[iArg++] );
		vecCodes.push_back( argv[iArg++] );
	}

	printf( "Writing feature images (%d)...\n", vecFeats.size() );

	// Read in NIFTI as isotropic
	// Convert all feature geometry to image units

	nifti_image returnHeader;

	for( int i = 0; i < vecFeats.size(); i++ )
	{
		char pcCurrFileName[400];
		sprintf( pcCurrFileName, "%s", vecImgNames[i] );
		if( i == 0 || strcmp( pcCurrFileName, vecImgNames[i-1] ) != 0 )
		{
			if( i > 0 )
			{
				// Delete previous image
				fioDelete( fioIn );
			}
			printf( "Reading image: %s .. ", pcCurrFileName );
			if( fioReadNifti( fioIn, pcCurrFileName, 1, 0, &returnHeader ) < 0 )
			{
				// Try different extensions
				char *pcName;
				if( pcName = strrchr( pcCurrFileName, '.' ) )
				{
					sprintf( pcName, ".hdr" );
					printf( ".hdr .. " );
					if( fioReadNifti( fioIn, pcCurrFileName, 1, 0, &returnHeader ) < 0 )
					{
						sprintf( pcName, ".nii" );
						printf( ".nii .. " );
						if( fioReadNifti( fioIn, pcCurrFileName, 1, 0, &returnHeader ) < 0 )
						{
							printf( "Error: could not read input image: %s\n", pcCurrFileName );
							return -1;
						}
					}
				}
			}
			printf( "done.\n" );
		}

		// Convert feature location & scale for output
		//vecFeats[i].

		char *pcFNameNoDir = tfInput.GetFileName( vecImgNames[i] );

		char pcFileName[400];
		sprintf( pcFileName, "%s.%s", vecCodes[i], pcFNameNoDir );

		if( bqto_xyz )
		{
			// Convert - qto_ijk should be a nice similarity transform ... 
			vecFeats[i].SimilarityTransform( &returnHeader.qto_ijk.m[0][0] );
		}
		vecFeats[i].OutputVisualFeatureInVolume( fioIn, pcFileName, 1 );
	}

	if( fioIn.pfVectors )
	{
		fioDelete( fioIn );
	}
	printf( "\nDone.\n" );

	return 0;
}

