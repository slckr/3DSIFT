#include <iostream>

#include "FeatureIOnifti.h"
#include "featMatchUtilities.h"
#include "src_common.h"
#include "nifti1_io.h"

#define N_CORES 4

int
readFeatsLabels(
	char *pcInputFilesLabels,
	vector< vector<Feature3DInfo> > &vvInputFeats,
	vector< int > &vLabels
)
{
	FILE *infile = fopen(pcInputFilesLabels, "rt");
	if (!infile)
	{
		return -1;
	}

	char pcBuffer[500];
	int iFeatVec = 0;
	vector<Feature3DInfo> vfRead;
	while (fgets(pcBuffer, 500, infile))
	{
		char *pcFile = &pcBuffer[0];
		char *pc = strchr(pcBuffer, '\t');
		if (!pc)
			continue;
		*pc = 0;
		int iLabel = atoi(pc + 1);

		vfRead.clear();
		if (msFeature3DVectorInputBin(vfRead, pcFile, 140) < 0)
		{
			continue;
		}
		removeReorientedFeatures(vfRead);

		vvInputFeats.push_back(vfRead);
		vLabels.push_back(iLabel);
		iFeatVec++;
		if (iFeatVec > 100)
		{
			break;
		}
	}
	fclose(infile);

	return 1;
}

int
readFeatsLabels(
	char *pcInputFilesLabels,
	vector<Feature3DInfo> &vInputFeats,
	vector< int > &vImageIndex,
	vector< int > &vLabels
)
{
	FILE *infile = fopen(pcInputFilesLabels, "rt");
	if (!infile)
	{
		return -1;
	}

	char pcBuffer[500];
	int iFeatVec = 0;
	vector<Feature3DInfo> vfRead;
	while (fgets(pcBuffer, 500, infile))
	{
		char *pcFile = &pcBuffer[0];
		char *pc = strchr(pcBuffer, '\t');
		if (!pc)
			continue;
		*pc = 0;
		int iLabel = atoi(pc + 1);

		vfRead.clear();
		if (msFeature3DVectorInputBin(vfRead, pcFile, 140) < 0)
		{
			continue;
		}
		removeReorientedFeatures(vfRead);

		for (int i = 0; i < vfRead.size(); i++)
		{
			vInputFeats.push_back(vfRead[i]);
			vImageIndex.push_back(iFeatVec);
		}

		vLabels.push_back(iLabel);
		iFeatVec++;
		if (iFeatVec > 500)
		{
			break;
		}
	}
	fclose(infile);

	return 1;
}



//
// matchBunchToBunch()
//
// This function is intended to investigate NN classification.
//
int
matchBunchToBunch(
	char *pcInputFilesLabels
)
{
	// Read in data, half for training, half for testing
	vector<Feature3DInfo> vInputFeats;
	vector< int > vImageIndex;
	vector< int > vLabels;

	readFeatsLabels(pcInputFilesLabels, vInputFeats, vImageIndex, vLabels);

	// Split in half
	int i;
	for (i = 0; i < vInputFeats.size() && vImageIndex[i] < vLabels.size() / 2; i++)
	{
	}
	int iSize1 = i;
	int iSize2 = vInputFeats.size() - i;
	float *pf1 = new float[iSize1*PC_ARRAY_SIZE];
	float *pf2 = new float[iSize2*PC_ARRAY_SIZE];
	for (int i = 0; i < iSize1; i++)
		memcpy(pf1 + i*PC_ARRAY_SIZE, &(vInputFeats[i].m_pfPC[0]), sizeof(vInputFeats[i].m_pfPC));
	for (int i = 0; i < iSize2; i++)
		memcpy(pf2 + i*PC_ARRAY_SIZE, &(vInputFeats[iSize1 + i].m_pfPC[0]), sizeof(vInputFeats[iSize1 + i].m_pfPC));

	vector< int > viImageFeatCounts;
	viImageFeatCounts.resize(vLabels.size());
	for (i = 0; i < vInputFeats.size(); i++)
	{
		int iImage = vImageIndex[i];
		viImageFeatCounts[iImage]++;
	}

	vector<int> vecMatchIndex12;
	vector<float> vecMatchDist12;

	int nn = 2;
	int *piResult = new int[iSize1*nn];
	float *pfDist = new float[iSize1*nn];

	msComputeNearestNeighborDistanceRatioInfoApproximate(pf1, pf2, iSize1, iSize2, piResult, pfDist);

	vector< vector< int > > vviCounts;
	vviCounts.resize(vLabels.size());
	for (int i = 0; i < vviCounts.size(); i++)
	{
		vviCounts[i].resize(vLabels.size(), 0);
	}
	for (i = 0; i < iSize1; i++)
	{
		int iImage = vImageIndex[i];
		int iMatchFeature = piResult[i*nn];
		int iMatchImage = vImageIndex[iMatchFeature];

		Feature3DInfo &feat1 = vInputFeats[i];
		Feature3DInfo &feat2 = vInputFeats[iSize1 + iMatchFeature];
		if (compatible_features(feat1, feat2))
		{
			vviCounts[iImage][iMatchImage]++;
		}
	}

	FILE *outfile = fopen("outimages.txt", "wt");
	for (int i = 0; i < vviCounts.size(); i++)
	{
		int iMaxIndex = 0;
		float fMaxCount = (float)vviCounts[i][0] / (float)viImageFeatCounts[0];
		for (int j = 0; j < vviCounts[i].size(); j++)
		{
			float fCount = (float)vviCounts[i][j] / (float)viImageFeatCounts[j];
			if (fCount > fMaxCount)
			{
				fMaxCount = fCount;
				iMaxIndex = j;
			}
		}
		fprintf(outfile, "%d\t%d\t%f\n", i, iMaxIndex, fMaxCount);
	}
	fclose(outfile);

	outfile = fopen("outlabels.txt", "wt");
	for (int i = 0; i < vviCounts.size(); i++)
	{
		int iMaxIndex = 0;
		float fMaxCount = (float)vviCounts[i][0] / (float)viImageFeatCounts[0];
		for (int j = 0; j < vviCounts[i].size(); j++)
		{
			float fCount = (float)vviCounts[i][j] / (float)viImageFeatCounts[j];
			if (fCount > fMaxCount)
			{
				fMaxCount = fCount;
				iMaxIndex = j;
			}
		}
		fprintf(outfile, "%d\t%d\t%f\n", vLabels[i], vLabels[iMaxIndex], fMaxCount);
	}
	fclose(outfile);

	delete[] pfDist;
	delete[] piResult;
	delete[] pf2;
	delete[] pf1;

	// Store feats in database

	//vector<int> vecModelMatches;
	//vector<Feature3DInfo> vecFeats2Updated;
	//vector<int> vecModelMatchesUpdated;

	//TransformSimilarity ts12, ts12Next, ts12Inv, ts31, ts32;

	//int bRefine = 1;

	//int iImg1=-1, iImg2=-1, iImg3=-1;
	//FILE *outfile = fopen( "corr_matrix.txt", "wt" );
	//fclose( outfile );
	//outfile = fopen( "closest_match.txt", "wt" );
	//fclose( outfile );
	//for( int i = 0; i < vvInputFeats.size(); i++ )
	//{
	//	int iMaxIndex = -1;
	//	int iMaxMatches = 0;

	//	// Save closest match and score
	//	vector<int> vecModelMatchesBest;
	//	vector<float> vecModelMatchesBestScore;
	//	vecModelMatchesBest.resize( vvInputFeats[i].size() );
	//	vecModelMatchesBestScore.resize( vvInputFeats[i].size() );
	//	for( int j = 0; j < vecModelMatchesBest.size(); j++ )
	//	{
	//		vecModelMatchesBest[j] = -1;
	//		vecModelMatchesBestScore[j] = 0;
	//	}

	//	for( int j = 0; j < vvInputFeats.size(); j++ )
	//	{
	//		iImg1 = i;
	//		iImg2 = j;

	//		char *pci1=0;
	//		char *pci2=0;

	//		// Match 1->2
	//		int iInliers1 = MatchKeys(
	//			vvInputFeats[iImg1],
	//			vvInputFeats[iImg2], vecModelMatches, -1, pci1, pci2, &vecFeats2Updated, 0, &ts12.m_fScale, ts12.m_ppfRot[0], ts12.m_pfTrans );


	//		int iMatches;
	//		iMatches = 0;
	//		for( int g = 0; g < vecModelMatches.size(); g++ )
	//		{
	//			if( vecModelMatches[g] >= 0 )
	//				iMatches++;
	//		}
	//		
	//		//int iInliers2 = MatchKeys(
	//		//	vvInputFeats[iImg1],
	//		//	vecFeats2Updated, vecModelMatchesUpdated, -1, pci1, pci2, 0, 1, &ts12.m_fScale, ts12.m_ppfRot[0], ts12.m_pfTrans );
	//		//iMatches = 0;
	//		//for( int g = 0; g < vecModelMatchesUpdated.size(); g++ )
	//		//{
	//		//	if( vecModelMatchesUpdated[g] >= 0 )
	//		//		iMatches++;
	//		//}

	//		//iMatches = iInliers1; // Produces about the same result on twins.

	//		outfile = fopen( "corr_matrix.txt", "a+" );
	//		fprintf( outfile, "%d\t", iMatches );
	//		fclose( outfile );

	//		if( iImg1 != iImg2 )
	//		{
	//			if( iMatches > iMaxMatches )
	//			{
	//				iMaxMatches = iMatches;
	//				iMaxIndex = iImg2;
	//			}

	//			for( int g = 0; g < vecModelMatches.size(); g++ )
	//			{
	//				if( vecModelMatches[g] >= 0 )
	//				{
	//					int iMatch = vecModelMatches[g];
	//					float fDist = vvInputFeats[iImg1][g].DistSqrPCs( vvInputFeats[iImg2][iMatch] );
	//					if( vecModelMatchesBest[g] >= 0 )
	//					{
	//						if( fDist < vecModelMatchesBestScore[g] )
	//						{
	//							vecModelMatchesBest[g] = iMatch;
	//							vecModelMatchesBestScore[g] = fDist;
	//						}
	//					}
	//					else
	//					{
	//						vecModelMatchesBest[g] = iMatch;
	//						vecModelMatchesBestScore[g] = fDist;
	//					}
	//				}
	//			}
	//		}
	//	}

	//	// Go through and could the other subject with the most closest matches.
	//	int iMostClosestMatchesIndex = -1;
	//	int iMostClosestMatchesCount = 0;
	//	for( int j = 0; j < vvInputFeats.size(); j++ )
	//	{
	//		int iClosestMatchesCount = 0;
	//		for( int g = 0; g < vecModelMatchesBest.size(); g++ )
	//		{
	//			if( vecModelMatchesBest[g] == j )
	//			{
	//				iClosestMatchesCount++;
	//			}
	//		}
	//		if( iClosestMatchesCount > iMostClosestMatchesCount )
	//		{
	//			iMostClosestMatchesCount = iClosestMatchesCount;
	//			iMostClosestMatchesIndex = j;
	//		}
	//	}

	//	// Print out max match count - original metric
	//	outfile = fopen( "corr_matrix.txt", "a+" );
	//	fprintf( outfile, "Max:\t%d\n", iMaxIndex );
	//	fclose( outfile );

	//	// Save coordinates for matches


	//}
	return 1;
}

//
// featuresMerge()
//
// Merge and output features from several images. Encode index in info field.
// 
int
featuresMerge(
	vector< vector<Feature3DInfo> > &vvInputFeats,
	char *pcOutputFile
)
{
	vector<Feature3DInfo> vecFeatMerged;
	for (int i = 0; i < vvInputFeats.size(); i++)
	{
		for (int j = 0; j < vvInputFeats[i].size(); j++)
		{
			Feature3DInfo &feat = vvInputFeats[i][j];
			unsigned int uinfo = (i * 256) + feat.m_uiInfo;
			feat.m_uiInfo = uinfo;
			vecFeatMerged.push_back(feat);
		}
	}
	return msFeature3DVectorOutputText(vecFeatMerged, pcOutputFile);
}

//
// featuresSplit()
//
// Split features from individual files into several feature files.
// Currently the split condition is poorly defined, here just the bits above 256
// 
int
featuresSplit(
	vector< vector<Feature3DInfo> > &vvInputFeats,
	char *pcOutputFile
)
{
	vector<Feature3DInfo> vecFeatSplit;
	int iCurrSplitIndex = -1;
	char pcFileName[400];
	for (int i = 0; i < vvInputFeats.size(); i++)
	{
		for (int j = 0; j < vvInputFeats[i].size(); j++)
		{
			Feature3DInfo &feat = vvInputFeats[i][j];
			int iSplitIndex = feat.m_uiInfo / 256;
			if (iSplitIndex != iCurrSplitIndex)
			{
				// print out current list
				if (vecFeatSplit.size() > 0)
				{
					sprintf(pcFileName, "%s.%4.4d.key", pcOutputFile, iCurrSplitIndex);
					msFeature3DVectorOutputText(vecFeatSplit, pcFileName);
				}
				// Reset list
				iCurrSplitIndex = iSplitIndex;
				vecFeatSplit.clear();
			}
			vecFeatSplit.push_back(feat);
		}
	}
	// print out last current list
	if (vecFeatSplit.size() > 0)
	{
		sprintf(pcFileName, "%s.%4.4d.key", pcOutputFile, iCurrSplitIndex);
		msFeature3DVectorOutputText(vecFeatSplit, pcFileName);
	}
	return iCurrSplitIndex;
}

//
// matchAllToAll()
//
// Match all images to all images.
//
int
matchAllToAll(
	vector< char * > &vecNames,
	vector< int > &vecMatched,
	vector< vector<Feature3DInfo> > &vvInputFeats,
	int iNeighbors,	// number of nearest neighbors to consider
	vector< int > &viLabels, // labels for each feature set
	float fGeometryWeight = -1,
	char *pcOutputFile = 0,
	int iImgSplit = -1  // image to split on for testing
)
{
	printf("Creating NN index structure, NN=%d, image split=%d", iNeighbors, iImgSplit);
	//int iNeighbors = 5;
	//msNearestNeighborApproximateInit( vvInputFeats, iNeighbors, viLabels, fGeometryWeight, iImgSplit );

	// Soft Jaccard (Need to find more neighbors to make sure we can find iNeighbors neighbors from different labels).
	//msNearestNeighborApproximateInit(vvInputFeats, 2 * iNeighbors, viLabels, fGeometryWeight, iImgSplit);
	msNearestNeighborApproximateInit(vvInputFeats, iNeighbors, viLabels, fGeometryWeight, iImgSplit);


	printf("done.\n");
	/*
	int iMaxSize = 0;
	FILE *outfile;
	if (!pcOutputFile)
	{
		pcOutputFile = "_nn_matching.txt";
	}

	outfile = fopen(pcOutputFile, "wt");
	for (int i = 0; i < vvInputFeats.size(); i++)
	{
		if (vvInputFeats[i].size() > iMaxSize)
		{
			iMaxSize = vvInputFeats[i].size();
		}
		//fprintf(outfile, "%d\t", vvInputFeats[i].size());
	}
	fprintf(outfile, "\n");
	*/

	vector< int > viImgCounts;
	vector< float > viLabelCounts;
	vector< float > vfLabelLogLikelihood;

	// Count labels
	viImgCounts.resize(vecNames.size(), 0);

	// Assume labels go from 
	int iMaxLabel = 0;
	for (int i = 0; i < viLabels.size(); i++)
	{
		if (viLabels[i] > iMaxLabel)
		{
			iMaxLabel = viLabels[i];
		}
	}
	viLabelCounts.resize(iMaxLabel + 1);
	vfLabelLogLikelihood.resize(iMaxLabel + 1);

	/*
	FILE *outFetalUS = fopen("fetal_US_2016.txt", "a+");
	for (int i = 0; i < vecNames.size(); i++)
	{
		fprintf(outFetalUS, "%s\t", vecNames[i]);
	}

	FILE *outfileLogLike = fopen("_log_likelihood.txt", "wt");
	*/
	
	// Store votes
	FILE *outfileVotes = fopen("matching_votes.txt", "wt");
	float **ppfMatchingVotes = new float*[vvInputFeats.size()];
	for (int i = 0; i < vvInputFeats.size(); i++)
	{
		ppfMatchingVotes[i] = new float[vvInputFeats.size()];
		for (int j = 0; j < vvInputFeats.size(); j++)
		{
			ppfMatchingVotes[i][j] = 0.0;
		}
	}	
	
	// Create image chunks
	int iChunkSize = std::ceil(float(vvInputFeats.size()) / N_CORES);
	
	std::vector< std::pair<int,int> > vpChunkStartEnd;
	for (int i = 0; i < N_CORES; ++i)
	  {
	    int iStart = i*iChunkSize;
	    int iEnd = 0;

	    if (i == N_CORES-1)
	      {
		iEnd = vvInputFeats.size()-1;
	      }
	    else
	      {
		iEnd = (i+1)*iChunkSize-1;
	      }

	    std::pair<int,int> pStartEnd(iStart, iEnd);
	    vpChunkStartEnd.push_back(pStartEnd);
	  }


#pragma omp parallel for num_threads(N_CORES) schedule(static,1) shared(viImgCounts, viLabelCounts, vfLabelLogLikelihood, ppfMatchingVotes)
	  for (int n = 0; n < N_CORES; ++n)
	    {
	      for (int i = vpChunkStartEnd[n].first; i <= vpChunkStartEnd[n].second; i++)
		{
		  printf("Searching image %d of %d ... ", i, vvInputFeats.size());
		  msNearestNeighborApproximateSearchSelf(i, viImgCounts, viLabelCounts, vfLabelLogLikelihood, ppfMatchingVotes);
		  printf("\n");
		}
	    }

	// Output votes
	for (int i = 0; i < vvInputFeats.size(); i++)
	{
		for (int j = 0; j < vvInputFeats.size(); j++)
		{
			fprintf(outfileVotes, "%f\t", ppfMatchingVotes[i][j]);
		}
		fprintf(outfileVotes, "\n");
		delete[] ppfMatchingVotes[i];
	}
	delete[] ppfMatchingVotes;
	fclose(outfileVotes);

	msNearestNeighborApproximateDelete();

	return 1;
}

//
// matchCorrelationMatrix() 
// 
// This function performs N*N matching between all feature pairs.
//
// C:\downloads\data\lilla\twin\featdir\featdir\*.txt
//
// 
//
int
matchAllToAllTwins(
	vector< char * > &vecNames,
	vector< int > &vecMatched,
	vector< vector<Feature3DInfo> > &vvInputFeats
)
{
	vector<int> vecModelMatches;
	vector<Feature3DInfo> vecFeats2Updated;
	vector<int> vecModelMatchesUpdated;

	TransformSimilarity ts12, ts12Next, ts12Inv, ts31, ts32;

	int bRefine = 1;

	int iImg1 = -1, iImg2 = -1, iImg3 = -1;
	FILE *outfile = fopen("corr_matrix.txt", "wt");
	fclose(outfile);
	outfile = fopen("closest_match.txt", "wt");
	fclose(outfile);
	for (int i = 0; i < vvInputFeats.size(); i++)
	{
		int iMaxIndex = -1;
		int iMaxMatches = 0;

		// Save closest match and score
		vector<int> vecModelMatchesBest;
		vector<float> vecModelMatchesBestScore;
		vecModelMatchesBest.resize(vvInputFeats[i].size());
		vecModelMatchesBestScore.resize(vvInputFeats[i].size());
		for (int j = 0; j < vecModelMatchesBest.size(); j++)
		{
			vecModelMatchesBest[j] = -1;
			vecModelMatchesBestScore[j] = 0;
		}

		for (int j = 0; j < vvInputFeats.size(); j++)
		{
			if (i == j)
			{
				continue;
			}
			iImg1 = i;
			iImg2 = j;

			char *pci1 = vecNames[i];
			char *pci2 = vecNames[j];

			//pci1=0; pci2=0;

			// Match 1->2
			int iInliers1 = MatchKeys(
				vvInputFeats[iImg1],
				vvInputFeats[iImg2], vecModelMatches, -1, pci1, pci2, &vecFeats2Updated, 0, &ts12.m_fScale, ts12.m_ppfRot[0], ts12.m_pfTrans);

			//outfile = fopen( "one-match.txt", "wt" );
			int iMatches;
			iMatches = 0;
			int ppiCounts[64][64];
			memset(ppiCounts, 0, sizeof(ppiCounts));
			for (int g = 0; g < vecModelMatches.size(); g++)
			{
				if (vecModelMatches[g] >= 0)
				{
					iMatches++;
					//fprintf( outfile, "%f\t%f\t%f\t%f\t", vvInputFeats[iImg1][g].x, vvInputFeats[iImg1][g].y, vvInputFeats[iImg1][g].z, vvInputFeats[iImg1][g].scale );
					//for( int h = 0; h < 9; h++ )
					//{
					//	fprintf( outfile, "%f\t", vvInputFeats[iImg1][g].ori[0][h] );
					//}
					//for( int h = 0; h < 3; h++ )
					//{
					//	fprintf( outfile, "%f\t", vvInputFeats[iImg1][g].eigs[h] );
					//}
					//for( int h = 0; h < 64; h++ )
					//{
					//	int iV1 = vvInputFeats[iImg1][g].m_pfPC[h];
					//	fprintf( outfile, "%d\t", iV1 );
					//}
					//fprintf( outfile, "\n" );

					//for( int h = 0; h < 64; h++ )
					//{
					//	int iV1 = vvInputFeats[iImg1][g].m_pfPC[h];
					//	for( int z = 0; z < 64; z++ )
					//	{
					//		int iV2 = vvInputFeats[1][g].m_pfPC[z];
					//		ppiCounts[h][z] += abs( iV1-iV2 );
					//	}
					//}

				}
			}

			//fclose( outfile );

			//outfile = fopen( "match-array.txt", "wt" );
			//for( int h = 0; h < 64; h++ )
			//{
			//	for( int z = 0; z < 64; z++ )
			//	{
			//		fprintf( outfile, "%d\t", ppiCounts[h][z] );
			//	}
			//	fprintf( outfile, "\n" );
			//}
			//fclose( outfile );

			//int iInliers2 = MatchKeys(
			//	vvInputFeats[iImg1],
			//	vecFeats2Updated, vecModelMatchesUpdated, -1, pci1, pci2, 0, 1, &ts12.m_fScale, ts12.m_ppfRot[0], ts12.m_pfTrans );
			//iMatches = 0;
			//for( int g = 0; g < vecModelMatchesUpdated.size(); g++ )
			//{
			//	if( vecModelMatchesUpdated[g] >= 0 )
			//		iMatches++;
			//}

			//iMatches = iInliers1; // Produces about the same result on twins.

			outfile = fopen("corr_matrix.txt", "a+");
			fprintf(outfile, "%d\t", iMatches);
			fclose(outfile);

			if (iImg1 != iImg2)
			{
				if (iMatches > iMaxMatches)
				{
					iMaxMatches = iMatches;
					iMaxIndex = iImg2;
				}

				for (int g = 0; g < vecModelMatches.size(); g++)
				{
					if (vecModelMatches[g] >= 0)
					{
						int iMatch = vecModelMatches[g];
						float fDist = vvInputFeats[iImg1][g].DistSqrPCs(vvInputFeats[iImg2][iMatch]);
						if (vecModelMatchesBest[g] >= 0)
						{
							if (fDist < vecModelMatchesBestScore[g])
							{
								vecModelMatchesBest[g] = iMatch;
								vecModelMatchesBestScore[g] = fDist;
							}
						}
						else
						{
							vecModelMatchesBest[g] = iMatch;
							vecModelMatchesBestScore[g] = fDist;
						}
					}
				}
			}
		}

		// Go through and could the other subject with the most closest matches.
		int iMostClosestMatchesIndex = -1;
		int iMostClosestMatchesCount = 0;
		for (int j = 0; j < vvInputFeats.size(); j++)
		{
			int iClosestMatchesCount = 0;
			for (int g = 0; g < vecModelMatchesBest.size(); g++)
			{
				if (vecModelMatchesBest[g] == j)
				{
					iClosestMatchesCount++;
				}
			}
			if (iClosestMatchesCount > iMostClosestMatchesCount)
			{
				iMostClosestMatchesCount = iClosestMatchesCount;
				iMostClosestMatchesIndex = j;
			}
		}

		// Print out max match count - original metric
		outfile = fopen("corr_matrix.txt", "a+");
		fprintf(outfile, "Max:\t%d\n", iMaxIndex);
		fclose(outfile);

		// Save coordinates for matches


	}
	return 1;
}

int
twinMatches(
	vector< char * > &vecNames,
	vector< vector<Feature3DInfo> > &vvInputFeats
)
{

	int piMatches[] = {
		15	,
		2	,
		1	,
		4	,
		3	,
		17	,
		47	,
		31	,
		9	,
		8	,
		12	,
		25	,
		10	,
		18	,
		44	,
		0	,
		33	,
		5	,
		13	,
		21	,
		28	,
		19	,
		46	,
		24	,
		23	,
		11	,
		27	,
		26	,
		20	,
		37	,
		34	,
		7	,
		40	,
		16	,
		30	,
		41	,
		38	,
		29	,
		36	,
		45	,
		32	,
		35	,
		43	,
		42	,
		14	,
		39	,
		22	,
		6
	};


	vector<int> vecModelMatches;
	vector<Feature3DInfo> vecFeats2Updated;
	vector<int> vecModelMatchesUpdated;

	TransformSimilarity ts12, ts12Next, ts12Inv, ts31, ts32;

	FEATUREIO fioTwin;
	FEATUREIO fioRandom;

	fioTwin.x = fioTwin.y = fioTwin.z = 256;
	fioTwin.t = 1;
	fioTwin.iFeaturesPerVector = 1;
	fioRandom = fioTwin;
	fioAllocate(fioTwin);
	fioAllocate(fioRandom);
	fioSet(fioTwin, 0);
	fioSet(fioRandom, 0);

	int iMatchesRandom = 0;
	int iMatchesTwin = 0;

	int iImg1 = -1, iImg2 = -1, iImg3 = -1;
	for (int i = 0; i < vvInputFeats.size(); i++)
	{
		int iMaxIndex = -1;
		int iMaxMatches = 0;

		iImg1 = i;
		iImg2 = piMatches[i];

		char *pci1 = 0;
		char *pci2 = 0;

		// Matches to twin brain
		int iInliers1 = MatchKeys(
			vvInputFeats[iImg1],
			vvInputFeats[iImg2], vecModelMatches, -1, pci1, pci2, &vecFeats2Updated, 0, &ts12.m_fScale, ts12.m_ppfRot[0], ts12.m_pfTrans);
		int iMatches;

		iMatches = 0;
		for (int g = 0; g < vecModelMatches.size(); g++)
		{
			int iMatchImg2 = vecModelMatches[g];
			if (vecModelMatches[g] >= 0)
			{
				Feature3DInfo &feat1 = vvInputFeats[iImg1][g];
				float *pfVec = fioGetVector(fioTwin, feat1.x, feat1.y, feat1.z);
				*pfVec += 1;

				Feature3DInfo &feat2 = vvInputFeats[iImg2][iMatchImg2];
				pfVec = fioGetVector(fioTwin, feat2.x, feat2.y, feat2.z);
				*pfVec += 1;
				iMatchesTwin++;
			}
		}

		// Matches to random brain
		iImg2 = (rand()*vvInputFeats.size()) / (RAND_MAX + 1.0);
		while (iImg2 == iImg1 || iImg2 == piMatches[iImg1])
		{
			iImg2 = (rand()*vvInputFeats.size()) / (RAND_MAX + 1.0);
		}
		iInliers1 = MatchKeys(
			vvInputFeats[iImg1],
			vvInputFeats[iImg2], vecModelMatches, -1, pci1, pci2, &vecFeats2Updated, 0, &ts12.m_fScale, ts12.m_ppfRot[0], ts12.m_pfTrans);

		iMatches = 0;
		for (int g = 0; g < vecModelMatches.size(); g++)
		{
			if (vecModelMatches[g] >= 0)
			{
				Feature3DInfo &feat = vvInputFeats[iImg1][g];
				float *pfVec = fioGetVector(fioRandom, feat.x, feat.y, feat.z);
				*pfVec += 1;
				iMatchesRandom++;
			}
		}

		// Matches to another random brain
		iImg2 = (rand()*vvInputFeats.size()) / (RAND_MAX + 1.0);
		while (iImg2 == iImg1 || iImg2 == piMatches[iImg1])
		{
			iImg2 = (rand()*vvInputFeats.size()) / (RAND_MAX + 1.0);
		}
		iInliers1 = MatchKeys(
			vvInputFeats[iImg1],
			vvInputFeats[iImg2], vecModelMatches, -1, pci1, pci2, &vecFeats2Updated, 0, &ts12.m_fScale, ts12.m_ppfRot[0], ts12.m_pfTrans);

		iMatches = 0;
		for (int g = 0; g < vecModelMatches.size(); g++)
		{
			if (vecModelMatches[g] >= 0)
			{
				Feature3DInfo &feat = vvInputFeats[iImg1][g];
				float *pfVec = fioGetVector(fioRandom, feat.x, feat.y, feat.z);
				*pfVec += 1;
				iMatchesRandom++;
			}
		}
	}

	fioWrite(fioTwin, "matches_twin");
	fioWrite(fioRandom, "matches_random");
	return 1;
}

int
matchAllToAllNN(
	vector< char * > &vecNames,
	vector< int > &vecMatched,
	vector< vector<Feature3DInfo> > &vvInputFeats
)
{
	vector<int> vecModelMatches;
	vector<Feature3DInfo> vecFeats2Updated;
	vector<int> vecModelMatchesUpdated;

	TransformSimilarity ts12, ts12Next, ts12Inv, ts31, ts32;

	int bRefine = 1;

	int iImg1 = -1, iImg2 = -1, iImg3 = -1;
	FILE *outfile = fopen("corr_matrix.txt", "wt");
	fclose(outfile);
	for (int i = 0; i < vvInputFeats.size(); i++)
	{
		int iMaxIndex = -1;
		int iMaxMatches = 0;
		for (int j = 0; j < vvInputFeats.size(); j++)
		{
			iImg1 = i;
			iImg2 = j;

			char *pci1 = 0;
			char *pci2 = 0;

			// Match 1->2
			int iInliers1 = MatchKeys(
				vvInputFeats[iImg1],
				vvInputFeats[iImg2], vecModelMatches, -1, pci1, pci2, &vecFeats2Updated, 0, &ts12.m_fScale, ts12.m_ppfRot[0], ts12.m_pfTrans);
			int iInliers2 = 0;
			int iInliers3 = 0;
			int iMatches;

			iMatches = 0;
			for (int g = 0; g < vecModelMatches.size(); g++)
			{
				if (vecModelMatches[g] >= 0) iMatches++;
			}

			outfile = fopen("corr_matrix.txt", "a+");
			fprintf(outfile, "%d\t", iMatches);
			fclose(outfile);

			if (iMatches > iMaxMatches)
			{
				if (iImg1 != iImg2)
				{
					iMaxMatches = iMatches;
					iMaxIndex = iImg2;
				}
			}
		}
		outfile = fopen("corr_matrix.txt", "a+");
		fprintf(outfile, "Max: %d\n", iMaxIndex);
		fclose(outfile);
	}
	return 1;
}

int
matchAllToAllNNApproximate(
	vector< char * > &vecNames,
	vector< int > &vecMatched,
	vector< vector<Feature3DInfo> > &vvInputFeats
)
{
	vector<int> vecModelMatches;
	vector<Feature3DInfo> vecFeats2Updated;
	vector<int> vecModelMatchesUpdated;

	TransformSimilarity ts12, ts12Next, ts12Inv, ts31, ts32;

	int bRefine = 1;

	int iImg1 = -1, iImg2 = -1, iImg3 = -1;
	FILE *outfile = fopen("corr_matrix.txt", "wt");
	fclose(outfile);
	for (int i = 0; i < vvInputFeats.size(); i++)
	{
		int iMaxIndex = -1;
		int iMaxMatches = 0;
		for (int j = 0; j < vvInputFeats.size(); j++)
		{
			iImg1 = i;
			iImg2 = j;

			char *pci1 = 0;
			char *pci2 = 0;

			// Match 1->2
			int iInliers1 = MatchKeys(
				vvInputFeats[iImg1],
				vvInputFeats[iImg2], vecModelMatches, -1, pci1, pci2, &vecFeats2Updated, 0, &ts12.m_fScale, ts12.m_ppfRot[0], ts12.m_pfTrans);
			int iInliers2 = 0;
			int iInliers3 = 0;
			int iMatches;

			iMatches = 0;
			for (int g = 0; g < vecModelMatches.size(); g++)
			{
				if (vecModelMatches[g] >= 0) iMatches++;
			}

			outfile = fopen("corr_matrix.txt", "a+");
			fprintf(outfile, "%d\t", iMatches);
			fclose(outfile);

			if (iMatches > iMaxMatches)
			{
				if (iImg1 != iImg2)
				{
					iMaxMatches = iMatches;
					iMaxIndex = iImg2;
				}
			}
		}
		outfile = fopen("corr_matrix.txt", "a+");
		fprintf(outfile, "Max: %d\n", iMaxIndex);
		fclose(outfile);
	}
	return 1;
}




//
// matchAllToOne()
//
// Matches all feature sets to a single feature set.
//
// Twins
// C:\downloads\data\lilla\twin\image\3Daff_congeal_result_tr_fname_3Daff_histo_DiffSubj_ds_1_origres_the_moved_volume_4.key C:\downloads\data\lilla\twin\image\3Daff_congeal_result_tr_fname_3Daff_histo_DiffSubj_ds_1_origres_the_moved_volume_5.key
// C:\downloads\data\MNI\bite-us\group1\05\pre\sweep_5c\5c.3dus.key C:\downloads\data\MNI\bite-us\group1\05\pre\sweep_5b\5b.3dus.key C:\downloads\data\MNI\bite-us\group1\05\pre\sweep_5a\5a.3dus.key 
// 
// -r- C:\downloads\data\MNI\bite-us\group1\02\pre\sweep_2a\2a.3dus.key C:\downloads\data\MNI\bite-us\group1\02\post\sweep_2u\2u.3dus.key
//
int
matchAllToOne(
	vector< char * > &vecNames,
	vector< int > &vecMatched,
	vector< vector<Feature3DInfo> > &vvInputFeats,
	int bExpandedMatching = 0, // Expanded matching, try to find some additional correspondences (inliers of inliers)
	char *pcOutFileName = 0, // Output file name, for results, etc
	int bSequential = 0, // Sequential matching
	int bOutputFeatures = 0 // Visualization
)
{
	vector<int> vecModelMatches;
	vector<Feature3DInfo> vecFeats2Updated;
	vector<int> vecModelMatchesUpdated;

	TransformSimilarity ts12, ts12Next, ts12Inv, ts31, ts32;

	int iImg1 = -1, iImg2 = -1, iImg3 = -1;

	int bRefine = 0;

	// Model features for inspection, RIRE 39
	int piFeatsToMatch[] = { 7,	129,	1142,	11,	12,	25,	29,	299,	1086,	905,	1266,	304,	2,	69,	76,	88,	89,	93,	221,	341,	1081,	1149,	1357,	0 };
	int iFeatsToMatchSize = sizeof(piFeatsToMatch) / sizeof(int);
	vector<int> vecFeatsToMatchFound;
	vecFeatsToMatchFound.resize(vecNames.size(), 0);
	vector<int> vecFeatsToMatchFreq;
	vecFeatsToMatchFreq.resize(vvInputFeats[0].size(), 0);

	// Keep track of matches - ahh, do this in learning ... 
	int iImage0Feats = vvInputFeats[0].size();
	unsigned char *pucMatches = new unsigned char[iImage0Feats*vvInputFeats.size()];
	memset(pucMatches, 0, vvInputFeats[0].size()*vvInputFeats.size());

	//while( iImg1 == -1 )
	//{
	//	iImg1 = (rand()*iFeatVec) / RAND_MAX;
	//}
	iImg1 = 0;
	for (int i = 0; i < vvInputFeats.size(); i++)
	{
		if (i == iImg1)
		{
			continue;
		}

		if (bSequential)
		{
			// Match in sequence

			iImg1 = i;
			iImg2 = i + 1;
			if (i + 1 == vvInputFeats.size())
			{
				iImg2 = 0; // wrap around? for cardiac data
						   //continue;
			}
		}
		else
		{
			iImg2 = i;
		}

		//iImg2 = 0;
		//iImg1 = i;

		char pcImg1[400], pcImg2[400];
		sprintf(pcImg1, "%s", vecNames[iImg1]);
		sprintf(pcImg2, "%s", vecNames[iImg2]);
		char *pch;

		pch = strrchr(pcImg1, '.');
		//pch[1] = 'i';pch[2] = 'm';pch[3] = 'g';
		//sprintf( pch, ".nii.gz" );
		sprintf(pch, ".hdr");

		pch = strrchr(pcImg2, '.');
		//pch[1] = 'i';pch[2] = 'm';pch[3] = 'g';
		//sprintf( pch, ".nii.gz" );
		sprintf(pch, ".hdr");

		char *pci1 = 0;
		char *pci2 = 0;

		if (bOutputFeatures)
		{
			pci1 = pcImg1; pci2 = pcImg2;
		}

		// Match 1->2
		int iInliers1 = MatchKeys(vvInputFeats[iImg1], vvInputFeats[iImg2], vecModelMatches,
			-1, pci1, pci2, &vecFeats2Updated, bExpandedMatching, &ts12.m_fScale, ts12.m_ppfRot[0], ts12.m_pfTrans,
			0);
		//bExpandedMatching );
		int iInliers2 = 0;
		int iInliers3 = 0;
		int iMatches;

		// This statement is for short circuiting
		// printf( "Inliers: %d\n", iInliers1 ); continue;

		iMatches = 0;
		for (int g = 0; g < vecModelMatches.size(); g++)
		{
			if (vecModelMatches[g] >= 0)
			{
				iMatches++;
				if (pucMatches)
				{
					pucMatches[iImg2*iImage0Feats + g] = vecModelMatches[g];
				}

				// Evaluate coverage for set of ground truth matches
				for (int j = 0; j < iFeatsToMatchSize; j++)
				{
					if (g == piFeatsToMatch[j])
					{
						vecFeatsToMatchFound[iImg2]++;
					}
				}

				if (!bSequential)	vecFeatsToMatchFreq[g]++;
			}
		}

		char pcFileName[400];
		FILE *outfile;
		FILE *outInfoFile;

		//
		// This is old output
		//
		//sprintf( pcFileName, "%s.matches.txt", vecNames[i] );
		//outfile = fopen( pcFileName, "wt" );
		//pci1 = strrchr( pcImg1, '\\' ) + 1;
		//pci2 = strrchr( pcImg2, '\\' ) + 1;
		//fprintf( outfile, "# Img1: %s\n", pcImg1 );
		//fprintf( outfile, "# Img2: %s\n", pcImg2 );
		//fprintf( outfile, "# Matches: %d\n", iMatches );
		//fprintf( outfile, "# Format: x1 y1 z1 s1 x2 y2 z2 s2 SquaredDifference\n" );
		//for( int g = 0; g < vecModelMatches.size(); g++ )
		//{
		//	if( vecModelMatches[g] >= 0 )
		//	{
		//		Feature3DInfo &f1 = vvInputFeats[iImg1][g];
		//		Feature3DInfo &f2 = vvInputFeats[iImg2][ vecModelMatches[g] ];
		//		float fDistSqr = f1.DistSqrPCs( f2 );
		//		fprintf( outfile, "%f\t%f\t%f\t%f\t", f1.x, f1.y, f1.z, f1.scale);
		//		fprintf( outfile, "%f\t%f\t%f\t%f\t", f2.x, f2.y, f2.z, f2.scale);
		//		fprintf( outfile, "%f\n", fDistSqr );
		//	}
		//}
		//fclose( outfile );

		sprintf(pcFileName, "%s.matches.info.txt", vecNames[i]);
		outInfoFile = fopen(pcFileName, "wt");

		sprintf(pcFileName, "%s.matches.img1.txt", vecNames[i]);
		outfile = fopen(pcFileName, "wt");
		fprintf(outfile, "# Img1: %s\n", pcImg1);
		fprintf(outfile, "# Img2: %s\n", pcImg2);
		fprintf(outfile, "# Matches: %d\n", iMatches);
		fprintf(outfile, "# Format: Img1 x1 y1 z1 s1 MatchIndexImg2 DistSqr\n");
		int iCurrMatch = 0;
		for (int g = 0; g < vecModelMatches.size(); g++)
		{
			if (vecModelMatches[g] >= 0)
			{
				Feature3DInfo &f1 = vvInputFeats[iImg1][g];
				Feature3DInfo &f2 = vvInputFeats[iImg2][vecModelMatches[g]];
				float fDistSqr = f1.DistSqrPCs(f2);
				fprintf(outInfoFile, "%d\t%d\n", f1.m_uiInfo, f2.m_uiInfo);

				//fprintf( outfile, "%s\t%f\t%f\t%f\t%f\timg2_match%4.4d_feat%6.6d\t%f\n", vecNames[iImg1], f1.x, f1.y, f1.z, f1.scale, iCurrMatch, vecModelMatches[g], fDistSqr ); 
				fprintf(outfile, "%s\t%f\t%f\t%f\t%f\timg2_match%4.4d_feat%6.6d\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n", vecNames[iImg1], f1.x, f1.y, f1.z, f1.scale, iCurrMatch, vecModelMatches[g], fDistSqr,
					f1.ori[0][0], f1.ori[0][1], f1.ori[0][2],
					f1.ori[1][0], f1.ori[1][1], f1.ori[1][2],
					f1.ori[2][0], f1.ori[2][1], f1.ori[2][2]
				);
				iCurrMatch++;
			}
		}
		fclose(outfile);
		fclose(outInfoFile);

		sprintf(pcFileName, "%s.matches.img2.txt", vecNames[i]);
		outfile = fopen(pcFileName, "wt");
		fprintf(outfile, "# Img1: %s\n", pcImg1);
		fprintf(outfile, "# Img2: %s\n", pcImg2);
		fprintf(outfile, "# Matches: %d\n", iMatches);
		fprintf(outfile, "# Format: Img2 x2 y2 z2 s2 MatchIndexImg1 DistSqr\n");
		iCurrMatch = 0;
		for (int g = 0; g < vecModelMatches.size(); g++)
		{
			if (vecModelMatches[g] >= 0)
			{
				Feature3DInfo &f1 = vvInputFeats[iImg1][g];
				Feature3DInfo &f2 = vvInputFeats[iImg2][vecModelMatches[g]];
				float fDistSqr = f1.DistSqrPCs(f2);
				//fprintf( outfile, "%s\t%f\t%f\t%f\t%f\timg1_match%4.4d_feat%6.6d\t%f\n", vecNames[iImg2], f2.x, f2.y, f2.z, f2.scale, iCurrMatch, g, fDistSqr );
				fprintf(outfile, "%s\t%f\t%f\t%f\t%f\timg2_match%4.4d_feat%6.6d\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n", vecNames[iImg2], f2.x, f2.y, f2.z, f2.scale, iCurrMatch, g, fDistSqr,
					f2.ori[0][0], f2.ori[0][1], f2.ori[0][2],
					f2.ori[1][0], f2.ori[1][1], f2.ori[1][2],
					f2.ori[2][0], f2.ori[2][1], f2.ori[2][2]
				);

				iCurrMatch++;
			}
		}
		fclose(outfile);

		if (bRefine)
		{
			pch = strrchr(vecNames[iImg2], '\\');
			if (pch == 0) pch = vecNames[iImg2]; else pch++;

			pci1 = 0; pci2 = 0;
			iInliers2 = MatchKeys(vvInputFeats[iImg1], vecFeats2Updated, vecModelMatchesUpdated, -1, 0, 0, 0, 1, &ts12Next.m_fScale, ts12Next.m_ppfRot[0], ts12Next.m_pfTrans);

			iMatches = 0;
			for (int g = 0; g < vecModelMatchesUpdated.size(); g++)
			{
				if (vecModelMatchesUpdated[g] >= 0) iMatches++;

			}
			//pci1=pcImg1; pci2=pcImg2;
			iInliers3 = DetermineTransform(vvInputFeats[iImg1], vvInputFeats[iImg2], vecModelMatchesUpdated, &ts12.m_fScale, ts12.m_ppfRot[0], ts12.m_pfTrans, pci1, pci2);

			iMatches = 0;
			for (int g = 0; g < vecModelMatchesUpdated.size(); g++)
			{
				if (vecModelMatchesUpdated[g] >= 0) iMatches++;
			}

			sprintf(pcFileName, "%s.matches.txt", vecNames[i]);
			outfile = fopen(pcFileName, "wt");
			pci1 = strrchr(pcImg1, '\\') + 1;
			pci2 = strrchr(pcImg2, '\\') + 1;
			fprintf(outfile, "Img1: %s\n", pci1);
			fprintf(outfile, "Img2: %s\n", pci2);
			fprintf(outfile, "Matches: %d\n", iMatches);
			fprintf(outfile, "Format: x1 y1 z1 s1 x2 y2 z2 s2 SquaredDifference\n");
			for (int g = 0; g < vecModelMatchesUpdated.size(); g++)
			{
				if (vecModelMatchesUpdated[g] >= 0)
				{
					Feature3DInfo &f1 = vvInputFeats[iImg1][g];
					Feature3DInfo &f2 = vvInputFeats[iImg2][vecModelMatchesUpdated[g]];
					float fDistSqr = f1.DistSqrPCs(f2);
					fprintf(outfile, "%f\t%f\t%f\t", f1.x, f1.y, f1.z, f1.scale);
					fprintf(outfile, "%f\t%f\t%f\t", f2.x, f2.y, f2.z, f2.scale);
					fprintf(outfile, "%f\n", fDistSqr);
				}
			}
			fclose(outfile);
		}

		sprintf(pcFileName, "%s.trans.txt", vecNames[i]);
		ts12.WriteMatrix(pcFileName);

		ts12Inv = ts12;
		ts12Inv.Invert();
		sprintf(pcFileName, "%s.trans-inverse.txt", vecNames[i]);
		ts12Inv.WriteMatrix(pcFileName);

		//ts12.Multiply( ts12Inv );
		//sprintf( pcFileName, "%s.zero.txt", vecNames[i] );
		//ts12.WriteMatrix( pcFileName );
		printf("%s: inliers %d\t%d\t%d\t%f\n", vecNames[i],
			iInliers1, iInliers2, iInliers3, ts12.m_fScale);

		if (pcOutFileName)
		{
			//FILE *outfileR = fopen( "report.txt", "a+" );
			FILE *outfileR = fopen(pcOutFileName, "a+");
			fprintf(outfileR, "%s:\tinliers\t%d\t%d\t%d\t%f\t%f\t%f\t%f\n", vecNames[i],
				iInliers1, iInliers2, iInliers3, ts12.m_fScale, ts12.m_pfTrans[0], ts12.m_pfTrans[1], ts12.m_pfTrans[2]);
			fclose(outfileR);
		}

		// Transform features in image 2 to image 1 space, output aligned features
		for (int g = 0; g < vvInputFeats[iImg2].size(); g++)
		{
			float pfZero[3] = { 0,0,0 };
			vvInputFeats[iImg2][g].SimilarityTransform(pfZero, ts12.m_pfTrans, ts12.m_ppfRot[0], ts12.m_fScale);
		}
		sprintf(pcFileName, "%s.update.key", vecNames[i]);
		msFeature3DVectorOutputText(vvInputFeats[iImg2], pcFileName);
	}

	printf("\n");

	return 1;

	// Print out feature 
	FILE *outfile = fopen("group-wise-matches.txt", "wt");
	for (int j = 0; j < vecNames.size(); j++)
	{
		fprintf(outfile, "\t%s", vecNames[j]);
	}
	fprintf(outfile, "\n");

	for (int i = 0; i < iImage0Feats; i++)
	{
		for (int j = 0; j < vecNames.size(); j++)
		{
			fprintf(outfile, "\t%d", pucMatches[iImage0Feats*j + i]);
		}
		fprintf(outfile, "\n");
	}
	fclose(outfile);

	outfile = fopen("rire39-coverage.txt", "wt");
	for (int j = 0; j < vecFeatsToMatchFound.size(); j++)
	{
		fprintf(outfile, "%d\t%d\n", j, vecFeatsToMatchFound[j]);
	}
	fclose(outfile);

	outfile = fopen("rire39-frequency.txt", "wt");
	for (int j = 0; j < vecFeatsToMatchFreq.size(); j++)
	{
		fprintf(outfile, "%d\t%d\n", j, vecFeatsToMatchFreq[j]);
	}
	fclose(outfile);



	return 1;

}

//
// matchAllToOne()
//
// Matches all feature sets to a single feature set.
//
// Twins
// C:\downloads\data\lilla\twin\image\3Daff_congeal_result_tr_fname_3Daff_histo_DiffSubj_ds_1_origres_the_moved_volume_4.key C:\downloads\data\lilla\twin\image\3Daff_congeal_result_tr_fname_3Daff_histo_DiffSubj_ds_1_origres_the_moved_volume_5.key
//
int
matchAllToOneFiles(
	int iFiles,
	char **ppcFileNames,
	int iFilesToProcess
)
{
	if (iFiles < iFilesToProcess)
	{
		iFilesToProcess = iFiles;
	}

	vector< char * > vecNames;
	vector< int > vecMatched;
	vector< vector<Feature3DInfo> > vvInputFeats;

	vecNames.resize(iFilesToProcess);
	vvInputFeats.resize(iFilesToProcess);
	vecMatched.resize(iFilesToProcess, -1);

	int iTotalFeats = 0;
	int iFeatVec = 0;

	for (int i = 0; i < iFiles; i++)
	{
		char pcImg1[400];
		sprintf(pcImg1, "%s", ppcFileNames[i]);
		char *pch = strrchr(pcImg1, '\\');
		if (pch) pch++; else pch = pcImg1;
		printf("Reading file %d: %s...", i, pch);

		vvInputFeats[iFeatVec].clear();

		if (msFeature3DVectorInputBin(vvInputFeats[iFeatVec], ppcFileNames[i], 140) < 0)
			//if( msFeature3DVectorInputText( vvInputFeats[iFeatVec], argv[i], 200 ) < 0 )
		{
			printf("Error: could not open feature file %d: %s\n", i, ppcFileNames[i]);
			//return -1;
			continue;
		}
		removeNonReorientedFeatures(vvInputFeats[iFeatVec]);
		//removeReorientedFeatures( vvInputFeats[iFeatVec] );
		iTotalFeats += vvInputFeats[iFeatVec].size();
		printf("feats: %d, total: %d\n", vvInputFeats[iFeatVec].size(), iTotalFeats);
		vecNames[iFeatVec] = ppcFileNames[i];
		iFeatVec++;

		if (iFeatVec >= iFilesToProcess)
		{
			// Process
			matchAllToOne(vecNames, vecMatched, vvInputFeats);
			// Leave first subject at the head of the list
			iFeatVec = 1; // Re
		}
	}

	if (iFeatVec > 1)
	{
		vecNames.resize(iFeatVec);
		vvInputFeats.resize(iFeatVec);
		vecMatched.resize(iFeatVec, -1);
		matchAllToOne(vecNames, vecMatched, vvInputFeats);
	}

	return 1;
}

int
test_nn(
)
{
	vector<Feature3DInfo> v1;
	vector<Feature3DInfo> v2;
	msFeature3DVectorInputText(v1, "C:\\downloads\\data\\oasis\\example\\OAS1_0002_MR1_mpr_n4_anon_111_t88_gfc.key", 140);
	msFeature3DVectorInputText(v2, "C:\\downloads\\data\\oasis\\example\\OAS1_0003_MR1_mpr_n4_anon_111_t88_gfc.key", 140);

	vector<int> vecMatchIndex21;
	vector<float> vecMatchDist21;

	msComputeNearestNeighborDistanceRatioInfo(v2, v1, vecMatchIndex21, vecMatchDist21);

	FILE *outfile = fopen("result.txt", "wt");
	for (int i = 0; i < vecMatchIndex21.size(); i++)
	{
		fprintf(outfile, "%d\t%d\t%f\n", i, vecMatchIndex21[i], vecMatchDist21[i]);
	}
	fclose(outfile);

	return 1;
}

//
//  output_body_feats()
//
//  quick hack to visualize some features.
//
int
output_body_feats(
	vector< Feature3DInfo > &vecFeats,
	char *pcFeatFile1
)
{
	FEATUREIO fio1;
	char pcFileName[400];
	char *pch;
	if (pcFeatFile1)
	{
		strncpy(pcFileName, pcFeatFile1, sizeof(pcFileName));
		pch = strrchr(pcFileName, '.');
		sprintf(pch, ".hdr");
		if (fioReadNifti(fio1, pcFileName) < 0)
		{
			sprintf(pch, ".nii");
			if (fioReadNifti(fio1, pcFileName) < 0)
			{
				memset(&fio1, 0, sizeof(fio1));
			}
		}
	}

	int piFeatList[] = { 1062,2131,3383 };
	for (int i = 0; i < sizeof(piFeatList) / sizeof(int); i++)
	{
		Feature3DInfo &featModel = vecFeats[piFeatList[i]];
		sprintf(pcFileName, "feat_%3.3d_mod%4.4d", i, piFeatList[i]);
		featModel.OutputVisualFeatureInVolume(fio1, pcFileName, 1, 1);
	}
	return 1;
}


//
// msExpandMultiModalFlipPeak()
//
// Invert feature intensity profile depending on wether it is a DoG min or max.
// If it is a DoG max, change to min.
//
int
msExpandMultiModalFlipPeak(
	vector<Feature3DInfo> &vecFeats
)
{
	int iFlipCount = 0;
	for (int iFeat = 0; iFeat < vecFeats.size(); iFeat++)
	{
		Feature3DInfo &feat1 = vecFeats[iFeat];
		if (feat1.m_uiInfo & INFO_FLAG_MIN0MAX1)
		{
			msInvertedIntensity(feat1);
			iFlipCount++;
		}
	}

	return iFlipCount;
}

//
// msExpandMirror()
//
// Expand a feature vector to include mirrored features.
// This could be useful in modeling approximately symmetric objects: brain, lungs, etc.
//
int
msExpandMirror(
	vector<Feature3DInfo> &vecFeats
)
{
	int iSize = vecFeats.size();
	for (int iFeat = 0; iFeat < iSize; iFeat++)
	{
		Feature3DInfo &feat1 = vecFeats[iFeat];
		Feature3DInfo feat2;
		feat2 = feat1;
		msMirrorYFeaturesGradientOrientationHistogram(feat2);
		vecFeats.push_back(feat2);
	}

	return 1;
}

//
// msScrambleGeometry()
// 
// Randomly scramble the geometry, to test NN hashing based on geometry.
//
int
msScrambleGeometry(
	vector<Feature3DInfo> &vecFeats
)
{
	int iSize = vecFeats.size();
	for (int iFeat = 0; iFeat < iSize; iFeat++)
	{
		Feature3DInfo &feat1 = vecFeats[iFeat];
		feat1.x += 1000 * (1.0 - (rand() / (RAND_MAX + 1.0)));
		feat1.y += 1000 * (1.0 - (rand() / (RAND_MAX + 1.0)));
		feat1.z += 1000 * (1.0 - (rand() / (RAND_MAX + 1.0)));
	}

	return 1;
}


//
// C:\downloads\data\oasis\example\OAS1_0002_MR1_mpr_n4_anon_111_t88_gfc.key C:\downloads\data\oasis\example\OAS1_0003_MR1_mpr_n4_anon_111_t88_gfc.key
// C:\downloads\data\oasis\example\OAS1_0002_MR1_mpr_n4_anon_111_t88_gfc.key C:\downloads\data\oasis\example\tp1_rs256.key out.txt
//
// Christian/Greg Sharp
// C:\downloads\data\christian-head-ct\head-neck\ct1-1rig-crop.key C:\downloads\data\christian-head-ct\head-neck\ct2-2rig-crop.key
// 
// Aligns 
//
// C:\downloads\data\oasis\example\OAS1_0002_MR1_mpr_n4_anon_111_t88_gfc.key C:\downloads\data\oasis\example\tp1_rs256.key out.txt
//
// Styner
// C:\downloads\data\styner\test_data\11053_0816_combined_result.txt  C:\downloads\data\styner\test_data\11048_092309_combined_result.txt 
// C:\downloads\data\styner\combined_feature\normal\11053_0816_combined_result.key C:\downloads\data\styner\combined_feature\enlarged\11048_092309_combined_result.key out.txt
// C:\downloads\data\styner\combined_feature\normal\11063_082009_combined_result.key C:\downloads\data\styner\combined_feature\enlarged\11066_082310_combined_result.key
// C:\downloads\data\styner\group\normal\*.key C:\downloads\data\styner\group\krabbe\*.key
// DOH!!! Omit features with poor localizability, registration gets way better.
// C:\downloads\data\styner\group\styner-dwi_normal_140.Top1000.txt C:\downloads\data\styner\group\krabbe\11063*result.key
// C:\downloads\data\styner\group\normal\neo-0011-2-1-1year_dwi_35_new_combined_result.key C:\downloads\data\styner\group\krabbe\*result.key
// C:\downloads\data\styner\group\styner-dwi_normal_140.Top1000.txt C:\downloads\data\styner\group\krabbe\11063*result.key
// C:\downloads\data\styner\group\styner-dwi_normal_140.Top1000.txt C:\downloads\data\styner\11067_large\11067_012511_new_combined_result.key
// C:\downloads\data\styner\group\normal\neo-0011-2-1-1year_dwi_35_new_combined_result.key C:\downloads\data\styner\11067_large\11067_012511_new_combined_result.key
// C:\downloads\data\styner\group\normal\neo-0011-2-1-1year_dwi_35_new_combined_result.key C:\downloads\data\styner\11067_large\11067_012511_new_combined_result.key
// C:\downloads\data\styner\group\normal_datasets_fa\neo-0011-2-1-1year_dwi_35_QCed_FA.key C:\downloads\data\styner\11067_large\11067_012511_DWI_QCed_FA.key
//
// Styner - major deformation
// C:\downloads\data\styner\group\normal_datasets_fa\neo-0011-2-1-1year_dwi_35_QCed_FA.key C:\downloads\data\styner\11067_large\11067_012511_DWI_QCed_FA.key
//
// Lilla - Infant
// C:\downloads\data\lilla\features\deface_1458_V1_t1w.conf.feat.txt C:\downloads\data\lilla\features\deface_1466_V1_t1w.conf.feat.txt
// C:\downloads\data\lilla\features\deface*.txt 
// C:\downloads\data\lilla\features\deface_143*_t1w.conf.feat.txt
// C:\downloads\data\lilla\features\baby_features2.txt C:\downloads\data\lilla\features\deface*.txt 
// C:\downloads\data\lilla\non-aligned-skull-stripped-features\features\masked_deface_1001_V1_t1w.conf.feat.txt C:\downloads\data\lilla\non-aligned-skull-stripped-features\features\no_good\extra\*.txt
// C:\downloads\data\lilla\non-aligned-non-stripped-features\features\*.txt
//
// C:\downloads\data\lilla\non-aligned-non-stripped-features\features\deface_1001_V1_t1w.conf.feat.txt C:\downloads\data\lilla\non-aligned-non-stripped-features\features\deface_1363_V2_t1w.conf.feat.txt 
// C:\downloads\data\lilla\non-aligned-skull-stripped-features\features\masked_deface_1001_V1_t1w.conf.feat.txt C:\downloads\data\lilla\non-aligned-skull-stripped-features\features\no_good\extra\*.txt
//
// UCL
// C:\downloads\data\UCL\image\templateT1.008.key C:\downloads\data\UCL\image\templateT1.000.key C:\downloads\data\UCL\image\templateT1.001.key C:\downloads\data\UCL\image\templateT1.002.key C:\downloads\data\UCL\image\templateT1.003.key C:\downloads\data\UCL\image\templateT1.004.key C:\downloads\data\UCL\image\templateT1.005.key C:\downloads\data\UCL\image\templateT1.006.key C:\downloads\data\UCL\image\templateT1.007.key C:\downloads\data\UCL\image\templateT1.009.key C:\downloads\data\UCL\image\templateT1.010.key C:\downloads\data\UCL\image\templateT1.011.key C:\downloads\data\UCL\image\templateT1.012.key C:\downloads\data\UCL\image\templateT1.013.key C:\downloads\data\UCL\image\templateT1.014.key C:\downloads\data\UCL\image\templateT1.015.key C:\downloads\data\UCL\image\templateT1.016.key
//
//
// Colonography - Interative Group-wise
// C:\downloads\data\liver\CT_Colonography\slicer-reg\*.key
// C:\downloads\data\liver\CT_Colonography\slicer-reg\iteration0\colon.Top1000.txt C:\downloads\data\liver\CT_Colonography\slicer-reg\*.key
// C:\downloads\data\liver\CT_Colonography\slicer-reg\iteration3\colon3.Top1000.txt C:\downloads\data\liver\CT_Colonography\slicer-reg\*.key
// C:\downloads\data\liver\CT_Colonography\slicer-reg\iteration3\colon3.Top1000.txt C:\downloads\data\liver\CT_Colonography\slicer-reg\0018*.key
//
//
// Andry Prostate
// C:\downloads\data\andrey\image\sst2ax-pre.key C:\downloads\data\andrey\image\st2ax-intra.key
//
// Lilla twins
// C:\downloads\data\lilla\twin\featdir\*.txt
//
// C:\downloads\data\bunch\image\ssANON0084_IMAGE.key C:\downloads\data\bunch\image\ssliverCT_s2c.key
// C:\downloads\data\bunch\image\sss3c.key C:\downloads\data\RIRE\back\test_patient_002\ct\simage-ct-out.keys C:\downloads\data\RIRE\back\test_patient_002\ssimage-CT-out.keys
//
// C:\downloads\data\RIRE\image\patient_105_mr_MP-RAGE_iso.key 
//
// C:\downloads\data\RIRE\image\pass17\rire17.Top1000.txt C:\downloads\data\RIRE\image\*.key
// C:\downloads\data\RIRE\image\pass20\rire20.Top1000.txt C:\downloads\data\RIRE\image\*.key
// C:\downloads\data\bunch\image\sss3c.key C:\downloads\data\RIRE\image\pass20\rire20.Top1000.txt 
// C:\downloads\data\RIRE\image\modal-uni\pass39\pass39.Top1000.txt C:\downloads\data\RIRE\image\*.key
//
// -m C:\downloads\data\RIRE\image\patient_105_mr_T1_iso.key C:\downloads\data\RIRE\image\patient_102_mr_T2_iso.key
// -m C:\downloads\data\tina\Gyn\image\*.key
//
// -m C:\downloads\data\tina\Gyn\image\sCT.key C:\downloads\data\tina\Gyn\image\ssMR.key 
// -m C:\downloads\data\tina\Gyn\image\sCT.key C:\downloads\data\tina\Gyn\image\ssMR.key 
// C:\downloads\data\tina\cases\cases\Case31_many_rounds_\*.key
//
// For aligning all training subjects to atlas
// C:\downloads\data\Gurman\training\Match03\GurmanLung03.Top1000.txt C:\downloads\data\Gurman\training\_Original\TrainingFeatures*.txt
// C:\downloads\data\Gurman\training\Match03\GurmanLung03.Top1000.txt C:\downloads\data\Gurman\testing\TrainingFeatures*.txt
//
// For aligning all training subjects to testing subject
// C:\downloads\data\Gurman\testing\testFeatures10.txt C:\downloads\data\Gurman\training\_Original\TrainingFeatures??.txt
//
// Gurman lung
// C:\downloads\data\Gurman\revisualization\train-mean150-all\trainingFeatures??.150.2.txt
// C:\downloads\data\Gurman\revisualization\train-mean150-all\Gurman100.150.2.1.Top1000.txt C:\downloads\data\Gurman\revisualization\testing\testFeatures??.txt 
// C:\downloads\data\Gurman\revisualization\testing\testFeatures00.txt
// C:\downloads\data\Gurman\revisualization\testing\testingFeatures\testFeatures??.txt 
//
//  C:\downloads\data\oasis\example\OAS1_0002_MR1_mpr_n4_anon_111_t88_gfc.key C:\downloads\data\oasis\example\OAS1_0003_MR1_mpr_n4_anon_111_t88_gfc.key
//
// 3D ultrasound
// C:\downloads\data\MNI\bite-us\group1\04\pre\sweep_4b\4b.3dus.key C:\downloads\data\MNI\bite-us\group1\05\pre\sweep_5c\5c.3dus.key
//
//
//  Testing some match stuff, October 2013
//    To handle multi-modality, flipping all features that are peaks appears to be the way to go. 
//	  It is simple, and seemingly does not affect regular modality matching.
//	  This could be added as the default feature encoding ... 
//
//  Other tests of extraction
// ..\featExtract\fart-world-half.key ..\featExtract\fart-iso-.key 
// C:\downloads\data\RIRE\image\original-anisotropic\patient_101_mr_MP-RAGE.key C:\downloads\data\RIRE\image\original-anisotropic\patient_101_mr_T2.key  C:\downloads\data\RIRE\image\original-anisotropic\patient_101_mr_T1.key 
// C:\downloads\data\RIRE\image\patient_101_mr_MP-RAGE_iso.key C:\downloads\data\RIRE\image\patient_101_mr_T2_iso.key C:\downloads\data\RIRE\image\patient_101_mr_T1_iso.key 
// C:\downloads\data\RIRE\image\original-anisotropic\patient_101_mr_T2.key C:\downloads\data\RIRE\image\original-anisotropic\patient_101_mr_MP-RAGE.key 
//
// Few matches (25), something is wrong with extraction (world coordinates?): C:\downloads\data\RIRE\tests\extract-original\patient_101_mr_MP-RAGE.key  C:\downloads\data\RIRE\tests\extract-rescaled\patient_101_mr_MP-RAGE_iso.key
// Many matches(300), with extraction and isotropic voxel coordinates: C:\downloads\data\RIRE\tests\extract-original\patient_101_ct.key  C:\downloads\data\RIRE\tests\extract-rescaled\patient_101_ct_iso.key
// Decent numbers of matches for intra-subject: C:\downloads\data\RIRE\tests\extract-original\patient_101_mr_T1.key C:\downloads\data\RIRE\tests\extract-original\patient_101_mr_T2.key C:\downloads\data\RIRE\tests\extract-original\patient_101_mr_MP-RAGE.key C:\downloads\data\RIRE\tests\extract-original\patient_101_ct.key 
// Many matches (300), something was wrong with nifti world coordinates: C:\downloads\data\RIRE\tests\extract-original\patient_101_mr_MP-RAGE.key  C:\downloads\data\RIRE\tests\extract-rescaled\patient_101_mr_MP-RAGE_iso.key
//
//
//
//	Expanded Hough transform
// C:\downloads\data\james\images\ss10077P_INSP_STD_NJC_COPD.key    C:\downloads\data\james\images\ss10077P_EXP_STD_NJC_COPD.key
// C:\downloads\data\oasis\example\OAS1_0003_MR1_mpr_n4_anon_111_t88_gfc.key  C:\downloads\data\oasis\example\OAS1_0002_MR1_mpr_n4_anon_111_t88_gfc.key
// C:\downloads\data\andriy\image\sst2ax-pre.key  C:\downloads\data\andriy\image\st2ax-intra.key
//
//
// -r- C:\downloads\data\MNI\bite-us\group1\12\test\model\model12b.txt.Top1000.txt  C:\downloads\data\MNI\bite-us\group1\12\post\sweep_12w\12w.3dus.key
// -r- C:\downloads\data\MNI\bite-us\group1\12\pre\sweep_12b\12b.3dus.key  C:\downloads\data\MNI\bite-us\group1\12\post\sweep_12w\12w.3dus.key
// -r- C:\downloads\data\tassilo\ID5_WaterProbablity.key  C:\downloads\data\tassilo\WaterMap3D_rr.key
// -r- C:\downloads\data\tassilo\TrackedImageSequence_CaptureDevice_20140121_100909-watermap.RFC-LOG.key C:\downloads\data\tassilo\TrackedImageSequence_CaptureDevice_20140121_113949-watermap.RFC-LOG.key
//
//
// -r- C:\downloads\data\Christian\ADNI_features_world\subset\1343_world.key  C:\downloads\data\Christian\ADNI_features_world\subset\1380_m6_world.key C:\downloads\data\Christian\ADNI_features_world\subset\1380_m18_world.key C:\downloads\data\Christian\ADNI_features_world\subset\1380_world.key
// -r- C:\downloads\data\Christian\ADNI_features_world\subset\1343_world.key  C:\downloads\data\Christian\ADNI_features_world\subset\*update.key
//
//  -r- -t 2 -f C:\downloads\data\Christian\visceral\name.txt
// -r- -t 2 C:\downloads\data\Christian\visceral\*CTce_ThAb.txt
//
int
//main_matchingIPMI_(
main(
	int argc,
	char **argv
)
{
	//matchBunchToBunch( "C:\\downloads\\data\\lilla\\Objective1\\doc\\obj1-tw1-resampled-labels.txt" );
	//matchBunchToBunch( "C:\\downloads\\data\\lilla\\twin\\doc\\twins-to-pair-labels-test.txt" );
	//return 0;

	//
	//int count = 0;
	//while( count < 5 )
	//{
	//	printf( "Florence!\n" );
	//	count = count + 1;
	//}

	if (argc < 3)
	{
		printf("Volumetric Feature matching v1.1\n");
		printf("Determines robust alignment solution mapping coordinates in image 2, 3, ... to image 1.\n");
		printf("Usage: %s [options] <input keys 1> <input keys 2> ... \n", "featMatchMultiple");
		printf("  <input keys 1, ...>: input key files, produced from featExtract.\n");
		printf("  <output transform>: output text file with linear transform from keys 2 -> keys 1.\n");
		return -1;
	}
	//test_nn();
	int iArg = 1;
	int bMultiModal = 0;
	int bExpandedMatching = 0;
	int bOnlyReorientedFeatures = 1;
	char *pcOutputFileName = "report.txt";
	char *pcInputFileList = 0;
	char *pcLabelFile = 0;
	int iMatchType = 1; // (1,2,3) -> (many->one, many->many, sequential)
	int bViewFeatures = 0;
	int iNeighbors = 5; //
	float fGeometryWeight = -1; // Weight of feature geometry in NN matching, 0 by default 
	float iImgSplit = -1; // Fraction of data to consider (for variable sized subsets, IPMI 2015 
	while (iArg < argc && argv[iArg][0] == '-')
	{
		switch (argv[iArg][1])
		{
			// Double initial image resolution option

		case 'm': case 'M':
			// Option to extract multi-modal features
			bMultiModal = 1;
			iArg++;
			break;

		case 'e': case 'E':
			// Option to perform expanded Hough transform
			// Results in more inliers for lung images
			bExpandedMatching = 1;
			iArg++;
			break;

		case 'v': case 'V':
			// Option to output visually features - note image must exist with features
			bViewFeatures = 1;
			iArg++;
			break;

		case 'o': case 'O':
			// General output file name
			iArg++;
			pcOutputFileName = argv[iArg];
			iArg++;
			break;

		case 'r': case 'R':
			// Option to not use reoriented features - for volumes that are already aligned
			bOnlyReorientedFeatures = 1;
			if (argv[iArg][2] == '-')
			{
				// Only 
				bOnlyReorientedFeatures = 0;
			}
			iArg++;
			break;

		case 't': case 'T':

			//
			// 1: many -> one, all images match to first in line (default)
			// 2: many -> many, nearest neighbor
			// 3: sequential image1 -> image2 -> image3 
			//
			iArg++;
			iMatchType = atoi(argv[iArg]);
			if (iMatchType != 1 &&
				iMatchType != 2 &&
				iMatchType != 3 &&
				iMatchType != 4 &&
				iMatchType != 5)
			{
				// Set to default
				iMatchType = 1;
			}

			iArg++;
			break;

		case 'n': case 'N':

			//
			// Number of nearest neighbors to consider
			//
			iArg++;
			iNeighbors = atoi(argv[iArg]);
			iArg++;
			break;

		case 'g': case 'G':

			//
			// Weight for geometry in MatchAllToAll()
			//
			iArg++;
			fGeometryWeight = atof(argv[iArg]);
			iArg++;
			break;

		case 'f': case 'F':
			//
			// Input features are listed in a single file (for long file lists)
			//
			iArg++;
			pcInputFileList = argv[iArg];
			iArg++;
			break;

		case 'l': case 'L':
			//
			// Input label file, there should be 1 label per input feature file
			//
			iArg++;
			pcLabelFile = argv[iArg];
			iArg++;
			break;

		case 's': case 'S':
			//
			// Data split image
			//
			iArg++;
			iImgSplit = atoi(argv[iArg]);
			iArg++;
			break;

		default:
			printf("Error: unknown command line argument: %s\n", argv[iArg]);
			return -1;
			break;
		}
	}

	FILE *outfileR = fopen(pcOutputFileName, "wt");
	fclose(outfileR);

	// This call is required to match many images to one
	//matchAllToOneFiles( argc-1, argv + 1, 11 );
	//return 1;

	vector< char * > vecNames;
	vector< int > vLabels;
	int iTotalFeats = 0;
	int iFeatVec = 0;
	int iFeatVecTotal = 0;

	// Create file list for read in - allows input file, for long lists of files
	// for example, longer than the linux limit: >> getconf ARG_MAX
	TextFile tfNames;
	if (pcInputFileList)
	{
		if (tfNames.Read(pcInputFileList) != 0)
		{
			printf("Error: could not read input file name list: %s\n");
			return -1;
		}
		iFeatVec = 0;
		for (int i = 0; i < tfNames.Lines(); i++)
		{
			if (strlen(tfNames.GetLine(i)) > 0)
			{
				vecNames.push_back(tfNames.GetLine(i));
				iFeatVec++;
			}
		}
		iFeatVecTotal = iFeatVec;
	}
	else
	{
		iFeatVec = 0;
		iFeatVecTotal = argc - iArg;
		for (int i = iArg; i < argc; i++)
		{
			vecNames.push_back(argv[i]);
			iFeatVec++;
		}
		assert(iFeatVec == iFeatVecTotal);
	}

	// read in labels
	TextFile tfLabels;
	if (pcLabelFile)
	{
		if (tfLabels.Read(pcLabelFile) != 0)
		{
			printf("Error: could not read input file name list: %s\n");
			return -1;
		}
		iFeatVec = 0;
		for (int i = 0; i < tfLabels.Lines(); i++)
		{
			if (strlen(tfLabels.GetLine(i)) > 0)
			{
				int iLabel = atoi(tfLabels.GetLine(i));
				vLabels.push_back(iLabel);
				iFeatVec++;
			}
		}
		assert(vLabels.size() == vecNames.size());
	}
	else
	{
		// Default labels = simply image indices
		vLabels.resize(vecNames.size());
		for (int i = 0; i < vLabels.size(); i++)
		{
			vLabels[i] = i;
		}
	}

	FILE *outfileNames = fopen("_names.txt", "wt");
	for (iFeatVec = 0; iFeatVec < iFeatVecTotal; iFeatVec++)
	{
		fprintf(outfileNames, "%s\t%d\n", vecNames[iFeatVec], vLabels[iFeatVec]);
	}
	fclose(outfileNames);

	assert(vecNames.size() == iFeatVecTotal);

	vector< int > vecMatched; // seems this is unused
	vector< vector<Feature3DInfo> > vvInputFeats;
	vecMatched.resize(vecNames.size(), -1); // unused ?
	vvInputFeats.resize(vecNames.size());

	//for( int i = iArg; i < argc; i++ )
	for (iFeatVec = 0; iFeatVec < iFeatVecTotal; iFeatVec++)
	{
		char pcImg1[400];
		sprintf(pcImg1, "%s", vecNames[iFeatVec]);
		char *pch = strrchr(pcImg1, '\\');
		if (pch) pch++; else pch = pcImg1;

		int bSameName = 0;
		for (int j = 0; j < iFeatVec && bSameName == 0; j++)
		{
			if (strcmp(vecNames[iFeatVec], vecNames[j]) == 0)
			{
				bSameName = 1;
			}
		}
		if (bSameName)
		{
			// Skip any duplicates
			//continue;
		}

		printf("Reading file %d: %s...", iFeatVec, pch);

		//if( msFeature3DVectorInputBin( vvInputFeats[iFeatVec], vecNames[iFeatVec], 140 ) < 0 )
		if (msFeature3DVectorInputText(vvInputFeats[iFeatVec], vecNames[iFeatVec], 140) < 0)
		{
			printf("Error: could not open feature file %d: %s\n", iFeatVec, vecNames[iFeatVec]);
			//return -1;
			continue;
		}

		if (bOnlyReorientedFeatures)
		{
			removeNonReorientedFeatures(vvInputFeats[iFeatVec]);
		}
		else
		{
			removeReorientedFeatures(vvInputFeats[iFeatVec]);
		}

		// Just for testing, present a mirrored version of myself
		if (0 && iFeatVec == 1)
		{
			//vvInputFeats[iFeatVec+1] = vvInputFeats[iFeatVec];
			for (int k = 0; k < vvInputFeats[iFeatVec].size(); k++)
			{
				// Something is messed up here - check previous code 
				msMirrorYFeaturesGradientOrientationHistogram(vvInputFeats[iFeatVec][k]);
				vvInputFeats[iFeatVec][k].y = 208 - vvInputFeats[iFeatVec][k].y;

				// Works the best
				vvInputFeats[iFeatVec][k].ori[0][1] = -vvInputFeats[iFeatVec][k].ori[0][1];
				vvInputFeats[iFeatVec][k].ori[1][1] = -vvInputFeats[iFeatVec][k].ori[1][1];
				vvInputFeats[iFeatVec][k].ori[2][2] = -vvInputFeats[iFeatVec][k].ori[2][2];
				vvInputFeats[iFeatVec][k].ori[2][0] = -vvInputFeats[iFeatVec][k].ori[2][0];

				//vvInputFeats[iFeatVec][k].x = 176-vvInputFeats[iFeatVec][k].x;

				//memset( &vvInputFeats[iFeatVec][k].ori[0][0], 0, sizeof(vvInputFeats[iFeatVec][k].ori) );
				//vvInputFeats[iFeatVec][k].ori[0][0] = 1;
				//vvInputFeats[iFeatVec][k].ori[1][1] = -1;
				//vvInputFeats[iFeatVec][k].ori[2][2] = 1;

				//vvInputFeats[iFeatVec][k].ori[1][1] = -vvInputFeats[iFeatVec][k].ori[1][1];

				//
				// This code seems to make the most sense for reorientation: right multiplication by a mirror
				//vvInputFeats[iFeatVec][k].ori[1][0] = -vvInputFeats[iFeatVec][k].ori[1][0];
				//vvInputFeats[iFeatVec][k].ori[1][1] = -vvInputFeats[iFeatVec][k].ori[1][1];
				//vvInputFeats[iFeatVec][k].ori[1][2] = -vvInputFeats[iFeatVec][k].ori[1][2];

				//vvInputFeats[iFeatVec][k].ori[0][1] = -vvInputFeats[iFeatVec][k].ori[0][1];
				//vvInputFeats[iFeatVec][k].ori[1][1] = -vvInputFeats[iFeatVec][k].ori[1][1];
				//vvInputFeats[iFeatVec][k].ori[2][1] = -vvInputFeats[iFeatVec][k].ori[2][1];
			}
		}

		if (0)
		{
			// Test simple flipping
			// Turns all negative DoG peaks into positive peaks
			// This may have the effect of making correspondences less distinct
			printf("Multi-modal image: %s\n", vecNames[iFeatVec]);
			msExpandMultiModalFlipPeak(vvInputFeats[iFeatVec]);
		}

		if ( /* iFeatVec == 0 && */ bMultiModal) // At some point I was only flipping the first vector
		{
			// This creates additional features, ensures that each input feature
			// has a positive and negative DoG peak
			printf("Multi-modal image: %s\n", vecNames[iFeatVec]);
			//msExpandMultiModal( vvInputFeats[iFeatVec] );
			// 2015 - see how we get more correspondences - simply peak flipping all vectors? Oct 21, 2015
			msExpandMultiModalFlipPeak(vvInputFeats[iFeatVec]);
		}
		if (iFeatVec > 0)
		{
			//removeZValue( vvInputFeats[iFeatVec] );
		}
		//removeReorientedFeatures( vvInputFeats[iFeatVec] );
		iTotalFeats += vvInputFeats[iFeatVec].size();
		printf("feats: %d, total: %d\n", vvInputFeats[iFeatVec].size(), iTotalFeats);
	}

	vecNames.resize(iFeatVec);
	vvInputFeats.resize(iFeatVec);
	vecMatched.resize(iFeatVec, -1);

	FILE *outfile = fopen("feature_count.txt", "wt");
	for (int i = 0; i < vvInputFeats.size(); i++)
	{
		//msScrambleGeometry(vvInputFeats[i] );
		//msExpandMirror( vvInputFeats[i] );
		fprintf(outfile, "%d\t%d\n", i, vvInputFeats[i].size());
	}
	fclose(outfile);

	// - This is nearest neighbor self-similarity 
	//MatchKeysNearest2ndNeighbor( vvInputFeats[0], vvInputFeats[1], vecNames[0], vecNames[1] );

	if (iMatchType == 1)
	{
		// All to one matching
		matchAllToOne(vecNames, vecMatched, vvInputFeats, bExpandedMatching, pcOutputFileName, 0, bViewFeatures); // For group-wise registration, all to one
	}
	else if (iMatchType == 2)
	{
		// All to all matching
		matchAllToAll(vecNames, vecMatched, vvInputFeats, iNeighbors, vLabels, fGeometryWeight, pcOutputFileName, iImgSplit);
	}
	else if (iMatchType == 3)
	{
		matchAllToOne(vecNames, vecMatched, vvInputFeats, bExpandedMatching, pcOutputFileName, 1); // Flag for sequential processing
	}
	else if (iMatchType == 4)
	{
		featuresMerge(vvInputFeats, pcOutputFileName);
	}
	else if (iMatchType == 5)
	{
		featuresSplit(vvInputFeats, pcOutputFileName);
	}


	//twinMatches( vecNames, vvInputFeats );

	//matchAllToAll( vecNames, vecMatched, vvInputFeats ); // For determining nearest neighbors

	//output_body_feats( vvInputFeats[1], vecNames[1] );

	return 1;

	//vector<int> vecModelMatches;
	//vector<Feature3DInfo> vecFeats2Updated;
	//vector<int> vecModelMatchesUpdated;

	//TransformSimilarity ts12, ts12Next, ts12Inv, ts31, ts32;

	//int bRefine = 1;

	//int iImg1=-1, iImg2=-1, iImg3=-1;

	//while( 1 )
	//{
	//	while( iImg1 == -1 )
	//	{
	//		iImg1 = (rand()*iFeatVec) / RAND_MAX;
	//	}
	//	while( iImg2 == -1 || iImg2 == iImg1 )
	//	{
	//		iImg2 = (rand()*iFeatVec) / RAND_MAX;
	//	}

	//	// Match 1->2
	//	MatchKeys( vvInputFeats[iImg1], vvInputFeats[iImg2], vecModelMatches, -1, 0, 0, &vecFeats2Updated, 0, &ts12.m_fScale, ts12.m_ppfRot[0], ts12.m_pfTrans );
	//	MatchKeys( vvInputFeats[iImg1], vecFeats2Updated,    vecModelMatchesUpdated, -1, 0, 0, 0, 1, &ts12.m_fScale, ts12.m_ppfRot[0], ts12.m_pfTrans );

	//	while( iImg3 == -1 || iImg3 == iImg1 || iImg3 == iImg2 )
	//	{
	//		iImg3 = (rand()*iFeatVec) / RAND_MAX;
	//	}

	//	// Match 1->3
	//	vecFeats2Updated.clear();vecModelMatchesUpdated.clear();
	//	MatchKeys( vvInputFeats[iImg1], vvInputFeats[iImg3], vecModelMatches, -1, 0, 0, &vecFeats2Updated, 0, &ts31.m_fScale, ts12.m_ppfRot[0], ts31.m_pfTrans );
	//	MatchKeys( vvInputFeats[iImg1], vecFeats2Updated,    vecModelMatchesUpdated, -1, 0, 0, 0, 1, &ts31.m_fScale, ts31.m_ppfRot[0], ts31.m_pfTrans );
	//	
	//	// Match 2->3
	//	vecFeats2Updated.clear();vecModelMatchesUpdated.clear();
	//	MatchKeys( vvInputFeats[iImg2], vvInputFeats[iImg3], vecModelMatches, -1, 0, 0, &vecFeats2Updated, 0, &ts32.m_fScale, ts32.m_ppfRot[0], ts32.m_pfTrans );
	//	MatchKeys( vvInputFeats[iImg2], vecFeats2Updated,    vecModelMatchesUpdated, -1, 0, 0, 0, 1, &ts32.m_fScale, ts32.m_ppfRot[0], ts32.m_pfTrans );

	//	// If 1->3 == 1->2->3, then continue
	//	ts32.Multiply( ts12 );
	//}

	return 1;
}



//
// main_featMatchDeformable()
//
// Here we test deformable feature alignment.
// 
//
int
main_featMatchDeformable(
	//main(
	int argc,
	char **argv
)
{
	if (argc < 3)
	{
		printf("Volumetric Feature matching v1.1\n");
		printf("Determines deformable alignment solution mapping coordinates in image 2 to image 1.\n");
		printf("Usage: %s <input keys 1> <input keys 2> <input deformation 1->2 \n", "featMatchMultiple");
		return -1;
	}

	FILE *outfileR = fopen("report.txt", "wt");
	fclose(outfileR);

	int iArg = 1;
	int bMultiModal = 0;
	char *pcDefFieldName = 0;
	while (iArg < argc && argv[iArg][0] == '-')
	{
		switch (argv[iArg][1])
		{
			// Double initial image resolution option

		case 'm': case 'M':
			// Option to extract multi-modal features
			bMultiModal = 1;
			iArg++;
			break;

		case 'd': case 'D':
			// Option to extract multi-modal features
			iArg++;
			pcDefFieldName = argv[iArg];
			iArg++;
			break;

		default:
			printf("Error: unknown command line argument: %s\n", argv[iArg]);
			return -1;
			break;
		}
	}

	// This call is required to match many images to one
	//matchAllToOneFiles( argc-1, argv + 1, 11 );
	//return 1;

	FEATUREIO fioIn;
	int bIsotropicProcessing = 0;
	nifti_image returnHeader;
	if (fioReadNifti(fioIn, pcDefFieldName, bIsotropicProcessing, 0) < 0)
	{
		//if( fioReadRaw( fioIn, argv[iArg] ) < 0 )
		{
			printf("Error: could not read input file: %s\n", argv[iArg]);
			return -1;
		}
	}

	vector< char * > vecNames;
	vector< int > vecMatched;
	vector< vector<Feature3DInfo> > vvInputFeats;
	vecNames.resize(argc);
	vvInputFeats.resize(argc);
	vecMatched.resize(argc, -1);

	int iTotalFeats = 0;
	int iFeatVec = 0;
	msFeature3DVectorInputText(vvInputFeats[0], argv[iArg], 140);
	msFeature3DVectorInputText(vvInputFeats[1], argv[iArg + 1], 140);
	removeNonReorientedFeatures(vvInputFeats[0]);
	removeNonReorientedFeatures(vvInputFeats[1]);

	vector<int> vecModelMatches;
	vector<Feature3DInfo> vecFeats2Updated;
	vector<int> vecModelMatchesUpdated;

	MatchKeysDeformationConstrained(vvInputFeats[0], vvInputFeats[1], vecModelMatches, -1, argv[iArg], argv[iArg + 1], fioIn);

	return 1;
}

