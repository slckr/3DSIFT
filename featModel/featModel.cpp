
#include <stdlib.h>
#include "src_common.h"
#include "ModelFeature3D.h"
#include "FeatureIOnifti.h"
#include <algorithm>
#include <map>
#include <list>


void
outputfeatures(
			   ModelFeature3D &mf,
			   int bLearn = 1,
			   int bSortSignificance = 1,
			   char *pcDir = 0
			   )
{
	if( bLearn )
	{
		//mf.SetValidFlag( 1 );

		//float fMinLikelihoodRatio = 2.0f;
		//SetAppearanceThresholds_f_given_s( mf.vecImgName, mf.vecFeatToImgName, mf.vecFeats,
		//	mf.vecIndexInfo, mf.vecFeatToIndexInfo, mf.vecFeatToIndexInfoCount,
		//	mf.vecFeatValid, mf.vecFeatAppearanceThresholds,
		//	fMinLikelihoodRatio );

		//int iMinMatches = 2;
		//FlagInvalidFeatures(mf.vecImgName, mf.vecFeatToImgName, mf.vecFeats,
		//	mf.vecIndexInfo, mf.vecFeatToIndexInfo, mf.vecFeatToIndexInfoCount,
		//	mf.vecFeatValid, mf.vecFeatAppearanceThresholds,
		//	mf.vecOrderToFeat, iMinMatches
		//	);

		mf.BuildClassifier( 1, 1 );

	}

	//printf( "Images: %d\n", mf.vecImgLabels.size() );
	//printf( "Images: %d\n", mf.vecImgName.size() );
	printf( "Features: %d\n", mf.vecFeats.size() );
	printf( "Valid features: %d\n", mf.vecOrderToFeat.size() );

	// Sort features according to info regarding classification
	FishersExactTest fet;
	fet.Init( mf.vecImgLabels.size() );

	FILE *outfile = fopen( "stuff.txt", "wt" );
	DATA_PAIR *pdp = new DATA_PAIR[mf.vecOrderToFeat.size()];
	for( int i = 0; i < mf.vecOrderToFeat.size(); i++ )
	{
		int iFeat = mf.vecOrderToFeat[i];
		pdp[i].iIndex = iFeat;
		if( mf.m_bayesClass.m_piCounts[1][i] - mf.m_bayesClass.m_piCounts[0][i] < 0 )
		{
			pdp[i].iIndex = -iFeat;
		}

		float fSig = fet.OneSidedPValue(
			mf.m_bayesClass.m_piCounts[0][i], mf.m_bayesClass.m_piCounts[1][i],
			mf.m_bayesClass.m_iZeros - mf.m_bayesClass.m_piCounts[0][i], mf.m_bayesClass.m_iOnes - mf.m_bayesClass.m_piCounts[1][i] );
		if( mf.m_bayesClass.m_piCounts[1][i] + mf.m_bayesClass.m_piCounts[0][i] < 7 )
		{
			fSig *= 100;
		}
		pdp[i].iValue = fSig*100000;
		
		//fSig = mf.vecFeatToIndexInfoCount[iFeat];
		fprintf( outfile, "%d\t%d\t%d\t%f\n", iFeat, mf.m_bayesClass.m_piCounts[0][i], mf.m_bayesClass.m_piCounts[1][i], fSig );

		//pdp[i].iValue = abs( mf.m_bayesClass.m_piCounts[0][i] - mf.m_bayesClass.m_piCounts[1][i]);
		//pdp[i].iValue = (100000*abs( mf.m_bayesClass.m_piCounts[1][i] - mf.m_bayesClass.m_piCounts[0][i] )) / (mf.m_bayesClass.m_piCounts[1][i] + mf.m_bayesClass.m_piCounts[0][i]);
		//pdp[i].iValue = abs( mf.m_bayesClass.m_piCounts[1][i] - mf.m_bayesClass.m_piCounts[0][i] )*(mf.m_bayesClass.m_piCounts[1][i] + mf.m_bayesClass.m_piCounts[0][i]);
	}

	fclose( outfile );

	if( bSortSignificance )
	{
		//dpSortDescending( pdp, mf.vecOrderToFeat.size() );
		dpSortAscending( pdp, mf.vecOrderToFeat.size() );
	}

	outfile = fopen( "counts.txt", "wt" );

	char *pcOutDir = "out\\";
	if( pcDir )
	{
		pcOutDir = pcDir;
	}

	for( int i = 0; i < 500 && i < mf.vecOrderToFeat.size(); i++ )//mf.vecFeatValid.size(); i++ )
	{
		//int iFeat = mf.vecOrderToFeat[i];
		int iFeat = pdp[i].iIndex;
		int iNeg = 0;
		if( iFeat < 0 )
		{
			iFeat = -iFeat;
			iNeg = 1;
		}
		MODEL_FEATURE &feat = mf.vecFeats[iFeat];

		int iIIIndex = mf.vecFeatToIndexInfo[iFeat];
		int iIICount = mf.vecFeatToIndexInfoCount[iFeat];
		float fAppThres = mf.vecFeatAppearanceThresholds[iFeat];

		char pcFileName[400];

		fprintf( outfile, "*** Feature: %d, count: %d, thres: %f\n", iFeat, iIICount, fAppThres );

		//ClassifyGeometrySXYZ cg;

		//cg.Clear();

		//mf.OutputNearestNeighbors( iFeat, "out\\nn_", 10, 0 );

		for( int j = 0; j < iIICount; j++ )
		{
			IndexInfo &II = mf.vecIndexInfo[ iIIIndex+j ];
			int iFeat2 = II.Index();
			MODEL_FEATURE &feat2 = mf.vecFeats[iFeat2];
			int iImg2 = mf.vecFeatToImgName[iFeat2];
			char *pcImgName = mf.vecImgName[ iImg2 ];
			int iLabel2 = mf.vecImgLabels[iImg2];
			int iLabel2Original = 0;// = g_vecImgLabelsBackup[iImg2];

			if( j == 0 )
			{
				sprintf( pcFileName, "%s\\sort%3.3d_feat%4.4d_count%3.3d_subject%3.3d_label%d%d_stats.txt", pcOutDir, i, iFeat, iIICount, iImg2, iLabel2, iLabel2Original  );
				mf.OutputFeatureStatsVsDist( iFeat, pcFileName, 3 );
			}

			if( II.DistSqr() < fAppThres )
			{
				fprintf( outfile, "%d\t%f\t\n", iFeat2, II.DistSqr() );

				if( iLabel2 == 1 || iLabel2 == 0 )
				{
					// See what
					//cg.AddSample( iLabel2, II.DistSqr(), feat2.x, feat2.y, feat2.z );
				}

				if( j < 5 )
				{
					FEATUREIO fio;
					if( 0 )//fioRead( fio, pcImgName ) )
					{
						sprintf( pcFileName, "%s\\sort%3.3d_feat%4.4d_count%3.3d_subject%3.3d_label%d%d_index%3.3d", pcOutDir, i, iFeat, iIICount, iImg2, iLabel2, iLabel2Original, j  );

						feat2.OutputVisualFeatureInVolume( fio, pcFileName, 1 );

						fioDelete( fio );
					}
					else
					{
						char pcFN[400];
						strcpy( pcFN, pcImgName );
						char *pc = strrchr( pcFN, '.' );
						if( pc )
						{
							sprintf( pc, ".nii.gz" );
						}
						if( fioReadNifti( fio, pcFN ) >=0 )
						{
							sprintf( pcFileName, "%s\\sort%3.3d_feat%4.4d_count%3.3d_subject%3.3d_label%d%d_index%3.3d", pcOutDir, i, iFeat, iIICount, iImg2, iLabel2, iLabel2Original, j  );

							feat2.OutputVisualFeatureInVolume( fio, pcFileName, 1 );

							fioDelete( fio );
						}
						else
						{
							printf( "Could not read image: %s\n", pcImgName );
						}
					}
				}
			}
		}

		//sprintf( pcFileName, "cg_sort%3.3d_feat%4.4d.txt",  i, iFeat);
		//cg.WriteToFile( pcFileName );
		//cg.ComputeOptimalClassificationThreshold();
	}

	fclose( outfile );

	delete [] pdp;
}

//
// classifyNearestNeighbor()
//
// This code attempts to enumerate nearest neighbors from a large set of images.
//
//
void
computeNearestNeighbors(
			   ModelFeature3D &mf
			   )
{
	int iImgPrev = -1;

	vector< float > vecCounts;
	vecCounts.resize( mf.vecImgName.size(), 0 );

	FILE *outfile = fopen( "_nn.txt", "wt" );
	for( int iFeat = 0; iFeat < mf.vecFeats.size(); iFeat++ )
	{

		MODEL_FEATURE &feat = mf.vecFeats[ iFeat ];
		int iImg1 = mf.vecFeatToImgName[iFeat];

		if( iImg1 != iImgPrev )
		{
			if( iImgPrev >= 0 )
			{
				//fprintf( outfile, "%d\t", iImgPrev );
				for( int i = 0; i < vecCounts.size(); i++ )
				{
					fprintf( outfile, "%f\t", vecCounts[i] );
				}
				fprintf( outfile, "\n" );
			}
			vecCounts.assign( vecCounts.size(), 0 );
			iImgPrev = iImg1;
		}
	
		float fNegatives = 0;

		for( int i1 = 1; i1 < mf.vecFeatToIndexInfoCount[iFeat]; i1++ )
		{
			IndexInfo &II1 = mf.vecIndexInfo[mf.vecFeatToIndexInfo[iFeat]+i1];
			int iFeat2 = II1.Index();
			int iImg2 = mf.vecFeatToImgName[iFeat2];
			//int iLabel2 = mf.vecImgLabels[iImg2];

			fNegatives += II1.DistSqrLessThanNeg();

			//if( II1.DistSqr() <= mf.vecFeatAppearanceThresholds[iFeat] )
			{
				vecCounts[iImg2] += 1.0f / (float)(i1+fNegatives);
			}
			//// else now it's just signififcant nearest neighbors
			//break;
		}
	}

	// Output one last one
	//fprintf( outfile, "%d\t", iImgPrev );
	for( int i = 0; i < vecCounts.size(); i++ )
	{
		fprintf( outfile, "%f\t", vecCounts[i] );
	}
	fprintf( outfile, "\n" );

	fclose( outfile );
}


//#include "HashRank64.h"

//
// parkinsons_model F:\McGill\parkinsons\vol\*.k3d
// ad_model F:\McGill\parkinsons\vol\*.k3d
//
// Entropy features, short data, Bioengineering 09
// ad_model_all_ent_shortdata C:\downloads\data\oasis\images\brain\*.k3e
//
// 
// -p brain_pcs.k3d -dI 140.0 -m oasis10data C:\downloads\data\oasis\images\head\OAS1_001*.k3d
// -p brain_pcs.k3d -dI 140.0 -m oasis100 C:\downloads\data\oasis\images\head\OAS1_00*.k3d
// -n 500 -dI -1 -m oasis10 C:\downloads\data\oasis\images\head\OAS1_001*.k3l
// -p brain_pcs.k3d -dI 140.0 -m oasis150 C:\downloads\data\oasis\images\brain_no_nn\*.k3d
//
// -p brain_pcs.k3d -dI 140.0 -m test16-sparse_pcs_noori C:\downloads\data\oasis\example\test16\*.k3d
// -p brain_pcs.k3d -n 500 -dI 140.0 -m test16-sparse_pcs_noori C:\downloads\data\oasis\example\test16\OAS1_001*.k3d
//
// -dI 140.0 -m ad-ori-rank C:\downloads\data\oasis\images\ad_brain_new_loc_scale\*.k3s
//
// -dI 140.0 -m ct-abdomen C:\downloads\data\liver\NLM\ct\*to-ssPatient03.k3d
// -dI 140.0 -m ct-colon C:\downloads\data\liver\CT_Colonography
//
// -dI 140.0 -m ct-colon C:\downloads\data\liver\CT_Colonography\*k3d.reg.k3d
//
// -m baby -t -r -dI 200 C:\downloads\data\lilla\features\*.key
// -m baby -t -r -dI 200 C:\downloads\data\lilla\features\deface_143*_V1_t1w.conf.feat.txt.update.key
// -m baby -t -r -dI 140 C:\downloads\data\lilla\doc\features-group\*.key
//
// -m baby-noreorient -n 1000 -t -dI 140 C:\downloads\data\lilla\doc\features-group\*.txt
//
// -m oasis-healthy-noreorient -n 1000 -t -dI 140 C:\downloads\data\oasis\images\healthy_edu_ses\*.key
// -m oasis-all-noreorient -n 1000 -t -dI 140 C:\downloads\data\oasis\images\brain_no_nn\*.key
//
// Important lesson: -dI 200 results in very poor alignment.
//
// -m styner-dwi_normal -t -r -dI 200 C:\downloads\data\styner\group\normal\*.key
// -m styner-dwi_normal_140 -t -r -dI 140 C:\downloads\data\styner\group\normal\*.key
//
// -m oasis-all-noreorient -n 1000 -t -dI 140 C:\downloads\data\oasis\images\brain_no_nn\*.key
//
// -m colon -n 1000 -t -dI 140 C:\downloads\data\liver\CT_Colonography\slicer-reg\iteration0\*.key
// -i 0-400 -m juvenile-t1w-noreorient -n 1000 -t -dI 140 C:\downloads\data\lilla\Objective1\t1w\masked-resampled-features\features\*.txt
//
//
// -i 0-100 -m ad-rank-noreorient-00 -n 1000 -t -dI 140 C:\downloads\data\oasis\images\ad_brain_new_loc_scale\OAS1_0*.key
// 
// -m rire02 -r -n 1000 -t -dI 140 C:\downloads\data\RIRE\image\pass02\*.key
// -m rire13 -r -n 1000 -t -dI 140 C:\downloads\data\RIRE\image\mm1-umN2\pass12\*.key
// -m rire -n 1000 -t -dI 140 C:\downloads\data\oasis\images\ad_brain_new_loc_scale\*.key
//
// -m GurmanLung00 -r -n 1000 -t -dI 140 C:\downloads\data\Gurman\training\*.key
//
//
// -l C:\downloads\data\dominik\labels_t01.txt -m SPMS_RRMS -n 1000 -t -dI 140 C:\downloads\data\dominik\FBM_key\SPMS_300*_t01_PDr.key 
//
// -c -l C:\downloads\data\HANS\TEST_DATA\labels_cont_high.txt -m HUNTINGTONS -n 1000 -t -dI 140 C:\downloads\data\HANS\TEST_DATA\98227\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\90842\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\90292\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\88632\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\86493\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\83476\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\77480\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\72050\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\71366\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\71011\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\63722\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\60553\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\59254\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\57978\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\55756\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\53108\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\52892\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\52788\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\52629\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\51887\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\49801\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\48969\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\47768\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\47713\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\46926\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\43696\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\42314\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\41642\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\41036\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\36087\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\35194\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\29441\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\20704\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\18040\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\17805\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\17288\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\16285\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\12141\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\11919\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\97281\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\93489\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\91752\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\91394\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\91299\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\88616\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\88395\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\88270\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\86324\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\84953\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\84153\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\82635\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\82394\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\76328\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\76130\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\74292\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\68578\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\68302\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\67490\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\67069\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\66773\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\62433\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\60262\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\59434\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\58734\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\54666\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\54627\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\53980\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\52987\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\52712\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\49568\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\49380\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\46230\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\45211\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\44888\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\42463\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\40437\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\32375\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\31610\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\27749\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\26239\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\24912\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\24795\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\24127\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\24034\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\20917\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\18927\t1_average_BRAINSABC.key C:\downloads\data\HANS\TEST_DATA\10464\t1_average_BRAINSABC.key 
//

int
main(
	 int argc,
	 char **argv
	 )
{
	if( argc < 3 )
	{
		printf( "featModel - learn a model of 3D volumetric features.\n" );
		printf( "Usage: featModel.exe <options> <features1> <features2> ...\n" );
		printf( "   where <options> are: \n" );
		printf( "      -n <negative matches>,  default -1 (all)\n" );
		printf( "      -d <hessian threshold>, default 140.0\n" );
		printf( "      -m <model name>,        default \"default_fbm_model_name\" \n" );
		return -1;
	}

	ModelFeature3D mf;

	vector<char*>		&vecImgName			= mf.vecImgName;
	vector<int>			&vecFeatToImgName	= mf.vecFeatToImgName;
	vector<MODEL_FEATURE>	&vecFeats			= mf.vecFeats;

	// Features to be read in are always floating point format
	vector<Feature3D>	vecFeatsRead;

	// PCs
	vector<Feature3D>	vecPCs;

	// Command line options

	char *pcModelName = "default_fbm_model_name";
	char *pcPCFile = 0;
	char *pcLabelFileName = 0; // 2014 - incorporate morphometry
	int iNegativeMatchesToConsider = -1; // By default, the max number of negative matches (very slow)
	//float fMinAcceptableHessianThreshold = 160.0f; // For OASIS Alzheimer's work
	float fMinAcceptableHessianThreshold = 140.0f; // For OASIS Alzheimer's work
	int bTextFileInput = 1;
	int bOnlyReorientedFeatures = 0;
	int iArg = 1;
	int iFirstImage = -1;
	int iLastImage = -1;
	int bClassifyFeatures = 0; // If true, read in the specified model, classify all feature sets on the command line
	if( argv[iArg][0] == '-' ) 
	{
		while( argv[iArg][0] == '-' )
		{
			switch( argv[iArg][1] )
			{
			// Model name
			case 'm': case 'M':
				iArg++;
				pcModelName = argv[iArg];
				iArg++;
				break;

			// Minimum dII Hessian threshold
			case 'd': case 'D':
				iArg++;
				fMinAcceptableHessianThreshold = atof( argv[iArg] );
				iArg++;
				break;

			// Label file name: if non-empty, feature-based morphometry is performed below
			case 'l': case 'L':
				iArg++;
				pcLabelFileName = argv[iArg];
				iArg++;
				break;

			// Number of negatives to consider
			case 'n': case 'N':
				iArg++;
				iNegativeMatchesToConsider = atoi(argv[iArg]);
				iArg++;
				break;

				// Principal component feature file
			case 'p': case 'P':
				iArg++;
				pcPCFile = argv[iArg];
				iArg++;
				break;

				// Text feature files
			case 't': case 'T':
				bTextFileInput = 1;
				iArg++;
				break;

				// Consider only reoriented features
			case 'r': case 'R':
				bOnlyReorientedFeatures = 1;
				iArg++;
				break;

			case 'i': case 'I':
				iArg++;
				iFirstImage = atoi( argv[iArg] );
				iLastImage = atoi( strchr( argv[iArg], '-' ) + 1 );
				iArg++;
				break;

			case 'c': case 'C':
				bClassifyFeatures = 1;
				iArg++;
				break;

			default:
				printf( "Error: unknown command line argument: %s\n", argv[iArg] );
				return -1;
				break;
			}
		}
	}
	else
	{
		// Command line has only <filename> <feature> ...
		pcModelName = argv[1];
		iArg = 2;
	}

	//XYZArray *paArrays = new XYZArray[3];

	// 1) Read in Features

	//if( pcPCFile && msFeature3DVectorInput( vecPCs, pcPCFile ) <= 0 )
	//{
	//	printf( "Error: could not read principal component file: %s\n", pcPCFile );
	//}

	if( iFirstImage < 0 )
	{
		iFirstImage = 0;
		iLastImage = argc - iArg;
	}
	if( iLastImage < 0 || iLastImage + iArg >= argc )
	{
		iLastImage = argc - iArg;
	}

	int iRejectedFeatureCount = 0;

	//for( int i = 2; i < 10; i++ )
	for( int i = iArg + iFirstImage; i < iArg + iLastImage; i++ )
	{
		int iCurrSize = vecFeats.size();
		printf( "Reading: %s...", argv[i] );
		vecFeatsRead.clear();
		int iSize = vecFeatsRead.size();

		if( bTextFileInput )
		{
			if( msFeature3DVectorInputText( vecFeatsRead, argv[i], fMinAcceptableHessianThreshold ) < 0 )
			{
				continue;
			}
		}
		else
		{
			//if( !msFeature3DVectorInput( vecFeats, argv[i], 30.0f ) ) // Testing parallel
			//if( !msFeature3DVectorInput( vecFeats, argv[i], 115.0f ) ) // Alzheimers - reduce
			//if( !msFeature3DVectorInput( vecFeats, argv[i], 140.0f ) ) // Alzheimers - reduce
			//if( !msFeature3DVectorInput( vecFeats, argv[i], 140.0f ) ) // ICBM 152 sex data trials
			if( !msFeature3DVectorInput( vecFeatsRead, argv[i], fMinAcceptableHessianThreshold ) ) // Alzheimers - reduce
			{
				printf( "Error: could not open input file: %s\n", argv[i] );
				return -1;
			}
		}

		// Normalize features, and convert to character format
		for( int iFeat = 0; iFeat < vecFeatsRead.size(); iFeat++ )
		{
			// Convert and save model feature
			MODEL_FEATURE feat;
			feat.InitFromFeature3D( vecFeatsRead[iFeat] );

			if( !strstr( argv[i], ".k3l" ) )
			{
				//feat.NormalizeData();
				if( vecPCs.size() > 0 )
				{
					//feat.ProjectToPCs( vecPCs );
				}
			}
			else
			{
				// these are line features, all ready
			}

			feat.NormalizeDataRankedPCs();

			int iCode = iFeat;

			if( bOnlyReorientedFeatures )
			{
				if( !(feat.m_uiInfo & INFO_FLAG_REORIENT ) )
				{
					// Only consider reoriented features
					continue;
				}
			}
			else
			{
				if( feat.m_uiInfo & INFO_FLAG_REORIENT )
				{
					// Only consider non-reoriented features
					continue;
				}

				// Jan 2014
				// If non-reoriented, then reset the orientation to identity				
				memset( &feat.ori[0][0], 0, sizeof( feat.ori ) );
				feat.ori[0][0] = 1;
				feat.ori[1][1] = 1;
				feat.ori[2][2] = 1;

			}
			if( feat.x < 0 || feat.x > 300
				|| feat.y < 0 || feat.y > 300
				|| feat.z < 0 || feat.z > 300 )
			{
				// Forget way out features
				iRejectedFeatureCount++;
				//continue;
			}

			//feat.DeterminePartialOrientationCube( iCode );
			vecFeats.push_back( feat );

			//vecFeats.push_back( vecFeatsRead[iFeat] );
		}

		printf( "Features: %d\n", vecFeats.size()-iCurrSize );

		//FILE *outfile = fopen( "feature_eigs.txt", "wt" );
		//for( int iFeat = 0; iFeat < mf.vecFeats.size(); iFeat++ )
		//{
		//	MODEL_FEATURE &feat3D = vecFeats[iFeat];
		//	float fEigSum = feat3D.eigs[0] + feat3D.eigs[1] + feat3D.eigs[2];
		//	float fEigPrd = feat3D.eigs[0]*feat3D.eigs[1]*feat3D.eigs[2];
		//	float fEigSumProd = fEigSum*fEigSum*fEigSum;
		//	fprintf( outfile, "%f\t%f\t%f\n", mf.vecFeats[iFeat].scale, fEigSumProd, fEigPrd );
		//}
		//fclose( outfile );

		// Save file name
		vecImgName.push_back( argv[i] );

		// Save index to file associated with features
		for( int j = iCurrSize; j < vecFeats.size(); j++ )
		{
			vecFeatToImgName.push_back(i-iArg);
		}

		//FEATUREIO	fioIn;
		//PpImage		ppImgIn;
		//if( !fioimgRead( argv[1], fioIn, ppImgIn ) )
		//{
		//	printf( "Error: could not open input file: %s\n", argv[1] );
		//	return -1;
		//}
	}

	//HashRank64 hf;
	//hf.Init( vecFeats );
	//mf.LoopThroughFeaturesForFun();

	//investigateFeatureStorage( vecFeats );

	// Determine orientation
	//mf.DetermineOrientation();

	//
	// 2) Normalize feature measurements
	// Normalization is now done after features are read in
	//for( int iFeat = 0; iFeat < vecFeats.size(); iFeat++ )
	//{
	//	vecFeats[iFeat].NormalizeData();
	//}

	//mf.Write( "alzheimers_model_replace" );

	//MODEL_FEATURE &feat0 = vecFeats[0];
	//MODEL_FEATURE &featN = vecFeats[vecFeats.size()-1];

	// 3) Compute correspondences (would be nice to parallelize...
	vector<IndexInfo>	&vecIndexInfo				= mf.vecIndexInfo;
	vector<int>			&vecFeatToIndexInfo			= mf.vecFeatToIndexInfo;
	vector<int>			&vecFeatToIndexInfoCount	= mf.vecFeatToIndexInfoCount;

	int iPCs = 0;
	if( vecPCs.size() >= Feature3DInfo::FEATURE_3D_PCS )
	{
		iPCs = Feature3DInfo::FEATURE_3D_PCS;
		iPCs = 16;
	}

	if( vecFeats[0].m_uiInfo & INFO_FLAG_LINE )
	{
		// Line features use 16 samples in PC field
		iPCs = 16;
	}

	//char *pcFeatFileName = vecImgName[ 2 ];
	//char pcImageName[400];
	//sprintf( pcImageName, "%s", pcFeatFileName );
	//char *pch = strrchr( pcImageName, '.' );
	//if( pch )
	//{
	//	sprintf( pch, ".nii.gz" );

	//	FEATUREIO fio;
	//	if( fioReadNifti( fio, pcImageName, 1 ) >= 1 )
	//	{
	//		char pcFileName[300];
	//		sprintf( pcFeatFileName, "test_feature" );
	//		Feature3DInfo feat2;
	//		feat2.x = feat2.y = feat2.z = 100;
	//		feat2.scale = 10;
	//		feat2.OutputVisualFeatureInVolume( fio, pcFeatFileName, 1 );
	//		fioDelete( fio );
	//	}
	//}

	// Consider SIFT features ... 
	iPCs = 64;

	//iNegativeMatchesToConsider = vecFeats.size();//vecImgName.size(); // vecFeats.size();

	vecFeatToIndexInfo.resize( vecFeats.size() );
	vecFeatToIndexInfoCount.resize( vecFeats.size() );
	//GenerateIndexInfoParallel(
	GenerateIndexInfo(
	//GenerateIndexInfoScaleSpaceHash(
	//GenerateIndexInfoSubSection(
		vecImgName, vecFeatToImgName, vecFeats,
		vecIndexInfo, vecFeatToIndexInfo, vecFeatToIndexInfoCount,
		iPCs, //Feature3DInfo::FEATURE_3D_PCS,
		iNegativeMatchesToConsider
 		);


	// 4) Set appearance thresholds for each feature (there are lots)
	vector<float>		&vecFeatAppearanceThresholds	= mf.vecFeatAppearanceThresholds;
	vector<int>			&vecFeatValid					= mf.vecFeatValid;
	vector<int>			&vecImgLabels					= mf.vecImgLabels;
	// 5) Perform clustering to determine a minimal set of descriptive features
	vector<int>			&vecOrderToFeat					= mf.vecOrderToFeat;


	FILE *outfile;
	printf( "Setting appearance thresholds ... \n" );
	float fMinLikelihoodRatio = 1.1f;
	int bLikelihoodRatioMaximum = 0;
	int iMinMatches = 1;
	printf( "Min Likelihood Ratio = %f\n", fMinLikelihoodRatio );

	// January 2014 - split into FBA or FBM models
	if( pcLabelFileName )
	{
		// This code building a model with feature class labels should be rolled into the same
		// as below in the case where no class labels are present (where all features are from the same class 0)
		// Models should be identical - to be tested 2014

		// Build an FBM model: Feature-based Morphometry

		TextFile tfTextLabels;
		mf.ReadDataInteger( pcLabelFileName, tfTextLabels, vecImgLabels );

		vector<int> &vecFeatValid = mf.vecFeatValid;
		vecFeatValid.resize( vecFeats.size(), 1 );

		// Learn models
		//SetAppearanceThresholds_f_given_s_labels(
		SetAppearanceThresholds_f_given_s_labels_new2(
		//SetAppearanceThresholds_f_given_s_fixed(
		//SetAppearanceThresholds_f_given_s_lowesterror(
			mf.vecImgName, mf.vecFeatToImgName, mf.vecFeats,
			mf.vecIndexInfo, mf.vecFeatToIndexInfo, mf.vecFeatToIndexInfoCount,
			mf.vecFeatValid,
			mf.vecImgLabels,
			mf.vecFeatAppearanceThresholds,
			fMinLikelihoodRatio,
			bLikelihoodRatioMaximum );

		FlagInvalidFeatures_labels(mf.vecImgName, mf.vecFeatToImgName, mf.vecFeats,
			mf.vecIndexInfo, mf.vecFeatToIndexInfo, mf.vecFeatToIndexInfoCount,
			mf.vecFeatValid, mf.vecFeatAppearanceThresholds,
			mf.vecImgLabels,
			mf.vecOrderToFeat, iMinMatches
			);

		printf( "Valid features: %d\n", mf.vecOrderToFeat.size() );
		outputfeatures( mf, 1, 1 );
	}
	else
	{
		// Build an FBA model: Feature-based Alignment

		SetAppearanceThresholds_f_given_s( vecImgName, vecFeatToImgName, vecFeats,
			vecIndexInfo, vecFeatToIndexInfo, vecFeatToIndexInfoCount,
			mf.vecFeatValid, mf.vecFeatAppearanceThresholds,
			fMinLikelihoodRatio );

		outfile = fopen( "list_counts.txt", "wt" );
		for( int i = 0; i < vecFeats.size(); i++ )
		{
			MODEL_FEATURE &feat = vecFeats[ i ];

			int iCount = vecFeatToIndexInfoCount[ i ];
			float fThres = vecFeatAppearanceThresholds[i] ;
			fprintf( outfile, "%d\n", iCount );
		}	
		fclose( outfile );

		// 5) Perform clustering to determine a minimal set of features

		printf( "Min Matches = %d\n", iMinMatches );
		FlagInvalidFeatures(vecImgName, vecFeatToImgName, vecFeats,
			vecIndexInfo, vecFeatToIndexInfo, vecFeatToIndexInfoCount,
			mf.vecFeatValid, mf.vecFeatAppearanceThresholds,
			vecOrderToFeat, iMinMatches
			);

	}

	// Here, output most significant features
	//outputfeatures( mf, 1, 1 );

	computeNearestNeighbors( mf );

	return 1;
}

